/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 *			    Battle & death module			    *
 ****************************************************************************/

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"


void change_align ( CHAR_DATA *ch, CHAR_DATA *victim, bool attack );
extern char		lastplayercmd[MAX_INPUT_LENGTH];
extern CHAR_DATA *	gch_prev;
OBJ_DATA *used_weapon;   /* Used to figure out which weapon later */
CHAR_DATA * infected;
void majin( CHAR_DATA *ch, char * argument );
void finalexplosion( CHAR_DATA *ch, char * argument );
void steam_transform( CHAR_DATA *ch );
int kidefend( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt );
double sqrt( double x );
double log10( double x );
double pow( double x, double y );
unsigned long long correctpl( CHAR_DATA *ch, unsigned long long inc );
void move_room(CHAR_DATA *ch, CHAR_DATA *victim);
/*
 * Local functions.
 */
void     new_dam_message	args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt, OBJ_DATA *obj, int greatest ) );        
bool     can_defend ( int dt );
void     ApplyEffect( CHAR_DATA *ch );
bool     unequal_rank( CHAR_DATA *ch, CHAR_DATA *victim ) ;
void     wear_obj        args( ( CHAR_DATA *ch, OBJ_DATA *obj, bool fReplace, sh_int wear_bit ) );
void	 death_cry	args( ( CHAR_DATA *ch ) );
void	 group_gain	args( ( CHAR_DATA *ch, CHAR_DATA *victim ) );
void	 pl_gain  	args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dam ) );
int	 align_compute	args( ( CHAR_DATA *gch, CHAR_DATA *victim ) );
ch_ret	 one_hit		args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dt ) );
int  	 obj_hitroll	args( ( OBJ_DATA *obj ) );
void     show_condition  args( ( CHAR_DATA *ch, CHAR_DATA *victim ) );
void     update_current        ( CHAR_DATA *ch, char *argument );
int	 xp_compute	args( ( CHAR_DATA *gch, CHAR_DATA *victim ) );
unsigned long long get_root( unsigned long long powerlevel );
unsigned long long My_sqrt( unsigned long long value );
unsigned long long My_abs(unsigned long long number);
void bio_gains( CHAR_DATA *ch, CHAR_DATA *victim, int dam );
void bio_stats( CHAR_DATA *ch, CHAR_DATA *victim, int where );
void rank_killer ( CHAR_DATA *ch );        /* Levels in accordance with the new system */
void exclusive_echo( CHAR_DATA *ch, ROOM_INDEX_DATA *room, char *argument );
int  gainspeed( CHAR_DATA *ch );
int  next_style( CHAR_DATA *ch );
void bioskills( CHAR_DATA *ch, CHAR_DATA *victim );
void emotion_plus( CHAR_DATA *ch, CHAR_DATA *victim, int dam );
bool hellfighter_absorbtion( CHAR_DATA *ch, int dam );
void absorb( CHAR_DATA *ch, CHAR_DATA *victim );

char *  const   StyleNames       [] =
{
  "Fighting", "Offensive", "Defensive", "Evasive", "Tank", "Ki-Lord", "Down-Right Fierce",
  "MENSA", "Berserker", "Tempest", "Indestructible Aura", "Mastermind", "Meathead",
  "Circular Weaponry", "Ghandi", "ADHD", "Dead-Angle", "Narcophobic", "Meltdown",
  "Nukeproofing", "Tira"
};


void update_current( CHAR_DATA *ch, char *argument )
{
 int mod = ch->plmod;
 CHAR_DATA *pet;

 if ( ch->plmod < 1 || ch->plmod > 50000 )
      ch->currpl = ch->basepl;
 else
 {
      if ( ch->enraged )
	  mod += ( ch->rage * 5 );
      if ( ch->race == RACE_BIOANDROID && ch->plmod > 99)
	  mod += ( ch->pkills * 8 );
      if ( ch->bursttimer <= 0 )
      mod = URANGE( 0, mod, 50000 );
      ch->currpl = ( ch->basepl / 100 ) * ( mod );
      if ( ch->currpl < ch->basepl && ch->plmod >= 100 )
   	  ch->currpl = ch->basepl;
      if ( ch->race == RACE_WIZARD )
      {
           for ( pet = ch->in_room->first_person; pet; pet = pet->next_in_room )
           {
	        if ( !str_cmp(pet->name, ch->pcdata->petname) )
                {
		   pet->basepl = ch->basepl * 20;
		   pet->currpl = ch->basepl * 20;
	        }
	   }
      }

 }
 return;
}

int rank_dif( CHAR_DATA *ch, CHAR_DATA *victim ) 
{
short cha = 0;
short vic = 0;
unsigned long long fighter      =      1000000;
unsigned long long warrior      =     10000000;
unsigned long long master       =    100000000;
unsigned long long bloodthirsty =   1000000000;
unsigned long long warlover     =   1000000000;
unsigned long long disruptor    =   1000000000;
unsigned long long battlefreak  =   1000000000;
unsigned long long powermonger  =   1000000000;
warlover *= 10;
disruptor *= 100;
battlefreak *= 1000;
powermonger *= 10000;

     if ( victim->basepl < fighter      )	vic = 1;
else if ( victim->basepl < warrior      )	vic = 2;
else if ( victim->basepl < master       )	vic = 3;
else if ( victim->basepl < bloodthirsty )  	vic = 4;
else if ( victim->basepl < warlover     )	vic = 5;
else if ( victim->basepl < disruptor    )  	vic = 6;
else if ( victim->basepl < battlefreak  )  	vic = 7;
else if ( victim->basepl < powermonger  )  	vic = 8;
else 						vic = 9;

     if ( ch->basepl < fighter      )  		cha = 1;
else if ( ch->basepl < warrior      )		cha = 2;
else if ( ch->basepl < master       )		cha = 3;
else if ( ch->basepl < bloodthirsty )		cha = 4;
else if ( ch->basepl < warlover     )		cha = 5;
else if ( ch->basepl < disruptor    )		cha = 6;
else if ( ch->basepl < battlefreak  )		cha = 7;
else if ( ch->basepl < powermonger  )		cha = 8;
else						cha = 9;

return ( cha - vic );
}


void do_fuse( CHAR_DATA *ch, char *argument )
{
 char        arg[MAX_INPUT_LENGTH];
 char        argument2[MAX_INPUT_LENGTH];
 char        newname[6];
 char        newname2[6]; 
 char	     buf[MAX_STRING_LENGTH];
 CHAR_DATA * buddy;
 OBJ_DATA  * obj_next;
// OBJ_DATA  * obj_lose;  
 OBJ_DATA  * obj; 
 OBJ_INDEX_DATA  * obj2;
 int         sn;
 int y = 0;
 int x = 70;
 

 if ( !argument || argument[0] == '\0')
 {
    pager_printf( ch, "You must choose someone to fuse with, or set a fusion partner." );
    return;
 }

 argument = one_argument( argument, arg );
 argument = one_argument( argument, argument2 );
 
 if ( !str_cmp( arg, "partner" ) )
 {
    buddy = get_char_room( ch, argument2 );
    if ( !buddy || IS_NPC( buddy ) )
    {
       pager_printf( ch, "They aren't here.\n\r" );
       return;
    }
    if ( ch->fusepartner && !str_cmp( ch->fusepartner, buddy->name ) )
    {
       pager_printf( ch, "They already are your fusion partner\n\r" );
       return;
    }

    if ( ch == buddy || buddy->currpl > ch->currpl * 1.05 || buddy->currpl < ch->currpl * 0.95 )
    {
       pager_printf( ch, "Your powerlevels are too different.\n\r" );
       return;
    }   
    STRFREE( ch->fusepartner );
    ch->fusepartner = STRALLOC( argument2 );
    pager_printf( ch, "Your fusion buddy is now %s.\n\r", argument2 );
    pager_printf( buddy, "%s has picked you as a fusion buddy!\n\r", ch->name );
    return;    
 }
 
 buddy = get_char_room( ch, arg );

 if ( !buddy || IS_NPC( buddy ) || ch->in_room != buddy->in_room || ch == buddy )
 {
    pager_printf( ch, "Hello... They're not here.\n\r" );
    return;
 }

/*
 if ( ch->rps < 75.0 || buddy->rps < 75.0 )
 {
	pager_printf( ch, "You need at least 75 rps each to fuse.\n\r" );
	return;
 }
*/ 

 if ( buddy->fusepartner && ch->name && str_cmp( buddy->fusepartner, ch->name ) )
 {
	pager_printf( ch, "You are not fusion partners.\n\r" );
	return;
 }

 if ( ch->fused == TRUE || buddy->fused == TRUE ) 
 {
    pager_printf( ch, "Fused fuses?  Nice try.\n\r" );
    return; 
 }

 pager_printf( ch, "You failed.\n\r" );
 return;
 
 buddy->back_str  = buddy->perm_str;
 buddy->back_int  = buddy->perm_int;
 buddy->back_wis  = buddy->perm_wis;
 buddy->back_dex  = buddy->perm_dex;
 buddy->back_con  = buddy->perm_con;
 buddy->back_cha  = buddy->perm_cha;
 buddy->back_sex  = buddy->sex; 
 buddy->back_lck  = buddy->perm_lck;      
 buddy->backpl    = buddy->basepl;
 buddy->back_race = buddy->race;
 buddy->back_name = str_dup( buddy->name );

 ch->back_str  = ch->perm_str;
 ch->back_int  = ch->perm_int;
 ch->back_wis  = ch->perm_wis;
 ch->back_dex  = ch->perm_dex;
 ch->back_con  = ch->perm_con;
 ch->back_cha  = ch->perm_cha;
 ch->back_sex  = ch->sex;
 ch->back_lck  = ch->perm_lck;      
 ch->backpl   = ch->basepl;
 ch->back_race = ch->race;
 ch->back_name = str_dup( ch->name); 
 do_save( ch, "" );
 do_save( buddy, "" );

 // Random Aesthetics
 
 sprintf( buf, "Ready? O.K!" );
 do_chat( ch, buf );
 do_chat( buddy, buf ); 

 sprintf( buf, "Fu...." );
 do_chat( ch, buf );
 do_chat( buddy, buf ); 

 sprintf( buf, "Sion...." );
 do_chat( ch, buf );
 do_chat( buddy, buf ); 

 sprintf( buf, "HA!!" );
 do_chat( ch, buf );
 do_chat( buddy, buf ); 


 talk_info( AT_PINK, "A blinding flash envelops the world!  Could it be... there has been... a fusion?!" );
 
 // w00t.  Maths.
 
 newname[0] = ch->name[0];
 newname[1] = ch->name[1];
 newname[2] = ch->name[2];

 for ( ; ; )
 {
     if ( buddy->name[y] == '\0' )
     {
        y--; 
        break;
     }
     y++;
 }
 
 newname[3] = buddy->name[y - 2];
 newname[4] = buddy->name[y - 1]; 
 newname[5] = buddy->name[y];
 newname[6] = '\0';
 
 y = 0;
 for ( ; ; )
 {
     if ( ch->name[y] == '\0' )
     {
        y--; 
        break;
     }
     y++;
 }
 newname2[0] = buddy->name[0];
 newname2[1] = buddy->name[1];
 newname2[2] = buddy->name[2];
 newname2[3] = ch->name[y - 2];
 newname2[4] = ch->name[y - 1]; 
 newname2[5] = ch->name[y];
 newname2[6] = '\0';
 
  
 // Prepared as much as I can.  Time to check for fuck-ups.

 ch->fused = TRUE;
 buddy->fused = TRUE;
 ch->fusetimer = 1800;
 buddy->fusetimer = 1800; 

// !can_use_skill( ch, number_percent(), gsn_fuse ) || !can_use_skill( buddy, number_percent(), gsn_fuse ) 
 
 if ( ch->pcdata->learned[gsn_fuse] < 100 || buddy->pcdata->learned[gsn_fuse] < 100 ) 
 {
    ch->fusetimer /= 2;
    buddy->fusetimer /= 2;
    // Time to punish them.
    ch->pcdata->learned[gsn_fuse] += 20;
    buddy->pcdata->learned[gsn_fuse] += 20;
    if ( ch->pcdata->learned[gsn_fuse] > 100 )
       ch->pcdata->learned[gsn_fuse] = 100;
    if ( buddy->pcdata->learned[gsn_fuse] > 100 )
       buddy->pcdata->learned[gsn_fuse] = 100;
    if ( number_range( 1, 2 ) == 1 )
    {
        strcat( "Old-", newname );
        ch->perm_str =  10;
        ch->perm_int -= buddy->perm_int / 2;
        ch->perm_wis -= buddy->perm_wis / 2;
        ch->perm_dex -= buddy->perm_dex / 2;
        ch->perm_con =  10;
        ch->perm_cha =  10;
        ch->perm_lck -= buddy->perm_lck / 2;                                                       
        ch->basepl   -= buddy->basepl / 2;        
        if ( ch->sex != buddy->sex )
           ch->sex = 0;
        pager_printf( ch, "You look down to see your glorious new fo-  Wait a minute... Something went wrong!  You must have messed up the dance!\n\r" );
    }
    else
    {
         strcat( "Fat-", newname );
        ch->perm_str -= buddy->perm_str / 2;
        ch->perm_int  = 10;
        ch->perm_wis  = 10;
        ch->perm_dex  = 10;
        ch->perm_con -= buddy->perm_con / 2;
        ch->perm_cha -= buddy->perm_cha / 2;
        ch->perm_lck  = 10;
        ch->basepl   -= buddy->basepl / 2;
        if ( ch->sex != buddy->sex )
           ch->sex = 0;
        pager_printf( ch, "You look down to see your glorious new fo-  Wait a minute... Something went wrong!  You must have messed up the dance!\n\r" );        
    }
    
    if ( number_range( 1, 2 ) == 1 )
    {
        strcat( "Old-", newname2 );
        buddy->perm_str =  10;
        buddy->perm_int -= ch->perm_int / 2;
        buddy->perm_wis -= ch->perm_wis / 2;
        buddy->perm_dex -= ch->perm_dex / 2;
        buddy->perm_con =  10;
        buddy->perm_cha =  10;
        buddy->perm_lck -= ch->perm_lck / 2;                                                       
        buddy->basepl   -= ch->basepl / 2;
        if ( ch->sex != buddy->sex )
           ch->sex = 0;
        pager_printf( buddy, "You look down to see your glorious new fo-  Wait a minute... Something went wrong!  You must have messed up the dance!\n\r" );
    }
    else
    {
        strcat("Fat-", newname2 );
        buddy->perm_str -= ch->perm_str / 2;
        buddy->perm_int  = 10;
        buddy->perm_wis  = 10;
        buddy->perm_dex  = 10;
        buddy->perm_con -= ch->perm_con / 2;
        buddy->perm_cha -= ch->perm_cha / 2;
        buddy->perm_lck  = 10;
        buddy->basepl   -= ch->basepl / 2;
        if ( ch->sex != buddy->sex )
           ch->sex = 0;
        pager_printf( buddy, "You look down to see your glorious new fo-  Wait a minute... Something went wrong!  You must have messed up the dance!\n\r" );           
    }    
 }
 else
 {
    // Base is +50% of what the buddy has
    // Idea from Vice - Different races add in more of particular stat

   
    ch->basepl    += ( buddy->basepl   * 0.5 );
    ch->perm_str  += ( buddy->perm_str * 0.5 );
    ch->perm_int  += ( buddy->perm_int * 0.5 );
    ch->perm_wis  += ( buddy->perm_wis * 0.5 );
    ch->perm_dex  += ( buddy->perm_dex * 0.5 );
    ch->perm_con  += ( buddy->perm_con * 0.5 );
    ch->perm_cha  += ( buddy->perm_cha * 0.5 );
    ch->perm_lck  += ( buddy->perm_lck * 0.5 );
    switch ( buddy->race )
    {
        default: 
             break;   
        case RACE_SAIYAN:            
             ch->perm_str  += ( buddy->perm_str * 0.3 );        
             break;
        case RACE_HUMAN:             
             ch->perm_con  += ( buddy->perm_con * 0.3 );        
             break;
        case RACE_HALFBREED:             
             ch->perm_str  += ( buddy->perm_str * 0.3 );        
             break;
        case RACE_TUFFLE:             
             ch->perm_int  += ( buddy->perm_int * 0.3 );        
             break;
        case RACE_ICER:             
             ch->perm_con += ( buddy->perm_con * 0.3 );        
             break;
        case RACE_ANDROID:             
             ch->perm_str  += ( buddy->perm_str * 0.3 );                
             break;
        case RACE_BIOANDROID:             
             ch->perm_lck  += ( buddy->perm_lck * 0.5 );        
             break;
        case RACE_NAMEK:             
             ch->perm_wis  += ( buddy->perm_wis * 0.3 );        
             break;             
        case RACE_MUTANT:             
             ch->perm_cha  += ( buddy->perm_cha * 0.3 );        
             break;
        case RACE_KAI:             
             ch->perm_wis  += ( buddy->perm_wis * 0.3 );        
             break;
        case RACE_WIZARD:             
             ch->perm_int  += ( buddy->perm_int * 0.3 );        
             break;
        case RACE_DEMON:             
             ch->perm_dex  += ( buddy->perm_dex * 0.3 );        
             break;   
    }

    // Now sort out the buddy's stats
        if ( ch->sex != buddy->sex )
           ch->sex = 0;
    
    buddy->basepl    += ( ch->basepl   * 0.5 );
    buddy->perm_str  += ( ch->perm_str * 0.5 );
    buddy->perm_int  += ( ch->perm_int * 0.5 );
    buddy->perm_wis  += ( ch->perm_wis * 0.5 );
    buddy->perm_dex  += ( ch->perm_dex * 0.5 );
    buddy->perm_con  += ( ch->perm_con * 0.5 );
    buddy->perm_cha  += ( ch->perm_cha * 0.5 );
    buddy->perm_lck  += ( ch->perm_lck * 0.5 );    
   switch ( ch->race )
    {
        default: 
             break;   
        case RACE_SAIYAN:            
             buddy->perm_str  += ( ch->perm_str * 0.3 );        
             break;
        case RACE_HUMAN:             
             buddy->perm_con  += ( ch->perm_con * 0.3 );        
             break;
        case RACE_HALFBREED:             
             buddy->perm_str  += ( ch->perm_str * 0.3 );        
             break;
        case RACE_TUFFLE:             
             buddy->perm_int  += ( ch->perm_int * 0.3 );        
             break;
        case RACE_ICER:             
             buddy->perm_con  += ( ch->perm_con * 0.3 );        
             break;
        case RACE_ANDROID:             
             buddy->perm_str  += ( ch->perm_str * 0.3 );                
             break;
        case RACE_BIOANDROID:             
             buddy->perm_lck  += ( ch->perm_lck * 0.5 );        
             break;
        case RACE_NAMEK:             
             buddy->perm_wis  += ( ch->perm_wis * 0.3 );        
             break;             
        case RACE_MUTANT:             
             buddy->perm_cha  += ( ch->perm_cha * 0.3 );        
             break;
        case RACE_KAI:             
             buddy->perm_wis  += ( ch->perm_wis * 0.3 );        
             break;
        case RACE_WIZARD:             
             buddy->perm_int  += ( ch->perm_int * 0.3 );        
             break;
        case RACE_DEMON:             
             buddy->perm_dex  += ( ch->perm_dex * 0.3 );        
             break;   
    }
    
    pager_printf( ch, "You look down at your glorious new form... SO AWESOME!\n\r" ); 
    pager_printf( buddy, "You look down at your glorious new form... SO AWESOME!\n\r" );
 }
 
 // This stuff happens, successful or not.
    ch->race = RACE_FUSED;
    buddy->race = RACE_FUSED; 

 // Knock out their form bonuses etc without them seeing do_powerdown

    while ( buddy->first_affect )
    	affect_remove( buddy, buddy->first_affect );
    while ( ch->first_affect )
    	affect_remove( ch, ch->first_affect );
    ch->plmod    = 100;
    buddy->plmod = 100;
    ch->multi_str = 0;
    ch->multi_int = 0;
    ch->multi_wis = 0;
    ch->multi_dex = 0;
    ch->multi_con = 0;
    ch->multi_cha = 0;
    ch->multi_lck = 0;
    buddy->multi_str = 0;
    buddy->multi_int = 0;
    buddy->multi_wis = 0;
    buddy->multi_dex = 0;
    buddy->multi_con = 0;
    buddy->multi_cha = 0;
    buddy->multi_lck = 0;
    update_current( ch, NULL );
    update_current( buddy, NULL );
    
 // Clear out the old EQ for the new

/*    for ( obj_lose= ch->first_carrying; obj_lose; obj_lose=obj_next ) 
    {
         obj_next = obj_lose->next_content;
         obj_from_char( obj_lose );
    }
    for ( obj_lose= buddy->first_carrying; obj_lose; obj_lose=obj_next ) 
    {
         obj_next = obj_lose->next_content;
         obj_from_char( obj_lose );
    } */

    do_remove( ch, "all" );
    do_remove( buddy, "all" );
    
 // Load in the new EQ and put it on them -  Still needs building.  Using vnums 70-73

    
    for ( ; ; )
    {
        obj2 = get_obj_index( x );
	obj = create_object( obj2, 1 );
        obj_to_char( obj, ch );
        x++;
        if ( x > 72 )
           break;
    }

	for ( obj = ch->first_carrying; obj; obj = obj_next )
	{
	    obj_next = obj->next_content;
		wear_obj( ch, obj, FALSE, -1 );
	}    

 // Same for buddy.

    x = 70;

    for ( ; ; )
    {
        obj2 = get_obj_index( x );
	obj = create_object( obj2, 1 );
        obj_to_char( obj, buddy );
        x++;
        if ( x > 72 )
           break;
    }

	for ( obj = buddy->first_carrying; obj; obj = obj_next )
	{
	    obj_next = obj->next_content;
		wear_obj( buddy, obj, FALSE, -1 );
	}          


 // Remove all skills, then max out all the fusion skills

    do_capsule( ch, "- all" );
    do_capsule( buddy, "- all" );    

    ch->practice = 10;
    buddy->practice = 10;    

    sn = 0; 
/*    for ( ; ; )
    {
        if ( skill_table[sn] && skill_table[sn]->race_adept[ch->race] > 10 )
           ch->pcdata->learned[sn] = 0;
        sn++;
        if ( !skill_table[sn] || sn > 500 )
           break;
    }

    sn = 0; */
    for ( ; ; )
    {
        if ( skill_table[sn]->race_adept[RACE_FUSED] >= 10 )
           ch->pcdata->learned[sn] = skill_table[sn]->race_adept[RACE_FUSED];
        sn++;
        if ( !skill_table[sn] || sn > 500 )
           break;
    }

/*    sn = 0; 
    for ( ; ; )
    {
        if ( skill_table[sn] && skill_table[sn]->race_adept[ch->race] > 10)
           buddy->pcdata->learned[sn] = 0;
        sn++;
        if ( !skill_table[sn] || sn > 500 )
           break;
    }*/

    sn = 0; 
    for ( ; ; )
    {
        if ( skill_table[sn]->race_adept[RACE_FUSED] >= 10 )
           buddy->pcdata->learned[sn] = skill_table[sn]->race_adept[RACE_FUSED];
        sn++;
        if ( !skill_table[sn] || sn > 500 )
           break;
    }

    pager_printf( ch, "NOTE: You have a new set of equipment, and a new set of skills!\n\r" ); 
    pager_printf( buddy, "NOTE: You have a new set of equipment, and a new set of skills!\n\r" );
        
 // New Names
  
     STRFREE( ch->name );
     STRFREE( buddy->name );
     ch->name = STRALLOC( newname );
     buddy->name = STRALLOC( newname2 ); 
  
    // And that's all
}


// Nice, easy function for un-fusing.

void fuse_split( CHAR_DATA *ch )
{
 int sn;
 OBJ_DATA  * obj_next;
 OBJ_DATA  * obj_lose;  

 pager_printf( ch, "You explode in a flash of white light, and you are no longer fused!\n\r" );
 ch->fused = FALSE;

 // Knock out their form bonuses etc without them seeing do_powerdown

 while ( ch->first_affect )
   	affect_remove( ch, ch->first_affect );
 ch->plmod     = 100;
 ch->multi_str = 0;
 ch->multi_int = 0;
 ch->multi_wis = 0;
 ch->multi_dex = 0;
 ch->multi_con = 0;
 ch->multi_cha = 0;
 ch->multi_lck = 0;

 // Return their stats
 
 ch->perm_str = ch->back_str;
 ch->perm_int = ch->back_int;
 ch->perm_wis = ch->back_wis;
 ch->perm_dex = ch->back_dex;
 ch->perm_con = ch->back_con;
 ch->perm_cha = ch->back_cha;
 ch->perm_lck = ch->back_lck;      
 ch->basepl   = ch->backpl;
 ch->plmod    = 100;
 STRFREE ( ch->name );
 ch->name     = STRALLOC( ch->back_name );
 ch->sex      = ch->back_sex;
 ch->race     = ch->back_race;

 // Say goodbye to that lovely fusion EQ.

 for ( obj_lose= ch->first_carrying; obj_lose; obj_lose=obj_next ) 
 {
      obj_next = obj_lose->next_content;
      if ( obj_lose->pIndexData->vnum >= 70 && obj_lose->pIndexData->vnum <= 72 )
         obj_from_char( obj_lose );
 }

 do_capsule( ch, "- all" );    

 ch->practice = 10;

 sn = 0; 
/* for ( ; ; )
 {
     if ( skill_table[sn] && skill_table[sn]->race_adept[RACE_FUSED] > 10 )
        ch->pcdata->learned[sn] = 0;
     sn++;
     if ( !skill_table[sn] || sn > 500 )
        break;
 }
 sn = 0; 
 for ( ; ; )
 {
     if ( skill_table[sn]->race_adept[ch->race] >= 10 )
        ch->pcdata->learned[sn] = skill_table[sn]->race_adept[ch->race];
     sn++;
     if ( !skill_table[sn] || sn > 500 )
        break;
 }*/
do_powerdown(ch, "hidden" );     
}



/*
         END OF FUSION CODE
 */


bool can_gain( CHAR_DATA *ch )
{
 OBJ_DATA *obj;

 if ( ch->fused )
	return FALSE;
 for ( obj = ch->first_carrying; obj; obj = obj->next_content )
 {
	if ( IS_OBJ_STAT( obj, ITEM_NOGAINS ) && is_wearing( ch, obj->pIndexData->vnum ))
		return FALSE;
 }
 if ( ch->style == STYLE_ADHD )
	return FALSE;
 if ( ch->basepl >= MAX_PL )
	ch->basepl = ( MAX_PL );
	 return TRUE;
}

int gainspeed( CHAR_DATA *ch )
{
if ( ch->race == RACE_HUMAN )
   return( 98 );
if ( ch->race == RACE_KAI )
   return( 80 );
if ( ch->race == RACE_MUTANT )
   return( 84 );
if ( ch->race == RACE_SAIYAN )
{
  if ( ch->pcdata->monkey )
   return( 45 );
  else
   return( 75 );
}
if ( ch->race == RACE_HALFBREED )
   return( 190 );
if ( ch->race == RACE_NAMEK )
   return( 84 );
if ( ch->race == RACE_BIOANDROID )
   return( 800 );
if ( ch->race == RACE_ANDROID )
   return( 78 );
if ( ch->race == RACE_ICER )
   return( 82 );
if ( ch->race == RACE_DEMON )
   return( 85 );
if ( ch->race == RACE_WIZARD )
   return( 64 );
if ( ch->race == RACE_TUFFLE )
   return( ( ch->eggslaid / 3 ) + 44 );

return( 100 );
}

void bio_gains( CHAR_DATA *ch, CHAR_DATA *victim, int dam)
{
 int where = number_range( 1, 7 );
 char buf[MAX_STRING_LENGTH];
 char pronoun[MAX_STRING_LENGTH];
 int multi = 1.5;
 multi += UMIN(get_timer(ch, TIMER_KILLSTREAK)/60, 6);

 if ( !can_gain(ch) )
	return;

 if ( IS_NPC(ch) )
	return;

 if ( victim->sex == 0 )
	sprintf( pronoun, "its" );
 if ( victim->sex == 1 )
	sprintf( pronoun, "his" );
 if ( victim->sex == 2 )
	sprintf( pronoun, "her" );

 switch( where )
 {
	{
	case 1:
	sprintf( buf, "&RYou stab %s in the chest, sucking out some of %s life force.\n\r", PERS( victim, ch ), pronoun );
        pl_gain( ch, victim, multi * 2.5 * ( 0.9087 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 2:
	sprintf( buf, "&RYou stab %s in the head, sucking out some of %s life force.\n\r", PERS( victim, ch ), pronoun );
        pl_gain( ch, victim,  multi * 2.5 * ( 1.6432 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 3:
	sprintf( buf, "&RYou stab %s in the leg, sucking out some of %s life force.\n\r", PERS( victim, ch ), pronoun);
        pl_gain( ch, victim,  multi * 2.5 * ( 0.4087 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 4:
	sprintf( buf, "&RYou stab %s in the arm, sucking out some of %s life force.\n\r", PERS( victim, ch ), pronoun);
        pl_gain( ch, victim,  multi * 2.5 * (  0.7288 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 5:
	sprintf( buf, "&RYou stab %s in the neck, sucking out some of %s life force.\n\r", PERS( victim, ch ), pronoun);
        pl_gain( ch, victim,  multi * 2.5 * ( 0.8615 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 6:
	sprintf( buf, "&RYou stab %s in the stomach, sucking out some of %s life force.\n\r",PERS( victim, ch ), pronoun);
        pl_gain( ch, victim, multi * 2.5 * ( 0.6989 * ( victim->max_hit * 700 ) ) );
	break;
	}
	{
	case 7:
	sprintf( buf, "&RYou stab at %s repeatedly in a frenzy, sucking out all of %s life force!!\n\r", PERS( victim, ch ), pronoun);
        pl_gain( ch, victim, multi * 2.5 * ( 1.8567 * ( victim->max_hit * 700 ) ) );
	break;
	}
 }
 pager_printf( ch, "%s", buf );

 if ( !IS_NPC(victim) )
	bio_stats( ch, victim, where );
 else if ( IS_NPC(victim) && number_range ( 1, 100 ) == 1 )
	bio_stats( victim, ch, where );
 return;
}

void bio_stats( CHAR_DATA *ch, CHAR_DATA *victim, int where )
{
 char buf[MAX_STRING_LENGTH];

 switch( where )
 {
	{
	case 1:
	sprintf( buf, "&rYour stab to %s's chest results in a constitution gain!", PERS( victim, ch ) );
	ch->perm_con += number_range( 4, 8);
	break;
	}
	{
	case 2:
	sprintf( buf, "&rYour stab to %s's head results in an intelligence gain!", PERS( victim, ch ) );
	ch->perm_int += number_range( 4, 8);
	break;
	}
	{
	case 3:
	sprintf( buf, "&rYour stab to %s's leg results in a dexterity gain!", PERS( victim, ch ) );
	ch->perm_dex += number_range( 4, 8);
	break;
	}
	{
	case 4:
	sprintf( buf, "&rYour stab to %s's arm results in a strength gain!", PERS( victim, ch ) );
	ch->perm_str += number_range( 4, 8);
	break;
	}
	{
	case 5:
	sprintf( buf, "&rYour stab to %s's neck results in a wisdom gain!", PERS( victim, ch ) );
	ch->perm_wis += number_range( 4, 8);
	break;
	}
	{
	case 6:
	sprintf( buf, "&rYour stab to %s's stomach results in a charisma gain!", PERS( victim, ch ) );
	ch->perm_cha += number_range( 4, 8);
	break;
	}
	{
	case 7:
	sprintf( buf, "&rYour repeated stabs to %s result in huge stat gains!!", PERS( victim, ch ) );
	ch->perm_lck += number_range( 4, 8);
	ch->perm_con += number_range( 3, 7);
	ch->perm_int += number_range( 2, 6);
	ch->perm_wis += number_range( 1, 5);
	break;
	}
 }
 pager_printf( ch, "%s", buf );
 return;
}


unsigned long long get_root( unsigned long long powerlevel )
  {
  unsigned long long number;
  unsigned long long sqrt_number;

  number = powerlevel;

  sqrt_number = My_sqrt(number);

  return(sqrt_number);
  }

unsigned long long My_sqrt( unsigned long long value )
  {
  unsigned long long update, solution;
  float convergence_tolerance = 0.0001;

  solution = value;

  do
    {
    update = 0.5*(value/solution - solution);
    solution += update;
    }
  while(My_abs(update/solution) > convergence_tolerance);

  return(solution);
  }

unsigned long long My_abs(unsigned long long number)
  {
  if(number < 0.0)
      number *= -1;
  return(number);
  }

void do_powerdown( CHAR_DATA *ch, char *argument )
{
 if ( IS_AFFECTED(ch, AFF_MAJIN ) )
 {
	send_to_char( "Why would you want to do that?\n\r",ch );
	return;
 }
    if ( IS_AFFECTED(ch, AFF_LSSJ) )
    {
        send_to_char( "YOU ARE A GOD!  SUCH THINGS ARE BEYOND YOU!\n\r", ch );
        return;
    }

 if ( ch->race == RACE_TUFFLE && ch->plmod > 200 )
 {
	send_to_char( "Retype your form name to powerdown.\n\r", ch );
	return;
 }
 if ( IS_AFFECTED(ch, AFF_OOZARU ) )
 {
	send_to_char( "Ook, chee, ack, eek!\n\r", ch );
	return;
 }
 if ( IS_AFFECTED( ch, AFF_NPOTWK ) )
 {
	send_to_char( "Arrrooooooooooo!\n\r", ch );
	return;
 }
 if ( ch->race == RACE_WIZARD )
 {
	send_to_char( "Huh?", ch );
	return;
 }
 if ( IS_AFFECTED( ch, AFF_SPLITFORM ) || IS_AFFECTED( ch, AFF_TRIFORM ) || IS_AFFECTED( ch, AFF_MULTIFORM ) )
	do_splitform( ch, "" );
 if ( str_cmp( argument, "hidden" ) )
 {
     act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
     send_to_char("&YYou get some self control and stabilise your powerlevel.\n\r", ch);
 }
 ch->multi_str = 0;
 ch->multi_dex = 0;
 ch->multi_int = 0;
 ch->multi_wis = 0;
 ch->multi_con = 0;
 ch->multi_cha = 0;
 ch->multi_lck = 0;
 ch->plmod = 100;
 update_current( ch, "" );
 xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
 xREMOVE_BIT( ch->affected_by, AFF_KID_BUU);
 xREMOVE_BIT( ch->affected_by, AFF_SUPER_BUU );
 xREMOVE_BIT( ch->affected_by, AFF_ULTIMATE_BUU );
 xREMOVE_BIT( ch->affected_by, AFF_SSJ );
 xREMOVE_BIT( ch->affected_by, AFF_GROW );
 xREMOVE_BIT( ch->affected_by, AFF_USSJ );
 xREMOVE_BIT( ch->affected_by, AFF_SSJ2 );
 xREMOVE_BIT( ch->affected_by, AFF_SSJ3 );
 xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
 xREMOVE_BIT( ch->affected_by, AFF_SSJ5 );
 xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
 xREMOVE_BIT( ch->affected_by, AFF_MYSTIC );
 xREMOVE_BIT( ch->affected_by, AFF_SEMI_PERFECT );
 xREMOVE_BIT( ch->affected_by, AFF_PERFECT );
 xREMOVE_BIT( ch->affected_by, AFF_FINAL_PERFECT );
 xREMOVE_BIT( ch->affected_by, AFF_SUPER_ANDROID );
 xREMOVE_BIT( ch->affected_by, AFF_SUPER_NAMEK );
 xREMOVE_BIT( ch->affected_by, AFF_HYPER );
 xREMOVE_BIT( ch->affected_by, AFF_FIRST_FORM );
 xREMOVE_BIT( ch->affected_by, AFF_SECOND_FORM );
 xREMOVE_BIT( ch->affected_by, AFF_THIRD_FORM );
 xREMOVE_BIT( ch->affected_by, AFF_FOURTH_FORM );
 xREMOVE_BIT( ch->affected_by, AFF_FIFTH_FORM );
 xREMOVE_BIT( ch->affected_by, AFF_MUTIE );
 xREMOVE_BIT( ch->affected_by, AFF_META );
 xREMOVE_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
 xREMOVE_BIT( ch->affected_by, AFF_COMBINED_STRENGTH );
 xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM ); 
 xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS );
 xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS );
 xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS );
 xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
 xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
 xREMOVE_BIT( ch->affected_by, AFF_SUPER );
 xREMOVE_BIT( ch->affected_by, AFF_SYNCED );
 de_equip_char( ch );
 re_equip_char(ch);
 update_current( ch, NULL );
 return;
}

void do_powerup( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];
 char buf2[MAX_STRING_LENGTH];


    if ( IS_AFFECTED(ch, AFF_LSSJ) )
    {
        send_to_char( "YOU ARE A GOD!  SUCH THINGS ARE BEYOND YOU!\n\r", ch );
        return;
    }
 if ( ch->race == RACE_WIZARD )
 {
	send_to_char( "Huh?\n\r", ch );
	return;
 }
 if ( ch->race != RACE_DEMON )
 {
	if ( ch->plmod > 200 )
 	{
        	send_to_char("&WYou need to find a more powerful method of powering up.\n\r", ch);
		return;
 	}
 }
 else
 {
	if ( IS_AFFECTED( ch, AFF_ULTIMATE_BUU ) && ch->plmod >= 1300 )
	{
                send_to_char("&WYou need to find a more powerful method of powering up.\n\r", ch);
                return;
	}	
	else if ( IS_AFFECTED( ch, AFF_SUPER_BUU ) && ch->plmod >= 2000 )
        {
                send_to_char("&WYou need to find a more powerful method of powering up.\n\r", ch);
                return;
        }
	else if ( IS_AFFECTED( ch, AFF_KID_BUU ) && ch->plmod >= 2900 )
        {
                send_to_char("&WYou need to find a more powerful method of powering up.\n\r", ch);
                return;
        }
	else if ( ch->plmod >= 500 && !IS_AFFECTED( ch, AFF_KID_BUU) && !IS_AFFECTED(ch, AFF_SUPER_BUU) && !IS_AFFECTED(ch,AFF_ULTIMATE_BUU))
        {
                send_to_char("&WYou need to find a more powerful method of powering up.\n\r", ch);
                return;
        }
 }
 if ( IS_AFFECTED(ch, AFF_OOZARU ) )
 {
	send_to_char( "Ook, ook, ack, eek!\n\r", ch );
	return;
 }
 if ( ch->position <= POS_SLEEPING )
	{
	send_to_char( "Wake up first!\n\r", ch );
	return;
	}
 if ( !str_cmp( argument, "max" ) )
 {
    sprintf( buf, "&CWith a final burst of energy, you reach your full power, and dust begins to settle around you.\n\r" );
    sprintf( buf2, "&CWith one final burst of energy, %s reaches full power, and dust begins to settle all around.\n\r", ch->name );
    if ( ch->race == RACE_DEMON )
    {
	if ( IS_AFFECTED( ch, AFF_ULTIMATE_BUU ) )
		ch->plmod = 1300;
	else if ( IS_AFFECTED( ch, AFF_SUPER_BUU ) )
                ch->plmod = 2000;
        else if ( IS_AFFECTED( ch, AFF_KID_BUU ) )
                ch->plmod = 2900;
	else
		ch->plmod = 500;
    }
    else
    {
	ch->plmod = 200;
    }
    act( AT_GREEN, buf2, ch, NULL, NULL, TO_CANSEE );
    pager_printf( ch, "%s", buf );
    update_current( ch, "" );
    return;
 }

 if ( ch->plmod < 100 )
 {
    sprintf( buf, "&CYou begin to glow %s &Cfaintly as you power up.\n\r", ch->energycolour );
    sprintf( buf2, "&C%s begins to glow %s &Cfaintly as %s powers up.\n\r", ch->name, ch->energycolour, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she" );
    ch->plmod = 100;
 }
 else if ( ch->plmod <= 120 && ch->plmod >= 100 )
 {
    sprintf( buf, "&CYou begin to glow %s &Cslightly more as you power up.\n\r", ch->energycolour );
    sprintf( buf2, "&C%s begins to glow %s &Cslightly more as %s powers up.\n\r", ch->name, ch->energycolour, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she" );
 }
 else if ( ch->plmod <= 140 && ch->plmod > 120 )
 {
    sprintf( buf, "&CYour %s &Cglow becomes more vibrant as the ground crumbles beneath you.\n\r", ch->energycolour );
    sprintf( buf2, "&C%s's %s &Cglow becomes more vibrant, and the ground beneath %s crumbles.\n\r", ch->name, ch->energycolour, ch->sex == 0 ? "it" : ch->sex == 1 ? "him" : "her" );
 }
 else if ( ch->plmod <= 160 && ch->plmod > 140 )
 {
    sprintf( buf, "&C%s energy flies upwards from you, and large pieces of the ground do the same!\n\r", ch->energycolour );
    sprintf( buf2, "&C%s's %s energy flies upwards, along with large pieces of the ground!\n\r", ch->name, ch->energycolour );
 }
 else if ( ch->plmod <= 180 && ch->plmod > 160 )
 {
    sprintf( buf, "&CWith a mighty scream, %s &Cenergy practcally hides everything from view, as you destroy the ground all around you!\n\r", ch->energycolour );
    sprintf( buf2, "&CWith a mighty scream, %s &Cenergy covers %s completely, and %s destroys the ground all around %s!\n\r", ch->energycolour, ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0 ? "it" : ch->sex == 1 ? "him" : "her" );
 }
 else if ( ( ch->plmod <= 198 && ch->plmod > 180 ) )
 {
    sprintf( buf, "&CAs your screams reach a deafening volume, your muscles begin to bulk up, all the while a massive %s &Caura growing around you!\n\r", ch->energycolour );
    sprintf( buf2, "&CAs %s's screams reach a deafening volume, %s muscles begin to bulk up while the %s &Caura around %s grows further still!\n\r", ch->name, ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour, ch->sex == 0 ? "it" : ch->sex == 1 ? "him" : "her" );
 }
 else if ( ch->plmod == 199 )
 {
    sprintf( buf, "&CWith a final burst of energy, you reach your full power, and dust begins to settle around you.\n\r" );
    sprintf( buf2, "&CWith one final burst of energy, %s reaches full power, and dust begins to settle all around.\n\r", ch->name );
 }
 else if ( ch->plmod > 200 )
 {
    sprintf( buf, "&RUtilising your mystical powers and steam, you continue to power up...\n\r" );
    sprintf( buf, "&CUtilising %s mystical powers and steam, %s continues to power up...\n\r", HISHER( ch->sex ), ch->name );
 }

 if ( ch->race == RACE_DEMON )
 {
     unsigned long long max = 500;
     if ( IS_AFFECTED( ch, AFF_ULTIMATE_BUU ) )
	  max = 1300;
     if ( IS_AFFECTED( ch, AFF_SUPER_BUU ) )
	  max = 2000;
     if ( IS_AFFECTED( ch, AFF_KID_BUU ) )
	  max = 2900;
     if ( ch->plmod < max )
     {
        act( AT_GREEN, buf2, ch, NULL, NULL, TO_CANSEE );
        pager_printf(ch, "%s", buf );
        ch->plmod = ch->plmod + number_range( 10, 20 );
        if ( ch->plmod > max )
            ch->plmod = max;
        update_current( ch, "" );
        return;
     }
 }
 else
 {
     if ( ch->plmod < 200 && ch->plmod >= 100 )
     {
   	act( AT_GREEN, buf2, ch, NULL, NULL, TO_CANSEE );
	pager_printf(ch, "%s", buf );
	ch->plmod = ch->plmod + number_range( 4, 16 );
	if ( ch->plmod > 200 )
	    ch->plmod = 200;
 	update_current( ch, "" );
	return;
    }
    else if ( ch->plmod < 100 )
    {
        send_to_char("&YYou power up to your base.\n\r", ch);
        ch->plmod = 100;
        update_current( ch, "" );
	return;
    }
 }

 send_to_char("&RYou need to find a more powerful method of powering up.\n\r", ch);
 return;
}

/*
 * Check to see if player's attacks are (still?) suppressed
 * #ifdef TRI
 */
bool is_attack_supressed( CHAR_DATA *ch )
{
  TIMER *timer;

  if (IS_NPC(ch))
    return FALSE;

  timer = get_timerptr( ch, TIMER_ASUPRESSED );

  if ( !timer )
    return FALSE;

  /* perma-supression -- bard? (can be reset at end of fight, or spell, etc) */
  if ( timer->value == -1 )
    return TRUE;

  /* this is for timed supressions */
  if ( timer->count >= 1 )
    return TRUE;

  return FALSE;
}
  
/*
 * Check to see if weapon is poisoned.
 */
bool is_wielding_poisoned( CHAR_DATA *ch )
{
    OBJ_DATA *obj;

    if ( !used_weapon )
    	return FALSE;

    if ( (obj=get_eq_char(ch, WEAR_WIELD)) != NULL
    &&    used_weapon == obj
    &&    IS_OBJ_STAT(obj, ITEM_POISONED) )
	return TRUE;
    if ( (obj=get_eq_char(ch, WEAR_DUAL_WIELD)) != NULL
    &&    used_weapon == obj
    &&	  IS_OBJ_STAT(obj, ITEM_POISONED) )
    	return TRUE;

    return FALSE;
}

/*
 * hunting, hating and fearing code				-Thoric
 */
bool is_hunting( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( !ch->hunting || ch->hunting->who != victim )
      return FALSE;
    
    return TRUE;    
}

bool is_hating( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( !ch->hating || ch->hating->who != victim )
      return FALSE;
    
    return TRUE;    
}

bool is_fearing( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( !ch->fearing || ch->fearing->who != victim )
      return FALSE;
    
    return TRUE;    
}

void stop_hunting( CHAR_DATA *ch )
{
    if ( ch->hunting )
    {
	STRFREE( ch->hunting->name );
	DISPOSE( ch->hunting );
	ch->hunting = NULL;
    }
    return;
}

void stop_hating( CHAR_DATA *ch )
{
    if ( ch->hating )
    {
	STRFREE( ch->hating->name );
	DISPOSE( ch->hating );
	ch->hating = NULL;
    }
    return;
}

void pl_gain	args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dam ) )
{
 PLANET_DATA *planet;
 unsigned long long inc = 0;
 DESCRIPTOR_DATA *d;

 if ( !ch || !victim || ch == victim )
	return;

 if ( IS_NPC(victim) && victim->pIndexData->vnum >= 30 && victim->pIndexData->vnum <= 40 )
        return;

 if ( !can_gain(ch) )
	return;

 if ( (IS_NPC(victim) && !xIS_SET( victim->act, ACT_MIMIC ) ))
 {
   if ( ch->currpl >= victim->currpl * 10 || victim->currpl == 0)
   {
        if ( ch->race == RACE_BIOANDROID )
        {
            inc = ( victim->perm_str + victim->perm_int + victim->perm_dex + 
                    victim->perm_lck + victim->perm_wis + victim->perm_cha +
                  ( victim->perm_con * 4 ) );

            if ( !IS_NPC(ch) && !IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) )
                pager_printf_color(ch, "&Y&W&w&BYour powerlevel increases by %s.&w\n\r", format_pl(inc) );
            if ( !IS_NPC(ch) )
                ch->pcdata->pl_gain_round += inc;
            ch->basepl += inc;
            if ( !IS_NPC(ch ) )
                ch->pcdata->pl_today += inc;
            newskills( ch, inc );
	    update_current(ch, "");
            return;
        }
	if ( !IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) )
	    send_to_char( "You are far too strong to gain from this fight...\n\r", ch );
	return;
   }
   if ( ch->currpl <= victim->currpl / 10 || ch->mana < 10 || ch->currpl == 0 )
   {
	if ( !IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) )
	    send_to_char( "You are too weak to learn from this engagement...\n\r", ch );
	return;
   }
 }

 if ( ch != victim )
 {
     if ( victim->currpl == 0  || ch->currpl == 0 )
         return;
     inc = (1.85* ( ch->currpl / ( 5 * pow(ch->currpl,0.4) ) ) * ( log(ch->currpl)/25) )* pow(dam, 0.3333);
     dam *= ( 1 + ( victim->currpl / ch->currpl ) );
     if ( IS_NPC(victim) )
         inc *= 0.9;
     else
	 inc *= 1.25;
     if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ) && ch->in_room->vnum != 5429 )
         inc = inc * 0.25;
     if ( ch->in_room->vnum == 5429 )
         inc *= 5;
     if ( ch->basepl < 100000 )
         inc *= 2;		 
     if ( ch->basepl > 500000 )
         inc *= 0.9;
     if ( ch->basepl > 500000000 )
         inc *= 0.85;
     if ( ch->basepl > 500000000000ULL )
         inc *= 0.85;

     inc *= 1.3;

     planet = get_planet( ch->in_room->area->planet );
     if ( planet && planet->gravity > 0 && planet->gravity < 100 )
         inc *= sqrt(( 2 * planet->gravity ));
     if ( inc > ch->currpl * 0.002 && ch->race != RACE_BIOANDROID )
         inc = ch->currpl * 0.002;
     inc = ( ( inc / 275 ) * (gainspeed(ch) ) );
     inc *= .0775;
     if ( ch->race != RACE_BIOANDROID )
     {
         inc += sqrt(sqrt(ch->currpl));
     }
     else
     {
         inc = inc/10;
	 if ( inc <= 1 )
	     inc = number_range( 1, 3 );
     }
     if ( ch->currpl < victim->currpl )
         inc *= 1.2;
     else
         inc *= 0.7;

     inc *= 0.4;
     if ( ch->currpl > victim->currpl/10 && ch->currpl < victim->currpl*10 )
         inc += number_range( 1, sqrt(dam/10));

     for(d = first_descriptor; d; d = d->next)
     {
         if ( d->character && d->character->basepl > 100000000 && !IS_IMMORTAL(d->character))
	 {
	     inc += (inc * 0.03);
         }     
     }
     inc += number_range( 4, 18 );

     if ( dam == 0 )
     {
         inc = 0;
     }
/*
    if ( xIS_SET(ch->act, PLR_KILLER) )
    {
	inc *= 0.25;
    }
*/
     if ( !IS_NPC(ch) && !IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) )
         pager_printf_color(ch, "&Y&W&w&BYour powerlevel increases by %s.&w\n\r", format_pl(inc) );
     if ( !IS_NPC(ch) )
         ch->pcdata->pl_gain_round += inc;
     ch->basepl += inc;
     if ( !IS_NPC(ch ) )
         ch->pcdata->pl_today += inc;
     newskills( ch, inc );
 }
 update_current( ch, "" );
 return;
}

void stop_fearing( CHAR_DATA *ch )
{
    if ( ch->fearing )
    {
	STRFREE( ch->fearing->name );
	DISPOSE( ch->fearing );
	ch->fearing = NULL;
    }
    return;
}

void start_hunting( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( ch->hunting )
      stop_hunting( ch );

    if ( ch->currpl < ch->basepl )
	return;
	
    CREATE( ch->hunting, HHF_DATA, 1 );
    ch->hunting->name = QUICKLINK( PERS( victim, ch ) );
    ch->hunting->who  = victim;
    return;
}

void start_hating( CHAR_DATA *ch, CHAR_DATA *victim )
{
return;
    if ( ch->hating )
      stop_hating( ch );

    CREATE( ch->hating, HHF_DATA, 1 );
//    ch->hating->name = QUICKLINK( PERS( victim, ch ) );
//    ch->hating->who  = victim;
    return;
}

void start_fearing( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if ( ch->fearing )
      stop_fearing( ch );

    CREATE( ch->fearing, HHF_DATA, 1 );
    ch->fearing->name = QUICKLINK( PERS( victim, ch ) );
    ch->fearing->who  = victim;
    return;
}
/*temp im to lazy to go and delete all vamp stuff KESH */
sh_int VAMP_AC( CHAR_DATA * ch )
{
    return 0;
}

int max_fight( CHAR_DATA *ch )
{
    return 8;
}


/*
 * Control the fights going on.
 * Called periodically by update_handler.
 * Many hours spent fixing bugs in here by Thoric, as noted by residual
 * debugging checks.  If you never get any of these error messages again
 * in your logs... then you can comment out some of the checks without
 * worry.
 *
 * Note:  This function also handles some non-violence updates.
 */
void violence_update( void )
{
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *ch;
    CHAR_DATA *lst_ch;
    CHAR_DATA *victim;
    CHAR_DATA *rch, *rch_next;
    AFFECT_DATA *paf, *paf_next;
    TIMER	*timer, *timer_next;
    ch_ret	retcode;
    int		attacktype, cnt;
    SKILLTYPE	*skill;
    static int	pulse = 0;
    static int  speedpulse = 0;

    lst_ch = NULL;
    pulse = (pulse+1) % 100;
    speedpulse = (pulse+1) % 30;

    for ( ch = last_char; ch; lst_ch = ch, ch = gch_prev )
    {
	set_cur_char( ch );

	if ( ch == first_char && ch->prev )
	{
	   bug( "ERROR: first_char->prev != NULL, fixing...", 0 );
	   ch->prev = NULL;
	}

	gch_prev	= ch->prev;

	if ( gch_prev && gch_prev->next != ch )
	{
	    sprintf( buf, "FATAL: violence_update: %s->prev->next doesn't point to ch.",
		ch->name );
	    bug( buf, 0 );	    
	    bug( "Short-cutting here", 0 );
	    ch->prev = NULL;
	    gch_prev = NULL;
	    do_shout( ch, "Thoric says, 'Prepare for the worst!'" );
	}

	/*
	 * See if we got a pointer to someone who recently died...
	 * if so, either the pointer is bad... or it's a player who
	 * "died", and is back at the healer...
	 * Since he/she's in the char_list, it's likely to be the later...
	 * and should not already be in another fight already
	 */
	if ( char_died(ch) )
	    continue;

    if ( IS_AFFECTED( ch, AFF_CHARGING ) )
    {
	  int minrange;
	  int maxrange;
	  int restoreamt;
	  char buf[MAX_STRING_LENGTH];

	  minrange = (ch->max_mana * 0.01 );
	  maxrange = (ch->max_mana * 0.10 );

	  restoreamt = number_range( minrange, maxrange );

	  if ( restoreamt < 1 )
	      restoreamt = 1;

	  sprintf( buf, "&BYour %s &Baura flares up as you charge up your power.\n\r", ch->energycolour );
	  pager_printf( ch, "%s", buf );

	  act( AT_BLUE, "&B$n's aura &Bflares around $m, as $e charges up power.", ch, NULL, NULL, TO_NOTVICT );
	  WAIT_STATE( ch, skill_table[gsn_charge]->beats );

	  ch->mana += restoreamt;
	  if ( ch->mana > ch->max_mana )
	      ch->mana = ch->max_mana;
    }
    else if ( ch->renzoku && (victim = who_fighting(ch) )!= NULL && !IS_NPC(ch) && !IS_AFFECTED( ch, AFF_AFTER_IMAGE))
    {
           if ( ch->mana > 1000 )
           {
                ch->mana -= 500;
                if ( can_use_skill(ch, number_percent(), gsn_renzoku ) )
                {
                    learn_from_success( ch, gsn_renzoku );
                    pager_printf( ch, "&YYou fire a %s&y energyball at %s!\n\r", ch->energycolour, victim->name );
                    act ( AT_PINK, "$n hits $N with an energyball!", ch, NULL, victim,TO_NOTVICT );
                    pager_printf( victim, "&Y%s fires a %s&Y energyball at you from above!\n\r", ch->name, ch->energycolour );
                    damage( ch, victim, number_range( 90, 180 ), gsn_renzoku );
                }
                else
                {
                    pager_printf( ch, "You fire your energyball but it just slams into the ground." );
                    act ( AT_PINK, "$n's energyball misses and slams into the ground.", ch, NULL, NULL, TO_CANSEE );
                }

                ch->mana -= 500;
                if ( can_use_skill(ch, number_percent(), gsn_renzoku ) )
                {
                    learn_from_success( ch, gsn_renzoku );
                    pager_printf( ch, "&YYou fire a %s&y energyball at %s!\n\r", ch->energycolour, victim->name );
                    pager_printf( victim, "&Y%s fires a %s&Y energyball at you from above!\n\r", ch->name, ch->energycolour );
                    act ( AT_PINK, "$n hits $N with an energyball!\n\r", ch, NULL, victim,TO_NOTVICT );
                    damage( ch, victim, number_range( 40, 80 ), gsn_renzoku );
                }
                else
                {
                    pager_printf( ch, "You fire your energyball but it just slams into the ground." );
                    act ( AT_PINK, "$n's energyball misses and slams into the ground.\n\r", ch, NULL, NULL, TO_CANSEE );
                }
           }
           else
           {
               interpret(ch, "blink" );
               pager_printf( ch, "Not enough energy.\n\r" );
               return;
           }
     }

	/*
	 * See if we got a pointer to some bad looking data...
	 */
	if ( !ch->in_room || !ch->name )
	{
	    log_string( "violence_update: bad ch record!  (Shortcutting.)" );
	    sprintf( buf, "ch: %d  ch->in_room: %d  ch->prev: %d  ch->next: %d",
	    	(int) ch, (int) ch->in_room, (int) ch->prev, (int) ch->next );
	    log_string( buf );
	    log_string( lastplayercmd );
	    if ( lst_ch )
	      sprintf( buf, "lst_ch: %d  lst_ch->prev: %d  lst_ch->next: %d",
	      		(int) lst_ch, (int) lst_ch->prev, (int) lst_ch->next );
	    else
	      strcpy( buf, "lst_ch: NULL" );
	    log_string( buf );
	    gch_prev = NULL;
	    continue;
	}


	for ( timer = ch->first_timer; timer; timer = timer_next )
	{
	    if ( timer == NULL )
		break;
	    timer_next = timer->next;
	    if ( --timer->count <= 0 )
	    {
		if ( timer->type == TIMER_ASUPRESSED )
		{
		    if ( timer->value == -1 )
		    {
			timer->count = 1000;
			continue;
		    }
		}
		if ( timer->type == TIMER_NUISANCE )
		{
		    DISPOSE( ch->pcdata->nuisance );
		}

		if ( timer->type == TIMER_DO_FUN )
		{
		    int tempsub;

		    tempsub = ch->substate;
		    ch->substate = timer->value;
		    (timer->do_fun)( ch, "" );
		    if ( char_died(ch) )
			break;
		    ch->substate = tempsub;
		}
		extract_timer( ch, timer );
	    }
	}

	if ( char_died(ch) )
	    continue;

	/*
	 * We need spells that have shorter durations than an hour.
	 * So a melee round sounds good to me... -Thoric
	 */
	for ( paf = ch->first_affect; paf; paf = paf_next )
	{
	    paf_next	= paf->next;
	    if ( paf->duration > 0 )
		paf->duration--;
	    else
	    if ( paf->duration < 0 )
		;
	    else
	    {
		if ( !paf_next
		||    paf_next->type != paf->type
		||    paf_next->duration > 0 )
		{
		    skill = get_skilltype(paf->type);
		    if ( paf->type > 0 && skill && skill->msg_off )
		    {
			set_char_color( AT_WEAROFF, ch );
			send_to_char( skill->msg_off, ch );
			send_to_char( "\n\r", ch );
		    }
		}
		affect_remove( ch, paf );
	    }
	}
	
	if ( char_died(ch) )
	    continue;

	/* check for exits moving players around */
	if ( (retcode=pullcheck(ch, pulse)) == rCHAR_DIED || char_died(ch) )
	    continue;

	/* Let the battle begin! */

	if ( ( victim = who_fighting( ch ) ) == NULL
	||   IS_AFFECTED( ch, AFF_PARALYSIS ) )
	    continue;

        retcode = rNONE;

	if ( IS_AWAKE(ch) && ch->in_room == victim->in_room )
	    retcode = multi_hit( ch, victim, TYPE_UNDEFINED );
	else
	    stop_fighting( ch, FALSE );

	if ( char_died(ch) )
	    continue;

	if ( retcode == rCHAR_DIED
	|| ( victim = who_fighting( ch ) ) == NULL )
	    continue;

	/*
	 *  Mob triggers
	 *  -- Added some victim death checks, because it IS possible.. -- Alty
	 */
	rprog_rfight_trigger( ch );
	if ( char_died(ch) || char_died(victim) )
	    continue;
	mprog_hitprcnt_trigger( ch, victim );
	if ( char_died(ch) || char_died(victim) )
	    continue;
	mprog_fight_trigger( ch, victim );
	if ( char_died(ch) || char_died(victim) )
	    continue;

	/*
	 * NPC special attack flags				-Thoric
	 */
	if ( IS_NPC(ch) ) 
	{
	    if ( !xIS_EMPTY(ch->attacks) )
	    {
		attacktype = -1;
		if ( 30 + (ch->level/4) >= number_percent( ) )
		{
		    cnt = 0;
		    for ( ;; )
		    {
			if ( cnt++ > 10 )
			{
			    attacktype = -1;
			    break;
			}
			attacktype = number_range( 7, MAX_ATTACK_TYPE-1 );
			if ( xIS_SET( ch->attacks, attacktype ) )
			    break;
		    }
		    switch( attacktype )
		    {
			case ATCK_BASH:
			    do_bash( ch, "" );
			    retcode = global_retcode;
			    break;
			case ATCK_STUN:
			    do_stun( ch, "" ); 
			    retcode = global_retcode;
			    break;
			case ATCK_GOUGE:
			    do_gouge( ch, "" );
			    retcode = global_retcode;
			    break;
			case ATCK_FEED:
			    do_gouge( ch, "" );
			    retcode = global_retcode;
			    break;
			case ATCK_DRAIN:
			    retcode = spell_energy_drain( skill_lookup( "energy drain" ), ch->level, ch, victim );
			    break;
			case ATCK_FIREBREATH:
			    retcode = spell_fire_breath( skill_lookup( "fire breath" ), ch->level, ch, victim );
			    break;
			case ATCK_FROSTBREATH:
			    retcode = spell_frost_breath( skill_lookup( "frost breath" ), ch->level, ch, victim );
			    break;
			case ATCK_ACIDBREATH:
			    retcode = spell_acid_breath( skill_lookup( "acid breath" ), ch->level, ch, victim );
			    break;
			case ATCK_LIGHTNBREATH:
			    retcode = spell_lightning_breath( skill_lookup( "lightning breath" ), ch->level, ch, victim );
			    break;
			case ATCK_GASBREATH:
			    retcode = spell_gas_breath( skill_lookup( "gas breath" ), ch->level, ch, victim );
			    break;
			case ATCK_SPIRALBLAST:         
			    retcode = spell_spiral_blast( skill_lookup( "spiral blast" ),
				ch->level, ch, victim );
			    break;
			case ATCK_NASTYPOISON:
			    /*
			    retcode = spell_nasty_poison( skill_lookup( "nasty poison" ), ch->level, ch, victim );
			    */
			    break;
			case ATCK_GAZE:
			    /*
			    retcode = spell_gaze( skill_lookup( "gaze" ), ch->level, ch, victim );
			    */
			    break;
			case ATCK_CAUSESERIOUS:
			    retcode = spell_cause_serious( skill_lookup( "cause serious" ), ch->level, ch, victim );
			    break;
			case ATCK_EARTHQUAKE:
			    retcode = spell_earthquake( skill_lookup( "earthquake" ), ch->level, ch, victim );
			    break;
			case ATCK_CAUSECRITICAL:
			    retcode = spell_cause_critical( skill_lookup( "cause critical" ), ch->level, ch, victim );
			    break;
			case ATCK_CURSE:
			    retcode = spell_curse( skill_lookup( "curse" ), ch->level, ch, victim );
			    break;
			case ATCK_FLAMESTRIKE:
			    retcode = spell_flamestrike( skill_lookup( "flamestrike" ), ch->level, ch, victim );
			    break;
			case ATCK_HARM:
			    retcode = spell_harm( skill_lookup( "harm" ), ch->level, ch, victim );
			    break;
			case ATCK_FIREBALL:
			    retcode = spell_fireball( skill_lookup( "fireball" ), ch->level, ch, victim );
			    break;
			case ATCK_COLORSPRAY:
			    retcode = spell_colour_spray( skill_lookup( "colour spray" ), ch->level, ch, victim );
			    break;
			case ATCK_WEAKEN:
			    retcode = spell_weaken( skill_lookup( "weaken" ), ch->level, ch, victim );
			    break;
		    }
		    if ( attacktype != -1 && (retcode == rCHAR_DIED || char_died(ch)) )
			continue;
		}
	    }
	    /*
	     * NPC special defense flags				-Thoric
	     */
	    if ( !xIS_EMPTY(ch->defenses) )
	    {
		attacktype = -1;
		if ( 50 + (ch->level/4) > number_percent( ) )
		{
		    cnt = 0;
		    for ( ;; )
		    {
			if ( cnt++ > 10 )
			{
			    attacktype = -1;
			    break;
			}
			attacktype = number_range( 2, MAX_DEFENSE_TYPE-1 );
			if ( xIS_SET( ch->defenses, attacktype ) )
			    break;
		    }

		    switch( attacktype )
		    {
			case DFND_CURELIGHT:
			    /* A few quick checks in the cure ones so that a) less spam and
			       b) we don't have mobs looking stupider than normal by healing
			       themselves when they aren't even being hit (although that
			       doesn't happen TOO often */
			    if ( ch->hit < ch->max_hit )
			    {
				act( AT_MAGIC, "$n mutters a few incantations...and looks a little better.", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "cure light" ), ch->level, ch, ch );
			    }
			    break;
			case DFND_CURESERIOUS:
			    if ( ch->hit < ch->max_hit )
			    {
				act( AT_MAGIC, "$n mutters a few incantations...and looks a bit better.", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "cure serious" ), ch->level, ch, ch );
			    }
			    break;
			case DFND_CURECRITICAL:
			    if ( ch->hit < ch->max_hit )
			    {
				act( AT_MAGIC, "$n mutters a few incantations...and looks healthier.", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "cure critical" ), ch->level, ch, ch );
			    }
			    break;
			case DFND_HEAL:
			    if ( ch->hit < ch->max_hit )
			    {
				act( AT_MAGIC, "$n mutters a few incantations...and looks much healthier.", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "heal" ), ch->level, ch, ch );
			    }
			    break;
			case DFND_DISPELMAGIC:
			    if ( ch->first_affect )
			    {
				act( AT_MAGIC, "$n utters an incantation...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_dispel_magic( skill_lookup( "dispel magic" ), ch->level, ch, victim );
			    }
			    break;
			case DFND_DISPELEVIL:
			    act( AT_MAGIC, "$n utters an incantation...", ch, NULL, NULL, TO_ROOM );
			    retcode = spell_dispel_evil( skill_lookup( "dispel evil" ), ch->level, ch, victim );
			    break;
			case DFND_TELEPORT:
			    retcode = spell_teleport( skill_lookup( "teleport"), ch->level, ch, ch );
			    break;
			case DFND_SHOCKSHIELD:
			    if ( !IS_AFFECTED( ch, AFF_SHOCKSHIELD ) )
			    {
				act( AT_MAGIC, "$n utters a few incantations...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "shockshield" ), ch->level, ch, ch );
			    }
			    else
				retcode = rNONE;
			    break;
                       case DFND_VENOMSHIELD:
/*                            if ( !IS_AFFECTED( ch, AFF_VENOMSHIELD ) )
                            {
                                act( AT_MAGIC, "$n utters a few incantations ...", ch, NULL, NULL, TO_ROOM );
                                retcode = spell_smaug( skill_lookup( "venomshield" ), ch->level, ch, ch );
                            }
                            else*/
                                retcode = rNONE;
                            break;
			case DFND_ACIDMIST:
			    if ( !IS_AFFECTED( ch, AFF_ACIDMIST ) )
			    {
				act( AT_MAGIC, "$n utters a few incantations ...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "acidmist" ), ch->level, ch, ch );
			    }
			    else
				retcode = rNONE;
			    break;
			case DFND_FIRESHIELD:
			    if ( !IS_AFFECTED( ch, AFF_FIRESHIELD ) )
			    {
				act( AT_MAGIC, "$n utters a few incantations...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "fireshield" ), ch->level, ch, ch );
			    }
			    else
				retcode = rNONE;
			    break;
			case DFND_ICESHIELD:
			    if ( !IS_AFFECTED( ch, AFF_ICESHIELD ) )
			    {
				act( AT_MAGIC, "$n utters a few incantations...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "iceshield" ), ch->level, ch, ch );
			    }
			    else
				retcode = rNONE;
			    break;
			case DFND_TRUESIGHT:
			    if ( !IS_AFFECTED( ch, AFF_TRUESIGHT ) )
				retcode = spell_smaug( skill_lookup( "true" ), ch->level, ch, ch );
			    else
				retcode = rNONE;
			    break;
			case DFND_SANCTUARY:
			    if ( !IS_AFFECTED( ch, AFF_SANCTUARY ) )
			    {
				act( AT_MAGIC, "$n utters a few incantations...", ch, NULL, NULL, TO_ROOM );
				retcode = spell_smaug( skill_lookup( "sanctuary" ), ch->level, ch, ch );
			    }
			    else
				retcode = rNONE;
			    break;
		    }
		    if ( attacktype != -1 && (retcode == rCHAR_DIED || char_died(ch)) )
			continue;
		}
	    }
	}

	/*
	 * Fun for the whole family!
	 */
	for ( rch = ch->in_room->first_person; rch; rch = rch_next )
	{
	    rch_next = rch->next_in_room;

            /*
             *   Group Fighting Styles Support:
             *   If ch is tanking
             *   If rch is using a more aggressive style than ch
             *   Then rch is the new tank   -h
             */
            /* &&( is_same_group(ch, rch)      ) */

            if( ( !IS_NPC(ch) && !IS_NPC(rch) )
              &&( rch!=ch                     )
              &&( rch->fighting               )
              &&( who_fighting(rch->fighting->who) == ch    )
              &&( !xIS_SET( rch->fighting->who->act, ACT_AUTONOMOUS ) )
              &&( rch->style < ch->style      )
              )
            {
                 rch->fighting->who->fighting->who = rch; 

            }

	    if ( IS_AWAKE(rch) && !rch->fighting )
	    {
		/*
		 * PC's auto-assist others in their group.
		 */
		if ( !IS_NPC(ch) || IS_AFFECTED(ch, AFF_CHARM) )
		{
		    if ( ( ( !IS_NPC(rch) && rch->desc )
		    ||        IS_AFFECTED(rch, AFF_CHARM) )
		    && is_same_group(ch, rch) )
			multi_hit( rch, victim, TYPE_UNDEFINED );
		    continue;
		}

		/*
		 * NPC's assist NPC's of same type or 12.5% chance regardless.
		 */
		if ( IS_NPC(rch) && !IS_AFFECTED(rch, AFF_CHARM)
		&&  !xIS_SET(rch->act, ACT_NOASSIST) )
		{
		    if ( char_died(ch) )
			break;
		    if ( rch->pIndexData == ch->pIndexData
		    ||   number_bits( 3 ) == 0 )
		    {
			CHAR_DATA *vch;
			CHAR_DATA *target;
			int number;

			target = NULL;
			number = 0;			
                        for ( vch = ch->in_room->first_person; vch; vch = vch->next )
			{
			    if ( can_see( rch, vch )
			    &&   is_same_group( vch, victim )
			    &&   number_range( 0, number ) == 0 )
			    {
//				if ( vch->mount && vch->mount == rch )
//				  target = NULL;
//				else
				{
				  target = vch;
				  number++;
				}
			    }
			}

			if ( target )
			   multi_hit( rch, target, TYPE_UNDEFINED );
		    }
		}
	    }
	}
    }

    return;
}

/*
 * Do one group of attacks.
 */
ch_ret multi_hit( CHAR_DATA *ch, CHAR_DATA *victim, int dt )
{
    int     chance;
    ch_ret  retcode = rNONE;
    bool a2,a3,a4;
    OBJ_DATA *offhand;

    if ( !IS_NPC(ch) )
    {
	ch->pcdata->damage_done_round=0;
	ch->pcdata->damage_taken_round=0;
	ch->pcdata->attacks_this_round=0;
	ch->pcdata->pl_gain_round=0;
    }

    if ( !IS_NPC(victim) )
    {
	victim->pcdata->damage_done_round=0;
	victim->pcdata->damage_taken_round=0;
	victim->pcdata->attacks_this_round=0;
	victim->pcdata->pl_gain_round=0;
    }

    /* add timer to pkillers */
    if ( !IS_NPC(ch) && !IS_NPC(victim) )
    {
        add_timer( ch,     TIMER_RECENTFIGHT, 11, NULL, 0 );
        add_timer( victim, TIMER_RECENTFIGHT, 11, NULL, 0 );
    }

    if ( is_attack_supressed( ch ) )
      return rNONE;

    if ( ch->skill_timer > 0 )
      return rNONE;

    if ( IS_AFFECTED(ch, AFF_CHARGING) )
	return rNONE;

    if ( IS_NPC(ch) && xIS_SET(ch->act, ACT_NOATTACK) )
      return rNONE;

	if ( ch->stance_learn_timer > 0 && ( !xIS_SET(victim->act, ACT_PACIFIST)  ) )
	    ch->stance_learn_timer--;
	if ( next_style(ch) != -1 && ch->styleunlocked[next_style(ch)] != 1 && ch->stance_learn_timer == 0 )
	{
	    pager_printf( ch, "You learned a new style!  \'%s\' is now available to you.\n\r", StyleNames[next_style(ch)] );
	    ch->styleunlocked[next_style(ch)] = 1;
	    ch->stance_learn_timer = -1;	    
	}


/*     if ( !IS_NPC(ch) && get_eq_char( ch, WEAR_WIELD ) )
     {
	retcode = one_hit( ch, victim, dt );
	return retcode;
     }*/


    if ( !IS_NPC(ch) && get_eq_char( ch, WEAR_WIELD ) )
    {
      retcode = one_hit( ch, victim, dt );
      if ( ch->style == STYLE_CIRCULARWEAPONRY )
      {
 	  retcode = one_hit( ch, victim, dt );
      }
      if ( (offhand = get_eq_char( ch, WEAR_DUAL_WIELD )) != NULL)
      {
//          send_to_char( "Making dual wield attempt...\n\r", ch );
	  chance = IS_NPC(ch) ? 80 : ch->pcdata->learned[gsn_dual_wield];
	  if ( number_range( 1, 99 ) < chance )
	  {
	      learn_from_success( ch, gsn_dual_wield );
	      retcode = one_hit( ch, victim, gsn_dual_wield );
	      if ( ch->style == STYLE_CIRCULARWEAPONRY )
	      {
	           retcode = one_hit( ch, victim, offhand->pIndexData->value[3] );
	      }
	  }
	  else
	  {
	      learn_from_failure( ch, gsn_dual_wield );
	  }
       }
       return retcode;
    }

    /*
     * NPC predetermined number of attacks			-Thoric
     */
    if ( IS_NPC(ch) &&  ch->numattacks == 0 )
    {
	retcode = one_hit( ch, victim, dt );
        if ( retcode != rNONE || who_fighting( ch ) != victim )
          return retcode;
    } 
    if ( IS_NPC(ch) && ch->numattacks >= 0 )
    {
	for ( chance = 0; chance < ch->numattacks; chance++ )
	{
	   retcode = one_hit( ch, victim, dt );
	   if ( retcode != rNONE || who_fighting( ch ) != victim )
	     return retcode;
	   if ( who_fighting( ch ) == NULL ) 
	     return rNONE;
	}
        if ( !IS_NPC(victim) )
        {
	    if (IS_SET(victim->pcdata->flags, PCFLAG_SUMMARY) )
	        ch_printf( victim, "&Y&wYou have taken %s damage this round.\n\r", format_pl(victim->pcdata->damage_taken_round) );
	    victim->pcdata->damage_taken_round = 0;
        }
	return retcode;
    }

    else
    {
        retcode = one_hit( ch, victim, TYPE_HIT );
    }


    if ( !IS_NPC(ch) && ch->perk[PERK_BONUS_HTH] )
    {
           one_hit( ch, victim, dt );
    }


	a2 = FALSE;
	a3 = FALSE;
	a4 = FALSE;

    if ( !IS_NPC(ch) && !xIS_SET(victim->act, ACT_PACIFIST) && ch->pcdata->set[gsn_tailattack] && can_use_skill( ch, number_percent(), gsn_tailattack ) )
    {
	learn_from_success( ch, gsn_tailattack );
	{
		ch_printf( ch, "You flick round and hammer %s in the chest with your tail!\n\r", PERS(victim, ch) );
		ch_printf( victim, "%s flicks round and hammers you in the chest with %s tail!\n\r", PERS(ch,victim), HISHER(ch->sex));
		damage( ch, victim, number_range( sqrt(ch->perm_str/10), sqrt(ch->perm_str/5) ), gsn_tailattack );
	}
    }
    if ( who_fighting( ch ) == NULL ) return rNONE;

    if ( can_use_skill( ch, number_percent(), gsn_second_attack ) )
    {
	learn_from_success( ch, gsn_second_attack );
	a2 = TRUE;
	retcode = one_hit( ch, victim, dt );
	if ( IS_AFFECTED( ch, AFF_SPLITFORM ) || IS_AFFECTED( ch, AFF_TRIFORM ) || IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, dt );
	if ( IS_AFFECTED( ch, AFF_TRIFORM ) || IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, dt );
	if ( IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, dt );
	if ( ch->style == STYLE_TIRA )
	   one_hit( ch, victim, dt );
	if ( retcode != rNONE || who_fighting( ch ) != victim )
	    return retcode;
    }
    else
	learn_from_failure( ch, gsn_second_attack );
    if ( who_fighting( ch ) == NULL ) return rNONE;
    if ( can_use_skill( ch, number_percent(), gsn_third_attack ) && a2)
    {
	learn_from_success( ch, gsn_third_attack );
	a3 = TRUE;
	retcode = one_hit( ch, victim, dt );
	if ( IS_AFFECTED( ch, AFF_SPLITFORM ) || IS_AFFECTED( ch, AFF_TRIFORM ) || IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, TYPE_HIT );
	if ( IS_AFFECTED( ch, AFF_TRIFORM ) || IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, TYPE_HIT );
	if ( IS_AFFECTED( ch, AFF_MULTIFORM ) )
	   one_hit( ch, victim, TYPE_HIT );
	if ( ch->style == STYLE_TIRA )
	   one_hit( ch, victim, dt );
	if ( retcode != rNONE || who_fighting( ch ) != victim )
	    return retcode;
    }
    else
	learn_from_failure( ch, gsn_third_attack );
    if ( who_fighting( ch ) == NULL ) return rNONE;
    if ( can_use_skill( ch, number_percent(), gsn_fourth_attack ) && a3 )
    {
	learn_from_success( ch, gsn_fourth_attack );
	retcode = one_hit( ch, victim, dt );
	if ( ch->style == STYLE_TIRA )
	   one_hit( ch, victim, dt );
	a4 = TRUE;
	if ( retcode != rNONE || who_fighting( ch ) != victim )
	    return retcode;
    }
    else
	learn_from_failure( ch, gsn_fourth_attack );
    if ( who_fighting( ch ) == NULL ) return rNONE;
    if ( can_use_skill( ch, number_percent(), gsn_fifth_attack ) && a4)
    {
	learn_from_success( ch, gsn_fifth_attack );
	retcode = one_hit( ch, victim, dt );
	if ( ch->style == STYLE_TIRA )
	   one_hit( ch, victim, dt );
	if ( retcode != rNONE || who_fighting( ch ) != victim )
	    return retcode;
    }
    else
	learn_from_failure( ch, gsn_fifth_attack );

    if ( IS_NPC(victim) && !IS_NPC(ch) && can_use_skill( ch, number_percent(), gsn_slaying_blow ) )
    {
        int percent = number_range(1,100);
        if ( victim->hit < victim->hit/2 && percent <= 5)
	{
	    damage(ch, victim, 9999999, gsn_slaying_blow);
            ch_printf(ch, "&RYou land a slaying blow on your enemy!  Most magnificent...\n\r" );
	}
        learn_from_success(ch, gsn_slaying_blow);
            
    }

    if ( !IS_NPC(ch) )
    {
	if ( IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) && ch->skill_timer <= 0 )
	{
	    ch_printf( ch, "&Y&wYou have inflicted %s damage in %d attacks this round.\n\r", format_pl(ch->pcdata->damage_done_round), ch->pcdata->attacks_this_round );
	    ch_printf( ch, "&Y&wYou have gained %s powerlevel.\n\r", format_pl( ch->pcdata->pl_gain_round ) );
	}
	ch->pcdata->damage_done_round = 0;
	ch->pcdata->attacks_this_round = 0;
	ch->pcdata->pl_gain_round = 0;
    }
    if ( !IS_NPC(victim) )
    {
	if (IS_SET(victim->pcdata->flags, PCFLAG_SUMMARY) )
	    ch_printf( victim, "You have taken %s damage this round.\n\r", format_pl(victim->pcdata->damage_taken_round) );
	victim->pcdata->damage_taken_round = 0;
    }

    retcode = rNONE;
    return retcode;
}


/*
 * Weapon types, haus
 */
int weapon_prof_bonus_check( CHAR_DATA *ch, OBJ_DATA *wield, int *gsn_ptr )
{
    int bonus;

    bonus = 0;	*gsn_ptr = -1;
    if ( !IS_NPC(ch) && ch->level > 5 && wield )   
    {
	switch(wield->value[3])
	{
	   default:		*gsn_ptr = -1;			break;
           case DAM_HIT:
	   case DAM_SUCTION:
	   case DAM_BITE:
	   case DAM_BLAST:	*gsn_ptr = gsn_pugilism;	break;
           case DAM_SLASH:
           case DAM_SLICE:	*gsn_ptr = gsn_long_blades;	break;
           case DAM_PIERCE:
           case DAM_STAB:	*gsn_ptr = gsn_short_blades;	break;
           case DAM_WHIP:	*gsn_ptr = gsn_flexible_arms;	break;
           case DAM_CLAW:	*gsn_ptr = gsn_talonous_arms;	break;
           case DAM_POUND:
           case DAM_CRUSH:	*gsn_ptr = gsn_bludgeons;	break;
	   case DAM_BOLT:
	   case DAM_ARROW:
	   case DAM_DART:
	   case DAM_STONE:
	   case DAM_PEA:	*gsn_ptr = gsn_missile_weapons;	break;

	}
	if ( *gsn_ptr != -1 )
	  bonus = (int) ((LEARNED(ch, *gsn_ptr) -50)/10);

       /* Reduce weapon bonuses for misaligned clannies.
       if ( IS_CLANNED(ch) )
       {
          bonus = bonus * 1000 / 
          ( 1 + abs( ch->alignment - ch->pcdata->clan->alignment ) );
       }*/

	if ( IS_DEVOTED(ch) )
	   bonus -= ch->pcdata->favor / -400 ;

    }
    return bonus;
}

/*
 * Calculate the tohit bonus on the object and return RIS values.
 * -- Altrag
 */
int obj_hitroll( OBJ_DATA *obj )
{
	int tohit = 0;
	AFFECT_DATA *paf;
	
	for ( paf = obj->pIndexData->first_affect; paf; paf = paf->next )
		if ( paf->location == APPLY_HITROLL )
			tohit += paf->modifier;
	for ( paf = obj->first_affect; paf; paf = paf->next )
		if ( paf->location == APPLY_HITROLL )
			tohit += paf->modifier;
	return tohit;
}

/*
 * Offensive shield level modifier
 */
sh_int off_shld_lvl( CHAR_DATA *ch, CHAR_DATA *victim )
{
    sh_int lvl;

    if ( !IS_NPC(ch) )          /* players get much less effect */
    {
        lvl = UMAX( 1, (ch->level - 10) / 2 );
        if ( number_percent() + (victim->level - lvl) < 40 )
	{
	  if ( CAN_PKILL( ch ) && CAN_PKILL( victim ) )
	    return ch->level;
	  else
	    return lvl;
	}
        else
          return 0;
    }
    else
    {
	lvl = ch->level / 2;
	if ( number_percent() + (victim->level - lvl) < 70 )
	  return lvl;
	else
	  return 0;
    }
}

/*
 * Hit one guy once.
 */
ch_ret one_hit( CHAR_DATA *ch, CHAR_DATA *victim, int dt )
{
    OBJ_DATA *wield;
    bool melee = FALSE;
//    double chit;
    long long dam = 0;
    int diceroll = 0;
//    int attacktype, cnt;
    int	prof_gsn = -1;
    ch_ret retcode = rNONE;
//    static bool dual_flip = FALSE;

    if ( ch->hit > ch->max_hit )
       ch->hit = ch->max_hit;
    if ( ch->hit < 0 )
       ch->hit = 0;

    if ( IS_AFFECTED(ch, AFF_HIDE) || IS_AFFECTED(ch, AFF_INVISIBLE)  )
    {
	do_visible(ch, NULL );
    }


    /*
     * Can't beat a dead char!
     * Guard against weird room-leavings.
     * Odd avatarisation bug.
     */
    if ( victim->position == POS_DEAD || ch->in_room != victim->in_room )
	return rVICT_DIED;

/*
    if ( ( !IS_NPC(ch) && !ch->desc ) )
	do_flee( ch, "" );
    if ( ( !IS_NPC(victim) && !victim->desc ) )
        do_flee( victim, "" );
*/

/*
 * Randomise targetting.  This'll teach those damned tankers (Yami)
 *
    if ( IS_NPC( ch ) )
    {
       for ( v1 = ch->in_room->first_person; v1; v1 = v1->next_in_room )
       {
	 if ( number_range( 1, 3) == 3 && ( !IS_NPC(ch) || !IS_NPC(v1) ) && ch != v1 && who_fighting(v1) == ch && ( !IS_NPC(v1) && v1->pcdata->petname != ch->name ) && (!IS_NPC(ch) && ch->pcdata->petname != v1->name ))
	 {
		if ( ch != v1 )
			victim = v1;
		break;
	 }
       }
    }
*/

    used_weapon = NULL;

    if ( dt == gsn_dual_wield ) 
    {
	wield = get_eq_char( ch, WEAR_DUAL_WIELD );
//	dt = TYPE_UNDEFINED;
    }
    else
    {
	wield = get_eq_char( ch, WEAR_WIELD );
    }
        
   if ( dt == TYPE_UNDEFINED )
   {
	dt = TYPE_HIT;
	if ( wield && wield->item_type == ITEM_WEAPON )
	    dt += wield->value[3];
        melee = TRUE;
    }

    /*
     * The moment of excitement!
     */

    while ( ( diceroll = number_bits( 5 ) ) >= 20 )
	;

	if ( FALSE )
	{
		/* Miss. */
		if ( prof_gsn != -1 )
		    learn_from_failure( ch, prof_gsn );
		damage( ch, victim, 0, dt );
		tail_chain( );
		return rNONE;
        }

        // Weapon + Base Damage
	   
        dam = number_range( 1, 20 );

        if ( wield )
        {
             dam = number_range( wield->value[1], wield->value[2] );
        }
   
        dam += (get_curr_str(ch)/200);

	if ( ch->race == RACE_ANDROID && !wield )
            dam += ( get_curr_dex( ch ) / 50 );
//	if ( ch->race != RACE_ANDROID && !wield )
//	    dam += ( get_curr_str( ch ) / 100 );

	if ( IS_NPC(ch) && xIS_SET(ch->act, ACT_MIMIC ))   // Bonus for mimics
 	    dam += number_range( 1, get_curr_str(ch) / 20 );

//        if ( wield && dam > wield->value[2] )  Why did I write this?!
// 	    dam = number_range( wield->value[1], wield->value[2] );

    // Perk Boosts
    if ( victim->hit <= victim->max_hit / 5 && ch->perk[PERK_REMORSELESS_BASTARD] )
	dam *= 1.3;
    if ( wield && ch->perk[PERK_BUSTER_BLADER] && ( dt >= TYPE_HIT ) )
	dam *= 1.5;
    if ( wield && victim->perk[PERK_AFTER_IMAGER] && ( dt >= TYPE_HIT ) )
	dam *= 0.9;
    if ( !wield && ch->perk[PERK_IRON_FISTED] && ( dt >= TYPE_HIT) )
	dam *= 1.2;
    if ( victim->perk[PERK_HIDE_OF_SCARS] )
	dam *= 0.9;
    if ( ch->perk[PERK_ADRENALINE_RUSH] && ch->hit < ch->max_hit / 3 )
	dam *= 1.5;
    if ( victim->perk[PERK_TOUGHNESS] )
        dam *= 0.9;
    if ( victim->perk[PERK_SHOWMAN] )
        dam *= 0.95;
    if ( ch->perk[PERK_SHOWMAN] )
        dam *= 1.05;
    if ( victim->perk[PERK_HIT_THE_DECK] && dt < TYPE_HIT && dt > 0 && skill_table[dt]->size >= 2 )
	dam *= 0.8;


    if ( (retcode = damage( ch, victim, dam, dt )) != rNONE )
	return retcode;
    if ( char_died(ch) )
	return rCHAR_DIED;
    if ( char_died(victim) )
	return rVICT_DIED;
//    if ( dam == 0 )
//	return retcode;

    retcode = rNONE;

    tail_chain( );
    return retcode;
}

/*
 * Hit one guy with a projectile.
 * Handles use of missile weapons (wield = missile weapon)
 * or thrown items/weapons
 */
ch_ret projectile_hit( CHAR_DATA *ch, CHAR_DATA *victim, OBJ_DATA *wield,
		       OBJ_DATA *projectile, sh_int dist )
{
//    int victim_ac;
//    int thac0;
//    int thac0_00;
//    int thac0_32;
    int plusris;
    int dam;
    int diceroll;
    int	prof_bonus;
    int	prof_gsn = -1;
    int proj_bonus;
    int dt;
    ch_ret retcode;

    if ( !projectile )
	return rNONE;

    if ( projectile->item_type == ITEM_PROJECTILE 
    ||   projectile->item_type == ITEM_WEAPON )
    {
	dt = TYPE_HIT + projectile->value[3];
	proj_bonus = number_range(projectile->value[1], projectile->value[2]);
    }
    else
    {
	dt = TYPE_UNDEFINED;
	proj_bonus = number_range(1, URANGE(2, get_obj_weight(projectile), 100) );
    }

    /*
     * Can't beat a dead char!
     */
    if ( victim->position == POS_DEAD || char_died(victim) )
    {
	extract_obj(projectile);
	return rVICT_DIED;
    }

    if ( wield )
	prof_bonus = weapon_prof_bonus_check( ch, wield, &prof_gsn );
    else
	prof_bonus = 0;

    if ( dt == TYPE_UNDEFINED )
    {
	dt = TYPE_HIT;
	if ( wield && wield->item_type == ITEM_MISSILE_WEAPON )
	    dt += wield->value[3];
    }

    /*
     * Calculate to-hit-armor-class-0 versus armor.
     *
    if ( IS_NPC(ch) )
    {
	thac0_00 = ch->mobthac0;
	thac0_32 =  0;
    }
    else
    {
	thac0_00 = class_table[ch->class]->thac0_00;
	thac0_32 = class_table[ch->class]->thac0_32;
    }
    thac0     = interpolate( ch->level, thac0_00, thac0_32 ) - GET_HITROLL(ch) + (dist*2);
    victim_ac = UMAX( -19, (int) (GET_AC(victim) / 10) );
*/
    /* if you can't see what's coming... */
/*    if ( !can_see_obj( victim, projectile) )
	victim_ac += 1;
    if ( !can_see( ch, victim ) )
	victim_ac -= 4;

   // Weapon proficiency bonus 
    victim_ac += prof_bonus;
*/
    /*
     * The moment of excitement!
     */
    while ( ( diceroll = number_bits( 5 ) ) >= 20 )
	;

//    if ( diceroll == 0
//    || ( diceroll < ( (get_curr_dex(ch) / (get_curr_dex(victim) ) ) ) ) * 10 )
if ( number_range( 1, 15 ) < ( sqrt( get_curr_dex(ch) ) / sqrt(get_curr_dex(victim) ) ) * 10  )
if ( FALSE )
    {
	/* Miss. */
	if ( prof_gsn != -1 )
	    learn_from_failure( ch, prof_gsn );

	/* Do something with the projectile */
	if ( number_percent() < 50 )
	    extract_obj(projectile);
	else
	{
	    if ( projectile->in_obj )
		obj_from_obj(projectile);
	    if ( projectile->carried_by )
		obj_from_char(projectile);
	    obj_to_room(projectile, victim->in_room);
	}
	damage( ch, victim, 0, dt );
	tail_chain( );
	return rNONE;
    }

    /*
     * Hit.
     * Calc damage.
     */

    if ( !wield )       /* dice formula fixed by Thoric */
	dam = proj_bonus;
    else
	dam = number_range(wield->value[1], wield->value[2]) + (proj_bonus / 10);

    /*
     * Bonuses.
     *
    dam += GET_DAMROLL(ch);

    if ( prof_bonus )
	dam += prof_bonus / 4;*/

    /*
     * Calculate Damage Modifiers from Victim's Fighting Style
     *
    if ( victim->position == POS_SAIYAN )
       dam = 2 * dam;
    else if ( victim->position == POS_ANCIENT )
       dam = 1.3 * dam;
    else if ( victim->position == POS_TURTLE )
       dam = .6 * dam;
    else if ( victim->position == POS_MUTANT )
       dam = .3 * dam;*/

/*    if ( !IS_NPC(ch) && ch->pcdata->learned[gsn_enhanced_damage] > 0 )
    {
	dam += (int) (dam * LEARNED(ch, gsn_enhanced_damage) / 120);
	learn_from_success( ch, gsn_enhanced_damage );
    }*/

    if ( !IS_AWAKE(victim) )
	dam *= 2;

    if ( dam <= 0 )
	dam = 1;

    plusris = 0;
/*
    if ( IS_OBJ_STAT(projectile, ITEM_MAGIC) )
	dam = ris_damage( victim, dam, RIS_MAGIC );
    else
	dam = ris_damage( victim, dam, RIS_NONMAGIC );
*/
    /*
     * Handle PLUS1 - PLUS6 ris bits vs. weapon hitroll	-Thoric
     */
    if ( wield )
	plusris = obj_hitroll( wield );

    /* check for RIS_PLUSx 					-Thoric */
    if ( dam )
    {
//	int x, res, imm, sus, mod;

int res, imm, sus;
	if ( plusris )
	   plusris = RIS_PLUS1 << UMIN(plusris, 7);

	/* initialize values to handle a zero plusris */
	imm = res = -1;  sus = 1;

	/* find high ris */
/*	for ( x = RIS_PLUS1; x <= RIS_PLUS6; x <<= 1 )
	{
	   if ( IS_SET( victim->immune, x ) )
		imm = x;
	   if ( IS_SET( victim->resistant, x ) )
		res = x;
	   if ( IS_SET( victim->susceptible, x ) )
		sus = x;
	}*/
/*	mod = 10;
	if ( imm >= plusris )
	  mod -= 10;
	if ( res >= plusris )
	  mod -= 2;
	if ( sus <= plusris )
	  mod += 2;

	 check if immune 
	if ( mod <= 0 )
	  dam = -1;
	if ( mod != 10 )
	  dam = (dam * mod) / 10;
*/
    }

    if ( prof_gsn != -1 )
    {
      if ( dam > 0 )
        learn_from_success( ch, prof_gsn );
      else
        learn_from_failure( ch, prof_gsn );
    }

    /* immune to damage */
    if ( dam == -1 )
    {
	if ( dt >= 0 && dt < top_sn )
	{
	    SKILLTYPE *skill = skill_table[dt];
	    bool found = FALSE;

	    if ( skill->imm_char && skill->imm_char[0] != '\0' )
	    {
		act( AT_HIT, skill->imm_char, ch, NULL, victim, TO_CHAR );
		found = TRUE;
	    }
	    if ( skill->imm_vict && skill->imm_vict[0] != '\0' )
	    {
		act( AT_HITME, skill->imm_vict, ch, NULL, victim, TO_VICT );
		found = TRUE;
	    }
	    if ( skill->imm_room && skill->imm_room[0] != '\0' )
	    {
		act( AT_ACTION, skill->imm_room, ch, NULL, victim, TO_NOTVICT );
		found = TRUE;
	    }
	    if ( found )
	    {
		if ( number_percent() < 50 )
		    extract_obj(projectile);
		else
		{
		    if ( projectile->in_obj )
			obj_from_obj(projectile);
		    if ( projectile->carried_by )
			obj_from_char(projectile);
		    obj_to_room(projectile, victim->in_room);
		}
		return rNONE;
	    }
	}
	dam = 0;
    }
    if ( (retcode = damage( ch, victim, dam, dt )) != rNONE )
    {
	extract_obj(projectile);
	return retcode;
    }
    if ( char_died(ch) )
    {
	extract_obj(projectile);
	return rCHAR_DIED;
    }
    if ( char_died(victim) )
    {
	extract_obj(projectile);
	return rVICT_DIED;
    }

    retcode = rNONE;
    if ( dam == 0 )
    {
	if ( number_percent() < 50 )
	    extract_obj(projectile);
	else
	{
	    if ( projectile->in_obj )
		obj_from_obj(projectile);
	    if ( projectile->carried_by )
		obj_from_char(projectile);
	    obj_to_room(projectile, victim->in_room);
	}
	return retcode;
    }

/* weapon spells	-Thoric */
    if ( wield )
//    &&  !IS_SET(victim->immune, RIS_MAGIC))
//    &&  !IS_SET(victim->in_room->room_flags, ROOM_NO_MAGIC) )
    {
	AFFECT_DATA *aff;
	
	for ( aff = wield->pIndexData->first_affect; aff; aff = aff->next )
	   if ( aff->location == APPLY_WEAPONSPELL
	   &&   IS_VALID_SN(aff->modifier)
	   &&   skill_table[aff->modifier]->spell_fun )
		retcode = (*skill_table[aff->modifier]->spell_fun) ( aff->modifier, (wield->level+3)/3, ch, victim );
	if ( retcode != rNONE || char_died(ch) || char_died(victim) )
	{
	    extract_obj(projectile);
	    return retcode;
	}
	for ( aff = wield->first_affect; aff; aff = aff->next )
	   if ( aff->location == APPLY_WEAPONSPELL
	   &&   IS_VALID_SN(aff->modifier)
	   &&   skill_table[aff->modifier]->spell_fun )
		retcode = (*skill_table[aff->modifier]->spell_fun) ( aff->modifier, (wield->level+3)/3, ch, victim );
	if ( retcode != rNONE || char_died(ch) || char_died(victim) )
	{
	    extract_obj(projectile);
	    return retcode;
	}
    }

    extract_obj(projectile);

    tail_chain( );
    return retcode;
}

/*
 * Calculate damage based on resistances, immunities and suceptibilities
 *					-Thoric */
// 99% of DB MUD players are too stupid to understand RIS... -- Yami
sh_int ris_damage( CHAR_DATA *ch, sh_int dam, int ris )
{
return dam;
/*   if ( IS_SET(ch->immune, ris )  && !IS_SET(ch->no_immune, ris) )
     modifier -= 10;
   if ( IS_SET(ch->resistant, ris ) && !IS_SET(ch->no_resistant, ris) )
     modifier -= 2;
   if ( IS_SET(ch->susceptible, ris ) && !IS_SET(ch->no_susceptible, ris) )
   {
     if ( IS_NPC( ch )
     &&   IS_SET( ch->immune, ris ) )
	modifier += 0;
     else
	modifier += 2;
   }
   if ( modifier <= 0 )
     return -1;
   if ( modifier == 10 )
     return dam;
   return (dam * modifier) / 10;*/

}


/*
 * Inflict damage from a hit.   This is one damn big function.
 */
ch_ret damage( CHAR_DATA *ch, CHAR_DATA *victim, long long damage, int dt )
{
    double dam = damage;
    AFFECT_DATA *aff;
    OBJ_DATA *obj = NULL;
    int iWear = 0;
    char buf1[MAX_STRING_LENGTH];
    char filename[256]; 
    sh_int dameq = 0;
    double damred = 0, ap = 0;
    static unsigned long maxdam = 999999;
    int conc = 1000000;
    bool npcvict = 0;
    bool loot;
    OBJ_DATA *damobj;
    ch_ret retcode;
    CHAR_DATA *gch /*, *lch */;
    int init_gold, new_gold, gold_diff;
    sh_int anopc = 0;  /* # of (non-pkill) pc in a (ch) */
    sh_int bnopc = 0;  /* # of (non-pkill) pc in b (victim) */
// Not good enough!
    OBJ_DATA *wield;
    unsigned long long victpl = 0;
    unsigned long long charpl = 0;
    float critrate = 4.98;
    int elema[6];
    int elemd[6];
    int counter = 0;
    int temp_cnt = 0;
    double multiplier = 0;
    double multiplier2 = 0;
    double multiplier3 = 0;
    sh_int largest_element = -1;
    char bufger[MAX_STRING_LENGTH];
    OBJ_INDEX_DATA * obj_index;
    OBJ_DATA	   * rand_obj;
    retcode = rNONE;
    for ( temp_cnt = 0; temp_cnt < 6 ; temp_cnt++)
	elema[temp_cnt] = 0; 
    for ( temp_cnt = 0; temp_cnt < 6 ; temp_cnt++)
	elemd[temp_cnt] = 0; 

    if ( !ch )
    {
	bug( "Damage: null ch!", 0 );
	return rERROR;
    }
    if ( !victim )
    {
	bug( "Damage: null victim!", 0 );
	return rVICT_DIED;
    }

    if ( victim->position == POS_DEAD )
	return rVICT_DIED;

    npcvict = IS_NPC(victim);

    /*
     * Precautionary step mainly to prevent people in Hell from finding
     * a way out. --Shaddai
     */
    if ( victim->in_room->vnum == 6 )
	dam = 0;

    if ( dt != gsn_dual_wield )
        wield = get_eq_char( ch, WEAR_WIELD );
    else
    {
	wield = get_eq_char( ch, WEAR_DUAL_WIELD );
        dt = TYPE_HIT;
        if ( wield && wield->item_type == ITEM_WEAPON )
            dt += wield->value[3];
    }



    if ( dam && npcvict && ch != victim )
    {
	if ( !xIS_SET( victim->act, ACT_SENTINEL ) )
 	{
	    if ( victim->hunting )
	    {
		if ( victim->hunting->who != ch )	
		{
		    STRFREE( victim->hunting->name );
		    victim->hunting->name = QUICKLINK( ch->name );
		    victim->hunting->who  = ch;
		}
            }
	    else
            if ( !xIS_SET(victim->act, ACT_PACIFIST)  ) /* Gorog */
		start_hunting( victim, ch );
	}

	if ( victim->hating )
	{
   	    if ( victim->hating->who != ch )
 	    {
		STRFREE( victim->hating->name );
		victim->hating->name = QUICKLINK( ch->name );
		victim->hating->who  = ch;
	    }
	}
	else
	if ( !xIS_SET(victim->act, ACT_PACIFIST)  ) /* Gorog */
	    start_hating( victim, ch );
    }


  if ( dam < 10000 && IS_NPC(ch) && dt < TYPE_HIT )
	dam += 5+dam;

    /*
     * PL Modifiers: Here so that ki attacks factor it too.
     */
	if ( IS_NPC(ch) )
		charpl = ch->pIndexData->currpl;
	else
		charpl = ch->currpl;

	if ( IS_NPC(victim) )
		victpl = victim->pIndexData->currpl;
	else
		victpl = victim->currpl;

		if ( victpl < 1 ) victpl = 1;
		if ( charpl < 1 ) charpl = 1;


	  if ( !xIS_SET(ch->act, ACT_MIMIC) && !xIS_SET(victim->act, ACT_MIMIC ) )
	  {

                multiplier = (double)(charpl/500)/(double)(victpl/500);
    //            pager_printf( ch, "Dam: %llu; multi: %f; ", dam, multiplier);

		if ( multiplier > 9999 )
		    dam = maxdam;
		if ( multiplier < 1/9999 )
		    dam = number_range(1,5);

		if ( dt >= TYPE_HIT )
		{
                    if ( multiplier > 1 )
                        multiplier2 = UMAX(multiplier2/5,1.15);
                    if ( multiplier < 1 )
                        multiplier2 = UMIN(multiplier2*5,0.95);
		    if ( multiplier <= 1.17 && multiplier >= 0.98 )
		    {
			dam += number_range( 10, 20+(get_curr_str(ch)/100) );
			if ( ch->race == RACE_SAIYAN )
			    dam += ch->rage/2;
		    }
    		    multiplier2 = pow(multiplier,1.01);
		}
		else
		{
                    if ( multiplier > 1 )
                        multiplier2 = UMAX(multiplier2/2,1.5);
                    if ( multiplier < 1 )
                        multiplier2 = UMIN(multiplier2*2,.75);
		    multiplier2 = multiplier * multiplier;
		}
		if ( IS_NPC(ch) && xIS_SET(ch->act, ACT_MIMIC) )
		    multiplier2 += 100;
		else if ( IS_NPC(ch) )
		    multiplier2 *= 0.6;
  //              pager_printf( ch, "damp: %f; ", multiplier2);
		if ( multiplier2 > 10000 ) multiplier2 = 10000;
                    multiplier3 = pow(multiplier2, 3);

		if ( IS_NPC(ch) && multiplier3 < 1 )
			multiplier3 = 1;
//                pager_printf( ch, "Multiplier: %f;", multiplier3);

                dam *= (multiplier3);
		if ( IS_NPC(ch) && dam < 1 )
		    dam += number_range(10,30);

		if ( IS_NPC(ch) )
		    dam *= 1+(((double)(ch->perm_str/500))/2);

		if ( IS_NPC(ch) && xIS_SET(ch->act, ACT_MIMIC) )
		    dam *= 10;
 	  }



    if (victim != ch)
    {
	/*
	 * Certain attacks are forbidden.
	 * Most other attacks are returned.
	 */
	if (is_safe(ch, victim, TRUE))
	    return rNONE;
	check_attacker(ch, victim);

	if (victim->position > POS_STUNNED)
	{
	    if (!victim->fighting && victim->in_room == ch->in_room)
		set_fighting(victim, ch);

	    /*
	       vwas: victim->position = POS_FIGHTING; 
	     */
	    if ( victim->fighting )
		victim->position = POS_FIGHTING;
	}

	if (victim->position > POS_STUNNED)
	{
	    if (!ch->fighting && victim->in_room == ch->in_room)
		set_fighting(ch, victim);

	    /*
	     * If victim is charmed, ch might attack victim's master.
	     */
	    if (IS_NPC(ch)
		&& npcvict
		&& IS_AFFECTED(victim, AFF_CHARM)
		&& victim->master
		&& victim->master->in_room == ch->in_room
		&& number_bits(3) == 0)
	    {
		stop_fighting(ch, FALSE);
		retcode = multi_hit(ch, victim->master, TYPE_UNDEFINED);
		return retcode;
	    }
	}

	/*
	   count the # of non-pkill pc in a ( not including == ch ) 
	 */
	for (gch = ch->in_room->first_person; gch; gch = gch->next_in_room)
	    if (is_same_group(ch, gch) && !IS_NPC(gch) && !IS_PKILL(gch) && (ch != gch))
		anopc++;

	/*
	   count the # of non-pkill pc in b ( not including == victim ) 
	 */
	for (gch = victim->in_room->first_person; gch; gch = gch->next_in_room)
	    if (is_same_group(victim, gch) && !IS_NPC(gch)
		&& !IS_PKILL(gch) && (victim != gch))
		bnopc++;

	/*
	 * Inviso attacks ... not.
	 */
	if (IS_AFFECTED(ch, AFF_INVISIBLE))
	{
	    xREMOVE_BIT(ch->affected_by, AFF_INVISIBLE);
	    act(AT_MAGIC, "$n fades into existence.", ch, NULL, NULL, TO_ROOM);
	}
	if ( ch->race == RACE_ANDROID )
		ch->energy = 122;
	if ( !IS_NPC(ch) && dt != TYPE_UNDEFINED && dt < TYPE_HIT )
   	{
	    if ( IS_SET( ch->pcdata->flags, PCFLAG_MANIAC) && ch->energy > 300 )
		ch->energy = 300;
	    if ( !IS_SET( ch->pcdata->flags, PCFLAG_MANIAC) && ch->energy > 150 )
		ch->energy = 150;

            if ( ch->race == RACE_BIOANDROID )
            {
    	        if ( IS_AFFECTED(ch, AFF_PERFECT ) )
		{
		    dam *= 1.8;
		}
	        if ( IS_AFFECTED(ch, AFF_FINAL_PERFECT ) )
		{
		    dam *= 3;
		}
            }
	
	    if ( !IS_AFFECTED(ch, AFF_DRAGON_INSTALL) && ch->bursttimer <= 0 )
		    dam = ( ( dam / 100 ) * ch->energy );
	    else
                dam = ( ( dam / 100 ) * ( ch->energy * 2 ) );

            if ( ch->race == RACE_HALFBREED )
		dam *= 2;

            dam *= 3.75;

	    ApplyEffect( ch );
	}

	if ( IS_NPC( ch )&& dt != TYPE_HIT && dt != TYPE_UNDEFINED )
	{
	    if ( ch->level > 1 && ch->level <= 10 )
		dam *= 1+(ch->level/2);
 	}

	/*
	 * Damage modifiers.
	 */
	if (IS_AFFECTED(victim, AFF_PROTECT) && IS_EVIL(ch))
	    dam *= 0.6;
	else if ( victim->race == RACE_BIOANDROID && victim->pkills / 25 > 30 )
            dam *= 0.7;
        else if (IS_AFFECTED(victim, AFF_SANCTUARY))
	    dam *= 0.8;
    }

    if ( !IS_NPC(victim) && dt != TYPE_UNDEFINED && dt < TYPE_HIT && dt > 0 )
    {
        if ( dam * 0.2 < victim->mana )
        {
            victim->mana -= dam *0.2;
            dam *= 0.5;
        }
    }
    

// Ignore powerlevels... weapon is a weapon.  May have to remove this one later.
if ( wield && dt >= TYPE_HIT )
     dam = number_range( wield->value[1], wield->value[2] );


//damred = ( GET_AC(victim) ) / 1000;

// Ok I have no fucking idea.  I'm just going to parrot out GET_AC macro.

damred = victim->armor + victim->armourboost;
if ( victim->race == RACE_ANDROID )
	damred += victim->hit/2;
if ( !IS_NPC(victim) && victim->race == RACE_DEMON && victim->plmod >= 2400 )
	damred += victim->absorbs[7] * 100;
if ( victim->style == STYLE_NUKEPROOFING )
	damred *= 2;

if ( (dt != TYPE_UNDEFINED && dt < TYPE_HIT) )
    damred /= 25;
else
    damred /= 1000;

if ( IS_NPC(victim) )
    damred *= 2;

if ( dt != gsn_sbc && dt != gsn_destructodisk && dt != gsn_ddd )
    dam -= (damred*0.8);

if ( victim->hit < victim->max_hit / 5 && victim->perk[PERK_DIE_HARD] == TRUE )
	dam -= ( dam / 4 );

// Racial/align banes on weapons. -Horus
// Vampyric regen - Yami
// Armour Piercing - More Yami
// Chaos effect - Yami

if (  wield && dam > 0 && dt > TYPE_HIT )
{
       for ( aff = wield->pIndexData->first_affect; aff; aff = aff->next )
       {
  	   if ( ( !str_cmp( "saiyan_bane", affect_loc_name(aff->location) ) && victim->race == RACE_SAIYAN )
	   || ( !str_cmp( "human_bane", affect_loc_name(aff->location) )  && victim->race == RACE_HUMAN )
	   || ( !str_cmp( "halfbreed_bane", affect_loc_name(aff->location) )  && victim->race == RACE_HALFBREED )
	   || ( !str_cmp( "tuffle_bane", affect_loc_name(aff->location) )  && victim->race == RACE_TUFFLE )
	   || ( !str_cmp( "icer_bane", affect_loc_name(aff->location) )  && victim->race == RACE_ICER )
	   || ( !str_cmp( "android_bane", affect_loc_name(aff->location) )  && victim->race == RACE_ANDROID )
	   || ( !str_cmp( "bioandroid_bane", affect_loc_name(aff->location) )  && victim->race == RACE_BIOANDROID )
	   || ( !str_cmp( "namek_bane", affect_loc_name(aff->location) )  && victim->race == RACE_NAMEK )
	   || ( !str_cmp( "kai_bane", affect_loc_name(aff->location) )  && victim->race == RACE_KAI )
	   || ( !str_cmp( "wizard_bane", affect_loc_name(aff->location) )  && victim->race == RACE_WIZARD )
 	   || ( !str_cmp( "demon_bane", affect_loc_name(aff->location) )  && victim->race == RACE_DEMON )
	   || ( !str_cmp( "fusion_bane", affect_loc_name(aff->location) )  && victim->race == RACE_FUSED )
	   || ( !str_cmp( "good_bane", affect_loc_name(aff->location) ) && victim->alignment > 333 )
	   || ( !str_cmp( "evil_bane", affect_loc_name(aff->location) ) && victim->alignment < -333  ) )
	       dam *= (float) ( aff->modifier / 100 );

           if ( !str_cmp( "armour_piercing", affect_loc_name(aff->location) ) )
  	   {
	       ap = aff->modifier;
	       if ( ap > 0 && ap >= damred )
		   dam += damred;
	       if ( ap > 0 && ap < damred )
 	           dam += ap;
 	   }
	   if ( !str_cmp( "soul_stealing", affect_loc_name(aff->location) ) && ch->race != RACE_ANDROID && victim->race != RACE_ANDROID && victim->mana > aff->modifier )
	   {
	       if ( aff->modifier > 100 )
	  	   aff->modifier = 100;
	       victim->mana -= aff->modifier;
		   ch->mana += aff->modifier;
	   }
           if ( !str_cmp( "vampiric_regeneration", affect_loc_name(aff->location) ) )
	   {
 	      if ( aff->modifier <= 10 )    // Capped
  	      {
	          dam += aff->modifier;
		  ch->hit += aff->modifier;
	      }
	      else
	      {
		  dam += 10;
		  ch->hit += 10;
	      }	
  	  }
          if ( !str_cmp( "aqua_strike", affect_loc_name(aff->location) ) )
       	      elema[0] += number_range(0,aff->modifier);
          if ( !str_cmp( "terra_strike", affect_loc_name(aff->location) ) )
       	      elema[1] += number_range(0,aff->modifier);
          if ( !str_cmp( "air_strike", affect_loc_name(aff->location) ) )
       	      elema[2] += number_range(0,aff->modifier);
          if ( !str_cmp( "flame_strike", affect_loc_name(aff->location) ) )
       	      elema[3] += number_range(0,aff->modifier);
          if ( !str_cmp( "hell_strike", affect_loc_name(aff->location) ) )
       	      elema[4] += number_range(0,aff->modifier);
          if ( !str_cmp( "heaven_strike", affect_loc_name(aff->location) ) )
       	      elema[5] += number_range(0,aff->modifier);
     }
}

if ( !IS_NPC(ch) && dam > 0 && can_use_skill( ch, number_percent(), gsn_divine_grace ) )
{
    learn_from_success( ch, gsn_divine_grace );
    float bonus = 0.05f * ((get_curr_wis(ch) + get_curr_cha(ch))/ 500);
    dam *= (1.0f + bonus);
}

if ( dam > 0 )
{
  if ( ch->starsign == Flare )
	elema[3] += 4;
  if ( ch->starsign == Heirophant )
	elema[5] += 4;
}


if ( !IS_NPC(victim) && dam > 0 && dt >= TYPE_HIT )
{
    for ( iWear = 0; iWear < MAX_WEAR; iWear++ )
    {
        for ( obj = victim->first_carrying; obj; obj = obj->next_content )
	{
           if ( obj->wear_loc == iWear )
           {
	      for ( aff = obj->pIndexData->first_affect; aff; aff = aff->next )
	      {
                 if ( !str_cmp( "aqua_guard", affect_loc_name(aff->location) ) )
                    elemd[0] += aff->modifier;
                 if ( !str_cmp( "terra_guard", affect_loc_name(aff->location) ) )
                    elemd[1] += aff->modifier;
                 if ( !str_cmp( "air_guard", affect_loc_name(aff->location) ) )
                    elemd[2] += aff->modifier;
                 if ( !str_cmp( "flame_guard", affect_loc_name(aff->location) ) )
                    elemd[3] += aff->modifier;
                 if ( !str_cmp( "hell_guard", affect_loc_name(aff->location) ) )
                    elemd[4] += aff->modifier;
                 if ( !str_cmp( "heaven_guard", affect_loc_name(aff->location) ) )
                    elemd[5] += aff->modifier;
	      }
	   }
	}
    }
}

dam -= damred;

if ( dam > maxdam )
{
    dam = maxdam;
}

    if ( IS_NPC(ch) && xIS_SET(ch->act, ACT_MIMIC ))  
	dam += number_range( get_curr_str(ch) / 50, get_curr_str(ch) / 25 );

    if ( ch->race == RACE_BIOANDROID && dt > 0 && dt != gsn_criticalhit && dt != gsn_combo )
    {
	int inc = 0;
	if ( IS_AFFECTED(ch, AFF_PERFECT) || IS_AFFECTED(ch, AFF_FINAL_PERFECT) )
	    inc = 25;
	if ( IS_AFFECTED(ch, AFF_FINAL_PERFECT) )
	    inc += 50;
	dam *= ( ( 100 + inc ) / 100 );
    }

 if ( victim->race == RACE_ANDROID && dt > 0 && dt < TYPE_HIT
 	&& dt!=gsn_tailattack && dt!=gsn_combo && dt!=gsn_dfp && dt != gsn_rocketpunch
	&& dt != gsn_criticalhit && dt != gsn_destructodisk && dt != gsn_ddd && dt != gsn_wolffang && dam > 0 ) // flarg
 {
     int learned, activ;

     if ( IS_NPC(victim) && victim->pIndexData->vnum == 1322 )
	 learned = 66;
     else if ( IS_NPC(victim) )
	 learned = 20;
     else if ( victim->pcdata->set[gsn_uberabsorb] )
  	 learned = victim->pcdata->learned[gsn_uberabsorb];
     else
         learned = -1;

     activ = number_range( 1, 100 );
     if ( !IS_AFFECTED( victim, AFF_CHARGING ) )
     {
         pager_printf( victim, "&cAttempting capsulate ki.  Your skill: %d.  You needed at least: %d.  %s\n\r", learned, activ, learned > activ ? "&GSuccess!" : "&rFailure!" );
         if ( learned >= activ )
         {
             pager_printf( victim, "^z&PAware of the presence of a ki attack you open your absorbtion recepticles.  Using your immense strength, you just suck all the might of the ki attack into your body!&W^x &z[&R%s absorbed!&z]&w\n\r", format_pl((dam/100) *learned ) );
       	     pager_printf( ch, "&R%s bulks up immensely for a moment and absorbs some of your attack into %s systems!&W\n\r", PERS(victim, ch), HISHER(ch->sex) );
             act ( AT_RED + AT_BLINK, "$n bulks up as $e absorbs some of $N's attack!", victim, NULL, ch, TO_NOTVICT );
	     victim->hit = URANGE( victim->hit, victim->hit + (( dam / 100 )*learned), victim->max_hit );
             victim->mana = URANGE( victim->mana, victim->mana + (( dam / 100 )*learned), victim->max_mana );
	     victim->basepl += ((dam/100)*learned) * 20;
	     if ( !IS_NPC(victim))
	         victim->pcdata->pl_today += ((dam/100)*learned) * 20;
	     learn_from_success( victim, gsn_uberabsorb );
	     dam -= ((dam/100) * learned);
	     update_current( victim, NULL );
	  }
          else
	  {
	      if ( IS_NPC(victim) )
	          learned = 40;
	      else if ( victim->pcdata->set[gsn_kiabsorb] )
	          learned = victim->pcdata->learned[gsn_kiabsorb];
  	      else
	           learned = 0;
	      activ = number_range(1, 100);

              pager_printf( victim, "&cAttempting ki absorb.  Your skill: %d.  You needed at least: %d.  %s\n\r", learned, activ, learned > activ ? "&GSuccess!" : "&rFailure!" );
              if ( learned >= activ )
              {
                  if ( number_range( 1, 100 ) < 95 ) /* 5% chance of full absorb */
                  {
 	              learn_from_success( victim, gsn_kiabsorb );
	              pager_printf( victim, "^z&YYou put up your hands and get a hold of %s's move, absorbing some of it!^x&w &z[&R%d absorbed!&z]\n\r", PERS(ch,victim), ( dam / 2 ) );
         	      pager_printf( ch, "^z&Y%s gets a hold of your move, absorbing some of it!^x&w &z[&R%d absorbed!&z]\n\r", PERS(victim,ch), dam - ( ( dam / 100 ) * learned ) );
                      dam /= 2;
	              victim->mana = URANGE( victim->mana, victim->mana + dam / 2, victim->max_mana );
	              update_current( victim, NULL );
	          }
                  else
                  {
	              learn_from_success( victim, gsn_kiabsorb );
	              pager_printf( victim, "^z&RYou put up your hands and get a hold of %s's move, absorbing it all!^x&w &z[&R%s absorbed!&z]\n\r", ch->name, format_pl( dam ) );
                      pager_printf( ch, "^z&R%s gets a hold of your move, absorbing it all!^x&w\n\r", victim->name );
  	              victim->mana = victim->max_mana;
	              dam = 0;
	          }
             }
          }
     }
  }

  if ( victim->skill_timer <= 0 
	&& dt > 1 
	&& dt < TYPE_HIT 
	&& skill_table[dt] != NULL 
	&& dt != TYPE_HIT 
	&& dt != TYPE_UNDEFINED 
	&& dt != gsn_criticalhit 
	&& dt!=gsn_tailattack  
	&& dt!=gsn_combo 
	&& dt!=gsn_shockwavepunch
	&& dt!=gsn_dfp 
	&& dt!=gsn_wolffang 
	&& dt!= gsn_counter 
	&& dam > 0 
	&& !IS_AFFECTED( victim, AFF_CHARGING ) )
       dam = kidefend( ch, victim, dam, dt );

    if ( IS_AFFECTED( victim, AFF_AFTER_IMAGE ) && dam > 0 )
    {
       if ( number_range( 1, 10 ) <= 80 )
       {
	   pager_printf( victim, "&YYou phase out of the way of %s's attack!\n\r", PERS( ch, victim ) );
 	   pager_printf( ch, "&YYour attack misses as %s phases out of the way!\n\r", PERS( victim, ch ) );
    	   dam = 0;
       }
    }
    /*
     * Code to handle equipment getting damaged, and also support  -Thoric
     * bonuses/penalties for having or not having equipment where hit
     */
    if (dt != TYPE_UNDEFINED && dam < 5000 && ( ( victim->style == STYLE_TEMPEST && dam > 100 ) 
					   || ( victim->style == STYLE_INDESTRUCTIBLEAURA && dam > 400 )
					   || ( victim->style != STYLE_INDESTRUCTIBLEAURA && dam > 200 ) ) )
    {
	/* get a random body eq part */
	dameq  = number_range(WEAR_LIGHT, WEAR_EYES);
	damobj = get_eq_char(victim, dameq);
	if ( damobj )
	{
            if ( dam > 0 && ch->starsign == Turtle ? get_obj_resistance(damobj) * 2 : get_obj_resistance(damobj)
            &&   number_bits(1) == 0 && !IS_OBJ_STAT( damobj, ITEM_MAGIC))
	    {
		set_cur_obj(damobj);
		damage_obj(damobj);
	    }
	    dam -= 5;  /* add a bonus for having something to block the blow */
	    if ( ch->starsign == Mask )
		dam -= 5;  // Extra bonus
	}
    }



    if ( IS_NPC( ch ) )
    {
        int tankers = 0;
	CHAR_DATA *v1;
        for ( v1 = ch->in_room->first_person; v1; v1 = v1->next_in_room )
        {
	  if ( v1 && who_fighting( v1 ) == ch )
          {
 	    tankers++;
 	  }
        }
	if ( tankers > 1 )
	{
	  dam *= ( tankers * 3 );
	}
    }


/****************************************************************************************
 *                									*
 *		            Support for the new fighting stances			*
 *											*
 ****************************************************************************************/

  switch ( ch->style )
  {
	case STYLE_OFFENSIVE:
		if ( !wield && ( dt == TYPE_HIT || dt == TYPE_UNDEFINED ) )
		   dam *= 1.15;
	break;
	case STYLE_DEFENSIVE:
		if ( !wield && ( dt == TYPE_HIT || dt == TYPE_UNDEFINED ) )
		   dam *= 0.85;
	break;
	case STYLE_KILORD:
		if ( dt > 0 && dt < TYPE_HIT )
		   dam *= 1.15;
	break;
        case STYLE_DOWNRIGHTFIERCE:
		if ( !wield && ( dt == TYPE_HIT || dt == TYPE_UNDEFINED ) )
                   dam *= 1.40;
        break;
        case STYLE_TEMPEST:
		if ( wield && ( dt == TYPE_HIT || dt == TYPE_UNDEFINED ) )
                   dam *= 1.15;
        break;
        case STYLE_ADHD:
		if ( !wield && ( dt == TYPE_HIT || dt == TYPE_UNDEFINED ) )
                   dam *= 2;
        break;
	case STYLE_NARCOPHOBIC:
		if (  dt > 0 && dt < TYPE_HIT )
		   dam *= 0.85;
	break;
	case STYLE_MELTDOWN:
		if (  dt > 0 && dt < TYPE_HIT  )
		   dam *= 2;
	break;
  }

  switch ( victim->style )
  {
	case STYLE_OFFENSIVE:
		if ( !wield &&( dt == TYPE_HIT || dt == TYPE_UNDEFINED ))
		    dam *= 1.15;
	break;
	case STYLE_DEFENSIVE:
                if ( !wield &&( dt == TYPE_HIT || dt == TYPE_UNDEFINED ))
                    dam *= 0.85;
        break;
	case STYLE_EVASIVE:
		if ( number_range( 1, 20 ) == 10 )
		    dam = 0;
	break;
	case STYLE_TANK:
		if ( number_range( 1, 5 ) == 3 )
		    dam += number_range( 2, 40 );
	break;
	case STYLE_KILORD:
		if ( !wield &&( dt == TYPE_HIT || dt == TYPE_UNDEFINED ))
		    dam *= 0.85;
	break;
	case STYLE_DOWNRIGHTFIERCE:
		if ( dt > 0 && dt < TYPE_HIT )
		    dam *= 1.40;
	break;
	case STYLE_INDESTRUCTIBLEAURA:
		if ( wield &&( dt == TYPE_HIT || dt == TYPE_UNDEFINED ))
		    dam *= 1.15;
	break;
	case STYLE_DEADANGLE:
		if ( dt == TYPE_HIT || dt == TYPE_UNDEFINED )
		    dam *= 1.40;
	break;
  }

/****************************************************************************************
 *                									*
 *	  	       End of support for the new fighting stances			*
 *											*
 ****************************************************************************************/

     if ( IS_AFFECTED(victim, AFF_LSSJ) )
	dam /= 4;

    critrate = ( (float)(get_curr_lck(ch) / 5 )/ 99.87 ) + 4.98;
    if ( ch->perk[PERK_MORE_CRITICALS] )
        critrate += 10;

    if ( ch->perk[PERK_SLAYER] )
        critrate += 33;

    if ( dam <= 1 )
	critrate = 0.00;

    if ( number_range( 1, 100 ) < critrate ) 
    {
	    dam *= 2;
	    if ( ch->perk[PERK_BETTER_CRITICALS] )
		dam *= 1.3;
	    if ( dt >= TYPE_HIT )
	    {
	        dt = gsn_criticalhit;
	    }
	    else
	    {
	        if ( IS_NPC(ch) || !IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY) )
	            send_to_char( "&RYou inflict a critical hit!\n\r", ch );
	        if ( IS_NPC(victim) || !IS_SET(victim->pcdata->flags, PCFLAG_SUMMARY) )
	 	    pager_printf( victim, "^z &P%s inflicts a critical hit!^x\n\r", ch->name );
	    }
    }


    /*
     * Hurt the victim.
     * Inform the victim of his new state.
     */

      for ( counter = 0 ; counter <= 5 ; counter++ )
      {
          if ( elema[counter] > 0 )
          {
              if ( elemd[counter] > 0 )
	      {
    	          elema[counter] = elema[counter] - elemd[counter];
	      }
	      if ( elema[counter] <= 0 )
		  elema[counter] = 0;
	      else
	      {
                  dam += elema[counter];
		  if ( largest_element == -1 ) largest_element = counter;
		  else if (elema[counter] > elema[largest_element]) largest_element = counter;
              }
          }
      }

      if ( !IS_NPC(ch) && /*ch->race == RACE_SAIYAN && */ch->pcdata->set[gsn_no_second_chances] 
        && number_range(0, 100 ) < ch->pcdata->learned[gsn_no_second_chances] )
      {
	  int hp_perc,hplevel;
	  hp_perc = (100 * ch->hit) / ch->max_hit;
	  hplevel = 50-hp_perc <= 0 ? 1 : 50-hp_perc;
	  dam     += (int)((hplevel * get_curr_str(ch)/50))/50;
	  learn_from_success( ch, gsn_no_second_chances );
	  largest_element = 10;
      }

      if ( !IS_NPC(victim) && /*victim->race == RACE_SAIYAN && */victim->pcdata->set[gsn_last_stand] 
        && number_range(0, 100 ) < victim->pcdata->learned[gsn_last_stand] )
      {
	  int hp_perc,hplevel;
	  hp_perc = (100 * victim->hit) / victim->max_hit;
	  hplevel = 50-hp_perc <= 0 ? 1 : 50-hp_perc;
	  dam     -= (int)((hplevel * get_curr_con(victim)/100))/50;
	  learn_from_success( victim, gsn_last_stand );
      }

	if ( dam > maxdam )
	    dam = maxdam;
	if ( dam <= 0 )
        {
	    dam = 0;
	}


	if ( dam > 0 )
	{
		if ( IS_AFFECTED(victim, AFF_INVULNERABILITYSHIELD) )
		{
		    dam *= 10;
		    victim->mana -= (dam);
		    largest_element = 6;
		    if ( victim->mana <= 0 )
		    {
			victim->mana = 0;
			xREMOVE_BIT( victim->affected_by, AFF_INVULNERABILITYSHIELD );
		    }
		}
		else
		    victim->hit -= dam;
	}

      if ( !IS_NPC(ch) )
      {
	  ch->pcdata->damage_done_round += dam;
	  ch->pcdata->attacks_this_round++;
      }

      if ( !IS_NPC(victim) )
      {
	  victim->pcdata->damage_taken_round += dam;
      }   

      dam_message(ch, victim, dam, dt, largest_element);

//      if ( dt == gsn_criticalhit && number_range(1,3) == 1 && !IS_SET(ch->in_room->room_flags, ROOM_DUEL) )
//	        move_room(ch, victim);

      if ( dam > 5 && !IS_NPC(ch) && ch->currpl < (victim->currpl*10)
	&& ( ( ch->race != RACE_ANDROID && number_range(1, 700 ) == 1 ) 
	|| ( ch->race == RACE_ANDROID && number_range( 1, 700 ) <= 4) ) )
      {
	send_to_char( "&w&G", ch );
	switch( number_range( 1, 7 ) )
	{
	   case 1:
		send_to_char( "Your strength has increased!&w\n\r", ch );
		ch->perm_str += number_range(4,7);
		break;
	   case 2:
		send_to_char( "Your dexterity has increased!&w\n\r", ch );
		ch->perm_dex += number_range(4,7);
		break;
	   case 3:
		send_to_char( "Your intelligence has increased!&w\n\r", ch );
		ch->perm_int += number_range(2,4);
		break;
	   case 4:
		send_to_char( "Your wisdom has increased!&w\n\r", ch );
		ch->perm_wis += number_range(2,4);
		break;
	   case 5:
		send_to_char( "Your constitution has increased!&w\n\r", ch );
		ch->perm_con += number_range(4,7);
		break;
	   case 6:
		send_to_char( "Your charisma has increased!&w\n\r", ch );
		ch->perm_cha += number_range(2,4);
		break;
	   case 7:
		send_to_char( "Your luck has increased!&w\n\r", ch );
		ch->perm_lck += number_range(4,10);
		break;
	   }
	}
	emotion_plus( ch, victim, dam );    	
	if ( ( victim->race == RACE_SAIYAN || victim->race == RACE_HALFBREED ) && dam > 20 )
	{
		rage_update( victim, TRUE, dam, ch );   
	        if ( ch->rage >= 50 && !ch->enraged )
        	        ch->enraged = TRUE;
	}


	if ( victim->race == RACE_KAI ) conc = get_curr_wis( victim ) * 25;
	else if ( victim->race == RACE_HALFBREED ) conc = get_curr_wis( victim ) * 2;
	else conc = get_curr_wis( victim );

	if ( victim->style == STYLE_MENSA )
		conc *= 1.5;
	if ( victim->style == STYLE_BERSERKER )
		conc *= 0.5;

	if ( ch->style == STYLE_MENSA )
		conc *= 1.5;
	if ( ch->style == STYLE_BERSERKER )
		conc *= 0.5;

	if ( dt >= TYPE_UNDEFINED && dt <= TYPE_HIT && dt != gsn_criticalhit)
	    conc *= 4;
	
            if ( (!IS_NPC(victim) && victim->pcdata->learned[gsn_epic_concentration] > 0) && 2 * dam > conc 
		&& can_use_skill( victim, number_percent(), gsn_epic_concentration ) )
            {
		send_to_char( "You feel your mind clear instantly.\n\r", victim );
                conc *= 5;
                learn_from_success( victim, gsn_epic_concentration );
            }
	    else if ( (!IS_NPC(victim) && victim->pcdata->learned[gsn_concentration] > 0) && 2 * dam > conc 
		&& can_use_skill( victim, number_percent(), gsn_concentration ) )
            {
		send_to_char( "You feel your mind clear momentarily.\n\r", victim );
                conc *= 2;
                learn_from_success( victim, gsn_concentration );
            }

	if ( !IS_NPC(victim) && dam > conc && victim->skill_timer > 1 )
 	{
	    send_to_char( "&GYou suffer a concentration failure!&w\n\r", victim );
	    act( AT_GREEN, "$n is interrupted!", victim, NULL, NULL, TO_CANSEE );
	    interpret( victim, "stun" );
//	    victim->skill_timer = 0;
//	    victim->substate = SUB_TIMER_DO_ABORT;
	}
// Chance of de-tailing
if ( !IS_NPC(ch) && !IS_NPC(victim))
{
   OBJ_DATA *wield = get_eq_char( ch, WEAR_WIELD );
   OBJ_DATA *obj;
   if ( victim->pcdata->monkey && wield && wield->value[2] >= 1 && wield->value[2] <= 3 && get_curr_int( ch ) > 700 && dam > 100 )
   {
	pager_printf( ch, "You slice %s's tail clean off!\n\r", victim->name );
	pager_printf( victim, "%s slices your tail clean off!!\n\r", ch->name );
	victim->pcdata->monkey = FALSE;
	victim->tailtimer = 14400; // Magic time: 2 hours of gameplay in half seconds.
	obj = create_object(get_obj_index(26),0);
	STRFREE( obj->short_descr );
	sprintf( bufger, "the tail of %s", ch->name );
	obj->short_descr = STRALLOC( bufger );
	obj_to_room( obj, ch->in_room );
   }
}

  if (!IS_NPC(ch)&& ch!=victim  && ch->race != RACE_BIOANDROID && dam > 0)
	pl_gain(ch, victim, dam);
  update_current( ch, NULL );

if ( ch!=victim&& !IS_SET(ch->in_room->room_flags, ROOM_DUEL ) 
  && dam > victim->hit + 5000 && ch->currpl < victim->currpl * 10 && !IS_NPC(ch))
{
	send_to_char( "&C ****OVERKILL****\n\r", ch );
	if ( number_range( 1, 10 ) == 1 )
	{
		send_to_char( "For your valour in battle, the legendary heroes of old grant you ", ch );
		switch( number_range( 1, 6 ) )
		{
			case 1:
				send_to_char( "5 mission kili!\n\r", ch );
				ch->pcdata->missionkili += 5;
			break;
			case 2:
				send_to_char( "5 strength!\n\r", ch  );
				ch->perm_str += 5;
			break;
			case 3:
				send_to_char( "5,000 zeni!\n\r", ch  );
				ch->gold += 5000;
			break;
			case 4:
				send_to_char( "nothing...\n\r", ch  );
			break;
			case 5:
				send_to_char( "5 constitution!\n\r", ch  );
				ch->perm_con += 5;
			break;
			case 6:
				if ( number_range( 1, 20 ) < 19 )
					send_to_char( "... nothing at all.\n\r", ch );
				else
				{
					obj_index = get_obj_index( number_range( 3101, 3119 ) );
					rand_obj = create_object( obj_index, ch->basepl / 10 );
					obj_to_char( obj, ch );
					send_to_char( obj->short_descr, ch );
					send_to_char( "\n\r", ch );
				}
			break;
		}
	}
}


    /* Dueling code means no death */
    if (!IS_NPC(victim) && victim->hit < 1 && IS_SET( victim->in_room->room_flags, ROOM_DUEL ))
	{
	pager_printf( ch, "%s is thoroughly wrecked, so you stop dueling.\n\r", PERS( victim, ch ) );
	send_to_char( "You decide to stop dueling, and not die...\n\r", victim );
	victim->hit = 1;
	stop_fighting( ch, TRUE );
	retcode = rCHAR_DIED;
	return (retcode);
	}

    update_pos( victim );
    if ( victim->hit <= 0 )
    {
	victim->position = POS_DEAD;
    }
    switch( victim->position )
    {
    case POS_MORTAL:
	act( AT_DYING, "$n is mortally wounded, and will die soon, if not aided.",
	    victim, NULL, NULL, TO_ROOM );
	act( AT_DANGER, "You are mortally wounded, and will die soon, if not aided.",
	victim, NULL, NULL, TO_CHAR );
	break;

    case POS_INCAP:
	act( AT_DYING, "$n is incapacitated and will slowly die, if not aided.",
	    victim, NULL, NULL, TO_ROOM );
	act( AT_DANGER, "You are incapacitated and will slowly die, if not aided.",
	victim, NULL, NULL, TO_CHAR );
	break;

    case POS_STUNNED:
        if ( !IS_AFFECTED( victim, AFF_PARALYSIS ) )
        {
	  act( AT_ACTION, "$n is stunned, but will probably recover.",
	    victim, NULL, NULL, TO_ROOM );
	  act( AT_HURT, "You are stunned, but will probably recover.",
	    victim, NULL, NULL, TO_CHAR );
	}
	break;

    case POS_DEAD:
	if ( dt >= 0 && dt < top_sn )
	{
	    SKILLTYPE *skill = skill_table[dt];

	    if ( skill->die_char && skill->die_char[0] != '\0' )
	      act( AT_DEAD, skill->die_char, ch, NULL, victim, TO_CHAR );
	    if ( skill->die_vict && skill->die_vict[0] != '\0' )
	      act( AT_DEAD, skill->die_vict, ch, NULL, victim, TO_VICT );
	    if ( skill->die_room && skill->die_room[0] != '\0' )
	      act( AT_DEAD, skill->die_room, ch, NULL, victim, TO_NOTVICT );
	}
	if ( ch->name != victim->name && !IS_NPC(ch) && !IS_NPC(victim) && can_use_skill ( ch, number_percent(), gsn_absorb ) && ch->in_room->vnum == victim->in_room->vnum )
	        absorb ( ch, victim );	
	if ( unequal_rank(ch, victim) && rank_dif(ch,victim) > 0 && !IS_NPC(ch) && !IS_NPC(victim) )
	{
		// Auto bounty
		ch->bullypk++;
		ch->pcdata->bounty += ch->bullypk * 1000;
	}
	if ( !IS_NPC(ch) && ch->pcdata->crusade && !str_cmp( ch->pcdata->crusade, victim->name ) )
	{
	    send_to_char( "Your crusade is over.  Report to the Supreme Kai.\n\r", ch );
	    ch->pcdata->crusade = STRALLOC ( "complete" );
	}

	act( AT_DEAD, "You mortally wound $N!\n\r$N is DEAD!!", ch, 0, victim, TO_CHAR );
	act( AT_DEAD, "$N is mortally wounded by $n!\n\r$N is DEAD!!", ch, 0, victim, TO_NOTVICT );
	act( AT_DEAD, "You are mortally wounded by $n!\n\r$N is DEAD!!", victim, 0, 0, TO_CHAR );


	if ( IS_NPC(victim) && victim->basepl > ch->currpl )
	{
	    if ( number_range(1,200) == 1 )
	    {
                int candyNum = number_range(25900, 25906);
		OBJ_DATA *candy = create_object( get_obj_index(candyNum), 1 );
		obj_to_room(candy, ch->in_room);
		act( AT_DEAD, "$n drops a candy as $e dies!", 0, 0, victim, TO_NOTVICT );
	    }
	}

	break;

    default:
	/*
	 * Victim mentalstate affected, not attacker -- oops ;)
	 * Thanks to gfinello@mail.karmanet.it for finding this bug
	 */
	if ( dam > victim->max_hit / 4 )
	{
	pager_printf(ch, "&RThat attack really hurt %s!\n\r", PERS(victim, ch));   
	act( AT_HURT, "&RThat really did HURT!", victim, 0, 0, TO_CHAR );
	   if ( number_bits(3) == 0 )
		worsen_mental_state( victim, 1 );
	}
	if ( victim->hit < victim->max_hit / 4 && dam > 0)
	{
	   act( AT_DANGER, "You are bleeding loads!",
		victim, 0, 0, TO_CHAR );
	}
	break;
    }

    /*
     * Payoff for killing things.
     */
    if ( victim->position == POS_DEAD || victim->hit <= 0)
    {
	if ( victim->race == RACE_DEMON )
	{
		if ( !IS_NPC(victim) && can_use_skill( victim, number_percent(),gsn_reformation ) && victim->steam > 10  && victim->fakelag <= 0 && victim->skill_timer <= 0 )
		{
			for ( ; ; )
			{
				if ( victim->steam <= 0 || victim->hit > victim->max_hit )
					break;
				victim->hit = URANGE( 0, victim->hit + 1000, victim->max_hit );
				victim->steam--;
			}
			if ( victim->hit > 0 )
			{
			learn_from_success( victim, gsn_reformation );
			act( AT_PINK, "$n just laughs evilly, pulling $mself back togther!!\n\r", victim, NULL, NULL, TO_CANSEE );
			send_to_char( "&PYou laugh evilly and pull yourself back together!\n\r", victim );
			retcode = rNONE;
			victim->position = POS_FIGHTING;
			return (retcode);
			}
		}
		else
			learn_from_failure( victim, gsn_reformation );
	}
	if ( victim->race == RACE_NAMEK && !IS_NPC(victim) )
	{
		if ( can_use_skill( victim, number_percent(),gsn_preservation ) )
		{
			learn_from_success( victim, gsn_preservation );
			act( AT_PINK, "$n spits out an egg, and emerges unharmed!\r", victim, NULL, NULL, TO_CANSEE );
			send_to_char( "&PYou spit out an egg and emerge unharmed!!\n\r", victim );
			victim->hit = victim->max_hit;  // Reformation
			retcode = rNONE;
			stop_fighting( victim, TRUE );
			return (retcode);
		}
		else if ( victim->pcdata->learned[gsn_preservation] > 10 )
		{
			send_to_char( "&PYou attempt to spit out an egg, but it breaks on impact with the ground!\n\r", victim );
			learn_from_failure( victim, gsn_preservation );
		}
	}

   if (!IS_NPC(ch) && xIS_SET(ch->act,PLR_MISSIONING ) && IS_NPC(victim) )
        {
            if (ch->pcdata->missionmob == victim->pIndexData->vnum)
            {
                send_to_char("You have almost completed your mission!\n\r",ch);
		send_to_char("Return to the mission master before your time runs out.\n\r",ch);
                ch->pcdata->missionmob = -1;
            }
        }
   if (!IS_NPC(ch) && xIS_SET(ch->act,PLR_MISSIONING ) && IS_NPC(victim) )
   {
	if ( ch->pcdata->missionzeni > 0 && victim->pIndexData->gold > 0 )
	{
	    ch->pcdata->missionzenicollected += victim->pIndexData->gold;
	    if ( ch->pcdata->missionzeni <= ch->pcdata->missionzenicollected )
	    {
                send_to_char("You have almost completed your mission!\n\r",ch);
                send_to_char("Return to the mission master before your time runs out.\n\r",ch);
                ch->pcdata->missionzeni = -1;
 	    }
	}
   }

   if ( !IS_NPC(victim) && victim->basepl <= 5000 && victim->hit <= 0 )
   {
	stop_fighting( victim, TRUE );
        stop_fighting( ch, TRUE );
	victim->hit = 1;
	send_to_char( "&RWarning!  Ordinarily here you would die.  BE MORE CAREFUL!\n\r", victim );
	send_to_char( "&CThis will ONLY last until you have 5,000 pl.\n\r", victim );
	victim->position = POS_RESTING;
	return (rNONE);
   }

	if ( !npcvict )
	{
	    sprintf( log_buf, "%s (%d) killed by %s at %d",
		PERS( victim, ch ),
		victim->level,
		(IS_NPC(ch) ? ch->short_descr : ch->name),
		victim->in_room->vnum );
	    log_string( log_buf );
	    to_channel( log_buf, CHANNEL_MONITOR, "Monitor", LEVEL_IMMORTAL );

            if (!IS_NPC( ch ) && !IS_IMMORTAL( ch ) && ch->pcdata->clan
	    &&   ch->pcdata->clan->clan_type != CLAN_ORDER
            &&   ch->pcdata->clan->clan_type != CLAN_GUILD
            &&   victim != ch )
            {
                sprintf( filename, "%s%s.record", CLAN_DIR, ch->pcdata->clan->name );
                sprintf( log_buf, "&P(%2d) %-12s &wvs &P(%2d) %s &P%s ... &w%s",
		  ch->level,
                  ch->name,
		  victim->level,
		  !CAN_PKILL( victim ) ? "&W<Peaceful>" :
		    victim->pcdata->clan ? victim->pcdata->clan->badge :
		      "&P(&WUnclanned&P)",
		  PERS( victim, ch ),
                  ch->in_room->area->name );
		if ( victim->pcdata->clan &&
		     victim->pcdata->clan->name == ch->pcdata->clan->name)
		;
		else
		  append_to_file( filename, log_buf );
            }

	    /*
	     * Dying penalty:
	     * 1/2 way back to previous level.
	     */
/*	    if ( victim->exp > exp_level(victim, victim->level) )
		gain_exp( victim, (exp_level(victim, victim->level) - victim->exp)/2 );
*/
	    /*
	     * New penalty... go back to the beginning of current level.
	     victim->exp = exp_level( victim, victim->level );
	     */
	}
	else
	if ( !IS_NPC(ch) )		/* keep track of mob vnum killed */
	    add_kill( ch, victim );

	check_killer( ch, victim );

	if ( ch->in_room == victim->in_room )
	    loot = legal_loot( ch, victim );
	else
	    loot = FALSE;



	if ( ch->race == RACE_BIOANDROID && ch != victim )
	{
		bio_gains(ch, victim, dam );
		bioskills( ch, victim );
		if ( ch->basepl < (victim->basepl*10) )
		{
			add_timer( ch, TIMER_KILLSTREAK, 60, NULL, 0 );	
			if ( !IS_NPC(victim) )
				add_timer( ch, TIMER_KILLSTREAK, 120, NULL, 0 );

			ch_printf( ch, "&YKillstreak extended.  Time left: %d seconds.  Future tail stab gains at %dx rate.\n\r", 
		 		get_timer(ch, TIMER_KILLSTREAK), 
				1+UMIN(get_timer(ch, TIMER_KILLSTREAK)/60, 10));
		}
	}

	if ( !IS_NPC(ch) )
	{
		if ( IS_NPC( victim ) )
		{
			if ( race_table[ch->race]->enemy1 == victim->race )
				ch->pcdata->loyalty++;
			if ( race_table[ch->race]->enemy2 == victim->race )
				ch->pcdata->loyalty++;
		}
		else
		{
                        if ( race_table[ch->race]->enemy1 == victim->race )
                                ch->pcdata->loyalty += 15;
                        if ( race_table[ch->race]->enemy2 == victim->race )
                                ch->pcdata->loyalty += 10;

		}
	}
	
	set_cur_char(victim);
        xREMOVE_BIT(victim->affected_by, AFF_LSSJ );
	do_say( victim, "Arrrrrrrrgh!" );
	raw_kill( ch, victim );
	if ( number_range( 1, 3 ) == 3 && ch->basepl < victim->basepl )
	    ch->honourkili+= number_range( 1, 10);

	if ( !IS_NPC(ch) && loot )
	{
	   /* Autogold by Scryn 8/12 */
	    if ( xIS_SET(ch->act, PLR_AUTOGOLD) )
	    {
		init_gold = ch->gold;
		do_get( ch, "coins corpse" );
		new_gold = ch->gold;
		gold_diff = (new_gold - init_gold);
		if (gold_diff > 0)
                {
                  sprintf(buf1,"%d",gold_diff);
		  do_split( ch, buf1 );
		} 
	    }
//	    if ( xIS_SET(ch->act, PLR_AUTOLOOT)
//	    &&   victim != ch )  /* prevent nasty obj problems -- Blodkai */
//		do_get( ch, "all corpse" );
//	    else
//		do_look( ch, "in corpse" );

	    if ( xIS_SET(ch->act, PLR_AUTOBEHEAD) && !IS_NPC(victim) && victim->pcdata->clan 
		&& ( !strcmp(victim->pcdata->clan->name, victim->pcdata->clan->leader)
		|| !strcmp(victim->pcdata->clan->name, victim->pcdata->clan->number1)
		|| !strcmp(victim->pcdata->clan->name, victim->pcdata->clan->number2) ) )
		do_behead( ch, "corpse" );


	    if ( xIS_SET(ch->act, PLR_AUTOSKIN) && ch->pcdata->learned[gsn_skin] > 0 && IS_NPC(victim) )
		do_skin( ch, "corpse" );

	    if ( xIS_SET(ch->act, PLR_AUTOSAC) && (!xIS_SET(ch->act, PLR_AUTOSKIN) || ch->pcdata->learned[gsn_skin] <= 0) )
		do_sacrifice( ch, "corpse" );
	}

        victim = NULL;
	if ( IS_SET( sysdata.save_flags, SV_KILL ) )
	   save_char_obj( ch );
	return rVICT_DIED;
//    }
}
    if ( victim == ch )
	return rNONE;

    /*
     * Take care of link dead people.
     *
    if ( !npcvict && !victim->desc  
    && !IS_SET( victim->pcdata->flags, PCFLAG_NORECALL ) )
    {
	if ( number_range( 0, victim->wait ) == 0)
	{
	    do_recall( victim, "" );
	    return rNONE;
	}
    }*/

    /*
     * Wimp out?
     */
    if ( npcvict && dam > 0 )
    {
	if ( ( xIS_SET(victim->act, ACT_WIMPY) && number_bits( 1 ) == 0
	&&   victim->hit < victim->max_hit / 2 )
	||   ( IS_AFFECTED(victim, AFF_CHARM) && victim->master
	&&     victim->master->in_room != victim->in_room ) )
	{
	    start_fearing( victim, ch );
	    stop_hunting( victim );
	    do_flee( victim, "" );
	}
    }

    if ( !npcvict
    &&   victim->hit > 0  && victim->wimpy != 0
    &&   victim->hit <= victim->wimpy
    &&   victim->wait == 0 )
	 do_flee( victim, "" );
    else
    if ( !npcvict && xIS_SET( victim->act, PLR_FLEE ) )
	do_flee( victim, "" );


    tail_chain( );
    return rNONE;
}



/*
 * Changed is_safe to have the show_messg boolian.  This is so if you don't
 * want to show why you can't kill someone you can't turn it off.  This is
 * useful for things like area attacks.  --Shaddai
 */
bool is_safe( CHAR_DATA *ch, CHAR_DATA *victim, bool show_messg )
{
    if ( char_died(victim) || char_died(ch) )
    	return TRUE;
   
    /* Thx Josh! */
    if ( who_fighting( ch ) == ch )
	return FALSE;

    if ( !victim ) /*Gonna find this is_safe crash bug -Blod*/
    {
        bug( "Is_safe: %s opponent does not exist!", ch->name );
        return TRUE;
    }
    if ( !victim->in_room )
    {
	bug( "Is_safe: %s has no physical location!", PERS( victim, ch ) );
	return TRUE;
    }
 
    if(IS_PACIFIST(ch)&&ch->rage < 70) /* Fireblade */
    {
        if ( show_messg ) {
    	set_char_color(AT_MAGIC, ch);
    	ch_printf(ch, "You are a pacifist and will not fight.\n\r");
	}
	return TRUE;
    }

    if ( IS_PACIFIST(victim) && victim->rage < 70 ) /* Gorog */
    {
        char buf[MAX_STRING_LENGTH];
        if ( show_messg ) {
        sprintf(buf, "%s is a pacifist and will not fight.\n\r",
                capitalize(victim->short_descr));
        set_char_color( AT_MAGIC, ch );
        send_to_char( buf, ch);
	}
        return TRUE;
    }

    if ( IS_NPC(victim) && IS_NPC(ch) )	
	return FALSE;

    if ( IS_SET(ch->in_room->room_flags, ROOM_SAFE ) )
    {
	send_to_char( "You are in a safe room.\n\r", ch );
	return FALSE;
    }

    if( !IS_NPC( ch ) && !IS_NPC( victim )
    &&   ch != victim 
    &&   IS_SET( victim->in_room->area->flags, AFLAG_NOPKILL ) ) 
    {
        if ( show_messg ) {
        set_char_color( AT_IMMORT, ch );
        send_to_char( "The gods have forbidden player killing in this area.\n\r", ch );
	}
        return TRUE;
    }
 
    if ( IS_NPC(ch) || IS_NPC(victim) )
	return FALSE;

/*    if ( get_age( ch ) < 18 || ch->level < 5 )
    {
        if ( show_messg ) {
	set_char_color( AT_WHITE, ch );	
	send_to_char( "You are not yet ready, needing age or experience, if not both. \n\r", ch );
	}
	return TRUE;
    }

    if ( get_age( victim ) < 18 || victim->level < 5 )
    {
        if ( show_messg ) {
	set_char_color( AT_WHITE, ch );
	send_to_char( "They are yet too young to die.\n\r", ch );
	}
	return TRUE;
    }

    if ( ch->level - victim->level > 5 
    ||   victim->level - ch->level > 5 )
    {
        if ( show_messg ) {
	set_char_color( AT_IMMORT, ch );
	send_to_char( "The gods do not allow murder when there is such a difference in level.\n\r", ch );
	}
	return TRUE;
    }*/

    if ( get_timer(victim, TIMER_PKILLED) > 0 )
    {
        if ( show_messg ) {
	set_char_color( AT_GREEN, ch );
        send_to_char( "That character has died within the last 10 minutes.\n\r", ch);
	}
        return TRUE;
    }
  
    if ( get_timer(ch, TIMER_PKILLED) > 0 )
    {
        if ( show_messg ) {
	set_char_color( AT_GREEN, ch );
        send_to_char( "You have been killed within the last 10 minutes.\n\r", ch );
	}
        return TRUE;
    }

    return FALSE;
}

/*
 * just verify that a corpse looting is legal
 */
bool legal_loot( CHAR_DATA *ch, CHAR_DATA *victim )
{
    /* anyone can loot mobs */
    if ( IS_NPC(victim) )
      return TRUE;
    /* non-charmed mobs can loot anything */
    if ( IS_NPC(ch) && !ch->master )
      return TRUE;
    /* members of different clans can loot too! -Thoric */
    if ( !IS_NPC(ch) && !IS_NPC(victim)
    &&    IS_SET( ch->pcdata->flags, PCFLAG_DEADLY ) 
    &&    IS_SET( victim->pcdata->flags, PCFLAG_DEADLY ) ) 
	return TRUE;
    return FALSE;
}

/*
 * See if an attack justifies a KILLER flag.
 */
void check_killer( CHAR_DATA *ch, CHAR_DATA *victim )
{
    /*
     * NPC's are fair game.
     */
    if ( IS_NPC(victim) )
    {
	if ( !IS_NPC( ch ) )
	{
	  int level_ratio;
	  level_ratio = URANGE( 1, ch->level / victim->level, 50);
	  if ( ch->pcdata->clan )
	    ch->pcdata->clan->mkills++;
	  ch->pcdata->mkills++;
	  ch->in_room->area->mkills++;
	  if ( ch->pcdata->deity ) 
	  {
	    if ( victim->race == ch->pcdata->deity->npcrace )
	      adjust_favor( ch, 3, level_ratio );
	    else
	      if ( victim->race == ch->pcdata->deity->npcfoe )
		adjust_favor( ch, 17, level_ratio );
	      else
                adjust_favor( ch, 2, level_ratio );
	  }
	}

	return;
    }

    /*
     * If you kill yourself nothing happens.
     */

    if ( ch == victim || ch->level >= LEVEL_IMMORTAL )
      return;

    /*
     * Any character in the arena is ok to kill.
     * Added pdeath and pkills here
     */
/*    	if ( !IS_NPC(ch) && !IS_NPC(victim) )
	{
	  ch->pcdata->pkills++;
	  victim->pcdata->pdeaths++;
	}
      return;*/

    /*
     * So are killers and thieves.
     *
    if ( xIS_SET(victim->act, PLR_KILLER)
    ||   xIS_SET(victim->act, PLR_THIEF) )
    {
	if ( !IS_NPC( ch ) )
	{
	  if ( ch->pcdata->clan )
	  {
	    if ( victim->level < 10 )
	      ch->pcdata->clan->pkills[0]++;
	    else if ( victim->level < 15 )
	      ch->pcdata->clan->pkills[1]++;
	    else if ( victim->level < 20 )
	      ch->pcdata->clan->pkills[2]++;
	    else if ( victim->level < 30 )
	      ch->pcdata->clan->pkills[3]++;
	    else if ( victim->level < 40 )
	      ch->pcdata->clan->pkills[4]++;
	    else if ( victim->level < 50 )
	      ch->pcdata->clan->pkills[5]++;
	    else
	      ch->pcdata->clan->pkills[6]++;
	  }	
	  ch->pcdata->pkills++;
	  ch->in_room->area->pkills++;
	}
	return;
    }*/

    /* clan checks					-Thoric */
    if ( !IS_NPC(ch) && !IS_NPC(victim)
    &&    IS_SET( ch->pcdata->flags, PCFLAG_DEADLY )
    &&    IS_SET( victim->pcdata->flags, PCFLAG_DEADLY ) )
    {
      /* not of same clan? Go ahead and kill!!! 
      if ( !ch->pcdata->clan
      ||   !victim->pcdata->clan 
      ||   ( ch->pcdata->clan->clan_type != CLAN_NOKILL
      &&   victim->pcdata->clan->clan_type != CLAN_NOKILL
      &&   (ch->pcdata->clan != victim->pcdata->clan || ch->in_room = 11056|| ch->in_room = 11057 || ch->in_room = 11058 || ch->in_room = 11059 || ch->in_room = 11060 || ch->in_room = 11061 || ch->in_room = 11062 || ch->in_room = 11063 || ch->in_room = 11064 || ch->in_room = 11065 || ch->in_room = 11066 || ch->in_room = 11067 || ch->in_room = 11068 || ch->in_room = 11069 || ch->in_room = 11070 || ch->in_room = 11071 || ch->in_room = 11072 || ch->in_room = 11073 || ch->in_room = 11074 || ch->in_room = 11075 )) )
      {
	if ( ch->pcdata->clan ) 
	{
            if ( victim->level < 10 )
              ch->pcdata->clan->pkills[0]++;
            else if ( victim->level < 15 )
              ch->pcdata->clan->pkills[1]++;
            else if ( victim->level < 20 )
              ch->pcdata->clan->pkills[2]++;
            else if ( victim->level < 30 )
              ch->pcdata->clan->pkills[3]++;
            else if ( victim->level < 40 )
              ch->pcdata->clan->pkills[4]++;
            else if ( victim->level < 50 )
              ch->pcdata->clan->pkills[5]++;
            else
              ch->pcdata->clan->pkills[6]++;
	}
	ch->pcdata->pkills++;
	update_pos(victim);
	if ( victim != ch )
	{
	  act( AT_MAGIC, "Bolts of blue energy rise from the corpse, seeping into $n.", ch, PERS( victim, ch ), NULL, TO_ROOM );
	  act( AT_MAGIC, "Bolts of blue energy rise from the corpse, seeping into you.", ch, PERS( victim, ch ), NULL, TO_CHAR ); 
	}
	if ( victim->pcdata->clan )
	{
            if ( ch->level < 10 )
              victim->pcdata->clan->pdeaths[0]++;
            else if ( ch->level < 15 )
              victim->pcdata->clan->pdeaths[1]++;
            else if ( ch->level < 20 )
              victim->pcdata->clan->pdeaths[2]++;
            else if ( ch->level < 30 )
              victim->pcdata->clan->pdeaths[3]++;
            else if ( ch->level < 40 )
              victim->pcdata->clan->pdeaths[4]++;
            else if ( ch->level < 50 )
              victim->pcdata->clan->pdeaths[5]++;
            else
              victim->pcdata->clan->pdeaths[6]++;
	}
	victim->pcdata->pdeaths++;   
	adjust_favor( victim, 11, 1 );
	adjust_favor( ch, 2, 1 );
	add_timer( victim, TIMER_PKILLED, 300, NULL, 0 );
	WAIT_STATE( victim, 3 * PULSE_VIOLENCE );
	 xSET_BIT(victim->act, PLR_PK); */
	return;
      }

    /*
     * Charm-o-rama.
     */
    if ( IS_AFFECTED(ch, AFF_CHARM) )
    {
	if ( !ch->master )
	{
	    char buf[MAX_STRING_LENGTH];

	    sprintf( buf, "Check_killer: %s bad AFF_CHARM",
		IS_NPC(ch) ? ch->short_descr : ch->name );
	    bug( buf, 0 );
	    xREMOVE_BIT( ch->affected_by, AFF_CHARM );
	    return;
	}

	/* stop_follower( ch ); */
	if ( ch->master )
	  check_killer( ch->master, victim );
	return;
    }

    /*
     * NPC's are cool of course (as long as not charmed).
     * Hitting yourself is cool too (bleeding).
     * So is being immortal (Alander's idea).
     * And current killers stay as they are.
     */
    if ( IS_NPC(ch) )
    {
      if ( !IS_NPC(victim) )
      {
	int level_ratio;
        if ( victim->pcdata->clan && victim->pcdata->clan->clan_type == CLAN_PLAIN )
          victim->pcdata->clan->mdeaths++;
        victim->pcdata->mdeaths++;
	victim->in_room->area->mdeaths++;
	level_ratio = URANGE( 1, ch->level / victim->level, 50 );
	if ( victim->pcdata->deity )
	{
	  if ( ch->race == victim->pcdata->deity->npcrace )
	    adjust_favor( victim, 12, level_ratio );
	  else
	    if ( ch->race == victim->pcdata->deity->npcfoe )
		adjust_favor( victim, 15, level_ratio );
	    else
	        adjust_favor( victim, 11, level_ratio );
	}
      }
      return;
    }

/*      
    if ( !IS_NPC(ch) )
    {
      if ( ch->pcdata->clan )
        ch->pcdata->clan->illegal_pk++;
      ch->pcdata->illegal_pk++;
      ch->in_room->area->illegal_pk++;
    }
    if ( !IS_NPC(victim) )
    {
      if ( victim->pcdata->clan )
      {
            if ( ch->level < 10 )
              victim->pcdata->clan->pdeaths[0]++;
            else if ( ch->level < 15 )
              victim->pcdata->clan->pdeaths[1]++;
            else if ( ch->level < 20 )
              victim->pcdata->clan->pdeaths[2]++;
            else if ( ch->level < 30 )
              victim->pcdata->clan->pdeaths[3]++;
            else if ( ch->level < 40 )
              victim->pcdata->clan->pdeaths[4]++;
            else if ( ch->level < 50 )
              victim->pcdata->clan->pdeaths[5]++;
            else
              victim->pcdata->clan->pdeaths[6]++;
      }
      victim->pcdata->pdeaths++;
      victim->in_room->area->pdeaths++;
    }*/
/*
    if ( xIS_SET(ch->act, PLR_KILLER) )
      return;

    set_char_color( AT_WHITE, ch );
    send_to_char( "A strange feeling grows deep inside you, and a tingle goes up your spine...\n\r", ch );
    set_char_color( AT_IMMORT, ch );
    send_to_char( "A deep voice booms inside your head, 'Thou shall now be known as a deadly murderer!!!'\n\r", ch );
    set_char_color( AT_WHITE, ch );
    send_to_char( "You feel as if your soul has been revealed for all to see.\n\r", ch );
    xSET_BIT(ch->act, PLR_KILLER);
    if ( xIS_SET( ch->act, PLR_ATTACKER) )
      xREMOVE_BIT(ch->act, PLR_ATTACKER);
    save_char_obj( ch );
*/
    return;
}

/*
 * See if an attack justifies a ATTACKER flag.
 * Modified by Veggy to deal with pkill flags
 */
void check_attacker( CHAR_DATA *ch, CHAR_DATA *victim )
{
    return;
 if ( xIS_SET(victim->act, PLR_KILLER )
   || ( xIS_SET(victim->act, PLR_KILLER ) && ch->basepl >= victim->basepl * 10  ) )
 {
     xSET_BIT(ch->act, PLR_KILLER);
     return;
 }

 return;
}
/*
 * Made some changes to this function Apr 6/96 to reduce the prolifiration
 * of attacker flags in the realms. -Narn
 */
    /*
     * NPC's are fair game.
     * So are killers and thieves.
     */
/*    if ( IS_NPC(victim)
    ||  xIS_SET(victim->act, PLR_KILLER)
    ||  xIS_SET(victim->act, PLR_THIEF) )
	return;*/

    /* deadly char check */
/*    if ( !IS_NPC(ch) && !IS_NPC(victim)
         && CAN_PKILL( ch ) && CAN_PKILL( victim ) )
	return;*/

/* Pkiller versus pkiller will no longer ever make an attacker flag
    { if ( !(ch->pcdata->clan && victim->pcdata->clan
      && ch->pcdata->clan == victim->pcdata->clan ) )  return; }
*/

    /*
     * Charm-o-rama.
     */
/*    if ( IS_AFFECTED(ch, AFF_CHARM) )
    {
	if ( !ch->master )
	{
	    char buf[MAX_STRING_LENGTH];

	    sprintf( buf, "Check_attacker: %s bad AFF_CHARM",
		IS_NPC(ch) ? ch->short_descr : ch->name );
	    bug( buf, 0 );
	    affect_strip( ch, gsn_charm_person );
	    xREMOVE_BIT( ch->affected_by, AFF_CHARM );
	    return;
	}*/

        /* Won't have charmed mobs fighting give the master an attacker 
           flag.  The killer flag stays in, and I'll put something in 
           do_murder. -Narn */
	/* xSET_BIT(ch->master->act, PLR_ATTACKER);*/
	/* stop_follower( ch ); */
/*	return;*/
//    }

    /*
     * NPC's are cool of course (as long as not charmed).
     * Hitting yourself is cool too (bleeding).
     * So is being immortal (Alander's idea).
     * And current killers stay as they are.
     */
/*    if ( IS_NPC(ch)
    ||   ch == victim
    ||   ch->level >= LEVEL_IMMORTAL
    ||   xIS_SET(ch->act, PLR_ATTACKER)
    ||   xIS_SET(ch->act, PLR_KILLER) )
	return;

    xSET_BIT(ch->act, PLR_ATTACKER);
    save_char_obj( ch );
    return;
}*/


/*
 * Set position of a victim.
 */
void update_pos( CHAR_DATA *victim )
{
    if ( !victim )
    {
      bug( "update_pos: null victim", 0 );
      return;
    }

    if ( victim->hit > 0 )
    {
	if ( victim->position <= POS_STUNNED )
	  victim->position = POS_STANDING;
	if ( IS_AFFECTED( victim, AFF_PARALYSIS ) )
	  victim->position = POS_STUNNED;
	return;
    }

    if ( IS_NPC(victim) || victim->hit < 1 )
    {
/*	if ( victim->mount )
	{
	  act( AT_ACTION, "$n falls from $N.",
		victim, NULL, victim->mount, TO_ROOM );
	  xREMOVE_BIT( victim->mount->act, ACT_MOUNTED );
	  victim->mount = NULL;
	}*/
	victim->position = POS_DEAD;
	return;
    }

    if ( victim->hit <= 5 ) victim->position = POS_MORTAL;
    else if ( victim->hit <= 10 ) victim->position = POS_INCAP;
    else if ( victim->hit < 15  ) victim->position = POS_STUNNED;
    else                          victim->position = POS_FIGHTING;

    if ( victim->position > POS_STUNNED
    &&   IS_AFFECTED( victim, AFF_PARALYSIS ) )
      victim->position = POS_STUNNED;

/*    if ( victim->mount )
    {
	act( AT_ACTION, "$n falls unconscious from $N.",
		victim, NULL, victim->mount, TO_ROOM );
	xREMOVE_BIT( victim->mount->act, ACT_MOUNTED );
	victim->mount = NULL;
    }*/
    return;
}


/*
 * Start fights.
 */
void set_fighting( CHAR_DATA *ch, CHAR_DATA *victim )
{
    FIGHT_DATA *fight;

    if ( ch->fighting )
    {
	char buf[MAX_STRING_LENGTH];

	sprintf( buf, "Set_fighting: %s -> %s",
		ch->name, 
                PERS( victim, ch ) );
	bug( buf, 0 );
	return;
    }


    /* Limit attackers -Thoric */
    if ( victim->num_fighting > max_fight(victim) )
    {
	send_to_char( "There are too many people fighting for you to join in.\n\r", ch );
	return;
    }

    CREATE( fight, FIGHT_DATA, 1 );
    fight->who	 = victim;
    fight->xp	 = (int) xp_compute( ch, victim ) * 0.85;
    fight->align = align_compute( ch, victim );
    if ( !IS_NPC(ch) && IS_NPC(victim) )
      fight->timeskilled = times_killed(ch, victim);
    ch->num_fighting = 1;
    ch->fighting = fight;
    /* ch->position = POS_FIGHTING; */
//	if ( IS_NPC(ch) )
    ch->position = POS_FIGHTING;
/*	else
	switch(ch->style)
	{
		case(STYLE_MUTANT): 
			ch->position = POS_MUTANT;
			break;
		case(STYLE_TURTLE): 
			ch->position = POS_TURTLE;
			break;
		case(STYLE_ANCIENT): 
			ch->position = POS_ANCIENT;
			break;
		case(STYLE_SAIYAN): 
			ch->position = POS_SAIYAN;
			break;
		default: ch->position = POS_FIGHTING;
	}*/
    victim->num_fighting++;
/*    if ( victim->switched && IS_AFFECTED(victim->switched, AFF_POSSESS) )
    {
	send_to_char( "You are disturbed!\n\r", victim->switched );
	do_return( victim->switched, "" );
    }*/
    return;
}


CHAR_DATA *who_fighting( CHAR_DATA *ch )
{
    if ( !ch )
    {
	bug( "who_fighting: null ch", 0 );
	return NULL;
    }
    if ( !ch->fighting )
      return NULL;
    return ch->fighting->who;
}

void free_fight( CHAR_DATA *ch )
{
   if ( !ch )
   {
	bug( "Free_fight: null ch!", 0 );
	return;
   }
   if ( ch->fighting )
   {
     if ( !char_died(ch->fighting->who) )
       --ch->fighting->who->num_fighting;
     DISPOSE( ch->fighting );
   }
   ch->fighting = NULL;
   ch->position = POS_STANDING;
   /* Berserk wears off after combat. -- Altrag */
   return;
}


/*
 * Stop fights.
 */
void stop_fighting( CHAR_DATA *ch, bool fBoth )
{
    CHAR_DATA *fch;

    free_fight( ch );
    update_pos( ch );
    ch->target = NULL;
    if ( !fBoth )   /* major short cut here by Thoric */
      return;

    for ( fch = first_char; fch; fch = fch->next )
    {
	if ( who_fighting( fch ) == ch )
	{
	    fch->target = NULL;
	    free_fight( fch );
	    update_pos( fch );
	}
    }
    return;
}

/* Vnums for the various bodyparts */
int part_vnums[] =
{	12,	/* Head */
	14,	/* arms */
	15,	/* legs */
	13,	/* heart */
	44,	/* brains */
	16,	/* guts */
	45,	/* hands */
	46,	/* feet */
	47,	/* fingers */
	48,	/* ear */
	49,	/* eye */
	50,	/* long_tongue */
	51,	/* eyestalks */
	52,	/* tentacles */
	53,	/* fins */
	54,	/* wings */
	55,	/* tail */
	56,	/* scales */
	59,	/* claws */
	87,	/* fangs */
	58,	/* horns */
	57,	/* tusks */
	55,	/* tailattack */
	85,	/* sharpscales */
	84,	/* beak */
	86,	/* haunches */
	83,	/* hooves */
	82,	/* paws */
	81,	/* forelegs */
	80,	/* feathers */
	0,	/* r1 */
	0	/* r2 */
};

/* Messages for flinging off the various bodyparts */
char* part_messages[] =
{
	"$n's severed head plops from its neck.",
	"$n's arm is sliced from $s dead body.",
	"$n's leg is sliced from $s dead body.",
	"$n's heart is torn from $s chest.",
	"$n's brains spill grotesquely from $s head.",
	"$n's guts spill grotesquely from $s torso.",
	"$n's hand is sliced from $s dead body.",
	"$n's foot is sliced from $s dead body.",
	"A finger is sliced from $n's dead body.",
	"$n's ear is sliced from $s dead body.",
	"$n's eye is gouged from its socket.",
	"$n's tongue is torn from $s mouth.",
	"An eyestalk is sliced from $n's dead body.",
	"A tentacle is severed from $n's dead body.",
	"A fin is sliced from $n's dead body.",
	"A wing is severed from $n's dead body.",
	"$n's tail is sliced from $s dead body.",
	"A scale falls from the body of $n.",
	"A claw is torn from $n's dead body.",
	"$n's fangs are torn from $s mouth.",
	"A horn is wrenched from the body of $n.",
	"$n's tusk is torn from $s dead body.",
	"$n's tail is sliced from $s dead body.",
	"A ridged scale falls from the body of $n.",
	"$n's beak is sliced from $s dead body.",
	"$n's haunches are sliced from $s dead body.",
	"A hoof is sliced from $n's dead body.",
	"A paw is sliced from $n's dead body.",
	"$n's foreleg is sliced from $s dead body.",
	"Some feathers fall from $n's dead body.",
	"r1 message.",
	"r2 message."
};
			
/*
 * Improved Death_cry contributed by Diavolo.
 * Additional improvement by Thoric (and removal of turds... sheesh!)  
 * Support for additional bodyparts by Fireblade
 */
void death_cry( CHAR_DATA *ch )
{
    ROOM_INDEX_DATA *was_in_room;
    EXIT_DATA *pexit;
    int vnum, shift, index, i;
    char *msg;

    return;

    if ( !ch )
    {
      bug( "DEATH_CRY: null ch!", 0 );
      return;
    }

    vnum = 0;
    msg = NULL;
    
    switch ( number_range(0, 5) )
    {
    default: msg  = "You hear $n's death cry.";				break;
    case  0:
      msg = "You feel $n's powerlevel fade away as $e falls to the ground."; break;
    case  1:
      msg  = "$n falls down in a limp heap.";			        break;
    case  2:
      msg = "$n cries out in pain as $e collopses and expires.";
                                                             break;
    case  3: msg  = "$n clutches $s chest in agony before and then falls.";		break;
    case  4: msg  = "$n waves $s arms at you in fury before giving in to $s injuries.";
                                                  break;
    case  5:
    	shift = number_range(0, 31);
    	index = 1 << shift;
    	
       	for(i = 0;i < 32 && ch->xflags;i++)
    	{
    		if(HAS_BODYPART(ch, index))
    		{
    			msg = part_messages[shift];
    			vnum = part_vnums[shift];
    			break;
    		}
    		else
    		{
    			shift = number_range(0, 31);
    			index = 1 << shift;
    		}
    	}
    	
    	if(!msg)
    		msg = "You hear $n's death cry.";
    	break;
    }

    act( AT_CARNAGE, msg, ch, NULL, NULL, TO_ROOM );

    if ( vnum )
    {
	char buf[MAX_STRING_LENGTH];
	OBJ_DATA *obj;
	char *name;

	if(!get_obj_index(vnum))
	{
		bug("death_cry: invalid vnum", 0);
		return;
	}

	name		= IS_NPC(ch) ? ch->short_descr : ch->name;
	obj		= create_object( get_obj_index( vnum ), 0 );
	obj->timer	= number_range( 4, 7 );
	if ( IS_AFFECTED( ch, AFF_POISON ) )
	  obj->value[3] = 10;

	sprintf( buf, obj->short_descr, name );
	STRFREE( obj->short_descr );
	obj->short_descr = STRALLOC( buf );

	sprintf( buf, obj->description, name );
	STRFREE( obj->description );
	obj->description = STRALLOC( buf );

	obj = obj_to_room( obj, ch->in_room );
    }

    if ( IS_NPC(ch) )
	msg = "You hear something's death cry.";
    else
	msg = "You hear someone's death cry.";

    was_in_room = ch->in_room;
    for ( pexit = was_in_room->first_exit; pexit; pexit = pexit->next )
    {
	if ( pexit->to_room
	&&   pexit->to_room != was_in_room )
	{
	    ch->in_room = pexit->to_room;
	    act( AT_CARNAGE, msg, ch, NULL, NULL, TO_ROOM );
	}
    }
    ch->in_room = was_in_room;

    return;
}



void raw_kill( CHAR_DATA *ch, CHAR_DATA *victim )
{
    char buf[MAX_STRING_LENGTH];
    int  blarg = 0;
    int stupid = 0;
    float pldif;

    if ( IS_NPC(victim) && victim->pIndexData->vnum >= 30 && victim->pIndexData->vnum < 40 )
    {
	law_broken( ch, 8 );
    }
    
    if ( victim->basepl == 0 )
      victim->basepl = 1;

    if ( !IS_NPC(victim) && victim->pcdata->legality < 0 && victim->basepl < 200000000 )
    {
        victim->pcdata->legality = 0;
    }

    if ( !IS_NPC( victim ) && !IS_NPC( ch ) )
    {
        if ( IS_AFFECTED(victim, AFF_SSJ2 ) && can_use_skill( victim, number_percent( ), gsn_majin ) && victim->pcdata->set[gsn_majin] )
        {
            learn_from_success( victim, gsn_majin );
            majin( victim, NULL );
            return;
        }
        if ( victim->plmod >= 2000 && can_use_skill( victim, number_percent(), gsn_decadentflare ) && victim->pcdata->set[gsn_decadentflare] )
        {
            if ( ch->currpl / 10 < victim->currpl && victim->hit != -1 && ch != victim && ch->hit < ch->max_hit * 0.33 )
            {
                finalexplosion( victim, NULL );
                return;
            }
        }
    }

    xREMOVE_BIT(ch->affected_by, AFF_OOZARU);
/*
	Weird infinite loop
    if ( victim->genotimer > 0 )
    {
	CHAR_DATA *fch = victim->in_room->first_person;
	CHAR_DATA *next = fch->next_in_room;
	int safety = 0;
        sprintf( log_buf, "Cleaning genocide aggro..." );
        log_string(log_buf);
        to_channel( log_buf, CHANNEL_MONITOR, "Monitor", LEVEL_IMMORTAL );
	for ( fch = victim->in_room->first_person; fch; fch = next )
        {
	    if ( safety > 3 )
		break;
	    if ( fch == victim )
	    {
		safety++;
		continue;
	    }
	    if ( IS_NPC(fch) && fch->retran > 1 && fch->retran != fch->in_room->vnum )
	    {
		next = fch->next_in_room;
	        act( AT_BLOOD, "$n's puts two fingers to $s head and teleports away.", fch, NULL, NULL, TO_ROOM );		
		char_from_room( fch );
	        sprintf( log_buf, "&p%s returning home to room %d after genocide death.",
			(IS_NPC(fch) ? fch->short_descr : PERS( fch, ch )), fch->retran );
	        log_string(log_buf);
	        to_channel( log_buf, CHANNEL_MONITOR, "Monitor", LEVEL_IMMORTAL );
		
		char_to_room( fch, get_room_index(fch->retran) );
		fch->retran = 1;
	    }
	}
    }
*/
    if ( IS_NPC(ch) && ch->pIndexData->vnum >= 20 && ch->pIndexData->vnum <= 24 )
    { 
        char_from_room( ch );
        char_to_room( ch, get_room_index( 6 ) );
    }

    change_align( ch, victim, FALSE );
    if ( victim->fused )
	fuse_split( victim );

    if ( infected && !str_cmp(infected->name, victim->name ) )
    {
	infected = NULL;
	if ( ch != victim && !IS_NPC(ch) )
	{
		infected = ch;
		send_to_char( "You have the viral heart disease now!\n\r", ch );
	}
	else
	talk_info( AT_PINK, "The Viral Heart Disease is no more.\n\r" );
    }
    pldif = ch->basepl / victim->basepl;

    if ( !victim )
    {
      bug( "raw_kill: null victim!", 0 );
      return;
    }
    stop_fighting( victim, TRUE );

    mprog_death_trigger( ch, victim );
    if ( char_died(victim) )
      return;

//    death_cry( victim );  // Rewrite this at some stage maybe? - J

    rprog_death_trigger( ch, victim );
    if ( char_died(victim) )
      return;

    make_corpse( victim, ch );
    de_equip_char( victim );
    if ( victim->in_room->sector_type == SECT_OCEANFLOOR
    ||   victim->in_room->sector_type == SECT_UNDERWATER
    ||   victim->in_room->sector_type == SECT_WATER_SWIM
    ||   victim->in_room->sector_type == SECT_WATER_NOSWIM )
      act( AT_BLOOD, "$n's blood slowly clouds the surrounding water.", victim, NULL, NULL, TO_ROOM );
    else if ( victim->in_room->sector_type == SECT_AIR )
      act( AT_BLOOD, "$n's blood sprays wildly through the air.", victim, NULL, NULL, TO_ROOM );
    else
      make_blood( victim );

	if ( IS_AFFECTED( ch, AFF_VISIBLE ) )
		xREMOVE_BIT( ch->affected_by, AFF_VISIBLE );
	if ( IS_AFFECTED( ch, AFF_AFTER_IMAGE ) )
		xREMOVE_BIT( ch->affected_by, AFF_AFTER_IMAGE );
	if ( IS_AFFECTED( victim, AFF_AFTER_IMAGE ) )
		xREMOVE_BIT( victim->affected_by, AFF_AFTER_IMAGE );

	if ( IS_AFFECTED( victim, AFF_FLAMING ) )
		xREMOVE_BIT( victim->affected_by, AFF_FLAMING );

	if ( ch->renzoku )
		ch->renzoku = FALSE;
	if ( victim->renzoku )
		victim->renzoku = FALSE;

    if ( IS_NPC(victim) )
    {
	CHAR_DATA *owner;
	for ( owner = victim->in_room->first_person; owner; owner = owner->next_in_room )
	{
		if ( !IS_NPC(owner) && owner->pcdata->petname && !str_cmp (owner->pcdata->petname,victim->name) )
		{
			send_to_char( "Ugh!  Your pet died, and you lose 1/4 of your life!\n\r", owner );
			owner->hit -= owner->max_hit / 4;
		}
	}
	victim->pIndexData->killed++;
	extract_char( victim, TRUE );
	victim = NULL;
	return;
    }

    if (!IS_NPC(ch) && ch != victim )
    {
	if (!IS_NPC(victim))
        {
	    add_timer( victim, TIMER_PKILLED, 300, NULL, 0 );
	    add_timer( ch, TIMER_PKER, 30, NULL, 0 );
	    ch->pkills++;
	    if ( ch->level < 50 )
                adjust_hiscore( "pkills", ch, ch->pkills );
            victim->pdeaths++;
	    if ( victim->level < 50 )
	    {
	 	adjust_hiscore( "deaths", victim, victim->pcdata->mdeaths + victim->pdeaths );
        	adjust_hiscore( "pdeaths", victim, victim->pdeaths );
	    }
	    if ( victim->pcdata->bounty > 0 )
	    {
	 	ch->pcdata->bountykills++;
           	sprintf(buf,"You receive a %d zeni bounty, for killing %s.\n\r", victim->pcdata->bounty, PERS( victim, ch ));
 	        send_to_char(buf, ch);
        	ch->gold += victim->pcdata->bounty;
 	        victim->pcdata->bounty =0;
            }
	    victim->bullypk = 0;
        }
	if ( !IS_NPC(ch) && ch->pcdata->clan && ch->pcdata->clan->clan_type == CLAN_PLAIN )
	    ch->pcdata->clan->pkills++;
 	if ( !IS_NPC(victim) && victim->pcdata->clan && victim->pcdata->clan->clan_type == CLAN_PLAIN )
	    victim->pcdata->clan->pdeaths++;
    }


   if ( ch == victim )
   {
	stupid = number_range (1 ,10 );
	switch (stupid)
	{
	case 1:
		sprintf( buf, "%s has been killed by %s own dumb self.", PERS( victim, ch ), victim->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
		break;
	case 2:
		 sprintf( buf, "%s has expired because of %s own carelessness.", PERS( victim, ch ), victim->sex == 0? "its" : ch->sex == 1   ? "his" : "her" );
		break;
	case 3:
	        sprintf( buf, "%s has died due to gross incompetence", PERS( victim, ch ) );
		break;
	case 4:
	        sprintf( buf, "%s should have been paying more attention and hence is now dead.", PERS( victim, ch )) ;
		break;
	case 5:
	        sprintf( buf, "%s is dead and has only %sself to blame.", PERS( victim, ch ), victim->sex == 0? "it" :victim->sex == 1? "him" : "her");
		break;
	case 6:
        	sprintf( buf, "%s has killed %sself.", PERS( victim, ch ),  victim->sex == 0? "it" :victim->sex == 1? "him" : "her");
		break;
	case 7:
		 sprintf( buf, "%s died.", PERS( victim, ch ) );
		break;
	case 8:
		 sprintf( buf, "%s let %s guard down and is now dead.", PERS( victim, ch ),victim->sex == 0? "its" : ch->sex== 1   ? "his" : "her" ); 
		break;
	case 9:
		 sprintf( buf, "%s died by %s own hand.", PERS( victim, ch ), HISHER(ch->sex) );
		break;
	case 10:
		 sprintf( buf, "%s is probably feeling stupid right now having killed %sself.", PERS( victim, ch ),  victim->sex == 0? "it" :victim->sex == 1? "him" : "her" );
		break;
	}
}
    else
    {
	blarg = number_range( 1, 40 );
	switch( blarg )
	{
	case 1:
		sprintf ( buf, "%s has been sent to King Yamma by %s.", PERS( victim, ch ), PERS( ch,victim  ) );
		break;
	case 2:
		 sprintf ( buf, "%s has been slain by %s.", PERS( victim, ch ),  PERS( ch,victim  )  );
                break;
	case 3:
		 sprintf ( buf, "%s has sent %s to a better place.",  PERS( ch,victim  )  , PERS( victim, ch ));
                break;
	case 4:
		 sprintf ( buf, "%s is now sushi thanks to %s.", PERS( victim, ch ),  PERS( ch,victim  )  );
                break;
	case 5:
		 sprintf ( buf, "%s has prematurely ended %s's life.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 6:
		 sprintf ( buf, "%s has expired at the hands of %s.", PERS( victim, ch ),  PERS( ch,victim  ) );
                break;
	 case 7:
		 sprintf ( buf, "%s has reduced %s into a gloopy mess.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
	 case 8:
		 sprintf ( buf, "%s has smashed up %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 9:
		 sprintf ( buf, "%s has sent %s into the next dimension.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 10:
		 sprintf ( buf, "%s has owned %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
	case 11:
		 sprintf ( buf, "%s has slaughtered %s.",  PERS( ch,victim  )  , PERS( victim, ch ));
                break;
         case 12:
		 sprintf ( buf, "%s has sent %s home in a box.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 13:
		 sprintf ( buf, "%s has rinsed %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 14:
		 sprintf ( buf, "%s has mangled %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 15:
		 sprintf ( buf, "%s has given %s a new definition of pain.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
         case 16:
		 sprintf ( buf, "%s has thoroughly killed %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
	case 17:
		 sprintf ( buf, "%s has sliced and diced %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 18:
		 sprintf ( buf, "%s has battered %s into a fine pulp.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 19:
		 sprintf ( buf, "%s has gutted %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
	case 20:
		 sprintf ( buf, "%s has fux0rd %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 21:
		 sprintf ( buf, "%s has been nailed by %s.",  PERS( victim,ch  ) , PERS( ch,victim ) );
                break;
        case 22:
		 sprintf ( buf, "%s has been disposed of by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 23:
		 sprintf ( buf, "%s has been pasted by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 24:
		 sprintf ( buf, "%s eviscerated %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 25:
		 sprintf ( buf, "%s has butchered %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 26:
		 sprintf ( buf, "%s has eradicated %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 27:
		 sprintf ( buf, "%s splattered %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 28:
		 sprintf ( buf, "%s was slapped down by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 29:
		 sprintf ( buf, "%s was masticated by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 30:
		 sprintf ( buf, "%s was pulverised by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 31:
		 sprintf ( buf, "%s obliterated %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 32:
		 sprintf ( buf, "%s was canned by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 33:
		 sprintf ( buf, "%s was crushed beneath the heel of %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 34:
		 sprintf ( buf, "%s was buried by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 35:
		 sprintf ( buf, "%s was flattened by %s.",  PERS( victim,ch  ) , PERS(ch, victim ) );
                break;
        case 36:
		 sprintf ( buf, "%s ripped %s apart.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 37:
		 sprintf ( buf, "%s annhiliated %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 38:
		 sprintf ( buf, "%s broke %s in half.",  PERS( ch,victim  ) , PERS( victim, ch ) );
               break;
        case 39:
		 sprintf ( buf, "%s mangled %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;
        case 40:
		 sprintf ( buf, "%s cremated %s.",  PERS( ch,victim  ) , PERS( victim, ch ) );
                break;




	}
    }
    talk_info( AT_LBLUE, buf );

    set_char_color( AT_DIEMSG, victim );

    if ( victim->race != RACE_ANDROID && victim->race != RACE_BIOANDROID )
      	do_help( victim, "_DIEMSG_" );
    else
      	do_help( victim, "_DIEMSGBOT_" );

    extract_char( victim, FALSE );
    if ( !victim )
    {
      bug( "oops! raw_kill: extract_char destroyed pc char", 0 );
      return;
    }
    while ( victim->first_affect )
	affect_remove( victim, victim->first_affect );
    xREMOVE_BIT( victim->affected_by, AFF_MAJIN );
    xREMOVE_BIT( victim->act, PLR_KILLER );
    victim->pktimer2    = 0;
    victim->armor	= 0;
    victim->plmod	= 100;
    if ( ch->in_room->vnum >= 120 )
    {
        if ( !IS_IMMORTAL(ch) && !IS_IMMORTAL(victim) && victim != ch )
        {
	    if ( !victim->perk[PERK_SILENT_DEATH] && victim->basepl > 10000000 )
 	    {
	        victim->basepl   -= victim->basepl * 0.02;
	    }
	    if ( !victim->perk[PERK_WAY_OF_THE_FRUIT] && victim->basepl > 10000000 )
	    {
		victim->perm_str = victim->perm_str * 0.95;
		victim->perm_con = victim->perm_con * 0.95;
		victim->perm_dex = victim->perm_dex * 0.95;
		victim->perm_int = victim->perm_int * 0.95;
		victim->perm_wis = victim->perm_wis * 0.95;
		victim->perm_cha = victim->perm_cha * 0.95;
		victim->perm_lck = victim->perm_lck * 0.95;
	    }
	}
    }
    if ( victim->perm_str < 50 )  victim->perm_str = 50;
    if ( victim->perm_dex < 50 )  victim->perm_dex = 50;
    if ( victim->perm_con < 50 )  victim->perm_con = 50;
    if ( victim->perm_int < 50 )  victim->perm_int = 50;
    if ( victim->perm_wis < 50 )  victim->perm_wis = 50;
    if ( victim->perm_cha < 50 )  victim->perm_cha = 50;
    if ( victim->perm_lck < 50 )  victim->perm_lck = 50;
    if ( ch->race == RACE_WIZARD )
	do_transform( victim, "dismiss" );
    else
	do_powerdown( victim, "hidden" );
    victim->currpl    = victim->basepl;
    victim->mod_str	= 0;
    victim->mod_dex	= 0;
    victim->mod_wis	= 0;
    victim->mod_int	= 0;
    victim->mod_con	= 0;
    victim->mod_cha	= 0;
    victim->mod_lck   	= 0;
    victim->multi_str	= 0;
    victim->multi_dex	= 0;
    victim->multi_wis	= 0;
    victim->multi_int	= 0;
    victim->multi_con	= 0;
    victim->multi_cha	= 0;
    victim->multi_lck  	= 0;
    if ( !IS_NPC( victim ) )
        REMOVE_BIT( victim->pcdata->flags, PCFLAG_CANDY );
    victim->alignment	= URANGE( -1000, victim->alignment, 1000 );
    victim->position	= POS_RESTING;
//    victim->hit		= UMAX( 1, victim->hit  );
    victim->hit 	= 1;
    victim->mana	= 1;
    xREMOVE_BIT( victim->affected_by, AFF_CHAOS );
    xREMOVE_BIT( victim->affected_by, AFF_LSSJ );
    xREMOVE_BIT( victim->affected_by, AFF_SYNCED );
    save_char_obj( victim );
    re_equip_char( victim );
    return;
}



void group_gain( CHAR_DATA *ch, CHAR_DATA *victim )
{
//    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *gch;
    CHAR_DATA *lch;
//    int xp;
    int members;

    /*
     * Monsters don't get kill xp's or alignment changes.
     * Dying of mortal wounds or poison doesn't give xp to anyone!
     */
    if ( IS_NPC(ch) || victim == ch )
	return;

    members = 0;
    for ( gch = ch->in_room->first_person; gch; gch = gch->next_in_room )
    {
	if ( is_same_group( gch, ch ) )
	    members++;
    }

    if ( members == 0 )
    {
	bug( "Group_gain: members.", members );
	members = 1;
    }

    lch = ch;
    for ( gch = ch->in_room->first_person; gch; gch = gch->next_in_room )
    {
	OBJ_DATA *obj;
	OBJ_DATA *obj_next;

	if ( !is_same_group( gch, ch ) )
	    continue;

	if ( gch->level - lch->level >  8 )
	{
	    send_to_char( "You are too high for this group.\n\r", gch );
	    continue;
	}

	if ( gch->level - lch->level < -8 )
	{
	    send_to_char( "You are too low for this group.\n\r", gch );
	    continue;
	}

//	gch->alignment = align_compute( gch, victim );
	for ( obj = ch->first_carrying; obj; obj = obj_next )
	{
	    obj_next = obj->next_content;
	    if ( obj->wear_loc == WEAR_NONE )
		continue;

	    if ( ( IS_OBJ_STAT(obj, ITEM_ANTI_EVIL)    && IS_EVIL(ch)    )
	    ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_GOOD)    && IS_GOOD(ch)    )
	    ||   ( IS_OBJ_STAT(obj, ITEM_ANTI_NEUTRAL) && IS_NEUTRAL(ch) ) )
	    {
		act( AT_MAGIC, "You are zapped by $p.", ch, obj, NULL, TO_CHAR );
		act( AT_MAGIC, "$n is zapped by $p.",   ch, obj, NULL, TO_ROOM );
	        
		obj_from_char( obj );
		obj = obj_to_room( obj, ch->in_room );
		oprog_zap_trigger(ch, obj);  /* mudprogs */
		if ( char_died(ch) )
		  return;
	    }
	}
    }
    return;
}


int align_compute( CHAR_DATA *gch, CHAR_DATA *victim )
{
    int align, newalign, divalign;

    align = gch->alignment - victim->alignment;

/*     slowed movement in good & evil ranges by a factor of 5, h 
     Added divalign to keep neutral chars shifting faster -- Blodkai 
     This is obviously gonna take a lot more thought */

    if ( gch->alignment > -350 && gch->alignment < 350 )
	divalign = 4;
    else
	divalign = 20;
    
    if ( align >  500 )
	newalign  = UMIN( gch->alignment + (align-500)/divalign,  1000 );
    else
    if ( align < -500 )
	newalign  = UMAX( gch->alignment + (align+500)/divalign, -1000 );
    else
	newalign  = gch->alignment - (int) (gch->alignment/divalign);

    return newalign;
}

/*
int align_compute( CHAR_DATA *gch, CHAR_DATA *victim )
{
  double align, difference, newalign;
  
  difference = gch->alignment - victim->alignment;
  align = gch->alignment;
  
  if (align <= -350){
    newalign = ( difference + (align/2.0) ) / 10.0;
  }
  else if (align >= 350 ){
    if (victim->alignment >= 350 ){
      newalign = -50 + ( (1000 - align)/100.0 );
    } else {
      newalign = ((
		   ( (difference-align) / 1000.0 ) 
		   *( (difference-align) / 1000.0 )
		   *( (difference-align) / 1000.0 )
		   )*100)
	+ (1000-align)/20.0;
    }
  } else { 
    newalign = -(
		 (  (difference-align) / 1000.0 ) 
		 *( (difference-align) / 1000.0 ) 
		 *( (difference-align) / 1000.0 )
		 * 100);
  }
  return (int) URANGE ( -1000, gch->alignment + newalign, 1000);
}
*/

/*
 * Calculate how much XP gch should gain for killing victim
 * Lots of redesigning for new exp system by Thoric
 */
int xp_compute( CHAR_DATA *gch, CHAR_DATA *victim )
{
    int align;
    int xp;
    int xp_ratio;
    int gchlev = gch->level;

    xp	  = (get_exp_worth( victim )
    	  *  URANGE( 0, (victim->level - gchlev) + 10, 13 )) / 10;
    align = gch->alignment - victim->alignment;

    /* bonus for attacking opposite alignment */
    if ( align >  990 || align < -990 )
	xp = (xp*5) >> 2;
    else
    /* penalty for good attacking same alignment */
    if ( gch->alignment > 300 && align < 250 )
	xp = (xp*3) >> 2;

    xp = number_range( (xp*3) >> 2, (xp*5) >> 2 );

    /* get 1/4 exp for players					-Thoric */
    if ( !IS_NPC( victim ) )
      xp /= 4;
    else
    /* reduce exp for killing the same mob repeatedly		-Thoric */
    if ( !IS_NPC( gch ) )
    {
	int times = times_killed( gch, victim );

	if ( times >= 20 )
	   xp = 0;
	else
	if ( times )
	{
	   xp = (xp * (20-times)) / 20;
	   if ( times > 15 )
	     xp /= 3;
	   else
	   if ( times > 10 )
	     xp >>= 1;
	}
    }

    /*
     * semi-intelligent experienced player vs. novice player xp gain
     * "bell curve"ish xp mod by Thoric
     * based on time played vs. level
     */
    if ( !IS_NPC( gch ) && gchlev > 5 )
    {
	xp_ratio = (int) gch->played / gchlev;
	if ( xp_ratio > 20000 )		/* 5/4 */
	    xp = (xp*5) >> 2;
	else
	if ( xp_ratio < 16000 )		/* 3/4 */
	    xp = (xp*3) >> 2;
	else
	if ( xp_ratio < 10000 )		/* 1/2 */
	    xp >>= 1;
	else
	if ( xp_ratio < 5000 )		/* 1/4 */
	    xp >>= 2;
	else
	if ( xp_ratio < 3500 )		/* 1/8 */
	    xp >>= 3;
	else
	if ( xp_ratio < 2000 )		/* 1/16 */
	    xp >>= 4;
    }

    /*
     * Level based experience gain cap.  Cannot get more experience for
     * a kill than the amount for your current experience level   -Thoric
     */
	// It's all different in DBZ...
    xp = 0;
    return URANGE(0, xp, exp_level(gch, gchlev+1) - exp_level(gch, gchlev) );
}


/*
 * Revamped by Thoric to be more realistic
 * Added code to produce different messages based on weapon type - FB
 * Added better bug message so you can track down the bad dt's -Shaddai
 */
void new_dam_message( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt, OBJ_DATA *obj, int greatest )
{
    char buf1[256], buf2[256], buf3[256], element[256];
    const char *vs;
    const char *vp;
    const char *attack;
    char punct;
    sh_int dampc;
    struct skill_type *skill = NULL;
    bool gcflag = FALSE;
    bool gvflag = FALSE;
    int d_index, w_index;
    ROOM_INDEX_DATA *was_in_room;
    
    if ( ! dam )
	dampc = 0;
    else
	dampc = ( (dam * 1000) / victim->max_hit) +
              ( 50 - ((victim->hit * 50) / victim->max_hit) );

    if ( ch->in_room != victim->in_room )
    {
	was_in_room = ch->in_room;
	char_from_room(ch);
	char_to_room(ch, victim->in_room);
    }
    else
	was_in_room = NULL;

    switch ( greatest )
    {
	case 0:  sprintf( element, "&Caquatic&Y&W " );  	break;
	case 1:  sprintf( element, "&Oebon&Y&W " );  		break;
	case 2:  sprintf( element, "&Ytempestuous&Y&W " );  	break;
	case 3:  sprintf( element, "&Rfiery&Y&W " );  		break;
	case 4:  sprintf( element, "&pdemonic&Y&W " );		break;
	case 5:  sprintf( element, "&Gdivine&Y&W " );  		break;
	case 6:  sprintf( element, "&Bmana&Y&W " ); 		break;
	case 10: sprintf( element, "&renraged&Y&W " );  	break;
    }

//  Get the weapon index 
    if ( dt > 0 && dt < top_sn )
    {
    	w_index = 0;
    }
    else
    if ( dt >= TYPE_HIT && dt < TYPE_HIT + sizeof(attack_table)/sizeof(attack_table[0]) )
    {
   	w_index = dt - TYPE_HIT;
    }
    else
    {
//	sprintf(bugbuf, "Dam_message: bad dt %d from %s in %d.",
//		dt, ch->name, ch->in_room->vnum );
//	bug( bugbuf, 0);
   	dt = TYPE_HIT;
   	w_index = 0;
    }

// get the damage index 
    if(dam == 0)
    	d_index = 0;
    else if(dampc < 0)
    	d_index = 1;
    else if(dampc <= 100)
   	d_index = 1 + dampc/10;
    else if(dampc <= 200)
   	d_index = 11 + (dampc - 100)/20;
    else if(dampc <= 900)
   	d_index = 16 + (dampc - 200)/100;
    else
   	d_index = 23;
   
// Lookup the damage message
    vs = s_message_table[w_index][d_index];
    vp = p_message_table[w_index][d_index];
   
    punct   = (dampc <= 30) ? '.' : '!';

    if ( dam == 0 && (!IS_NPC(ch) && 
       (IS_SET(ch->pcdata->flags, PCFLAG_GAG)))) gcflag = TRUE;

    if (!IS_NPC(ch) && (IS_SET(ch->pcdata->flags, PCFLAG_SUMMARY)) ) gcflag = TRUE;

    if (!IS_NPC(victim) && (IS_SET(victim->pcdata->flags, PCFLAG_SUMMARY)) ) gvflag = TRUE;

    if ( dam == 0 && (!IS_NPC(victim) &&
       (IS_SET(victim->pcdata->flags, PCFLAG_GAG)))) gvflag = TRUE;

    if ( dt >=0 && dt < top_sn )
	skill = skill_table[dt];
     if ( dt == TYPE_HIT )
    {
        switch ( number_range( 1, 5 ) )
        {
            default:attack = "punch"; break;
            case 1: attack = "punch"; break;
            case 2: attack = "kick"; break;
            case 3: attack = "elbow"; break;
            case 4: attack = "headbutt"; break;
            case 5: attack = "knee"; break;
        }
	sprintf( buf1, "$n's %s%s %s $N%c (%d)", greatest >= 0 ? element : "physical ",  attack, vp, punct, dam );	
	sprintf( buf2, "Your %s%s %s $N%c (%d)", greatest >= 0 ? element : "physical ", attack, vp, punct, dam);
	sprintf( buf3, "$n's %s%s %s you%c (%d)", greatest >= 0 ? element : "physical ", attack, vp, punct, dam );
    }
    else
    if ( dt > TYPE_HIT && is_wielding_poisoned( ch ) )
    {
	if ( dt < TYPE_HIT + sizeof(attack_table)/sizeof(attack_table[0]) )
	    attack	= attack_table[dt - TYPE_HIT];
	else
	{
//         sprintf(bugbuf, "Dam_message: bad dt %d from %s in %d.",
//                dt, ch->name, ch->in_room->vnum );
//         bug( bugbuf, 0);
	    dt  = TYPE_HIT;
	    attack  = attack_table[0];
        }

	sprintf( buf1, "$n's poisoned %s %s $N%c", attack, vp, punct );
	sprintf( buf2, "Your poisoned %s %s $N%c", attack, vp, punct );
	sprintf( buf3, "$n's poisoned %s %s you%c", attack, vp, punct ); 
    }
    else
    {
	if ( skill )
	{
	    attack	= skill->noun_damage;
	    if ( dam == 0 )
	    {
		bool found = FALSE;

		if ( skill->miss_char && skill->miss_char[0] != '\0' )
		{
		   act( AT_HIT, skill->miss_char, ch, NULL, victim, TO_CHAR );
		   found = TRUE;
		}
		if ( skill->miss_vict && skill->miss_vict[0] != '\0' )
		{
		   act( AT_HITME, skill->miss_vict, ch, NULL, victim, TO_VICT );
		   found = TRUE;
		}
		if ( skill->miss_room && skill->miss_room[0] != '\0' )
		{
		   if (strcmp( skill->miss_room,"supress" ) )
			act( AT_ACTION, skill->miss_room, ch, NULL, victim, TO_NOTVICT );
		   found = TRUE;
		}
		if ( found )	// miss message already sent 
		{
		   if ( was_in_room )
		   {
			char_from_room(ch);
			char_to_room(ch, was_in_room);
		   }
		   return;
		}
	    }
	    else
	    {
		if ( skill->hit_char && skill->hit_char[0] != '\0' )
		  act( AT_HIT, skill->hit_char, ch, NULL, victim, TO_CHAR );
		if ( skill->hit_vict && skill->hit_vict[0] != '\0' )
		  act( AT_HITME, skill->hit_vict, ch, NULL, victim, TO_VICT );
		if ( skill->hit_room && skill->hit_room[0] != '\0' )
		  act( AT_ACTION, skill->hit_room, ch, NULL, victim, TO_NOTVICT );
	    }
	}
	else if ( dt >= TYPE_HIT
	&& dt < TYPE_HIT + sizeof(attack_table)/sizeof(attack_table[0]) )
	{
	    if ( obj )
		attack = obj->short_descr;
	    else
		attack = attack_table[dt - TYPE_HIT];
	}
	else
	{
//            sprintf(bugbuf, "Dam_message: bad dt %d from %s in %d.",
//                dt, ch->name, ch->in_room->vnum );
//           bug( bugbuf, 0);
	    dt  = TYPE_HIT;
	    attack  = attack_table[0];
	}
    if ( !str_cmp( attack, "hit" ) )
    {
        switch ( number_range( 1, 5 ) )
        {
            default:attack = "punch"; break;
            case 1: attack = "punch"; break;
            case 2: attack = "kick"; break;
            case 3: attack = "elbow"; break;
            case 4: attack = "headbutt"; break;
            case 5: attack = "knee"; break;
        }
    }
	sprintf( buf1, "$n's %s%s %s $N%c (%d)",  greatest >= 0 ? element : "physical ", attack, vp, punct, dam );
	sprintf( buf2, "Your %s%s %s $N%c (%d)",  greatest >= 0 ? element : "physical ", attack, vp, punct, dam );
	sprintf( buf3, "$n's %s%s %s you%c (%d)", greatest >= 0 ? element : "physical ", attack, vp, punct, dam );
    }


    act( AT_ACTION, buf1, ch, NULL, victim, TO_NOTVICT );
    if (!gcflag)  act( AT_HIT, buf2, ch, NULL, victim, TO_CHAR );
    if (!gvflag)  act( AT_HITME, buf3, ch, NULL, victim, TO_VICT );

   if ( was_in_room )
   {
	char_from_room(ch);
	char_to_room(ch, was_in_room);
   }
    return;
}

// Yami's new damage message code
/*
void new_dam_message( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt, OBJ_DATA *obj )
{
    char buf1[256], buf2[256], buf3[256];
    const char *vs;
    const char *vp;
    const char *attack;
    char punct;
    sh_int dampc;
    struct skill_type *skill = NULL;
    bool gcflag = FALSE;
    bool gvflag = FALSE;
    int d_index, w_index;
    ROOM_INDEX_DATA *was_in_room;
    
    if ( !dam )
	  dampc = 0;
    else
	  dampc = ( (dam * 1000) / victim->max_hit) +
              ( 50 - ((victim->hit * 50) / victim->max_hit) );

    if ( ch->in_room != victim->in_room )
    {
	    was_in_room = ch->in_room;
	    char_from_room(ch);
	    char_to_room(ch, victim->in_room);
    }
    else
	    was_in_room = NULL;

    if ( dt > 0 && dt < top_sn )
    {
    	w_index = 0;
    }
    else
    if ( dt >= TYPE_HIT && dt < TYPE_HIT + sizeof(attack_table)/sizeof(attack_table[0]) )
    {
   	    w_index = dt - TYPE_HIT;
    }
    else
    {
        dt = TYPE_HIT;
        w_index = 0;
    }

    if(dam == 0)
        d_index = 0;
    else if(dampc < 0)
        d_index = 1;
    else if(dampc <= 100)
        d_index = 1 + dampc/10;
    else if(dampc <= 200)
        d_index = 11 + (dampc - 100)/20;
    else if(dampc <= 900)
        d_index = 16 + (dampc - 200)/100;
    else
        d_index = 23;
   
    vs = s_message_table[w_index][d_index];
    vp = p_message_table[w_index][d_index];
   
    punct   = (dampc <= 30) ? '.' : '!';
    
    if ( dam == 0 && (!IS_NPC(ch) && ( IS_SET(ch->pcdata->flags, PCFLAG_GAG ) ) ) ) 
        gcflag = TRUE;
    if ( dam == 0 && (!IS_NPC(victim) && ( IS_SET(victim->pcdata->flags, PCFLAG_GAG ) ) ) )
        gvflag = TRUE;

    if ( dt >= 0 && dt < top_sn )
    {
	    skill = skill_table[dt];
	    attack	= skill->noun_damage;
    }

    if ( obj )
	attack = obj->short_descr;	
    else if ( attack_table[dt - TYPE_HIT] )
	attack = attack_table[dt - TYPE_HIT];
    if ( !str_cmp( attack, "hit" ) )
    {
        switch ( number_range( 1, 5 ) )
        {
	    default:attack = "punch"; break;
            case 1: attack = "punch"; break;
            case 2: attack = "kick"; break;
            case 3: attack = "elbow"; break;                        
            case 4: attack = "headbutt"; break;
            case 5: attack = "knee"; break;
        }
    }
    sprintf( buf1, "$n's %s %s $N%c (%d)",  attack, vp, punct, dam );
    sprintf( buf2, "Your %s %s $N%c (%d)",  attack, vp, punct, dam );
    sprintf( buf3, "$n's %s %s you%c (%d)", attack, vp, punct, dam );
    act( AT_ACTION, buf1, ch, NULL, victim, TO_NOTVICT );
    if (!gcflag)  
        act( AT_HIT, buf2, ch, NULL, victim, TO_CHAR );
    if (!gvflag)  
        act( AT_HITME, buf3, ch, NULL, victim, TO_VICT );
    if ( was_in_room )
    {
   	    char_from_room(ch);
   	    char_to_room(ch, was_in_room);
    }
    return;
}
*/
#ifndef dam_message
void dam_message( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt, int greatest )
{
    new_dam_message(ch, victim, dam, dt, greatest);
}
#endif

void do_kill( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char buf[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *slave;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Kill whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ))
    {
	send_to_char( "This room is for dueling only.\n\r", ch );
    return;
    }

    if ( !IS_NPC(victim) )
    {
        send_to_char( "You must MURDER a player.\n\r", ch );
	    return;
    }
/*
    if ( !IS_NPC(ch) && xIS_SET(victim->act, PLR_KILLER) )
    {
        send_to_char( "They're an enormous coward.  If you don't like this change, blame carebears.  Loudly.  On OOC.\n\r", ch );
	    return;
    }
*/
    if ( victim == ch )
    {
	send_to_char( "You hit yourself.  Ouch!\n\r", ch );
	multi_hit( ch, ch, TYPE_UNDEFINED );
	return;
    }


    if ( IS_AFFECTED(ch, AFF_HIDE) || IS_AFFECTED(ch, AFF_INVISIBLE) )
    {
	send_to_char( "You are hidden.  Type 'visible' before you start a combat.\n\r", ch );
	return;
    }

	ch->renzoku = FALSE;

    if ( ch->position == POS_FIGHTING )
    {
	send_to_char( "You do the best you can!\n\r", ch );
	return;
    }

    for ( slave = ch->in_room->first_person; slave; slave = slave->next_in_room )
    {
	if ( ch->race == RACE_WIZARD && !IS_NPC(ch) && ch->pcdata->petname && !str_cmp( ch->pcdata->petname, slave->name ) )
	{	
	    multi_hit(slave, victim, TYPE_UNDEFINED );
	}
    }

        for ( slave = ch->in_room->first_person; slave; slave = slave->next_in_room )
        {
            if ( !slave || slave->covering == NULL || !str_cmp( slave->name,  slave->covering ) )
                break;
            if ( !str_cmp(slave->covering, victim->name ) && ( !xIS_SET(ch->act, PLR_KILLER ) || !unequal_rank(slave,ch) ))
            {
                if ( can_use_skill( slave, number_percent(), gsn_protect ) && ( IS_NPC(slave) || slave->pcdata->set[gsn_protect] ) )
                {
                    sprintf( buf, "Run, %s!  I'll protect you!", slave->covering );
                    do_wartalk( slave, buf );
                    learn_from_success( slave, gsn_protect );
                    multi_hit( ch, slave, TYPE_UNDEFINED );
                    return;
                }
            else
                learn_from_failure( slave, gsn_protect );
            }
        }

    change_align( ch, victim, TRUE );
    WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
    check_attacker( ch, victim );
    ch->skill_timer = 0;
    multi_hit( ch, victim, TYPE_UNDEFINED );
    if ( IS_SET( ch->in_room->area->flags, AFLAG_LAWFUL ) )
	law_broken(ch, 1);
    return;
}



void do_murde( CHAR_DATA *ch, char *argument )
{
    send_to_char( "If you want to MURDER, spell it out.\n\r", ch );
    return;
}



void do_murder( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CHAR_DATA *slave;

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Murder whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

/*  Give 'em an inch and they'll abuse it for a mile -- Yami */
    if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ))
    {
	send_to_char( "This room is for dueling only.\n\r", ch );
        return;
    }

    if ( IS_AFFECTED(ch, AFF_HIDE) || IS_AFFECTED(ch, AFF_INVISIBLE) )
    {
	send_to_char( "You are hidden.  Type 'visible' before you start a combat.\n\r", ch );
	return;
    }


    if ( IS_AFFECTED( ch, AFF_SCARED ) && !IS_AFFECTED(ch, AFF_LSSJ))
    {
        send_to_char( "No way!  That's far too dangerous...\n\r", ch );
        return;
    }

    if ( IS_AFFECTED(victim, AFF_COOKING ) || IS_AFFECTED(victim, AFF_FISHING ) )
    {	
	send_to_char( "Whilst they're busy making food?  How rude!\n\r", ch );
        return;
    }

    if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ))
    {
	send_to_char( "Noes.\n\r", ch );
 	return;
    }

    if ( victim == who_fighting( ch ) )
    {
	send_to_char( "Again?\n\r", ch );
	return;
    }

    if ( victim == ch )
    {
	send_to_char( "Killing yourself would invariably mean you'd get your ass kicked.\n\r", ch );
	return;
    }
    if ( IS_NPC(victim) )
    {
	send_to_char( "You KILL NPCs...\n\r", ch );
	return;
    }
/*
    if ( !IS_NPC(ch) && xIS_SET(victim->act, PLR_KILLER) )
    {
	send_to_char( "They're an enormous coward.  If you don't like this change, blame carebears.  Loudly.  On OOC.\n\r", ch );
	return;
    }
*/
    if ( !IS_NPC(ch) && !IS_NPC( victim ) )
    {
        if ( get_timer(victim, TIMER_PKILLED) > 0 )
        {
            set_char_color( AT_GREEN, ch );
            send_to_char( "That character has died within the last 10 minutes.\n\r", ch);
            return;
        }
        if ( get_timer(ch, TIMER_PKILLED) > 0 )
        {
            set_char_color( AT_GREEN, ch );
            send_to_char( "You have been killed within the last 10 minutes.\n\r", ch );
            return;
        }
        if ( ch->pcdata->clan && victim->pcdata->clan )
        {
            if ( ch->pcdata->clan == victim->pcdata->clan )
            {
	        send_to_char( "Kill a clannie!?\n\r", ch );
	        return;
            }
	    if ( strstr( ch->pcdata->clan->treaty, victim->pcdata->clan->name ) || strstr( victim->pcdata->clan->treaty, ch->pcdata->clan->name ) )
	    {
	        send_to_char( "Your clans are in an era of peace.\n\r", ch );
	        return;	
	    }
        }
    }

    if ( !IS_NPC(ch) && !IS_NPC(victim ) )
    {
        if ( xIS_SET(ch->act, PLR_KILLER ) && unequal_rank( ch, victim ) )
        {
	    set_char_color( AT_MAGIC, ch );
	    send_to_char( "If you can't be pked, you can't pk yourself.\n\r", ch );
	    return;
        }
        if ( xIS_SET( victim->act, PLR_KILLER ) )//&& unequal_rank( ch, victim ) )
        {
	    set_char_color( AT_MAGIC, ch );
	    send_to_char( "An aura of protection surrounds them..\n\r", ch );
	    return;
        }

        if ( victim->basepl < 1000000 )
        {
	    set_char_color( AT_MAGIC, ch );
	    send_to_char( "An aura of protection surrounds your prey...\n\r", ch );
	    return;
        }

        if ( rank_dif( ch, victim ) == 0 )
	    ch->pktimer2 = ( 10 * 60 );
        if ( rank_dif( ch, victim ) > 0 )
	    ch->pktimer2 = ( ( 10 * 60 ) * rank_dif( ch, victim ) );
    }
     
    if ( is_safe( ch, victim, TRUE ) )
	return;

    WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
    ch->renzoku = FALSE;
    if ( victim->perk[PERK_FORWARD_PLANNING] && victim->hit > victim->max_hit * 0.5 && !IS_AFFECTED(victim,AFF_LSSJ))
	victim->hit = victim->max_hit;

    if ( !IS_NPC(victim) )
    {
        sprintf( buf, "%s was attacked by %s, %s", victim->name, IS_NPC( ch ) ? ch->short_descr : ch->name, ch->in_room->area->planet ? ch->in_room->area->planet : ch->in_room->area->name );
	talk_info( AT_LBLUE, buf );
    }

    for ( slave = ch->in_room->first_person; slave; slave = slave->next_in_room )
    {
	if ( !slave )
	    break;
        if ( slave->master == ch && IS_NPC(slave) )
	{
	  do_say( slave, "I will assist my master in this task." );	
	  multi_hit( slave, victim, TYPE_UNDEFINED );
	}
    }

    for ( slave = ch->in_room->first_person; slave; slave = slave->next_in_room )
    {
	if ( ch->race == RACE_WIZARD && !IS_NPC(ch) && ch->pcdata->petname && !str_cmp( ch->pcdata->petname, slave->name ) )
        {
            multi_hit(slave, victim, TYPE_UNDEFINED );
        }
    }


    for ( slave = ch->in_room->first_person; slave; slave = slave->next_in_room )
    {
	if ( !slave )
	    break;

	if ( IS_NPC(slave) && slave->master == victim )
	{
	    do_say( slave, "I will defend my master." );	
            multi_hit( slave, ch, TYPE_UNDEFINED );
	}

	if ( slave->covering == NULL )
	    continue;

        if ( !str_cmp( slave->covering, victim->name ) )
        {
	    if ( can_use_skill( slave, number_percent(), gsn_protect ) && !IS_NPC(slave) )
	    {
	        sprintf( buf, "Run, %s!  I'll protect you!", slave->covering );
	        do_wartalk( slave, buf );
	        learn_from_success( slave, gsn_protect );
	        multi_hit( ch, slave, TYPE_UNDEFINED );
	        return;
	    }
	    else
		learn_from_failure( slave, gsn_protect );
        }
    }
    change_align( ch, victim, TRUE );

    if ( !IS_SET(ch->in_room->area->flags, AFLAG_LAWFUL) )
        law_broken(ch, 9);
    else
        law_broken(ch, 10);
    ch->skill_timer = 0;
    multi_hit( ch, victim, TYPE_UNDEFINED );
    return;
}


void do_duel( CHAR_DATA *ch, char *argument )
{
//    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;

    argument = one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Duel whom?\n\r", ch );
	return;
    }

	if ( !str_cmp( arg, "Partner" ) )
	{
		argument = one_argument( argument, arg2 );
		if ( arg2==NULL || arg2[0] == '\0' )
		{
			pager_printf ( ch, "You must designate a partner.\n\r" );
			return;
		}
		ch->partner = STRALLOC ( arg2 );
		pager_printf ( ch, "Now accepting duels from %s\n\r", arg2 );
		if ( ( victim = get_char_room ( ch, arg2 ) ) == NULL )
			pager_printf( victim, "%s is now accepting duels with you.\n\r", ch->name );
		return;
	}

    if ( ( victim = get_char_room( ch, arg ) ) == NULL /*|| victim->level > 50 || ch->level > 50*/)
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( IS_NPC(victim) || IS_NPC(ch) )
    {
	send_to_char( "They don't seem interested in dueling\n\r", ch );
    return;
    }

    if ( !IS_SET(ch->in_room->room_flags, ROOM_DUEL ))
    {
	send_to_char( "This isn't the place for dueling!\n\r", ch );
    return;
    }

    if ( victim == ch )
    {
	send_to_char( "Duel yourself?  Maybe you should be in a gtrain chamber....\n\r", ch );
	return;
    }

    if ( ch->hit < ch->max_hit || victim->hit < victim->max_hit )
    {
	send_to_char( "You both need to be at full health to duel.\n\r", ch );
	return;
    }

    if ( is_safe( ch, victim, TRUE ) )
	return;

    if ( ( victim->currpl * 100 < ch->currpl || ch->currpl * 100 < victim->currpl )&& ( ch->race == RACE_SAIYAN && victim->race == RACE_SAIYAN ) )
	{
		pager_printf( ch, "There'd be nothing to gain from such a fight.\n\r" );
		return;
	}

    if ( victim->partner && ( ( str_cmp( victim->partner, ch->name ) &&  str_cmp( victim->partner, "all" )) || !str_cmp( victim->partner, "none" )) )
    {
	pager_printf ( ch, "They need to designate you as a duelling partner first, using the 'duel partner <name>' command.\n\r" );
	return;
    }

	if ( (victim->race == RACE_TUFFLE || ch->race == RACE_TUFFLE) && ch->race != victim->race )
	{
		send_to_char( "Duel a non-Tuffle?!  BLASPHEMY!\n\r", ch );
		return;
	}

    if ( ch->position == POS_FIGHTING 
       || ch->position ==  POS_MUTANT
       || ch->position ==  POS_TURTLE
       || ch->position ==  POS_ANCIENT
       || ch->position ==  POS_SAIYAN
       )
    {
	send_to_char( "You already are!\n\r", ch );
	return;
    }
    if ( ch->dueltimer > 0 )
    	{
	send_to_char( "You have proved your combat abilities recently enough.\n\r", ch );
	return;
	}
    if ( victim->dueltimer > 0 )
    	{
	send_to_char( "They have proved your combat abilities recently enough.\n\r", ch );
	return;
	}
    ch->dueltimer = 20;
    victim->dueltimer = 20;
    ch->duelinactivitytimer = 0;
    victim->duelinactivitytimer = 0;
/*	pager_printf( ch, "You say 'It's time to D-D-D-D-D-D-D-Duel, %s!'\n\r", victim->name );
	sprintf( buf, "%s says 'It's time to D-D-D-D-D-D-D-Duel, %s!", ch->name, victim->name );
	act( AT_GREEN, buf, ch, NULL, NULL, TO_CANSEE );*/
    WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
    multi_hit( ch, victim, TYPE_UNDEFINED );
    return;
}


/*
 * Check to see if the player is in an "Arena".
 */
bool in_arena( CHAR_DATA *ch )
{
/*    if ( IS_SET(ch->in_room->room_flags, ROOM_ARENA) )
	return TRUE;
    if ( IS_SET(ch->in_room->area->flags, AFLAG_FREEKILL) )
	return TRUE;
    if ( ch->in_room->vnum >= 29 && ch->in_room->vnum <= 43 )
	return TRUE;*/
    if ( !str_cmp(ch->in_room->area->filename, "arena.are") )
	return TRUE;

    return FALSE;
}

bool check_illegal_pk( CHAR_DATA *ch, CHAR_DATA *victim )
{
  char buf[MAX_STRING_LENGTH];
  char buf2[MAX_STRING_LENGTH];

  if ( !IS_NPC(victim) && !IS_NPC(ch) )
  {
	if ( ( !IS_SET(victim->pcdata->flags, PCFLAG_DEADLY)
	|| ch->level - victim->level > 10 
	|| !IS_SET(ch->pcdata->flags, PCFLAG_DEADLY) ) 
	&& !in_arena(ch)
	&& ch != victim 
	&& !( IS_IMMORTAL(ch) && IS_IMMORTAL(victim) ) )
	{
	    if ( IS_NPC(ch) )
		sprintf(buf, " (%s)", ch->name);
	    if ( IS_NPC(victim) )
		sprintf(buf2, " (%s)", PERS( victim, ch ));

	    sprintf( log_buf, "&p%s on %s%s in &W***&rILLEGAL PKILL&W*** &pattempt at %d",
		(lastplayercmd),
		(IS_NPC(victim) ? victim->short_descr : PERS( victim, ch )),
		(IS_NPC(victim) ? buf2 : ""),
		victim->in_room->vnum );
	    last_pkroom = victim->in_room->vnum;
	    log_string(log_buf);
	    to_channel( log_buf, CHANNEL_MONITOR, "Monitor", LEVEL_IMMORTAL );
	    return TRUE;
	}
    }
    return FALSE;
}


void do_flee( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *was_in;
    ROOM_INDEX_DATA *now_in;
    char buf[MAX_STRING_LENGTH];
    int attempt;   
    unsigned long long los = 0;
    sh_int door;
    EXIT_DATA *pexit;
    CHAR_DATA *victim = who_fighting(ch);
    int exits=0;

    if ( !victim )
    {
	if ( ch->position == POS_FIGHTING
	||   ch->position == POS_MUTANT
        ||   ch->position == POS_TURTLE
        ||   ch->position == POS_ANCIENT
        ||   ch->position == POS_SAIYAN )
	{
	    ch->position = POS_STANDING;
	}
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    if ( IS_AFFECTED( ch, AFF_BERSERK ) ) 
    {
        send_to_char( "Flee while berserking?  You aren't thinking very clearly...\n\r", ch);
        return;
    }
    if ( IS_AFFECTED( ch, AFF_LSSJ ) ) 
    {
        send_to_char( "Flee?  What a strange sounding word...\n\r", ch);
        return;
    }

    if ( ch->mana <= 0 ) 
    {
	send_to_char( "You haven't got the energy!\n\r", ch );
	return;
    }
    if ( ( !IS_NPC( victim ) && number_range( 1, 1000 ) > get_curr_lck( ch ) ) || ( IS_NPC(victim ) &&number_range(1,2) == 1 ) )
    {
		send_to_char( "You failed.\n\r", ch );
	        WAIT_STATE( ch, 2 * PULSE_PER_SECOND );
		return;
    }
    /* No fleeing while more aggressive than standard or hurt. - Haus */
    if ( !IS_NPC( ch ) && ch->position < POS_FIGHTING ) {
	send_to_char( "You can't flee while stunned...\n\r", ch );
	return; 
    }

    for ( pexit = ch->in_room->first_exit; pexit; pexit = pexit->next )
	exits++;

    if ( exits == 0 )
    {
	send_to_char( "There are no exits!\n\r", ch );
	return;
    }

    if ( IS_NPC( ch ) && ch->position <= POS_SLEEPING )
	return;
    was_in = ch->in_room;
    for ( attempt = 0; attempt < 8; attempt++ )
    {
	pexit = NULL;
        door = number_door( );

        pexit = get_exit( was_in, door );

        if ( !pexit 
	  || !pexit->to_room 
          || ( IS_SET( pexit->exit_info, EX_CLOSED ) &&!IS_AFFECTED( ch, AFF_PASS_DOOR ) )
	  || ( IS_NPC( ch ) && IS_SET( pexit->to_room->room_flags, ROOM_NO_MOB ) ) )
		continue;
        xREMOVE_BIT  ( ch->affected_by, AFF_SNEAK );
	move_char( ch, pexit, 0 );
	if ( ( now_in = ch->in_room ) == was_in )
	    continue;
	ch->in_room = was_in;
	act( AT_FLEE, "$n flees head over heels!", ch, NULL, NULL, TO_ROOM );

/*
   Weird infinite loop
    if ( ch->genotimer > 0 )
    {
        CHAR_DATA *fch = ch->in_room->first_person;
        CHAR_DATA *next = fch->next_in_room;
	int safety = 0;

        sprintf( log_buf, "Cleaning genocide aggro..." );
        log_string(log_buf);
        for ( fch = ch->in_room->first_person; fch; fch = next)
        {
	    if ( safety > 3 )
		break;
	    if ( fch == victim )
	    {
		safety++;
		continue;
	    }
            if ( IS_NPC(fch) && fch->retran > 1 && fch->retran != fch->in_room->vnum )
            {
		next = fch->next_in_room;
                act( AT_BLOOD, "$n's puts two fingers to $s head and teleports away.", fch, NULL, NULL, TO_ROOM );
                char_from_room( fch );
                sprintf( log_buf, "&p%s returning home to room %d after genocide flee.",
                        (IS_NPC(fch) ? fch->short_descr : PERS( fch, ch )), fch->retran );
                log_string(log_buf);

                char_to_room( fch, get_room_index(fch->retran) );
                fch->retran = 1;
            }
        }
    }
*/
	ch->in_room = now_in;
	act( AT_FLEE, "$n glances around for signs of pursuit.", ch, NULL, NULL, TO_ROOM );

	if ( IS_NPC(victim) && victim->pIndexData->vnum >= 20 && victim->pIndexData->vnum <= 24 )
	{
	    char_from_room( victim );
	    char_to_room( victim, get_room_index( 6 ) );
	}

	if ( !IS_NPC( ch ) )
	{
	    CHAR_DATA *wf = who_fighting( ch );
	    act( AT_FLEE, "You flee head over heels from combat!", ch, NULL, NULL, TO_CHAR );
 	    if ( !IS_NPC(wf))
	        los = ch->basepl / 1000;
	    else
	        los = sqrt(ch->basepl);
	    if ( los >= 0 && !IS_AFFECTED(ch, AFF_SCARED) )
	    {
	      sprintf( buf, "Damn it all!  You've lost %s fighting power!", format_pl(los) );
 	      act( AT_FLEE, buf, ch, NULL, NULL, TO_CHAR );
	      ch->basepl -= los;
	      update_current( ch, NULL );
	    }
	    if ( wf && ch->pcdata->deity )
	    {
	      int level_ratio = URANGE( 1, wf->level / ch->level, 50 );

	      if ( wf && wf->race == ch->pcdata->deity->npcrace )
		adjust_favor( ch, 1, level_ratio );
   	      else
		if ( wf && wf->race == ch->pcdata->deity->npcfoe )
		  adjust_favor( ch, 16, level_ratio );
		else
		  adjust_favor( ch, 0, level_ratio );
	    }
	}
	stop_fighting( ch, TRUE );
	return;
    }
    act( AT_FLEE, "You attempt to flee from combat but can't escape!", ch, NULL, NULL, TO_CHAR );
    return;
}


void do_sla( CHAR_DATA *ch, char *argument )
{
    send_to_char( "If you want to SLAY, spell it out.\n\r", ch );
    return;
}


void do_slay( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];

    argument = one_argument( argument, arg );
    one_argument( argument, arg2 );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Slay whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_world( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( ch == victim )
    {
	send_to_char( "Suicide is a mortal sin.\n\r", ch );
	return;
    }

    if ( !IS_NPC(victim) && ch->level < 62 )
    {
	send_to_char( "You failed.\n\r", ch );
	return;
    }

    if ( !str_cmp( arg2, "immolate" ) )
    {
      act( AT_FIRE, "Your fireball turns $N into a blazing inferno.",  ch, NULL, victim, TO_CHAR    );
      act( AT_FIRE, "$n releases a searing fireball in your direction.", ch, NULL, victim, TO_VICT    );
      act( AT_FIRE, "$n points at $N, who bursts into a flaming inferno.",  ch, NULL, victim, TO_NOTVICT );
    }

    else if ( !str_cmp( arg2, "shatter" ) )
    {
      act( AT_LBLUE, "You freeze $N with a glance and shatter the frozen corpse into tiny shards.",  ch, NULL, victim, TO_CHAR    );
      act( AT_LBLUE, "$n freezes you with a glance and shatters your frozen body into tiny shards.", ch, NULL, victim, TO_VICT    );
      act( AT_LBLUE, "$n freezes $N with a glance and shatters the frozen body into tiny shards.",  ch, NULL, victim, TO_NOTVICT );
    }

    else if ( !str_cmp( arg2, "demon" ) )
    {
      act( AT_IMMORT, "You gesture, and a slavering demon appears.  With a horrible grin, the",  ch, NULL, victim, TO_CHAR );
      act( AT_IMMORT, "foul creature turns on $N, who screams in panic before being eaten alive.",  ch, NULL, victim, TO_CHAR );
      act( AT_IMMORT, "$n gestures, and a slavering demon appears.  The foul creature turns on",  ch, NULL, victim, TO_VICT );
      act( AT_IMMORT, "you with a horrible grin.   You scream in panic before being eaten alive.",  ch, NULL, victim, TO_VICT );
      act( AT_IMMORT, "$n gestures, and a slavering demon appears.  With a horrible grin, the",  ch, NULL, victim, TO_NOTVICT );
      act( AT_IMMORT, "foul creature turns on $N, who screams in panic before being eaten alive.",  ch, NULL, victim, TO_NOTVICT );
    }

    else if ( !str_cmp( arg2, "pounce" ) )
    {
      act( AT_BLOOD, "Leaping upon $N with bared fangs, you tear open $S throat and toss the corpse to the ground...",  ch, NULL, victim, TO_CHAR );
      act( AT_BLOOD, "In a heartbeat, $n rips $s fangs through your throat!  Your blood sprays and pours to the ground as your life ends...", ch, NULL, victim, TO_VICT );
      act( AT_BLOOD, "Leaping suddenly, $n sinks $s fangs into $N's throat.  As blood sprays and gushes to the ground, $n tosses $N's dying body away.",  ch, NULL, victim, TO_NOTVICT );
    }
 
    else if ( !str_cmp( arg2, "slit" ) )
    {
      act( AT_BLOOD, "You calmly slit $N's throat.", ch, NULL, victim, TO_CHAR );
      act( AT_BLOOD, "$n reaches out with a clawed finger and calmly slits your throat.", ch, NULL, victim, TO_VICT );
      act( AT_BLOOD, "$n calmly slits $N's throat.", ch, NULL, victim, TO_NOTVICT );
    }

    else if ( !str_cmp( arg2, "dog" ) )
    {
      act( AT_BLOOD, "You order your dogs to rip $N to shreds.", ch, NULL, victim, TO_CHAR );
      act( AT_BLOOD, "$n orders $s dogs to rip you apart.", ch, NULL, victim, TO_VICT );
      act( AT_BLOOD, "$n orders $s dogs to rip $N to shreds.", ch, NULL, victim, TO_NOTVICT );
    }

    else
    {
      act( AT_IMMORT, "You slay $N in cold blood!",  ch, NULL, victim, TO_CHAR    );
      act( AT_IMMORT, "$n slays you in cold blood!", ch, NULL, victim, TO_VICT    );
      act( AT_IMMORT, "$n slays $N in cold blood!",  ch, NULL, victim, TO_NOTVICT );
    }

    set_cur_char(victim);
    do_say( victim, "Arrrrrrrrgh!" );
    raw_kill( ch, victim );
    return;
}


void rank_killer ( CHAR_DATA *ch )
{
 char buf[MAX_STRING_LENGTH];

 return;

 if ( IS_NPC(ch) || ch->level > 10)      /* Low limit... for now */
    return;

 switch ( ch->pcdata->pkills )
 {
 case 1:
 send_to_char( "You are filled with a sense of pride as you notice small animals around you cower...", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &COne Hit Wonder&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 3;
 break;

 case 5:
 send_to_char( "You are filled with a sense of pride as you notice all the animals around you cower...", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CFamily Destroyer&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 4;
 break;

 case 25:
 send_to_char( "You are filled with a sense of pride as you notice weaker people run and hide from you...", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CSkilled Fighter&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 5;
 break;

 case 50:
 send_to_char( "You are filled with a sense of pride as you notice people run as fast as they can from you!", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CDemi Hunter&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 6;
 break;

 case 80:
 if ( ch->alignment > 0 )
    send_to_char( "The skies open, and a bright light envelops you as you feel full of pride!", ch );
 else
    send_to_char( "The skies open, and a dark energy envelops you as you feel full of pride!", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CAdvanced Hunter&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 7;
 break;

 case 110:
 send_to_char( "Thunder and lighting rumbles in the distance as you begin to feel unstopable...", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CSupreme Bounty Hunter&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 8;
 break;

 case 150:
 send_to_char( "You glow with an awesome red light as you realise how strong you are...", ch );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CUltimate Judge&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 9;
 break;

 case 200:
 pager_printf( ch, "The earth shakes as you roar and are covered with sparks of %s lightning", ch->energycolour );
 sprintf( buf, "&R[&WWARNING&R]&G -&Y %s has reached the level of &CServant of the Kais&Y!", ch->name );
 echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
 ch->level = 10;
 break;
 }
return;
}

void exclusive_echo( CHAR_DATA *ch, ROOM_INDEX_DATA *room, char *argument )
{
 CHAR_DATA *vic;

 for ( vic = room->first_person; vic; vic = vic->next_in_room )
 {
    if ( vic != ch )
    {
    	send_to_char( argument, vic );
    	send_to_char( "\n\r",   vic );
    }
 }
}

void bioskills( CHAR_DATA *ch, CHAR_DATA *victim )
{
 int ctr1  = 0;
 int which = 0;

 if ( ch->race != RACE_BIOANDROID )
    return;
 if ( IS_NPC(ch) )
    return;

 if ( !IS_NPC(victim) )
 {
    for( ; ; )
    {
       ctr1++;
       which = number_range(1, 150 );
       if ( ( victim->pcdata->set[which] == TRUE )
         && ( ch->pcdata->learned[which] < skill_table[which]->race_adept[ch->race] )
         && ( skill_table[which]->type == SKILL_SKILL )
         && ( skill_table[which]->form != TRUE )
         && ( ch->basepl > skill_table[which]->race_level[ch->race] )
         && ( skill_table[which]->name ))
              {
	 	 if ( ch->pcdata->learnsn != 0 )
		    which = ch->pcdata->learnsn;  //Learn skill  -Zeno

                  pager_printf( ch, "You shudder with excitement as you extract the knowledge needed to perform '%s'!\n\r", skill_table[which]->name );
                  ch->pcdata->learned[which] += number_range( 1, 10 );
                  if ( ch->pcdata->learned[which] >= skill_table[which]->race_adept[ch->race] )
                     {
                     pager_printf( ch, "Finally you have mastered '%s'!\n\r", skill_table[which]->name );
                     ch->pcdata->learned[which] = skill_table[which]->race_adept[ch->race];
                     }
		  break;
              }
              if ( ctr1 > 10 )
                  break;
    }
 }
 else if ( number_range(1, 4 ) == 3 )
 {
    for( ; ; )
    {
       ctr1++;
       which = number_range(1,150);
       if ( ch->pcdata->learnsn != 0 )
            which = ch->pcdata->learnsn;  //Learn skill  -Zeno
	if ( !skill_table[which] )
		break;
       if ( ( victim->basepl > skill_table[which]->race_level[victim->race] )
         && ( ch->pcdata->learned[which] < skill_table[which]->race_adept[ch->race] )
         && ( skill_table[which]->type == SKILL_SKILL )
         && ( skill_table[which]->form != TRUE )
         && ( ch->basepl > skill_table[which]->race_level[ch->race] )
         && ( skill_table[which]->name ) 
         && ( ch->pcdata->learnsn != 0 && ch->pcdata->learnsn == which ))
         {
              pager_printf( ch, "You shudder with excitement as you extract the knowledge needed to perform '%s'!\n\r", skill_table[which]->name );
              ch->pcdata->learned[which] += number_range( 2, 20 );
              if ( ch->pcdata->learned[which] >= skill_table[which]->race_adept[ch->race] )
                  {
                  pager_printf( ch, "Finally you have mastered '%s'!\n\r", skill_table[which]->name );
                  ch->pcdata->learned[which] = skill_table[which]->race_adept[ch->race];
                  }
       	      break;
              }
       if ( ctr1 > 100 )
          break;
     }
   }

 return;
}

void emotion_plus( CHAR_DATA *ch, CHAR_DATA *victim, int dam )
{
 if ( ch->race == RACE_SAIYAN )
	ch->rage += number_range(1, 3);
 if ( ch->race == RACE_HALFBREED )
        ch->rage += number_range(1, 2);
 if ( ch->rage > 100 ) ch->rage = 100;
 if ( ch->race == RACE_DEMON && dam <= ( 30 + (ch->plmod/100) ))
 {
	ch->steam++;
	steam_transform( ch );
 }
 if ( ch->steam > 100 ) ch->steam = 100;
 if ( victim->steam < 0 ) victim->steam = 0;
return;
}


unsigned long long correctpl( CHAR_DATA *ch, unsigned long long inc )
{
//	if ( ch->plmod > 100 )
//		inc = ( inc / 100 ) * ( 101 - ch->plmod );
	return inc;
}
/*
bool cant_gain( CHAR_DATA *ch )
{
	if ( ch->basepl <= 1000000 )
}
*/


void absorb( CHAR_DATA *ch, CHAR_DATA *victim )
{
     if (!ch->pcdata->set[gsn_absorb] || (ch->absorbs[0]+ch->absorbs[1]+ch->absorbs[2]+ch->absorbs[3]+ch->absorbs[4]+ch->absorbs[5]+ch->absorbs[6]+ch->absorbs[7]) >= 255 )
	return;
     pager_printf( ch, "Sensing %s's weakened state, you release a part of your putty-like body to envelop %s and use %s powers for yourself!\n\r", victim->name, victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "his" );
     pager_printf( victim, "In your weakened state, you are no match for %s's mighty absorbtion technique.  Soon all you can see is pink putty, and there is no way out...\n\r", ch->name );
     if ( IS_AFFECTED( victim, AFF_MYSTIC ) || ( ch->race == RACE_HALFBREED && ch->plmod >= 3500 ))
     {
        pager_printf ( ch, "&CHmm... the powers of a mystic.  The essence of the universe, and the purpose of the Kaio race, seems clear to you now, as your mind clears and fills with transcendental ideas and understanding.\n\r" );
        ch->absorbs[0]++;
     }
/*     else if ( IS_AFFECTED( victim, AFF_SSJ )   || IS_AFFECTED( victim, AFF_USSJ )   || IS_AFFECTED( victim, AFF_SSJ2 ) 
                   || IS_AFFECTED( victim, AFF_SSJ3 )  || IS_AFFECTED( victim, AFF_SSJ4 )
                   || IS_AFFECTED( victim, AFF_SSJ5 ) )*/
     else if ( ch->race == RACE_SAIYAN && ch->plmod >= 1200 )
     {
        pager_printf( ch, "&YAh... the powers of a Super Saiyan.  So this is what Vegeta was always going on about...\n\r" );
        ch->absorbs[1]++;
     }
     else if ( victim->race == RACE_ANDROID )
     {
        // Androids: Armour and a little STR
        pager_printf( ch, "Hmm... you seem to have gotten tougher skin... This will be handy for blocking blows.\n\r" );
        ch->absorbs[7]++;
     }
     else if ( victim->race == RACE_HALFBREED && IS_AFFECTED( victim, AFF_SSJ3 ) )
     {
        pager_printf( ch, "&PWoah!  Dude!  Fusion powers are, like, totally hot!  You can do like ANYTHING!  And you put exclamation marks on everything you say!  Even when you don't want to!\n\r" );
        ch->absorbs[2]++;        
     }
     else if ( victim->race == RACE_NAMEK )
     {
        pager_printf( ch, "&GHmm... Your skin has become a darker, greener colour, and you feel more intelligent and wise...  It's not quite like absorbing a mystic though..." );
        ch->absorbs[3]++;        
     }
     else if ( victim->race == RACE_DEMON )
     {
        if ( victim->alignment < -333 )
        {
                pager_printf( ch, "&pAh, this is familiar.  Majin powers are a small boost to everything.\n\r" );
                ch->absorbs[4]++;
        }
        else if ( victim->alignment > 333 )
        {
                pager_printf( ch, "&PEUGH!! Everything's become pink!  Fatter!  Weaker!!  And... What's that word?  B... b... BUU!!!\n\r" );
                ch->absorbs[5]++;
        }       
     }
     else
     {
         pager_printf( ch, "&WYawn... another kill, another absorbtion.  Same old, same old...\n\r" );
         ch->absorbs[6]++;                                                      
     }
     return;     
}

/*

Mystics              0
Super Saiyans        1
Fuses                2
Super Nameks         3
Good Demons          4
Evil Demons          5
Other                6

*/


void majin( CHAR_DATA *ch, char * argument )
{
ch->position = POS_FIGHTING;
xREMOVE_BIT( ch->affected_by, AFF_SSJ2 );
xSET_BIT( ch->affected_by, AFF_MAJIN    );
ch->hit  = ch->max_hit / 4;  // Else they'll just drop dead
ch->mana = ch->max_hit;      // Fairness
ch->plmod     = 2300;
ch->multi_str = 200;
ch->multi_int = 200;
ch->multi_wis = 200;
ch->multi_dex = 200;
ch->multi_con = 200;
ch->multi_cha = 200;
ch->multi_lck = 200;
pager_printf( ch, "&RA voice screams out in your head, and you feel your every action being taken over!\n\r" );
act( AT_BLOOD, "$n collapses to the ground, then throws back $s head, screaming with pain, as an 'M' appears on $s forehead!\n\r",ch, NULL, NULL, TO_CANSEE );
update_current( ch, NULL );
}

void finalexplosion( CHAR_DATA *ch, char * argument )
{
    CHAR_DATA *vict;

    //Bugfix by Zeno
    if ( !ch )
    {
        bug( "%s: NULL ch.", __FUNCTION__ );
        return;
    }

    if ( (vict = who_fighting( ch )) == NULL )
    {
        bug( "%s: NULL vict.", __FUNCTION__ );
        return;
    }
    ch->position = POS_FIGHTING;
    pager_printf( ch, "You collapse to your knees, then fly up in the air, sending ki flying from your body all over the place!  When you feel fully charged, you explode with extreme force!\n\r" );
    //do_wartalk( ch, "I'm gonna blow you to bits!  BITE ME!" );
    learn_from_success(ch, gsn_decadentflare);
    act( AT_BLOOD, "$n explodes in ki, cascading torrents of energy from all over $s body, then explodes with extreme force!\n\r", ch, NULL, NULL, TO_CANSEE );
    stop_fighting( ch, TRUE );
    ch->hit = -1; //Make sure raw_kill doesn't call this function again
    vict->perm_str -= UMIN(vict->perm_str * 0.005,30);
    vict->perm_dex -= UMIN(vict->perm_dex * 0.005,30);
    vict->perm_con -= UMIN(vict->perm_con * 0.005,30);
    do_say( vict, "Arrrrrrrrgh!" );
    raw_kill( ch, vict );
    raw_kill( ch, ch );
}


bool unequal_rank( CHAR_DATA *ch, CHAR_DATA *victim ) 
{
short cha;
short vic;

unsigned long long weak        =  1000000;
unsigned long long fighter     =  10000000;
unsigned long long warrior     =  100000000;
unsigned long long master      =  100000000;
unsigned long long bloodthirsty = 100000000;
unsigned long long warlover    =  100000000;
unsigned long long disruptor   =  100000000;
unsigned long long battlefreak =  100000000;
master *= 10;
bloodthirsty *= 100;
warlover *= 1000;
disruptor *= 10000;
battlefreak *=100000;

	

// Simplest way: Get rank for each, see if different.
if ( ch->basepl < weak )
cha = 1;
else if ( ch->basepl < fighter )
cha = 2;
else if ( ch->basepl < warrior )
cha = 3;
else if ( ch->basepl < master )
cha = 4;
else if ( ch->basepl < bloodthirsty )
cha = 5;
else if ( ch->basepl < warlover )
cha = 6;
else if ( ch->basepl < disruptor )
cha = 7;
else if ( ch->basepl < battlefreak )
cha = 8;
else
cha = 9;

if ( victim->basepl < weak )
vic = 1;
else if ( victim->basepl < fighter )
vic = 2;
else if ( victim->basepl < warrior )
vic = 3;
else if ( victim->basepl < master )
vic = 4;
else if ( victim->basepl < bloodthirsty )
vic = 5;
else if ( victim->basepl < warlover )
vic = 6;
else if ( victim->basepl < disruptor )
vic = 7;
else if ( victim->basepl < battlefreak )
vic = 8;
else
vic = 9;


if ( cha != vic )
 return TRUE;

return FALSE;
}


void do_protect ( CHAR_DATA *ch, char * argument )
{
	CHAR_DATA *ally;

	if ( !argument || argument == NULL || argument[0] == '\0' )
	{
		pager_printf( ch, "Protect who?\n\r" );
		return;
	}

	ally = get_char_room ( ch, argument );
	if ( !ally )
	{
		pager_printf( ch, "They are not here.\n\r" );
		return;
	}

	if ( IS_NPC( ally ) && !IS_NPC( ch ) )
	{
		pager_printf( ch, "Covering mobs?  Uh...\n\r" );
		return;
	}

	if ( ch->basepl <= ally->basepl )	
	{
		pager_printf( ch, "You can only protect people weaker than you are.\n\r" );
		return;
	}

	STRFREE ( ch->covering );
	ch->covering = STRALLOC( ally->name );
	pager_printf( ch, "Now covering %s.\n\r", ch->covering );
	pager_printf( ally, "%s is now covering you.\n\r", ch->name );
	if ( !IS_NPC(ch) && ch->race == RACE_NAMEK && ch->basepl >= skill_table[gsn_sync]->race_level[RACE_NAMEK] && ch->pcdata->learned[gsn_sync] < 10 )
	{
	    pager_printf( ch, "&CThis noble action sparks a notion in your mind.  What if, instead of just protecting the weak, you could combine with the strong and use their power to fight?  You could do so much more... fight so much better... perhaps you should meditate on this.\n\r\n\r&YYou have learned the skill '&WSynchronise&Y'!\n\r" );
	    ch->pcdata->learned[gsn_sync] = 100;
	}
}

void move_room( CHAR_DATA *ch, CHAR_DATA *victim )
{
    EXIT_DATA *pexit = NULL;
    ROOM_INDEX_DATA * room = NULL;
//    int rand = number_range( 1, 10 );
//    int ctr = 0;
    int loops = 0;

//    if ( !IS_NPC(ch) || !IS_NPC(victim) )
//	return;

    if ( number_range( 1, 12 ) != 2 )
	return;

    if ( IS_SET( ch->in_room->room_flags, ROOM_DUEL ) )
	return;
	 
       while( 1 )
       {
            if ( room || loops > 8 )
                break;
            for ( pexit = ch->in_room->first_exit; pexit; pexit = pexit->next )
            {
                if ( (room = pexit->to_room)
                  && !IS_SET( room->room_flags, ROOM_PRIVATE )
                  && !IS_SET( room->room_flags, ROOM_SOLITARY )
                  && !IS_SET( room->room_flags, ROOM_DND )
                  && !IS_SET( pexit->exit_info, EX_CLOSED )
                  && number_range( 1, 2 ) == 1 )
                {
		    char buf[MAX_STRING_LENGTH];
		    sprintf( buf, "&G$n smashes $N away, and chases &R%s &w&Gafter $M!", dir_name[pexit->vdir] );
		    act( AT_GREEN, buf,  ch, NULL, victim, TO_NOTVICT );
		    char_from_room( ch );
		    char_to_room( ch, pexit->to_room );
		    char_from_room( victim );
		    char_to_room( victim, pexit->to_room );
		    loops = 9;
		    switch ( ch->in_room->sector_type )
		    {
			default:
				send_to_char( "^YYou knock your opponent away with that hit!^x^z\n\r", ch );
				send_to_char( "^YThat last hit sent you flying!^z^x\n\r", victim );
			break;
			case SECT_INSIDE:
				send_to_char( "^YYou knock your opponent through some walls!^z^x\n\r", ch );
				send_to_char( "^YYou get hit hard and crash through some walls!^z^x\n\r", victim );
			break;
			case SECT_CITY:
        		        send_to_char( "^YYou knock your opponent flying!^z^x\n\r", ch );
		                send_to_char( "^YYou get hit hard and get sent flying!^z^x\n\r", victim );
		        break;	
			case SECT_AIR:
		                send_to_char( "^YYou smash your oppenent hard and chase after them!^z^x\n\r", ch );
                		send_to_char( "^YYou get smashed good and hard!^z^x\n\r", victim );
		        break;
			case SECT_MOUNTAIN:
		                send_to_char( "^YThat hit sent them through a mountain!^z^x\n\r", ch );
		                send_to_char( "^YYou get knocked through a mountain!^z^x\n\r", victim );
		        break;
			case SECT_UNDERGROUND:
		                send_to_char( "^YYou knock them off the walls!^z^x\n\r", ch );
		                send_to_char( "^YYou get knocked flying and ricochet off the walls!^z^x\n\r", victim );
		        break;
			case SECT_UNDERWATER:
		                send_to_char( "^YYou knock your opponent away in a plume of bubbles!^z^x\n\r", ch );
		                send_to_char( "^YYou get knocked away in a plume of bubbles!^z^x\n\r", victim );
		        break;
			case SECT_WATER_NOSWIM:
		                send_to_char( "^YYou knock your opponent away in a plume of bubbles!^z^\n\r", ch );
		                send_to_char( "^YYou get knocked away in a plume of bubbles!^z^x\n\r", victim );
		        break;
		    }
                }
                room = NULL;
         }
         loops++;
    }
    return;
}

void do_throw( CHAR_DATA *ch, char * argument )
{
  char        arg1[MAX_INPUT_LENGTH];
  char        arg2[MAX_INPUT_LENGTH];
  OBJ_DATA  * thrown;
  CHAR_DATA * victim;
  int         dam = 0;
  int         hitrate = 0;

  if ( !argument || argument == NULL || argument[0] == '\0' )
  {
       send_to_char( "Throw what?\n\r", ch );
       return;
  }

  argument = one_argument( argument, arg1 );
  argument = one_argument( argument, arg2 );

  thrown = get_obj_carry( ch, arg1 );

  if ( !thrown )
  {
       send_to_char( "You don't have that to throw!\n\r", ch );
       return;
  }

  if ( thrown->pIndexData->vnum == 4413 )
  {
       send_to_char( "There are easier ways to kill someone than with paper-cuts.\n\r", ch );
       return;
  }

  if ( ch->fighting )
       victim = who_fighting( ch );
  else
  {
       victim = get_char_room( ch, arg2 );
       if ( !victim )
       {
          send_to_char( "They are not here.\n\r", ch );
          return;
       }
       if ( ch == victim )
       {
          send_to_char( "Because that makes sense.\n\r", ch );
          return;
       }
  }
   if ( get_timer( ch, TIMER_PKILLED ) > 0 )
   {
       send_to_char( "You have been killed in the last 10 minutes.\n\r", ch);
       return;
   }

  if ( unequal_rank( ch, victim ) || victim->position == POS_SLEEPING )
  {
       send_to_char( "That would hardly be fair now, would it?\n\r", ch );
       return;
  }

  if ( !can_use_skill( ch, number_percent(), gsn_throw ) )
  {
      send_to_char( "You look for a the right moment but you can't "
                    "find one long enough.\n\r", ch );
      learn_from_failure( ch, gsn_throw );
      return;
  }

  learn_from_success( ch, gsn_throw );
  if ( thrown->item_type == ITEM_WEAPON )
     dam = number_range( thrown->value[1], thrown->value[1] * thrown->value[2] );
  else
     dam = 20;

  dam += thrown->weight;
  dam += number_range( ch->perm_str, get_curr_str( ch ) );
  if ( IS_NPC( ch ) )
    hitrate = 80;
  else
    hitrate = ch->pcdata->learned[gsn_throw];

  ch->fakelag = 3;

  hitrate -= thrown->weight;
  hitrate += sqrt( get_curr_dex( ch ) )*0.8;
  hitrate -= sqrt( get_curr_dex( victim ) )*1.1;
  separate_obj( thrown );
  if ( number_percent() < hitrate )     // Success
  {
     if ( number_range( 1, 100 ) < sqrt(get_curr_dex(victim)) )
     {
        pager_printf( ch, "&CYou deftly dive to one side and throw %s&C at %s, but %s just plucks it out of the air!", 
			thrown->short_descr,
                       IS_NPC( victim ) ? victim->short_descr : victim->name,
                       HESHE(victim->sex) );
        pager_printf( victim, "&C%s deftly dives to one side and throw %s&C at you.  You quickly snap it out of the air!",
                       IS_NPC( ch ) ? ch->short_descr : ch->name,
                       thrown->short_descr );
        act( AT_MAGIC, "$n quickly throws something at $N, but $E catches it!", ch, NULL, victim, TO_NOTVICT );
	obj_from_char( thrown );
        if ( victim->carry_number + 1 > can_carry_n( victim ) )
        {
            act( AT_PLAIN, "  You can't carry that many items, and toss it away.", victim, NULL, NULL, TO_CHAR );
	    extract_obj( thrown );
        }
	else
	{
	    obj_to_char( thrown, victim );
	}
        return;
     }

     extract_obj( thrown );
     pager_printf( ch, "&CYou deftly dive to one side and throw %s&C at %s.  It "
                       "hits %s directly!", thrown->short_descr,
                       IS_NPC( victim ) ? victim->short_descr : victim->name,
                       HIMHER( victim->sex ) );
     pager_printf( victim, "&C%s deftly dives to one side and throw %s&C at you.  It "
                       "hits directly!",
                       IS_NPC( ch ) ? ch->short_descr : ch->name,
                       thrown->short_descr );
     act( AT_MAGIC, "$n quickly throws something at $N.", ch, NULL, victim, TO_NOTVICT );
     pager_printf( ch, "  &Y[%s]\n\r", format_pl(dam) );
     pager_printf( victim, "  &Y[%s]\n\r", format_pl(dam) );
     victim->hit -= dam;
     if ( !who_fighting( victim ) )
        do_murder( victim, ch->name );
  }
  else
  {
     extract_obj( thrown );
     pager_printf( ch, "&CYou deftly dive to one side and throw %s&C at %s.  It "
                       "completely misses %s.\n\r", thrown->short_descr,
                       IS_NPC( victim ) ? victim->short_descr : victim->name,
                       HIMHER( victim->sex ) );
     send_to_char( "Something flies past your shoulder.\n\r", victim );
  }
  ch->skill_timer = 10;
  ch->fakelag = 10;
}


void change_align ( CHAR_DATA *ch, CHAR_DATA *victim, bool attack )
{
    float alteration = 0;

    if ( victim->alignment >= 333 )      		// Attacking a good always means align down
    {
	alteration = -1.25;
    }
    else if ( victim->alignment <= -333 )     		// Whoops, you killed an evil.
    {
	if ( ch->alignment <= victim->alignment )  	// But you're more evil.  So to you, they're good.
	    alteration = -0.5;
	else
	    alteration = 0.5;			  	// But you're LESS evil.  So to you, they're even worse.
    }
    else						// They were neutral.
    {
        if ( ch->alignment < victim->alignment )        // But you're more evil.  So to you, they're good.
            alteration = -0.5;
        else
            alteration = 0.5;                           // But you're LESS evil.  So to you, they're even worse.
    }
    if ( ch->alignment < 0 )				// Severity factor
	alteration *= sqrt( sqrt( 0 - ch->alignment ));
    else
	alteration *= sqrt( sqrt( ch->alignment ));
    if ( attack == TRUE )				// Attacking isn't as bad as killing
	alteration /= 10;
    if ( (int) alteration > 3 )
	alteration = 3;
    if ( (int) alteration < -3 )
	alteration = -3;
    if ( (int) alteration == 0 )
	alteration *= 2.55;				// Try to force a _GENTLE_ response if 0.
    if ( alteration == 0.000f && victim->alignment >= -333 )
	alteration = -1;
    else if ( alteration == 0.000f )
	alteration = 1;

    ch->alignment += (int) alteration;
    if ( attack == FALSE && ch->alignment < 333 )	// Dying makes you embittered unless you're pure  
	victim->alignment--;
    if ( victim->alignment >= 1000 )
	victim->alignment = 1000;
    if ( victim->alignment <= -1000 )
	victim->alignment = -1000;
}

void do_target( CHAR_DATA *ch, char *argument )
{
  CHAR_DATA *victim;
  if ( who_fighting( ch ) == NULL )
  {
	send_to_char( "You are not fighting anyone!\n\r", ch );
	return;
  }
  victim = get_char_room( ch, argument );
  if ( !victim )
  {
	ch_printf( ch, "You are not fighting them.\n\r" );
	return;
  }
  if ( who_fighting( victim ) != ch )
  {
	ch_printf( ch, "%s is not in combat with you!\n\r", PERS( ch, victim ) );
	return;
  }
  if ( get_timer( ch, TIMER_RECENTFIGHT ) > 20 )
  {
	ch_printf( ch, "You have switched targets too recently.\n\r" );
	return;
  }
  add_timer( ch,     TIMER_RECENTFIGHT, 30, NULL, 0 );
  WAIT_STATE( ch, 2 * PULSE_PER_SECOND );
  stop_fighting( ch, FALSE );
  multi_hit( ch, victim, TYPE_UNDEFINED );
}
