 /****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.0 (C) 1994, 1995, 1996 by Derek Snider             |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh and Tricops  |~'~.VxvxV.~'~*
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 *		     Color Module -- Allow user customizable Colors.        *
 *                                   --Matthew                              *
 ****************************************************************************/
 

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "mud.h"

int change_color( CHAR_DATA *ch, char *arg2, char *arg3, int orig_color );

void do_color(CHAR_DATA *ch, char *argument)
{
   char arg1[MAX_STRING_LENGTH];
   char arg2[MAX_STRING_LENGTH];
   char arg3[MAX_STRING_LENGTH];
   
   argument = one_argument(argument, arg1);
   argument = one_argument(argument, arg2);
   argument = one_argument(argument, arg3);

   if ( arg1[0] == '\0' )
     {
	set_char_color(AT_BLOOD, ch);
	send_to_char("BLOOD\t\t", ch);
	set_char_color(AT_SAY, ch);
	send_to_char("SAY\t\t", ch);
	set_char_color(AT_TELL, ch);
	send_to_char("TELL\t\t", ch);
	set_char_color(AT_ACTION, ch);
	send_to_char("ACTION\t\t", ch);
	send_to_char("\n", ch);
	set_char_color(AT_IMMORT, ch);
	send_to_char("IMMORT\t\t", ch);
	set_char_color(AT_HITME, ch);
	send_to_char("HITME\t\t", ch);
	set_char_color(AT_MAGIC, ch);
	send_to_char("MAGIC\t\t", ch);
	set_char_color(AT_SOCIAL, ch);
	send_to_char("SOCIAL\t\t", ch);
	send_to_char("\n", ch);
	set_char_color(AT_WARTALK, ch);
	send_to_char("WARTALK\t\t", ch);
	set_char_color(AT_GTELL, ch);
	send_to_char("GTELL\t\t", ch);
	set_char_color(AT_GOSSIP, ch);
	send_to_char("CHAT\t\t", ch);
	set_char_color(AT_YELL, ch);
	send_to_char("YELL\t\t", ch);
	send_to_char("\n", ch);
	set_char_color(AT_SHOUT, ch);
	send_to_char("SHOUT\t\t", ch);
	set_char_color(AT_NORMAL, ch);
	send_to_char("NORMAL\t\t", ch);
	set_char_color(AT_BLINK, ch);
	send_to_char("BLINK\t\t", ch);
	set_char_color(AT_HURT, ch);
	send_to_char("HURT\t\t", ch);
	send_to_char("\n", ch);
	set_char_color(AT_SKILL, ch);
	send_to_char("SKILLS\t\t", ch);
	set_char_color(AT_HIT, ch);
	send_to_char("HIT\t\t", ch);
	set_char_color(AT_HITVICT, ch);
	send_to_char("HITVICT\t\t", ch);
	set_char_color(AT_AUCTION, ch);
	send_to_char("AUCTION\t\t", ch);
	send_to_char("\n", ch);
	set_char_color(AT_SYSTEM, ch);
	send_to_char("SYSTEM\t\t", ch);
	set_char_color(AT_RMNAME, ch);
	send_to_char("ROOMNAME\t", ch);
	set_char_color(AT_PROMPT, ch);
	send_to_char("PROMPT\t\t", ch);
	set_char_color(AT_EXITS, ch);
	send_to_char("EXITS\n", ch);
	set_char_color(AT_SPAM, ch);
	send_to_char("SPAM\t\t", ch);
	set_char_color(AT_WHITE, ch);
	send_to_char("\n", ch);
	send_to_char("\nTo set a color:", ch);
	send_to_char("\n\tCOLOR <color type> [blink]", ch);
	send_to_char("\n\tIf you specify blink the color type will...", ch);
	send_to_char("\n\t(What do you think?)", ch);
	send_to_char("\n\nType: color reset to reset all colors to grey.", ch);
	send_to_char("\n", ch);
     }
   
   if ( !str_cmp("blood", arg1 ) )
     ch->pcdata->C_BLOOD = change_color(ch, arg2, arg3, ch->pcdata->C_BLOOD);
   else if ( !str_cmp("say", arg1 ) )
     ch->pcdata->C_SAY = change_color(ch, arg2, arg3, ch->pcdata->C_SAY);
   else if ( !str_cmp("tell", arg1 ) )
     ch->pcdata->C_TELL = change_color(ch, arg2, arg3, ch->pcdata->C_TELL);
   else if ( !str_cmp("action", arg1 ) )
     ch->pcdata->C_ACTION = change_color(ch, arg2, arg3, ch->pcdata->C_ACTION);
   else if ( !str_cmp("immort", arg1 ) )
     ch->pcdata->C_IMMORT = change_color(ch, arg2, arg3, ch->pcdata->C_IMMORT);
   else if ( !str_cmp("hitme", arg1 ) )
     ch->pcdata->C_HITME = change_color(ch, arg2, arg3, ch->pcdata->C_HITME);
   else if ( !str_cmp("magic", arg1 ) )
     ch->pcdata->C_MAGIC = change_color(ch, arg2, arg3, ch->pcdata->C_MAGIC);
   else if ( !str_cmp("social", arg1 ) )
     ch->pcdata->C_SOCIAL = change_color(ch, arg2, arg3, ch->pcdata->C_SOCIAL);
   else if ( !str_cmp("wartalk", arg1 ) )
     ch->pcdata->C_WARTALK = change_color(ch, arg2, arg3, ch->pcdata->C_WARTALK);
   else if ( !str_cmp("gtell", arg1 ) )
     ch->pcdata->C_GTELL = change_color(ch, arg2, arg3, ch->pcdata->C_GTELL);
   else if ( !str_cmp("chat", arg1 ) )
     ch->pcdata->C_GOSSIP = change_color(ch, arg2, arg3, ch->pcdata->C_GOSSIP);
   else if ( !str_cmp("yell", arg1 ) )
     ch->pcdata->C_YELL = change_color(ch, arg2, arg3, ch->pcdata->C_YELL);
   else if ( !str_cmp("shout", arg1 ) )
     ch->pcdata->C_SHOUT = change_color(ch, arg2, arg3, ch->pcdata->C_SHOUT);
   else if ( !str_cmp("normal", arg1 ) )
     ch->pcdata->C_NORMAL = change_color(ch, arg2, arg3, ch->pcdata->C_NORMAL);
   else if ( !str_cmp("blink", arg1 ) )
     ch->pcdata->C_BLINK = change_color(ch, arg2, arg3, ch->pcdata->C_BLINK);
   else if ( !str_cmp("hurt", arg1 ) )
     ch->pcdata->C_HURT = change_color(ch, arg2, arg3, ch->pcdata->C_HURT);
   else if ( !str_cmp("skills", arg1 ) )
     ch->pcdata->C_SKILLS = change_color(ch, arg2, arg3, ch->pcdata->C_SKILLS);
   else if ( !str_cmp("hit", arg1 ) )
     ch->pcdata->C_HIT = change_color(ch, arg2, arg3, ch->pcdata->C_HIT);
   else if ( !str_cmp("hitvict", arg1 ) )
     ch->pcdata->C_HITVICT = change_color(ch, arg2, arg3, ch->pcdata->C_HITVICT);
   else if ( !str_cmp("auction", arg1 ) )
     ch->pcdata->C_AUCTION = change_color(ch, arg2, arg3, ch->pcdata->C_AUCTION);
   else if ( !str_cmp("system", arg1 ) )
     ch->pcdata->C_SYSTEM = change_color(ch, arg2, arg3, ch->pcdata->C_SYSTEM);
   else if ( !str_cmp("roomname", arg1 ) )
     ch->pcdata->C_RMNAME = change_color(ch, arg2, arg3, ch->pcdata->C_RMNAME);
   else if ( !str_cmp("prompt", arg1 ) )
     ch->pcdata->C_PROMPT = change_color(ch, arg2, arg3, ch->pcdata->C_PROMPT);
   else if ( !str_cmp("exits", arg1 ) )
     ch->pcdata->C_EXITS = change_color(ch, arg2, arg3, ch->pcdata->C_EXITS);
   else if ( !str_cmp("spam", arg1 ) )
     ch->pcdata->C_SPAM = change_color(ch, arg2, arg3, ch->pcdata->C_SPAM);
   else if ( !str_cmp("reset", arg1 ) )
     reset_colors(ch);
}

int change_color( CHAR_DATA *ch, char *arg2, char *arg3, int orig_color )
{
#ifdef DEBUG
   char buf[MAX_STRING_LENGTH];
#endif
   
   int color = 0;
   if ( arg2[0] == '\0' )
     {
	send_to_char("You must type a color!\n", ch);
	send_to_char("\tColors Are:\n", ch);
	set_char_color(AT_BLACK, ch);
	send_to_char("BLACK\t", ch);
	set_char_color(AT_DGREEN, ch);
	send_to_char("DGREEN\t", ch);
	set_char_color(AT_ORANGE, ch);
	send_to_char("ORANGE\t", ch);
	set_char_color(AT_DBLUE, ch);
	send_to_char("DBLUE\t", ch);
	set_char_color(AT_PURPLE, ch);
	send_to_char("PURPLE\n", ch);
	set_char_color(AT_GREY, ch);
	send_to_char("GREY\t", ch);
	set_char_color(AT_DGREY, ch);
	send_to_char("DGREY\t", ch);
	set_char_color(AT_RED, ch);
	send_to_char("RED\t", ch);
	set_char_color(AT_GREEN, ch);
	send_to_char("GREEN\t", ch);
	set_char_color(AT_YELLOW, ch);
	send_to_char("YELLOW\n", ch);
	set_char_color(AT_PINK, ch);
	send_to_char("PINK\t", ch);
	set_char_color(AT_LBLUE, ch);
	send_to_char("LBLUE\t", ch);
	set_char_color(AT_WHITE, ch);
	send_to_char("WHITE\t", ch);
      	set_char_color(AT_CYAN, ch);
	send_to_char("CYAN\t", ch);
      	set_char_color(AT_BLUE, ch);
	send_to_char("BLUE\n", ch);
     }

   if ( !str_cmp("blink", arg3 ) )
     color += COLOR_BLINK;
   
   if ( !str_cmp("black", arg2 ) )
     color += COLOR_BLACK;
   else if (!str_cmp("dgreen", arg2 ) )
     color += COLOR_DGREEN;
   else if (!str_cmp("orange", arg2 ) )
     color += COLOR_ORANGE;
   else if (!str_cmp("dblue", arg2 ) )
     color += COLOR_DBLUE;
   else if (!str_cmp("purple", arg2 ) )
     color += COLOR_PURPLE;
   else if (!str_cmp("grey", arg2 ) )
     color += COLOR_GREY;
   else if (!str_cmp("dgrey", arg2 ) )
     color += COLOR_DGREY;
   else if (!str_cmp("red", arg2 ) )
     color += COLOR_RED;
   else if (!str_cmp("green", arg2 ) )
     color += COLOR_GREEN;
   else if (!str_cmp("yellow", arg2 ) )
     color += COLOR_YELLOW;
   else if (!str_cmp("pink", arg2 ) )
     color += COLOR_PINK;
   else if (!str_cmp("lblue", arg2 ) )
     color += COLOR_LBLUE;
   else if (!str_cmp("white", arg2 ) )
     color += COLOR_WHITE;
   else if (!str_cmp("cyan", arg2 ) )
     color += COLOR_CYAN;
   else if (!str_cmp("blue", arg2 ) )
     color += COLOR_BLUE;

#ifdef DEFUG   
   sprintf(buf, "Arg2: %s\tArg3: %s\tOrig Color: %d\tNew Color: %d\n\n\n", arg2, arg3, orig_color, color);
   set_char_color(AT_WHITE, ch);
   send_to_char(buf, ch);
#endif
   
   if ( color == 0 )
     return orig_color;
   else
     return color;
}
    
void reset_colors(CHAR_DATA *ch)
{
   /* Reset All Char Colors to 7 (GREY) */
     ch->pcdata->C_BLOOD = 7;
     ch->pcdata->C_SAY = 7;
     ch->pcdata->C_TELL = 7;
     ch->pcdata->C_ACTION =7;
     ch->pcdata->C_IMMORT =7;
     ch->pcdata->C_HITME = 7;
     ch->pcdata->C_MAGIC = 7;
     ch->pcdata->C_SOCIAL =7;
     ch->pcdata->C_WARTALK =7;
     ch->pcdata->C_GTELL = 7;
     ch->pcdata->C_GOSSIP =7;
     ch->pcdata->C_YELL = 7;
     ch->pcdata->C_SHOUT =7;
     ch->pcdata->C_NORMAL =7;
     ch->pcdata->C_BLINK = 7;
     ch->pcdata->C_HURT = 7;
     ch->pcdata->C_SKILLS =7; 
     ch->pcdata->C_HIT = 7;
     ch->pcdata->C_HITVICT =7;
     ch->pcdata->C_AUCTION =7;
     ch->pcdata->C_SYSTEM = 7;
     ch->pcdata->C_RMNAME = 7;
     ch->pcdata->C_PROMPT = 7;
     ch->pcdata->C_EXITS = 7;
}
