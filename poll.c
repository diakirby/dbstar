#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"

int YesVotes;
int NoVotes;
bool PollActive;
char *Poll;

void do_poll ( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];

 if ( IS_NPC(ch) || !ch )
       return;

if ( PollActive || PollActive == TRUE )
{
    pager_printf(ch, "Poll already active\n\r" );
    return;
}

if ( !argument )
{
   pager_printf(ch, "Poll for what, exactly?\n\r" );
   return;
}
YesVotes = 0;
NoVotes  = 0;
PollActive = TRUE;
sprintf( buf, "&Y%s runs the poll '&W%s&w&Y' - Vote &GYES &Yor &RNO&Y.", ch->name, argument );
do_echo( ch, buf );
return;
}

void do_vote( CHAR_DATA *ch, char *argument )
{
 char arg[MAX_INPUT_LENGTH];
 char buf[MAX_STRING_LENGTH];
 argument = one_argument( argument, arg );

 if ( ch->votetimer > 0 )
 {
	pager_printf( ch, "You have voted too recently.\n\r" );
	return;
 }

 if ( !PollActive || PollActive == FALSE )
 	return;

 ch->votetimer = 20;

 if ( !str_cmp(arg, "yes") )
 {
   YesVotes++;
   sprintf( buf, "&z[&YPOLL&z] &GYes: &g%d   &w-   &RNo: &r%d", YesVotes, NoVotes );
   echo_to_all( AT_PINK, buf, ECHOTAR_ALL );
   return;
 }

 if ( !str_cmp(arg, "no") )
 {
   NoVotes++;
   sprintf( buf, "&z[&YPOLL&z] &GYes: &g%d   &w-   &RNo: &r%d", YesVotes, NoVotes );
   echo_to_all( AT_PINK, buf, ECHOTAR_ALL );
   return;
 }

 send_to_char( "You must pick an option.\n\r", ch );
 return;
}

void do_closepoll( CHAR_DATA *ch, char *argument )
{
  char buf[MAX_STRING_LENGTH];

  sprintf( buf, "&z[&YPOLL&z]&w Poll closed.  Results: &GYes: &g%d&w, &RNo: &r%d&w.", YesVotes, NoVotes );
  PollActive = FALSE;
  do_echo( ch, buf );
  YesVotes = 0;
  NoVotes = 0;
  return;
}
