#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mud.h"
#include <math.h>

bool switch_bodies( CHAR_DATA *ch, CHAR_DATA *victim );
void steam_transform( CHAR_DATA *ch );
void ubuu( CHAR_DATA *ch );
void superbuu( CHAR_DATA *ch );
void kidbuu( CHAR_DATA *ch );
bool AreaAttack;
void do_rvdb( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 int bonus;

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( ch->plmod < 2000 )
 {
         send_to_char( "You are too weak to use Revenge Deathball.\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 11;
        add_timer( ch, TIMER_DO_FUN, 11, do_rvdb, 1 );
        pager_printf(ch, "&GYou float high into the air and throw your hands up above your head.  A small &zblack&G ball of ki begins to form, and then rapidly increases in size!\n\r" );
        pager_printf(victim, "%s floats high into the air and throws %s arms up above %s head.  A small &zblack&G ball of ki begins to form, and then rapidly inreases in size!\n\r", ch->name, ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her", ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her" );
        act( AT_GREEN, "$n floats high into the air and throws $s hands above $s head.  A small &zblack&G ball of ki begins to form, but then rapidly increases in size!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Revenge Deathball was interupted!\n\r", ch );
          sprintf( buf, "&G%s's Revenge Deathball was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's Revenge Deathball was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your Revenge Deathball.\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Revenge Deathball.\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_rvdb ) )
    {
        learn_from_success( ch, gsn_rvdb );
        pager_printf(ch, "&GYou feel that your attack is now ready to be launched.  Grinning evilly, you scream '&ZR&we&Zv&we&Zn&wg&Ze &wD&Ze&wa&Zt&wh&Zb&wa&Zl&wl&z!&G and guide your black ki ball down on %s!\n\r", victim->name );
        pager_printf(victim, "&GFeeling that the time is right to attack and grinning evilly, %s screams '&ZR&we&Zv&we&Zn&wg&Ze &wD&Ze&wa&Zt&wh&Zb&wa&Zl&wl&z!&G and guides a black ki ball with blue electricty arcing around it down on you!\n\r", ch->name );
        act( AT_GREEN, "$n screams 'Revenge Deathball!' and guides $s huge black ki blast down on $N!",  ch, NULL, victim, TO_NOTVICT );
        bonus = ( number_range( 1000, 2000 ) + ( ch->pdeaths * ch->eggslaid ) );
	if ( bonus > 10000 )
		bonus = 10000;
        global_retcode = damage( ch, victim, bonus, gsn_rvdb );
    }
    else
    {

        learn_from_failure( ch, gsn_rvdb );
        pager_printf(ch, "&GYou feel that your attack is now ready to be launched.  Grinning evilly, you scream '&ZR&we&Zv&we&Zn&wg&Ze &wD&Ze&wa&Zt&wh&Zb&wa&Zl&wl&z!&G and guide your black ki ball down on %s, who makes light work of avoiding it!\n\r", victim->name );
        pager_printf(victim, "&GFeeling that the time is right to attack and grinning evilly, %s screams '&ZR&we&Zv&we&Zn&wg&Ze &wD&Ze&wa&Zt&wh&Zb&wa&Zl&wl&z!&G and guides a black ki ball with blue electricty arcing around it down on you!  You react fast and move out of the way\n\r", ch->name );
        act( AT_GREEN, "$n screams 'Revenge Deathball!' and guides $s huge black ki blast down on $N, but $E just moves out of the way!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_rvdb );
    }
return;
}


void do_lightpillar( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
                  return;
 }

/*if ( !IS_AFFECTED( ch, AFF_META ) || !IS_AFFECTED( ch, AFF_COMBINED_STRENGTH ) )

{
         send_to_char( "You are too weak to use Light Pillar.\n\r", ch );
         return;
 }*/
	 if ( ch->plmod < 2000 )
 {
         send_to_char( "You are too weak to use Light Pillar.\n\r", ch );
         return;
 }
    

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 7;
        add_timer( ch, TIMER_DO_FUN, 7, do_lightpillar, 1 );
        pager_printf(ch, "&GThe pain of your race overwhelms you as you throw back your head and explode with a large %s &Gaura!\n\r", ch->energycolour);
        pager_printf(victim, "%s throws back %s head and explodes in violent %s&G aura!", ch->name, ch->sex == 2 ? "her" : "his", ch->energycolour );
        act( AT_GREEN, "$n looks visibly enraged and throws back $s head, exploding in a huge aura!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Light Pillar was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Light Pillar was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming your Light Pillar!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Light Pillar!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_lightpillar ) )
    {
        learn_from_success( ch, gsn_lightpillar );
        pager_printf(ch, "&GYou scream with anger and a huge %s &Gbeam of ki comes out of the ground, enveloping %s!", ch->energycolour, victim->name );
        pager_printf(victim, "&G%s screams with anger, and a huge beam of ki flies out of the ground and envelops you!\n\r", ch->name );
        act( AT_GREEN, "$n screams with anger and a massive ki beam comes out of the ground and hits $N!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 750, 3500), gsn_lightpillar );
    }
    else
    {

        learn_from_failure( ch, gsn_lightpillar );
        pager_printf(ch, "&GYou scream with anger and a puny little beam of ki comes out of the ground, but it misses %s!\n\r", victim->name );
        pager_printf(victim, "&G%s screams with anger and a puny little beam of ki comes out of the ground, but it misses you!\n\r", ch->name );
        act( AT_GREEN, "$n squeaks with rage and tries to hit $N with a beam of ki, but misses...",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_lightpillar );
    }
return;
}

/*
 *   Independant Will transformation - Tuffle level 1
 */

void do_independantwill( CHAR_DATA *ch, char *argument )
{
// CHAR_DATA vic;
// char buf[MAX_STRING_LENGTH];

if ( ch->is_mystic )
	{
	send_to_char("You're kinda past that already...\n\r", ch );
        return;
	}

if ( IS_AFFECTED( ch, AFF_META ) || IS_AFFECTED( ch, AFF_COMBINED_STRENGTH ) )
{
   	ch->multi_str = 50;
	ch->multi_int = 25;
	ch->multi_wis = 25;
	ch->multi_dex = 50;
	ch->multi_con = 50;
	ch->multi_cha = -25;
	ch->multi_lck = 25;
    if ( IS_AFFECTED( ch, AFF_META ) )
        ch->armor -= 1500;
    else
        ch->armor -= (500 + (ch->eggslaid > 500 ? 500 : ch->eggslaid));
	ch->plmod     = 1500;
	xREMOVE_BIT( ch->affected_by, AFF_META );
	xREMOVE_BIT( ch->affected_by, AFF_COMBINED_STRENGTH );
	xSET_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
	send_to_char("You drop down to your weakest form.  Your shoulder pads disappear with the rest of your armour.\n\r", ch );
    act( AT_GREY, "$n's shoulder pads disappear and $s whole body slumps forwards a little.", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}
if ( IS_AFFECTED( ch, AFF_INDEPENDANT_WILL ) )
{
   	ch->multi_str = 0;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 100;
	xREMOVE_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
	send_to_char("You drop out of your weakest form.  No longer able to hold its structure, your body reverts to it's liquid metal state.\n\r", ch );
    act( AT_GREY, "$n's whole internal structure collapses and $e reverts to $s liquid metal form!", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}

if ( can_use_skill( ch, number_percent(), gsn_independantwill ) )
{
    learn_from_success( ch, gsn_independantwill );
   	ch->multi_str = 50;
	ch->multi_int = 25;
	ch->multi_wis = 25;
	ch->multi_dex = 50;
	ch->multi_con = 50;
	ch->multi_cha = -25;
	ch->multi_lck = 25;
	ch->plmod     = 1500;
	xSET_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
	pager_printf( ch, "&wSick of the way you and your race have been treated, a %s aura begins to grow around you...\n\r", ch->energycolour );
	pager_printf( ch, "&wSlowly you close up into a foetal position and begin to change internally...\n\r" );
	pager_printf( ch, "&wA sudden wave of nausea overcomes you as quickly overtake the nearest host body!\n\r" );
	pager_printf( ch, "&wHaving taken control of someone, you explode with a grey aura and %s lines appear on your face.\n\r", ch->energycolour );
    act( AT_GREY, "$n makes $s move and leaps inside a host body!\n\r", ch, NULL, NULL, TO_CANSEE );
        update_current( ch, NULL );
    return;
}

learn_from_failure( ch, gsn_independantwill );
pager_printf( ch, "You could try that... but chances are you'd be killed as you did it.\n\r");
return;
}

/*
 *   Meta transformation - Tuffle level 2
 */

void do_meta( CHAR_DATA *ch, char *argument )
{
if ( ch->is_mystic )
	{
	send_to_char("You're kinda past that already...\n\r", ch );
    return;
	}

if ( IS_AFFECTED( ch, AFF_COMBINED_STRENGTH ) )
{
   	ch->multi_str = 150;
	ch->multi_int = 125;
	ch->multi_wis = 125;
	ch->multi_dex = 150;
	ch->multi_con = 150;
	ch->multi_cha = 75;
	ch->multi_lck = 125;
    ch->armor    -= (500 + (ch->eggslaid > 500 ? 500 : ch->eggslaid));
	ch->plmod     = 2200;
	xREMOVE_BIT( ch->affected_by, AFF_COMBINED_STRENGTH );
 	xSET_BIT( ch->affected_by, AFF_META );
	send_to_char("You decide that you no longer need the power of other people.\n\rYour muscles density drops and your eyes turn back to a natural colour.\n\r", ch );
    act( AT_GREY, "$n looks arrogant and stretches out a bit.  You feel $s powerlevel decrease.", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}
if ( IS_AFFECTED( ch, AFF_META ) )
{
   	ch->multi_str = 50;
	ch->multi_int = 25;
	ch->multi_wis = 25;
	ch->multi_dex = 50;
	ch->multi_con = 50;
	ch->multi_cha = -25;
	ch->multi_lck = 25;
    ch->armor    -= 1500;
	ch->plmod     = 1500;
	xSET_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
  	xREMOVE_BIT( ch->affected_by, AFF_META );
	send_to_char("You decide that it's time for a change in look.\n\rYou revert back to your original body, keeping only a few of your old features.\n\rYour armour disappears and your shoulders pads go back into your shoulders.", ch );
    act( AT_GREY, "$n looks arrogant and stretches out a bit.  You feel $s powerlevel decrease.", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}

if ( can_use_skill( ch, number_percent(), gsn_meta ) )
{
    learn_from_success( ch, gsn_meta );
   	ch->multi_str = 150;
	ch->multi_int = 125;
	ch->multi_wis = 125;
	ch->multi_dex = 150;
	ch->multi_con = 150;
	ch->multi_cha = 75;
	ch->multi_lck = 125;
        ch->armor     += 1500;
	ch->plmod     = 2200;
        xREMOVE_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
	xSET_BIT( ch->affected_by, AFF_META );
	pager_printf( ch, "&wIt occurs to you that it is time to turn up the heat a little.\n\rYou cast back your arms and head, and armour grows around your body,\n\rand enormous shoulder pads grow from your shoulders.\n\r" );
    act( AT_GREY, "$n casts back $s arms and head, and armour grows around his body, arms and legs!\n\r", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}

learn_from_failure( ch, gsn_meta );
pager_printf( ch, "You strain yourself trying to surpass people's expectations of you, but only fail.\n\r" );
return;
}

/*
 *   Combined Strength transformation - Tuffle level 3
 */

void do_combinedstrength( CHAR_DATA *ch, char *argument )
{
CHAR_DATA *vch, *vch_next = NULL;
if ( ch->is_mystic )
	{
	send_to_char("You're kinda past that already...\n\r", ch );
    return;
	}

if ( IS_AFFECTED( ch, AFF_COMBINED_STRENGTH ) )
{
   	ch->multi_str = 150;
	ch->multi_int = 125;
	ch->multi_wis = 125;
	ch->multi_dex = 150;
	ch->multi_con = 150;
	ch->multi_cha = 75;
	ch->multi_lck = 125;
        ch->armor     -= (500 + (ch->eggslaid > 500 ? 500 : ch->eggslaid));
	ch->plmod     = 2200;
	xSET_BIT( ch->affected_by, AFF_META );
  	xREMOVE_BIT( ch->affected_by, AFF_COMBINED_STRENGTH );
	send_to_char("&wYou tilt your head back a little and grin.  You stop relying on other people for your power.\n\r", ch );
    act( AT_GREY, "$n looks arrogant and grins.  You feel $s powerlevel decrease.", ch, NULL, NULL, TO_CANSEE );
    update_current( ch, NULL );
    return;
}

if ( ch->eggslaid < 20 )
{
   pager_printf( ch, "You need to lay at least %d more eggs first.\n\r", 20 - ch->eggslaid );
   return;
}

if ( can_use_skill( ch, number_percent(), gsn_combinedstrength ) )
{
    learn_from_success( ch, gsn_combinedstrength );
   	ch->multi_str = (ch->eggslaid * 5 > 600 ? 600 : ch->eggslaid * 5);
	ch->multi_int = (ch->eggslaid * 3 > 425 ? 425 : ch->eggslaid * 3);
	ch->multi_wis = (ch->eggslaid * 3 > 425 ? 425 : ch->eggslaid * 3);
	ch->multi_dex = (ch->eggslaid * 5 > 600 ? 600 : ch->eggslaid * 5);
	ch->multi_con = (ch->eggslaid * 5 > 600 ? 600 : ch->eggslaid * 5);
	ch->multi_cha = (ch->eggslaid * 1.5 > 175 ? 175 : ch->eggslaid * 1.5);
	ch->multi_lck = (ch->eggslaid * 3 > 325 ? 325 : ch->eggslaid * 3);
        ch->armor     += (500 + (ch->eggslaid > 500 ? 500 : ch->eggslaid));
	ch->plmod     = 1200 + (100 * ( ch->eggslaid / 2 ));
	if ( ch->plmod > 10000 )
		ch->plmod = 10000;
    xREMOVE_BIT( ch->affected_by, AFF_INDEPENDANT_WILL );
    xREMOVE_BIT( ch->affected_by, AFF_META );
    xSET_BIT( ch->affected_by, AFF_COMBINED_STRENGTH );
    send_to_char("&wYou tense up, your muscles begining to bulge a little.\n\rYour eyes turn a shade of piercing &Bblue&was your muscles begin to seriously bulk up!\n\rFinally you scream in anger, and &zdark&w lightning starts arcing around your body!", ch );
    act( AT_GREY, "$n tenses up, and suddenly $s eyes turn a piercing shade of blue as dark lightning starts arcing around $m!\n\r", ch, NULL, NULL, TO_CANSEE );
/*if ( ch->firstcmb == TRUE )
{*/
        for( vch = first_char; vch; vch = vch_next)
        {
                if ( vch->vasselto == ch->name )
                {
                send_to_char( "You are going to a greater cause...\n\r", ch );
                raw_kill( ch, vch );
                }
        }
//ch->firstcmb = FALSE;
    update_current( ch, NULL );
    return;
}

learn_from_failure( ch, gsn_combinedstrength );
pager_printf( ch, "You search desperately for the power of your vassels, but fail to get a hold on it.\n\r" );
return;
}

void do_shockwavepunch( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg[MAX_STRING_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 4;
        add_timer( ch, TIMER_DO_FUN, 4, do_shockwavepunch, 1 );
        pager_printf(ch, "&GYou kick %s in the chest and pull back your right arm...\n\r", victim->name );
        pager_printf(victim, "&G%s kicks you in the chest and pulls back %s right arm...", ch->name, ch->sex == 2 ? "her" : "his" );
        act( AT_GREEN, "$n kicks $N in the chest and pulls $s right arm back...", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Shockwave Punch was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Shockwave Punch was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming your Shockwave Punch!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Shockwave Punch!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_shockwavepunch ) )
    {
        learn_from_success( ch, gsn_shockwavepunch );
        pager_printf(ch, "&GYou go to punch %s but stop several inches away.  %s, however, goes flying back!", victim->name, victim->sex == 2 ? "She" : "He" );
        pager_printf(victim, "&G%s punches the air in front of you, and you are sent flying!!\n\r", ch->name );
        act( AT_GREEN, "$n punches the air in front of $N!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 75, 125 ), gsn_shockwavepunch );
    }
    else
    {

        learn_from_failure( ch, gsn_shockwavepunch );
        pager_printf(ch, "&GYou go to punch %s but stop several inches away.  %s, however, feels nothing...\n\r", victim->name, victim->sex == 2 ? "She" : "He" );
        pager_printf(victim, "&G%s punches the air in front of you, but you don't even feel anything...\n\r", ch->name );
        act( AT_GREEN, "$n punches the air in front of $N, but nothing happens.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_shockwavepunch );
    }
return;
}

void steam_transform( CHAR_DATA *ch )
{
 unsigned long long ULTIMATE_REQ  =     2500000;
 unsigned long long SUPER_REQ     =   250000000;
 unsigned long long KID_REQ       =  SUPER_REQ * 10;


 if ( ch->race != RACE_DEMON || IS_NPC(ch))
     return;

 if ( ch->steam >= 50 && ch->steam < 55 && ch->plmod < 800 && ch->basepl > ULTIMATE_REQ )
     ubuu( ch );

 if ( ch->steam >= 75 && ch->steam < 80 && ch->plmod < 1500 && ch->basepl > SUPER_REQ )      
     superbuu( ch );

 if ( ch->steam >= 100 && ch->plmod < 2400 && ch->basepl > KID_REQ )
     kidbuu( ch );
 update_current( ch, NULL );
 return;
}

void ubuu( CHAR_DATA *ch )
{
 ch->plmod     = 800;
 ch->multi_str = 100;
 ch->multi_int = 0;
 ch->multi_dex = 100;
 ch->multi_wis = 0;
 ch->multi_con = 100;
 ch->multi_cha = 100;
 ch->multi_lck = 0;
 ch->steam     = 0;
 xSET_BIT( ch->affected_by, AFF_ULTIMATE_BUU );
 pager_printf( ch, "&PSteam begins pouring out of you in great gouts. As the pain becomes unbearable, you litterally begin to tear your self apart to stop it. From the shattered remains of your pink corpse something dark and sinister rises...\n\r" );
 act( AT_PINK, "$n fills the place with steam.  You hear a huge explosion, and when $e reappears, $n is about half $s size!", ch, NULL, NULL, TO_CANSEE );
 return;
}

void superbuu( CHAR_DATA *ch )
{
 ch->plmod     = 1500;
 ch->multi_str = 200;
 ch->multi_int = 25;
 ch->multi_wis = 25;
 ch->multi_dex = 200;
 ch->multi_con = 200;
 ch->multi_cha = 100;
 ch->multi_lck = 50;
 ch->steam     = 0;
 xREMOVE_BIT( ch->affected_by, AFF_ULTIMATE_BUU );
 xSET_BIT( ch->affected_by, AFF_SUPER_BUU );
 pager_printf( ch, "&pYou begin to well up with steam, and thin clouds of the stuff\n\r" );
 pager_printf( ch, "&ppour out of the holes in your body.  Soon you are completely\n\r" );
 pager_printf( ch, "&phidden by %s&P steam, and you double over, screaming in pain.\n\r", ch->energycolour );
 pager_printf( ch, "&pYour putty-like body morphs into a sleeker, stronger shape!\n\r" );
 pager_printf( ch, "&pYour skin turns darker and you now have fingers on your hands!\n\r" );
 act( AT_PINK, "$n fills the place with steam, and when $e reappears $e is a totally different shape!", ch, NULL, NULL, TO_CANSEE );
 return;
}

void kidbuu( CHAR_DATA *ch )
{
 ch->plmod     = 2400;
 ch->multi_str = 300;
 ch->multi_int = 0;
 ch->multi_wis = 300;
 ch->multi_dex = 400;
 ch->multi_con = 500;
 ch->multi_cha = 200;
 ch->multi_lck = 100;
 ch->steam    = 0;
 xREMOVE_BIT( ch->affected_by, AFF_SUPER_BUU );
 xREMOVE_BIT( ch->affected_by, AFF_ULTIMATE_BUU );
 xSET_BIT( ch->affected_by, AFF_KID_BUU );
 pager_printf( ch, "&PYou soon become incapable of controlling your anger any longer.\n\r" );
 pager_printf( ch, "&PYou start releasing steam in large, powerful bursts and quickly\n\r" );
 pager_printf( ch, "&Phide yourself from view... Again you begin to change, and your\n\r" );
 pager_printf( ch, "&Pskin gets much lighter as you gain a much more human-like appearance!\n\r" );
 act( AT_PINK, "$n fills the place with steam, and when $e reappears $e is a much lighter shade of pink!", ch, NULL, NULL, TO_CANSEE );
 return;
}

/* Test time */

void do_twistbeam ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
char arg[MAX_STRING_LENGTH];
int vnum = 0, attempt = 0;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

//if ( !ch->target || ch->target == NULL )

switch( ch->substate )
    {
        default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_twistbeam, 1 );
        pager_printf(ch, "&GYou put your arms out to both sides, forming a &Blilac&G ball of ki in one and a %s&G ball of ki in the other.", ch->energycolour );
        act( AT_GREEN, "$n puts $s arms out to both sides, and begins to form a pair of different coloured ki balls.", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        ch->target = victim;
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your twist beam was interupted!\n\r", ch );
          act( AT_GREEN, "$n's twist beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your twist beam, discarding both of your energy balls.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }
    ch->substate = SUB_NONE;

/* Move victim to ch and back after damage is done if... */
if ( victim && (ch->in_room->vnum != victim->in_room->vnum ) )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "%s's twist beam flies in from the %s!!\n\r", ch->name, dir_name[pexit->vdir] );
      pager_printf( ch, "Your attack flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}}

if ( can_use_skill(ch, number_percent(),gsn_twistbeam ) )
    {
          learn_from_success( ch, gsn_twistbeam );
          pager_printf( ch, "&GYou grin coolly, and bring both hands together, creating a %s &Gand &Blilac&G ball of ki!  Then, with all your might, you force the ki blast at %s, and the spiral beam connects satisfyingly!\n\r", ch->energycolour, victim->name );
          pager_printf( victim, "&G%s grins idiotically, bringing both hands together and creating a %s&G and &Blilac&G ball of ki.  Quickly, %s sends the multicoloured beam flying right at you!", ch->name, ch->energycolour, ch->sex == 2 ? "she" : "he" );
          act( AT_GREEN, "$n grins coolly, and brings both hands together to launch a multicoloured beam of ki at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, number_range(300, 900), gsn_twistbeam );
          ch->target = NULL;
    }
    else
    {
          learn_from_failure( ch, gsn_twistbeam );
          pager_printf( ch, "&GYou grin coolly, and bring both hands together, creating a %s &Gand &Blilac&G ball of ki!  Then, with all your might, you force the ki blast at %s... but it unravels in flight, and two ki beams fly off in the wrong direction!\n\r", ch->energycolour, victim->name );
          pager_printf( victim, "&G%s grins idiotically, bringing both hands together and creating a %s&G and &Blilac&G ball of ki.  Quickly, %s sends the multicoloured beam flying right at you... but it comes apart in flight and misses completely!", ch->name, ch->energycolour, ch->sex == 2 ? "she" : "he" );
          act( AT_GREEN, "$n coolly, and brings both hands together to launch a multicoloured beam of ki at $N, but it comes apart and misses completely.",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, 0, gsn_twistbeam );
          ch->target = NULL;
    }

 if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ));
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
return;
}

void do_metalmorph ( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 char buf [MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_metalmorph, 1 );
        sprintf( buf, "&GYou charge a smooth lance of %s &Gki, and grip it firmly, in order to draw attention away from your mouth...", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s charges up a smooth lance of ki, and looks like %s is going to throw it at you...", ch->name, ch->sex == 2 ? "she" : "he" );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n charges up a smooth lance of ki, and eyes $N carefully...", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your metalmorph was interupted!\n\r", ch );
          sprintf( buf, "&G%s's metalmorph was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's metalmorph was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your metalmorph, and throw the ki lance over your shoulder.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_metalmorph ) )
    {
          learn_from_success( ch, gsn_metalmorph );
          pager_printf( ch, "&G%s looks apprehensive, and starts to move towards you!  You scream '&RMETAL MORPH!&G' and fire a %s &GBeam of ki from your mouth at %s, turning %s to metal!", victim->name, ch->energycolour, ch->sex == 2 ? "her" : "him", ch->sex == 2 ? "her" : "him" );
          pager_printf( victim, "&GYou start to move towards %s, but %s quickly fires a %s&G beam of ki at you from %s mouth, screaming '&RMETAL MORPH!&G'  It hits, and you start turn into metal!", ch->name, ch->sex == 0? "it" : ch->sex == 1? "he" :"she", ch->energycolour, ch->sex == 2 ? "her" : "his" );
          act( AT_GREEN, "$n yells 'METAL MORPH' and turns $N into metal!\n\r",  ch, NULL, victim, TO_NOTVICT );
          victim->skill_timer = 10;
          WAIT_STATE( victim, 10 * PULSE_VIOLENCE );
          global_retcode = damage( ch, victim, number_range(10, 20), gsn_metalmorph );
    }
    else
    {
          learn_from_failure( ch, gsn_metalmorph );
          pager_printf( ch, "&G%s looks apprehensive, and starts to move towards you!  You scream '&RMETAL MORPH!&G' and fire a %s &GBeam of ki from your mouth at %s, but %s notices and dodges out of the way!", victim->name, ch->energycolour, ch->sex == 2 ? "her" : "him", ch->sex == 2 ? "she" : "he" );
          pager_printf( victim, "&GYou start to move towards %s, but %s quickly fires a %s&G beam of ki at you from %s mouth, screaming '&RMETAL MORPH!&G'  Quickly, you move out of the way!", ch->name, ch->sex == 0? "it" : ch->sex == 1? "he" :"she", ch->energycolour, ch->sex == 2 ? "her" : "his" );
          act( AT_GREEN, "$n yells 'METAL MORPH!' and fires a beam of ki at $N, but $E dodges out of the way.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, 0, gsn_metalmorph );
    }
return;
}

void do_solarflare ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
// char buf [MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_solarflare, 1 );
        pager_printf(ch, "&GYou bring both of your hands to your forehead and begin to charge up an attack." );
        act( AT_GREEN, "$n brings both of $s hands to $s face and charges an attack...", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your solar flare was interupted!\n\r", ch );
          act( AT_GREEN, "$n's solar flare was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your solar flare, and drop your hands to your sides.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_solarflare ) )
    {
          learn_from_success( ch, gsn_solarflare );
          pager_printf( ch, "&GYou scream '%s&G', releasing a burst of ki and blinding %s for a while!", ch->hit >= ch->max_hit / 4 ? "&RSOLAR FLARE!" : "&CL&YI&CG&YH&CT &YO&CF &YT&CH&YE &CS&YU&CN&R!", victim->name );
          pager_printf( victim, "&G%s screams '%s&G', releasing a burst of ki and blinding you for a while!", ch->name, ch->hit >= ch->max_hit / 4 ? "&RSOLAR FLARE!" : "&CL&YI&CG&YH&CT &YO&CF &YT&CH&YE &CS&YU&CN&R!" );
          act( AT_GREEN, "$n yells 'SOLAR FLARE' and releases a burst of ki, blinding $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
          victim->skill_timer = 6;
          WAIT_STATE( victim, 6 * PULSE_VIOLENCE );
          global_retcode = damage( ch, victim, number_range(10, 20), gsn_solarflare );
    }
    else
    {
          learn_from_failure( ch, gsn_solarflare );
          pager_printf( ch, "&GYou scream '%s&G', releasing a burst of ki, but %s blocks the attack, shielding %s eyes!", ch->hit >= ch->max_hit / 4 ? "&RSOLAR FLARE!" : "&CL&YI&CG&YH&CT &YO&CF &YT&CH&YE &CS&YU&CN&R!", victim->name, victim->sex <= 1 ? "his" : "her" );
          pager_printf( victim, "&G%s screams '%s&G', releasing a burst of ki to blind you, but you quickly shield your eyes!", ch->name, ch->hit >= ch->max_hit / 4 ? "&RSOLAR FLARE!" : "&CL&YI&CG&YH&CT &YO&CF &YT&CH&YE &CS&YU&CN&R!" );
          act( AT_GREEN, "$n yells 'SOLAR FLARE' and releases a burst of ki, but $N shields $S eyes!\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, 0, gsn_solarflare );
    }
return;
}

void do_mouthbeam ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
//char buf[MAX_STRING_LENGTH];
char arg[MAX_STRING_LENGTH];
int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 4;
        add_timer( ch, TIMER_DO_FUN, 4, do_mouthbeam, 1 );
        pager_printf(ch, "&GYou bring your hands up to your open mouth, and %s&G glow soon eminates from it.", ch->energycolour );
        act( AT_GREEN, "$n brings $s hands up to $s open mouth, and begins to glow.", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        ch->target = victim;
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your mouth beam was interupted!\n\r", ch );
          act( AT_GREEN, "$n's mouth beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your mouth beam, and your mouth stops glowing.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }
    ch->substate = SUB_NONE;

/* Move victim to ch and back after damage is done if... */
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's mouth beam flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your mouth beam flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

if ( can_use_skill(ch, number_percent(),gsn_mouthbeam ) )
    {
          learn_from_success( ch, gsn_mouthbeam );
          pager_printf( ch, "&GYou glare in the direction of %s, and shout '&RERASER GUN!&G', firing a %s &Gbeam of ki at %s from your mouth!\n\r", PERS( victim, ch ), ch->energycolour, victim->sex == 2 ? "her" : "him" );
          pager_printf( victim, "&G%s glances in your direction, and then shouts '&RERASER GUN!&G' - firing a %s &Gbeam of ki at you from %s mouth!\n\r", PERS( ch, victim ), ch->energycolour, ch->sex == 2 ? "her" : "his" );
          act( AT_GREEN, "$n glances at $N, and screams 'ERASER GUN!' firing a beam of ki at $M from $s mouth!", ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, number_range(200, 400), gsn_mouthbeam );
          ch->target = NULL;
    }
    else
    {
          learn_from_failure( ch, gsn_mouthbeam );
          pager_printf( ch, "&GYou glare at %s, and scream '&RERASER GUN!&G' firing a %s&G beam of ki at %s!  However, you miss and only aid the destruction of the landscape.\n\r", PERS( victim, ch ), ch->energycolour, victim->sex == 2 ? "her" : "him" );
          pager_printf( victim, "&G%s glances at you, and screams '&RERASER GUN!&G' firing a %s&G beam of ki at you!  However, the attack misses and only aids the destruction of the landscape.\n\r", PERS( victim, ch ), ch->energycolour );
          act( AT_GREEN, "$n glances at $N, and screams 'ERASER GUN', firing a beam of ki at $M from $s mouth, which misses completely.", ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, 0, gsn_mouthbeam );
          ch->target = NULL;
    }

 if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ) );
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
return;
}

void do_bodyswap( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
 }

 if ( victim->fused )
 {
	 send_to_char( "Odd: You seem to be detecting two powerlevels...\n\r", ch );	
	 return;    
 }

 if ( victim->basepl < ch->basepl / 2 )
 {
	send_to_char( "But they're so much weaker than you!\n\r", ch );
	return;
 }

 switch( ch->substate )
 {
         default:
         ch->skill_timer = 8;
         add_timer( ch, TIMER_DO_FUN, 8, do_bodyswap, 1 );
         pager_printf( ch, "&GRage overcomes you, and you punch yourself in the chest, revealing a deep and nasty wound!  Slowly you glow %s&G...\n\r", ch->energycolour );
         pager_printf( victim, "&G%s punches %s in the chest, screaming in pain from the new wound!  Slowly, %s begins to glow %s&G...", ch->name, ch->sex == 0? "itself" : ch->sex == 1 ? "himself" : "herself", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she", ch->energycolour );
         act( AT_GREEN, "$n punches $mself in the chest, revealing a new wound!! Slowly $e then begins to power up...", ch, NULL, victim, TO_NOTVICT );
         ch->hit -= get_curr_str(ch) * 4;
         if ( ch->hit < 1 )
            {
            raw_kill( ch, ch );
            return;
            }
         ch->alloc_ptr = str_dup( arg );
         return;

	     case 1:
	     if ( !ch->alloc_ptr )
     	 {
                 send_to_char( "Your bodyswap was interupted!\n\r", ch );
                 sprintf( buf, "&G%s's bodyswap was interrupted!", ch->name );
                 pager_printf(victim, "%s\n\r", buf );
                 act( AT_GREEN, "$n's bodyswap was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
                 bug( "do_search: alloc_ptr NULL", 0 );
                 return;
          }
          strcpy( arg, ch->alloc_ptr );
          DISPOSE( ch->alloc_ptr );
          break;

          case SUB_TIMER_DO_ABORT:
          DISPOSE( ch->alloc_ptr );
          ch->substate = SUB_NONE;
          send_to_char( "You stop your bodyswap.\n\r", ch );
          pager_printf(victim, "%s stops %s bodyswap\n\r", buf );
          act( AT_GREEN, "$n's cancels $s bodyswap!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
  }
  ch->substate = SUB_NONE;
  if ( victim->currpl > ch->currpl * 10 && !IS_NPC(victim))
  {
     if ( can_use_skill(ch, number_percent(),gsn_bodyswap ) )
     {
        learn_from_success( ch, gsn_bodyswap );
	if ( victim->race != RACE_ANDROID && switch_bodies(ch,victim) == TRUE )
	{
        pager_printf( ch, "&GYou shout 'YES!! Now you're MINE! &RCHANGE NOW!&G' and shoot a %s &Gbeam of ki at %s from your mouth! Suddenly, you find yourself staring at your old body...", ch->energycolour, victim->name );
        pager_printf( victim, "&G%s shouts 'YES!! Now you're MINE! &RCHANGE NOW!&G', shooting a %s&G ki beam from %s mouth! Suddenly, you find yourself staring at... YOURSELF!", ch->name, ch->energycolour, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
        act( AT_GREEN, "$n shouts 'YES!! Now you're MINE! CHANGE NOW!', and a bright flash blinds you for a second!", ch, NULL, victim, TO_NOTVICT );
	}
	else
	{
        pager_printf( ch, "&GYou shout 'YES!! Now you're MINE! &RCHANGE NOW!&G' and shoot a %s &Gbeam of ki at %s from your mouth... But that frog... NO!  IT'S TOO LATE TO STOP!", ch->energycolour, victim->name );
        pager_printf( victim, "&G%s shouts 'YES!! Now you're MINE! &RCHANGE NOW!&G', shooting a %s&G ki beam from %s mouth.  Realising that you're going to be toast pretty soon, you grab a frog and throw it in front of the energy beam!", ch->name, ch->energycolour, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
        act( AT_GREEN, "$n shouts 'YES!! Now you're MINE! CHANGE NOW!', and a bright flash blinds you for a second!", ch, NULL, victim, TO_NOTVICT );
	}
     }
     else
     {
       learn_from_failure( ch, gsn_bodyswap );
       pager_printf( ch, "&GYou suddenly feel like all the confidence has been drained from your body! You swear under your breath and continue the fight...\n\r" );
       pager_printf( victim, "&G%s suddenly looks worried and mutters 'No... not now! ANY TIME BUT NOW!!'\n\r", ch->name );
       act( AT_GREEN, "$n half collapses to the ground, swearing about $s failing techniques!\n\r", ch, NULL, victim, TO_NOTVICT );
       global_retcode = damage( ch, victim, 0, gsn_bodyswap );
     }
  }
  else
  {
       pager_printf( victim, "&GSick of this stupid act, you fly over to %s and smack %s across the face, ending whatever plan %s had in store!\n\r", ch->name, ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she" );
       pager_printf( ch, "&GSick of your amazing act, %s flies over to you and smacks you in the face!  You completely lose concentration!\n\r", victim->name );
       act( AT_GREEN, "$N yawns and flies over to $n, smacking $m in the face!\n\r", ch, NULL, victim, TO_NOTVICT );
  }
return;
}

bool switch_bodies( CHAR_DATA *ch, CHAR_DATA *victim )
{
 CHAR_DATA *oldc, *oldv;
// int plmod;
 int sex;
 unsigned long long pl, bpl;
 int str, inte, wis, dex, con, cha, lck;

 oldc = ch;
 oldv = victim;

inte = ch->perm_int;
str = ch->perm_str;
wis = ch->perm_wis;
dex = ch->perm_dex;
con = ch->perm_con;
cha = ch->perm_cha;
lck = ch->perm_lck;
pl = ch->currpl;
bpl = ch->basepl;
sex = ch->sex;

// Spammers Beware!
if ( number_range( 1, 40 ) == 1 || IS_IMMORTAL( ch ) )
{
	talk_info( AT_PINK, "Frog stomping season has begun!" );
	pager_printf( ch, "&RSomething is going wrong... Is that... A FROG?!" );
	 ch->perm_str                  = 50;
	 ch->perm_dex                  = 50;
	 ch->perm_int                  = 50;
	 ch->perm_wis                  = 50;
	 ch->perm_con                  = 50;
	 ch->perm_cha                  = 50;
	 ch->perm_lck                  = 50;
	 xSET_BIT(ch->affected_by, AFF_PARALYSIS ); 
	 return FALSE;
}

 ch->hit                       = oldv->hit;
 ch->mana                      = oldv->mana;
 ch->basepl                    = oldv->basepl * 0.75;
 ch->currpl                    = oldv->currpl * 0.75;
 ch->perm_str                  = oldv->perm_str;
 ch->perm_dex                  = oldv->perm_dex;
 ch->perm_int                  = oldv->perm_int;
 ch->perm_wis                  = oldv->perm_wis;
 ch->perm_con                  = oldv->perm_con;
 ch->perm_cha                  = oldv->perm_cha;
 ch->perm_lck                  = oldv->perm_lck;
 ch->sex		       = oldv->sex;

 victim->sex		       = oldc->sex;
 victim->hit                   = oldc->hit;
 victim->mana                  = oldc->mana;
 victim->basepl                = bpl;
 victim->currpl                = pl;
/*
 victim->perm_str              = oldc->perm_str;
 victim->perm_dex              = oldc->perm_dex;
 victim->perm_int              = oldc->perm_int;
 victim->perm_wis              = oldc->perm_wis;
 victim->perm_con              = oldc->perm_con;
 victim->perm_cha              = oldc->perm_cha;
 victim->perm_lck              = oldc->perm_lck;
*/
 victim->perm_str              = str;
 victim->perm_dex              = dex;
 victim->perm_int              = inte;
 victim->perm_wis              = wis;
 victim->perm_con              = con;
 victim->perm_cha              = cha;
 victim->perm_lck              = lck;

 do_powerdown( ch, "");
 do_powerdown( victim, "" );
 return TRUE;
}

void do_decadentflare ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
char arg[MAX_INPUT_LENGTH];
char buf[MAX_STRING_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
     }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 20;
        add_timer( ch, TIMER_DO_FUN, 20, do_decadentflare, 1 );
        pager_printf(ch, "&GYou look up at %s, realising what you must do to end the pain of your people.  A strong sense of pride and belonging swells inside of you as you expolde in a huge %s &Gaura, and fly right at %s, holding %s still while you overcharge your ki.", victim->name, ch->energycolour, victim->name, victim->sex == 2 ? "her" : "him" );
        pager_printf(victim, "&G%s looks up at you resolutely, resigned to whatever fate is left to %s.  Suddenly, %s explodes in a %s&G aura, and flies at you, stunning you momentarily!\n\r",ch->name, ch->sex == 2 ? "her" : "him", ch->sex == 2 ? "she" : "he", ch->energycolour );
        act( AT_GREEN, "$n looks at $N defiantly, seemingly knowing the way to end the torment that $s people have been suffering.", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your decadent flare was interupted!\n\r", ch );
          act( AT_GREEN, "$n's decadent flare was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You drop your victim, no longer having the courage to go through with your self-sacrifice technique.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }
    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_decadentflare ) )
    {
          learn_from_success( ch, gsn_decadentflare );
          pager_printf( ch, "&GYou lift your head up high and, screaming, release all of the ki you have built up in one devastating explosion!\n\r" );
          pager_printf( victim, "&G%s lifts %s head proudly and, screaming, releases all of %s ki in one devastating explosion!", PERS(ch, victim), ch->sex == 2 ? "her" : "his", ch->sex == 2 ? "her" : "his" );
          act( AT_GREEN, "$n looks at $N solemenly, and screams with all $s might, ending both $s and $S life in one massive explosion!", ch, NULL, victim, TO_NOTVICT );
	  pl_gain( ch, victim, victim->hit / 10 );
	  if ( !IS_NPC(ch ))
 	      ch->pcdata->pl_today += victim->hit / 10;
//	  ch->basepl += (sqrt( victim->currpl ) * victim->hit ) / 5;  
//	  update_current(ch, NULL );
          raw_kill( ch, victim );
          raw_kill( ch, ch );
	  sprintf( buf, "%s died in a glorious attempt to annihilate %s forever.", ch->name, victim->name );
	  talk_info( AT_PINK, buf );
    }
    else
    {
          learn_from_failure( ch, gsn_decadentflare );
          pager_printf( ch, "&GYou lift your head up high and, screaming, release all of the ki you have built up in one go!  However, it is far from enough to destroy even yourself...\n\r" );
          pager_printf( victim, "&G%s lifts %s head proudly and, screaming, releases all of %s ki in one go!  It is far from enough to kill either of you, however.", PERS(ch, victim), ch->sex == 2 ? "her" : "his", ch->sex == 2 ? "her" : "his" );
          act( AT_GREEN, "$n looks at $N solemenly, and screams with all $s might, but fails to destroy either $mself or $N!", ch, NULL, victim, TO_NOTVICT );
    }
return;
}

void do_meteorwing ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim = NULL;
char arg[MAX_INPUT_LENGTH];

if ( who_fighting( ch ) != NULL )
{
     send_to_char( "You are fighting someone right now.\n\r", ch );
     return;
}

if ( IS_SET(ch->in_room->room_flags, ROOM_SAFE ) )
{
     send_to_char( "Not in here...\n\r", ch );
     return;
}

switch( ch->substate )
    {
        default:

        ch->skill_timer = 4;
        add_timer( ch, TIMER_DO_FUN, 4, do_meteorwing, 1 );
        pager_printf( ch, "&GYou ascend higher into the air like some kind of fallen angel, and spread your arms wide, charging up your energy.\n\r"  );
        act( AT_GREEN, "$n floats higher into the air, spreading $s arms like some kind of angel.", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your meteor wing interupted!\n\r", ch );
          act( AT_GREEN, "$n's meteor wing was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your thunder attack, and float back down to earth.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }
    ch->substate = SUB_NONE;


if ( can_use_skill(ch, number_percent(),gsn_meteorwing ) )
    {
          learn_from_success( ch, gsn_meteorwing );
          pager_printf( ch, "&GYou look around you, and release electrical energy balls at anyone within range, as thunder crashes all around!\n\r"  );
          for ( victim = ch->in_room->first_person; victim; victim = victim->next_in_room )
          {
               if ( victim == ch || xIS_SET(victim->act, PLR_KILLER) )
               {
		   int d = number_range( 1000, 9999 );
	           ch_printf( ch, "Your electrical attack sends a shockwave of feedback into your system! (%d)", d );
                   ch->hit -= d; 
                   if ( ch->hit <= 0 ) ch->hit = 0;
	           continue;
	       }
//               if ( !IS_NPC(ch) && ( victim->basepl < ch->basepl / 10 ) )
//                  continue;
               pager_printf( victim, "&G%s launches a %s&G energyball at you!\n\r", PERS( ch, victim ), ch->energycolour );
	       global_retcode = damage( ch, victim, number_range( 500, 2500 ), gsn_meteorwing );
	       multi_hit( victim, ch, TYPE_HIT );
	       continue;
          }
    }
    else
    {
          learn_from_failure( ch, gsn_meteorwing );
          pager_printf( ch, "&GYou suddenly feel unable to go ahead with your actions... it must be some kind of... hardware fault...");
    }
return;
}


void do_super_android( CHAR_DATA *ch, char *argument )
{

 if ( IS_AFFECTED( ch, AFF_SUPER_ANDROID ) )
 {
      send_to_char( "You turn off a number of your subystems, and powerdown a little.\n\r", ch );
      act( AT_PINK, "$n seems to visibly get weaker.\n\r", ch, NULL, NULL, TO_CANSEE );
      ch->plmod = 100;
      ch->multi_str = 0;
      ch->multi_int = 0;
      ch->multi_wis = 0;
      ch->multi_dex = 0;
      ch->multi_con = 0;
      ch->multi_cha = 0;
      ch->multi_lck = 0;
      xREMOVE_BIT( ch->affected_by, AFF_SUPER_ANDROID );
      do_powerdown( ch, "" );
	update_current( ch, NULL);
      return;    
 }

 if ( ch->pkills < 50 )
 {
    send_to_char( "You must slaughter 50 innocents before you can use this powerful ability.\n\r", ch );
    return;
 }

 if ( ch->perm_int < 100 )
 {
    send_to_char( "You must be of optimum intelligence for this skill.\n\r", ch );
    return;
 }

 if ( ch->perm_str < 100 )
 {
    send_to_char( "It takes more strength than you have for this awesome technique.\n\r", ch );
    return;
 }

 ch->multi_str = 300;
 ch->multi_wis = 100;
 ch->multi_dex = 100;
 ch->multi_con = 200;
 ch->multi_int = 300;
 ch->multi_cha = 100;
 ch->multi_lck = 50;
 ch->plmod     = 2850;
 pager_printf( ch, "&zYou collapse each of your individual circuits, shutting down all non-essential subroutines until you are just barely alive.  Then you reroute all your energy through your body, creating a blinding screen of %s&z in order to hide your transformation.  You soon feel your new power flowing through you, and it quickly becomes obvious that there is only one thing to do...\n\r", ch->energycolour );
 act( AT_GREY, "$n blinds you with a huge flash of light, and when you next see $m $e has changed completely...", ch, NULL, NULL, TO_CANSEE );
// interpret( ch, "wartalk I'm going to teach each and every one of you a lesson you won't forget..." );
 xSET_BIT( ch->affected_by, AFF_SUPER_ANDROID );
 update_current( ch, NULL);
 return;
}


void do_mutie ( CHAR_DATA *ch, char * argument )
{
 if ( IS_AFFECTED( ch, AFF_MUTIE ) )
 {
      send_to_char( "You stand up straight, looking less cool.\n\r", ch );
      act( AT_PINK, "$n stands up straight, looking like less of fool.\n\r", ch, NULL, NULL, TO_CANSEE );
      ch->plmod = 100;
      ch->multi_str = 0;
      ch->multi_int = 0;
      ch->multi_wis = 0;
      ch->multi_dex = 0;
      ch->multi_con = 0;
      ch->multi_cha = 0;
      ch->multi_lck = 0;
      xREMOVE_BIT( ch->affected_by, AFF_MUTIE );
      do_powerdown( ch, "" );
      return;
 }

 ch->plmod = (100 *  (4 + (ch->perm_cha / 10)));
 if ( ch->plmod > 2400 )
	ch->plmod = 2400;
 update_current(ch, NULL );
 ch->multi_cha = 0;
 ch->multi_str = ch->perm_cha / 3;
 ch->multi_dex = ch->perm_cha / 3;
 ch->multi_con = ch->perm_cha / 3;
 ch->multi_int = ch->perm_cha / 8;
 ch->multi_wis = ch->perm_cha / 8;
 ch->multi_lck = ch->perm_cha / 8;
 xSET_BIT( ch->affected_by, AFF_MUTIE );
 if ( ch->perm_cha < 100 )
    send_to_char( "You do the first few steps of the dance of joy, turn your feet inwards and stick your arms in the air!\n\r", ch );
 else if ( ch->perm_cha < 200 )
    send_to_char( "You do the dance of finding lost objects, flex your muscles and strike a fetching pose!\n\r", ch );
 else
    send_to_char( "You do the complete dance of joy, with the extra-special super-hero finale!\n\r", ch );
 act( AT_PINK, "$n does some silly little dance.", ch, NULL, NULL, TO_CANSEE );
 return;
}


void do_supernova( CHAR_DATA *ch, char *argument )
{
CHAR_DATA *victim;
char arg[MAX_STRING_LENGTH];
int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

if ( ch->plmod < 2000 && !IS_NPC(ch) )
{
   send_to_char( "That is one of your ultimate techniques.\n\r", ch );
   return;
}

	switch( ch->substate )
    {
	default:
         ch->skill_timer = 10;
         add_timer( ch, TIMER_DO_FUN, 10, do_supernova, 1 );
         pager_printf( ch, "&GYou slowly discharge bursts of %s&G energy as you bring one hand up level with your chest, slowly gathering %s energy around a thick, black core.\n\r", ch->energycolour, ch->energycolour );
         act( AT_GREEN, "$n discharges bursts of ki as $e brings one hand up level with $s chest, gathering energy around a thick, black core...\n\r", ch, NULL, victim, TO_CANSEE );
         ch->target = victim;
         ch->alloc_ptr = str_dup( arg );
         return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your antimatter overload was interupted!\n\r", ch );
          pager_printf( victim, "&G%s's antimatter overload was interrupted!\n\r", ch->name );
          act( AT_GREEN, "$n's antimatter overload was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your antimatter overload.\n\r", ch );
            act( AT_GREEN, "$n's cancels $s technique.\n\r",  ch, NULL, NULL, TO_CANSEE );
	    ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( ch->in_room != victim->in_room )
    {
       for ( attempt = 0; attempt < 8; attempt++ )
       {
          door = number_door( );
          if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
                continue;
          if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
          {
                pager_printf( victim, "&P%s's antimatter overload flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
                pager_printf( ch, "Your antimatter overload flies %s to %s!\n\r", dir_name[pexit->vdir], PERS(victim, ch ) );
                vnum = victim->in_room->vnum;
                char_from_room( victim );
                char_to_room( victim, ch->in_room );
                moved = TRUE;
          }
       }
    }

    if ( can_use_skill(ch, number_percent(),gsn_supernova ) )
    {
         learn_from_success( ch, gsn_supernova );
         pager_printf( ch, "&GYou bring your other hand up level with the first and lean into your Antimatter Overload, pushing it at %s with all your strength.  When it hits it echoes out with an earsplitting explosion", victim->name );
         pager_printf( victim, "&G%s brings the other hand level with the first and leans into the huge Antimatter Overload, pushing it at you with all %s might!\n\r", ch->name, ch->sex == 2? "her" : "his" );
         act( AT_GREEN, "$n brings one hand level with the other, and leans into $s attack, pushing it with all $s might at $N!\n\r", ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, number_range( 6000, 8000 ), gsn_supernova );
         ch->target = NULL;
    }
    else
    {
        learn_from_failure( ch, gsn_supernova );
        pager_printf( ch, "&GYour control really isn't up to the standard it needs to be: As soon as you move your arms your attack evaportates!\n\r" );
        pager_printf( victim, "&G%s swings both arms round to %s front, but it is obvious that %s has lost the attack energy %s collected!\n\r", ch->name, ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she" );
        act( AT_GREEN, "$n speedily arcs both arms round to $s front, but $s attack just evaporates...\n\r", ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_supernova );
        ch->target = NULL;
    }
 if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ) );
     act( AT_GREEN, "$n's Antimatter Overload flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
return;
}

void do_finaljudgement ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

int vnum =0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
  }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  8;
        add_timer( ch, TIMER_DO_FUN, 8, do_finaljudgement, 1 );
	ch->target = victim;

	sprintf( buf, "&GYou drop your head, feeling the rage of all you have lost over the years, and suddenly explode in a huge aura of %s ki.", ch->energycolour );
            pager_printf(ch, "%s\n\r", buf );
	 sprintf( buf, "&G%s drops %s head and reflects on the past as %s explodes in a tremendous aura of %s &GKi.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her",ch->sex == 0? "it" : ch->sex == 1? "he" : "she", ch->energycolour);
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n drops his head and reflects on the past as $e explodes in an aura of Ki.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your reflections are ended by an enemy attack!\n\r", ch );
          sprintf( buf, "&G%s's move was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's move was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's final judgement flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your spirit final judgement %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

            send_to_char( "You stop your final judgement.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s move.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
 continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's final judgement flies in from the %s!!\n\r", PERS( ch, victim ), 
dir_name[pexit->vdir] );
      pager_printf( ch, "Your final judgement flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
}}
ch->target = NULL;
if ( can_use_skill(ch, number_percent(),gsn_finaljudgement) )
    {
          learn_from_success( ch, gsn_finaljudgement);
          sprintf( buf, "&R'The Final Judgement!' &GYou rush towards your opponent and annihilate him under a flurry of blows, before jetting back and charging an enormous ball of ki. You throw it, hitting %s.",  PERS(victim,ch));
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&R'The Final Judgement!' &G%s rushes towards you, beats you into oblivion, before jetting back and firing a large ball of %s energy at you!.", ch->name, ch->energycolour);
pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n smashes $s with the Final Judgement! $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, (number_range(2000, 3500)), gsn_finaljudgement );
    }
    else
    {
        learn_from_failure( ch, gsn_finaljudgement);
send_to_char( "&R'Fial Judgement!' &GYou cannot retain your power and collapse from exhaustion.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "&R'Final Judgement!'&G %s powers up and and looks at you, then collapses.\n\r", PERS(ch,victim));
         else
                 sprintf(buf, "&R'Final Judgement!'&G %s powers up and looks at you, then collapses\n\r", PERS(ch,victim));
          pager_printf(victim, "%s\n\r", buf );
         act( AT_GREEN, "$n charges up and tries to attack $N but can't hold $s power.\n\r", ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_finaljudgement);
         global_retcode = damage( ch, victim, 0, gsn_finaljudgement);
    }
if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ));
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
}
return;
}



void do_genocideattack ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
char arg[MAX_INPUT_LENGTH];
char buf[MAX_STRING_LENGTH];
int ctr, pcctr = 0, npcctr = 0, dctr = 0;

if ( ch->genotimer > 0 )
{
	send_to_char( "Not right now...\n\r", ch );
	return;
}

if ( ch->plmod < 1800 )
{
     send_to_char( "Like you could pull that off with your body.\n\r", ch );
     return;
}

if ( get_timer(ch, TIMER_PKILLED) > 0 )
{
     send_to_char( "You have died recently.\n\r", ch );
     return;
}

if ( xIS_SET(ch->act,PLR_KILLER))
{
     send_to_char( "You start to use this attack, but your NPK protection shield blocks all the attacks in.  They detonate right next to you!\n\r", ch );
     AreaAttack = FALSE;
     raw_kill( ch, ch );
     return;
}
if ( IS_SET(ch->in_room->room_flags, ROOM_NO_ASTRAL) || 
IS_SET(ch->in_room->room_flags, ROOM_PRIVATE) )
{
   if ( ch->alignment < 0 )
     send_to_char( "Doing that in here would certainly get the likes of Goku on your ass.\n\r", ch );
   else
     send_to_char( "Doing that in here would certainly get the likes of Yami on your ass.\n\r", ch );
     AreaAttack = FALSE;
   return;
}

if ( who_fighting( ch ) != NULL )
{
     send_to_char( "You are fighting someone right now.\n\r", ch );
     AreaAttack = FALSE;
     return;
}

switch( ch->substate )
    {
        default:

        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_genocideattack, 1 );
        pager_printf( ch, "&GYou take a deep breath and begin to search for every powerlevel around you, noting its location.\n\r"  );
        act( AT_GREEN, "$n takes a deep breath and looks around.", ch, NULL, NULL, TO_CANSEE );
	if ( ch->in_room->area->planet )
	        sprintf( buf, "^z%s initiates genocide attack on %s!^x", ch->name, ch->in_room->area->planet );
	else
	        sprintf( buf, "^z%s initiates genocide attack in %s!^x", ch->name, ch->in_room->area->name );
        talk_info( AT_LBLUE, buf );
	AreaAttack = TRUE;
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your genocide attack was interupted!\n\r", ch );
	  AreaAttack = FALSE;
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        AreaAttack = FALSE;
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        AreaAttack = FALSE;
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your genocide attack and carry on as you were.\n\r", ch );
            return;
    }
    ch->substate = SUB_NONE;


if ( can_use_skill(ch, number_percent(),gsn_genocideattack ) )
{
	  AreaAttack = FALSE;
          ctr = 0;
          learn_from_success( ch, gsn_genocideattack );
          sprintf( buf, "%s fires %s genocide attack!", ch->name, ch->sex == 0 ? "its" : ch->sex == 1? "his" : "her" );
          talk_info( AT_PINK, buf );
          pager_printf( ch, "&GYou put one hand in the air, and launch a sphere of energy at everyone in the area!\n\r"  );
          for ( victim = first_char; victim; victim = victim->next )
	  {
              if ( victim->in_room && 
		( ( ch->in_room->area->planet && !str_cmp(victim->in_room->area->planet, ch->in_room->area->planet) ) 
		|| ( victim->in_room->area == ch->in_room->area ) ) )
              {
		 if ( ch == victim || IS_IMMORTAL(victim) || (IS_NPC(victim) && xIS_SET(victim->act, ACT_PACIFIST)) || 
			IS_SET(victim->in_room->room_flags, ROOM_SAFE) || (!IS_NPC(victim) && victim->basepl > 1000000 ) )
			continue;
                 send_to_char( "An energyball shoots out of the sky at you!\n\r", victim );
		 if ( IS_NPC(victim) )
		 {
		     victim->hit -= number_range( 2500, 10000 );
//                     damage( ch, victim, number_range( 2500, 7500 ), gsn_genocideattack );
		    npcctr++;
		    if ( victim->hit <= 0 )
			dctr++;
		 }
		 else
		 {
		     victim->hit -= number_range( 2500, 7500 );
		     pcctr++;
		     if ( victim->hit < 1 )
		         victim->hit = 1;
		 }
                 ch->alignment -= 100;
   	         ch->genotimer += 120;
                 if ( victim->currpl > ch->currpl/2 || (IS_NPC(victim) &&  xIS_SET(victim->act, ACT_MIMIC)) )
                 {
                    pager_printf( victim, "Keen for revenge, you speed over to face %s for yourself!\n\r", ch->name );
		    victim->retran = victim->in_room->vnum;
		    char_from_room( victim );
		    char_to_room( victim, ch->in_room );
                    multi_hit( victim, ch, TYPE_HIT );
                    ctr++;
		    ch->genotimer += 120;
                 }
              }
	      continue;
          }
          if ( ch->alignment < -1000 )
               ch->alignment = -1000;
          law_broken( ch, 11 );

	  pager_printf( ch, "You hit %d NPC(s) and %d PC(s), with %d character(s) perishing immediately.\n\r", npcctr, pcctr, dctr );

          if ( ctr >= 5 )
          {
               sprintf( buf, "%s's genocide attack initiates a huge battle, with many of the world's best warriors deciding to kill %s!", ch->name, ch->sex == 2 ? "her" : "him" );
               talk_info( AT_PINK, buf );
	       if ( !IS_NPC(ch) && ch->pcdata->learned[gsn_obliterator_cannon] < 10 )
               {
                 ch->pcdata->learned[gsn_obliterator_cannon] = 10;
                 pager_printf( ch, "&P\n\r\n\rSuddenly seeing this many morons in such a small area gives you a nice idea for a new skill... Perhaps some kind of magic based attack...\n\r\n\r" );
                 pager_printf( ch, "&PYou have learned the skill 'Obliterator Cannon'!\n\r" );
  	       }
          }
    }
    else
    {
	  AreaAttack = FALSE;
          learn_from_failure( ch, gsn_genocideattack );
          pager_printf( ch, "&GYou suddenly feel unable to go ahead with your actions... is it some kind of conscience...?\n\r");
          sprintf( buf, "%s fails to perform %s genocide attack!", ch->name, ch->sex == 0 ? "its" : ch->sex == 1? "his" : "her" );
          talk_info( AT_PINK, buf );
	  ch->genotimer = 60 * 5;
    }
return;
}



void do_bakuhatsuha ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
char arg[MAX_INPUT_LENGTH];
char buf[MAX_STRING_LENGTH];
int ctr;

if ( ch->genotimer > 0 )
{
	send_to_char( "Not right now...\n\r", ch );
	return;
}

if ( ch->plmod < 2200 )
{
     send_to_char( "Like you could pull that off with your body.\n\r", ch );
     return;
}
if ( xIS_SET(ch->act,PLR_KILLER) )
{
     send_to_char( "You start to use this attack, but your NPK protection shield blocks all the attacks in.  They detonate right next to you!\n\r", ch );
     raw_kill( ch, ch );
     return;
}

if ( IS_SET(ch->in_room->room_flags, ROOM_NO_ASTRAL) ||IS_SET(ch->in_room->room_flags, ROOM_PRIVATE))
{
   if ( ch->alignment < 0 )
     send_to_char( "Doing that in here would certainly get the likes of Goku on your ass.\n\r", ch );
   else
     send_to_char( "Doing that in here would certainly get the likes of Yami on your ass.\n\r", ch );
   return;
}

if ( who_fighting( ch ) != NULL )
{
     send_to_char( "You are fighting someone right now.\n\r", ch );
     return;
}

switch( ch->substate )
    {
        default:
	if ( AreaAttack )
	{
	    send_to_char( "Someone else is already using an area attack.", ch);
	    return;
	}
        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_bakuhatsuha, 1 );
        pager_printf( ch, "&GYou look up, and raise two fingers in the air.\n\r"  );
        act( AT_GREEN, "$n takes a deep breath and sticks two fingers in the air.", ch, NULL, NULL, TO_CANSEE );
	if ( ch->in_room->area->planet != NULL && ch->in_room->area->planet[0] != '(' )
	        sprintf( buf, "^z%s initiates stream wave somewhere on %s!^x", ch->name, ch->in_room->area->planet );
	else
	        sprintf( buf, "^z%s initiates stream wave in %s!^x", ch->name, ch->in_room->area->name );
        talk_info( AT_PINK, buf );
	AreaAttack = TRUE;
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          bug( "do_search: alloc_ptr NULL", 0 );
	  AreaAttack = FALSE;
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
	  AreaAttack = FALSE;
        send_to_char( "You stop your attack and carry on as you were.\n\r", ch );
            return;
    }
    ch->substate = SUB_NONE;


if ( can_use_skill(ch, number_percent(),gsn_bakuhatsuha ) )
    {
          ctr = 0;
	  AreaAttack = FALSE;
          learn_from_success( ch, gsn_bakuhatsuha );
          pager_printf( ch, "&GYou grin evily, and launch ki out of the sky all over the place!\n\r"  );
	  SET_BIT( ch->in_room->room_flags, ROOM_TRASHED );
          for ( victim = first_char; victim; victim = victim->next )
	  {
              if ( victim->in_room && ( victim->in_room->area == ch->in_room->area ) && victim->basepl > 10000000 )
              {
		 if ( ch == victim || IS_IMMORTAL(victim) || xIS_SET(victim->act, PLR_KILLER) 
		   || IS_NPC(victim) || IS_SET(victim->in_room->room_flags, ROOM_DUEL ) || xIS_SET(ch->act,PLR_ATTACKER))
			continue;
	         ch->genotimer += 60;
                 send_to_char( "The sky above you suddenly errups with ki!\n\r", ch );
                 damage( ch, victim, number_range( 500, 1250 ), gsn_bakuhatsuha );
                 ch->alignment -= 75;
              }
	      continue;

          }
          if ( ch->alignment < -1000 )
               ch->alignment = -1000;
    }
    else
    {
	  AreaAttack = FALSE;
          learn_from_failure( ch, gsn_bakuhatsuha );
          pager_printf( ch, "&GYou suddenly feel unable to go ahead with your actions... is it some kind of conscience...?\n\r");
	  ch->genotimer = 30;
    }
return;
}

void do_bakuhatsuhachou ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim;
char arg[MAX_INPUT_LENGTH];
char buf[MAX_STRING_LENGTH];
int ctr;

if ( ch->genotimer > 0 )
{
	send_to_char( "Not right now...\n\r", ch );
	return;
}

if ( ch->plmod < 6000 )
{
     send_to_char( "Like you could pull that off with your body.\n\r", ch );
     return;
}

if ( who_fighting( ch ) != NULL )
{
     send_to_char( "You are fighting someone right now.\n\r", ch );
     return;
}

switch( ch->substate )
    {
        default:

        ch->skill_timer = 25;
        add_timer( ch, TIMER_DO_FUN, 25, do_bakuhatsuhachou, 1 );
        pager_printf( ch, "&GYou look up, and raise two fingers in the air.\n\r"  );
        act( AT_GREEN, "$n takes a deep breath and sticks two fingers in the air.", ch, NULL, NULL, TO_CANSEE );
        sprintf( buf, "%s initiates chou bakuhatsuha!  RUN THE HELL AWAY!", ch->name );
        talk_info( AT_PINK, buf );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your chou bakuhatsuha was interupted!\n\r", ch );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your chou bakuhatsuha and carry on as you were.\n\r", ch );
            return;
    }
    ch->substate = SUB_NONE;


if ( can_use_skill(ch, number_percent(),gsn_bakuhatsuhachou ) )
    {
          ctr = 0;
          learn_from_success( ch, gsn_bakuhatsuhachou );
          sprintf( buf, "%s fires %s CHOU bakuhatsuha!", ch->name, ch->sex == 0 ? "its" : ch->sex == 1? "his" : "her" );
          talk_info( AT_PINK, buf );
          pager_printf( ch, "&GYou grin evily, and launch ki out of the ground all over the universe!!\n\r"  );
	  SET_BIT( ch->in_room->room_flags, ROOM_TRASHED );
          for ( victim = first_char; victim; victim = victim->next )
	  {
 	  if ( ch == victim || IS_IMMORTAL(victim) || IS_NPC(victim))
		continue;
          ch->genotimer += 120;
          send_to_char( "The ground below you suddenly errups with CHOU ki!\n\r", ch );
          damage( ch, victim, number_range( 2000, 3000 ) * get_curr_wis(ch) / 50, gsn_bakuhatsuhachou );
          ch->alignment -= 150;
            }
          if ( ch->alignment < -1000 )
               ch->alignment = -1000;
    }
    else
    {
          learn_from_failure( ch, gsn_bakuhatsuhachou );
          pager_printf( ch, "&GYou suddenly feel unable to go ahead with your actions... is it some kind of conscience...?\n\r");
          sprintf( buf, "%s fails to perform %s chou bakuhatsuha correctly.", ch->name, ch->sex == 0 ? "its" : ch->sex == 1? "his" : "her" );
          talk_info( AT_PINK, buf );
	  ch->genotimer = 120;
    }
ch->mana = 0;
return;
}


void do_invulnerabilityshield( CHAR_DATA *ch, char *argument )
{

 if ( IS_AFFECTED( ch, AFF_INVULNERABILITYSHIELD ) )
 {
	send_to_char( "You powerdown your invulnerability shield.\n\r", ch );
	act ( AT_LBLUE, "$n powers down $s aura of inulnerability.\n\r", ch, NULL, NULL, TO_CANSEE );
	xREMOVE_BIT( ch->affected_by, AFF_INVULNERABILITYSHIELD );
	return;
 }
 if ( ch->mana < ch->max_mana / 2 )
 {
	send_to_char( "You cannot activate this shield unless you have at least 50% of your maximum ki.\n\r", ch );
	return;
 }
 if ( !can_use_skill( ch, number_percent(), gsn_invulnerabilityshield ) )
 {
	learn_from_failure( ch, gsn_invulnerabilityshield );
	pager_printf( ch, "You attempt to summon the power of the Elder Gods, but fail.\n\r" );
	act( AT_LBLUE, "A little energy cascades from $n, but nothing happens.\n\r",ch, NULL, NULL, TO_CANSEE );
 }
 else
 {
	learn_from_success( ch, gsn_invulnerabilityshield );
	pager_printf( ch, "&YA divine white glow explodes around you, infused with the power of the Elder Gods!\n\r" ); 
	act( AT_LBLUE, "$n errupts into a divine glow as the power of the Elder Gods surrounds $m!\r", ch, NULL, NULL, TO_CANSEE );
	xSET_BIT( ch->affected_by, AFF_INVULNERABILITYSHIELD );
	return;
 }
 return;
}


void do_energybarrier( CHAR_DATA *ch, char *argument )
{
 int  level;

if ( ch->race != RACE_ANDROID )
{
send_to_char( "Huh?\n\r", ch);
return;
}
 if ( IS_AFFECTED( ch, AFF_ENERGYBARRIER ) )
 {
	send_to_char( "You powerdown your energy shield.\n\r", ch );
	act ( AT_LBLUE, "$n powers down $s energy shield.\n\r", ch, NULL, NULL, TO_CANSEE );
	xREMOVE_BIT( ch->affected_by, AFF_ENERGYBARRIER );
	ch->armourboost = 0;
	return;
 }

 level = 2;
 if ( ch->basepl > 5000000 )
	level++;
 if ( ch->basepl > 10000000 )
	level++;
 if ( ch->basepl > 50000000 )
	level++;
 if ( ch->basepl > 100000000 )
	level++;
 if ( ch->basepl > 250000000 )
	level++;
 if ( ch->basepl > 500000000 )
	level++;
 if ( ch->basepl > 1000000000 )
	level++;
 if ( ch->basepl > 2000000000 )
	level++;

 if ( !can_use_skill( ch, number_percent(), gsn_energybarrier ) )
 {
	learn_from_failure( ch, gsn_energybarrier );
	pager_printf( ch, "You clench your fists, but nothing happens.\n\r" );
	act( AT_LBLUE, "A little energy cascades from $n, but nothing happens.\n\r",ch, NULL, NULL, TO_CANSEE );
 }
 else
 {
	learn_from_success( ch, gsn_energybarrier );
	pager_printf( ch, "&YAn enormous %s&Y shield of energy flares up around you and the ground shakes a little!  Very cool.\n\r", ch->energycolour ); 
	act( AT_LBLUE, "The ground shakes as $n powers up $s electric shield!  Pretty cool, huh?\n\r", ch, NULL, NULL, TO_CANSEE );
	xSET_BIT( ch->affected_by, AFF_ENERGYBARRIER );
	ch->armourboost = pow( level, 2 ) * 250;
	return;
 }
return;
}

void do_arclightning ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
// char buf [MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ch->race != RACE_ANDROID )
{
send_to_char( "Huh?\n\r", ch);
return;
}

  if ( ( victim = who_fighting( ch ) ) == NULL )
  {
       send_to_char( "You aren't fighting anyone.\n\r", ch );
       return;
  }

  switch( ch->substate )
  {
      default:
              ch->skill_timer = 8;
              add_timer( ch, TIMER_DO_FUN, 8, do_arclightning, 1 );
              pager_printf( ch, "&GYou point your fingers straight down with your arms by your side, and glare at %s menacingly.\n\r", PERS( victim, ch ) );
              pager_printf( victim, "&G%s glares at you menacingly, fingers pointing straight down and arms by %s side.", PERS( ch, victim ), ch->sex == 2 ? "her" : "his" );
              act( AT_GREEN, "$n glares at $N menacingly, pointing $s fingers straight down.\n\r", ch, NULL, victim, TO_NOTVICT );
              ch->alloc_ptr = str_dup( arg );
              return;

      case 1:
              if ( !ch->alloc_ptr )
              {
              }
              strcpy( arg, ch->alloc_ptr );
              DISPOSE( ch->alloc_ptr );
              break;

      case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your arc lightning.\n\r", ch );
            act( AT_GREEN, "$n's cancels $s arc lightning.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_arclightning ) )
    {
        pager_printf( ch, "&GWith %s&G lightning dancing around your fingers, you point at %s and send a bolt of electricity right through %s!\n\r", ch->energycolour, PERS( victim, ch ) , victim->sex == 2 ? "her" : "his" );
        pager_printf( victim, "&GWith %s&G lightning dancing around %s fingers, %s points at you and sends a bolt of electricity at you!\n\r", ch->energycolour, ch->sex == 2 ? "her" : "his", PERS( ch, victim ) );
        act( AT_GREEN, "$n sends a bolt of lightning right through $N!\n\r", ch, NULL, victim, TO_NOTVICT);
        learn_from_success( ch, gsn_arclightning );
        global_retcode = damage( ch, victim, (number_range(750, 1250)), gsn_arclightning );
    }
    else
    {
        learn_from_failure( ch, gsn_arclightning );
        send_to_char( "You miss-time your arc lightning.\n\r", ch);
        act( AT_GREEN, "$n miss-times $s attack.\n\r", ch, NULL, victim, TO_CANSEE );
        global_retcode = damage( ch, victim, 0, gsn_arclightning );
    }
return;
}

void do_hyperrunning( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA /**location = NULL,*/ *in_room;
    CHAR_DATA *fch;

    one_argument( argument, arg );
    one_argument( argument, arg2 );
    if ( !str_cmp( arg, "Running") || !str_cmp( arg, "running" ))
        sprintf( arg, "%s", arg2);

    if ( ch->position <= POS_SLEEPING )
    {
    send_to_char( "Try waking up first\n\r", ch );
    return;
    }

if ( xIS_SET(ch->act, PLR_MISSIONING) )
{
send_to_char( "System Error: Missing Cheapness Subroutines\n\r", ch );
return;
}

    if ( arg[0] == '\0' )
    {
        send_to_char( "Hyper Run to who?\n\r", ch );
        return;
    }

    if ( is_number(arg) )
    {
	     send_to_char( "That doesn't sound much like somebody's name...\n\r", ch );
         return;
    }

    if ( (fch=(get_char_world(ch, arg))) )
    {
       if ( !IS_NPC(fch) && get_trust(ch) < get_trust(fch) &&   IS_SET(fch->pcdata->flags, PCFLAG_DND) && IS_IMMORTAL(fch) )
       {
          pager_printf( ch, "Why the hell would you want to see their ugly mug?\n\r", fch->name);
          return;
       }
    }

    if ( !fch )
    {
       send_to_char( "Who?!\n\r", ch );
       return;
    }

    if ( fch == ch || fch->in_room->vnum == ch->in_room->vnum || IS_SET(fch->in_room->room_flags, ROOM_NO_ASTRAL) || 
	IS_SET( ch->in_room->room_flags, ROOM_NO_ASTRAL) )
	{
        send_to_char( "That'd be interesting to watch.\n\r", ch );
        return;
	}

    if ( xIS_SET(ch->act, PLR_KILLER ) )
    {
	send_to_char( "Not with a PK Timer...\n\r", ch );
	return;
    }

    if ( ( !fch->in_room->area->planet || !ch->in_room->area->planet ) && ch->in_room->area != fch->in_room->area )
    {
    	send_to_char( "They aren't anywhere nearby for you to run to.\n\r", ch );
    	return;
    }
	
    if ( ch->in_room->area->planet && fch->in_room->area->planet && ch->in_room->area->planet != fch->in_room->area->planet)
    {
    	send_to_char( "They aren't on this planet right now.\n\r", ch );
    	return;
    }

    in_room = ch->in_room;
    if ( ch->fighting )
    {
        send_to_char( "Maybe AFTER you're not having the crap beaten out of you.\n\r", ch );
        return;
    }

    WAIT_STATE( ch, 8 );
    if ( !can_use_skill( ch, number_percent(), gsn_hyperrunning ) )
    {
    	learn_from_failure( ch, gsn_hyperrunning );
    	send_to_char( "You start to run, but trip over in a hilariously irritating manner.\n\r", ch );
    	act( AT_GREEN, "$n starts to run away, but falls over.  Perhaps you should laugh at $m.\n\r", ch, NULL, NULL, TO_ROOM );
    	return;
    }
  	learn_from_success( ch, gsn_hyperrunning );
   	act( AT_GREEN, "$n runs away so fast that you can't see which way $e ran!\n\r", ch, NULL, NULL, TO_ROOM );
   	char_from_room( ch );
   	char_to_room( ch, fch->in_room );
   	send_to_char( "&RYou duck right down and begin to run faster than light!  Very soon you arrive where you wanted to.\n\r", ch );
   	act( AT_GREEN, "$n charges in to the immediate vacinity, scaring the crap out of you!\n\r", ch, NULL, NULL, TO_ROOM );
  	do_look( ch, "auto" );
	mprog_greet_trigger( ch );
	if ( IS_SET( ch->in_room->room_flags, ROOM_DOCK ) && ch->pcdata->learned[gsn_orbitallaunch] < 10 )
	{
	    send_to_char( "&R^zMaybe you could extend this skill, some how...^x\n\r&GINCOMING DATA STREAM.... STAND BY....\n\r", ch );
	    send_to_char( "&G0000101001010101010101010010100101010101001010101\n\r",ch );
	    send_to_char( "&G1000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010010100101010101010101001010010101010100010101\n\r",ch  );
	    send_to_char( "&G0010100001010011010010110101010010101011001010101\n\r",ch  );
	    send_to_char( "&G1110101101001101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010101000101010101010101001010010101010100100101\n\r",ch  );
	    send_to_char( "&G1101000010100101001010101010010001010100100101010\n\r",ch  );
	    send_to_char( "&G0101010000101001010101010101010010100101010101111\n\r",ch  );
	    send_to_char( "&G0000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G0010100001010011010010110101010010101011001010101\n\r",ch  );
	    send_to_char( "&G1110101101001101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010101000110001101100101001100101010101001010101\n\r",ch  );
	    send_to_char( "&G1101000010100101001010101010010001010100100101010\n\r",ch  );
	    send_to_char( "&G0000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010010100101010101010101001010010101010100010101\n\r",ch  );
	    send_to_char( "&G0010100001010011010010110101010010101011001010101\n\r",ch  );
	    send_to_char( "&G1110101101001101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010101000101010101010101001010010101010100100101\n\r",ch  );
	    send_to_char( "&G1010101000110001101100101001100101010101001010101\n\r",ch  );
	    send_to_char( "&G1101000010100101001010101010010001010100100101010\n\r",ch  );
	    send_to_char( "&G0101010000101001010101010101010010100101010101111\n\r",ch  );
	    send_to_char( "&G0000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1000101001010101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010010100101010101010101001010010101010100010101\n\r",ch  );
	    send_to_char( "&G0010100001010011010010110101010010101011001010101\n\r",ch  );
	    send_to_char( "&G1110101101001101010101010010100101010101001010101\n\r",ch  );
	    send_to_char( "&G1010101000101010101010101001010010101010100100101\n\r",ch  );
	    send_to_char( "&G1010101000110001101100101001100101010101001010101\n\r",ch  );
	    send_to_char( "&G1101000010100101001010101010010001010100100101010\n\r",ch  );
	    send_to_char( "&G0101010000101001010101010101010010100101010101111\n\r",ch  );
	    ch->pcdata->learned[gsn_orbitallaunch] = 10;
	    return;
	}
    return;
}

void do_razorbeam( CHAR_DATA *ch, char *argument )
{
CHAR_DATA *victim;
char arg [MAX_INPUT_LENGTH];
 char buf[MAX_STRING_LENGTH];
if ( ( victim = who_fighting( ch ) ) == NULL )
{
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
}

switch( ch->substate )
    {
    default:
        ch->skill_timer = 16;
        add_timer( ch, TIMER_DO_FUN, 16, do_razorbeam, 1 );
        pager_printf(ch, "&GYou take a moment to calculate %s's movements!\n\r", PERS(victim,ch) );
        pager_printf(victim, "%s almost looks like %s is studying your moves for some reason!", PERS(ch,victim), ch->sex == 2 ? "she" : "he" );
        act( AT_GREEN, "$n seems to be watching $N's every move!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your razorbeam was interupted!\n\r", ch );
          act( AT_GREEN, "$n's razorbeam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming your razorbeam!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s razorbeam!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_razorbeam ) )
    {
	if (number_range(1, 100) < 4)
	{
	        learn_from_success( ch, gsn_razorbeam );
		sprintf (buf, "&RYou point your fingers out and and slash %s in half!!!", PERS( victim , ch ) );
		pager_printf(ch, "%s\n\r",buf);
		sprintf (buf, "&R%s makes a slashing motion with %s finger and cuts you in two!!", PERS(ch,victim), ch->sex == 2? "her":"his"); 
		pager_printf (victim, "%s\n\r",buf);
		act( AT_RED, "$n slashes $N into two pieces with a quick gesture of the hand!!",  ch, NULL, victim, TO_NOTVICT);
		raw_kill(ch, victim);
	}
    	else
	{ 
        	learn_from_success( ch, gsn_razorbeam );
 	        pager_printf(ch, "&GYou point your fingers at %s and make a quick slash, sending a razor like beam of ki at %s\n\r", PERS(victim,ch), HIMHER(victim->sex) );
	        pager_printf(victim, "&G%s raises two fingers into the air and makes a quick slash, sending a razor like kibeam in your direction!\n\r", PERS(ch,victim) );
        	act( AT_GREEN, "$n raises two fingers into the air and makes a quick slash, sending a razor like kibeam at $N!",  ch, NULL, victim, TO_NOTVICT );
	        global_retcode = damage( ch, victim, number_range( 5000, 7500 ), gsn_razorbeam );
	}
    }
    else
    {

        learn_from_failure( ch, gsn_razorbeam );
        pager_printf(ch, "&G%s dodges your razorbeam with amazing speed!\n\r", PERS(victim,ch) );
        pager_printf(victim, "&G%s sends a thin razor like kibeam towards you but you quickly dodge out of the way!\n\r", PERS(ch,victim) );
        act( AT_GREEN, "$n misses $N with a razorbeam but manages to slash up the scenery",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_razorbeam );
    }
return;
}

void do_dfp( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
// char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 int bonus;

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 16;
        add_timer( ch, TIMER_DO_FUN, 16, do_dfp, 1 );
        pager_printf(ch, "&YThe power of the dragon begins to flow through your body, and your fist glows bright %s&Y.\n\r", ch->energycolour );
        act( AT_MAGIC, "$n's fist begins to glow vibrantly, and you feel the power of the dragons being drawn to $m!", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Dragonfist Punch was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Dragonfist Punch was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your Dragonfist Punch.\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Dragonfist Punch.\n\r",  ch, NULL, victim, TO_CANSEE );
	    ch->skill_timer = 0;
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(), gsn_dfp ) )
    {
        learn_from_success( ch, gsn_dfp );
        pager_printf(ch, "&YWith the power of all the dragons in the universe flowing through your body, you turn into pure energy and you fly straight at, into and through %s, leaving a gaping hole!  The image of Shenron is left streaking through the air behind you, as you reform back into a solid mass.\n\r", PERS(victim,ch) );
        pager_printf(victim, "&GWith all the power of the dragons concentrated on %s, %s flies right at you, tearing a huge hole in your stomach!\n\r", ch->sex == 2 ? "her" : "him", ch->name );
        act( AT_GREEN, "$n screams 'Ultimate Dragonfist!' and torpedoes straight through $N, leaving an enormous hole in $M, leaving the image of Shenron streaking through the air!",  ch, NULL, victim, TO_NOTVICT );
        bonus = ( get_curr_str(ch) + get_curr_wis(ch) + get_curr_dex(ch) + get_curr_lck(ch) + get_curr_cha(ch) + get_curr_con(ch) );
	if ( bonus > 25000 ) bonus = 25000;
        global_retcode = damage( ch, victim, bonus, gsn_dfp );
    }
    else
    {
        learn_from_failure( ch, gsn_dfp );
        pager_printf(ch, "&GAll the power of the dragons just dispells into the air...\n\r", victim->name );
        act( AT_GREEN, "$n screams 'Ultimate Dragonfist!' but all $s energy just fizzles away.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_dfp );
    }
return;
}

void do_shogekiha( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_shogekiha, 1 );
        pager_printf(ch, "&GYou begin trying to predict %s's next few moves.\n\r", PERS(victim,ch) );
        pager_printf(victim, "%s appears to be predicting your every movement before you make it!", PERS(ch,victim) );
        act( AT_GREEN, "$n seems to be predicting $N's every move!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your shogekiha was interupted!\n\r", ch );
          act( AT_GREEN, "$n's shogekiha was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You cancel your shogekiha!\n\r", ch );
            act( AT_GREEN, "$n cancels $s shogekiha!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_shogekiha ) )
    {
        learn_from_success( ch, gsn_shogekiha );
        pager_printf(ch, "&GYour hand comes forward in front of you.  You rear back and swipe with an open palm through the air. An invisible ki force lands onto %s stunning %s!" , PERS(victim, ch) , ch->sex == 2 ? "her" : "him" );
        pager_printf(victim, "&G%s's hand comes forward in front of %s.  %s rear's back and swipe's an open palm through the air. An invisible Ki force lands onto you leavening you partly stunned!\n\r", PERS(ch, victim) , PERS(ch, victim), PERS(ch, victim));
        act( AT_GREEN, "$n's hand comes forward in front of $m $e rare's back and swipe's an open palm through the air. an invisible Ki force lands onto $N! leaving $m partly stunned!",  ch, NULL, victim, TO_NOTVICT );
        damage( ch, victim, number_range( 1500, 3500) , gsn_shogekiha );
    }
    else
    {

        learn_from_failure( ch, gsn_shogekiha );
        pager_printf(ch, "&G%s manages to dodge out of the way avoiding the full force of the attack!\n\r", PERS(victim,ch));
        pager_printf(victim, "&GYou manage to dodge out of the way just in the nick of time, avoiding the full force of %s's attack!\n\r", PERS(ch,victim) );
        act( AT_GREEN, "$N dodges the full force of $n's attack!",  ch, NULL, victim, TO_NOTVICT );
    }
return;
}

void do_wolffang ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
        send_to_char( "You aren't fighting anyone.\n\r", ch );
        return;
    }

    switch( ch->substate )
    {
        default:
	        ch->skill_timer = 8;
                add_timer( ch, TIMER_DO_FUN, 8, do_wolffang, 1 );
		pager_printf( ch, "&GYou jump back a few feet and bend down and stretch out for a second.\n\r" );
                act( AT_GREEN, "$n jumps back a few feet and bends down and starts stretching out for a few seconds.", ch, NULL, NULL, TO_CANSEE );
                ch->alloc_ptr = str_dup( arg );
                return;
         case 1:
                if ( !ch->alloc_ptr )
                {
                     send_to_char( "Your Wolf Fang First was interupted!\n\r", ch );
                     act( AT_GREEN, "$n's Wolf Fang First was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
                     bug( "do_search: alloc_ptr NULL", 0 );
                     return;
                }
                strcpy( arg, ch->alloc_ptr );
                DISPOSE( ch->alloc_ptr );
                break;

         case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
                send_to_char( "You stop your Wolf Fang First.\n\r", ch );
                act( AT_GREEN, "$n's cancels $s Wolf Fang First.\n\r",  ch, NULL, victim, TO_NOTVICT );
                return;
    }

    ch->substate = SUB_NONE;
         if ( can_use_skill(ch, number_percent(),gsn_wolffang) )
         {
	    int hits = number_range(4,7);
            learn_from_success( ch, gsn_wolffang);
            pager_printf(ch, "&GYou Charge at %s with all your strength, screeming &RWolf Fang Fist &Gpunching %s  6 times, Then with a final howl you launch your fist uppward hitting %s in the face sending %s flying!\n\r", PERS( victim, ch ), victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "him" );
            pager_printf(victim, "&G %s Charges at you with all %s strength, screeming &RWolf Fang Fist &Gpunching you 6 times, Then with a final howl %s launches %s fist uppward hitting you in the face sending you flying!\n\r", PERS( ch, victim ), victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "him" );
            act( AT_GREEN, "&G $n Charges at $N with all $S strength, screeming &RWolf Fang Fist &Gpunching $N 6 times, Then with a final howl $n launches $S fist uppward hitting $N in the face sending $M flying!\n\r",  ch, NULL, victim, TO_NOTVICT );
	
            global_retcode = damage( ch, victim, (number_range(75, 150)), gsn_wolffang );
            global_retcode = damage( ch, victim, (number_range(87, 200)), gsn_wolffang );
            global_retcode = damage( ch, victim, (number_range(100, 250)), gsn_wolffang );
            global_retcode = damage( ch, victim, (number_range(150, 300)), gsn_wolffang );
	    if ( hits >= 5 )
                global_retcode = damage( ch, victim, (number_range(300, 550)), gsn_wolffang );
	    if ( hits >= 6 )
	        global_retcode = damage( ch, victim, (number_range(550, 900)), gsn_wolffang );
	    if ( hits >= 7 )
	        global_retcode = damage( ch, victim, (number_range(1000, 2500)), gsn_wolffang );
         }
         else
         {
            learn_from_failure( ch, gsn_wolffang);
            pager_printf( ch, "&GYou fail to accomplish anything, and %s &Csays 'What was that supposed to do? make me look weak? or make you look stupid!'\n\r", PERS(victim,ch));
            pager_printf( victim, "&CYou say 'What was that supposed to do? make me look weak? or make you look stupid! \n\r");
            act( AT_GREEN, "$n stops stretching as $N &CSays 'What was that supposed to do? make me look weak? or make you look stupid!.\n\r",  ch, NULL, victim, TO_NOTVICT );
            learn_from_failure( ch, gsn_wolffang);
            global_retcode = damage( ch, victim, 0, gsn_wolffang);
         }
    return;
}


void do_bakurikimahaan( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 10;
        add_timer( ch, TIMER_DO_FUN, 10, do_bakurikimahaan, 1 );
        sprintf( buf, "&GYou jump back, landing a very good distance away, then raise your right arm (bracing your wrist with your left hand) as a large %s ball of energy begins forming!\n\r", ch->energycolour );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s takes a leap backwards landing a great distance away and raises %s right wrist with %s left hand and a large %s ball of energy begins forming!\n\r", PERS(ch,victim), ch->sex == 2 ? "her" : "his"  , ch->sex == 2 ? "her" : "his",ch->energycolour );  pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n takes a giant leap away from $N and bring's $s right arm forward braceing it with $s left hand as a large Ball of energy begins forming!\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;
	case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your bakurikimahaan was interupted!\n\r", ch );
          sprintf( buf, "&G%s's bakurikimahaan was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's bakurikimahaan was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your bakurikimahaan\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch-> sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch-> sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s bakurikimahaan!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
 }

   ch->substate = SUB_NONE;

   if ( can_use_skill(ch, number_percent(), gsn_bakurikimahaan ) )
   {
      learn_from_success( ch, gsn_bakurikimahaan );
      sprintf( buf, "&GYou glare at %s as you finish charging your attack, and a large beam emerges from the ball of ki that you formed, pounding %s into the ground!\n\r", PERS( victim, ch ) , PERS( victim, ch ));
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s gives you an evil look as a large beam emerges from the ball of energy %s formed, pounding you into the ground!\n\r", ch->name, HESHE( ch->sex) );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "$n gives $N an evil look as a large beam emerges from $s ball of Ki, pounding $N into the ground!", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, number_range( 900, 1400 ),  gsn_bakurikimahaan );
   }
   else
   {
      learn_from_failure( ch, gsn_bakurikimahaan );
      sprintf( buf, "&RAs soon as you gather some ki it just discharges from your hand, totally missing %s.\n\r" , PERS( victim, ch ) );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&R%s miss-fires %s attack and totally misses you!\n\r", ch->name, HISHER(ch->sex) );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "$n miss-fires $s attack totally missing $N.\n\r", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, 0, gsn_bakurikimahaan );
      }
return;
}

/* Final Flash 
void do_multiroomfinalflash(CHAR_DATA *ch, char *argument)
{

     CHAR_DATA *victim;
//     sh_int level;
//     sh_int mastery;
     char arg1[MAX_STRING_LENGTH];

     char arg2[MAX_STRING_LENGTH];
     ROOM_INDEX_DATA *to_room;

     ROOM_INDEX_DATA *in_room;
     EXIT_DATA *pexit = NULL;
     int dist = 0;
     int dir = 0;
     int MAX_DIST = 3;

     argument = one_argument( argument, arg1 );
     argument = one_argument( argument, arg2 );

        
     if ( arg1[0] == '\0' )
        {
           send_to_char("Who is your victim of your destruction?\n\r", ch);
           return;
        }
     in_room = ch->in_room;
     to_room = ch->in_room;

     if ( ( victim = who_fighting(ch) ) == NULL )
        {
           if ( arg2[0] == '\0' )
           {
              if ( ( victim = get_char_room( ch, arg1 ) ) == NULL )
              {
                  send_to_char("You victim is not in you sight.\n\r", ch );
                  return;
              }
           }
           for ( dir = 0; dir < MAX_DIR; dir++ )
	   {
	           if ( !str_cmp( arg2, dir2_name[dir] ) )
        	   break;
	   }
        }
     if ( dir == MAX_DIR )
        {
           send_to_char("What way is your victim?\n\r", ch );
           return;
        }

     pexit = get_exit(get_room_index( ch->in_room->vnum ), dir);

     if ( dir2_name[dir] == NULL || ( to_room = pexit->to_room ) == NULL )
        {
          send_to_char("You just barly deverted your attack form blowing up the planet. The way you choose all most killed you, pick a new one!\n\r", ch );
           return;
        }

     if ( IS_SET( pexit->exit_info, EX_CLOSED ) )
        {
           send_to_char("You attack can't go through a energy barrier thats a door.\n\r", ch );
           return;
        }

     if ( IS_SET( pexit->exit_info, EX_SECRET ) )
        {
           send_to_char("You attack can't go through a energy barrier thats in that direction.\n\r", ch );
           return;
        }
     if ( number_percent() > ch->pcdata->learned[gsn_multiroomfinalflash] + 
         (get_curr_int( ch ) / 200))
        {
           send_to_char("You could not gather the energy!\n\r", ch );
           return;
        }
     for ( dist = 1; dist <= MAX_DIST; dist++ )
        {
           char_from_room( ch );
           char_to_room( ch, to_room );
           {
              if ( ( victim = get_char_room( ch, arg1 ) ) != NULL )
              break;
           }
        }

     if ( dir2_name[dir] == NULL ||( to_room = pexit->to_room ) == NULL )
        {
           char_from_room( ch );
           char_to_room( ch, in_room );
           send_to_char("Your attack was so weak it did not reach your victim./n/r", ch );
           return;
        }

     if ( victim == NULL )
        {
           send_to_char("You failed and feel ashamed of your poor ability.\n\r", ch );
           char_from_room( ch );
           char_to_room( ch, in_room );

           act(AT_PLAIN, "$n tires to use final blast and fails. He must be a weekling!", ch, NULL, NULL, TO_CANSEE);
           return;
        }
     if ( dist > 0 )
        {
           char_from_room( ch );
           char_to_room( ch, in_room );
           act( AT_WHITE, "A massive blast of energy flies in and hits %s!", victim, NULL, victim, TO_CANSEE );
           send_to_pager("A massive blast of energy flies in and hits you!", victim);
           act( AT_RED, "Your final blast hits $N!", ch, NULL, victim, TO_CHAR );
        }
        WAIT_STATE( ch, skill_table[gsn_multiroomfinalflash]->beats );
        if ( can_use_skill(ch, number_percent(),gsn_multiroomfinalflash ) )
           {
	         learn_from_success( ch, gsn_multiroomfinalflash );
              
	             global_retcode = damage( ch, victim, number_range( 1000,1500), gsn_multiroomfinalflash );
           }
        else
           {
	         learn_from_failure( ch, gsn_multiroomfinalflash );
	         global_retcode = damage( ch, victim, 0, gsn_multiroomfinalflash );
           }
        return;
}
*/

void do_multiroomfinalflash(CHAR_DATA *ch, char *argument)
{
     CHAR_DATA  * victim;
     char         arg[MAX_INPUT_LENGTH];
     char	  arg2[MAX_INPUT_LENGTH];
     char 	  buf[MAX_STRING_LENGTH];
     ROOM_INDEX_DATA  * nroom;
     EXIT_DATA  * pexit;
     int          size;
     int          dist;
     int          dam = 0;
     int          energy = 0;
     int          dir = DIR_NORTH;
     bool         found = FALSE;
     
     argument = one_argument( argument, arg );
     argument = one_argument( argument, arg2 );
     
     if ( ch->genotimer > 0 )
     {
        pager_printf( ch, "Not for another %d minutes and %d seconds.\n\r", ch->genotimer / 60, ch->genotimer % 60 );
        return;
     }
     
     for ( ; ; ) /* Make sure they've got a real direction */
     {
         if ( dir > MAX_DIR )
            break;
         if ( !str_cmp( arg, dir2_name[dir] ) ) /* We've got our dir */
         {
            found = TRUE;
            break;
         }
         dir++;
         continue;          
     }
     
     if ( !found || found == FALSE ) /* No such direction */
     {
        pager_printf( ch, "Choose a direction: (North, South, etc) that actually exists!\n\r" );
        return;
     }
     
     /* get our size */
     if ( !is_number(arg2) )
     {
        pager_printf( ch, "You must pick a percentage of your energy to put into this attack.\n\r" );
        return;
     }

     size = atoi(arg2);
     if ( size < 1 || size > 100 )
     {
        pager_printf( ch, "You must put at least 1%% into this attack and no more than 100%%\n\r" );
        return;   
     }

     /* Valid Size, Valid Direction, Does exit exist that way? */
     found = FALSE;
     for ( pexit = ch->in_room->first_exit; pexit; pexit = pexit->next )
     {
         if ( pexit->vdir == dir )
         {
              found = TRUE;
              break;
         }              
     }
     
     if ( found == FALSE )
     {
        pager_printf( ch, "There's nothing in that direction.\n\r" );
        return;
     }
     
     /* Everything ok, proceed to check for energy */
     energy = ch->max_mana / 100;
     energy *= size;
     dist = size / 10;
     if ( dist == 0 )
        dist = 1;
     
     if ( ch->mana < energy )
     {
        pager_printf( ch, "You need at least %d energy to use this attack.\n\r", energy );
        return;
     }
     
     if ( ch->position < POS_STANDING )
     {
        pager_printf( ch, "You have to be standing to use this attack.\n\r" );
        return;
     }
     
     if ( ch->plmod < 2000 )
     {
        pager_printf( ch, "With that body?!\n\r" );
        return;
     }
     
     if ( xIS_SET( ch->act, PLR_KILLER ) )
     {
        pager_printf( ch, "You're too much of a disgrace to your race to do that now.\n\r" );
        return;
     }
     
     ch->mana -= energy;
                if ( ch->mana < 0 )
                        ch->mana = 0;

     dam = ( get_curr_wis(ch) / 5 ) * ( number_range( 500, 2000 ) );
     dam = dam / 100;
     dam = dam / 100 * size;
     
     if ( number_range( 1, 200 ) < size )
     {
          pager_printf( ch, "You completely overcharged your attack and blew yourself up!!!\n\r" );
          sprintf( buf, "&R%s explodes in a glorious shower of %s&Rki and &Rblood!\n\r", ch->name, ch->energycolour );
          act( AT_RED + AT_BLINK, buf, ch, NULL, NULL, TO_CANSEE );
          raw_kill( ch, ch );
          return;
     }
          
     if ( can_use_skill( ch, number_percent(), gsn_multiroomfinalflash ) && dam > 0 )
     {
          ch->skill_timer = ( 10 + ( size / 10 ) );
          WAIT_STATE( ch, ( ( 10 + ( size / 10 ) ) * PULSE_PER_SECOND ) );
          pager_printf( ch, "&CYou bring both your hands behind your back and make a pair of large energyballs.  Grinning evilly you swing both arms in front of you, screaming \"Final Flash!\", and launch a huge %s&C beam of ki %s!\n\r", ch->energycolour, dir2_name[dir] );
          act( AT_PINK, "$n brings both $s arms behind $s back and fires a huge Final Flash off in another direction!", ch, NULL, NULL, TO_CANSEE );
          nroom = get_room_index( pexit->to_room->vnum );
          sprintf( buf, "%s's Final Flash flies in, catching you off guard!!", ch->name );
          echo_to_room( AT_RED + AT_BLINK, nroom, buf );
          for ( victim = nroom->first_person; victim; victim = victim->next_in_room )
          {
              if ( xIS_SET( victim->act, PLR_KILLER ) )
                 continue;
              pager_printf( victim, "&R%s's %s&R Final Flash flies in and envelops you completely!\n\r", ch->name, ch->energycolour );
              damage( ch, victim, dam, gsn_multiroomfinalflash );
              learn_from_success( ch, gsn_multiroomfinalflash );
          }
          dist--;
          if ( dist <= 0 )
             return;
          for ( ; ; )
          {
              if ( dist <= 0 )
                  break;
              found = FALSE;
              for ( pexit = nroom->first_exit; pexit; pexit = pexit->next )
              {
                  if ( pexit->vdir == dir )
                  {
                      found = TRUE;
                      break;
                  }              
              }
              if ( found == FALSE )
                 break;
              nroom = get_room_index( pexit->to_room->vnum );
              sprintf( buf, "%s's Final Flash flies in, catching you off guard!!", ch->name );
              echo_to_room( AT_RED + AT_BLINK, nroom, buf );
              if ( !IS_SET( nroom->room_flags, ROOM_TRASHED ) )
                   SET_BIT( nroom->room_flags, ROOM_TRASHED );
              for ( victim = nroom->first_person; victim; victim = victim->next_in_room )
              {
                  ch->genotimer += 15;
                  /* Guard against weird looping rooms */                
                  if ( xIS_SET( victim->act, PLR_KILLER ) || ch == victim )
                     continue;
                  pager_printf( victim, "&R%s's %s&R Final Flash flies in envelops you completely!\n\r", ch->name, ch->energycolour );
                  damage( ch, victim, dam, gsn_multiroomfinalflash );
                  learn_from_success( ch, gsn_multiroomfinalflash );
              }
              dist--;
          }
          
     }
     else
     {
         pager_printf( ch, "Your attack fails to work the way it should.\n\r" );
         ch->genotimer = 60;
         learn_from_failure( ch, gsn_multiroomfinalflash );
     }
 
}
void do_mysticflash( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 int i;
 char arg [MAX_INPUT_LENGTH];
 ROOM_INDEX_DATA *pRoomIndex = NULL;

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }
 if ( IS_NPC(victim) )
 {
      send_to_char( "&YYou haven't the power to succeed against them.\n\r", ch );
      return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_mysticflash, 1 );
        pager_printf(ch, "&GYou raise both of your arms and point spread your fingers, aiming at %s\n\r", PERS(victim,ch) );
        pager_printf(victim, "%s raises both of %s arms and spreads %s fingers while aiming at you", PERS(ch,victim), ch->sex == 2 ? "her" : "him", HISHER(ch->sex) );
        act( AT_GREEN, "$n brings both arms forward, fingers spread wide, aiming at $N!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Heavenly Gale was interupted!\n\r", ch );
          act( AT_GREEN, "$n's skill was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You cancel your skill.\n\r", ch );
            act( AT_GREEN, "$n cancels $s skill.\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    for ( i = 0; i < 200 ; i++ ) 
    {
        pRoomIndex = get_room_index( number_range( ch->in_room->area->low_r_vnum+1, ch->in_room->area->hi_r_vnum-1  ) );
        if ( pRoomIndex )
            if ( !IS_SET(pRoomIndex->room_flags, ROOM_SOLITARY)
            &&   !IS_SET(pRoomIndex->room_flags, ROOM_PRIVATE)
            &&   !IS_SET(pRoomIndex->room_flags, ROOM_PROTOTYPE) )
             break; 
    }
    if ( can_use_skill(ch, number_percent(), gsn_mysticflash) && pRoomIndex )
    {
	CHAR_DATA *vch;
        learn_from_success( ch, gsn_mysticflash);
        pager_printf(ch, "&GA powerful thrust of air blasts %s far into the sky, where, after twinkling briefly, %s comes crashing down somewhere else in the area.\n\r" , PERS(victim,ch), HESHE(victim->sex) );
        pager_printf(victim, "&GBefore you can realise what's going on, you find yourself high in the air, blasted right up!  Spinning madly, and paralysed, you slam back down into the ground somewhere else in the area!\n\r" );
        act( AT_GREEN, "$n fires a gust of air at $N, blasting him up into the sky to come crashing down somewhere nearby!",ch, NULL, victim, TO_NOTVICT );
        victim->skill_timer = 4;
        WAIT_STATE( victim, 4 * PULSE_VIOLENCE );
	stop_fighting( victim, TRUE );
	char_from_room( victim );
	char_to_room( victim, pRoomIndex );
	victim->position = POS_RESTING;
	act( AT_MAGIC, "THUD!  $n slams into the ground in a sudden gust of wind, collapsed.", victim, NULL, NULL, TO_ROOM );
	for (vch = first_char; vch; vch = vch->next )
	    if ( vch->in_room->area == victim->in_room->area && victim != vch )
		send_to_char( "&w&WYou hear a loud thud, followed by a scream of agony.  A cloud of dust rises somewhere.\n\r", vch );
	do_look( victim, "auto" );
	victim->hit -= number_range( 1000, 2000 );
	return;
    }
    else
    {

        learn_from_failure( ch, gsn_mysticflash);
        pager_printf(ch, "&G%s just looks at you and laughs; 'You are really a pathetic Kaio!'\n\r", PERS(victim,ch));
        pager_printf(victim, "&GYou look at %s and say 'You really are pathetic!'\n\r", PERS(ch,victim) );
        act( AT_GREEN, "$N looks at $n and says 'You're pathetic!'",  ch, NULL, victim, TO_NOTVICT );
    }
return;
}

void do_henkacannon ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
    }

    if ( IS_NPC(victim) )
    {
         send_to_char( "They don't look like they'd taste good.\n\r", ch );
         return;
    }

    if ( ch->steam < 25 )
    {
         send_to_char( "You need 25 steam for this skill.\n\r", ch );
         return;
    }

    switch( ch->substate )
    {
        default:
          ch->skill_timer = 12;
          add_timer( ch, TIMER_DO_FUN, 12, do_henkacannon, 1 );
          pager_printf( ch, "&GYou bring back your Head Tentacle and glare angrily at %s as you release clouds of steam in huge %s&G bursts.", victim->name, ch->energycolour );
          pager_printf( victim, "&G%s brings back %s Head Tentacle and glares at you irritably, slowly releasing steam in great big %s &Gbursts", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour );
          act( AT_GREEN, "$n brings back $s Head Tentacle and glares at $N as $e continues to release big clouds of steam.", ch, NULL, victim, TO_NOTVICT );
          ch->alloc_ptr = str_dup( arg );
          return;
        case 1:
          if ( !ch->alloc_ptr )
          {
                 send_to_char( "Your Henka Cannon was interupted!\n\r", ch );
                 pager_printf( victim, "&G%s's Henka Cannon was interrupted!", ch->name );
                 act( AT_GREEN, "$n's Henka Cannon was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
                 bug( "do_search: alloc_ptr NULL", 0 );
                 return;
          }
          strcpy( arg, ch->alloc_ptr );
          DISPOSE( ch->alloc_ptr );
          break;
        case SUB_TIMER_DO_ABORT:
          DISPOSE( ch->alloc_ptr );
          ch->substate = SUB_NONE;
          send_to_char( "You stop your Henka Cannon.\n\r", ch );
          act( AT_GREEN, "$n's cancels $s Henka Cannon.\n\r",  ch, NULL, victim, TO_CANSEE );
          return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_henkacannon) )
    {
          ch->steam -= 25;
          learn_from_success( ch, gsn_henkacannon);
          pager_printf( ch, "&GYou glower angrily at %s before flinging your head tentacle forwards and shouting &R'TURN INTO CANDY!!'&G  %s is zapped by the beam and turned into candy!!", victim->name, victim->name );
          pager_printf( victim, "&G%s glowers angrily at you before flinging %s head tentacle forwards and shouting &R'TURN INTO CANDY!!'&G  You are zapped by the beam and turned into candy!!", ch->name, ch->sex == 2 ? "her" : "his" );
	  if ( get_curr_dex( victim ) * number_range( 1, 8 ) < get_curr_dex( ch ) * number_range( 1, 6 ) )
          {
              sprintf( buf, "Yummy Yummy!  %s is in %s's tummy!", victim->name, ch->name );
              raw_kill( ch, victim );
              act( AT_GREEN, "$n yells 'TURN INTO CANDY' and fires a beam at $N.  It hits $M, and $n picks $M up and eats $M!!\n\r",  ch, NULL, victim, TO_NOTVICT );
              ch->perm_str+= 5;
              send_to_char( "Your strength was boosted!\n\r", ch );
              pl_gain( ch, victim, sqrt(victim->basepl / 100 ));
          }
          else
          {
              sprintf( buf, "Could it be true... %s is incredible fighting candy!", victim->name );
              act( AT_GREEN, "$n yells 'TURN INTO CANDY' and fires a beam at $N.  It hits $M, but only seems to help $M's cause!!\n\r",  ch, NULL, victim, TO_NOTVICT );
              SET_BIT( victim->pcdata->flags, PCFLAG_CANDY );
          }
          talk_info( AT_PINK, buf );
    }
    else
    {
        learn_from_failure( ch, gsn_henkacannon);
        pager_printf( victim, "You bugger up your Henka Cannon very well, and turn a passer-by into candy.  Oh well, waste-not want-not...\n\r" );
        pager_printf( ch, "%s buggers up the Henka Cannon skill and turns a passer-by into candy.  Oh well, waste-not, want-not...\n\r", ch->name );        
        act( AT_GREEN, "$n fires fires Henka Cannon at $N but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
        learn_from_failure( ch, gsn_henkacannon);
        ch->hit += 10;
        if ( ch->hit > ch->max_hit )
           ch->hit = ch->max_hit;
        global_retcode = damage( ch, victim, 0, gsn_henkacannon);
    }

return;
}
void do_hyperrage( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

   if ( ( victim = who_fighting( ch ) ) == NULL )
    {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
    }

    if ( IS_NPC(victim) )
    {
         send_to_char( "You dont need such an attack to kill them!\n\r", ch );
         return;
    }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 12;
        add_timer( ch, TIMER_DO_FUN, 12, do_hyperrage, 1 );
        pager_printf(ch, "&GYou glance at %s knowing that there is only way to deal with them!\n\r", PERS(victim,ch) );
        pager_printf(victim, "&G%s glance's up at you, %s aura flares up as they stare at you.", PERS(ch,victim), ch->sex == 2 ? "her" : "him" );
        act( AT_GREEN, "$n looks up at $N, $n's aura flares brightly as they stare $N in the face!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Hyper Rage was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Hyper Rage was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You cancel your Hyper Rage Attack!\n\r", ch );
            act( AT_GREEN, "$n cancels their Hyper Rage Attack!!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(), gsn_hyperrage) )
    {
        learn_from_success( ch, gsn_hyperrage);
        pager_printf(ch, "&GYou lower your head and grasp the power you have collected and begin your assault!" , PERS(victim,ch) );
        pager_printf(victim, "&G%s lower;s their head and seems to be grasping an unknown power and begins to attack at random!\n\r", PERS(ch,victim));
        act( AT_GREEN, "$n lower's $s head and grasp's some sort of unknown energy and begins attacking $N at radom!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 100, 300)* get_curr_str(ch) / 800, gsn_hyperrage);
        WAIT_STATE( ch, 3 * PULSE_VIOLENCE );
        pager_printf(ch, "&GWith the last bit of energy you collected you fire a large ball of energy at %s in an attempt to finsh the job causeing a massive explosion!" , PERS(victim,ch) );
        pager_printf(victim, "&G%s takes a small moment to grasp their remaining energy to launch a large beam of ki at you in attempt to finish you off for good!\n\r", PERS(ch,victim));
        act( AT_GREEN, "$n fire's a large beam of ki in a last ditch effort to destory $N once and for all!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 1000, 3000)* get_curr_wis(ch) / 1000, gsn_hyperrage);
        }
        else
    {

        learn_from_failure( ch, gsn_hyperrage);
        pager_printf(ch, "&GYou lower your head as you fail to collect your energy!\n\r", PERS(victim,ch  ));
        pager_printf(victim, "&G%s lower's %s head in shame as %s fails to collect the energy needed.\n\r", PERS(ch,victim), ch->sex == 2? "her" : "his" , ch->sex == 2? "she" : "he" );
        act( AT_GREEN, "$n lowers $s head as $e fails to collect any energy!",  ch, NULL, victim, TO_NOTVICT );
    }
return;
}

void 	do_cosmicsurge ( CHAR_DATA *ch, char * argument )
{
CHAR_DATA *victim = NULL;
char arg[MAX_INPUT_LENGTH];


if ( who_fighting( ch ) != NULL )
{
     send_to_char( "You are fighting someone right now.\n\r", ch );
     return;
}

switch( ch->substate )
    {
        default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_cosmicsurge, 1 );
        pager_printf( ch, "&GYou ascend high into the air and cross your arms across your chest and lower your head and begin to collect energy from the cosmos.\n\r"  );
        act( AT_GREEN, "&G$n ascends into the air and crosses $s arms across thier chest and lowers their head and begins collecting energy from the cosmos!", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Cosmic Surge interupted!\n\r", ch );
          act( AT_GREEN, "$n's Cosmic Surge was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
        send_to_char( "You stop your Cosmic Surge, and float back down to earth.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, NULL, TO_CANSEE);
            return;
    }
    ch->substate = SUB_NONE;


if ( can_use_skill(ch, number_percent(),gsn_cosmicsurge ) )
    {
          learn_from_success( ch, gsn_cosmicsurge );
          pager_printf( ch, "&GYou raise your head and spread your arms wide, using the energy you gathered from &gthe cosmos you unleash a massive blast of energy irradiating the area with holy energy destroying all the good and evil within the area!\n\r"  );
        
	  for ( victim = ch->in_room->first_person; victim; victim = victim->next_in_room )
          {
		
		 if ( ch == victim ) continue; 
               if ( IS_NPC(victim) || IS_NPC(ch) || (victim->pcdata->clan && ch->pcdata->clan && victim->pcdata->clan == ch->pcdata->clan) )
                  continue;
	       if ( victim == ch )
		  continue;
               if (victim->in_room)
               {
	           damage( ch, victim, number_range(1500, 2000), gsn_cosmicsurge );
               }
          }

          act( AT_PINK, "&G$n raises their head and spreads their arms wide, using the energy they gathered from &gthe cosmos they unleash a massive blast of energy irradiating the area with holy energy destroying all the good and evil within the area!\n\r", ch, NULL, NULL, TO_CANSEE );
    }
    else
    {
          learn_from_failure( ch, gsn_cosmicsurge );
          pager_printf( ch, "&GYou suddenly feel unable to go ahead with your actions.....");
    }
return;
}

void do_renzoku( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:
        ch->skill_timer = 30;
        add_timer( ch, TIMER_DO_FUN, 30, do_renzoku, 1 );
	ch->renzoku = TRUE;
        sprintf( buf, "&GYou smack %s around the head and leap into the air, firing %s energyballs!\n\r", victim->name, ch->energycolour );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s smacks you in the head and leaps into the air!\n\r", PERS(ch,victim));
	pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n smacks $N around the head and leaps into the air, firing energyballs at $M!\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
            return;
        case 1:
	break;
         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your renzoku energy dan.\n\r", ch );
            act( AT_GREEN, "$n's cancels $s renzoku energy dan!\n\r",  ch, NULL, victim, TO_CANSEE );
	    ch->renzoku = FALSE;
            ch->skill_timer = 0;
            return;
 }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(), gsn_renzoku ) )
      {
      sprintf( buf, "&GTime for the big finish!  You charge a huge energy ball above your head and slam it into %s with all your might!\n\r", victim->name );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&GTime for the big finish!  %s charges a huge energyball and slams it into you with everything %s has!\n\r", ch->name, ch->sex == 2 ? "she" : "he" );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "$n slams an enormous energyball into $N inflicting huge injuries!", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage(ch, victim, ( number_range( 500, 1500 ) ), gsn_renzoku );
	ch->renzoku = FALSE;
      }
    else
      {
      learn_from_failure( ch, gsn_renzoku );
      pager_printf( ch, "You land with a satisfied grin on your face.", buf );
      act( AT_SKILL, "$n lands with a satisfied grin on $s face.\n\r", ch, NULL, NULL, TO_CANSEE );
      global_retcode = damage( ch, victim, 0, gsn_renzoku );
	ch->renzoku = FALSE;
      }
return;
}


/*
if ( ch->skill_timer > 0 )
{
           if ( ch->mana > 1000 )
           {
                ch->mana -= 500;
                if ( can_use_skill(ch, number_percent(), gsn_renzoku ) )
                {
                    learn_from_success( ch, gsn_renzoku );
                    pager_printf( ch, "&YYou fire a %s&y energyball at %s!\n\r", ch->energycolour, victim->name );
                    pager_printf( victim, "&Y%s fires a %s&Y energyball at you from above!\n\r", ch->name, ch->energycolour );
                    act ( AT_PINK, "$n hits $N with an energyball!\n\r", ch, victim, NULL, TO_CANSEE );
                    damage( ch, victim, ( number_range( 10, 100 ) * ( get_curr_wis(ch) / 100 )), gsn_renzoku );
                }
                else
                {
                    learn_from_failure( ch, gsn_renzoku );
                    pager_printf( ch, "You fire your energyball but it just slams into the ground." );
                    act ( AT_PINK, "$n's energyball misses and slams into the ground.\n\r", ch, NULL, NULL, TO_CANSEE );
                }

                ch->mana -= 500;
                if ( can_use_skill(ch, number_percent(), gsn_renzoku ) )
                {
                    learn_from_success( ch, gsn_renzoku );
                    pager_printf( ch, "&YYou fire a %s&y energyball at %s!\n\r", ch->energycolour, victim->name );
                    pager_printf( victim, "&Y%s fires a %s&Y energyball at you from above!\n\r", ch->name, ch->energycolour );
                    act ( AT_PINK, "$n hits $N with an energyball!\n\r", ch, victim, NULL, TO_CANSEE );
                    damage( ch, victim, ( number_range( 10, 100 ) * ( get_curr_wis(ch) / 100 )), gsn_renzoku );
                }
                else
                {
                    learn_from_failure( ch, gsn_renzoku );
                    pager_printf( ch, "You fire your energyball but it just slams into the ground." );
                    act ( AT_PINK, "$n's energyball misses and slams into the ground.\n\r", ch, NULL, NULL, TO_CANSEE );
                }
                return;
           }
           else
           {
               interpret(ch, "blink" );
               pager_printf( ch, "Not enough energy.\n\r" );
               return;
           }

*/

/*void do_multieyebeam( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

       switch( ch->substate )
    {
        default:

             ch->skill_timer = 4;
             add_timer( ch, TIMER_DO_FUN, 3, do_multieyebeam, 1 );
	     ch_printf( ch, "You close your eyes and begin to draw power.\n\r" );
	     ch_printf( victim, "%s closes %s eyes and begins to draw power.\n\r", ch->name, HISHER(ch->sex) );
             act( AT_GREEN, "$n looks at $N then closes $s eyes, begining to draw energy into $s eyelids.", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your eye beam was interupted!\n\r", ch );
          sprintf( buf, "&G%s's eye beam was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's eye beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case 	SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your eye beam.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s eye beam!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

         if ( can_use_skill(ch, number_percent(),gsn_multieyebeam) )
    {
          learn_from_success( ch, gsn_multieyebeam);
          sprintf( buf, "&GYou reopen your eyes with a %s tint in them as you begin fireing multiple eyebeams at %s.", ch->energycolour, PERS( victim, ch ));
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s opens %s eyes wide and fires multiple beams of ki them towards you.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her" );
          pager_printf (victim, "%s\n\r", buf );
          act( AT_GREEN, "$n stares at $N and fires multiple beams of ki from $s eyes at $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(100, 150)), gsn_multieyebeam);
          }
    else
    {
        learn_from_failure( ch, gsn_multieyebeam);
         send_to_char( "Your eye beam wizzes past your target.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s opens %s eyes and fires a beam from each one, but you duck underneath them.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her" );
         else
                 sprintf(buf, "%s fires two beams of ki from %s eyes but you leap out of the way.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires two thin bolts of ki at $N from $s eyes but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_eyebeam);
         global_retcode = damage( ch, victim, 0, gsn_multieyebeam);
    }
return;
}

void do_kaiobeam( CHAR_DATA *ch, char *argument )
{
ch_ret  one_hit args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dt ) );
ROOM_INDEX_DATA *generate_exit( ROOM_INDEX_DATA *in_room, EXIT_DATA **pexit );
CHAR_DATA *get_char_room_mp( CHAR_DATA *ch, char *argument );

   char              arg[MAX_INPUT_LENGTH];  
   char              arg2[MAX_INPUT_LENGTH];
   sh_int            dir, dist;
   sh_int            max_dist = 3;
   EXIT_DATA       * pexit;
   ROOM_INDEX_DATA * was_in_room;
   ROOM_INDEX_DATA * to_room;
   CHAR_DATA       * victim = 0;
   int               chance;
   char              buf[MAX_STRING_LENGTH];
   bool              pfound = FALSE;
   
   if ( get_eq_char( ch, WEAR_DUAL_WIELD ) != NULL )
   {
         send_to_char( "You can't do that while wielding two weapons.",ch );
         return;
   }
    
   argument = one_argument( argument, arg );
   argument = one_argument( argument, arg2 );
   
   if ( ( dir = get_door( arg ) ) == -1 || arg2[0] == '\0' )
   {
     send_to_char( "Usage: Kaiobeam <dir> <target>\n\r", ch );
     return;
   }
 
   if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
   {
     send_to_char( "Are you expecting to fire through a wall!?\n\r", ch );
     return;
   }

   if ( IS_SET( pexit->exit_info, EX_CLOSED ) )
   {
     send_to_char( "Are you expecting to fire through a door!?\n\r", ch );
     return;
   }

   was_in_room = ch->in_room;
   
   for ( dist = 0; dist <= max_dist; dist++ )   
   {
     if ( IS_SET( pexit->exit_info, EX_CLOSED ) )
        break; 
     
     if ( !pexit->to_room )
        break;
     
    to_room = NULL;
    if ( pexit->distance > 1 )
       to_room = generate_exit( ch->in_room , &pexit );
    
    if ( to_room == NULL )
       to_room = pexit->to_room;
    
     char_from_room( ch );
     char_to_room( ch, to_room );    
     

     if ( IS_NPC(ch) && ( victim = get_char_room( ch, arg2 ) ) != NULL )
     {
        pfound = TRUE;
        break;
     }
     else if ( !IS_NPC(ch) && ( victim = get_char_room( ch, arg2 ) ) != NULL )
     {
        pfound = TRUE;
        break;
     }


     if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
        break;
            
   }
   
   char_from_room( ch );
   char_to_room( ch, was_in_room );    
       
   if ( !pfound )
   {
       ch_printf( ch, "You don't see that person to the %s!\n\r", dir_name[dir] );
       char_from_room( ch );
       char_to_room( ch, was_in_room );    
       return;
   }
   
    if ( victim == ch )
    {
	send_to_char( "Shoot yourself ... really?\n\r", ch );
	return;
    }
    
        
    if ( IS_AFFECTED(ch, AFF_CHARM) && ch->master == victim )
    {
        act( AT_PLAIN, "$N is your beloved master.", ch, NULL, victim, TO_CHAR );
	return;
    }

    if ( ch->position == POS_FIGHTING )
    {
	send_to_char( "You do the best you can!\n\r", ch );
	return;
    }
    
     if ( !IS_NPC(victim ) && xIS_SET(ch->act, ACT_DEADLY ) )
    {
      send_to_char( "You feel too nice to do that!\n\r", ch );
      return;
    }

    if ( !IS_NPC( victim ) && xIS_SET( ch->act, PLR_NICE ) )
    {
      send_to_char( "You feel too nice to do that!\n\r", ch );
      return;
    }
    
    chance = IS_NPC(ch) ? 100
           : (int)  (ch->pcdata->learned[gsn_kaiobeam]) ; 
    
    switch ( dir )
    {
        case 0:
        case 1:
           dir += 2;
           break;
        case 2:
        case 3:
           dir -= 2;
           break;
        case 4:
        case 7:
           dir += 1;
           break;
        case 5:
        case 8:
           dir -= 1;
           break;
        case 6:
           dir += 3;
           break;
        case 9:
           dir -=3;
           break;
    }
    
    char_from_room( ch );
    char_to_room( ch, victim->in_room );    
                
    if ( number_percent() < chance )
    {                         
       sprintf( buf , "An energy beam flies in at you from the %s." , dir_name[dir] );
       act( AT_ACTION, buf , victim, NULL, ch, TO_CHAR );      
       act( AT_ACTION, "You raise your arm and fire a beam of energy at $N.", ch, NULL, victim, TO_CHAR );         
       sprintf( buf, "A beam of energy flies in at $N from the %s." , dir_name[dir] );
       act( AT_ACTION, buf, ch, NULL, victim, TO_NOTVICT );  
                                                   
       damage( ch, victim, number_range(100,200), gsn_kaiobeam  );  
       
       if ( char_died(ch) ) 
          return;
          
       stop_fighting( ch , TRUE );
       
       learn_from_success( ch, gsn_kaiobeam );             
    }
    else
    {
       act( AT_ACTION, "You fire at $N but don't even come close.", ch, NULL, victim, TO_CHAR );         
       sprintf( buf, "A blast of energy fired from the %s barely misses you." , dir_name[dir] );
       act( AT_ACTION, buf, ch, NULL, victim, TO_ROOM );  
       learn_from_failure( ch, gsn_kaiobeam );
    }
            
    char_from_room( ch );
    char_to_room( ch, was_in_room );    
     
   if ( IS_NPC(ch) )                               
      WAIT_STATE( ch, 1 * PULSE_VIOLENCE );
   else
   {
   	if ( number_percent() < ch->pcdata->learned[gsn_third_attack] )
   	     WAIT_STATE( ch, 1 * PULSE_PER_SECOND );
   	else if ( number_percent() < ch->pcdata->learned[gsn_second_attack] )
   	     WAIT_STATE( ch, 2 * PULSE_PER_SECOND );
   	else 
   	     WAIT_STATE( ch, 3 * PULSE_PER_SECOND );
   }
   if ( IS_NPC( victim ) && !char_died(victim) )
   {
      if ( xIS_SET( victim->act , ACT_SENTINEL ) )
      {
         xREMOVE_BIT( victim->act, ACT_SENTINEL );
      }
      
      start_hating( victim , ch );
      start_hunting( victim, ch );
     
   } 
   
}
*/
/* Use cedit to add in as imm command.
 * Syntax is: Invade <area filename> <# of invaders> <vnum of mobs> 
 * example: Invade newacad.are 300 10399 would send 300 mistress tsythia's rampaging 
 * through the academy. This function doesnt make the mobiles aggressive but can be
 * modified to do so easily if you wish this, or you can just edit the mob before
 * hand.
 */
void do_invade( CHAR_DATA *ch , char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char arg3[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    AREA_DATA *tarea;
    int count, created;
    bool found=FALSE;
    MOB_INDEX_DATA *pMobIndex;
    ROOM_INDEX_DATA *location;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );
    count = atoi( arg2 );
    if ( arg1[0] == '\0' || arg2[0] == '\0' )
    {
	send_to_char( "Invade <area> <# of invaders> <mob vnum>\n\r", ch );
	return;
    }
    for ( tarea = first_area; tarea; tarea = tarea->next )
	if ( !str_cmp( tarea->filename, arg1 ) )
	{
	  found = TRUE;
	  break;
	}
    if ( !found )
    {
	send_to_char( "Area not found.\n\r", ch );
	return;
    }
    if ( count > 300)
    {
	send_to_char( "Whoa...Less than 300 please.\n\r", ch );
	return;
    }
    if ( ( pMobIndex = get_mob_index( atoi(arg3) ) ) == NULL )
    {
	send_to_char( "No mobile has that vnum.\n\r", ch );
	return;
    }

    for ( created=0; created < count; created++ )
    {
	if ( (location = get_room_index(number_range(tarea->low_r_vnum, tarea->hi_r_vnum ))) == NULL )
        {
          --created;
	  continue;
        }

        victim = create_mobile( pMobIndex );
        char_to_room( victim, location );
        act( AT_IMMORT, "$N appears as part of an invasion force!", ch, NULL, victim, TO_ROOM );
    }
	sprintf( buf, "%d mobs invade %s!  Someone call the bounty hunters!", count, tarea->name );
	talk_info( AT_PINK, buf );
	send_to_char( "The invasion was successful!\n\r", ch );

 return;
}

void do_multiform( CHAR_DATA *ch, char * argument )
{
}


void do_daodenspike( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 int duration = number_range( 9, 18 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( !IS_NPC(ch) && ch->plmod < 1000 )
 {
         send_to_char( "Daoden Spike is only open to those with a ten times pl modifier.\n\r", ch );
         return;
 }


 switch( ch->substate )
    {
    default:
        ch->skill_timer = duration;
        add_timer( ch, TIMER_DO_FUN, duration, do_daodenspike, 1 );
        pager_printf(ch, "&G%s&G energy spirals around your arms and legs as you point one hand in front of you, and dig in for some recoil...\n\r", ch->energycolour );
        act( AT_GREEN, "$n is covered in spirals of energy as $e points out one hand and digs into the ground...", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Daoden Spike was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Daoden Spike was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming Daoden Spike!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Daoden Spike!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_daodenspike ) )
    {
        learn_from_success( ch, gsn_daodenspike );
        pager_printf(ch, "&GThe spirals of %s&G energy shift from your extremities to your hands as you thrust a powerful blast of ki towards %s!\n\r", ch->energycolour, PERS( victim, ch ) );
        pager_printf(victim, "&GThe spirals of energy that surrounded %s move to %s hands, and %s thrusts a powerful blast of ki at you!\n\r", PERS(ch, victim), ch->sex == 2 ? "her" : "his", ch->sex == 2 ? "she" : "he" );
        act( AT_GREEN, "$n thrusts $s hand forward, and concentrates the spirals of energy around $m into a blast of ki, which $e launches at $N!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 250, 500 ), gsn_daodenspike );
	if ( !IS_NPC(ch) && ch->plmod < 75000 )
	        ch->plmod += 25;
        pager_printf( ch, "&R^r&Y....&WStronger and stronger&Y....^x\n\r" );
    }
    else
    {

        learn_from_failure( ch, gsn_daodenspike );
        pager_printf(ch, "&GYou scream with anger and a puny little beam of ki comes out of your hand, but it misses %s!\n\r", PERS( victim, ch ) );
        pager_printf(victim, "&G%s launches a daoden spike at you, but it flies off to the right and just glances your arm.\n\r", ch->name );
        act( AT_GREEN, "$n roars with rage and tries to hammer $N with a Daoden Spike, but fails miserably.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_daodenspike );
    }
return;
}


void do_afterimage( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 int duration = number_range( 2, 5 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( get_curr_dex( ch ) < ( get_curr_dex( victim ) * 0.8 ) )
 {
         send_to_char( "You are currently being matched blow for blow: You need to speed up...\n\r", ch );
         return;
 }

 ch->renzoku = FALSE;
 if ( !can_use_skill ( ch, number_percent(), gsn_afterimage  ) )
 {
	 pager_printf( ch, "&GYou begin to phase in and out around %s, but you lose focus and can't keep it up.\n\r", PERS( victim, ch ) );
	 act( AT_GREEN, "$n begins to phase in and out, but $e loses focus and can't keep it up.", ch, NULL, NULL, TO_CANSEE );
         xREMOVE_BIT( ch->affected_by, AFF_AFTER_IMAGE );
	 return;
 }

 switch( ch->substate )
 {
    default:
        ch->skill_timer = duration;
        add_timer( ch, TIMER_DO_FUN, duration, do_afterimage, 1 );
	pager_printf( ch, "&GYou begin to phase in and out around %s, getting faster and faster until you appear to be everywhere at once!\n\r", PERS( victim, ch ) );
        act( AT_GREEN, "$n starts to speed up, phasing in and out all over the place until $e appears to be everywhere at once.", ch, NULL, NULL, TO_CANSEE );
        learn_from_success( ch, gsn_afterimage );
	xSET_BIT( ch->affected_by, AFF_AFTER_IMAGE );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
	  pager_printf( ch, "You stop after-imaging.\n\r" );
	  xREMOVE_BIT( ch->affected_by, AFF_AFTER_IMAGE );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
	    pager_printf( ch, "You stop phasing in and out, and come to a stop.\n\r" );
	    xREMOVE_BIT( ch->affected_by, AFF_AFTER_IMAGE );
            act( AT_GREEN, "$n stops using After Image.\n\r",  ch, NULL, NULL, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    pager_printf(ch, "&GYou slowly come to a halt, and stop phasing in and out.\n\r" );
    act( AT_GREEN, "$n slows down to a halt, and stops phasing in and out all over the place.",  ch, NULL, NULL, TO_CANSEE );
    learn_from_success( ch, gsn_afterimage );
    xREMOVE_BIT( ch->affected_by, AFF_AFTER_IMAGE );
    return;
}


void do_decosmichalo( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 bool chance = number_range( 1, 50 ) == 1 ? TRUE : FALSE;

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( ch->plmod < 2500  && !IS_NPC(ch))
 {
         send_to_char( "You need to power up some more first.\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 12;
        add_timer( ch, TIMER_DO_FUN, 12, do_decosmichalo, 1 );
        pager_printf(ch, "&GYou raise one hand above your head, and create a halo of %s&G energy over %s's head...\n\r", ch->energycolour, PERS( victim, ch ) );
        act( AT_GREEN, "$n puts one arm in the air and creates a halo of pure energy a little way above and in front of him...", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your DeCosmic Halo was interupted!\n\r", ch );
          act( AT_GREEN, "$n's DeCosmic Halo was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming DeCosmic Halo!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Decosmic Halo!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_decosmichalo ) )
    {
        learn_from_success( ch, gsn_decosmichalo );
        pager_printf(ch, "&GThe halo of %s&G energy shifts from over %s's head to down over their body, squeezing them tight!\n\r", ch->energycolour, PERS( victim, ch ) );
        pager_printf(victim, "&GThe halo of energy that %s creaed moves down over your body, and %s brings %s hands together quickly, trying to cut you in half!  The nerve!\n\r", PERS(ch, victim), ch->sex == 2 ? "she" : "he", ch->sex == 2 ? "her" : "his" );
        act( AT_GREEN, "$n pulls a halo of energy down and around $N!",  ch, NULL, victim, TO_NOTVICT );
	if ( chance )
	{
		pager_printf( victim, "&RGah!  It's too tight...\n\r&W" );
		act( AT_DANGER, "$N explodes in a shower of meaty chunks from the intensity of the DeCosmic Halo!", ch, NULL, NULL, TO_CANSEE );
		raw_kill( ch, victim );
		return;
	}
        global_retcode = damage( ch, victim, URANGE( 1, number_range( 300, 600 ) * ( !IS_NPC(victim) ? (victim->pcdata->pdeaths + victim->pcdata->mdeaths ) : 5 ), 20000 ), gsn_decosmichalo );
    }
    else
    {

        learn_from_failure( ch, gsn_decosmichalo );
        pager_printf(ch, "&GYou are so busy laughing your head off that you fail to notice %s dodge your DeCosmic Halo!\n\r", PERS( victim, ch ) );
        pager_printf(victim, "&G%s is so busy laughing that you manage to dodge the DeCosmic Halo unharmed!\n\r", ch->name );
        act( AT_GREEN, "$n is so busy laughing that $e completely misses $N!.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_decosmichalo );
    }
return;
}


void do_victorycannon( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( !IS_NPC(ch) )
 {
    if ( ch->plmod < 2500 )
    {
         send_to_char( "You need to power up some more first.\n\r", ch );
         return;
    }
 
    if ( ch->mana < ch->max_mana / 4 )
    {
         send_to_char( "Victory cannon takes one quarter of your max energy.\n\r", ch );
         return;
    }
 }
 switch( ch->substate )
    {
    default:
        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_victorycannon, 1 );
        pager_printf(ch, "&GYou pull back your arms and head and begin to gather ki, in order to fire it at %s.\n\r", PERS( victim, ch ) );
        act( AT_GREEN, "$n pulls back $s arms and head and begins to gather $s energy.", ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Victory Cannon was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Victory Cannon was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming Victory Cannon!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Victory Cannon!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    ch->mana -= ch->max_mana / 4;
    
    if ( can_use_skill(ch, number_percent(),gsn_victorycannon ) )
    {
        learn_from_success( ch, gsn_victorycannon );
        pager_printf(ch, "&GYou throw forward your head and arms, and channel your ki at %s.  A %s&G blast of ki comes flying from your mouth, arcing and twisting in the air, and hammers right into your opponent's chest!\n\r", PERS( victim, ch ), ch->energycolour );
        pager_printf(victim, "&GOut of nowhere a blast of %s&G ki comes spiraling at you and hammers you in the chest, while you hear someone yelling &R'VICTORY CANNON!'&G\n\r", ch->energycolour );
        act( AT_GREEN, "$n fires $s victory cannon at $N, and yells out &R'VICTORY CANNON!'",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 2000, 5000 ), gsn_victorycannon );
    }
    else
    {
        learn_from_failure( ch, gsn_victorycannon );
        pager_printf(ch, "&GYou fire your Victory Cannon at %s, but it goes off course and ploughs into the ground.\n\r", PERS( victim, ch ) );
        pager_printf(victim, "&G%s finds the concentration of Victory Cannon too hard, and the attack flies off course.\n\r", ch->name );
        act( AT_GREEN, "$n fires $s victory cannon at $N, but it goes flying off course.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_victorycannon );
    }
return;
}

void do_assimilate ( CHAR_DATA *ch, char * argument )
{
 // Stat and pl gains for bios.  Ends the fight though.
 CHAR_DATA *victim;
 
 victim = who_fighting( ch );
 if ( !victim )
 {
	pager_printf( ch, "You're not fighting anyone.\n\r" );
	return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ) )
 {
	pager_printf( ch, "Not in a duel.\n\r" );
	return;
 }

 if ( victim->hit > victim->max_hit * 0.1 || IS_NPC(victim) || victim->currpl > ch->currpl * 1.2 )
 {
	pager_printf( ch, "They're currently too healthy for you to assimilate them.\n\r" );
	return;
 }

 if ( can_use_skill( ch, number_percent(), gsn_assimilate ) )
 {
	pager_printf( ch,     "&GYour tail opens wide up, and you slide it violently over %s, sucking %s up into your body through your assimilation organs.  Soon, a burst of new-found power overwhelms you, and your body alters subtly.\n\r", victim->name, victim->sex == 2 ? "her" : "him" );
	pager_printf( victim, "&GSuddenly you find yourself being sucked up inside %s through %s tail.  Soon enough, everything fades to black as you pass out...\n\r", ch->name,  ch->sex == 2 ? "her" : "his"  );
	act       ( AT_GREEN, "$n's tail opens up as $e sucks $N up it, assimilating $m!", ch, victim, victim, TO_NOTVICT );
	switch( number_range( 1, 3 ) )
	{
		case 1:
			pager_printf( ch, "&YYour combat systems improve!\n\r" );
			ch->perm_str += 5;
			ch->perm_dex += 5;
			ch->perm_con += 5;
			break;
		case 2:
			pager_printf( ch, "&YYour mental systems improve!\n\r" );
			ch->perm_int += 10;
			ch->perm_wis += 10;
			break;
		case 3:
			pager_printf( ch, "&YYour interface systems improve!\n\r" );
			ch->perm_lck += 10;
			ch->perm_cha += 10;
			break;
	}
	learn_from_success( ch, gsn_assimilate );
	ch->basepl += ( sqrt(victim->basepl ) * number_range( 1, 5 ) );
        ch->pkills++;
	update_current( ch, NULL );
	stop_fighting( ch, TRUE );
	char_from_room( victim );
	if ( victim->race != RACE_ANDROID )
	    char_to_room( victim, get_room_index( 23800 ) );
	else
	    char_to_room( victim, get_room_index( 905 ) );
 }
 else
 {
	pager_printf( ch, "You can't manage to get them to stay still enough.\n\r" );
	learn_from_failure( ch, gsn_assimilate );
	return;
 }
	
}

void do_punishment( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 int bonus;

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( ch->plmod < 2500 )
 {
        send_to_char( "You need a 25x powerlevel multiplier for this one.\n\r", ch );
        return;
 }

 if ( victim->hit < ch->hit )
 {
	send_to_char( "I think you'll find they're already closer to death than you.\n\r", ch );
	return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_punishment, 1 );
	sprintf( buf, "&GYou fall back a little, and begin to reflect on the virtues of your race, and how easily you" 
" could crush all the %ss were it not for %s... Slowly a kind of rage builds up inside of you, as you begin to feel your "
"purpose.  Torrents of %s &Genergy begin to cascade from your body, burning dark black holes into the ground, as the "
"anger and hatred wells up inside of you...\n\r", get_race(victim), 
PERS( victim, ch ), 
ch->energycolour );

	send_to_char( buf, ch );
	sprintf( buf, "&G%s suddenly looks a little bit subdued, and begins to mutter about the harshness of the way Tuffles have been treated... you notice %s &Genergy begin to cascade from %s, as well...", ch->name, ch->energycolour, HIMHER(ch->sex) );
        act( AT_MAGIC, buf, ch, NULL, NULL, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Punishment was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Punishment was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your Punishment.\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Punishment.\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(), gsn_punishment) )
    {
        learn_from_success( ch, gsn_punishment );

	bonus = ( ch->hit - victim->hit );
	if ( bonus < 0 )
		bonus = 100  + ( ch->eggslaid );

	sprintf( buf, "&GIn the end you are overcome with rage, and you feel so angry that it is as if every molecule in your body is about to explode with the force of a galaxy... You look straight up at %s, and send a shockwave punch right at %s, stopping %s mid-attack.\n\r", PERS(victim,ch), HIMHER(victim->sex), HIMHER(victim->sex) );
	send_to_char( buf, ch );
	sprintf( buf, "&G%s looks straight up at you, and sends a shockwave punch right at you, stopping you mid-attack.  This can't be good.\n\r", PERS(ch,victim));
	send_to_char( buf, victim );
	damage( ch, victim, number_range( 100, 500 ), gsn_shockwavepunch );

	sprintf( buf, "&GNext, you open up your palms to reveal two cores of energy, one &zblack &Gand one &Rred&G.  You hurl the &zblack&G one at your target, %s.\n\r", PERS(victim,ch) );
	send_to_char( buf, ch );
	sprintf( buf, "&GNext, %s opens up %s palms to reveal two cores of energy, one &zblack &Gand one &Rred&G, and then %s hurl the &zblack&G one at you!\n\r", PERS(ch, victim), HISHER(ch->sex), HESHE(ch->sex) );
	send_to_char( buf, victim );
	damage( ch, victim, number_range( 500, 3000 ), gsn_rvdb);

	sprintf( buf, "&GIt comes to pass that you can bear it no more.  You swallow down the red ball of energy and feel yourself refreshed, surging with the power of all your hatred and anger.  As you seem to detach from life itself, becoming one with your long dead ancestry, you move towards %s at the speed of light, and bring your foot up right into %s chest.\n\r", PERS(victim, ch), HISHER(victim->sex) );
	send_to_char( buf, ch );
	sprintf( buf, "&GCrazily, %s swallows down the red ball of energy and looks refreshed, surging with power.  Seeming to detach from life itself, %s moves towards you at the speed of light, and brings %s foot up right into your chest!  Painful...\n\r", PERS(ch,victim), HESHE(ch->sex), HISHER(ch->sex) );
	send_to_char( buf, victim );
	damage( ch, victim, number_range( 150, 650 ), gsn_kick);

	sprintf( buf, "&GTaking advantage of your intimate position now, you lean in to %s and speek seethingly: &R\"This is for the fallen... you will suffer the pain of my entire people in one final blow!\"&G  Then you force your hand into %s chest and let all the ki you have built up explode in one devestating cocktail of pain and torment, anger and rage!\n\r", PERS(victim,ch), HISHER(victim->sex) );
	send_to_char( buf, ch );
	sprintf( buf, "&GTaking advantage of your intimate position, %s leans in and speeks seethingly: &R\"This is for the fallen... you will suffer the pain of my entire people in one final blow!\"&G  Then %s force %s hand into your chest and lets all the ki %s has built up explode in one devestating cocktail of pain and torment, anger and rage!\n\r", PERS(ch,victim), HESHE(victim->sex), HISHER(victim->sex), HESHE(victim->sex) );
	send_to_char( buf, victim );

	bonus = bonus * 1.5;
	damage( ch, victim, bonus, gsn_punishment );

	sprintf( buf, "%s lays into %s with a number of awe-inspiring attacks!", ch->name, victim->name );
        act( AT_GREEN, buf, ch, NULL, victim, TO_NOTVICT );
    }
    else
    {
        learn_from_failure( ch, gsn_punishment );
        pager_printf(ch, "&GSomehow... you can't quite muster up enough rage towards %s... Maybe... Maybe they're innocent...\n\r", PERS(victim,ch) );
        act( AT_GREEN, "$n collapses to the ground, muttering 'No... it can't be...'",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_punishment );
    }
return;
}


void do_haerenn( CHAR_DATA *ch, char * argument ) 
{ 
}


void do_ssb( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 int duration = number_range( 18, 22 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( !IS_NPC(ch) && ch->plmod < 2300 )
 {
         send_to_char( "You need a 23x modifier to use Enhanced Spirit Ball.  Perhaps you should try Wolf Warrior?\n\r", ch );
         return;
 }

 switch( ch->substate )
    {
    default:
        ch->skill_timer = duration;
        add_timer( ch, TIMER_DO_FUN, duration, do_ssb, 1 );
        pager_printf(ch, "&GYou raise your arm infront of you griping it tightly as %s energy begins to surge throughout your body!&G ...\n\r", ch->energycolour );
        act( AT_GREEN, "$n raises $s arm infront of $n griping it firmly as $s energy begins to surge throughout $s body!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Enhanced  Spirit Ball was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Enhanced Spirit Ball was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming your Enhanced Spirit Ball!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Enhanced Spirit Ball!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_ssb ) )
    {
        learn_from_success( ch, gsn_ssb );
        pager_printf(ch, "&G A massive %s &gball of Ki forms floating above your hand as you scream &REnhanced Spirit Ball &G you begin makeing arcain gestures with your hands seding the Ki Ball Slaming into %s 10 times!\n\r", ch->energycolour, PERS( victim, ch ) );
        pager_printf(victim, "&GA massive Sphere of ki forms above %s's hand as %s screams &REnhanced Spirit Ball! &g %s then begins makeing arcane gestures with %s hands sending the Sphere of Ki slaming into you 10 times!\n\r", ch->name, ch->sex == 2 ? "he" : "she", ch->sex == 2 ? "She" : "He", ch->sex == 2 ? "her" : "his" );
        act( AT_GREEN, "A Giant Spheare of Ki forms over $n's gripped hand as $e screams &REnhanced Spirit ball &G $e then begins makeing arcain gestures with $s hands sending the ball slaming into $N 10 times!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 2650, 6000 ), gsn_ssb );
	if ( !IS_NPC(ch) && ch->plmod < 3200 )
	        ch->plmod += 10;
        pager_printf( ch, "&B.....&RPowerup&B......\n\r" );
    }
    else
    {

        learn_from_failure( ch, gsn_ssb );
        pager_printf(ch, "&GYou scream with anger and launch your Enhanced Spirit Ball at %s forgeting how to control the KiBall it slams into You!!\n\r", PERS( victim, ch ) );
        pager_printf(victim, "&G%s Screams in anger as he launches a Ki Ball at you but fails.\n\r", ch->name );
        act( AT_GREEN, "$n roars with rage and tries control $s attack but fails miserably.",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_ssb );
    }
return;
}

void do_firecrusher( CHAR_DATA *ch, char *argument )

{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

  if ( !IS_AFFECTED( ch, AFF_COMBINED_STRENGTH ) )

{
         send_to_char( "You are too weak to use Fire Crusher.\n\r", ch );
         return;
 }
 if ( ch->hit > victim->hit )
 {
        send_to_char( "You are already winning the battle there is no need for that!.\n\r", ch );
        return;
 }   


 if ( victim->mana < ch->mana )
 {
        send_to_char( "Your Energy Level is still higher then theres!.\n\r", ch );
        return;
 } 
 switch( ch->substate )
    {
    default:
        ch->skill_timer = 12;
        add_timer( ch, TIMER_DO_FUN, 12, do_firecrusher, 1 );
        pager_printf(ch, "&GYou do a summersault backwards landing a few yards away and your %s &GAura flares brightly!\n\r", ch->energycolour);
        pager_printf(victim, "%s Summersault's backwards landing a few yards away and %s  %s aura begins flareing up brightly!", ch->name, ch->sex == 2 ? "her" : "his", ch->energycolour );
        act( AT_GREEN, "$n summersault's backwards away from $N and $s aura, explodes outwards!", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

    case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your Fire Crusher was interupted!\n\r", ch );
          act( AT_GREEN, "$n's Fire Crusher was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

    case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop perfoming your Fire Crusher!\n\r", ch );
            act( AT_GREEN, "$n stops perfoming $s Fire Crusher!\n\r",  ch, NULL, victim, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_firecrusher ) )
    {
        learn_from_success( ch, gsn_firecrusher );
        pager_printf(ch, "&GYou raise both arms into the air and 5 %s &Gsmall energy balls begin to form above %s's head!", ch->energycolour, victim->name );
        pager_printf(victim, "&G%s raise's both arms into the air and 5 small energy balls form above your head!\n\r", ch->name );
        act( AT_GREEN, "$n raise's $s arms into the air and 5 small energy balls form above $N's head!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 150, 250), gsn_firecrusher );
		
//		  break;
	//	 add_timer( ch, TIMER_DO_FUN, 2, do_firecrusher, 1 );
		 ch->skill_timer = 3;
		 WAIT_STATE( ch, 2 * PULSE_VIOLENCE );
//	return;
		  learn_from_success( ch, gsn_firecrusher );
        pager_printf(ch, "&GFocusing on the 5 %s &Genergy balls they begin to expand and grow traping %s in the center", ch->energycolour, victim->name );
        pager_printf(victim, "&G%s concentrates causeing the 5 energy balls to trap you int the center!\n\r", ch->name );
        act( AT_GREEN, "$n focus's as the 5 energy balls grow larger and expand traping $N in the center!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range( 50, 150), gsn_firecrusher );
//	  break;
		//add_timer( ch, TIMER_DO_FUN, 3, do_firecrusher, 1 ); 
		 ch->skill_timer = 3;
		 WAIT_STATE( ch, 3 * PULSE_VIOLENCE );
//	return;
		  learn_from_success( ch, gsn_firecrusher );
        pager_printf(ch, "&GYou scream as you launch a massive %s &G energy beam directly upwards into the sky as it arc's downwards hiting %s directly in the head causeing all 5 energy balls to explode!", ch->energycolour, victim->name );
        pager_printf(victim, "&G%s screams and launches a massive energy beam directly upwards into the sky, as it arc's back down pummleing you in the head causeing the other 5 energy balls to explode with you inside!\n\r", ch->name );
        act( AT_GREEN, "$n scream's as $s launches a massive energy beam directly into the sky, as the beam begins to arc it lands directly on $N's head causeing the other 5 energy balls to explode with $N being in the center!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, number_range(1, 1000 + ch->eggslaid  * 100), gsn_firecrusher );
    }
    else
    {

        learn_from_failure( ch, gsn_firecrusher );
        pager_printf(ch, "&G %s loses concertration and fails to do anything!\n\r", victim->name );
        pager_printf(victim, "&G%s losses concentration and fails to do anything!\n\r", ch->name );
        act( AT_GREEN, "$n loses concentration and fails to do anything!",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_firecrusher );
    }
return;
}

void do_orbital_launch ( CHAR_DATA *ch, char *argument )
{
  PLANET_DATA *planet;
  if ( ( planet = get_planet( argument ) ) != NULL )
  {
      if ( !IS_SET( ch->in_room->room_flags, ROOM_DOCK ) )
      {
	  send_to_char( "You should do this from a spaceport, for best results.\n\r", ch );
	  return;
      }
      if ( can_use_skill( ch, number_percent(), gsn_orbitallaunch ) )
      {
	  send_to_char( "You leap up into the sky, going orbital within seconds, and head straight towards your target at immense speed.  You arrive within moments!\n\r", ch );
	  act ( AT_GREEN + AT_BLINK, "$n launches into space!\n\r", ch, NULL, NULL, TO_CANSEE );
	  learn_from_success( ch, gsn_orbitallaunch );
	  char_from_room( ch );
	  char_to_room( ch, get_room_index( planet->dock ) );
 	  act( AT_GREEN + AT_BLINK, "$n slams into the ground with a mighty thud!\n\r", ch, NULL, NULL, TO_CANSEE );
	  ch->skill_timer = 8;
	  WAIT_STATE( ch, 8 * PULSE_PER_SECOND );
	  return;
      }
      else
      {
	  send_to_char( "You can't quite calculate the trajectory necessary.\n\r", ch );
	  learn_from_failure( ch, gsn_orbitallaunch );
	  return;
      }
  }
  else
  {
	send_to_char( "No such planet is in your database.\n\r", ch );
	return;
  }
}
