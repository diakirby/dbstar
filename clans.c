/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 *			     Special clan module			    *
 ****************************************************************************/

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
/* #include <stdlib.h> */
#include <time.h>
#include "mud.h"



#define MAX_NEST	100
static	OBJ_DATA *	rgObjNest	[MAX_NEST];

CLAN_DATA * first_clan;
CLAN_DATA * last_clan;
COUNCIL_DATA * first_council;
COUNCIL_DATA * last_council;

/* local routines */
void	fread_clan	args( ( CLAN_DATA *clan, FILE *fp ) );
bool	load_clan_file	args( ( char *clanfile ) );
void	write_clan_list	args( ( void ) );

void	fread_council	args( ( COUNCIL_DATA *council, FILE *fp ) );
bool	load_council_file	args( ( char *councilfile ) );
void	write_council_list	args( ( void ) );

bool is_holding( CHAR_DATA *ch, int vnum );


/*
 * Get pointer to clan structure from clan name.
 */
CLAN_DATA *get_clan( char *name )
{
    CLAN_DATA *clan;
    
    for ( clan = first_clan; clan; clan = clan->next )
       if ( !str_cmp( name, clan->name ) )
         return clan;
    return NULL;
}

COUNCIL_DATA *get_council( char *name )
{
    COUNCIL_DATA *council;
    
    for ( council = first_council; council; council = council->next )
       if ( !str_cmp( name, council->name ) )
         return council;
    return NULL;
}

void write_clan_list( )
{
    CLAN_DATA *tclan;
    FILE *fpout;
    char filename[256];

    sprintf( filename, "%s%s", CLAN_DIR, CLAN_LIST );
    fpout = Fopen( filename, "w" );
    if ( !fpout )
    {
	bug( "FATAL: cannot open clan.lst for writing!\n\r", 0 );
 	return;
    }	  
    for ( tclan = first_clan; tclan; tclan = tclan->next )
	fprintf( fpout, "\"%s\"\n", tclan->filename );
    fprintf( fpout, "$\n" );
    Fclose( fpout );
}

void write_council_list( )
{
    COUNCIL_DATA *tcouncil;
    FILE *fpout;
    char filename[256];

    sprintf( filename, "%s%s", COUNCIL_DIR, COUNCIL_LIST );
    fpout = Fopen( filename, "w" );
    if ( !fpout )
    {
	bug( "FATAL: cannot open council.lst for writing!\n\r", 0 );
 	return;
    }	  
    for ( tcouncil = first_council; tcouncil; tcouncil = tcouncil->next )
	fprintf( fpout, "%s\n", tcouncil->filename );
    fprintf( fpout, "$\n" );
    Fclose( fpout );
}

/*
 * Save a clan's data to its data file
 */
void save_clan( CLAN_DATA *clan )
{
    FILE *fp;
    char filename[256];
    char buf[MAX_STRING_LENGTH];

    if ( !clan )
    {
	bug( "save_clan: null clan pointer!", 0 );
	return;
    }
        
    if ( !clan->filename || clan->filename[0] == '\0' )
    {
	sprintf( buf, "save_clan: %s has no filename", clan->name );
	bug( buf, 0 );
	return;
    }
    
    sprintf( filename, "%s%s", CLAN_DIR, clan->filename );
    
    Fclose( fpReserve );
    if ( ( fp = Fopen( filename, "w" ) ) == NULL )
    {
    	bug( "save_clan: Fopen", 0 );
    	perror( filename );
    }
    else
    {
	fprintf( fp, "#CLAN\n" );
	fprintf( fp, "Name         %s~\n",	clan->name		);
	fprintf( fp, "Filename     %s~\n",	clan->filename		);
	fprintf( fp, "Motto        %s~\n",	clan->motto		);
	fprintf( fp, "Note         %s~\n",	clan->note		);
	fprintf( fp, "Description  %s~\n",	clan->description	);
	fprintf( fp, "Leader       %s~\n",	clan->leader		);
	fprintf( fp, "NumberOne    %s~\n",	clan->number1		);
	fprintf( fp, "NumberTwo    %s~\n",	clan->number2		);
	fprintf( fp, "Badge        %s~\n",	clan->badge		);
	fprintf( fp, "Leadrank     %s~\n",	clan->leadrank		);
	fprintf( fp, "Onerank      %s~\n",	clan->onerank		);
	fprintf( fp, "Tworank      %s~\n",	clan->tworank		);
	fprintf( fp, "PKills	   %d\n", 	clan->pkills 		);
	fprintf( fp, "PDeaths	   %d\n", 	clan->pdeaths 		);
	fprintf( fp, "MKills       %d\n",	clan->mkills		);
	fprintf( fp, "MDeaths      %d\n",	clan->mdeaths		);
	fprintf( fp, "CKills       %d\n",	clan->ckills		);
	fprintf( fp, "CDeaths      %d\n",	clan->cdeaths		);
	fprintf( fp, "OKills       %d\n",	clan->okills		);
	fprintf( fp, "ODeaths      %d\n",	clan->odeaths		);
	fprintf( fp, "OAttempts	   %d\n",	clan->oattempts		);
	fprintf( fp, "Type         %d\n",	clan->clan_type		);
	fprintf( fp, "Members      %d\n",	clan->members		);
	fprintf( fp, "MemLimit     %d\n",	clan->mem_limit		);
	fprintf( fp, "Alignment    %d\n",	clan->alignment		);
	fprintf( fp, "Command 	   %d\n",	clan->command		);
	fprintf( fp, "ClanObjOne   %d\n",	clan->clanobj1		);
	fprintf( fp, "ClanObjTwo   %d\n",	clan->clanobj2		);
	fprintf( fp, "ClanObjThree %d\n",	clan->clanobj3		);
        fprintf( fp, "ClanObjFour  %d\n",	clan->clanobj4		);
	fprintf( fp, "ClanObjFive  %d\n", 	clan->clanobj5		);
	fprintf( fp, "Recall       %d\n",	clan->recall		);
	fprintf( fp, "Storeroom    %d\n",	clan->storeroom		);
	fprintf( fp, "GuardOne     %d\n",	clan->guard1		);
	fprintf( fp, "GuardTwo     %d\n",	clan->guard2		);
	fprintf( fp, "Morale	   %d\n",       clan->morale 		);
	fprintf( fp, "Funds	   %d\n",       clan->funds 		);
	fprintf( fp, "Waring       %s~\n",      clan->waring  		);
	fprintf( fp, "Treaty  	   %s~\n",	clan->treaty 		);
	fprintf( fp, "End\n\n"						);
	fprintf( fp, "#END\n"						);
    }
    Fclose( fp );
    fpReserve = Fopen( NULL_FILE, "r" );
    write_clan_list( );
    return;
}

/*
 * Save a council's data to its data file
 */
void save_council( COUNCIL_DATA *council )
{
    FILE *fp;
    char filename[256];
    char buf[MAX_STRING_LENGTH];

    if ( !council )
    {
	bug( "save_council: null council pointer!", 0 );
	return;
    }
        
    if ( !council->filename || council->filename[0] == '\0' )
    {
	sprintf( buf, "save_council: %s has no filename", council->name );
	bug( buf, 0 );
	return;
    }
 
    sprintf( filename, "%s%s", COUNCIL_DIR, council->filename );
    
    Fclose( fpReserve );
    if ( ( fp = Fopen( filename, "w" ) ) == NULL )
    {
    	bug( "save_council: Fopen", 0 );
    	perror( filename );
    }
    else
    {
	fprintf( fp, "#COUNCIL\n" );
	fprintf( fp, "Name         %s~\n",	council->name		);
	fprintf( fp, "Filename     %s~\n",	council->filename	);
	fprintf( fp, "Description  %s~\n",	council->description	);
	fprintf( fp, "Head         %s~\n",	council->head		);
      	if ( council->head2 != NULL)
        	fprintf (fp, "Head2        %s~\n", council->head2);
	fprintf( fp, "Members      %d\n",	council->members	);
	fprintf( fp, "Board        %d\n",	council->board		);
	fprintf( fp, "Meeting      %d\n",	council->meeting	);
	fprintf( fp, "Powers       %s~\n",	council->powers		);
	fprintf( fp, "End\n\n"						);
	fprintf( fp, "#END\n"						);
    }
    Fclose( fp );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}


/*
 * Read in actual clan data.
 */

#if defined(KEY)
#undef KEY
#endif

#define KEY( literal, field, value )					\
				if ( !str_cmp( word, literal ) )	\
				{					\
				    field  = value;			\
				    fMatch = TRUE;			\
				    break;				\
				}

/*
 * Reads in PKill and PDeath still for backward compatibility but now it
 * should be written to PKillRange and PDeathRange for multiple level pkill
 * tracking support. --Shaddai
 * Added a hardcoded limit memlimit to the amount of members a clan can 
 * have set using setclan.  --Shaddai
 */

void fread_clan( CLAN_DATA *clan, FILE *fp )
{
    char buf[MAX_STRING_LENGTH];
    char *word;
    bool fMatch;

    clan->mem_limit = 0;  /* Set up defaults */
    
    for ( ; ; )
    {
	word   = feof( fp ) ? "End" : fread_word( fp );
	fMatch = FALSE;

	switch ( UPPER(word[0]) )
	{
	case '*':
	    fMatch = TRUE;
	    fread_to_eol( fp );
	    break;

	case 'A':
	    KEY( "Alignment",	clan->alignment,	fread_number( fp ) );
	    break;

	case 'B':
            KEY( "Badge",       clan->badge,            fread_string( fp ) );
	    break;

	case 'C':
	    KEY( "ClanObjOne",	clan->clanobj1,		fread_number( fp ) );
	    KEY( "ClanObjTwo",	clan->clanobj2,		fread_number( fp ) );
	    KEY( "ClanObjThree",clan->clanobj3,		fread_number( fp ) );
            KEY( "ClanObjFour", clan->clanobj4,         fread_number( fp ) );
            KEY( "ClanObjFive", clan->clanobj5,         fread_number( fp ) );
            KEY( "Command",	clan->command, 	        fread_number( fp ) );
            KEY( "CKills",	clan->ckills, 	        fread_number( fp ) );
            KEY( "Cdeaths",	clan->cdeaths, 	        fread_number( fp ) );

	    break;

	case 'D':
	    KEY( "Description",	clan->description,	fread_string( fp ) );
	    break;

	case 'E':
	    if ( !str_cmp( word, "End" ) )
	    {
		if (!clan->name)
		  clan->name		= STRALLOC( "" );
		if (!clan->leader)
		  clan->leader		= STRALLOC( "" );
		if (!clan->description)
		  clan->description 	= STRALLOC( "" );
		if (!clan->motto)
		  clan->motto		= STRALLOC( "" );
		if (!clan->number1)
		  clan->number1		= STRALLOC( "" );
		if (!clan->number2)
		  clan->number2		= STRALLOC( "" );
		if (!clan->badge)
	  	  clan->badge		= STRALLOC( "" );
		if (!clan->leadrank)
		  clan->leadrank	= STRALLOC( "" );
		if (!clan->onerank)
		  clan->onerank		= STRALLOC( "" );
		if (!clan->tworank)
		  clan->tworank		= STRALLOC( "" );
		return;
	    }
	    break;
	    
	case 'F':
	    KEY( "Funds",	clan->funds,		fread_number( fp ) );
	    KEY( "Filename",	clan->filename,		fread_string_nohash( fp ) );

	case 'G':
	    KEY( "GuardOne",	clan->guard1,		fread_number( fp ) );
	    KEY( "GuardTwo",	clan->guard2,		fread_number( fp ) );
	    break;


	case 'L':
	    KEY( "Leader",	clan->leader,		fread_string( fp ) );
	    KEY( "Leadrank",	clan->leadrank,		fread_string( fp ) );
	    break;

	case 'M':
	    KEY( "MDeaths",	clan->mdeaths,		fread_number( fp ) );
	    KEY( "Members",	clan->members,		fread_number( fp ) );
	    KEY( "MemLimit",	clan->mem_limit,	fread_number( fp ) );
	    KEY( "MKills",	clan->mkills,		fread_number( fp ) );
	    KEY( "Motto",	clan->motto,		fread_string( fp ) );
	    KEY( "Morale",	clan->morale,		fread_number( fp ) );
	    break;
 
	case 'N':
	    KEY( "Name",	clan->name,		fread_string( fp ) );
	    KEY( "Note",	clan->note,		fread_string( fp ) );
	    KEY( "NumberOne",	clan->number1,		fread_string( fp ) );
	    KEY( "NumberTwo",	clan->number2,		fread_string( fp ) );
	    break;

	case 'O':
	    KEY( "Onerank",	clan->onerank,		fread_string( fp ) );
            KEY( "OKills",	clan->okills, 	        fread_number( fp ) );
            KEY( "ODeaths",	clan->odeaths, 	        fread_number( fp ) );
            KEY( "OAttempts",	clan->oattempts, 	fread_number( fp ) );
	    break;

	case 'P':
	    KEY( "PDeaths",	clan->pdeaths,		fread_number( fp ) );
	    KEY( "PKills",	clan->pkills,		fread_number( fp ) );
	    /* Addition of New Ranges 
	    if ( !str_cmp ( word, "PDeathRange" ) )
	    {
		fMatch = TRUE;
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
            }
	    if ( !str_cmp ( word, "PDeathRangeNew" ) )
            {
		fMatch = TRUE;
		clan->pdeaths[0] = fread_number( fp );
		clan->pdeaths[1] = fread_number( fp );
		clan->pdeaths[2] = fread_number( fp );
		clan->pdeaths[3] = fread_number( fp );
		clan->pdeaths[4] = fread_number( fp );
		clan->pdeaths[5] = fread_number( fp );
		clan->pdeaths[6] = fread_number( fp );
	    }
	    if ( !str_cmp ( word, "PKillRangeNew" ) )
            {
		fMatch = TRUE;
		clan->pkills[0] = fread_number( fp );
		clan->pkills[1] = fread_number( fp );
		clan->pkills[2] = fread_number( fp );
		clan->pkills[3] = fread_number( fp );
		clan->pkills[4] = fread_number( fp );
		clan->pkills[5] = fread_number( fp );
		clan->pkills[6] = fread_number( fp );
	    }
	    if ( !str_cmp ( word, "PKillRange" ) )
	    {
		fMatch = TRUE;
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
		fread_number( fp );
            }*/
	    break;

	case 'R':
	    KEY( "Recall",	clan->recall,		fread_number( fp ) );
	    break;

	case 'S':
	    KEY( "Storeroom",	clan->storeroom,	fread_number( fp ) );
	    break;

	case 'T':
	    KEY( "Tworank",	clan->tworank,		fread_string( fp ) );
	    KEY( "Type",	clan->clan_type,	fread_number( fp ) );
	    KEY( "Treaty",	clan->treaty,		fread_string( fp ) );
	    break;

	case 'W':
	    KEY( "Waring",	clan->waring,		fread_string( fp ) );
	}
	
	if ( !fMatch )
	{
	    sprintf( buf, "Fread_clan: no match: %s", word );
	    bug( buf, 0 );
	}
    }
}

/*
 * Read in actual council data.
 */

#if defined(KEY)
#undef KEY
#endif

#define KEY( literal, field, value )					\
				if ( !str_cmp( word, literal ) )	\
				{					\
				    field  = value;			\
				    fMatch = TRUE;			\
				    break;				\
				}

void fread_council( COUNCIL_DATA *council, FILE *fp )
{
    char buf[MAX_STRING_LENGTH];
    char *word;
    bool fMatch;

    for ( ; ; )
    {
	word   = feof( fp ) ? "End" : fread_word( fp );
	fMatch = FALSE;

	switch ( UPPER(word[0]) )
	{
	case '*':
	    fMatch = TRUE;
	    fread_to_eol( fp );
	    break;

	case 'B':
	    KEY( "Board",	council->board,		fread_number( fp ) );
	    break;

	case 'D':
	    KEY( "Description",	council->description,	fread_string( fp ) );
	    break;

	case 'E':
	    if ( !str_cmp( word, "End" ) )
	    {
		if (!council->name)
		  council->name		= STRALLOC( "" );
		if (!council->description)
		  council->description 	= STRALLOC( "" );
		if (!council->powers)
		  council->powers	= STRALLOC( "" );
		return;
	    }
	    break;
	    
	case 'F':
	    KEY( "Filename",	council->filename,	fread_string_nohash( fp ) );
  	    break;

	case 'H':
	    KEY( "Head", 	council->head, 		fread_string( fp ) );
            KEY ("Head2", 	council->head2, 	fread_string( fp ) );
	    break;

	case 'M':
	    KEY( "Members",	council->members,	fread_number( fp ) );
	    KEY( "Meeting",   	council->meeting, 	fread_number( fp ) );
	    break;
 
	case 'N':
	    KEY( "Name",	council->name,		fread_string( fp ) );
	    break;

	case 'P':
	    KEY( "Powers",	council->powers,	fread_string( fp ) );
	    break;
	}
	
	if ( !fMatch )
	{
	    sprintf( buf, "Fread_council: no match: %s", word );
	    bug( buf, 0 );
	}
    }
}


/*
 * Load a clan file
 */

bool load_clan_file( char *clanfile )
{
    char filename[256];
    CLAN_DATA *clan;
    FILE *fp;
    bool found;

    CREATE( clan, CLAN_DATA, 1 );

    /* Make sure we default these to 0 --Shaddai */
    clan->pkills  = 0;
    clan->pdeaths = 0;
    clan->waring  = "";
    clan->treaty  = "";

    found = FALSE;
    sprintf( filename, "%s%s", CLAN_DIR, clanfile );

    if ( ( fp = Fopen( filename, "r" ) ) != NULL )
    {

	found = TRUE;
	for ( ; ; )
	{
	    char letter;
	    char *word;

	    letter = fread_letter( fp );
	    if ( letter == '*' )
	    {
		fread_to_eol( fp );
		continue;
	    }

	    if ( letter != '#' )
	    {
		bug( "Load_clan_file: # not found.", 0 );
		break;
	    }

	    word = fread_word( fp );
	    if ( !str_cmp( word, "CLAN"	) )
	    {
	    	fread_clan( clan, fp );
	    	break;
	    }
	    else
	    if ( !str_cmp( word, "END"	) )
	        break;
	    else
	    {
		char buf[MAX_STRING_LENGTH];

		sprintf( buf, "Load_clan_file: bad section: %s.", word );
		bug( buf, 0 );
		break;
	    }
	}
	Fclose( fp );
    }

    if ( found )
    {
	ROOM_INDEX_DATA *storeroom;

	LINK( clan, first_clan, last_clan, next, prev );

	if ( clan->storeroom == 0
	|| (storeroom = get_room_index( clan->storeroom )) == NULL )
	{
	    return found;
	}
	
	sprintf( filename, "%s%s.vault", CLAN_DIR, clan->filename );
	if ( ( fp = Fopen( filename, "r" ) ) != NULL )
	{
	    int iNest;
	    bool found;
	    OBJ_DATA *tobj, *tobj_next;

	    log_string( "Loading clan storage room" );
	    rset_supermob(storeroom);
	    for ( iNest = 0; iNest < MAX_NEST; iNest++ )
		rgObjNest[iNest] = NULL;

	    found = TRUE;
	    for ( ; ; )
	    {
		char letter;
		char *word;

		letter = fread_letter( fp );
		if ( letter == '*' )
		{
		    fread_to_eol( fp );
		    continue;
		}

		if ( letter != '#' )
		{
		    bug( "Load_clan_vault: # not found.", 0 );
		    bug( clan->name, 0 );
		    break;
		}

		word = fread_word( fp );
		if ( !str_cmp( word, "OBJECT" ) )	/* Objects	*/
		  fread_obj  ( supermob, fp, OS_CARRY );
		else
		if ( !str_cmp( word, "END"    ) )	/* Done		*/
		  break;
		else
		{
		    bug( "Load_clan_vault: bad section.", 0 );
		    bug( clan->name, 0 );
		    break;
		}
	    }
	    Fclose( fp );
	    for ( tobj = supermob->first_carrying; tobj; tobj = tobj_next )
	    {
		tobj_next = tobj->next_content;
		obj_from_char( tobj );
		obj_to_room( tobj, storeroom );
	    }
	    release_supermob();
	}
	else
	    log_string( "Cannot open clan vault" );
    }
    else
      DISPOSE( clan );

    return found;
}

/*
 * Load a council file
 */

bool load_council_file( char *councilfile )
{
    char filename[256];
    COUNCIL_DATA *council;
    FILE *fp;
    bool found;

    CREATE( council, COUNCIL_DATA, 1 );

    found = FALSE;
    sprintf( filename, "%s%s", COUNCIL_DIR, councilfile );

    if ( ( fp = Fopen( filename, "r" ) ) != NULL )
    {

	found = TRUE;
	for ( ; ; )
	{
	    char letter;
	    char *word;

	    letter = fread_letter( fp );
	    if ( letter == '*' )
	    {
		fread_to_eol( fp );
		continue;
	    }

	    if ( letter != '#' )
	    {
		bug( "Load_council_file: # not found.", 0 );
		break;
	    }

	    word = fread_word( fp );
	    if ( !str_cmp( word, "COUNCIL"	) )
	    {
	    	fread_council( council, fp );
	    	break;
	    }
	    else
	    if ( !str_cmp( word, "END"	) )
	        break;
	    else
	    {
		bug( "Load_council_file: bad section.", 0 );
		break;
	    }
	}
	Fclose( fp );
    }

    if ( found )
      LINK( council, first_council, last_council, next, prev );

    else
      DISPOSE( council );

    return found;
}

/*
 * Load in all the clan files.
 */
void load_clans( )
{
    FILE *fpList;
    char *filename;
    char clanlist[256];
    char buf[MAX_STRING_LENGTH];
    
    
    first_clan	= NULL;
    last_clan	= NULL;

    log_string( "Loading clans..." );

    sprintf( clanlist, "%s%s", CLAN_DIR, CLAN_LIST );
    Fclose( fpReserve );
    if ( ( fpList = Fopen( clanlist, "r" ) ) == NULL )
    {
	perror( clanlist );
	exit( 1 );
    }

    for ( ; ; )
    {
	filename = feof( fpList ) ? "$" : fread_word( fpList );
	log_string( filename );
	if ( filename[0] == '$' )
	  break;

	if ( !load_clan_file( filename ) )
	{
	  sprintf( buf, "Cannot load clan file: %s", filename );
	  bug( buf, 0 );
	}
    }
    Fclose( fpList );
    log_string(" Done clans " );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}

/*
 * Load in all the council files.
 */
void load_councils( )
{
    FILE *fpList;
    char *filename;
    char councillist[256];
    char buf[MAX_STRING_LENGTH];
    
    
    first_council	= NULL;
    last_council	= NULL;

    log_string( "Loading councils..." );

    sprintf( councillist, "%s%s", COUNCIL_DIR, COUNCIL_LIST );
    Fclose( fpReserve );
    if ( ( fpList = Fopen( councillist, "r" ) ) == NULL )
    {
	perror( councillist );
	exit( 1 );
    }

    for ( ; ; )
    {
	filename = feof( fpList ) ? "$" : fread_word( fpList );
	log_string( filename );
	if ( filename[0] == '$' )
	  break;

	if ( !load_council_file( filename ) )
	{
	  sprintf( buf, "Cannot load council file: %s", filename );
	  bug( buf, 0 );
	}
    }
    Fclose( fpList );
    log_string(" Done councils " );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}

void do_make( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    OBJ_INDEX_DATA *pObjIndex;
    OBJ_DATA *obj;
    CLAN_DATA *clan;
    int level;

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;
    
    if ( str_cmp( ch->name, clan->leader )
    &&  (clan->clan_type != CLAN_GUILD
    ||   str_cmp( ch->name, clan->number1 )) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Make what?\n\r", ch );
	return;
    }

    pObjIndex = get_obj_index( clan->clanobj1 );
    level = 40;

    if ( !pObjIndex || !is_name( arg, pObjIndex->name ) )
    {
      pObjIndex = get_obj_index( clan->clanobj2 );
      level = 45;
    }
    if ( !pObjIndex || !is_name( arg, pObjIndex->name ) )
    {
      pObjIndex = get_obj_index( clan->clanobj3 );
      level = 50;
    }
    if ( !pObjIndex || !is_name( arg, pObjIndex->name ) )
    {
      pObjIndex = get_obj_index( clan->clanobj4 );
      level = 35;
    }
    if ( !pObjIndex || !is_name( arg, pObjIndex->name ) )
    {
      pObjIndex = get_obj_index( clan->clanobj5 );
      level = 1;
    }

    if ( !pObjIndex || !is_name( arg, pObjIndex->name ) )
    {
	send_to_char( "You don't know how to make that.\n\r", ch );
	return;
    }

    obj = create_object( pObjIndex, level );
    xSET_BIT( obj->extra_flags, ITEM_CLANOBJECT );
    if ( CAN_WEAR(obj, ITEM_TAKE) )
      obj = obj_to_char( obj, ch );
    else
      obj = obj_to_room( obj, ch->in_room );
    act( AT_MAGIC, "$n makes $p!", ch, obj, NULL, TO_ROOM );
    act( AT_MAGIC, "You make $p!", ch, obj, NULL, TO_CHAR );
    return;
}

void do_induct( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
//    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    CLAN_DATA *clan;

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;
    
    if ( (ch->pcdata && ch->pcdata->bestowments
    &&    is_name("induct", ch->pcdata->bestowments))
    ||	 ch->level > 50
    ||   !str_cmp( ch->name, clan->leader  )
    ||   !str_cmp( ch->name, clan->number1 )
    ||   !str_cmp( ch->name, clan->number2 ) )
	;
    else
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Induct whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    if ( IS_IMMORTAL(victim) )
    {
	send_to_char( "You can't induct such a godly presence.\n\r", ch );
	return;
    }

    

    if ( victim->pcdata->clan )
    {
	if ( victim->pcdata->clan == clan )
	  send_to_char( "This player already belongs to your clan!\n\r", ch );
	else
	  send_to_char( "This player already belongs to a clan!\n\r", ch );
	return;
    }

    if ( clan->mem_limit && clan->members >= clan->mem_limit )
    {
    	send_to_char("Your clan is too big to induct anymore players.\n\r",ch);
	return;
    }

    if ( clan->clan_type != CLAN_PLAIN )
    {
        if (clan->clan_type != CLAN_INACTIVE || clan->members >= 3 )
        {
  	    send_to_char( "Your clan does not yet have divine acceptance.\n\r", ch );
	    return;
    	}
    }

    if ( ( clan->alignment == CLAN_LAWFUL  &&   victim->alignment < -333 ) ||
	 ( clan->alignment == CLAN_CHAOTIC &&   victim->alignment > 333 ) )
    {
	send_to_char( "They do not suit your clan's alignment profile.\n\r", ch );
	return;
    }


    /* Acceptance system by Horus, 10-May-04 */

    if ( victim->pcdata->aclan )
    {
	if ( victim->pcdata->aclan == ch->pcdata->clan )
	{
	    send_to_char( "You've already extended an offer, wait for a response.\n\r", ch );
	    return;
	}
	else
	{
	    send_to_char( "They have been offered membership by someone else and haven't yet responded.\n\r", ch );
	    return;
	}
    }
    victim->pcdata->aclan = clan;
    act( AT_MAGIC, "You offer membership in $t to $N.", ch, clan->name, victim, TO_CHAR );
    act( AT_MAGIC, "$n offers membership in $t to $N.", ch, clan->name, victim, TO_NOTVICT );
    act( AT_MAGIC, "$n offers you membership in $t.", ch, clan->name, victim, TO_VICT );
    send_to_char( "\n\r&YTo accept, type 'join accept', to refuse, type 'join refuse'.\n\r", victim );
    return;
}

void do_join( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CLAN_DATA *clan;

    if ( IS_NPC( ch ) || !ch->pcdata->aclan )
    {
	send_to_char( "You haven't been offered membership in any clan.\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' || (str_cmp( arg, "accept" ) && str_cmp( arg, "refuse" )) )
    {
	send_to_char( "Accept or refuse?\n\r", ch );
	return;
    }

    clan = ch->pcdata->aclan;
    if ( !str_cmp( arg, "refuse" ) )
    {
	act( AT_MAGIC, "You refuse the offer to join $t.", ch, clan->name, NULL, TO_CHAR );
	act( AT_MAGIC, "$n refuses the offer to join $t.", ch, clan->name, NULL, TO_NOTVICT );
	ch->pcdata->aclan = NULL;
	return;
    }

    clan->members++;
    if ( clan->clan_type != CLAN_ORDER && clan->clan_type != CLAN_GUILD )
      SET_BIT(ch->speaks, LANG_CLAN);

    ch->pcdata->clan = clan;
    STRFREE(ch->pcdata->clan_name);
    ch->pcdata->clan_name = QUICKLINK( clan->name );
    act( AT_MAGIC, "You accept membership in $t.", ch, clan->name, NULL, TO_CHAR );
    act( AT_MAGIC, "$n accepts membership in $t.", ch, clan->name, NULL, TO_NOTVICT );
    sprintf( buf, "%s signs up to the %s!\n\r", ch->name, clan->name );
    talk_info( AT_LBLUE, buf );
    ch->pcdata->aclan = NULL;
    save_char_obj( ch );
    save_clan( clan );
    return;
}
    

void do_council_induct( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    COUNCIL_DATA *council;

    if ( IS_NPC( ch ) || !ch->pcdata->council )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    council = ch->pcdata->council;
    
  if ((council->head == NULL || str_cmp (ch->name, council->head))
      && ( council->head2 == NULL || str_cmp ( ch->name, council->head2 ))
      && str_cmp (council->name, "mortal council"))
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Induct whom into your council?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

/*    if ( victim->level < 51 )
    {
	send_to_char( "This player is not worthy of joining any council yet.\n\r", ch );
	return;
    }
*/
    if ( victim->pcdata->council )
    {
	send_to_char( "This player already belongs to a council!\n\r", ch );
	return;
    }

    council->members++;
    victim->pcdata->council = council;
    STRFREE(victim->pcdata->council_name);
    victim->pcdata->council_name = QUICKLINK( council->name );
    act( AT_MAGIC, "You induct $N into $t", ch, council->name, victim, TO_CHAR );
    act( AT_MAGIC, "$n inducts $N into $t", ch, council->name, victim, TO_ROOM );
    act( AT_MAGIC, "$n inducts you into $t", ch, council->name, victim, TO_VICT );
    save_char_obj( victim );
    save_council( council );
    return;
}

void do_outcast( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    CLAN_DATA *clan;
    char buf[MAX_STRING_LENGTH];

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;

    if ( ch->pcdata->clan->clan_type != CLAN_PLAIN )
    {
	send_to_char( "Your clan first needs divine approval.\n\r", ch );
	return;
    }

    if ( (ch->pcdata && ch->pcdata->bestowments
    &&    is_name("outcast", ch->pcdata->bestowments))
    ||   !str_cmp( ch->name, clan->leader  )
    ||   !str_cmp( ch->name, clan->number1 )
    ||   !str_cmp( ch->name, clan->number2 ) )
	;
    else
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }


    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Outcast whom?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    if ( victim == ch )
    {
	if ( ch->pcdata->clan->clan_type == CLAN_ORDER )
	{
	    send_to_char( "Kick yourself out of your own order?\n\r", ch );
	    return;
	}
	else
	if ( ch->pcdata->clan->clan_type == CLAN_GUILD )
	{
	    send_to_char( "Kick yourself out of your own guild?\n\r", ch );
	    return;
	}
	else
	{
	    send_to_char( "Kick yourself out of your own clan?\n\r", ch );
	    return;
	}
    }
/* 
    if ( victim->level > ch->level )
    {
	send_to_char( "This player is too powerful for you to outcast.\n\r", ch );
	return;
    }
*/
    if ( victim->pcdata->clan != ch->pcdata->clan )
    {
	if ( ch->pcdata->clan->clan_type == CLAN_ORDER )
	{
	    send_to_char( "This player does not belong to your order!\n\r", ch );
	    return;
	}
	else
	if ( ch->pcdata->clan->clan_type == CLAN_GUILD )
	{
	    send_to_char( "This player does not belong to your guild!\n\r", ch );
	    return;
	}
	else
	{
	    send_to_char( "This player does not belong to your clan!\n\r", ch );
	    return;
	}
    }

    if ( victim->speaking & LANG_CLAN )
        victim->speaking = LANG_COMMON;
    REMOVE_BIT( victim->speaks, LANG_CLAN );
    --clan->members;
    if ( !str_cmp( victim->name, ch->pcdata->clan->number1 ) )
    {
	STRFREE( ch->pcdata->clan->number1 );
	ch->pcdata->clan->number1 = STRALLOC( "" );
    }
    if ( !str_cmp( victim->name, ch->pcdata->clan->number2 ) )
    {
	STRFREE( ch->pcdata->clan->number2 );
	ch->pcdata->clan->number2 = STRALLOC( "" );
    }
    victim->pcdata->clan = NULL;
    STRFREE(victim->pcdata->clan_name);
    victim->pcdata->clan_name = STRALLOC( "" );
    act( AT_MAGIC, "You outcast $N from $t", ch, clan->name, victim, TO_CHAR );
    act( AT_MAGIC, "$n outcasts $N from $t", ch, clan->name, victim, TO_ROOM );
    act( AT_MAGIC, "$n outcasts you from $t", ch, clan->name, victim, TO_VICT );
    if ( clan->clan_type != CLAN_GUILD 
    &&   clan->clan_type != CLAN_ORDER )
    {
	sprintf(buf, "%s has been outcast from %s!", victim->name, clan->name);
	talk_info(AT_MAGIC, buf );
    }

/* Outcast flag setting removed by Narn.  It's useless now that deadlies
   remain deadly even on being cast out of a clan.
*/ 
/*    if ( clan->clan_type != CLAN_GUILD )
	xSET_BIT(victim->act, PLR_OUTCAST);
*/
    save_char_obj( victim );	/* clan gets saved when pfile is saved */
    save_clan( clan );
    return;
}

void do_council_outcast( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    COUNCIL_DATA *council;

    if ( IS_NPC( ch ) || !ch->pcdata->council )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    council = ch->pcdata->council;

  if ((council->head == NULL || str_cmp (ch->name, council->head))
      && ( council->head2 == NULL || str_cmp ( ch->name, council->head2 ))
      && str_cmp (council->name, "mortal council"))
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Outcast whom from your council?\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    if ( victim == ch )
    {
	send_to_char( "Kick yourself out of your own council?\n\r", ch );
	return;
    }
 
    if ( victim->pcdata->council != ch->pcdata->council )
    {
	send_to_char( "This player does not belong to your council!\n\r", ch );
	return;
    }

    --council->members;
    victim->pcdata->council = NULL;
    STRFREE(victim->pcdata->council_name);
    victim->pcdata->council_name = STRALLOC( "" );
    act( AT_MAGIC, "You outcast $N from $t", ch, council->name, victim, TO_CHAR );
    act( AT_MAGIC, "$n outcasts $N from $t", ch, council->name, victim, TO_ROOM );
    act( AT_MAGIC, "$n outcasts you from $t", ch, council->name, victim, TO_VICT );
    save_char_obj( victim );
    save_council( council );
    return;
}

void do_setclan( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    CLAN_DATA *clan;
    CHAR_DATA *victim;

    set_char_color( AT_PLAIN, ch );
    if ( IS_NPC( ch ) || ( ch->level < 50 && !ch->pcdata->clan ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    if ( ch->level < 50 && ( ( ch->played  / 60 ) / 60 ) < 400 )
    {
        send_to_char( "You require 400 hours of play to create a clan.\n\r", ch );
        return;
    }

    if ( ch->level > 50 )
	argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg2[0] == '\0' )
    {
        send_to_char( "You need to supply an argument.\n\r", ch );
	send_to_char( "&WUsage: &Csetclan &r<field> &O<value>\n\r", ch );
	send_to_char( "\n\r&RField&W being one of:\n\r", ch );
	send_to_char( " &Wnumber1 number2\n\r", ch ); 
	send_to_char( " &Wleadrank onerank tworank\n\r", ch );
	send_to_char( " &Wdesc motto badge note\n\r", ch );
	send_to_char( " &Rsubmit &Y(cannot be cancelled!)&w\n\r", ch );
	if ( get_trust( ch ) >= 60 )
	{
	    send_to_char( " command recall shop guard1 guard2\n\r", ch );
	    send_to_char( " leader align funds\n\r", ch );
  	    send_to_char( " obj1 obj2 obj3 obj4 obj5\n\r", ch );
	    send_to_char( " allies enemies morale\n\r", ch );
	    send_to_char( " active name filename\n\r", ch );
	    send_to_char( " authorise deny\n\r", ch );
	}
	return;
    }
    
    if ( ch->level > 50 )
	clan = get_clan( arg1 );
    else
	clan = ch->pcdata->clan;

    if ( !clan )
    {
	send_to_char( "No such clan.\n\r", ch );
	return;
    }
    if ( clan->clan_type != CLAN_INACTIVE  && ch->level < 50 )
    {
	send_to_char( "Your clan has already been submitted for divine authorisation!\n\r", ch );
	return;
    }
    if ( ch->level < 50 && str_cmp(clan->leader, ch->name) && str_cmp(clan->number1, ch->name) )
    {
	send_to_char( "Only a clan's leader and first officer may use this command.\n\r", ch );
    }

    if ( !str_cmp( arg2, "leader" ) )
    {
	STRFREE( clan->leader );
	clan->leader = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "number1" ) )
    {
        if ( ch->level < 50 && ( victim = get_char_room( ch, argument ) ) == NULL )
        {
	    send_to_char( "Your potential officer needs to be in the same room first.\n\r", ch );
            return;
        }
        if ( ch->level < 50 && ( IS_NPC(victim) || !victim->pcdata->clan || victim->pcdata->clan != clan ) )
        {
	    send_to_char( "They need to be in your clan, first - use the 'Induct' command.\n\r", ch );
            return;
        }
	STRFREE( clan->number1 );
	clan->number1 = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "number2" ) )
    {
        if ( ch->level < 50 && ( victim = get_char_room( ch, argument ) ) == NULL )
        {
	    send_to_char( "Your potential officer needs to be in the same room first.\n\r", ch );
            return;
        }
        if ( ch->level < 50 && ( IS_NPC(victim) || !victim->pcdata->clan || victim->pcdata->clan != clan ) )
        {
	    send_to_char( "They need to be in your clan, first - use the 'Induct' command.\n\r", ch );
            return;
        }
	STRFREE( clan->number2 );
	clan->number2 = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "leadrank" ) )
    {
	STRFREE( clan->leadrank );
	clan->leadrank = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "onerank" ) )
    {
	STRFREE( clan->onerank );
	clan->onerank = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "tworank" ) )
    {
	STRFREE( clan->tworank );
	clan->tworank = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "badge" ) )
    {
	STRFREE( clan->badge );
	clan->badge = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "motto" ) )
    {
	STRFREE( clan->motto );
	clan->motto = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "desc" ) )
    {
	STRFREE( clan->description );
	clan->description = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "note" ) )
    {
	STRFREE( clan->note );
	clan->note = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "submit" ) )
    {
	if ( clan->clan_type == CLAN_INACTIVE )
	    clan->clan_type = CLAN_NOKILL;
	send_to_char( "Your clan has now been submitted for divine authorisation.\n\r", ch );
	save_clan( clan );
	return;
    }



    if ( get_trust( ch ) < 60 )
    {
	do_setclan( ch, "" );
	return;
    }
    if ( !str_cmp( arg2, "align" ) )
    {
	if ( argument[0] == 'L' || argument[0] == 'l' )
	    clan->alignment = CLAN_LAWFUL;
	else if ( argument[0] == 'C' || argument[0] == 'c' )
	    clan->alignment = CLAN_CHAOTIC;
	else if ( argument[0] == 'N' || argument[0] == 'n' )
	    clan->alignment = CLAN_NEUTRAL;
	else
	{
	    send_to_char( "Alignment must be 'Lawful', 'Chaotic', or 'Neutral'", ch );	
	}
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "morale" ) )
    {
	clan->morale = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "recall" ) )
    {
	clan->recall = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "command" ) )
    {
	clan->command = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "shop" ) )
    {
	clan->storeroom = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "obj1" ) )
    {
	clan->clanobj1 = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "obj2" ) )
    {
	clan->clanobj2 = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "obj3" ) )
    {
	clan->clanobj3 = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "obj4" ) )
    {
        clan->clanobj4 = atoi( argument );
        send_to_char( "Done.\n\r", ch );
        save_clan( clan );
        return;
    }
    if ( !str_cmp( arg2, "obj5" ) )
    {
        clan->clanobj5 = atoi( argument );
        send_to_char( "Done.\n\r", ch );
        save_clan( clan );
        return;
    }
    if ( !str_cmp( arg2, "guard1" ) )
    {
	clan->guard1 = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "guard2" ) )
    {
	clan->guard2 = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "funds" ) )
    {
	clan->funds = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }

    if ( !str_cmp( arg2, "name" ) )
    {
	STRFREE( clan->name );
	clan->name = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "filename" ) )
    {
	if ( clan->filename );
	STRFREE( clan->filename );
	clan->filename = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	write_clan_list( );
	return;
    }
    if ( !str_cmp( arg2, "deny" ) )
    {
        clan->clan_type = CLAN_INACTIVE;
	echo_to_all( AT_RED, "With a divine crash of thunder, a clan is shut-down by the gods!", ECHOTAR_ALL );
	send_to_char( "Clan deactivated.  Best write a note using setclan to inform the clan leader what the problem is.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "authorise" ) )
    {
	clan->clan_type = CLAN_PLAIN;
	echo_to_all( AT_RED, "With a divine crash of thunder, a new clan opens for business!", ECHOTAR_ALL );
	send_to_char( "Clan authorised!\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "enemies" ) )
    {
	clan->waring = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "allies" ) )
    {
	clan->treaty = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }




    if ( ch->level < 65 )
    {
        do_setclan( ch, "" );
        return;
    }
    if ( !str_prefix( "pkill", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->pkills = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "pdeath", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->pdeaths = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "ckill", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->ckills = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "cdeath", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->cdeaths = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "okill", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->okills = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "odeath", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->odeaths = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_prefix( "oattempts", arg2) )
    {
	if ( !argument )
	{
		do_setclan ( ch, "" );
		return;
	}
	clan->odeaths = atoi( argument );
	send_to_char ("Ok.\n\r", ch );
	return;
    }
    if ( !str_cmp( arg2, "memlimit") )
    {
    	clan->mem_limit = atoi( argument );
	send_to_char( "Done.\n\r", ch  );
	save_clan( clan );
	return;
    }
    if ( !str_cmp( arg2, "members" ) )
    {
	clan->members = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_clan( clan );
	return;
    }

    do_setclan( ch, "" );
    return;
}

void do_setcouncil( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    COUNCIL_DATA *council;

    set_char_color( AT_PLAIN, ch );

    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    if ( arg1[0] == '\0' )
    {
	send_to_char( "Usage: setcouncil <council> <field> <deity|leader|number1|number2> <player>\n\r", ch );
	send_to_char( "\n\rField being one of:\n\r", ch );
	send_to_char( " head head2 members board meeting\n\r", ch ); 
	if ( get_trust( ch ) >= LEVEL_GOD )
	  send_to_char( " name filename desc\n\r", ch );
        if ( get_trust( ch ) >= LEVEL_SUB_IMPLEM )
	  send_to_char( " powers\n\r", ch);
	return;
    }

    council = get_council( arg1 );
    if ( !council )
    {
	send_to_char( "No such council.\n\r", ch );
	return;
    }
    if ( !str_cmp( arg2, "head" ) )
    {
	STRFREE( council->head );
	council->head = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }

  if (!str_cmp (arg2, "head2"))
    {
      if ( council->head2 != NULL )
        STRFREE (council->head2);
      if ( !str_cmp ( argument, "none" ) || !str_cmp ( argument, "clear" ) )
        council->head2 = NULL;
      else
        council->head2 = STRALLOC (argument);
      send_to_char ("Done.\n\r", ch);
      save_council (council);
      return;
    }
    if ( !str_cmp( arg2, "board" ) )
    {
	council->board = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    if ( !str_cmp( arg2, "members" ) )
    {
	council->members = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    if ( !str_cmp( arg2, "meeting" ) )
    {
	council->meeting = atoi( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    if ( get_trust( ch ) < LEVEL_GOD )
    {
	do_setcouncil( ch, "" );
	return;
    }
    if ( !str_cmp( arg2, "name" ) )
    {
	STRFREE( council->name );
	council->name = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    if ( !str_cmp( arg2, "filename" ) )
    {
	DISPOSE( council->filename );
	council->filename = str_dup( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	write_council_list( );
	return;
    }
    if ( !str_cmp( arg2, "desc" ) )
    {
	STRFREE( council->description );
	council->description = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    if ( get_trust( ch ) < LEVEL_SUB_IMPLEM )
    {
	do_setcouncil( ch, "" );
	return;
    }
    if ( !str_cmp( arg2, "powers" ) )
    {
	STRFREE( council->powers );
	council->powers = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_council( council );
	return;
    }
    
    do_setcouncil( ch, "" );
    return;
}

/*
 * Added multiple levels on pkills and pdeaths. -- Shaddai
 */

void do_showclan( CHAR_DATA *ch, char *argument )
{   
    CLAN_DATA *clan;

    set_char_color( AT_PLAIN, ch );

    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }
    if ( argument[0] == '\0' )
    {
	send_to_char( "Usage: showclan <clan>\n\r", ch );
	return;
    }

    clan = get_clan( argument );
    if ( !clan )
    {
	send_to_char( "No such clan, guild or order.\n\r", ch );
	return;
    }

    ch_printf_color( ch, "\n\r&w%s    : &W%s\t\t&wBadge: %s\n\r&wFilename : &W%s\n\r&wMotto    : &W%s\n\r",
			clan->clan_type == CLAN_ORDER ? "Order" :
		       (clan->clan_type == CLAN_GUILD ? "Guild" : "Clan "),
    			clan->name,
			clan->badge ? clan->badge : "(not set)",
    			clan->filename,
    			clan->motto );
    ch_printf_color( ch, "&wDesc     : &W%s\n\r",
    			clan->description );
    ch_printf_color( ch, "&wLeader   : &W%-19.19s\t&wRank: &W%s\n\r",
			clan->leader,
			clan->leadrank );
    ch_printf_color( ch, "&wNumber1  : &W%-19.19s\t&wRank: &W%s\n\r",
			clan->number1,
			clan->onerank );
    ch_printf_color( ch, "&wNumber2  : &W%-19.19s\t&wRank: &W%s\n\r",
			clan->number2,
			clan->tworank );
    ch_printf_color( ch, "&wPKills   : &W%-3d\n\r",  
    			clan->pkills );
    ch_printf_color( ch, "&wPDeaths  : &W%-5d\n\r",  
    			clan->pdeaths );
    ch_printf_color( ch, "&wMKills   : &W%-6d   &wMDeaths: &W%-6d\n\r",
    			clan->mkills,
    			clan->mdeaths );
    ch_printf_color( ch, "&wMembers  : &W%-6d  &wMemLimit: &W%-6d   &wAlign  : &W%-6d\n\r",
    			clan->members,
    			clan->mem_limit,
    			clan->alignment );
    ch_printf_color( ch, "&wRecall : &W%-5d    &wStorage: &W%-5d\n\r",
			clan->recall,
			clan->storeroom ); 
    ch_printf_color( ch, "&wGuard1   : &W%-5d    &wGuard2 : &W%-5d\n\r",
 			clan->guard1,
			clan->guard2 );
    ch_printf_color( ch, "&wObj1( &W%d &w)  Obj2( &W%d &w)  Obj3( &W%d &w)  Obj4( &W%d &w)  Obj5( &W%d &w)\n\r",
    			clan->clanobj1,
    			clan->clanobj2,
    			clan->clanobj3,
			clan->clanobj4,
			clan->clanobj5 );
    return;
}

void do_showcouncil( CHAR_DATA *ch, char *argument )
{
    COUNCIL_DATA *council;

    set_char_color( AT_PLAIN, ch );

    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }
    if ( argument[0] == '\0' )
    {
	send_to_char( "Usage: showcouncil <council>\n\r", ch );
	return;
    }

    council = get_council( argument );
    if ( !council )
    {
	send_to_char( "No such council.\n\r", ch );
	return;
    }

    ch_printf_color( ch, "\n\r&wCouncil :  &W%s\n\r&wFilename:  &W%s\n\r",
    			council->name,
    			council->filename );
  ch_printf_color (ch, "&wHead:      &W%s\n\r", council->head );
  ch_printf_color (ch, "&wHead2:     &W%s\n\r", council->head2 );
  ch_printf_color (ch, "&wMembers:   &W%-d\n\r", council->members );
    ch_printf_color( ch, "&wBoard:     &W%-5d\n\r&wMeeting:   &W%-5d\n\r&wPowers:    &W%s\n\r",
    			council->board,
    			council->meeting,
			council->powers );
    ch_printf_color( ch, "&wDescription:\n\r&W%s\n\r", council->description );
    return;
}

void do_makeclan( CHAR_DATA *ch, char *argument )
{
    char filename[256];
    CLAN_DATA *clan;
    bool found;
    char buf[MAX_STRING_LENGTH];  

    set_char_color( AT_IMMORT, ch );

    if ( !argument || argument[0] == '\0' )
    {
	send_to_char( "Usage: makeclan <clan name>\n\r", ch );
	return;
    }

    if ( get_clan( argument ) )
    {
	send_to_char( "A clan with that name exists already.\n\r", ch );
	return;
    }

    if ( ch->level > 50 )
    {
        send_to_char( "Clans are mortal affairs.\n\r", ch );
	return;
    }

    if ( ch->pcdata->clan )
    {
        send_to_char( "You are already in a clan.\n\r", ch );
        return;
    }

    if ( ( ( ch->played  / 60 ) / 60 ) < 400 )
    {
        send_to_char( "You require 400 hours of play to create a clan.\n\r", ch );
        return;
    }

    found = FALSE;
    sprintf( filename, "%s%s", CLAN_DIR, strlower(argument) );

    CREATE( clan, CLAN_DATA, 1 );
    LINK( clan, first_clan, last_clan, next, prev );

    clan->name		= 	STRALLOC( argument );
    clan->filename	= 	STRALLOC( filename ); 
    clan->motto	      	= 	STRALLOC( "" );
    clan->description   = 	STRALLOC( "" );
    clan->leader	= 	STRALLOC( ch->name );
    clan->number1	= 	STRALLOC( "" );
    clan->number2	= 	STRALLOC( "" );
    clan->leadrank	= 	STRALLOC( "Leader" );
    clan->onerank	= 	STRALLOC( "Lieutenant" );
    clan->tworank	= 	STRALLOC( "Knight" );
    clan->badge		= 	STRALLOC( "" );
    clan->note		= 	STRALLOC( "" );
    clan->clan_type	= 	CLAN_INACTIVE;
    clan->members   	=	1;
    clan->mem_limit 	=	10;
    clan->alignment	=	ch->alignment > 333 ? CLAN_LAWFUL : ch->alignment < -333 ? CLAN_CHAOTIC : CLAN_NEUTRAL;
    clan->funds		=	0;
    clan->morale	=	UMIN( 100, ch->perm_cha );
    ch->pcdata->clan    =	clan;
    ch->pcdata->clan_name    =	STRALLOC( clan->name );

    pager_printf( ch, "You found your clan '%s', and declare yourself its leader!\n\r", clan->name );
    sprintf( buf, "%s has founded '%s'!", ch->name, clan->name );
    talk_info( AT_WHITE, buf );
    pager_printf( ch, "You may now change the specifics of your clan using the 'setclan' command.\n\r"
		      "Before submitting your clan, you require:\n\r - A second and third in command\n\r"
		      " - A clan badge\n\r - An in-character clan description\n\r - A clan motto\n\r"
	              "If your application is rejected, you will be told why and allowed to make changes.\n\r" );

    save_clan( ch->pcdata->clan );
    do_save( ch, "auto" );
}

void do_makecouncil( CHAR_DATA *ch, char *argument )
{
    char filename[256];
    COUNCIL_DATA *council;
    bool found;

    set_char_color( AT_IMMORT, ch );

    if ( !argument || argument[0] == '\0' )
    {
	send_to_char( "Usage: makecouncil <council name>\n\r", ch );
	return;
    }

    found = FALSE;
    sprintf( filename, "%s%s", COUNCIL_DIR, strlower(argument) );

    CREATE( council, COUNCIL_DATA, 1 );
    LINK( council, first_council, last_council, next, prev );
    council->name		= STRALLOC( argument );
    council->head		= STRALLOC( "" );
    council->head2 		= NULL;
    council->powers		= STRALLOC( "" );
}

/*
 * Added multiple level pkill and pdeath support. --Shaddai
 */

void do_clans( CHAR_DATA *ch, char *argument )
{
    CLAN_DATA *clan;
    int count = 0;

    if ( argument[0] == '\0' )
    {
        set_char_color( AT_MAGIC, ch );
        send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information and a breakdown of victories.\n\r", ch );
        for ( clan = first_clan; clan; clan = clan->next )
        {
            if ( clan->clan_type != CLAN_PLAIN )
              continue;
	    pager_printf( ch, "&Y&WName:&Y   %-40s   &WAlignment: &G%-15s\n\r", clan->name, clan->alignment == CLAN_LAWFUL ? "Lawful" : clan->alignment == CLAN_CHAOTIC ? "Chaotic" : "Neutral"  );
	    pager_printf( ch, "&GLeader:&W %-8s &G              First: &W%-8s &G              Second &w%-8s\n\r", clan->leader, clan->number1, clan->number2 );
            pager_printf( ch, "&W---------------------------------------------------------------------\n\r&Y&W" );
            count++;
        }
        set_char_color( AT_MAGIC, ch );
        if ( !count )
          send_to_char( "There are no clans currently formed.  Try 'clans pending' or 'clans unfinished'.\n\r", ch );
        else
          send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information and a breakdown of victories, or 'clans pending', or 'clans unfinished'.\n\r", ch );
        return;
    }
    if ( !str_cmp( argument, "pending" ) )
    {
        set_char_color( AT_MAGIC, ch );
        send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information and a breakdown of victories.\n\r", ch );
        for ( clan = first_clan; clan; clan = clan->next )
        {
            if ( clan->clan_type != CLAN_NOKILL )
              continue;
	    pager_printf( ch, "&Y&WName:&Y   %-20s     &GLeader:&W %-8s    &WAlignment: &G%-15s\n\r", clan->name, clan->leader, clan->alignment == CLAN_LAWFUL ? "Lawful" : clan->alignment == CLAN_CHAOTIC ? "Chaotic" : "Neutral" );
            pager_printf( ch, "&W---------------------------------------------------------------------\n\r&Y&W" );
            count++;
        }
        set_char_color( AT_MAGIC, ch );
        if ( !count )
          send_to_char( "There are no clans currently pending.\n\r", ch );
        else
          send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information.\n\r", ch );
        return;

    }
    if ( !str_cmp( argument, "unfinished" ) )
    {
        set_char_color( AT_MAGIC, ch );
        send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information and a breakdown of victories.\n\r", ch );
        for ( clan = first_clan; clan; clan = clan->next )
        {
            if ( clan->clan_type != CLAN_INACTIVE )
              continue;
	    pager_printf( ch, "&Y&WName:&Y   %-20s     &GLeader:&W %-8s    &WAlignment: &G%-15s\n\r", clan->name, clan->leader, clan->alignment == CLAN_LAWFUL ? "Lawful" : clan->alignment == CLAN_CHAOTIC ? "Chaotic" : "Neutral" );
            pager_printf( ch, "&W---------------------------------------------------------------------\n\r&Y&W" );
            count++;
        }
        set_char_color( AT_MAGIC, ch );
        if ( !count )
          send_to_char( "There are no unfinished Clans.\n\r", ch );
        else
          send_to_char( "_________________________________________________________________________\n\r\n\r&Y&WUse 'clans <clan>' for detailed information.\n\r", ch );
        return;

    }

    clan = get_clan( argument );
    if ( !clan || clan->clan_type == CLAN_GUILD || clan->clan_type == CLAN_ORDER )
    {
        set_char_color( AT_BLOOD, ch );
        send_to_char( "No such clan.\n\r", ch );
        return;
    }
    set_char_color( AT_WHITE, ch );
    ch_printf( ch, "\n\r%s (%s), '%s'\n\r", clan->name, clan->badge, clan->motto );
    if ( ch->level > 50 || ch->pcdata->clan == clan )
        ch_printf( ch, "&Y  Clan Note:  %s\n\r", clan->note );
    set_char_color( AT_WHITE, ch );
    send_to_char_color( "\n\r&RStats:&w\n\r", ch );
    set_char_color( AT_GREY, ch );
    ch_printf( ch, "&w Leader     :    %-8s\tNumber One :  %-8s\tNumber Two :  %-8s\tFunds    :  %-8s\n\r",  clan->leader, clan->number1, clan->number2, format_pl( clan->funds ) );
    ch_printf( ch, "&w Members    :    %d/%d\t\tKills      :  %d\t\tDeaths     :  %d\n\r",clan->members, clan->mem_limit, clan->pkills, clan->pdeaths );
    ch_printf( ch, "&RAssassinations                   Clan Wars\n\r");
    ch_printf( ch, "&W Successful :  %d		Kills  :  %d\n\r Attempts   :  %d		Deaths :  %d\n\r Suffered   :  %d\n\r",clan->okills, clan->ckills, clan->oattempts, clan->cdeaths, clan->odeaths );
    ch_printf( ch, "\n\r&Y&W&RDescription:  %s\n\r", clan->description );
    return; 
}


void do_orders( CHAR_DATA *ch, char *argument )
{
    CLAN_DATA *order;
    int count = 0;

    if ( argument[0] == '\0' )
    {
        set_char_color( AT_DGREEN, ch );
        send_to_char( "\n\rOrder            Deity          Leader           Mkills      Mdeaths\n\r____________________________________________________________________\n\r\n\r", ch );
	set_char_color( AT_GREEN, ch );
        for ( order = first_clan; order; order = order->next )
        if ( order->clan_type == CLAN_ORDER )
        {
            ch_printf( ch, "%-16s  %-14s   %-7d       %5d\n\r",
	      order->name, order->leader, order->mkills, order->mdeaths );
            count++;
	}
        set_char_color( AT_DGREEN, ch );
	if ( !count )
	  send_to_char( "There are no Orders currently formed.\n\r", ch );
	else
	  send_to_char( "____________________________________________________________________\n\r\n\rUse 'orders <order>' for more detailed information.\n\r", ch );
	return;
    }

    order = get_clan( argument );
    if ( !order || order->clan_type != CLAN_ORDER )
    {
        set_char_color( AT_DGREEN, ch );
        send_to_char( "No such Order.\n\r", ch );
        return;
    }
 
    set_char_color( AT_DGREEN, ch );
    ch_printf( ch, "\n\rOrder of %s\n\r'%s'\n\r\n\r", order->name, order->motto );
    set_char_color( AT_GREEN, ch );
    ch_printf( ch, "Leader     :  %s\n\rNumber One :  %s\n\rNumber Two :  %s\n\r",
                        order->leader,
                        order->number1,
                        order->number2 );
    if ( ch->level > 50
    ||   !str_cmp( ch->name, order->leader  )
    ||   !str_cmp( ch->name, order->number1 )
    ||   !str_cmp( ch->name, order->number2 ) )
        ch_printf( ch, "Members    :  %d\n\r",
			order->members );
    set_char_color( AT_DGREEN, ch );
    ch_printf( ch, "\n\rDescription:\n\r%s\n\r", order->description );
    return;
}

void do_councils( CHAR_DATA *ch, char *argument)
{
    COUNCIL_DATA *council;

    set_char_color( AT_CYAN, ch );
    if ( !first_council )
    {
	send_to_char( "There are no councils currently formed.\n\r", ch );
	return;
    }
    if ( argument[0] == '\0' )
    {
        send_to_char_color ("\n\r&cTitle                    Head\n\r", ch);
      for (council = first_council; council; council = council->next)
      {
        if ( council->head2 != NULL )
           ch_printf_color (ch, "&w%-24s %s and %s\n\r",  council->name,
                council->head, council->head2 );
        else
           ch_printf_color (ch, "&w%-24s %-14s\n\r", council->name, council->head);
      }
      send_to_char_color( "&cUse 'councils <name of council>' for more detailed information.\n\r", ch );
      return;
    }        
    council = get_council( argument );
    if ( !council )
    {
      send_to_char_color( "&cNo such council exists...\n\r", ch );
      return;
    }
    ch_printf_color( ch, "&c\n\r%s\n\r", council->name );
  if ( council->head2 == NULL )
        ch_printf_color (ch, "&cHead:     &w%s\n\r&cMembers:  &w%d\n\r",
        council->head, council->members );
  else
        ch_printf_color (ch, "&cCo-Heads:     &w%s &cand &w%s\n\r&cMembers:  &w%d\n\r",
           council->head, council->head2, council->members );
    ch_printf_color( ch, "&cDescription:\n\r&w%s\n\r",
        council->description );
    return;
} 

void do_guilds( CHAR_DATA *ch, char *argument)
{
/*
    char buf[MAX_STRING_LENGTH];  
    CLAN_DATA *guild;
    int count = 0;

    if ( argument[0] == '\0' )
    {
        set_char_color( AT_HUNGRY, ch );
        send_to_char( "\n\rGuild                  Leader             Mkills      Mdeaths\n\r_____________________________________________________________\n\r\n\r", ch );
	set_char_color( AT_YELLOW, ch );
        for ( guild = first_clan; guild; guild = guild->next )
        if ( guild->clan_type == CLAN_GUILD )
	{
	    ++count;
	    ch_printf( ch, "%-20s   %-14s     %-6d       %6d\n\r", guild->name, guild->leader, guild->mkills, guild->mdeaths );
	}
        set_char_color( AT_HUNGRY, ch );
        if ( !count )
          send_to_char( "There are no Guilds currently formed.\n\r", ch );
        else
          send_to_char( "_____________________________________________________________\n\r\n\rUse guilds <class>' for specifics. (ex:  guilds thieves)\n\r", ch );
	return;
    }

    sprintf( buf, "guild of %s", argument );
    guild = get_clan( buf );
    if ( !guild || guild->clan_type != CLAN_GUILD )
    {
        set_char_color( AT_HUNGRY, ch );
        send_to_char( "No such Guild.\n\r", ch );
        return;
    }
    set_char_color( AT_HUNGRY, ch );
    ch_printf( ch, "\n\r%s\n\r", guild->name );
    set_char_color( AT_YELLOW, ch );
    ch_printf( ch, "Leader:    %s\n\rNumber 1:  %s\n\rNumber 2:  %s\n\rMotto:     %s\n\r",
                        guild->leader,
                        guild->number1,
                        guild->number2,
			guild->motto );
    if ( ch->level > 50 
    ||   !str_cmp( ch->name, guild->leader  )
    ||   !str_cmp( ch->name, guild->number1 )
    ||   !str_cmp( ch->name, guild->number2 ) )
        ch_printf( ch, "Members:   %d\n\r",
			guild->members );
    set_char_color( AT_HUNGRY, ch );
    ch_printf( ch, "Guild Description:\n\r%s\n\r", guild->description );
    return;
*/
return;
}                                                                           

void do_victories( CHAR_DATA *ch, char *argument )
{
    char filename[256]; 

    if ( IS_NPC( ch ) || !ch->pcdata->clan ) {
        send_to_char( "Huh?\n\r", ch );
        return;
    }
    if ( ch->pcdata->clan->clan_type != CLAN_ORDER
    &&   ch->pcdata->clan->clan_type != CLAN_GUILD ) {
        sprintf( filename, "%s%s.record",
          CLAN_DIR, ch->pcdata->clan->name );
	set_pager_color( AT_PURPLE, ch );
	if ( !str_cmp( ch->name, ch->pcdata->clan->leader )
	&&   !str_cmp( argument, "clean" ) ) {
	  FILE *fp = Fopen( filename, "w" );
	  if ( fp )
	    Fclose( fp );
	  send_to_pager( "\n\rVictories ledger has been cleared.\n\r", ch );
	  return;
	} else {
	  send_to_pager( "\n\rLVL  Character       LVL  Character\n\r", ch );
	  show_file( ch, filename );
	  return;
	}
    }
    else
    {
        send_to_char( "Huh?\n\r", ch );
        return;
    }
}


void do_shove( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    int exit_dir;
    EXIT_DATA *pexit;
    CHAR_DATA *victim;
    bool nogo;
    ROOM_INDEX_DATA *to_room;    
    int chance = 0;
    int race_bonus = 0;

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if  ( get_timer(ch, TIMER_PKILLED) > 0)
    {
	send_to_char("You can't shove a player right now.\n\r", ch);
	return;
    }

    if ( arg[0] == '\0' )
    {
	send_to_char( "Shove whom?\n\r", ch);
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch);
	return;
    }


    if (victim == ch)
    {
	send_to_char("You shove yourself around, to no avail.\n\r", ch);
	return;
    }
    
    if  ( IS_NPC(victim) || xIS_SET(ch->act, PLR_KILLER) || victim->skill_timer > 0 )
    {
	send_to_char("You can't shove that player right now.\n\r", ch);
	return;
    }

    if ( (victim->position) != POS_STANDING )
    {
	act( AT_PLAIN, "$N isn't standing up.", ch, NULL, victim, TO_CHAR );
	return;
    }

    if ( arg2[0] == '\0' )
    {
	send_to_char( "Shove them in which direction?\n\r", ch);
	return;
    }

    exit_dir = get_dir( arg2 );

    victim->position = POS_SHOVE;
    nogo = FALSE;
    if ((pexit = get_exit(ch->in_room, exit_dir)) == NULL )
      nogo = TRUE;
    else
    if ( IS_SET(pexit->exit_info, EX_CLOSED)
    && (!IS_AFFECTED(victim, AFF_PASS_DOOR)
    ||   IS_SET(pexit->exit_info, EX_NOPASSDOOR)) )
      nogo = TRUE;
    if ( nogo )
    {
	send_to_char( "There's no exit in that direction.\n\r", ch );
        victim->position = POS_STANDING;
	return;
    }
    to_room = pexit->to_room;
    if (IS_SET(to_room->room_flags, ROOM_DEATH))
    {
      send_to_char("You cannot shove someone into a death trap.\n\r", ch);
      victim->position = POS_STANDING;
      return;
    }
    
    if (ch->in_room->area != to_room->area
    &&  !in_hard_range( victim, to_room->area ) )
    {
      send_to_char("That character cannot enter that area.\n\r", ch);
      victim->position = POS_STANDING;
      return;
    }

/* Check for class, assign percentage based on that. */
if (ch->class == CLASS_ELITE)
  chance = 70;
if (ch->class == CLASS_TREACHEROUS)
  chance = 65;
if (ch->class == CLASS_HENCHMEN)
  chance = 60;
if (ch->class == CLASS_LOW_LEVEL)
  chance = 45;

/* Add 3 points to chance for every str point above 15, subtract for 
below 15 */

chance += ((get_curr_str(ch) - 15) * 3);

chance += (ch->level - victim->level);

if (ch->race == 1)
race_bonus = -3;

if (ch->race == 2)
race_bonus = 3;

if (ch->race == 3)
race_bonus = -5;

if (ch->race == 4)
race_bonus = -7;
 
if (ch->race == 6)
race_bonus = 5;
 
if (ch->race == 7)
race_bonus = 7;
 
if (ch->race == 8)
race_bonus = 10;
 
if (ch->race == 9)
race_bonus = -2;
 
chance += race_bonus;
 
/* Debugging purposes - show percentage for testing */

/* sprintf(buf, "Shove percentage of %s = %d", ch->name, chance);
act( AT_ACTION, buf, ch, NULL, NULL, TO_ROOM );
*/

if (chance < number_percent( ))
{
  send_to_char("You failed.\n\r", ch);
  victim->position = POS_STANDING;
  return;
}
    do_follow( victim, "self" );
    act( AT_ACTION, "You shove $M.", ch, NULL, victim, TO_CHAR );
    act( AT_ACTION, "$n shoves you.", ch, NULL, victim, TO_VICT );
    move_char( victim, get_exit(ch->in_room,exit_dir), 0);
    if ( !char_died(victim) )
      victim->position = POS_STANDING;
    WAIT_STATE(ch, 12);
    /* Remove protection from shove/drag if char shoves -- Blodkai */
}

void do_drag( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    int exit_dir;
    CHAR_DATA *victim;
    EXIT_DATA *pexit;
    ROOM_INDEX_DATA *to_room;
    bool nogo;
    int chance = 0;
    int race_bonus = 0;

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if ( IS_NPC(ch) )
    /* || !IS_SET( ch->pcdata->flags, PCFLAG_DEADLY ) )  */
    {
	send_to_char("Only characters can drag.\n\r", ch);
	return;
    }

    if  ( get_timer(ch, TIMER_PKILLED) > 0)
    {
	send_to_char("You can't drag a player right now.\n\r", ch);
	return;
    }

    if ( arg[0] == '\0' )
    {
	send_to_char( "Drag whom?\n\r", ch);
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "They aren't here.\n\r", ch);
	return;
    }

    if ( victim == ch )
    {
	send_to_char("You take yourself by the scruff of your neck, but go nowhere.\n\r", ch);
	return; 
    }

    if ( IS_NPC(victim)  )
         /* || !IS_SET( victim->pcdata->flags, PCFLAG_DEADLY ) ) */
    {
	send_to_char("You can only drag characters.\n\r", ch);
	return;
    }


    if ( victim->fighting )
    {
        send_to_char( "You try, but can't get close enough.\n\r", ch);
        return;
    }
          
    if ( arg2[0] == '\0' )
    {
	send_to_char( "Drag them in which direction?\n\r", ch);
	return;
    }


    exit_dir = get_dir( arg2 );


    nogo = FALSE;
    if ((pexit = get_exit(ch->in_room, exit_dir)) == NULL )
      nogo = TRUE;
    else
    if ( IS_SET(pexit->exit_info, EX_CLOSED)
    && (!IS_AFFECTED(victim, AFF_PASS_DOOR)
    ||   IS_SET(pexit->exit_info, EX_NOPASSDOOR)) )
      nogo = TRUE;
    if ( nogo )
    {
	send_to_char( "There's no exit in that direction.\n\r", ch );
	return;
    }

    to_room = pexit->to_room;
    if (IS_SET(to_room->room_flags, ROOM_DEATH))
    {
      send_to_char("You cannot drag someone into a death trap.\n\r", ch);
      return;
    }

    if (ch->in_room->area != to_room->area
    && !in_hard_range( victim, to_room->area ) )
    {
      send_to_char("That character cannot enter that area.\n\r", ch);
      victim->position = POS_STANDING;
      return;
    }
    
/* Check for class, assign percentage based on that. */
if (ch->class == CLASS_ELITE)
  chance = 70;
if (ch->class == CLASS_TREACHEROUS)
  chance = 65;
if (ch->class == CLASS_HENCHMEN)
  chance = 60;
if (ch->class == CLASS_LOW_LEVEL)
  chance = 45;

/* Add 3 points to chance for every str point above 15, subtract for 
below 15 */

chance += ((get_curr_str(ch) - 15) * 3);

chance += (ch->level - victim->level);

if (ch->race == 1)
race_bonus = -3;

if (ch->race == 2)
race_bonus = 3;

if (ch->race == 3)
race_bonus = -5;

if (ch->race == 4)
race_bonus = -7;

if (ch->race == 6)
race_bonus = 5;

if (ch->race == 7)
race_bonus = 7;

if (ch->race == 8)
race_bonus = 10;

if (ch->race == 9)
race_bonus = -2;

chance += race_bonus;
/*
sprintf(buf, "Drag percentage of %s = %d", ch->name, chance);
act( AT_ACTION, buf, ch, NULL, NULL, TO_ROOM );
*/
if (chance < number_percent( ))
{
  send_to_char("You failed.\n\r", ch);
  victim->position = POS_STANDING;
  return;
}
    if ( victim->position < POS_STANDING )
    {
	sh_int temp;

	temp = victim->position;
	victim->position = POS_DRAG;
	act( AT_ACTION, "You drag $M into the next room.", ch, NULL, victim, TO_CHAR ); 
	act( AT_ACTION, "$n grabs your hair and drags you.", ch, NULL, victim, TO_VICT ); 
	move_char( victim, get_exit(ch->in_room,exit_dir), 0);
	if ( !char_died(victim) )
	  victim->position = temp;
/* Move ch to the room too.. they are doing dragging - Scryn */
	move_char( ch, get_exit(ch->in_room,exit_dir), 0);
	WAIT_STATE(ch, 12);
	return;
    }
    send_to_char("You cannot do that to someone who is standing.\n\r", ch);
    return;
}

void do_clanbank( CHAR_DATA *ch, char *argument )
{
 char arg1[MAX_STRING_LENGTH];
 char arg2[MAX_STRING_LENGTH];
 int  cash = 0;

 if ( IS_NPC(ch) )
	return;

 if ( !ch->pcdata->clan )
 {
	send_to_char( "Huh?", ch );
	return;
 }

 if ( ch->pcdata->clan->clan_type != CLAN_PLAIN )
 {
     send_to_char( "Your clan first needs divine approval.\n\r", ch );
     return;
 }

 argument = one_argument( argument, arg1 );
 argument = one_argument( argument, arg2 );

 if ( str_cmp(ch->pcdata->clan->leader, ch->name) )
 {
	send_to_char( "Ask your clan leader.\n\r", ch );
	return;
 }

 if ( arg1[0] == 'w' )
 {
	if ( !is_number(arg2) )
	{
	    send_to_char( "How much?", ch );
	    return;
	}
	cash = atoi( arg2 );
	if ( cash < ch->pcdata->clan->funds )
	{
	    ch->gold += cash;
	    ch->pcdata->clan->funds -= cash;
	    save_clan( ch->pcdata->clan );
	    send_to_char( "Done.\n\r", ch );
	    return;
	}
	else
	{
	    send_to_char( "Your clan doesn't have that much.\n\r", ch );
	    return;
	}
	return;
 }
 if ( arg1[0] == 'd' )
 {
	if ( !is_number(arg2) )
	{
	send_to_char( "How much?", ch );
	return;
	}
	cash = atoi( arg2 );
	if ( cash < ch->gold )
	{
	ch->gold -= cash;
	ch->pcdata->clan->funds += cash;
	save_clan( ch->pcdata->clan );
	send_to_char( "Done.\n\r", ch );
	return;
	}
	else
	{
	send_to_char( "You don't have that much.\n\r", ch );
	return;
	}
	return;
 }
 send_to_char( "Bank <deposit/withdraw> <amount>\n\r", ch );
 return;
}

/* Revamped by Horus, 10-May-04 */
void do_appoint( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    CLAN_DATA *clan;

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;
    
    if ( str_cmp( ch->name, clan->leader  )
    &&   str_cmp( ch->name, clan->number1  ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Appoint whom?  Syntax: appoint <person> <1/2>\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    if ( ch->pcdata->clan->clan_type != CLAN_PLAIN )
    {
	send_to_char( "Your clan first needs divine approval.\n\r", ch );
	return;
    }

    if ( IS_IMMORTAL(victim) )
    {
	send_to_char( "You can't appoint such a godly presence.\n\r", ch );
	return;
    }

    if ( !victim->pcdata->clan || ch->pcdata->clan != victim->pcdata->clan )
    {
	pager_printf( ch, "But they aren't in your clan...\n\r" );
	return;
    }

    if ( arg2[0] == '\0' || ( str_cmp( arg2, "1" ) && str_cmp( arg2, "2" ) ) )
    {
	send_to_char( "Promote them to what rank?\n\r", ch );
	return;
    }

    if ( !str_cmp( arg2, "1" ) )
    {
	if ( clan->number1 && !str_cmp( victim->name, clan->number1 ) )
	{
	    send_to_char( "They are already the first.\n\r", ch );
	    return;
	}
	if ( strlen(clan->number1)>2)
	{
	    send_to_char( "You already have a first.\n\r", ch );
	    return;
	}
	clan->number1 = STRALLOC( victim->name );
	save_clan( clan );
	sprintf( buf, "%s is promoted to number one in %s!", victim->name, clan->name );
	talk_info( AT_PINK, buf );
	return;
    }

    if ( !str_cmp( arg2, "2" ) )
    {
	if ( clan->number2 && !str_cmp( victim->name, clan->number2 ) )
	{
	    send_to_char( "They are already the second.\n\r", ch );
	    return;
	}
	if ( strlen(clan->number2)>2)
	{
	    send_to_char( "You already have a second.\n\r", ch );
	    return;
	}
	clan->number2 = STRALLOC( victim->name );
	save_clan( clan );
	sprintf( buf, "%s is promoted to number two in %s!", victim->name, clan->name );
	talk_info( AT_PINK, buf );
	return;
    }

    return;
}


void do_battlestations ( CHAR_DATA *ch, char * argument )
{
  CHAR_DATA 	*victim;
  unsigned long  room;

  
  if ( !IS_NPC( ch ) )
  {
	send_to_char( "Huh?\n\r", ch );
	return;
  }

  for ( victim = first_char ; victim ; victim = victim->next )
  {
	if ( !str_cmp( argument, victim->name ) )
	    break;
  }

  switch ( victim->race )
  {
	default:
		do_say( ch, "Hmm... Best send you to Earth, I guess.  New South City, here you come!" );
		room = 3000;
	break;
	case RACE_KAI:
		do_say ( ch, "I'm sending you to the Kaio homeworld.  Be sure to explore well before moving on again." );
		room = 1666;
	break;
	case RACE_SAIYAN:
		do_say ( ch, "Vegeta has recently been restored to it's former glory.  I'll send you there." );
		room = 10908;
	break;
	case RACE_TUFFLE:
		do_say( ch, "The asteroid is the best place for you troublesome Tuffles!" );
		room = 731;
	break;
	case RACE_DEMON:
		do_say( ch, "Eugh... More Demon scum!  I suppose you want some damn candy now." );
		room = 2600;
	break;
	case RACE_HALFBREED:
		do_say( ch, "Hehehe... Back to the future you go!" );
		room = 1304;
	break;
	case RACE_ICER:
		if ( ch->basepl < 500000 )
		{
		    do_say( ch, "Ah... One of Frieza's goons... Off you go, to the ship." );
		    room = 5831;
		}
		else
		{
		    do_say( ch, "I guess you're through with your conscription for Frieza... I'll send you to Icer." );
		    room = 5200;
		}
	break;
	case RACE_MUTANT:
		if ( ch->basepl < 500000 )
		{
		    do_say( ch, "Ah... One of Frieza's goons... Off you go, to the ship." );
		    room = 5831;
		}
		else
		{
		    do_say( ch, "It's back to your Home Planet for you, with all the other smelly, ugly creatures!" );
		    room = 1964;
		}
	break;
	case RACE_ANDROID:
	case RACE_BIOANDROID:
		    do_say( ch, "Gero *will* be pleased..." );
		    room = 905;
	break;
	case RACE_NAMEK:
		    do_say( ch, "Off to Namek!" );
		    room = 8000;
	break;
	case RACE_WIZARD:
		    do_say( ch, "Wizard?  Time to send you home then..." );
		    room = 10400;
	break;
	case RACE_HUMAN:
		    do_say( ch, "I guess Roshi will be glad of some company, huh?" );
		    room = 2501;
	break;
  }
  char_from_room( victim );
  char_to_room( victim, get_room_index( room ) );
  do_look( victim, "auto" );
}

void do_resign( CHAR_DATA *ch, char *argument )
{
    CLAN_DATA *clan;
    char buf[MAX_STRING_LENGTH];

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;

    if ( ch->pcdata->clan->clan_type != CLAN_PLAIN )
    {
	send_to_char( "Your clan first needs divine approval.\n\r", ch );
	return;
    }

    if ( str_cmp( argument, "confirm" ) )
    {
	send_to_char( "Are you sure?  Type 'Resign confirm' to leave the clan.\n\r", ch );
	return;
    }

    if ( ch->speaking & LANG_CLAN )
        ch->speaking = LANG_COMMON;
    REMOVE_BIT( ch->speaks, LANG_CLAN );
    --clan->members;

    if ( !str_cmp( ch->name, ch->pcdata->clan->number1 ) )
    {
	STRFREE( ch->pcdata->clan->number1 );
	ch->pcdata->clan->number1 = STRALLOC( "" );
    }
    if ( !str_cmp( ch->name, ch->pcdata->clan->number2 ) )
    {
	STRFREE( ch->pcdata->clan->number2 );
	ch->pcdata->clan->number2 = STRALLOC( "" );
    }
    if ( !str_cmp( ch->name, clan->leader  ) )
    {
	STRFREE( ch->pcdata->clan->leader);
	ch->pcdata->clan->leader = STRALLOC( ch->pcdata->clan->number1 );
	STRFREE( ch->pcdata->clan->number1);
	ch->pcdata->clan->number1 = STRALLOC( "" );
    }

    ch->pcdata->clan = NULL;
    STRFREE(ch->pcdata->clan_name);
    ch->pcdata->clan_name = STRALLOC( "" );
    act( AT_MAGIC, "You resign from $t", ch, clan->name, NULL, TO_CHAR );
    act( AT_MAGIC, "$n resigns from $t", ch, clan->name, NULL, TO_ROOM );

    if ( clan->clan_type != CLAN_GUILD && clan->clan_type != CLAN_ORDER )
    {
	sprintf(buf, "%s has left %s", ch->name, clan->name);
	talk_info(AT_MAGIC, buf );
    }

    save_char_obj( ch );
    save_clan( clan );
    return;
}


void do_demote( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
    CLAN_DATA *clan;

    if ( IS_NPC( ch ) || !ch->pcdata->clan )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    clan = ch->pcdata->clan;
    
    if ( str_cmp( ch->name, clan->leader  ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg );
    argument = one_argument( argument, arg2 );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Demote whom?  Syntax: demote <person> <1/2>\n\r", ch );
	return;
    }

    if ( ( victim = get_char_room( ch, arg ) ) == NULL )
    {
	send_to_char( "That player is not here.\n\r", ch);
	return;
    }

    if ( ch->pcdata->clan->clan_type != CLAN_PLAIN )
    {
	send_to_char( "Your clan first needs divine approval.\n\r", ch );
	return;
    }

    if ( !victim->pcdata->clan || ch->pcdata->clan != victim->pcdata->clan )
    {
	pager_printf( ch, "But they aren't in your clan...\n\r" );
	return;
    }

    if ( !str_cmp(victim->pcdata->clan->number1, victim->name) )
    {
	STRFREE(clan->number1);
        clan->number1 = STRALLOC( "" );
    }
    else if ( !str_cmp(victim->pcdata->clan->number2, victim->name) )
    {
	STRFREE(clan->number2);
        clan->number2 = STRALLOC( "" );        
    }
    else
    {
	send_to_char( "They are not an officer in your clan.\n\r", ch );
	return;
    }
    save_char_obj(victim);
    save_clan( clan );
    sprintf( buf, "%s is demoted within %s.", victim->name, clan->name );
    talk_info( AT_PINK, buf );
    return;
}

