/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag746, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 *                     Housing Module Source File                           *
 ****************************************************************************
 * Author : Senir                                                           *
 * E-Mail : oldgaijin@yahoo.com                                             *
 ****************************************************************************/

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include "mud.h"

void instaroom      args( ( CHAR_DATA *ch, ROOM_INDEX_DATA *room, bool dodoors ) );
void wipe_resets    args( ( ROOM_INDEX_DATA *pRoom ) );
char *strrep( const char *src, const char *sch, const char *rep );
int is_on_guestlist( CHAR_DATA *ch, char *who );
void save_house_room( int vnum );

void do_house( CHAR_DATA *ch, char *argument )
{
  int x;
  char arg1[MAX_INPUT_LENGTH];
  char arg2[MAX_INPUT_LENGTH];
  char arg3[MAX_INPUT_LENGTH];

  argument = one_argument(argument, arg1);
  argument = one_argument(argument, arg2);
  argument = one_argument(argument, arg3);

  if ( ch->position != POS_STANDING )
  {
	send_to_char( "Stand up first.\n\r", ch );
	return;
  }

  if ( IS_NPC(ch) )
  {
	send_to_char( "NPCs cannot own houses.\n\r", ch );
	return;
  }

  if ( !str_cmp( arg1, "saveall" ) && ch->level == 65 )
  {
      AREA_DATA *house_area = get_area( HOUSE_AREA );
      for ( x = house_area->low_r_vnum; x <= house_area->hi_r_vnum; x++ )
      {
          ROOM_INDEX_DATA * room = get_room_index(x);
          if ( room )
          {
	      save_house_room( x );
          }
      }
      return;
  }

  if ( !str_cmp( arg1, "enter" ) )
  {
	if ( arg2[0] == '\0' )
	{
	    if ( ch->pcdata->house_vnum == 0 ) 
	    {
		send_to_char( "Enter whose house?\n\r", ch );
		return;
	    }
		if ( (ch->in_room->vnum == RES_ZONE_EARTH && ch->pcdata->house_planet != RES_ZONE_EARTH) ||
		     (ch->in_room->vnum == RES_ZONE_NAMEK && ch->pcdata->house_planet != RES_ZONE_NAMEK) ||
		     (ch->in_room->vnum == RES_ZONE_VEGETA && ch->pcdata->house_planet != RES_ZONE_VEGETA) ||
        	     (ch->in_room->vnum == RES_ZONE_ICER && ch->pcdata->house_planet != RES_ZONE_ICER) )
		{
		    send_to_char( "You are nowhere near your house!  Go to the correct residential zone.\n\r", ch );
		    return;
		}

		if ( ch->in_room->vnum != RES_ZONE_EARTH  && ch->in_room->vnum != RES_ZONE_NAMEK 
		  && ch->in_room->vnum != RES_ZONE_VEGETA && ch->in_room->vnum != RES_ZONE_ICER )
		{
		    send_to_char( "You are nowhere near your house!  Go to the residential zone.\n\r", ch );
		    return;
		}

		act( AT_ACTION, "$n enters $s home, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
		char_from_room( ch );
		char_to_room( ch, get_room_index(ch->pcdata->house_vnum) );
		send_to_char( "You enter your home, closing the door behind you.\n\r", ch );
		act( AT_ACTION, "$n enters, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
		do_look( ch, "" );
		return;
	}
	else
	{
	    int housenum = is_on_guestlist( ch, arg2 );
	    if ( housenum == 0 )
		return;
	    act( AT_ACTION, "$n enters a house, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
	    if ( ch->pcdata->house_planet == 0 ) 	
	        ch->pcdata->house_planet = ch->in_room->vnum;
	    char_from_room( ch );
	    char_to_room( ch, get_room_index(housenum) );
	    send_to_char( "You enter the house, closing the door behind you.\n\r", ch );
	    act( AT_ACTION, "$n enters, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
	    do_look( ch, "" );
	    return;
	}
  }

  if ( !str_cmp( arg1, "leave"  ) )
  {
      OBJ_DATA *obj;
      bool found = FALSE;
      if ( ch->in_room->vnum < 25000 || ch->in_room->vnum > 25999 )
      {
	  send_to_char( "Not really possible unless you're in a house...\n\r", ch );
	  return;
      }
      for ( obj = ch->first_carrying; obj; obj = obj->next_content )
      {
	  if ( obj->item_type == ITEM_FURNITURE )
	  {
	      found = TRUE;
	      break;
	  }
      }

      if ( found )
      {
	  send_to_char( "You are currently holding furniture... Can't leave until you drop it.\n\r", ch );
	  return;
      }
      act( AT_ACTION, "$n leaves, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
      char_from_room( ch );
	  char_to_room( ch, get_room_index(ch->pcdata->house_planet) );
      send_to_char( "You leave the house, closing the door firmly behind you.\n\r", ch );     
      act( AT_ACTION, "$n steps out of a house, closing the door firmly behind $m.", ch, NULL, NULL, TO_CANSEE );
      do_look( ch, "" );
      return;
  }

  if ( !str_cmp( arg1, "buy" ) && ch->pcdata->house_type == HOUSE_NONE )
  {
      if ( ch->in_room->vnum != RES_ZONE_EARTH  && ch->in_room->vnum != RES_ZONE_NAMEK
        && ch->in_room->vnum != RES_ZONE_VEGETA && ch->in_room->vnum != RES_ZONE_ICER 
        && ch->in_room->vnum != RES_ZONE_BRUMA )
      {
          send_to_char( "You must first locate a residential zone.\n\r", ch );
	  return;
      }

      if ( !str_cmp( arg2, "home" ) )
      {
          AREA_DATA *house_area = get_area(HOUSE_AREA);
	  if ( !house_area )
	  {
	      ch_printf( ch, "House area not found.\n\r" );
	      return;
	  }

          if ( ch->gold < HOUSE_PRICE )
          {
 	      ch_printf( ch, "&WHomes cost %s.\n\r", format_pl(HOUSE_PRICE) );
	      return;
	  }
	  if ( str_cmp( arg3, "yes" ) )
	  {
	      send_to_char( "You must suffix your purchase with the word \'yes\'\n\r", ch );
	      return;
	  }

	  for ( x = house_area->low_r_vnum; x <= house_area->hi_r_vnum; x++ )
	  {
	      ROOM_INDEX_DATA *room = get_room_index( x );
	      if ( !room || !ch || !ch->pcdata )
	      {
		  send_to_char( "Critical error.  Contact an immortal.\n\r", ch );
	 	  return;
	      }
	      if ( !str_cmp( room->name, "An Empty Room" ) )
	      {
	          ch->pcdata->house_vnum = x;
	          ch->pcdata->house_type = HOUSE_FULL;
		  ch->pcdata->house_planet = ch->in_room->vnum;
	          char_from_room( ch );
	          char_to_room( ch, room );
	          STRFREE( room->name );
	          room->name = STRALLOC( "Your home" );
                  send_to_char( "Home purchased!  Use the \'Decorate\' command to edit its appearance, and the \'House\' command to make more solid customisations.\n\r", ch );
	          ch->gold -= HOUSE_PRICE;
		  do_look( ch, "" );
		  do_save( ch, "auto" );
	          fold_area( house_area, house_area->filename, FALSE );
	          return;
	      }
	  }
 	  send_to_char( "No more room to build houses!  Contact an immortal for help.\n\r", ch );
	  return;
	}

	if ( !str_cmp( arg2, "apartment" ) )
	{
	    AREA_DATA *house_area = get_area(HOUSE_AREA);
	    if ( !house_area )
	    {
	        ch_printf( ch, "MASSIVE FUCKING ERROR OH JESUS FUCKING SHIT WHAT HAVE YOU DONE?\n\r" );
 	        return;
 	    }
	    if ( ch->basepl < 500000000 && ch->gold < APARTMENT_PRICE )
	    {   
		 ch_printf( ch, "&WApartments cost %s.\n\r", format_pl(APARTMENT_PRICE) );
		 return;
	    }
	    if ( str_cmp( arg3, "yes" ) )
	    {
		 send_to_char( "You must suffix your purchase with the word 'yes'\n\r", ch );
		 return;
	    }

	    for ( x = house_area->low_r_vnum; x <= house_area->hi_r_vnum; x++ )
	    {
	        ROOM_INDEX_DATA *room = get_room_index( x );
	        if ( !room || !ch || !ch->pcdata )  // weird error checking
	        {
		    send_to_char( "Critical error.  Contact an immortal.\n\r", ch );
	   	    return;
	        }
	        if ( !str_cmp( room->name, "An Empty Room" ) )
	        {
		    ch->pcdata->house_vnum = x;
	 	    ch->pcdata->house_type = HOUSE_APARTMENT;
		    ch->pcdata->house_planet = ch->in_room->vnum;
	            STRFREE( room->name );
	            room->name = STRALLOC( "Your dwelling's first room" );
		    char_from_room( ch );
		    char_to_room( ch, room );
		    send_to_char( "Apartment purchased!  Use the 'Decorate' command to edit its appearance.\n\r", ch );
		    if ( ch->basepl < 500000000 )
		        ch->gold -= APARTMENT_PRICE;
		    do_look( ch, "" );
		    do_save( ch, "auto" );
 	            fold_area( house_area, house_area->filename, FALSE );
		    return;
	 	}
	    }
	    send_to_char( "No more room to build houses!  Contact an immortal for help.\n\r", ch );
	    return;
	}	
        send_to_char( "&CSyntax: &WHouse buy <argument> yes\n\r", ch );
        send_to_char( "&WWhere argument is one of:\n\r", ch );
        ch_printf( ch,"&W  Apartment (&O%s zeni)\n\r", format_pl(APARTMENT_PRICE) );
        send_to_char( "&w  (A single room with limited upgrade possiblities)\n\r", ch );
        ch_printf( ch,"&W  Home      (&O%s zeni)\n\r", format_pl(HOUSE_PRICE) );
        send_to_char( "&w  (A fully customisable and upgradable house)\n\r", ch );
        send_to_char( "&W<argument> must be followed by 'Yes' to confirm transaction.\n\r", ch );
        return;
  }

  if ( !str_cmp( arg1, "upgrade" ) && ch->pcdata->house_type == HOUSE_APARTMENT )
  {
      if ( ch->in_room->vnum != ch->pcdata->house_planet )
      {
	  send_to_char( "&YYou must be near the foreman of your house's residential zone to have it upgraded.\n\r", ch );
	  return;
      }
      if ( ch->gold < HOUSE_UPGRADE_COST )
      {
	  ch_printf( ch, "You must pay %s zeni to upgrade an apartment to a house.\n\r", format_pl( HOUSE_UPGRADE_COST ) );
	  return;
      }

      send_to_char( "You tell the foreman what you want done.\n\r", ch );
      send_to_char( "The foreman nods and takes your payment, before barking some orders at his men.\n\r", ch );
      ch->gold -= HOUSE_UPGRADE_COST;
      ch_printf( ch, "&CA runner approaches and hands the foreman some property deeds.  The foreman scribbles on them hurridly.\n\r" );
      ch_printf( ch, "The foreman says 'Alright, I've upgraded your property rights.  Feel free to start expanding the house.'\n\r" );
      ch->pcdata->house_type = HOUSE_FULL;
      return;
  }

  if ( !str_cmp( arg1, "expand" ) && (ch->pcdata->house_type == HOUSE_FULL || ch->pcdata->house_type == HOUSE_CLANHALL ))
  {
      EXIT_DATA *xit, *rxit;
      AREA_DATA *house_area = get_area( HOUSE_AREA );
      ROOM_INDEX_DATA *new_room = NULL;
      int edir;
      bool found = FALSE;

      if ( arg2==NULL || arg2[0] == '\0' )
      {
          send_to_char( "Create a room in an unused direction.\n\r", ch );
          send_to_char( "Usage: house expand <dir>\n\r", ch );
          return;
      }

      if ( ch->in_room->vnum != ch->pcdata->house_vnum    && ch->in_room->vnum != ch->pcdata->house_room[0] &&
           ch->in_room->vnum != ch->pcdata->house_room[1] && ch->in_room->vnum != ch->pcdata->house_room[2] &&
           ch->in_room->vnum != ch->pcdata->house_room[3] && ch->in_room->vnum != ch->pcdata->house_room[4] )
      {
	  send_to_char( "You must first be in a room of your house.\n\r", ch );
	  return;
      }


      if ( (edir = get_dir( arg2 )) < 0 )
      {
	  send_to_char( "That is not a direction.\n\r", ch );
	  return;
      }

      if ( get_exit(ch->in_room, edir) )
      {
	  send_to_char( "You can't have two exits in one direction.\n\r", ch );
	  return;
      }

      if ( ch->pcdata->house_room[HOUSE_MAX_SIZE-1] != 0 && ch->pcdata->house_type == HOUSE_FULL)
      {
	  send_to_char( "Your house cannot be any larger than it is now.\n\r", ch );
	  return;
      }

      if ( ch->gold < ROOM_COST )
      {
	  send_to_char( "You cannot afford to expand your house.\n\r", ch );
	  return;
      }
    
      for ( x = house_area->low_r_vnum; x <= house_area->hi_r_vnum; x++ )
      {
          new_room = get_room_index( x );
          if ( !str_cmp( new_room->name, "An Empty Room" ) )
          {
	    found = TRUE;
	    break;
	  }
      }
      if ( !found )
      {
	  send_to_char( "No more room to build houses!  Contact an immortal for help.\n\r", ch );      
	  return;
      }
      ch->gold -= ROOM_COST;
      xit = make_exit( ch->in_room, new_room, edir );
      xit->keyword                 = STRALLOC( "" );
      xit->description             = STRALLOC( "" );
      xit->key                     = -1;
      xit->exit_info               = 3;
      rxit = make_exit( new_room, ch->in_room, rev_dir[edir] );
      rxit->keyword                = STRALLOC( "" );
      rxit->description            = STRALLOC( "" );
      rxit->key                    = -1;
      rxit->exit_info              = 3;
      STRFREE( new_room->name );
      new_room->name = STRALLOC( "A Recently Plastered Room" );
      act( AT_IMMORT, "$n knocks a hole in the wall and installs a door to a new room!", ch, NULL, NULL, TO_ROOM );
      send_to_char( "You knock a hole through the wall and install a door to a new room.\n\r", ch );
      save_house_by_vnum(new_room->vnum);
      save_house_by_vnum(ch->in_room->vnum);
      for ( x = 0; x < HOUSE_MAX_SIZE; x++ )
      {
	  if ( ch->pcdata->house_room[x] == 0 )
	  {
	      ch->pcdata->house_room[x] = new_room->vnum;
	      break;
          }
      }
      return;
  }

  if ( !str_cmp( arg1, "guest" ) && ch->pcdata->house_type == HOUSE_FULL )
  {
      char buf[MAX_STRING_LENGTH];
      CHAR_DATA *victim;
  
      if ( !str_cmp( arg2, "list" ) )
      {
          ch_printf( ch, "Current guest-list: %s.\n\r", strlen(ch->pcdata->house_guests) < 3 ? "Nobody" : ch->pcdata->house_guests );
          return;
      }
  
      if ( !str_cmp( arg2, "add" ) )
      {
          if ( arg3 == NULL )// || !str_cmp( arg3, "list" ) )
          {
              ch_printf( ch, "Current guest-list: %s.\n\r", strlen(ch->pcdata->house_guests) < 3 ? "Nobody" : ch->pcdata->house_guests );
              return;
          }

          if (ch->pcdata->house_guests == NULL)
              ch->pcdata->house_guests = str_dup(" ");

  	  if ( ( ( victim = get_plr_world( arg3 ) ) == NULL || IS_NPC(victim) ) && str_cmp( arg3, "all" ) )
	  {
	      ch_printf( ch, "No such player online.\n\r" );
	      return;
  	  }

          sprintf( buf, "%s %s", ch->pcdata->house_guests, arg3 );
          ch->pcdata->house_guests = str_dup( " " );
          ch->pcdata->house_guests = str_dup( buf );
          ch_printf( ch, "You added %s to your guest-list.\n\r", !victim ? "everyone" : victim->name );
          if ( victim )
              ch_printf( victim, "%s has put you on the guest-list of %s house.\n\r", ch->name, HISHER(ch->sex) );
	  return;
      }
  
      if ( !str_cmp( arg2, "remove" ) )
      {
          if ( arg3==NULL || !str_cmp( arg3, "list" ) )
          {
              ch_printf( ch, "Current guest-list: %s.\n\r", !ch->pcdata->house_guests ? "Nobody" : ch->pcdata->house_guests );
              return;
          }

          if (!ch->pcdata->house_guests)
              ch->pcdata->house_guests = str_dup(" ");

          if ( !str_cmp( arg3, "all" ) )
          {
              DISPOSE( ch->pcdata->house_guests );
              ch->pcdata->house_guests = str_dup(" ");
              ch_printf( ch, "House guest-list emptied.\n\r" );
              return;
          }
  
  	  victim = get_plr_world( arg3 );
	  ch->pcdata->house_guests = strrep( ch->pcdata->house_guests, arg3, " " );
	  ch->pcdata->house_guests = strrep( ch->pcdata->house_guests, "  ", " " );
          if ( victim )
              ch_printf( victim, "%s has removed you from the guest-list of %s house.\n\r", ch->name, HISHER(ch->sex) );
	  ch_printf( ch, "%c%s removed from guest-list.\n\r", toupper(arg3[0]), arg3+1 );
	  return;
      }
  }

  send_to_char( "&CSyntax:&W House <argument>\n\r", ch );
  send_to_char( "&WWhere argument is one of:\n\r", ch );
  if ( ch->pcdata->house_vnum == 0 || ch->pcdata->house_type == HOUSE_NONE )
  {
      send_to_char( "&W  buy\n\r", ch );
      return;
  }
  send_to_char( "&W  Enter  Leave\n\r", ch );
  if ( ch->pcdata->house_type == HOUSE_APARTMENT )
  {
      send_to_char( "&W  upgrade\n\r", ch );
      return;
  }
  if ( ch->pcdata->house_type == HOUSE_FULL || ch->pcdata->house_type == HOUSE_CLANHALL )
  {
      ch_printf( ch, "&W  expand <dir>  &O(%s zeni)\n\r", format_pl( ROOM_COST ) );
      send_to_char( "&W  guest add all &z| &W<player name>\n\r", ch );
      send_to_char( "&W  guest remove all &z| &W<player name>\n\r", ch );
      send_to_char( "&W  guest list\n\r", ch );
  }
  return;
}


void do_decorate( CHAR_DATA *ch, char *argument )
{
  char arg[MAX_INPUT_LENGTH];
  ROOM_INDEX_DATA *location = NULL;
  argument = one_argument( argument, arg );

  switch( ch->substate )
  {

  default:
      break;
  case SUB_ROOM_DESC:
      location = ch->dest_buf;
      if ( !location )
      {
          bug( "house: sub_room_desc: NULL ch->dest_buf", 0 );
          location = ch->in_room;
      }
      STRFREE( location->description );
      location->description = copy_buffer( ch );
      stop_editing( ch );
      ch->substate = ch->tempnum;
      strrep( location->description, "~", " " );
      strrep( location->description, "&", " " );
      save_house_by_vnum(location->vnum);
      send_to_char_color( "&C&wHouse room description set.\n\r", ch);
      return;
  }

  if ( IS_NPC(ch) ) 
  {
      send_to_char( "NPCs cannot own houses.\n\r", ch );
      return;
  }

  if ( ch->in_room->vnum != ch->pcdata->house_vnum    && ch->in_room->vnum != ch->pcdata->house_room[0] &&
       ch->in_room->vnum != ch->pcdata->house_room[1] && ch->in_room->vnum != ch->pcdata->house_room[2] &&
       ch->in_room->vnum != ch->pcdata->house_room[3] && ch->in_room->vnum != ch->pcdata->house_room[4] )
  {
       send_to_char( "You must first be in a room of your house.\n\r", ch );
       return;
  }

  if ( !str_cmp( arg, "name"))
  {
      if (!argument[0] || argument[0] == '\0')
      {
          send_to_char_color( "&C&wHouse name <Desired Title of Room>\n\r", ch);
          return;
      }
      if ( strstr( argument, "~" ) || strstr( argument, "&" ) || strstr( argument, "\\" ) )
      {
	  send_to_char_color( "&RColour tokens and ~ symbols cannot be used in room descriptions or names.", ch );
	  return;
      }
      STRFREE( ch->in_room->name );
      ch->in_room->name = STRALLOC( argument );
      send_to_char_color( "&C&wRoom name set.\n\r", ch);
      save_house_by_vnum(ch->in_room->vnum);
      return;
  }

  if ( !str_cmp( arg, "desc" ) )
  {
      ch->tempnum = SUB_NONE;
      ch->substate = SUB_ROOM_DESC;
      ch->dest_buf = ch->in_room;
      start_editing( ch, ch->in_room->description );
      return;
  }

  if ( !str_cmp( arg, "terrain" ) )
  {
        if ( !argument || argument[0] == '\0' )
        {
            send_to_char( "Set the terrain type.\n\r", ch );
            send_to_char( "Available terrain types: indoor, field, underwater, underground, ice, snow.\n\r", ch );
            send_to_char( "Note: Only the indoor sector has inherent lighting.\n\r", ch );
            return;
        }
	if ( ch->gold < 10000 )
	{
	    send_to_char( "This costs 10,000 zeni.\n\r", ch );
	    return;
	}
	if ( !str_cmp(argument, "indoor" ) )
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_INSIDE;
	    SET_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    SET_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    save_house_by_vnum(ch->in_room->vnum);
	    ch->gold -= 10000;
	    return;
	}
	if ( !str_cmp(argument, "field" ) )
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_FIELD;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    ch->gold -= 10000;
	    save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	if ( !str_cmp(argument, "underwater" ))
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_UNDERWATER;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    ch->gold -= 10000;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	if ( !str_cmp(argument, "underground" ))
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_UNDERGROUND;
	    ch->gold -= 10000;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	if ( !str_cmp(argument, "ice" ))
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_ICE;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    ch->gold -= 10000;
	    save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	if ( !str_cmp(argument, "snow" ))
	{
	    send_to_char( "Done.\n\r", ch );
	    ch->in_room->sector_type = SECT_SNOW;
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_LIGHT );
	    REMOVE_BIT(ch->in_room->room_flags, ROOM_INDOORS );
	    ch->gold -= 10000;
	    save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	

        send_to_char( "Set the terrain type.\n\r", ch );
        send_to_char( "Available terrain types: indoor, field, underwater, underground, ice, snow.\n\r", ch );
        send_to_char( "Note: Only the indoor sector has inherent lighting.\n\r", ch );
        return;
  }

  if ( !str_cmp( arg, "install" ) )
  {
	if ( IS_SET(ch->in_room->room_flags, ROOM_HOSPITAL) )
	{
	    send_to_char( "This room is already a healing room.\n\r", ch );
	    return;
	}
	if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL) )
	{
	    send_to_char( "This room is already a duelling room.\n\r", ch );
	    return;
	}

	if ( !str_cmp( argument, "heal" ) )
	{
	    if ( ch->gold < ROOM_ADD_HEAL_COST )
	    {
		ch_printf( ch, "You need %s zeni to purchase this upgrade", format_pl(ROOM_ADD_HEAL_COST) );
		return;
	    }
	    send_to_char( "Type 'decorate install heal now' to complete this transaction.\n\r",ch );
	    return;
	}
	if ( !str_cmp( argument, "duel" ) )
	{
	    if ( ch->gold < ROOM_ADD_DUEL_COST )
	    {
		ch_printf( ch, "You need %s zeni to purchase this upgrade", format_pl(ROOM_ADD_DUEL_COST) );
		return;
	    }
	    send_to_char( "Type 'decorate install duel now' to complete this transaction.\n\r",ch );
	    return;
	}
	if ( !str_cmp( argument, "heal now" ) )
	{
	    if ( ch->gold < ROOM_ADD_HEAL_COST )
	    {
		ch_printf( ch, "You need %s zeni to purchase this upgrade", format_pl(ROOM_ADD_HEAL_COST) );
		return;
	    }
	    SET_BIT(ch->in_room->room_flags, ROOM_HOSPITAL);
	    ch->gold -= ROOM_ADD_HEAL_COST;
	    send_to_char( "A team of builders enter your house and make the necessary adjustments, collecting their fee at the end.\n\r", ch );
            save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	if ( !str_cmp( argument, "duel now" ) )
	{
	    if ( ch->gold < ROOM_ADD_DUEL_COST )
	    {
		ch_printf( ch, "You need %s zeni to purchase this upgrade", format_pl(ROOM_ADD_DUEL_COST) );
		return;
	    }
	    SET_BIT(ch->in_room->room_flags, ROOM_DUEL);
	    ch->gold -= ROOM_ADD_DUEL_COST;
	    send_to_char( "A team of builders enter your house and make the necessary adjustments, collecting their fee at the end.\n\r", ch );
            save_house_by_vnum(ch->in_room->vnum);
	    return;
	}
	send_to_char( "Install what?\n\r", ch );
	return;
  }

  if ( !str_cmp( arg, "desc" ) )
  {
      ch->tempnum = SUB_NONE;
      ch->substate = SUB_ROOM_DESC;
      ch->dest_buf = ch->in_room;
      start_editing( ch, ch->in_room->description );
      return;
  }

  send_to_char( "&CSyntax: &Wdecorate name <room name>\n\r&W        decorate desc\n\r&W	decorate terrain <type> &Y(10,000 zeni)\n\r&W        decorate install <argument>\n\r&CWhere argument is one of:\n\r&WHealing  &Y(100,000 zeni)\n\r&WDuel     &Y(2,000,000 zeni)\n\r", ch );
  return;
}


void save_house_by_vnum(int vnum)
{
//   ROOM_INDEX_DATA *room = get_room_index( vnum );
   AREA_DATA *area = get_area(HOUSE_AREA);

   if ( vnum < area->low_r_vnum || vnum > area->hi_r_vnum )  // Not a house room that's changed; abort function
	return;

   save_house_room(vnum);
   fold_area( area, area->filename, FALSE );
/*
   wipe_resets( room );
   instaroom( NULL, room, TRUE );
   fold_area( area, area->filename, FALSE );
*/
   return;
}


/* 
   Original Code from SW:FotE 1.1 
   Reworked strrep function. 
   Fixed a few glaring errors. It also will not overrun the bounds of a string. 
   -- Xorith 
*/ 
char *strrep( const char *src, const char *sch, const char *rep ) 
{ 
   int lensrc = strlen( src ), lensch = strlen( sch ), lenrep = strlen( rep ), x, y, in_p; 
   static char newsrc[MAX_STRING_LENGTH]; 
   bool searching = FALSE; 
 
   newsrc[0] = '\0'; 
   for( x = 0, in_p = 0; x < lensrc; x++, in_p++ ) 
   { 
      if( src[x] == sch[0] ) 
      { 
         searching = TRUE; 
         for( y = 0; y < lensch; y++ ) 
            if( src[x+y] != sch[y] ) 
               searching = FALSE; 
 
         if( searching ) 
         { 
            for( y = 0; y < lenrep; y++, in_p++ ) 
            { 
               if( in_p == ( MAX_STRING_LENGTH - 1 ) ) 
               { 
                  newsrc[in_p] = '\0'; 
                  return newsrc; 
               } 
               newsrc[in_p] = rep[y]; 
            } 
            x += lensch - 1; 
            in_p--; 
            searching = FALSE; 
            continue; 
         } 
      } 
      if( in_p == ( MAX_STRING_LENGTH - 1 ) ) 
      { 
         newsrc[in_p] = '\0'; 
         return newsrc; 
      } 
      newsrc[in_p] = src[x]; 
   } 
   newsrc[in_p] = '\0'; 
   return newsrc; 
} 


int is_on_guestlist( CHAR_DATA *ch, char *who )
{
    CHAR_DATA *temp;
    char fname[1024];
    char arg[MAX_STRING_LENGTH];
    struct stat fst;
    bool loaded;
    char *argument;
    DESCRIPTOR_DATA *d;

    set_char_color( AT_IMMORT, ch );
    if ( who[0] == '\0' )  // Attempting to enter their own house
        return ch->pcdata->house_vnum;

    for ( temp = first_char; temp; temp = temp->next )
    {
          if (  IS_NPC(temp) ) continue;
          if ( can_see (ch, temp) && !str_cmp( who, temp->name ) )
                  break;
    }
    if ( temp != NULL ) // Entering an online player's house
    {
	if ( temp->pcdata->house_type == HOUSE_NONE || temp->pcdata->house_type == HOUSE_APARTMENT )
	{
	    send_to_char( "They don't have a house.\n\r", ch );
	    return 0;
	}
	if ( temp->pcdata->house_planet != ch->in_room->vnum )
	{
	    send_to_char( "You are not in the correct housing district to enter the house from.\n\r", ch );
	    return 0;
	}

	argument = str_dup(temp->pcdata->house_guests);
	while ( (argument = one_argument( argument, arg )) != NULL )
	{
	    if ( !str_cmp( arg, " " ) || arg[0] == '\0' || strlen(arg) < 3 )
		break;
	    if ( !str_cmp( arg, strlower(ch->name) ) || !str_cmp( arg, "all" ) )
	    {
	        return temp->pcdata->house_vnum;
	    }
	}

        send_to_char( "You try to enter, but the scanner won't let you in!  You're obviously not on the guest list...\n\r", ch );
        return 0;
    }

    who[0] = UPPER(who[0]);
    sprintf( fname, "%s%c/%s", PLAYER_DIR, tolower(who[0]), capitalize( who ) );

    if ( stat( fname, &fst ) != -1 )
    {
        CREATE( d, DESCRIPTOR_DATA, 1 );
        d->next = NULL;
        d->prev = NULL;
        d->connected = CON_GET_NAME;
        d->outsize = 2000;
        CREATE( d->outbuf, char, d->outsize );

        loaded = load_char_obj( d, who, FALSE );
	temp = d->character;
	d->character = NULL;
	d = NULL;
        if ( !loaded )
        {
                pager_printf( ch, "Error checking guest list - contact an immortal for help.\n\r" );
                return 0;
        }

        if ( temp->pcdata->house_type == HOUSE_NONE || temp->pcdata->house_type == HOUSE_APARTMENT )
        {
            send_to_char( "They don't have a house.\n\r", ch );
	    quitting_char = temp;
	    saving_char = NULL;
	    extract_char( temp, TRUE );
            return 0;
        }
        if ( temp->pcdata->house_planet != ch->in_room->vnum )
        {
            send_to_char( "You are not in the correct housing district to enter the house from.\n\r", ch );
	    quitting_char = temp;
	    saving_char = NULL;
	    extract_char( temp, TRUE );
            return 0;
        }

	argument = str_dup(temp->pcdata->house_guests);
	while ( (argument = one_argument( argument, arg )) != NULL )
	{
	    if ( !str_cmp( arg, " " ) || arg[0] == '\0' || strlen(arg) < 3 )
		break;
	    if ( !str_cmp( arg, strlower(ch->name) ) || !str_cmp( arg, "all" ) )
	    {
		int housenum = temp->pcdata->house_vnum;
		temp = NULL;
		return housenum;
	    }
	}

        send_to_char( "You try to enter, but the scanner won't let you in!  You're obviously not on the guest list...\n\r", ch );
        return 0;
    }
    /* else no player file */
    send_to_char( "No such player.\n\r", ch );
    return 0;

}

void do_catalogue( CHAR_DATA *ch, char *argument )
{
    OBJ_INDEX_DATA      *obj;
    OBJ_DATA		*item;
    int                  vnum;
    AREA_DATA           *tarea;
    char arg1[MAX_INPUT_LENGTH];
    int lrange;
    int trange;

    set_pager_color( AT_PLAIN, ch );

    if ( IS_NPC(ch) )
    {
        send_to_char( "NPCs cannot own houses.\n\r", ch );
        return;
    }

    if ( ch->pcdata->house_vnum <= 1 )
    {
       send_to_char( "You do not own a house.\n\r", ch );
       return;
    }

    if ( !( tarea = get_area( "Houses" ) ) )
    {
        send_to_char_color( "&YHousing area missing!  Contact Yami.\n\r", ch );
        return;
    }

    lrange = tarea->low_o_vnum; 
    trange = tarea->hi_o_vnum;
    argument = one_argument( argument, arg1);

    if ( !str_cmp( arg1, "list" ) )
    {
        for ( vnum = lrange; vnum <= trange; vnum++ )
        {
            if ( (obj = get_obj_index( vnum )) == NULL ||( vnum >= 25900 && vnum <= 25906 ))
                continue;
            pager_printf( ch, "&W(&G#%5d&W) (&Y%8s zeni&W) &C%s&W\n\r",vnum,format_pl(obj->cost),obj->short_descr );
	}
	pager_printf( ch, "Note: The first number is the catalogue number.  This is used to purchase items with 'catalogue buy'.\n\r" );
	return;
    }
    if ( !str_cmp( arg1, "buy" ) )
    {
        if ( !is_number( argument ) )
	{
	    send_to_char( "Items are bought by supplying 5 digit a catalogue number.\n\r", ch );
	    return;
	}
	
	vnum = atoi(argument);
	if ( vnum > trange || vnum < lrange )
        {
	    send_to_char( "Invalid catalogue number.\n\r", ch );
	    return;
        }

        if ( (obj = get_obj_index( vnum )) == NULL || (vnum >= 25900 && vnum <= 25906))
	{
	    send_to_char( "Unfortunately, that item appears to be out of stock right now.\n\r", ch );
	    return;
	}

	if ( IS_OBJ_STAT( obj, ITEM_PROTOTYPE ) )
	{
	    send_to_char( "Unfortunately, that item appears to be out of stock right now.\n\r", ch );
	    return;
	}

	if ( ch->gold < obj->cost )
	{
	    send_to_char("Perhaps if you weren't so disgustingly poor, you'd actually be able to afford nice stuff like this.  You worthless bum.\n\r",ch );
	    return;
	}

	ch->gold -= obj->cost;
	item = create_object( obj, 0 );
	obj_to_room( item, get_room_index(ch->pcdata->house_vnum) );
	act( AT_ACTION, "$n whips out $s cellphone and buys some furniture.", ch, NULL, NULL, TO_CANSEE );
	pager_printf(ch,"You have bought %s, and it has been delivered to the first room of your house, from where you may move it.\n\r",item->short_descr);
        save_house_by_vnum(ch->pcdata->house_vnum);
	return;
    }
    send_to_char( "Either type CATALOGUE BUY <NUM> or CATALOGUE LIST.  There are no other choices.\n\r", ch );
    return;
}

void save_house_room( int vnum )
{
    FILE *fp = NULL;
    char  strsave[MAX_INPUT_LENGTH];
    AREA_DATA *house_area = get_area( HOUSE_AREA );
    ROOM_INDEX_DATA *room;

    if ( vnum > house_area->hi_r_vnum || vnum < house_area->low_r_vnum )
    {
        bug( "House vnum outside acceptable range", 0 );
        return;
    }

    sprintf( strsave, "%s%d.house", HOUSE_DIR, vnum );
    room = get_room_index(vnum);
    if ( !room )
    {
        bug ( "No room found in save_house_room", 0 );
        return;
    }

    // Open file
    if ( (fp = fopen(strsave, "w")) != NULL )
    {
        // Write each object in vnum to vnum.house
	if ( room->last_content )
            fwrite_obj( NULL, room->last_content, fp, 0, OS_PLACE );
        fprintf( fp, "#END \n\r" );
        fclose( fp );
    }

    // close
}


void load_house_room( int vnum )
{
    FILE *fp = NULL;
    char  strload[MAX_INPUT_LENGTH];
    AREA_DATA * house_area = get_area( HOUSE_AREA );
    ROOM_INDEX_DATA *room;

    if ( vnum > house_area->hi_r_vnum || vnum < house_area->low_r_vnum )
    {
        bug( "House vnum outside acceptable range", 0 );
        return;
    }

    sprintf( strload, "%s%d.house", HOUSE_DIR, vnum );
    room = get_room_index(vnum);

    if ( !room )
    {
        bug ( "No room found in load_house_room", 0 );
        return;
    }
//    char_from_room (supermob );
  //  char_to_room( supermob, room );
    rset_supermob( room );

    if ( ( fp = fopen( strload, "r" ) ) != NULL )
    {
         for ( ; ; )
         {
            char letter;
            char *word;

            letter = fread_letter( fp );
            if ( letter == '#')
            {
              word = fread_word( fp );

              if (!strcmp(word,"END" ))
                   break;

              if (!strcmp(word,"OBJECT"))
              {
                fread_obj( supermob, fp, OS_PLACE );
              }
           }
         }
         fclose( fp );
    }
    release_supermob();
}
