/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 * 		Commands for personal player settings/statictics	    *
 ****************************************************************************/

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"

/*
 *  Locals
 */
char *tiny_affect_loc_name(int location);
int encumbered(CHAR_DATA *ch);
void deduct_potential(CHAR_DATA *ch);
char *get_rp_power(CHAR_DATA *ch);
int get_modifier(CHAR_DATA *ch);

extern char *const Materials[];

int get_modifier(CHAR_DATA *ch)
{
	int mod = ch->plmod;
	if (ch->enraged)
		mod += (ch->rage * 5);
	if (ch->race == RACE_BIOANDROID && ch->plmod > 99)
		mod += (ch->pkills * 8);
	if (ch->bursttimer <= 0)
		mod = URANGE(0, mod, 50000);

	return mod;
}

char *sick_mind(CHAR_DATA *ch)
{
	if (IS_AFFECTED(ch, AFF_CHAOS) || IS_AFFECTED(ch, AFF_LSSJ) || IS_AFFECTED(ch, AFF_DEMENTED))
		return "&RBroken ";
	if (IS_AFFECTED(ch, AFF_TIRED) || IS_AFFECTED(ch, AFF_TRAUMA) || IS_AFFECTED(ch, AFF_SCARED))
		return "&YAiling ";

	return "&GHealthy";
}

char *sick_body(CHAR_DATA *ch)
{
	return "&GHealthy";
}

char *get_rp_power(CHAR_DATA *ch)
{
	if (ch->rps > 5000.0)
		return "a Dragon";
	if (ch->rps > 4000.0)
		return "Bebi";
	if (ch->rps > 3000.0)
		return "Super Seventeen";
	if (ch->rps > 2500.0)
		return "Uub";
	if (ch->rps > 2300.0)
		return "Vegito";
	if (ch->rps > 2200.0)
		return "Buu";
	if (ch->rps > 2100.0)
		return "SSJ3 Goku";
	if (ch->rps > 2000.0)
		return "Mystic Gohan";
	if (ch->rps > 1900.0)
		return "Supreme Kai";
	if (ch->rps > 1800.0)
		return "Saiyaman";
	if (ch->rps > 1750.0)
		return "SSJ2 Gohan";
	if (ch->rps > 1500.0)
		return "Cell";
	if (ch->rps > 1400.0)
		return "Android Sixteen";
	if (ch->rps > 1300.0)
		return "Android Seventeen";
	if (ch->rps > 1200.0)
		return "Android Eighteen";
	if (ch->rps > 1100.0)
		return "Dr Gero";
	if (ch->rps > 1000.0)
		return "Android 19";
	if (ch->rps > 750.0)
		return "Future Trunks";
	if (ch->rps > 600.0)
		return "King Cold";
	if (ch->rps > 500.0)
		return "Super Saiyan Goku";
	if (ch->rps > 400.0)
		return "Final Form Frieza";
	if (ch->rps > 250.0)
		return "First Form Frieza";
	if (ch->rps > 151.0)
		return "Zarbon";
	if (ch->rps > 100.0)
		return "Nail";
	if (ch->rps > 75.0)
		return "Master Roshi";
	if (ch->rps > 50.0)
		return "Ox King";
	if (ch->rps > 25.0)
		return "Saiyan Scout";
	else
		return "Saibaman";
}

void do_gold(CHAR_DATA *ch, char *argument)
{
	set_char_color(AT_GOLD, ch);
	ch_printf(ch, "You have %s zeni.\n\r", num_punct(ch->gold));
	return;
}

void do_worth(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_STRING_LENGTH];
	char buf2[MAX_STRING_LENGTH];

	set_pager_color(AT_SCORE, ch);
	pager_printf(ch, "\n\rWorth for %s%s.\n\r", ch->name, ch->pcdata->title);
	send_to_pager(" ----------------------------------------------------------------------------\n\r", ch);
	if (!ch->pcdata->deity)
		sprintf(buf, "N/A");
	else if (ch->pcdata->favor > 2250)
		sprintf(buf, "loved");
	else if (ch->pcdata->favor > 2000)
		sprintf(buf, "cherished");
	else if (ch->pcdata->favor > 1750)
		sprintf(buf, "honored");
	else if (ch->pcdata->favor > 1500)
		sprintf(buf, "praised");
	else if (ch->pcdata->favor > 1250)
		sprintf(buf, "favored");
	else if (ch->pcdata->favor > 1000)
		sprintf(buf, "respected");
	else if (ch->pcdata->favor > 750)
		sprintf(buf, "liked");
	else if (ch->pcdata->favor > 250)
		sprintf(buf, "tolerated");
	else if (ch->pcdata->favor > -250)
		sprintf(buf, "ignored");
	else if (ch->pcdata->favor > -750)
		sprintf(buf, "shunned");
	else if (ch->pcdata->favor > -1000)
		sprintf(buf, "disliked");
	else if (ch->pcdata->favor > -1250)
		sprintf(buf, "dishonored");
	else if (ch->pcdata->favor > -1500)
		sprintf(buf, "disowned");
	else if (ch->pcdata->favor > -1750)
		sprintf(buf, "abandoned");
	else if (ch->pcdata->favor > -2000)
		sprintf(buf, "despised");
	else if (ch->pcdata->favor > -2250)
		sprintf(buf, "hated");
	else
		sprintf(buf, "damned");

	if (ch->level < 10)
	{
		if (ch->alignment > 900)
			sprintf(buf2, "Heart of gold");
		else if (ch->alignment > 700)
			sprintf(buf2, "noble");
		else if (ch->alignment > 350)
			sprintf(buf2, "honorable");
		else if (ch->alignment > 100)
			sprintf(buf2, "good-natured");
		else if (ch->alignment > -100)
			sprintf(buf2, "neutral");
		else if (ch->alignment > -350)
			sprintf(buf2, "base");
		else if (ch->alignment > -700)
			sprintf(buf2, "evil");
		else if (ch->alignment > -900)
			sprintf(buf2, "scum");
		else
			sprintf(buf2, "manicial");
	}
	else
		sprintf(buf2, "%d", ch->alignment);
	pager_printf(ch, "|Level: %-4d |Favor: %-10s |Alignment: %-9s |\n\r",
				 ch->level, buf, buf2);
	send_to_pager(" ----------------------------------------------------------------------------\n\r", ch);
	/*        switch (ch->style) 
	{
        case STYLE_MUTANT:
                sprintf(buf, "mutant");
                break;
        case STYLE_TURTLE:
                sprintf(buf, "turtle");
                break;
        case STYLE_ANCIENT:
                sprintf(buf, "ancient");
                break;
        case STYLE_SAIYAN:
                sprintf(buf, "saiyan");
                break;
        default:
                sprintf(buf, "standard");
                break;
        }*/
	pager_printf(ch, "|Glory: %-4d |Weight: %-9d |Style: %-13s |Gold: %-14s |\n\r",
				 ch->pcdata->quest_curr, ch->carry_weight, buf, num_punct(ch->gold));
	send_to_pager(" ----------------------------------------------------------------------------\n\r", ch);
	return;
}

/*
 * Yami 
 */
void do_score(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_STRING_LENGTH];
	int ctr = 0;
	float critrate = 0.0f;

	if (IS_NPC(ch))
		return;

	pager_printf(ch, "&Y&W---------------------- &YScore sheet&Y&W -------------------------");
	pager_printf(ch, "\n\r");
	if (ch->sex == 0)
		sprintf(buf, "Neutral");
	if (ch->sex == 1)
		sprintf(buf, "Male");
	if (ch->sex == 2)
		sprintf(buf, "Female");
	pager_printf(ch, "&cName:&Y&W  %-13s  &cSex:&Y&W  %7s       &c  Race:&Y&W  %-10s", ch->name, buf, capitalize(get_race(ch)));
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&cAge:&Y&W   %-3d            &cPlayed:&Y&W  %4d hours &c  Aura:  %-9s&c ", ch->pcdata->age, ((ch->played / 60) / 60),
				 ch->energycolour);
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&cTemp:&Y&G  Normal&W (37c)   &cMind:&Y&G    %-7s     &c Body:&Y&G  %-7s", sick_mind(ch), sick_body(ch));
	pager_printf(ch, "\n\r");
	if (str_cmp(sick_mind(ch), "&GHealthy"))
	{
		pager_printf(ch, "&cAilments: ");
		if (IS_AFFECTED(ch, AFF_TIRED))
			pager_printf(ch, "&W| &YFatigue &W|");
		if (IS_AFFECTED(ch, AFF_TRAUMA))
			pager_printf(ch, "&W| &YPTSD &W|");
		if (IS_AFFECTED(ch, AFF_SCARED))
			pager_printf(ch, "&W| &YFear &W|");
		if (IS_AFFECTED(ch, AFF_CHAOS))
			pager_printf(ch, "&W| &RHysteria &W|");
		if (IS_AFFECTED(ch, AFF_DEMENTED))
			pager_printf(ch, "&W| &RDementia &W|");
		if (IS_AFFECTED(ch, AFF_LSSJ))
			pager_printf(ch, "&W| &RSociopathy &W|");
		pager_printf(ch, "\n\r");
	}
	pager_printf(ch, "&Y&W------------------------- &YAbility&Y&W --------------------------\n\r");
	pager_printf(ch, "&cRacial ATK:&Y&W %3i%%   &c   DEF: &Y&W%3d%%             &cDmg Mod: &Y&W-%i", race_table[ch->race]->atkperc, race_table[ch->race]->defperc, (GET_AC(ch)) / 1000);
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&cStr: &Y&w%4d&c (&Y&W%4d&c)      Dex: &Y&w%4d&c (&Y&W%4d&c)      Con: &Y&w%4d&c (&Y&W%4d&c)\n\r", get_curr_str(ch), URANGE(0, ch->perm_str, 9999), get_curr_dex(ch), URANGE(0, ch->perm_dex, 9999), get_curr_con(ch), URANGE(0, ch->perm_con, 9999));
	pager_printf(ch, "&cInt: &Y&w%4d&c (&Y&W%4d&c)      Wis: &Y&w%4d&c (&Y&W%4d&c)      Cha: &Y&w%4d&c (&Y&W%4d&c)\n\r", get_curr_int(ch), URANGE(0, ch->perm_int, 9999), get_curr_wis(ch), URANGE(0, ch->perm_wis, 9999), get_curr_cha(ch), URANGE(0, ch->perm_cha, 9999));
	critrate = (((float)get_curr_lck(ch) / 5) / 99.87) + 4.98;
	if (ch->perk[PERK_SLAYER])
		critrate += 33;
	if (ch->perk[PERK_MORE_CRITICALS])
		critrate += 10;
	pager_printf(ch, "&cArmour: &Y&W%s%11s    &cCrit rate:&Y&W %3.2f\n\r", num_punct(GET_AC(ch)), encumbered(ch) > 0 ? "&R" : "&W", critrate);
	pager_printf(ch, "&cHP:&Y&W %7s&c/", num_punct(ch->hit));
	pager_printf(ch, "&Y&w%7s   &cKi:&Y&W  ", num_punct(ch->max_hit));
	pager_printf(ch, "%s&c/", num_punct(ch->mana));
	pager_printf(ch, "&Y&w%s", num_punct(ch->max_mana));
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&cBase PL: &Y&w%s&c", format_pl(ch->basepl));
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&cCurr PL: &Y&W%s &C[&rx&R%d.%d&C]", format_pl(ch->currpl), get_modifier(ch) / 100, get_modifier(ch) % 100);
	pager_printf(ch, "\n\r");
	pager_printf(ch, "&Y&W-------------------------- &YWorth&Y&W ---------------------------\n\r");

	if (!IS_NPC(ch))
	{
		if (ch->pcdata->legality > 1000)
			sprintf(buf, "&GYou ARE the law!");
		else if (ch->pcdata->legality > 500)
			sprintf(buf, "&GUniversal Peacekeeper");
		else if (ch->pcdata->legality > 400)
			sprintf(buf, "&GGalactic Crusader");
		else if (ch->pcdata->legality > 300)
			sprintf(buf, "&GPlanetary Activist");
		else if (ch->pcdata->legality > 200)
			sprintf(buf, "&GNational Hero");
		else if (ch->pcdata->legality > 100)
			sprintf(buf, "&GVillage Sheriff");
		else if (ch->pcdata->legality > -100)
			sprintf(buf, "&Y&WLibertarian");
		else if (ch->pcdata->legality > -200)
			sprintf(buf, "&RVillage Swindler");
		else if (ch->pcdata->legality > -300)
			sprintf(buf, "&RNational Crook");
		else if (ch->pcdata->legality > -400)
			sprintf(buf, "&RPlanetary Criminal");
		else if (ch->pcdata->legality > -500)
			sprintf(buf, "&RGalactic Pirate");
		else if (ch->pcdata->legality > -1000)
			sprintf(buf, "&RUniversal Scourge");
		else
			sprintf(buf, "&RAbsolute power corrupts absolutely");

		pager_printf(ch, "&cLegality: &Y&W%5d       &c(%s&c)\n\r", ch->pcdata->legality, buf);
	}
	if (ch->alignment > 900)
		sprintf(buf, "&GPaladin");
	else if (ch->alignment > 700)
		sprintf(buf, "&GGuardian");
	else if (ch->alignment > 350)
		sprintf(buf, "&GShield");
	else if (ch->alignment > 100)
		sprintf(buf, "&GDefender");
	else if (ch->alignment > -100)
		sprintf(buf, "&Y&WWarrior");
	else if (ch->alignment > -350)
		sprintf(buf, "&RBetrayer");
	else if (ch->alignment > -700)
		sprintf(buf, "&RMonster");
	else if (ch->alignment > -900)
		sprintf(buf, "&RBlackguard");
	else
		sprintf(buf, "&RDemonspawn");
	pager_printf(ch, "&cMorality: &Y&W%5d       &c(%s&c)\n\r", ch->alignment, buf);
	pager_printf(ch, "&W&cWimpy:  &Y&W%7d       &W&cKili:&Y&W    %5d        &cZeni:&Y&W  %-12s\n\r", ch->wimpy, KILI(ch), num_punct(ch->gold));
	pager_printf(ch, "&cPKills:   &W%5d       &cMKills:  &W%5d        &cBounty: &W%-5d\n\r", ch->pkills, ch->pcdata->mkills, ch->pcdata->bountykills);
	pager_printf(ch, "&cPDeaths:  &W%5d       &cMDeaths: &W%5d        ", ch->pdeaths, ch->pcdata->mdeaths);
	if (ch->pcdata->learnsn != 0)
		pager_printf(ch, "&cLearning: &W%s", skill_table[ch->pcdata->learnsn]->name);
	pager_printf(ch, "\n\r");
	/*
pager_printf( ch, "&Y&W----------------------- &YTradeskills&Y&W ------------------------\n\r" );
pager_printf( ch, "Cooking:    lv %2.2d: %3d%%)           Fishing:    lv %2.2d: %3d%%)\n\r",1,0,1,0 );
pager_printf( ch, "Salvaging:  lv %2.2d: %3d%%)           Crafting:   lv %2.2d: %3d%%)\n\r",1,0,1,0 );
pager_printf( ch, "Scavenging: lv %2.2d: %3d%%)           Decorating: lv %2.2d: %3d%%)\n\r",1,0,1,0 );
*/

	if (ch->race == RACE_BIOANDROID)
		pager_printf(ch, "&RKill streak: %d seconds.\n\r", get_timer(ch, TIMER_KILLSTREAK));

	pager_printf(ch, "&Y&W-------------------------- &YPerks&Y&W ---------------------------\n\r");

	for (;;)
	{
		if (ctr >= PERK_MAX)
			break;
		if (ch->perk[ctr])
			pager_printf(ch, "&Y&W%-20s&w:&w %s\n\r", PERK[ctr], perk_defines[ctr]);
		ctr++;
	}
	pager_printf(ch, "\n\r");

	if (IS_IMMORTAL(ch))
	{
		pager_printf(ch, "&Y&W--------------------------- &YImm&Y&W ----------------------------\n\r");
		pager_printf(ch, "&cBamfin:&Y&W  %s %s\n\r", ch->name, (ch->pcdata->bamfin[0] != '\0') ? ch->pcdata->bamfin : "appears from NOWHERE!");
		pager_printf(ch, "&cBamfout: &Y&W%s %s\n\r", ch->name, (ch->pcdata->bamfout[0] != '\0') ? ch->pcdata->bamfout : "disappears into NOWHERE!");
		if (ch->pcdata->area)
		{
			pager_printf(ch, "&cVnums:&W   Room (%-5.5d - %-5.5d)   Object (%-5.5d - %-5.5d)   Mob (%-5.5d - %-5.5d)\n\r",
						 ch->pcdata->area->low_r_vnum, ch->pcdata->area->hi_r_vnum,
						 ch->pcdata->area->low_o_vnum, ch->pcdata->area->hi_o_vnum,
						 ch->pcdata->area->low_m_vnum, ch->pcdata->area->hi_m_vnum);
			pager_printf(ch, "&cWizinvis [&Y&W%s&c]  Wizlevel (&Y&W%d&c)   &cArea Loaded [&W%s&c]\n\r",
						 xIS_SET(ch->act, PLR_WIZINVIS) ? "X" : " ", ch->pcdata->wizinvis, (IS_SET(ch->pcdata->area->status, AREA_LOADED)) ? "yes" : "no");
			//        pager_printf( ch, "&cArea Loaded [&W%s&c]\n\r", (IS_SET (ch->pcdata->area->status, AREA_LOADED)) ? "yes" : "no");
		}
	}
	return;
}

void do_newscore(CHAR_DATA *ch, char *argument)
{
	/*    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    AFFECT_DATA    *paf;

    if (IS_NPC(ch))
    {
	do_oldscore(ch, argument);
	return;
    }
    set_pager_color(AT_SCORE, ch);

    pager_printf_color(ch, "\n\r&C%s%s.\n\r", ch->name, ch->pcdata->title);
    if ( get_trust( ch ) != ch->level )
	pager_printf( ch, "You are trusted at level %d.\n\r", get_trust( ch ) );
    
    send_to_pager_color("&W----------------------------------------------------------------------------\n\r", ch);

    pager_printf_color(ch, "Level: &W%-3d         &CRace : &W%-10.10s        &CPlayed: &W%d &Chours\n\r",
	ch->level, capitalize(get_race(ch)), (get_age(ch) - 17) * 2);
    pager_printf_color(ch, "&CYears: &W%-6d      &CClass: &W%-11.11s       &CLog In: %s\r",
		get_age(ch), capitalize(get_class(ch)), ctime(&(ch->logon)) );
	if ( GET_AC(ch) > 101 )
	sprintf(buf, "the rags of a beggar");
    else if (GET_AC(ch) >= 80)
	sprintf(buf, "improper for adventure");
    else if (GET_AC(ch) >= 55)
	sprintf(buf, "shabby and threadbare");
    else if (GET_AC(ch) >= 40)
	sprintf(buf, "of poor quality");
    else if (GET_AC(ch) >= 20)
	sprintf(buf, "scant protection");
    else if (GET_AC(ch) >= 10)
	sprintf(buf, "that of a knave");
    else if (GET_AC(ch) >= 0)
	sprintf(buf, "moderately crafted");
    else if (GET_AC(ch) >= -10)
	sprintf(buf, "well crafted");
    else if (GET_AC(ch) >= -20)
	sprintf(buf, "the envy of squires");
    else if (GET_AC(ch) >= -40)
	sprintf(buf, "excellently crafted");
    else if (GET_AC(ch) >= -60)
	sprintf(buf, "the envy of knights");
    else if (GET_AC(ch) >= -80)
	sprintf(buf, "the envy of barons");
    else if (GET_AC(ch) >= -100)
	sprintf(buf, "the envy of dukes");
    else if (GET_AC(ch) >= -200)
	sprintf(buf, "the envy of emperors");
    else
	sprintf(buf, "that of an avatar");
    if (ch->level > 24)
	pager_printf_color(ch, "&CWIS  : &W%2.2d&C(&w%2.2d&C)      Armor: &W%-d; %s\n\r",
		get_curr_wis(ch), ch->perm_wis, GET_AC(ch), buf);
    else
	pager_printf_color(ch, "&CWIS  : &W%2.2d&C(&w%2.2d&C)      Armor: &W%s \n\r",
		get_curr_wis(ch), ch->perm_wis, buf);

    if (ch->alignment > 900)
	sprintf(buf, "heart of gold");
    else if (ch->alignment > 700)
	sprintf(buf, "noble");
    else if (ch->alignment > 350)
	sprintf(buf, "honourable");
    else if (ch->alignment > 100)
	sprintf(buf, "good-natured");
    else if (ch->alignment > -100)
	sprintf(buf, "neutral");
    else if (ch->alignment > -350)
	sprintf(buf, "base");
    else if (ch->alignment > -700)
	sprintf(buf, "evil");
    else if (ch->alignment > -900)
	sprintf(buf, "scum");
    else
	sprintf(buf, "manicial");
    if (ch->level < 10)
	pager_printf_color(ch, "&CDEX  : &W%2.2d&C(&w%2.2d&C)      Align: &W%-20.20s    &CItems:  &W%d (max %d)\n\r",
		get_curr_dex(ch), ch->perm_dex, buf, ch->carry_number, can_carry_n(ch));
    else
	pager_printf_color(ch, "&CDEX  : &W%2.2d&C(&w%2.2d&C)      Align: &W%4d; %-14.14s   &CItems:  &W%d &w(max %d)\n\r",
		get_curr_dex(ch), ch->perm_dex, ch->alignment, buf, ch->carry_number, can_carry_n(ch));

    switch (ch->position)
    {
	case POS_DEAD:
		sprintf(buf, "slowly decomposing");
		break;
	case POS_MORTAL:
		sprintf(buf, "mortally wounded");
		break;
	case POS_INCAP:
		sprintf(buf, "incapacitated");
		break;
	case POS_STUNNED:
		sprintf(buf, "stunned");
		break;
	case POS_SLEEPING:
		sprintf(buf, "sleeping");
		break;
	case POS_RESTING:
		sprintf(buf, "resting");
		break;
	case POS_STANDING:
		sprintf(buf, "standing");
		break;
	case POS_FIGHTING:
		sprintf(buf, "fighting");
		break;
        case POS_MUTANT:
                sprintf(buf, "fighting (mutant)");//    Fighting style support -haus 
                break;
        case POS_TURTLE:
                sprintf(buf, "fighting (turtle)");
                break;
        case POS_ANCIENT:
                sprintf(buf, "fighting (ancient)");
                break;
        case POS_SAIYAN:
                sprintf(buf, "fighting (saiyan)");
                break;
	case POS_MOUNTED:
		sprintf(buf, "mounted");
		break;
        case POS_SITTING:
		sprintf(buf, "sitting");
		break;
    }
    pager_printf_color(ch, "&CCON  : &W%2.2d&C(&w%2.2d&C)      Pos\'n: &W%-21.21s  &CWeight: &W%d &w(max %d)\n\r",
	get_curr_con(ch), ch->perm_con, buf, ch->carry_weight, can_carry_w(ch));


     *
     * Fighting style support -haus
     *
    pager_printf_color(ch, "&CCHA  : &W%2.2d&C(&w%2.2d&C)      Wimpy: &Y%-5d      ",
	get_curr_cha(ch), ch->perm_cha, ch->wimpy);
 
        switch (ch->style) {
        case STYLE_MUTANT:
                sprintf(buf, "mutant");
                break;
        case STYLE_TURTLE:
                sprintf(buf, "turtle");
                break;
        case STYLE_ANCIENT:
                sprintf(buf, "ancient");
                break;
        case STYLE_SAIYAN:
                sprintf(buf, "saiyan");
                break;
        default:
                sprintf(buf, "standard");
                break;
        }
    pager_printf_color(ch, "\n\r&CLCK  : &W%2.2d&C(&w%2.2d&C)      Style: &W%-10.10s\n\r",
	get_curr_lck(ch), ch->perm_lck, buf );

    pager_printf_color(ch, "&CGlory: &W%d&C/&w%d\n\r",
	ch->pcdata->quest_curr, ch->pcdata->quest_accum );

    pager_printf_color(ch, "&CPRACT: &W%3d         &CHitpoints: &G%-5d &Cof &g%5d   &CPager: (&W%c&C) &W%3d    &CAutoExit(&W%c&C)\n\r",
	ch->practice, ch->hit, ch->max_hit,
	IS_SET(ch->pcdata->flags, PCFLAG_PAGERON) ? 'X' : ' ',
	ch->pcdata->pagerlen, xIS_SET(ch->act, PLR_AUTOEXIT) ? 'X' : ' ');

   pager_printf_color(ch, "&CMana: &B%-5d &Cof &b%5d   &CMKills:  &W%5d\n\r",
		ch->mana, ch->max_mana, ch->pcdata->mkills);

    pager_printf_color(ch, "&CGOLD : &Y%-13s    &CMove: &W%-5d &Cof &w%5d   &CMdeaths: &W%5d    &CAutoSac (&W%c&C)\n\r",
	num_punct(ch->gold), ch->move, ch->max_move, ch->pcdata->mdeaths, xIS_SET(ch->act, PLR_AUTOSAC) ? 'X' : ' ');

    if (!IS_NPC(ch) && ch->pcdata->condition[COND_DRUNK] > 10)
	send_to_pager("You are drunk.\n\r", ch);
    if (!IS_NPC(ch) && ch->pcdata->condition[COND_THIRST] == 0)
	send_to_pager("You are in danger of dehydrating.\n\r", ch);
    if (!IS_NPC(ch) && ch->pcdata->condition[COND_FULL] == 0)
	send_to_pager("You are starving to death.\n\r", ch);
    if ( ch->position != POS_SLEEPING )
	switch( ch->mental_state / 10 )
	{
	    default:   send_to_pager( "You're completely messed up!\n\r", ch );	break;
	    case -10:  send_to_pager( "You're barely conscious.\n\r", ch );	break;
	    case  -9:  send_to_pager( "You can barely keep your eyes open.\n\r", ch );	break;
	    case  -8:  send_to_pager( "You're extremely drowsy.\n\r", ch );	break;
	    case  -7:  send_to_pager( "You feel very unmotivated.\n\r", ch );	break;
	    case  -6:  send_to_pager( "You feel sedated.\n\r", ch );		break;
	    case  -5:  send_to_pager( "You feel sleepy.\n\r", ch );		break;
	    case  -4:  send_to_pager( "You feel tired.\n\r", ch );		break;
	    case  -3:  send_to_pager( "You could use a rest.\n\r", ch );		break;
	    case  -2:  send_to_pager( "You feel a little under the weather.\n\r", ch );	break;
	    case  -1:  send_to_pager( "You feel fine.\n\r", ch );		break;
	    case   0:  send_to_pager( "You feel great.\n\r", ch );		break;
	    case   1:  send_to_pager( "You feel energetic.\n\r", ch );	break;
	    case   2:  send_to_pager( "Your mind is racing.\n\r", ch );	break;
	    case   3:  send_to_pager( "You can't think straight.\n\r", ch );	break;
	    case   4:  send_to_pager( "Your mind is going 100 miles an hour.\n\r", ch );	break;
	    case   5:  send_to_pager( "You're high as a kite.\n\r", ch );	break;
	    case   6:  send_to_pager( "Your mind and body are slipping apart.\n\r", ch );	break;
	    case   7:  send_to_pager( "Reality is slipping away.\n\r", ch );	break;
	    case   8:  send_to_pager( "You have no idea what is real, and what is not.\n\r", ch );	break;
	    case   9:  send_to_pager( "You feel immortal.\n\r", ch );	break;
	    case  10:  send_to_pager( "You are a Supreme Entity.\n\r", ch );	break;
	}
    else
    if ( ch->mental_state >45 )
	send_to_pager( "Your sleep is filled with strange and vivid dreams.\n\r", ch );
    else
    if ( ch->mental_state >25 )
	send_to_pager( "Your sleep is uneasy.\n\r", ch );
    else
    if ( ch->mental_state <-35 )
	send_to_pager( "You are deep in a much needed sleep.\n\r", ch );
    else
    if ( ch->mental_state <-25 )
	send_to_pager( "You are in deep slumber.\n\r", ch );
  send_to_pager("Languages: ", ch );
    for ( iLang = 0; lang_array[iLang] != LANG_UNKNOWN; iLang++ )
	if ( knows_language( ch, lang_array[iLang], ch )
	||  (IS_NPC(ch) && ch->speaks == 0) )
	{
	    if ( lang_array[iLang] & ch->speaking
	    ||  (IS_NPC(ch) && !ch->speaking) )
		set_pager_color( AT_RED, ch );
	    send_to_pager( lang_names[iLang], ch );
	    send_to_pager( " ", ch );
	    set_pager_color( AT_SCORE, ch );
	}
    send_to_pager( "\n\r", ch );

    if ( ch->pcdata->bestowments && ch->pcdata->bestowments[0] != '\0' )
	pager_printf_color(ch, "&CYou are bestowed with the command(s): &Y%s\n\r", 
		ch->pcdata->bestowments );

    if ( ch->morph && ch->morph->morph )
    {
      send_to_pager_color("&W----------------------------------------------------------------------------&C\n\r", ch);
      if ( IS_IMMORTAL( ch ) )
         pager_printf (ch, "Morphed as (%d) %s with a timer of %d.\n\r",
                ch->morph->morph->vnum, ch->morph->morph->short_desc, 
		ch->morph->timer
                );
      else
        pager_printf (ch, "You are morphed into a %s.\n\r",
                ch->morph->morph->short_desc );
      send_to_pager_color("&W----------------------------------------------------------------------------&C\n\r", ch);
    } 
    if ( CAN_PKILL( ch ) )
    {
	send_to_pager_color("&W----------------------------------------------------------------------------&C\n\r", ch);
	pager_printf_color(ch, "&CPKILL DATA:  Pkills (&W%d&C)     Illegal Pkills (&W%d&C)     Pdeaths (&W%d&C)\n\r",
		ch->pcdata->pkills, ch->pcdata->illegal_pk, ch->pcdata->pdeaths );
    }
    if (ch->pcdata->clan && ch->pcdata->clan->clan_type != CLAN_ORDER  && ch->pcdata->clan->clan_type != CLAN_GUILD )
    {

	send_to_pager_color( "&W----------------------------------------------------------------------------&C\n\r", ch);

	pager_printf_color(ch, "&CCLAN STATS:  &W%-14.14s  &CClan AvPkills : &W%-5d  &CClan NonAvpkills : &W%-5d\n\r",
		ch->pcdata->clan->name, ch->pcdata->clan->pkills[5],
		(ch->pcdata->clan->pkills[0]+ch->pcdata->clan->pkills[1]+
		 ch->pcdata->clan->pkills[2]+ch->pcdata->clan->pkills[3]+
		 ch->pcdata->clan->pkills[4]) );
        pager_printf_color(ch, "&C                             Clan AvPdeaths: &W%-5d  &CClan NonAvpdeaths: &W%-5d\n\r",
		ch->pcdata->clan->pdeaths[5],
		( ch->pcdata->clan->pdeaths[0] + ch->pcdata->clan->pdeaths[1] +
		  ch->pcdata->clan->pdeaths[2] + ch->pcdata->clan->pdeaths[3] +
		  ch->pcdata->clan->pdeaths[4] ) );
    }
    if (ch->pcdata->deity)
    {
	send_to_pager_color( "&W----------------------------------------------------------------------------&C\n\r", ch);
	if (ch->pcdata->favor > 2250)
	  sprintf( buf, "loved" );
	else if (ch->pcdata->favor > 2000)
	  sprintf( buf, "cherished" );
	else if (ch->pcdata->favor > 1750)
	  sprintf( buf, "honored" );
	else if (ch->pcdata->favor > 1500)
	  sprintf( buf, "praised" );
	else if (ch->pcdata->favor > 1250)
	  sprintf( buf, "favored" );
	else if (ch->pcdata->favor > 1000)
	  sprintf( buf, "respected" );
	else if (ch->pcdata->favor > 750)
	  sprintf( buf, "liked" );
	else if (ch->pcdata->favor > 250)
	  sprintf( buf, "tolerated" );
	else if (ch->pcdata->favor > -250)
	  sprintf( buf, "ignored" );
	else if (ch->pcdata->favor > -750)
	  sprintf( buf, "shunned" );
	else if (ch->pcdata->favor > -1000)
	  sprintf( buf, "disliked" );
	else if (ch->pcdata->favor > -1250)
	  sprintf( buf, "dishonored" );
	else if (ch->pcdata->favor > -1500)
	  sprintf( buf, "disowned" );
	else if (ch->pcdata->favor > -1750)
	  sprintf( buf, "abandoned" );
	else if (ch->pcdata->favor > -2000)
	  sprintf( buf, "despised" );
	else if (ch->pcdata->favor > -2250)
	  sprintf( buf, "hated" );
	else
	  sprintf( buf, "damned" );
	pager_printf_color(ch, "&CDeity:  &W%-20s &CFavor:  &W%s&C\n\r", ch->pcdata->deity->name, buf );
    }
    if (ch->pcdata->clan && ch->pcdata->clan->clan_type == CLAN_ORDER )
    {
        send_to_pager_color( "&W----------------------------------------------------------------------------&C\n\r", ch);
	pager_printf_color(ch, "&COrder:  &W%-20s  &COrder Mkills:  &W%-6d   &COrder MDeaths:  &W%-6d\n\r",
		ch->pcdata->clan->name, ch->pcdata->clan->mkills, ch->pcdata->clan->mdeaths);
    }
    if (ch->pcdata->clan && ch->pcdata->clan->clan_type == CLAN_GUILD )
    {
        send_to_pager_color( "&W----------------------------------------------------------------------------&C\n\r", ch);
        pager_printf_color(ch, "&CGuild:  &W%-20s  &CGuild Mkills:  &W%-6d   &CGuild MDeaths:  &W%-6d\n\r",
                ch->pcdata->clan->name, ch->pcdata->clan->mkills, ch->pcdata->clan->mdeaths);
    }
    argument = one_argument( argument, arg );
    if (ch->first_affect && !str_cmp( arg, "affects" ) )
    {
	int i;
	SKILLTYPE *sktmp;

	i = 0;
	send_to_pager_color( "&W----------------------------------------------------------------------------&C\n\r", ch);
	send_to_pager_color("AFFECT DATA:                            ", ch);
	for (paf = ch->first_affect; paf; paf = paf->next)
	{
	    if ( (sktmp=get_skilltype(paf->type)) == NULL )
		continue;
	    if (ch->level < 20)
	    {
		pager_printf_color(ch, "&C[&W%-34.34s&C]    ", sktmp->name);
		if (i == 0)
		   i = 2;
		if ((++i % 3) == 0)
		   send_to_pager("\n\r", ch);
	     }
	     if (ch->level >= 20)
	     {
		if (paf->modifier == 0)
		    pager_printf_color(ch, "&C[&W%-24.24s;%5d &Crds]    ",
			sktmp->name,
			paf->duration);
		else
		if (paf->modifier > 999)
		    pager_printf_color(ch, "&C[&W%-15.15s; %7.7s;%5d &Crds]    ",
			sktmp->name,
			tiny_affect_loc_name(paf->location),
			paf->duration);
		else
		    pager_printf_color(ch, "&C[&W%-11.11s;%+-3.3d %7.7s;%5d &Crds]    ",
			sktmp->name,
			paf->modifier,
			tiny_affect_loc_name(paf->location),
			paf->duration);
		if (i == 0)
		    i = 1;
		if ((++i % 2) == 0)
		    send_to_pager("\n\r", ch);
	    }
	}
    }
    send_to_pager("\n\r", ch);*/
	return;
}

/*
 * Return ascii name of an affect location.
 */
char *
tiny_affect_loc_name(int location)
{
	switch (location)
	{
	case APPLY_NONE:
		return "NIL";
	case APPLY_STR:
		return " STR  ";
	case APPLY_DEX:
		return " DEX  ";
	case APPLY_INT:
		return " INT  ";
	case APPLY_WIS:
		return " WIS  ";
	case APPLY_CON:
		return " CON  ";
	case APPLY_CHA:
		return " CHA  ";
	case APPLY_LCK:
		return " LCK  ";
	case APPLY_SEX:
		return " SEX  ";
	case APPLY_CLASS:
		return " CLASS";
	case APPLY_LEVEL:
		return " LVL  ";
	case APPLY_AGE:
		return " AGE  ";
	case APPLY_MANA:
		return " MANA ";
	case APPLY_HIT:
		return " HV   ";
	case APPLY_MOVE:
		return " MOVE ";
	case APPLY_GOLD:
		return " GOLD ";
	case APPLY_EXP:
		return " EXP  ";
	case APPLY_AC:
		return " AC   ";
	case APPLY_HITROLL:
		return " HITRL";
	case APPLY_DAMROLL:
		return " DAMRL";
	case APPLY_SAVING_POISON:
		return "SV POI";
	case APPLY_SAVING_ROD:
		return "SV ROD";
	case APPLY_SAVING_PARA:
		return "SV PARA";
	case APPLY_SAVING_BREATH:
		return "SV BRTH";
	case APPLY_SAVING_SPELL:
		return "SV SPLL";
	case APPLY_HEIGHT:
		return "HEIGHT";
	case APPLY_WEIGHT:
		return "WEIGHT";
	case APPLY_AFFECT:
		return "AFF BY";
	case APPLY_RESISTANT:
		return "RESIST";
	case APPLY_IMMUNE:
		return "IMMUNE";
	case APPLY_SUSCEPTIBLE:
		return "SUSCEPT";
	case APPLY_WEAPONSPELL:
		return " WEAPON";
	case APPLY_BACKSTAB:
		return "BACKSTB";
	case APPLY_PICK:
		return " PICK  ";
	case APPLY_TRACK:
		return " TRACK ";
	case APPLY_STEAL:
		return " STEAL ";
	case APPLY_SNEAK:
		return " SNEAK ";
	case APPLY_HIDE:
		return " HIDE  ";
	case APPLY_PALM:
		return " PALM  ";
	case APPLY_DETRAP:
		return " DETRAP";
	case APPLY_DODGE:
		return " DODGE ";
	case APPLY_PEEK:
		return " PEEK  ";
	case APPLY_SCAN:
		return " SCAN  ";
	case APPLY_GOUGE:
		return " GOUGE ";
	case APPLY_SEARCH:
		return " SEARCH";
	case APPLY_MOUNT:
		return " MOUNT ";
	case APPLY_DISARM:
		return " DISARM";
	case APPLY_KICK:
		return " KICK  ";
	case APPLY_PARRY:
		return " PARRY ";
	case APPLY_BASH:
		return " BASH  ";
	case APPLY_STUN:
		return " STUN  ";
	case APPLY_PUNCH:
		return " PUNCH ";
	case APPLY_CLIMB:
		return " CLIMB ";
	case APPLY_GRIP:
		return " GRIP  ";
	case APPLY_SCRIBE:
		return " SCRIBE";
	case APPLY_BREW:
		return " BREW  ";
	case APPLY_WEARSPELL:
		return " WEAR  ";
	case APPLY_REMOVESPELL:
		return " REMOVE";
	case APPLY_EMOTION:
		return "EMOTION";
	case APPLY_MENTALSTATE:
		return " MENTAL";
	case APPLY_STRIPSN:
		return " DISPEL";
	case APPLY_REMOVE:
		return " REMOVE";
	case APPLY_DIG:
		return " DIG   ";
	case APPLY_FULL:
		return " HUNGER";
	case APPLY_THIRST:
		return " THIRST";
	case APPLY_DRUNK:
		return " DRUNK ";
	case APPLY_BLOOD:
		return " BLOOD ";
	case APPLY_COOK:
		return " COOK  ";
	case APPLY_RECURRINGSPELL:
		return " RECURR";
	case APPLY_CONTAGIOUS:
		return "CONTGUS";
	case APPLY_ODOR:
		return " ODOR  ";
	case APPLY_ROOMFLAG:
		return " RMFLG ";
	case APPLY_SECTORTYPE:
		return " SECTOR";
	case APPLY_ROOMLIGHT:
		return " LIGHT ";
	case APPLY_TELEVNUM:
		return " TELEVN";
	case APPLY_TELEDELAY:
		return " TELEDY";
	};

	bug("Affect_location_name: unknown location %d.", location);
	return "(/?/?/?)";
}

char *
get_class(CHAR_DATA *ch)
{
	if (IS_NPC(ch) && ch->class < MAX_NPC_CLASS && ch->class >= 0)
		return (npc_class[ch->class]);
	else if (!IS_NPC(ch) && ch->class < MAX_PC_CLASS && ch->class >= 0)
		return class_table[ch->class]->who_name;
	return ("Unknown");
}

char *
get_race(CHAR_DATA *ch)
{
	if (ch->race < MAX_PC_RACE && ch->race >= 0)
		return (race_table[ch->race]->race_name);
	if (ch->race < MAX_NPC_RACE && ch->race >= 0)
		return (npc_race[ch->race]);
	return ("Unknown");
}

void do_oldscore(CHAR_DATA *ch, char *argument)
{
	AFFECT_DATA *paf;
	SKILLTYPE *skill;

	set_pager_color(AT_SCORE, ch);
	pager_printf(ch,
				 "You are %s%s, level %d, %d years old (%d hours).\n\r",
				 ch->name,
				 IS_NPC(ch) ? "" : ch->pcdata->title,
				 ch->level,
				 get_age(ch),
				 (get_age(ch) - 17) * 2);

	if (get_trust(ch) != ch->level)
		pager_printf(ch, "You are trusted at level %d.\n\r",
					 get_trust(ch));

	if (IS_NPC(ch) && xIS_SET(ch->act, ACT_MOBINVIS))
		pager_printf(ch, "You are mobinvis at level %d.\n\r",
					 ch->mobinvis);

	pager_printf(ch,
				 "You have %d/%d hit, %d/%d mana, %d/%d movement, %d practices.\n\r",
				 ch->hit, ch->max_hit,
				 ch->mana, ch->max_mana,
				 ch->move, ch->max_move,
				 ch->practice);

	pager_printf(ch,
				 "You are carrying %d/%d items with weight %d/%d kg.\n\r",
				 ch->carry_number, can_carry_n(ch),
				 ch->carry_weight, can_carry_w(ch));

	pager_printf(ch,
				 "Str: %d  Int: %d  Wis: %d  Dex: %d  Con: %d  Cha: %d  Lck: %d.\n\r",
				 get_curr_str(ch),
				 get_curr_int(ch),
				 get_curr_wis(ch),
				 get_curr_dex(ch),
				 get_curr_con(ch),
				 get_curr_cha(ch),
				 get_curr_lck(ch));

	pager_printf(ch, "You have %s gold coins.\n\r", num_punct(ch->gold));

	if (!IS_NPC(ch))
		pager_printf(ch,
					 "You have achieved %d glory during your life, and currently have %d.\n\r",
					 ch->pcdata->quest_accum, ch->pcdata->quest_curr);

	pager_printf(ch,
				 "Autoexit: %s   Autosac: %s   Autogold: %s\n\r",
				 (!IS_NPC(ch) && xIS_SET(ch->act, PLR_AUTOEXIT)) ? "yes" : "no",
				 (!IS_NPC(ch) && xIS_SET(ch->act, PLR_AUTOSAC)) ? "yes" : "no",
				 (!IS_NPC(ch) && xIS_SET(ch->act, PLR_AUTOGOLD)) ? "yes" : "no");

	pager_printf(ch, "Wimpy set to %d hit points.\n\r", ch->wimpy);

	if (!IS_NPC(ch))
	{
		if (ch->pcdata->condition[COND_DRUNK] > 10)
			send_to_pager("You are drunk.\n\r", ch);
		if (ch->pcdata->condition[COND_THIRST] == 0)
			send_to_pager("You are thirsty.\n\r", ch);
		if (ch->pcdata->condition[COND_FULL] == 0)
			send_to_pager("You are hungry.\n\r", ch);
	}

	switch (ch->mental_state / 10)
	{
	default:
		send_to_pager("You're completely messed up!\n\r", ch);
		break;
	case -10:
		send_to_pager("You're barely conscious.\n\r", ch);
		break;
	case -9:
		send_to_pager("You can barely keep your eyes open.\n\r", ch);
		break;
	case -8:
		send_to_pager("You're extremely drowsy.\n\r", ch);
		break;
	case -7:
		send_to_pager("You feel very unmotivated.\n\r", ch);
		break;
	case -6:
		send_to_pager("You feel sedated.\n\r", ch);
		break;
	case -5:
		send_to_pager("You feel sleepy.\n\r", ch);
		break;
	case -4:
		send_to_pager("You feel tired.\n\r", ch);
		break;
	case -3:
		send_to_pager("You could use a rest.\n\r", ch);
		break;
	case -2:
		send_to_pager("You feel a little under the weather.\n\r", ch);
		break;
	case -1:
		send_to_pager("You feel fine.\n\r", ch);
		break;
	case 0:
		send_to_pager("You feel great.\n\r", ch);
		break;
	case 1:
		send_to_pager("You feel energetic.\n\r", ch);
		break;
	case 2:
		send_to_pager("Your mind is racing.\n\r", ch);
		break;
	case 3:
		send_to_pager("You can't think straight.\n\r", ch);
		break;
	case 4:
		send_to_pager("Your mind is going 100 miles an hour.\n\r", ch);
		break;
	case 5:
		send_to_pager("You're high as a kite.\n\r", ch);
		break;
	case 6:
		send_to_pager("Your mind and body are slipping appart.\n\r", ch);
		break;
	case 7:
		send_to_pager("Reality is slipping away.\n\r", ch);
		break;
	case 8:
		send_to_pager("You have no idea what is real, and what is not.\n\r", ch);
		break;
	case 9:
		send_to_pager("You feel immortal.\n\r", ch);
		break;
	case 10:
		send_to_pager("You are a Supreme Entity.\n\r", ch);
		break;
	}

	switch (ch->position)
	{
	case POS_DEAD:
		send_to_pager("You are DEAD!!\n\r", ch);
		break;
	case POS_MORTAL:
		send_to_pager("You are mortally wounded.\n\r", ch);
		break;
	case POS_INCAP:
		send_to_pager("You are incapacitated.\n\r", ch);
		break;
	case POS_STUNNED:
		send_to_pager("You are stunned.\n\r", ch);
		break;
	case POS_SLEEPING:
		send_to_pager("You are sleeping.\n\r", ch);
		break;
	case POS_RESTING:
		send_to_pager("You are resting.\n\r", ch);
		break;
	case POS_STANDING:
		send_to_pager("You are standing.\n\r", ch);
		break;
	case POS_FIGHTING:
		send_to_pager("You are fighting.\n\r", ch);
		break;
	case POS_MOUNTED:
		send_to_pager("Mounted.\n\r", ch);
		break;
	case POS_SHOVE:
		send_to_pager("Being shoved.\n\r", ch);
		break;
	case POS_DRAG:
		send_to_pager("Being dragged.\n\r", ch);
		break;
	}

	if (ch->level >= 25)
		pager_printf(ch, "AC: %d.  ", GET_AC(ch));

	send_to_pager("You are ", ch);
	if (GET_AC(ch) >= 101)
		send_to_pager("WORSE than naked!\n\r", ch);
	else if (GET_AC(ch) >= 80)
		send_to_pager("naked.\n\r", ch);
	else if (GET_AC(ch) >= 60)
		send_to_pager("wearing clothes.\n\r", ch);
	else if (GET_AC(ch) >= 40)
		send_to_pager("slightly armored.\n\r", ch);
	else if (GET_AC(ch) >= 20)
		send_to_pager("somewhat armored.\n\r", ch);
	else if (GET_AC(ch) >= 0)
		send_to_pager("armored.\n\r", ch);
	else if (GET_AC(ch) >= -20)
		send_to_pager("well armored.\n\r", ch);
	else if (GET_AC(ch) >= -40)
		send_to_pager("strongly armored.\n\r", ch);
	else if (GET_AC(ch) >= -60)
		send_to_pager("heavily armored.\n\r", ch);
	else if (GET_AC(ch) >= -80)
		send_to_pager("superbly armored.\n\r", ch);
	else if (GET_AC(ch) >= -100)
		send_to_pager("divinely armored.\n\r",
					  ch);
	else
		send_to_pager("invincible!\n\r", ch);

	if (ch->level >= 10)
		pager_printf(ch, "Alignment: %d.  ", ch->alignment);

	send_to_pager("You are ", ch);
	if (ch->alignment > 900)
		send_to_pager("angelic.\n\r", ch);
	else if (ch->alignment > 700)
		send_to_pager("saintly.\n\r", ch);
	else if (ch->alignment > 350)
		send_to_pager("good.\n\r", ch);
	else if (ch->alignment > 100)
		send_to_pager("kind.\n\r", ch);
	else if (ch->alignment > -100)
		send_to_pager("neutral.\n\r", ch);
	else if (ch->alignment > -350)
		send_to_pager("mean.\n\r", ch);
	else if (ch->alignment > -700)
		send_to_pager("evil.\n\r", ch);
	else if (ch->alignment > -900)
		send_to_pager("demonic.\n\r", ch);
	else
		send_to_pager("satanic.\n\r", ch);

	if (ch->first_affect)
	{
		send_to_pager("You are affected by:\n\r", ch);
		for (paf = ch->first_affect; paf; paf = paf->next)
			if ((skill = get_skilltype(paf->type)) != NULL)
			{
				pager_printf(ch, "Spell: '%s'", skill->name);

				if (ch->level >= 20)
					pager_printf(ch,
								 " modifies %s by %d for %d rounds",
								 affect_loc_name(paf->location),
								 paf->modifier,
								 paf->duration);

				send_to_pager(".\n\r", ch);
			}
	}

	if (!IS_NPC(ch) && IS_IMMORTAL(ch))
	{
		pager_printf(ch, "\n\rWizInvis level: %d   WizInvis is %s\n\r",
					 ch->pcdata->wizinvis,
					 xIS_SET(ch->act, PLR_WIZINVIS) ? "ON" : "OFF");
		if (ch->pcdata->r_range_lo && ch->pcdata->r_range_hi)
			pager_printf(ch, "Room Range: %d - %d\n\r", ch->pcdata->r_range_lo,
						 ch->pcdata->r_range_hi);
		if (ch->pcdata->o_range_lo && ch->pcdata->o_range_hi)
			pager_printf(ch, "Obj Range : %d - %d\n\r", ch->pcdata->o_range_lo,
						 ch->pcdata->o_range_hi);
		if (ch->pcdata->m_range_lo && ch->pcdata->m_range_hi)
			pager_printf(ch, "Mob Range : %d - %d\n\r", ch->pcdata->m_range_lo,
						 ch->pcdata->m_range_hi);
	}

	return;
}

/*								-Thoric
 * Display your current exp, level, and surrounding level exp requirements
 */
void do_level(CHAR_DATA *ch, char *argument)
{
}

/* 1997, Blodkai */
void do_remains(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_STRING_LENGTH];
	OBJ_DATA *obj;
	bool found = FALSE;

	if (IS_NPC(ch))
		return;
	set_char_color(AT_MAGIC, ch);
	if (!ch->pcdata->deity)
	{
		send_to_pager("You have no deity from which to seek such assistance...\n\r", ch);
		return;
	}
	if (ch->pcdata->favor < ch->level * 2)
	{
		send_to_pager("Your favor is insufficient for such assistance...\n\r", ch);
		return;
	}
	pager_printf(ch, "%s appears in a vision, revealing that your remains... ", ch->pcdata->deity->name);
	sprintf(buf, "the corpse of %s", ch->name);
	for (obj = first_object; obj; obj = obj->next)
	{
		if (obj->in_room && !str_cmp(buf, obj->short_descr) && (obj->pIndexData->vnum == 11))
		{
			found = TRUE;
			pager_printf(ch, "\n\r  - at %s will endure for %d ticks",
						 obj->in_room->name,
						 obj->timer);
		}
	}
	if (!found)
		send_to_pager(" no longer exist.\n\r", ch);
	else
	{
		send_to_pager("\n\r", ch);
		ch->pcdata->favor -= ch->level * 2;
	}
	return;
}

/* Affects-at-a-glance, Blodkai */
void do_affected(CHAR_DATA *ch, char *argument)
{
	char arg[MAX_INPUT_LENGTH];
	AFFECT_DATA *paf;
	SKILLTYPE *skill;

	if (IS_NPC(ch))
		return;

	set_char_color(AT_SCORE, ch);

	argument = one_argument(argument, arg);
	if (!str_cmp(arg, "by"))
	{
		send_to_char_color("\n\r&BImbued with:\n\r", ch);
		ch_printf_color(ch, "&C%s\n\r",
						!xIS_EMPTY(ch->affected_by) ? affect_bit_name(&ch->affected_by) : "nothing");
		/*        if ( ch->level >= 20 )
        {
            send_to_char( "\n\r", ch );
            if ( ch->resistant > 0 )
	    {
                send_to_char_color( "&BResistances:  ", ch );
                ch_printf_color( ch, "&C%s\n\r", flag_string(ch->resistant, ris_flags) );
	    }
            if ( ch->immune > 0 )
	    {
                send_to_char_color( "&BImmunities:   ", ch);
                ch_printf_color( ch, "&C%s\n\r",  flag_string(ch->immune, ris_flags) );
	    }
            if ( ch->susceptible > 0 )
	    {
                send_to_char_color( "&BSuscepts:     ", ch );
                ch_printf_color( ch, "&C%s\n\r", flag_string(ch->susceptible, ris_flags) );
	    }
        }*/
		return;
	}

	if (!ch->first_affect)
	{
		send_to_char_color("\n\r&CNo cantrip or skill affects you.\n\r", ch);
	}
	else
	{
		send_to_char("\n\r", ch);
		for (paf = ch->first_affect; paf; paf = paf->next)
			if ((skill = get_skilltype(paf->type)) != NULL)
			{
				set_char_color(AT_BLUE, ch);
				send_to_char("Affected:  ", ch);
				set_char_color(AT_SCORE, ch);
				if (ch->level >= 20 || IS_PKILL(ch))
				{
					if (paf->duration < 25)
						set_char_color(AT_WHITE, ch);
					if (paf->duration < 6)
						set_char_color(AT_WHITE + AT_BLINK, ch);
					ch_printf(ch, "(%5d)   ", paf->duration);
				}
				ch_printf(ch, "%-18s\n\r", skill->name);
			}
	}
	return;
}

void do_inventory(CHAR_DATA *ch, char *argument)
{
	set_char_color(AT_RED, ch);
	send_to_char("You are carrying:\n\r", ch);
	show_list_to_char(ch->first_carrying, ch, TRUE, TRUE);
	set_char_color(AT_RED, ch);
	send_to_char("You can feel your cellphone safely in your pocket.\n\r", ch);
	if (!IS_NPC(ch) && (ch->pcdata->house_type == HOUSE_APARTMENT || ch->pcdata->house_type == HOUSE_FULL))
		send_to_char("You can also feel your housekey, in the same pocket as your phone.\n\r", ch);
	return;
}

void do_equipment(CHAR_DATA *ch, char *argument)
{
	OBJ_DATA *obj;
	int iWear;
	bool found;

	set_char_color(AT_RED, ch);
	send_to_char("&Y&W---------------------&REquipment&Y&W---------------------\n\r", ch);
	found = FALSE;
	set_char_color(AT_OBJECT, ch);
	for (iWear = 0; iWear < MAX_WEAR; iWear++)
	{
		for (obj = ch->first_carrying; obj; obj = obj->next_content)
			if (obj->wear_loc == iWear)
			{
				/*                if( (!IS_NPC(ch)) && (ch->race>0) && (ch->race<MAX_PC_RACE))
                    send_to_char(race_table[ch->race]->where_name[iWear], ch);
                else*/
				send_to_char(where_name[iWear], ch);

				if (can_see_obj(ch, obj))
				{
					send_to_char(format_obj_to_char(obj, ch, TRUE), ch);
					send_to_char("\n\r", ch);
				}
				else
					send_to_char("&Y&wsomething.\n\r", ch);
				found = TRUE;
			}
	}

	if (!found)
		send_to_char("&Y&wNothing.\n\r", ch);

	return;
}

void set_title(CHAR_DATA *ch, char *title)
{
	char buf[MAX_STRING_LENGTH];

	if (IS_NPC(ch))
	{
		bug("Set_title: NPC.", 0);
		return;
	}

	if (isalpha(title[0]) || isdigit(title[0]))
	{
		buf[0] = ' ';
		strcpy(buf + 1, title);
	}
	else
		strcpy(buf, title);

	STRFREE(ch->pcdata->title);
	ch->pcdata->title = STRALLOC(buf);
	return;
}

void do_title(CHAR_DATA *ch, char *argument)
{
	if (IS_NPC(ch))
		return;

	set_char_color(AT_SCORE, ch);
	if (IS_SET(ch->pcdata->flags, PCFLAG_NOTITLE))
	{
		set_char_color(AT_IMMORT, ch);
		send_to_char("The Gods prohibit you from changing your title.\n\r", ch);
		return;
	}

	if (argument[0] == '\0')
	{
		send_to_char("Change your title to what?\n\r", ch);
		return;
	}

	if (strlen(argument) > 50)
		argument[50] = '\0';

	smash_tilde(argument);
	set_title(ch, argument);
	send_to_char("Ok.\n\r", ch);
}

void do_homepage(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_STRING_LENGTH];

	if (IS_NPC(ch))
		return;

	if (argument[0] == '\0')
	{
		if (!ch->pcdata->homepage)
			ch->pcdata->homepage = str_dup("");
		ch_printf(ch, "Your homepage is: %s\n\r",
				  show_tilde(ch->pcdata->homepage));
		return;
	}

	if (!str_cmp(argument, "clear"))
	{
		if (ch->pcdata->homepage)
			DISPOSE(ch->pcdata->homepage);
		ch->pcdata->homepage = str_dup("");
		send_to_char("Homepage cleared.\n\r", ch);
		return;
	}

	if (strstr(argument, "://"))
		strcpy(buf, argument);
	else
		sprintf(buf, "http://%s", argument);
	if (strlen(buf) > 70)
		buf[70] = '\0';

	hide_tilde(buf);
	if (ch->pcdata->homepage)
		DISPOSE(ch->pcdata->homepage);
	ch->pcdata->homepage = str_dup(buf);
	send_to_char("Homepage set.\n\r", ch);
}

/*
 * Set your personal description				-Thoric
 */
void do_description(CHAR_DATA *ch, char *argument)
{
	if (IS_NPC(ch))
	{
		send_to_char("Monsters are too dumb to do that!\n\r", ch);
		return;
	}

	if (!ch->desc)
	{
		bug("do_description: no descriptor", 0);
		return;
	}

	switch (ch->substate)
	{
	default:
		bug("do_description: illegal substate", 0);
		return;

	case SUB_RESTRICTED:
		send_to_char("You cannot use this command from within another command.\n\r", ch);
		return;

	case SUB_NONE:
		ch->substate = SUB_PERSONAL_DESC;
		ch->dest_buf = ch;
		start_editing(ch, ch->description);
		return;

	case SUB_PERSONAL_DESC:
		STRFREE(ch->description);
		ch->description = copy_buffer(ch);
		stop_editing(ch);
		return;
	}
}

/* Ripped off do_description for whois bio's -- Scryn*/
void do_bio(CHAR_DATA *ch, char *argument)
{
	if (IS_NPC(ch))
	{
		send_to_char("Mobs cannot set a bio.\n\r", ch);
		return;
	}
	if (!ch->desc)
	{
		bug("do_bio: no descriptor", 0);
		return;
	}

	switch (ch->substate)
	{
	default:
		bug("do_bio: illegal substate", 0);
		return;

	case SUB_RESTRICTED:
		send_to_char("You cannot use this command from within another command.\n\r", ch);
		return;

	case SUB_NONE:
		ch->substate = SUB_PERSONAL_BIO;
		ch->dest_buf = ch;
		start_editing(ch, ch->pcdata->bio);
		return;

	case SUB_PERSONAL_BIO:
		STRFREE(ch->pcdata->bio);
		ch->pcdata->bio = copy_buffer(ch);
		stop_editing(ch);
		return;
	}
}

/*
 * New stat and statreport command coded by Morphina
 * Bug fixes by Shaddai
 */

void do_statreport(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_INPUT_LENGTH];

	if (IS_NPC(ch))
	{
		send_to_char("Huh?\n\r", ch);
		return;
	}

	ch_printf(ch, "You report: %d/%d hp %d/%d ki.\n\r",
			  ch->hit, ch->max_hit, ch->mana, ch->max_mana);
	sprintf(buf, "$n reports: %d/%d hp %d/%d ki.",
			ch->hit, ch->max_hit, ch->mana, ch->max_mana);
	act(AT_REPORT, buf, ch, NULL, NULL, TO_ROOM);

	ch_printf(ch, "Your base stats:    %-4d str %-4d wis %-4d int %-4d dex %-4d con %-4d cha.\n\r",
			  URANGE(3, ch->perm_str, 9999), URANGE(3, ch->perm_wis, 9999), URANGE(3, ch->perm_int, 9999), URANGE(3, ch->perm_dex, 9999),
			  URANGE(3, ch->perm_con, 9999), URANGE(3, ch->perm_cha, 9999));
	sprintf(buf, "$n's base stats:    %-4d str %-4d wis %-4d int %-4d dex %-4d con %-4d cha.",
			URANGE(3, ch->perm_str, 9999), URANGE(3, ch->perm_wis, 9999), URANGE(3, ch->perm_int, 9999), URANGE(3, ch->perm_dex, 9999),
			URANGE(3, ch->perm_con, 9999), URANGE(3, ch->perm_cha, 9999));
	act(AT_REPORT, buf, ch, NULL, NULL, TO_ROOM);

	ch_printf(ch, "Your current stats: %-4d str %-4d wis %-4d int %-4d dex %-4d con %-4d cha.\n\r",
			  get_curr_str(ch), get_curr_wis(ch), get_curr_int(ch),
			  get_curr_dex(ch), get_curr_con(ch), get_curr_cha(ch));
	sprintf(buf, "$n's current stats: %-4d str %-4d wis %-4d int %-4d dex %-4d con %-4d cha.",
			get_curr_str(ch), get_curr_wis(ch), get_curr_int(ch),
			get_curr_dex(ch), get_curr_con(ch), get_curr_cha(ch));
	act(AT_REPORT, buf, ch, NULL, NULL, TO_ROOM);

	ch_printf(ch, "You have: %d; Your maximum: %d", ch->carry_number, can_carry_n(ch));

	return;
}

void do_delet(CHAR_DATA *ch, char *argument)
{
	send_to_char("If you want to DELETE, spell it out.\n\r", ch);
	return;
}

/* Delete command for players to remove themselves  - Gareth */
/* Bugfix by Energon to support caps in passwords */
void do_delete(CHAR_DATA *ch, char *argument)
{
	char arg1[MAX_INPUT_LENGTH];
	char arg2[MAX_INPUT_LENGTH];
	char *pArg;
	char cEnd;
	char buf[MAX_STRING_LENGTH];
	//  char buf2[MAX_STRING_LENGTH];
	int x, y;
	bool wasimm = FALSE;

	//Abuse fix by Zeno
	if (auction != NULL && auction->item != NULL && ((ch == auction->buyer) || (ch == auction->seller)))
	{
		send_to_char("Wait until you have bought/sold the item on auction.\n\r", ch);
		return;
	}

	pArg = arg1;
	while (isspace(*argument))
		argument++;

	cEnd = ' ';
	if (*argument == '\'' || *argument == '"')
		cEnd = *argument++;

	while (*argument != '\0')
	{
		if (*argument == cEnd)
		{
			argument++;
			break;
		}
		*pArg++ = *argument++;
	}
	*pArg = '\0';

	pArg = arg2;
	while (isspace(*argument))
		argument++;

	cEnd = ' ';
	if (*argument == '\'' || *argument == '"')
		cEnd = *argument++;

	while (*argument != '\0')
	{
		if (*argument == cEnd)
		{
			argument++;
			break;
		}
		*pArg++ = *argument++;
	}
	*pArg = '\0';

	if (arg1[0] == '\0' || arg2[0] == '\0')
	{
		send_to_char("Syntax: delete <password> <yourname>\n\r", ch);
		return;
	}

	if (IS_NPC(ch))
	{
		send_to_char("Huh?\n\r", ch);
		return;
	}

	if (strcasecmp(ch->name, arg2))
	{
		send_to_char("That's not your name.\n\r", ch);
		return;
	}

	if (strcmp(crypt(arg1, ch->pcdata->pwd), ch->pcdata->pwd))
	{
		WAIT_STATE(ch, 40);
		send_to_char("Wrong password.  Wait 10 seconds.\n\r", ch);
		return;
	}

	if (IS_IMMORTAL(ch))
		wasimm = TRUE;

	/*    if ( ch->pcdata->house_type != HOUSE_NONE && ch->pcdata->house_vnum > 0 )
    {
        if ( ch->pcdata->house_type == HOUSE_APARTMENT || ch->pcdata->house_type == HOUSE_FULL )
        {
            ROOM_INDEX_DATA *room = get_room_index( ch->pcdata->house_vnum );
            STRFREE( room->name );
            STRFREE( room->description );
            room->name = STRALLOC( "An Empty Room" );
            REMOVE_BIT( room->room_flags, ROOM_GTRAIN );
            REMOVE_BIT( room->room_flags, ROOM_HOSPITAL );
            SET_BIT( room->room_flags, ROOM_NO_ASTRAL );
        }
        if ( ch->pcdata->house_type == HOUSE_FULL )
        {
            int ctr = 0;
            for ( ctr = 0; ctr < HOUSE_MAX_SIZE; ctr++ )
            {
                if ( ch->pcdata->house_room[ctr] != 0 )
                {
                    ROOM_INDEX_DATA *room = get_room_index( ch->pcdata->house_room[ctr] );
                    STRFREE( room->name );
                    STRFREE( room->description );
                    room->name = STRALLOC( "An Empty Room" );
                    REMOVE_BIT( room->room_flags, ROOM_GTRAIN );
                    REMOVE_BIT( room->room_flags, ROOM_HOSPITAL );
                    SET_BIT( room->room_flags, ROOM_NO_ASTRAL );
                }
            }
        }
            save_house_by_vnum( ch->pcdata->house_vnum );
    }
*/

	quitting_char = ch;
	save_char_obj(ch);
	saving_char = NULL;
	extract_char(ch, TRUE);
	for (x = 0; x < MAX_WEAR; x++)
		for (y = 0; y < MAX_LAYERS; y++)
			save_equipment[x][y] = NULL;

	if (wasimm)
	{
		sprintf(buf, "%s%s", GOD_DIR, capitalize(arg2));
		if (remove(buf))
		{
			sprintf(buf, "%s's wizfile failed to delete.", capitalize(arg2));
			log_string(buf);
		}
		make_wizlist();
	}
	sprintf(buf, "%s%c/%s", PLAYER_DIR, tolower(arg2[0]),
			capitalize(arg2));
	//  sprintf( buf2, "%s%c/%s", BACKUP_DIR, tolower(arg2[0]),
	//          capitalize( arg2 ) );
	remove(buf);
	sprintf(buf, "%s deleted self, pfile not saved.", capitalize(arg2));
	log_string(buf);
}

void do_stat(CHAR_DATA *ch, char *argument)
{
	if (IS_NPC(ch))
	{
		send_to_char("Huh?\n\r", ch);
		return;
	}

	ch_printf(ch, "You report: %d/%d hp %d/%d mana %d/%d mv.\n\r",
			  ch->hit, ch->max_hit, ch->mana, ch->max_mana,
			  ch->move, ch->max_move);

	ch_printf(ch, "Your base stats:    %-2d str %-2d wis %-2d int %-2d dex %-2d con %-2d cha %-2d lck.\n\r",
			  ch->perm_str, ch->perm_wis, ch->perm_int, ch->perm_dex,
			  ch->perm_con, ch->perm_cha, ch->perm_lck);

	ch_printf(ch, "Your current stats: %-2d str %-2d wis %-2d int %-2d dex %-2d con %-2d cha %-2d lck.\n\r",
			  get_curr_str(ch), get_curr_wis(ch), get_curr_int(ch),
			  get_curr_dex(ch), get_curr_con(ch), get_curr_cha(ch),
			  get_curr_lck(ch));
	return;
}

void do_report(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_INPUT_LENGTH];

	if (IS_NPC(ch) && ch->fighting)
		return;

	if (IS_AFFECTED(ch, AFF_POSSESS))
	{
		send_to_char("You can't do that in your current state of mind!\n\r", ch);
		return;
	}

	ch_printf(ch,
			  "You report: Life: %d/%d. Ki: %d/%d. Powerlevel: %s.\n\r",
			  ch->hit, ch->max_hit,
			  ch->mana, ch->max_mana,
			  format_pl(ch->currpl));

	sprintf(buf, "$n reports: Life: %d/%d. Ki: %d/%d. Powerlevel: %s.",
			ch->hit, ch->max_hit,
			ch->mana, ch->max_mana,
			format_pl(ch->currpl));

	act(AT_REPORT, buf, ch, NULL, NULL, TO_ROOM);

	return;
}

void do_fprompt(CHAR_DATA *ch, char *argument)
{
	char arg[MAX_INPUT_LENGTH];

	set_char_color(AT_GREY, ch);

	if (IS_NPC(ch))
	{
		send_to_char("NPC's can't change their prompt..\n\r", ch);
		return;
	}
	smash_tilde(argument);
	one_argument(argument, arg);
	if (!*arg || !str_cmp(arg, "display"))
	{
		send_to_char("Your current fighting prompt string:\n\r", ch);
		set_char_color(AT_WHITE, ch);
		ch_printf(ch, "%s\n\r", !str_cmp(ch->pcdata->fprompt, "") ? "(default prompt)" : ch->pcdata->fprompt);
		set_char_color(AT_GREY, ch);
		send_to_char("Type 'help prompt' for information on changing your prompt.\n\r", ch);
		return;
	}
	send_to_char("Replacing old prompt of:\n\r", ch);
	set_char_color(AT_WHITE, ch);
	ch_printf(ch, "%s\n\r", !str_cmp(ch->pcdata->fprompt, "") ? "(default prompt)" : ch->pcdata->fprompt);
	if (ch->pcdata->fprompt)
		STRFREE(ch->pcdata->fprompt);
	if (strlen(argument) > 128)
		argument[128] = '\0';

	/* Can add a list of pre-set prompts here if wanted.. perhaps
     'prompt 1' brings up a different, pre-set prompt */
	if (!str_cmp(arg, "default"))
		ch->pcdata->fprompt = STRALLOC("");
	else
		ch->pcdata->fprompt = STRALLOC(argument);
	return;
}

void do_prompt(CHAR_DATA *ch, char *argument)
{
	char arg[MAX_INPUT_LENGTH];

	set_char_color(AT_GREY, ch);

	if (IS_NPC(ch))
	{
		send_to_char("NPC's can't change their prompt..\n\r", ch);
		return;
	}
	smash_tilde(argument);
	one_argument(argument, arg);
	if (!*arg || !str_cmp(arg, "display"))
	{
		send_to_char("Your current prompt string:\n\r", ch);
		set_char_color(AT_WHITE, ch);
		ch_printf(ch, "%s\n\r", !str_cmp(ch->pcdata->prompt, "") ? "(default prompt)" : ch->pcdata->prompt);
		set_char_color(AT_GREY, ch);
		send_to_char("Type 'help prompt' for information on changing your prompt.\n\r", ch);
		return;
	}
	send_to_char("Replacing old prompt of:\n\r", ch);
	set_char_color(AT_WHITE, ch);
	ch_printf(ch, "%s\n\r", !str_cmp(ch->pcdata->prompt, "") ? "(default prompt)" : ch->pcdata->prompt);
	if (ch->pcdata->prompt)
		STRFREE(ch->pcdata->prompt);
	if (strlen(argument) > 128)
		argument[128] = '\0';

	/* Can add a list of pre-set prompts here if wanted.. perhaps
     'prompt 1' brings up a different, pre-set prompt */
	if (!str_cmp(arg, "default"))
		ch->pcdata->prompt = STRALLOC("");
	else
		ch->pcdata->prompt = STRALLOC(argument);
	return;
}

/*
 * Figured this belonged here seeing it involves players... 
 * really simple little function to tax players with a large
 * amount of gold to help reduce the overall gold pool...
 *  --TRI
 */
void tax_player(CHAR_DATA *ch)
{
	return;
}

void newskills(CHAR_DATA *ch, int gain)
{

	int sn = 1;
	char skill[MAX_STRING_LENGTH];

	if (IS_NPC(ch) || TRUE)
		return;

	for (sn = 0; sn < top_sn; sn++)
	{
		if (!skill_table[sn]->name)
			break;

		if (skill_table[sn]->type != SKILL_SKILL)
			continue;

		if (!skill_table[sn])
			break;

		if (SPELL_FLAG(skill_table[sn], SF_SECRETSKILL))
			continue;

		if (ch->basepl > skill_table[sn]->race_level[ch->race] && skill_table[sn]->race_level[ch->race] > ch->basepl - gain)
		{
			if (skill_table[sn]->form == FALSE)
			{
				ch_printf(ch, "You can now use %s!\n\r", skill_table[sn]->name);
			}
			else
			{
				pager_printf(ch, "Something deep inside you is changing...");
				ch->pcdata->learned[sn] = 100;
				ch->pcdata->set[sn] = TRUE;
				sprintf(skill, "%s", skill_table[sn]->name);
				interpret(ch, skill);
			}
		}
		continue;
	}
	return;
}

/* void do_finishbuster( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

	switch( ch->substate )
    {
	default:
         ch->skill_timer = 4;
         add_timer( ch, TIMER_DO_FUN, 4, do_finishbuster, 1 );
         pager_printf( ch, "&GYou bring both hands back behind your back and discreetly form two %s &Gballs of ki, trying to keep their energy signature undetectable.\n\r", ch->energycolour );
         pager_printf( ch, "&GAs %s brings both hands back behind %s back you notice a faint %s &Gglow eminating from %s\n\r", ch->name, ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour, ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
         act( AT_GREEN, "$n brings both $s arms behind $m and you notice $e begins to glow very faintly...\n\r", ch, NULL, victim, TO_NOTVICT );
         ch->alloc_ptr = str_dup( arg );
         return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your Finish Buster was interupted!\n\r", ch );
          pager_printf( victim, "&G%s's Finish Buster was interrupted!\n\r", ch->name );
          act( AT_GREEN, "$n's Finish Buster was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your Finish Buster.\n\r", ch );
    	act( AT_GREEN, "$n's cancels $s attack.\n\r",  ch, NULL, NULL, TO_CANSEE );
 	    return;
    }

    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_finishbuster ) )
    {
         learn_from_success( ch, gsn_finishbuster );
         pager_printf( ch, "&GYour attack now complete, you quickly arc both your hands round to your front and, aiming at %s, scream '&RFINISH BUSTER!&G' and fire a light wave of %s &Gki at %s!\n\r", victim->name, ch->energycolour, ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
         pager_printf( victim, "&G%s suddenly looks ready to attack; %s quickly arcs both hands round to %s front and, screaming '&RFINISH BUSTER!' lauches a light %s &Gwave of ki at you\n\r", ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "her", ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her",  ch->energycolour );
         act( AT_GREEN, "$n speedily arcs both arms round to $s front and screams '&RFINISH BUSTER!', firing a light ki wave at $N!\n\r", ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, (number_range( 150, 300 ) * ( get_curr_wis(ch) 
/ 100 ) ), gsn_finishbuster );
    }
    else
    {
        learn_from_failure( ch, gsn_finishbuster );
        pager_printf( ch, "&GYour ki control really isn't up to the standard it should be: As soon as you move your arms the ki you collected dissipates!\n\r", victim->name, ch->energycolour, ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
        pager_printf( victim, "&G%s swings both arms round to %s front, but it becomes quickly obvious that %s has lost the ki %s collected!\n\r", ch->name, ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she" );
        act( AT_GREEN, "$n speedily arcs both arms round to $s, but the ki $e had collected just evaporates as $e laughs sheepishly!\n\r", ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_finishbuster );
    }
return;
}
*/
void do_layegg(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *victim;
	VASSAL_DATA *vassal;

	if ((victim = who_fighting(ch)) == NULL)
	{
		send_to_char("You aren't fighting anyone.\n\r", ch);
		return;
	}

	if (IS_NPC(victim))
	{
		send_to_char("&GWaste an egg on that piece of trash?\n\r", ch);
		return;
	}

	for (vassal = ch->first_vassal; vassal; vassal = vassal->next)
	{
		if (!str_cmp(vassal->name, victim->name))
		{
			send_to_char("You've already laid an egg in that person.\n\r", ch);
			return;
		}
	}

	if (can_use_skill(ch, number_percent(), gsn_layegg) && victim->basepl < 25 * ch->basepl)
	{
		if (victim->race == RACE_DEMON || victim->race == RACE_NAMEK)
		{
			ch_printf(ch, "Damn!  You laid an egg in %s, but %s just spat it out...\n\r", victim->name, HESHE(victim->sex));
			ch_printf(victim, "%s tries to lay an egg in you... What a loser.\n\r", ch->name);
			return;
		}
		learn_from_success(ch, gsn_layegg);
		/*         pager_printf( ch, "&GYou sieze your opportunity and leap inside %s, quickly laying an egg!!\n\r", victim->name );
         pager_printf( victim, "&G%s suddenly disappears from view, and you notice just too late that %s has entered your body! In a few seconds, %s emerges, but you feel exactly the same...", ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
         act( AT_GREEN, "$n turns to liquid metal and leaps inside $N, emerging a few seconds later!", ch, NULL, victim, TO_NOTVICT );*/
		if (!victim->vasselto || str_cmp(victim->vasselto, ch->name))
		{
			ch->eggslaid++;
			CREATE(vassal, VASSAL_DATA, 1);
			vassal->name = STRALLOC(victim->name);
			LINK(vassal, ch->first_vassal, ch->last_vassal, next, prev);
			pager_printf(ch, "&GYou sieze your opportunity and leap inside %s, quickly laying an egg!!\n\r", victim->name);
			pager_printf(victim, "&G%s suddenly disappears from view, and you notice just too late that %s has entered your body! In a few seconds, %s emerges, but you feel exactly the same...", ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her");
			act(AT_GREEN, "$n turns to liquid metal and leaps inside $N, emerging a few seconds later!", ch, NULL, victim, TO_NOTVICT);
			victim->vasselto = STRALLOC(ch->name);
		}
		else
		{
			pager_printf(ch, "&GYou sieze your opportunity and leap inside %s, to lay an egg, only to find %s has already been impregnated!!\n\r", victim->name, HESHE(victim->sex));
			pager_printf(victim, "&G%s suddenly disappears from view, and you notice just too late that %s has entered your body! In a few seconds, %s emerges, but you feel exactly the same...", ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0 ? "its" : ch->sex == 1 ? "his" : "her");
			act(AT_GREEN, "$n turns to liquid metal and leaps inside $N, emerging a few seconds later!", ch, NULL, victim, TO_NOTVICT);
		}
		if (number_range(0, 100) > URANGE(25, (25 + ch->eggslaid), 95) && !IS_NPC(ch) && ch->pcdata->learned[gsn_scrambled_egg] < 10)
		{
			ch->pcdata->learned[gsn_scrambled_egg] = 10;
			pager_printf(ch, "\n\r\n\r\n\r&RIn an utterly underplayed thought, you realise that maybe you could detonate the egg for damage...\n\r\n\r\n\r");
		}
	}
	else
	{
		learn_from_failure(ch, gsn_layegg);
		pager_printf(ch, "&GYou sieze your opportunity and leap inside %s, but %s explodes in a small burst of ki, realising your intention!!\n\r", victim->name, victim->sex == 0 ? "it" : victim->sex == 1 ? "he" : "she");
		pager_printf(victim, "&G%s suddenly disappears from view, but you realise what %s is doing! In a few seconds, %s appears again, and you quickly blast %s in the face!\n\r", ch->name, ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0 ? "it" : ch->sex == 1 ? "he" : "she", ch->sex == 0 ? "it" : ch->sex == 1 ? "him" : "her");
		act(AT_GREEN, "$n turns to liquid metal and goes to leap inside $N, but $E realises with is happening and blasts $n in the face!", ch, NULL, victim, TO_NOTVICT);
	}
	return;
}

void do_scrambled_egg(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *victim;
	VASSAL_DATA *vassal = NULL;
	bool match = FALSE;
	char arg[MAX_INPUT_LENGTH];

	if ((victim = who_fighting(ch)) == NULL)
	{
		send_to_char("You aren't fighting anyone.\n\r", ch);
		return;
	}

	if (IS_NPC(victim))
	{
		send_to_char("&GYou've got no egg in %s.\n\r", ch);
		return;
	}

	for (vassal = ch->first_vassal; vassal; vassal = vassal->next)
	{
		if (vassal->name && !str_cmp(vassal->name, victim->name))
			match = TRUE;
	}
	if (!match)
	{
		send_to_char("You need to lay an egg in that person first, bud.\n\r", ch);
		return;
	}

	switch (ch->substate)
	{
	default:
		ch->skill_timer = 7;
		add_timer(ch, TIMER_DO_FUN, 7, do_scrambled_egg, 1);
		pager_printf(ch, "&GYou raise a hand, and stare hard at %s, concentrating on the egg you laid in %s.\n", victim->name, HIMHER(victim->sex));
		pager_printf(victim, "%s raises a hand, and stares at your throat... What the hell?", ch->name);
		act(AT_GREEN, "$n stares at $N's throat... Well this looks like an interesting move >_>", ch, NULL, victim, TO_NOTVICT);
		ch->alloc_ptr = str_dup(arg);
		return;

	case 1:
		if (!ch->alloc_ptr)
		{
			send_to_char("Your Scrambled Egg was interrupted!\n\r", ch);
			act(AT_GREEN, "$n's Scrambled Egg was interrupted!\n\r", ch, NULL, victim, TO_NOTVICT);
			return;
		}
		strcpy(arg, ch->alloc_ptr);
		DISPOSE(ch->alloc_ptr);
		break;

	case SUB_TIMER_DO_ABORT:
		DISPOSE(ch->alloc_ptr);
		ch->substate = SUB_NONE;
		send_to_char("You stop perfoming your attack.\n\r", ch);
		act(AT_GREEN, "$n stops perfoming $s attack.\n\r", ch, NULL, victim, TO_CANSEE);
		return;
	}

	ch->substate = SUB_NONE;

	if (can_use_skill(ch, number_percent(), gsn_scrambled_egg))
	{
		learn_from_success(ch, gsn_scrambled_egg);
		pager_printf(ch, "&GYou snap back your hand, and pull it down in a grand, sweeping motion.  %s starts to choke, apparently unable to breathe or speak, before vomiting up the egg you laid in %s!  %s mouth covered in vomit, %s convulses as you detonate the egg in %s mouth! &R[65,000 damage]\n\r", victim->name, HIMHER(victim->sex), HISHER(victim->sex), victim->name, HISHER(victim->sex));
		pager_printf(victim, "&G%s snaps back %s hand, and clenches %s fist, pulling it down in a grand, sweeping movement.  You start to choke and convulse, unable to breathe, before finally you vomit up the egg that was laid in you.  As it passes up your throat, you feel it expanding, before the thing detonates in your mouth! &R[65,000 damage]\n\r", ch->name, HISHER(ch->sex), HISHER(ch->sex));
		act(AT_GREEN, "$N starts vomiting up an egg, which explodes in $S mouth!", ch, NULL, victim, TO_NOTVICT);
		victim->hit -= 65000;

		for (vassal = ch->first_vassal; vassal; vassal = vassal->next)
		{
			if (!str_cmp(victim->name, vassal->name))
			{
				ch->eggslaid--;
				UNLINK(vassal, ch->first_vassal, ch->last_vassal, next, prev);
				STRFREE(vassal->name);
				STRFREE(ch->vasselto);
				DISPOSE(vassal);
				do_save(ch, "auto");
				do_save(victim, "auto");
				break;
			}
		}
	}
	else
	{
		learn_from_failure(ch, gsn_scrambled_egg);
		pager_printf(ch, "&GNothing happens...\n\r");
	}
	return;
}

void do_vassels(CHAR_DATA *ch, char *argument)
{
	if (ch->race != RACE_TUFFLE)
	{
		send_to_char("Huh?", ch);

		return;
	}

	pager_printf(ch, "&YP&ye&Yo&yp&Yl&ye &Yy&yo&Yu &yc&Yo&yn&Yt&yr&Yo&yl&Y a&yc&Yt&yi&Yv&ye&Yl&yy&Y:\n\r");
	pager_printf(ch, "&C%d eggs laid\n\r", ch->eggslaid);
	pager_printf(ch, "Projected powerlevel multiplier with combined strength: %sx\n\r", format_pl(URANGE(0, (ch->eggslaid / 2), 100)));
	return;
}

void do_steam(CHAR_DATA *ch, char *argument)
{
	if (IS_NPC(ch))
		return;
	if (ch->race != RACE_DEMON)
		return;
	if (ch->desteam == TRUE)
	{
		send_to_char("You stop releasing steam.\n\r", ch);
		ch->desteam = FALSE;
		return;
	}
	ch->desteam = TRUE;
	return;
}

void do_skills(CHAR_DATA *ch, char *argument)
{
	int sn = 0;
	int curr = 0;
	int raceskills = 0;
	int i = 0;
	unsigned long long j, index, index2;

	if (IS_NPC(ch))
		return;

	for (sn = 0; sn < top_sn; sn++)
	{
		if (!skill_table[sn]->name)
			break;
		if (skill_table[sn]->type != SKILL_SKILL)
			continue;
		if (skill_table[sn]->race_adept[ch->race] < 10)
			continue;
		raceskills++;
	}

	if (raceskills > 0)
	{
		unsigned long long skills[raceskills][2];

		for (sn = 0; sn < top_sn; sn++)
		{
			if (!skill_table[sn]->name)
				break;
			if (skill_table[sn]->type != SKILL_SKILL)
				continue;
			if (skill_table[sn]->race_adept[ch->race] < 10)
				continue;
			skills[curr][0] = sn;
			skills[curr][1] = skill_table[sn]->race_level[ch->race];
			curr++;
		}

		for (i = 1; i < raceskills; i++)
		{
			index = skills[i][1];
			index2 = skills[i][0];
			j = i;
			while ((j > 0) && (skills[j - 1][1] > index))
			{
				skills[j][1] = skills[j - 1][1];
				skills[j][0] = skills[j - 1][0];
				j = j - 1;
			}
			skills[j][1] = index;
			skills[j][0] = index2;
		}

		pager_printf(ch, "&Y =-=-=-=-=-=-=-=-=-=-=-=-= &WSkill List &Y=-=-=-=-=-=-=-=-=-=-=-=-= \n\r");
		for (i = 0; i < raceskills; i++)
		{
			sn = skills[i][0];

			if (SPELL_FLAG(skill_table[sn], SF_SECRETSKILL) && ch->pcdata->learned[sn] <= 0)
			{
				send_to_char("??????????????????????\n\r", ch);
				continue;
			}
			pager_printf(ch, "%s%-23s    &W%20s pl    ",
						 skill_table[sn]->race_level[ch->race] > ch->basepl ? "&R" : ch->pcdata->learned[sn] > 0 ? "&G" : "&Y",
						 capitalize(skill_table[sn]->name),
						 format_pl(skill_table[sn]->race_level[ch->race]));
			if (skill_table[sn]->min_mana <= 0)
				pager_printf(ch, "&c[Free use]\n\r");
			else
				pager_printf(ch, "&C%u ki\n\r", (skill_table[sn]->min_mana));
		}
	}
	return;
}

/*
 *  The perk setting command.
 */

void do_perk(CHAR_DATA *ch, char *argument)
{
	int ctr = 0;
	char arg[MAX_INPUT_LENGTH];
	argument = one_argument(argument, arg);

	if (IS_NPC(ch))
	{
		pager_printf(ch, "You need 6 potential to get a perk.\n\r");
		return;
	}

	if (arg == NULL || arg[0] == '\0')
	{
		pager_printf(ch, "&W      Perk         &w|&W        Requirements:\n\r");
		if (ch->perk[PERK_AFTER_IMAGER] == FALSE && ch->perm_dex > 300 && ch->perm_wis > 300)
			pager_printf(ch, "&CAfter Imager       &W-  &cOver 300 dexterity and wisdom\n\r");
		if (ch->perk[PERK_ALL_SEEING_EYE] == FALSE && ch->perm_dex > 300 && ch->perm_int > 300)
			pager_printf(ch, "&CAll Seeing Eye     &W-  &cOver 300 dexterity and intelligence\n\r");
		if (ch->perk[PERK_ADRENALINE_RUSH] == FALSE && ch->perm_con > 150 && ch->perm_str > 150)
			pager_printf(ch, "&CAdrenaline Rush    &W-  &cOver 150 constitution and strength\n\r");
		if (ch->perk[PERK_AWARENESS] == FALSE && ch->perm_int > 300)
			pager_printf(ch, "&CAwareness          &W-  &cOver 300 intelligence\n\r");
		if (ch->perk[PERK_BETTER_CRITICALS] == FALSE && ch->perm_str > 500)
			pager_printf(ch, "&CBetter Criticals   &W-  &cOver 500 strength\n\r");
		if (ch->perk[PERK_BONUS_HTH] == FALSE && ch->perm_dex > 500)
			pager_printf(ch, "&CBonus HtH          &W-  &cOver 500 dexterity\n\r");
		if (ch->perk[PERK_BROWN_NOSER] == FALSE && ch->perm_cha > 300 && ch->perm_int > 200)
			pager_printf(ch, "&CBrown Noser        &W-  &cOver 300 charisma and 200 intelligence\n\r");
		if (ch->perk[PERK_BRUTISH_HULK] == FALSE && ch->perm_con > 500)
			pager_printf(ch, "&CBrutish Hulk       &W-  &cOver 500 constitution\n\r");
		if (ch->perk[PERK_BUSTER_BLADER] == FALSE && ch->perm_str > 400)
			pager_printf(ch, "&CBuster Blader      &W-  &cOver 400 strength\n\r");
		if (ch->perk[PERK_CANCEROUS_GROWTH] == FALSE && ch->perm_con < 100)
			pager_printf(ch, "&CCancerous Growth   &W-  &cLess than 100 constitution\n\r");
		if (ch->perk[PERK_CHEAPSKATE] == FALSE && ch->perm_cha > 500)
			pager_printf(ch, "&CCheapskate         &W-  &cOver 500 charisma\n\r");
		if (ch->perk[PERK_DIE_HARD] == FALSE && ch->perm_con > 400 && ch->perm_str > 400)
			pager_printf(ch, "&CDie Hard           &W-  &cOver 400 constitution and strength\n\r");
		if (ch->perk[PERK_DIVINE_FAVOUR] == FALSE && ch->perm_wis > 300)
			pager_printf(ch, "&CDivine Favour      &W-  &cOver 300 wisdom\n\r");
		if (ch->perk[PERK_FASTER_HEALING] == FALSE && ch->perm_con > 500)
			pager_printf(ch, "&CFaster Healing     &W-  &cOver 500 constitution\n\r");
		if (ch->perk[PERK_FORWARD_PLANNING] == FALSE && ch->perm_int > 600 && ch->perm_con > 600 && ch->perm_dex > 600)
			pager_printf(ch, "&CForward Planning   &W-  &cOver 600 constitution, intelligence and dexterity.\n\r");
		if (ch->perk[PERK_GAMBLER] == FALSE && ch->perm_lck > 250)
			pager_printf(ch, "&CGambler            &W-  &cOver 250 luck\n\r");
		if (ch->perk[PERK_HERE_AND_NOW] == FALSE && ch->basepl / 100 <= 100000000)
			pager_printf(ch, "&CHere And Now       &W-  &cLess than 10,000,000,000 base powerlevel\n\r");
		if (ch->perk[PERK_HIDE_OF_SCARS] == FALSE && ch->pdeaths >= 50)
			pager_printf(ch, "&CHide Of Scars      &W-  &cAt least 50 deaths to other players.\n\r");
		if (ch->perk[PERK_HIT_THE_DECK] == FALSE && ch->perm_dex > 600)
			pager_printf(ch, "&CHit The Deck       &W-  &cOver 600 dexterity\n\r");
		if (ch->perk[PERK_IRON_FISTED] == FALSE && ch->perm_str > 250 && ch->perm_cha > 250)
			pager_printf(ch, "&CIron Fisted        &W-  &cOver 250 strength and charisma\n\r");
		if (ch->perk[PERK_MORE_CRITICALS] == FALSE && ch->perm_lck > 600)
			pager_printf(ch, "&CMore Criticals     &W-  &cOver 600 luck\n\r");
		if (ch->perk[PERK_QUICK_RECOVERY] == FALSE && ch->perm_con >= 600)
			pager_printf(ch, "&CQuick Recovery     &W-  &cOver 600 constitution\n\r");
		if (ch->perk[PERK_RANGER] == FALSE && ch->perm_int > 300 && ch->perm_wis > 300 && ch->perm_con > 300)
			pager_printf(ch, "&CRanger             &W-  &cOver 300 intelligence, wisdom and constitution\n\r");
		if (ch->perk[PERK_REMORSELESS_BASTARD] == FALSE && ch->alignment < -800)
			pager_printf(ch, "&CRemorseless Bastard&W-  &cAlignment under -800\n\r");
		if (ch->perk[PERK_SHOWMAN] == FALSE && ch->perm_cha > 250)
			pager_printf(ch, "&CShowman            &W-  &cOver 250 charisma\n\r");
		if (ch->perk[PERK_SILENT_DEATH] == FALSE && (ch->perm_str + ch->perm_int + ch->perm_wis + ch->perm_dex + ch->perm_con + ch->perm_cha + ch->perm_lck) > 2500)
			pager_printf(ch, "&CSilent Death       &W-  &cAll stats total 2500 or more\n\r");
		if (ch->perk[PERK_SLAYER] == FALSE && (ch->perm_str + ch->perm_int + ch->perm_wis + ch->perm_dex + ch->perm_con + ch->perm_cha + ch->perm_lck) > 10000)
			pager_printf(ch, "&CSlayer             &W-  &cAll stats total 10000 or more\n\r");
		if (ch->perk[PERK_STUNNING_LOOKS] == FALSE && ch->sex == 2 && ch->perm_cha < 500)
			pager_printf(ch, "&CStunning Looks     &W-  &cFemale with under 500 charisma\n\r");
		if (ch->perk[PERK_TOUGHNESS] == FALSE && ch->perm_con >= 300)
			pager_printf(ch, "&CToughness          &W-  &cOver 300 constitution\n\r");
		if (ch->perk[PERK_WAY_OF_THE_FRUIT] == FALSE && ch->perm_con > 800)
			pager_printf(ch, "&CWay Of The Fruit   &W-  &cOver 800 constitution\n\r");
		return;
	}
	else
	{
		if (IS_NPC(ch) || ((ch->rps < 50.0 && !ch->perk[PERK_CHEAPSKATE]) || (ch->rps < 25.0)))
		{
			pager_printf(ch, "You need more roleplay points to get a perk.\n\r");
			return;
		}
		ctr = 0;
		for (;;)
		{
			if (!str_cmp(PERK[ctr], arg))
				break;
			ctr++;
			if (ctr == PERK_MAX)
				break;
		}

		if (ctr == PERK_MAX)
		{
			pager_printf(ch, "That is not a perk.\n\r", ch);
			return;
		}
		switch (ctr)
		{
		case PERK_ADRENALINE_RUSH:
			if (ch->perk[PERK_ADRENALINE_RUSH] == FALSE && ch->perm_con >= 50 && ch->perm_str >= 50)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_AFTER_IMAGER:
			if (ch->perk[PERK_AFTER_IMAGER] == FALSE && ch->perm_dex >= 200 && ch->perm_wis >= 200)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_ALL_SEEING_EYE:
			if (ch->perk[PERK_ALL_SEEING_EYE] == FALSE && ch->perm_dex >= 200 && ch->perm_int >= 200)
				;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_AWARENESS:
			if (ch->perk[PERK_AWARENESS] == FALSE && ch->perm_int >= 200)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_BETTER_CRITICALS:
			if (ch->perk[PERK_BETTER_CRITICALS] == FALSE && ch->perm_str >= 250)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_BONUS_HTH:
			if (ch->perk[PERK_BONUS_HTH] == FALSE && ch->perm_dex >= 200)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_BROWN_NOSER:
			if (ch->perk[PERK_BROWN_NOSER] == FALSE && ch->perm_cha >= 200 && ch->perm_int > 100)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_BRUTISH_HULK:
			if (ch->perk[PERK_BRUTISH_HULK] == FALSE && ch->perm_con >= 250)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
			if (ch->perk[PERK_BUSTER_BLADER] == FALSE && ch->perm_str > 400)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_CANCEROUS_GROWTH:
			if (ch->perk[PERK_CANCEROUS_GROWTH] == FALSE && ch->perm_con <= 100)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_CHEAPSKATE:
			if (ch->perk[PERK_CHEAPSKATE] == FALSE && ch->perm_cha >= 200)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_DIE_HARD:
			if (ch->perk[PERK_DIE_HARD] == FALSE && ch->perm_con >= 100 && ch->perm_str >= 100)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_DIVINE_FAVOUR:
			if (ch->perk[PERK_DIVINE_FAVOUR] == FALSE && ch->perm_wis >= 200)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_FASTER_HEALING:
			if (ch->perk[PERK_FASTER_HEALING] == FALSE && ch->perm_con >= 150)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_FORWARD_PLANNING:
			if (ch->perk[PERK_FORWARD_PLANNING] == FALSE && ch->perm_int >= 300 && ch->perm_con >= 200 && ch->perm_dex >= 50)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_HIDE_OF_SCARS:
			if (ch->perk[PERK_HIDE_OF_SCARS] == FALSE && ch->pdeaths >= 50)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_GAMBLER:
			if (ch->perk[PERK_GAMBLER] == FALSE && ch->perm_lck >= 175)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_HERE_AND_NOW:
			if (ch->perk[PERK_HERE_AND_NOW] == FALSE && ch->basepl / 100 <= 100000000)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_HIT_THE_DECK:
			if (ch->perk[PERK_HIT_THE_DECK] == FALSE && ch->perm_dex >= 300)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_IRON_FISTED:
			if (ch->perk[PERK_IRON_FISTED] == FALSE && ch->perm_str >= 150 && ch->perm_cha >= 150)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_MORE_CRITICALS:
			if (ch->perk[PERK_MORE_CRITICALS] == FALSE && ch->perm_lck >= 300)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_QUICK_RECOVERY:
			if (ch->perk[PERK_QUICK_RECOVERY] == FALSE && ch->perm_con >= 300)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_RANGER:
			if (ch->perk[PERK_RANGER] == FALSE && ch->perm_int >= 200 && ch->perm_wis >= 200 && ch->perm_con <= 100)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_REMORSELESS_BASTARD:
			if (ch->perk[PERK_REMORSELESS_BASTARD] == FALSE && ch->alignment <= -800)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_SHOWMAN:
			if (ch->perk[PERK_SHOWMAN] == FALSE && ch->perm_cha >= 250)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_SILENT_DEATH:
			if (ch->perk[PERK_SILENT_DEATH] == FALSE && (ch->perm_str + ch->perm_int + ch->perm_wis + ch->perm_dex + ch->perm_con + ch->perm_cha + ch->perm_lck) > 1750)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_SLAYER:
			if (ch->perk[PERK_SLAYER] == FALSE && (ch->perm_str + ch->perm_int + ch->perm_wis + ch->perm_dex + ch->perm_con + ch->perm_cha + ch->perm_lck) > 10000)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_STUNNING_LOOKS:
			if (ch->perk[PERK_STUNNING_LOOKS] == FALSE && ch->sex == 2 && ch->perm_cha >= 175)
				break;
			else
			{
				pager_printf(ch, "You do not fit the requirements for that perk.\n\r");
				return;
			}
			break;
		case PERK_TOUGHNESS:
			if (ch->perk[PERK_TOUGHNESS] == FALSE && ch->perm_con >= 300)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
			break;
		case PERK_WAY_OF_THE_FRUIT:
			if (ch->perk[PERK_WAY_OF_THE_FRUIT] == FALSE && ch->perm_con > 400)
				break;
			else
			{
				pager_printf(ch, "You don't have the requisites for that perk.\n\r");
				return;
			}
		}
		ch->perk[ctr] = TRUE;
		deduct_potential(ch);
		pager_printf(ch, "Perk Set.  Roleplays deducted.\n\r");
		if (ctr == PERK_BROWN_NOSER)
		{
			ch->perm_str += 50;
			ch->perm_int += 50;
			ch->perm_wis += 50;
			ch->perm_dex += 50;
			ch->perm_con += 50;
			ch->perm_cha += 50;
			ch->perm_lck += 50;
		}
		if (ctr == PERK_DIVINE_FAVOUR)
		{
			ch->perm_str += 100;
			ch->perm_int += 100;
			ch->perm_wis += 100;
			ch->perm_dex += 100;
			ch->perm_con += 100;
			ch->perm_cha += 100;
			ch->perm_lck += 100;
		}
		if (ctr == PERK_GAMBLER)
			ch->perm_lck *= 2;
		if (ctr == PERK_STUNNING_LOOKS)
			ch->perm_cha *= 2;
		if (ctr == PERK_RANGER)
			ch->perm_con *= 1.2;
		if (ctr == PERK_HERE_AND_NOW)
		{
			ch->basepl *= 2;
			update_current(ch, NULL);
		}
		return;
	}
}

void deduct_potential(CHAR_DATA *ch)
{
	if (ch->perk[PERK_CHEAPSKATE])
		ch->rps -= 25.0;
	else
		ch->rps -= 50.0;
	if (ch->rps < 0.0)
		ch->rps = 0.0;
	return;
}

void do_setsignature(CHAR_DATA *ch, char *argument)
{
	return; /*
 SIG_DATA *sig = NULL;
 sig->name = STRALLOC( "Nullskill" );
 char   arg1[MAX_INPUT_LENGTH];
 int    num = 0;

 if ( IS_NPC(ch) || ch->rppoints < 6 )
 {
	pager_printf( ch, "A warrior requires enormous potential to create his or her own moves.\n\r" );
	return;
 }

 if ( !argument || !arg1 || !str_cmp( arg1, "help" ) || !str_cmp( argument, "help" ) )
 {
	pager_printf( ch, "Syntax: setsignature	<field> <argument>\n\r" );
	pager_printf( ch, "Field being one of...\n\r" );
	pager_printf( ch, "Name      (Requires a string as the skill's name)\n\r" );
        pager_printf( ch, "Roomstart (Requires a string, using variables, sent to the room when you begin this skill.\n\r" );
        pager_printf( ch, "Victstart (Requires a string, using variables, sent to the victim when you begin this skill.\n\r" );
        pager_printf( ch, "Selfstart (Requires a string, using variables, sent to you when you begin this skill.\n\r" );
        pager_printf( ch, "Roomend (Requires a string, using variables, sent to the room when you finish this skill.\n\r" );
        pager_printf( ch, "Victend (Requires a string, using variables, sent to the victim when you finish this skill.\n\r" );
        pager_printf( ch, "Selfend (Requires a string, using variables, sent to you when you finish this skill.\n\r" );
	pager_printf( ch, "MinDam\t\tMaxDam\t\tShow (Shows your sig tech so far)\n\r" );
	pager_printf( ch, "Effects (&WPiercing, Deathtouch, Counterattack, Blindtouch, Nabzeni, Stonetouch, Secret)\n\r" );
	return;
 }
 argument = one_argument( argument, arg1 );
 if ( !str_cmp( arg1, "Name" ) )
 {
	if ( !argument )
	{
		pager_printf( ch, "You need to specify the signature technique's name.\n\r" );
		return;
	}
	if ( !ch->sig )
		ch->sig = sig;
	STRFREE( ch->sig->name );
	ch->sig->name = STRALLOC( argument );
	pager_printf( ch, "&YDone\n\r" );
 }
 if ( !str_cmp( arg1, "Roomstart" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's roomstart.\n\r" );
                return;
        }
        ch->sig->roomstart = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }
 if ( !str_cmp( arg1, "Victstart" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's victstart.\n\r" );
                return;
        }
        ch->sig->victstart = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }
 if ( !str_cmp( arg1, "Selfstart" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's selfstart.\n\r" );
                return;
        }
        ch->sig->selfstart = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }


 if ( !str_cmp( arg1, "Roomend" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's roomend.\n\r" );
                return;
        }
        ch->sig->roomend = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }
 if ( !str_cmp( arg1, "Victend" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's victend.\n\r" );
                return;
        }
        ch->sig->victend = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }
 if ( !str_cmp( arg1, "Selfend" ) )
 {
        if ( !argument )
        {
                pager_printf( ch, "You need to specify the signature technique's selfend.\n\r" );
                return;
        }
        ch->sig->selfend = STRALLOC( argument );
        pager_printf( ch, "&YDone\n\r" );
 }

// This is where it gets tricky

 if ( !str_cmp( arg1, "Mindam" ) )
 {
	if ( !is_number( argument ) )
	{
		pager_printf( ch, "That is not a number!\n\r" );
		return;
	}	
	num = atoi( argument );
	if ( num < 1 || num > 1000 )
	{
		pager_printf( ch, "Mindam cannot be over 1000, at least 0.\n\r" );
		return;
	}
	ch->sig->mindam = num;
	ch->sig->cost   = 10 * num;
	pager_printf( ch, "&YDone\n\r" );
 }

 if ( !str_cmp( arg1, "Maxdam" ) )
 {
        if ( !is_number( argument ) )
        {
                pager_printf( ch, "That is not a number!\n\r" );
                return;
        }
        num = atoi( argument );
        if ( num < 1001 || num > 9999 )
        {
                pager_printf( ch, "Maxdam cannot be over 9999, at least 1001.\n\r" );
                return;
        }
        ch->sig->maxdam = num;
        ch->sig->length = ( 6 + ( num / 1000 ) ) * 2;
        pager_printf( ch, "&YDone\n\r" );
 }

 if ( !str_cmp( arg1, "Effects" ) )
 {
	if ( !str_cmp( arg1, "Piercing" ) )
		ch->sig->effects[SIG_PIERCING] = !ch->sig->effects[SIG_PIERCING];
        else if ( !str_cmp( arg1, "Deathtouch" ) )
                ch->sig->effects[SIG_DEATHTOUCH] = !ch->sig->effects[SIG_DEATHTOUCH];
        else if ( !str_cmp( arg1, "Counterattack" ) )
                ch->sig->effects[SIG_COUNTERATTACK] = !ch->sig->effects[SIG_COUNTERATTACK];
        else if ( !str_cmp( arg1, "Blindtouch" ) )
                ch->sig->effects[SIG_BLINDTOUCH] = !ch->sig->effects[SIG_BLINDTOUCH];
        else if ( !str_cmp( arg1, "Nabzeni" ) )
                ch->sig->effects[SIG_NABZENI] = !ch->sig->effects[SIG_NABZENI];
        else if ( !str_cmp( arg1, "Stonetouch" ) )
                ch->sig->effects[SIG_STONETOUCH] = !ch->sig->effects[SIG_STONETOUCH];
        else if ( !str_cmp( arg1, "Secret" ) )
                ch->sig->effects[SIG_SECRET] = !ch->sig->effects[SIG_SECRET];
	else
		pager_printf ( ch, "Effect not found.\n\r" );
	pager_printf( ch, "Effect %s toggled.\n\r" );
 }


 if ( ch->sig && ch->sig->name && ch->sig->roomstart && ch->sig->victstart && ch->sig->selfstart 
&& ch->sig->roomend && ch->sig->victend && ch->sig->selfend && ch->sig->mindam 
&& ch->sig->maxdam && ch->sig->mindam > 0 && ch->sig->mindam < 1001 
&& ch->sig->maxdam > 1000 && ch->sig->maxdam < 10000 )
 {
        ch->sig->length = ( 6 + ( num / 1000 ) ) * 2;
        ch->sig->cost   = 10 * num;
	if ( ch->sig->effects[SIG_PIERCING] )  ch->sig->length += 3;
        if ( ch->sig->effects[SIG_DEATHTOUCH] )  ch->sig->length += 6;
        if ( ch->sig->effects[SIG_NABZENI] )  ch->sig->length += 4;
        if ( ch->sig->effects[SIG_COUNTERATTACK] )  ch->sig->length += 5;
        if ( ch->sig->effects[SIG_BLINDTOUCH] )  ch->sig->length += 7;
        if ( ch->sig->effects[SIG_STONETOUCH] )  ch->sig->length += 2;
	ch->sig->canuse = TRUE;
	pager_printf( ch, "You can use your skill by typing 'Signature'\n\r" );
	return;
 }
 pager_printf( ch, "Your skill is unfinished.\n\r" );
 return;*/
}

/*
void do_create( CHAR_DATA *ch, char * argument )
{
  char              arg1[MAX_INPUT_LENGTH];
  char              arg2[MAX_INPUT_LENGTH];
  char              buf[MAX_STRING_LENGTH];
  OBJ_INDEX_DATA  * objindex;
  OBJ_DATA        * obj;
  OBJ_DATA        * material;
  AFFECT_DATA     * paf;
  int	  	    affectloc = 0;
  bool		    done = FALSE;
  int chance = number_range ( 1, 100 );
  
  if ( ch->createtimer > 0 || ch->position != POS_STANDING )
  {
	send_to_char( "Just a second...\n\r", ch );
	return;
  }

  if ( ch->loyaltykili < 1 )
  {
	send_to_char( "You don't have any loyality kili!\n\r", ch );
	return;
  }

  argument = one_argument( argument, arg1 );
  argument = one_argument( argument, arg2 );

  if ( !arg1 || arg1[0] == '\0' )
  {
        send_to_char( "You need to specify arguments.  Type 'Create list' or 'Create help' for more information.\n\r", ch );
        return;
  }

  if( !str_cmp( arg1, "help" ) )
  {
	interpret( ch, "help create" );
	return;
  }

  if( !str_cmp( arg1, "list" ) )
  {
	send_to_char( "&Y------------------------------\n\r", ch );
	send_to_char( "&WOBJECTS MADE FROM 'SKIN'\n\r", ch );
	send_to_char( "&CBoots\n\r", ch );
	send_to_char( "&CClothes\n\r", ch );
	send_to_char( "&CCloak\n\r", ch );
	send_to_char( "&CToy\n\r", ch );
	send_to_char( "&Y------------------------------\n\r", ch );
	send_to_char( "&WOBJECTS MADE FROM 'TRASH'\n\r", ch );
	send_to_char( "&CArmor (Armour)\n\r", ch );
	send_to_char( "&CWeapon\n\r", ch );
	send_to_char( "&CShield\n\r", ch );
	send_to_char( "&CJewellery\n\r", ch );
	send_to_char( "&Y------------------------------\n\r", ch );
	send_to_char( "&WOBJECTS MADE FROM 'TREASURE'\n\r", ch );
	send_to_char( "&CPower Armor &r(KEYWORD: Power)\n\r", ch );
	send_to_char( "&CTraining gear &r(KEYWORD: Training)\n\r", ch );
	return;
  }

  if ( !str_cmp( arg2, "Skin" ) )
  {
	send_to_char( "Be more specific.\n\r", ch );
	return;
  }

  material = get_obj_carry( ch, arg2 );

  if ( !material )
  {
	send_to_char( "You do not have that item.\n\r", ch );
	return;
  }

  if ( str_cmp( arg1, "Boots"     ) && str_cmp( arg1, "Clothes"   ) &&
       str_cmp( arg1, "Cloak"     ) && str_cmp( arg1, "Toy"       ) &&
       str_cmp( arg1, "Armor"     ) && str_cmp( arg1, "Weapon"    ) &&
       str_cmp( arg1, "Shield"    ) && str_cmp( arg1, "Jewellery" ) &&
       str_cmp( arg1, "Power"     ) && str_cmp( arg1, "Training"  ) )
  {
	send_to_char( "Type 'Create list' for the list of items you can make.\n\r", ch );
	return;
  }

  ch->createtimer = 20;  

  if ( !can_use_skill( ch, number_percent(), gsn_create ) )
  {
	pager_printf( ch, "You try to forge %s into something, but fail and just make garbage.\n\r", material->short_descr );
	act( AT_PINK, "$n tries to make something, but $e fails.", ch, NULL, NULL, TO_CANSEE );
	separate_obj(material);
	obj_from_char( material );
	extract_obj( material );
	learn_from_failure( ch, gsn_create );
	objindex = get_obj_index( 77 );
	obj = create_object( objindex, 2 );
	obj_to_char( obj, ch );
	return;
  }


//  ch->loyalitykili--;
  learn_from_success( ch, gsn_create );
  objindex = get_obj_index( 78 );
  obj = create_object( objindex, ch->basepl / 100 );
  separate_obj( obj );
  obj->cost = 50;
  STRFREE( obj->name );
  STRFREE( obj->short_descr );
  STRFREE( obj->description );
  if ( strstr( material->name, "skin" ) && material->item_type == ITEM_FOOD )
  {
	if ( !str_cmp( arg1, "Boots" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = 0.5 * number_range( get_curr_lck( ch ) / 10 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 5 );
		obj->value[1]  = obj->value[0];
		TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "feet" ) );
		switch ( number_range( 1, 4 ) )
		{
		case 1:
			sprintf( buf, "custom boots" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "custom made boots" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A pair of nice, animal skin boots have been left here." );
   			obj->description = STRALLOC ( buf );
			break;
		case 2:
			sprintf ( buf, "home made boots" );
			obj->name = STRALLOC ( buf );
			sprintf ( buf, "home made boots" );
			obj->short_descr = STRALLOC (buf );
			sprintf ( buf, "A pair of reasonable looking home-made boots have been left here." );
			obj->description = STRALLOC ( buf );
			break;
		case 3:
			sprintf ( buf, "generic boots" );
			obj->name = STRALLOC (buf );
			sprintf ( buf,  "generic boots" );
			obj->short_descr = STRALLOC ( buf );
			sprintf ( buf, "A pair of boots lie here.  They look pretty ordinary." );
			obj->description = STRALLOC ( buf );
			break;
		case 4:
			sprintf ( buf,"organic boots" );
			obj->name = STRALLOC ( buf );
     		sprintf ( buf, "organic boots" );
			obj->short_descr = STRALLOC ( buf );
			sprintf ( buf, "A pair of boots have been made and dropped here." );
			obj->description = STRALLOC ( buf );
			break;
		}
		affectloc = APPLY_DEX;

// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 8 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 4 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -75, -25 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 8 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 4 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 25, 75 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
		done = TRUE;
	}	

	if ( !str_cmp( arg1, "Clothes" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = 0.5 * number_range( get_curr_lck( ch ) / 10 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 5 );
		obj->value[1]  = obj->value[0];
		switch ( number_range( 1, 4 ) )
		{
		case 1:
            sprintf( buf, "custom gloves" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "custom made gloves" );
   			obj->short_descr = STRALLOC (buf );
            sprintf( buf, "A pair of nice, animal skin gloves have been left here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hands" ) );
			affectloc = APPLY_STR;
			break;
		case 2:
            sprintf( buf,  "custom hat" );
			obj->name = STRALLOC ( buf );
	            sprintf( buf, "home made hat" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf,  "A reasonable looking home-made hat has been left here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "head" ) );
			affectloc = APPLY_INT;
			break;
		case 3:
			sprintf( buf, "custom pants" );
			obj->name = STRALLOC ( buf );
			sprintf( buf,  "generic pants" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A pair of pants lie here.  They look pretty ordinary." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "legs" ) );
			affectloc = APPLY_DEX;
			break;
		case 4:
			sprintf ( buf, "custom shirt" );
			obj->name = STRALLOC ( buf );
			sprintf ( buf, "organic shirt" );
			obj->short_descr = STRALLOC ( buf );
			sprintf ( buf, "A shirt has been made and dropped here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
			affectloc = APPLY_CON;
			break;
		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 7 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 3 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -80, -30 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 7 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 3 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 30, 80 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}

			sprintf( buf, "shirt pants hat gloves" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}

	if ( !str_cmp( arg1, "Cloak" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = 0.5 * number_range( get_curr_lck( ch ) / 12 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 8 );
		obj->value[1]  = obj->value[0];
		switch ( number_range( 1, 2 ) )
		{
		case 1:
            sprintf( buf, "nymph cloak" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "nymph cloak" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A nice, purpley-pink cloak lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "about" ) );
			affectloc = APPLY_CHA;
			break;
		case 2:
			sprintf( buf, "rabbit cloak" );
			obj->name = STRALLOC ( buf );
			sprintf( buf,"rabbit skin cloak" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A reasonable looking cloak, made of rabbit fur, has been left here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "about" ) );
			affectloc = APPLY_LCK;
			break;
		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 8 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 5 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -60, -40 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			obj->value[0]  = number_range( get_curr_lck( ch ) / 8 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 5 );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 40, 60 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
			sprintf( buf, "cloak nymph rabbit" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}

	if ( !str_cmp( arg1, "Toy" ) )
	{
		obj->item_type = get_otype( "treasure" );
		switch ( number_range( 1, 5 ) )
		{
		case 1:
			sprintf( buf, "Chichi plushie" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "Chichi plushie" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A plushie of Chichi, one of the cutest gods in the universe, lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
			affectloc = APPLY_CON;
			break;
		case 2:
			sprintf( buf, "Mai plushie" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "Mai plushie" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A plushie of Mai, one of the cutest gods in the universe, lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
			affectloc = APPLY_CHA;
			break;
		case 3:
			sprintf( buf, "Videl plushie" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "Videl plushie" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A plushie of Videl, one of the cutest gods in the universe, lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
			affectloc = APPLY_WIS;
			break;
                case 4:
                        sprintf( buf, "Aisha plushie" );
                        obj->name = STRALLOC ( buf );
                        sprintf( buf, "Aisha plushie" );
                        obj->short_descr = STRALLOC ( buf );
                        sprintf( buf, "A plushie of Aisha, one of the cutest gods in the universe, lies here." );
                        obj->description = STRALLOC ( buf );
                        TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
                        affectloc = APPLY_STR;
                        break;
                case 5:
                        sprintf( buf, "Bra plushie" );
                        obj->name = STRALLOC ( buf );
                        sprintf( buf, "Bra plushie" );
                        obj->short_descr = STRALLOC ( buf );
                        sprintf( buf, "A plushie of Bra, one of the cutest gods in the universe, lies here." );
                        obj->description = STRALLOC ( buf );
                        TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
                        affectloc = APPLY_CHA;
                        break;

		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -60, -30 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 10, 20 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
			sprintf( buf, "Mai Chichi Videl Plushie" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}
  }
  else if ( material->item_type == ITEM_TRASH )
  {
	if ( !str_cmp( arg1, "Armor" )  || !str_cmp( arg1, "Armour" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = number_range( ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 5 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) * 2 );
		obj->value[1]  = obj->value[0];

		switch ( number_range( 1, 3 ) )
		{
		case 1:
			sprintf( buf, "Metalsilver Armour Armor" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "&wMetalsiver Armour&W" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A set of shiny silver armour lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
			affectloc = APPLY_DEX;
			break;
		case 2:
			sprintf( buf, "Raregold Armour Armor" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "&YRaregold Armour&W" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A set of shiny golden armour lies here.." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
			affectloc = APPLY_STR;
			break;
		case 3:
			sprintf( buf, "Heavyemerald Armour Armor" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "&GHeavyemerald Armour&W" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A set of shiny green armour lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
			affectloc = APPLY_CON;
			break;

		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -100, -50 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 50, 100 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
			sprintf( buf, "armour armor metalsiver raregold heavyemerald" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}

	if ( !str_cmp( arg1, "Weapon" ) )
	{
		obj->item_type = get_otype( "weapon" );
		obj->value[0] = 100;
		obj->value[1] = number_range( 3, 9 );
		obj->value[2] = obj->value[1] * number_range( 2, 12 );
		obj->value[3] = 3;
		switch ( number_range( 1, 3 ) )
		{
		case 1:
			sprintf( buf, "Murasame Copy" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "&ra copy of the Murasame&W" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A long, shiny blade lies here, embeded in the ground partially." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "wield" ) );
			affectloc = APPLY_LCK;
			break;
		case 2:
			sprintf( buf, "Fission Blade" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "&Ca fission blade&W" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A light blue weapon lies here, occassionally flashing with darker blue." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "wield" ) );
			affectloc = APPLY_STR;
			break;
		case 3:
			sprintf( buf, "Sword" );
			obj->name = STRALLOC ( buf );
			sprintf( buf, "a standard sword" );
			obj->short_descr = STRALLOC ( buf );
			sprintf( buf, "A decent length longsword lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "wield" ) );
			affectloc = APPLY_WIS;
			break;
		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -60, -40 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 40, 60 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
			sprintf( buf, "Murasame sword blade fission" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}
	if ( !str_cmp( arg1, "Shield" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0] = number_range( get_curr_lck( ch ), get_curr_lck( ch ) + get_curr_int( ch ) );
		obj->value[1] = obj->value[0];
		switch ( number_range( 1, 2 ) )
		{
		case 1:
            sprintf( buf, "Riot Shield" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "a riot shield" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A tall, full length riot shield lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "shield" ) );
			affectloc = APPLY_CON;
			break;
		case 2:
            sprintf( buf, "Circle Shield" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "a circle shield" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A small circle shield lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "shield" ) );
			affectloc = APPLY_DEX;
			break;
		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -50, -20 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 40, 90 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
            sprintf( buf, "Circle Riot Shield" );
			obj->name = STRALLOC ( buf );
		done = TRUE;
	}
	if ( !str_cmp( arg1, "Jewellery" ) )
	{
		obj->item_type = get_otype( "treasure" );
		switch ( number_range( 1, 3 ) )
		{
		case 1:
            sprintf( buf, "pretty necklace" );
			obj->name = STRALLOC ( buf );
            sprintf( buf,"&Pa pretty necklace&W" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A pretty gold necklace lies here, embeded with pink gems." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "neck" ) );
			affectloc = APPLY_CHA;
			break;
		case 2:
            sprintf( buf, "sparkly ring" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "&Ya sparkly ring&W" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A small, golden ring lies here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "finger" ) );
			affectloc = APPLY_CHA;
			break;
		case 3:
            sprintf( buf, "shiny earrings" );
			obj->name = STRALLOC ( buf );
            sprintf( buf, "&Gshiny earrings&W" );
			obj->short_descr = STRALLOC ( buf );
            sprintf( buf, "A pair of earrings studded with emeralds lie here." );
			obj->description = STRALLOC ( buf );
			TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "ears" ) );
			affectloc = APPLY_CHA;
			break;
		}


// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -60, -20 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 50, 100 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}  
	   	        sprintf( buf, "ring earrings necklace" );
			obj->name = STRALLOC ( buf );

		done = TRUE;
	}
  }
  else if ( material->item_type == ITEM_TREASURE )
  {
	if ( !str_cmp( arg1, "Power" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = number_range( ( get_curr_lck( ch ) + get_curr_int( ch ) ) / 2 , ( get_curr_lck( ch ) + get_curr_int( ch ) ) );
		obj->value[0] *= 0.75;
		obj->value[1]  = obj->value[0];
		sprintf( buf, "&CPower &cArmour&W" );
		obj->short_descr = STRALLOC ( buf );
		sprintf( buf, "Some fantabulous looking sparkly blue armour lies here." );
		obj->description = STRALLOC ( buf );
		TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
		affectloc = APPLY_CON;

// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -100, -50 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = APPLY_STR;
			paf->modifier = number_range( -100, -50 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 75, 125 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = APPLY_STR;
			paf->modifier = number_range( 50, 100 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
		sprintf( buf, "Power Armour" );
		obj->name = STRALLOC ( buf );
		done = TRUE;
	}
	if ( !str_cmp( arg1, "Training" ) )
	{
		obj->item_type = get_otype( "armor" );
		obj->value[0]  = number_range( 100, 500 );
		obj->value[1]  = obj->value[0];
		sprintf( buf, "some weighted training gear" );
		obj->short_descr = STRALLOC ( buf );
		sprintf( buf, "Some training gear has been left here." );
		obj->description = STRALLOC ( buf );
		TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hold" ) );
		affectloc = APPLY_PLMOD;

// Special Rare items - Bad and Good.

		if ( chance <= 2 )
		{
			send_to_char( "&cYou sense something TERRIFYING about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( -100, -5000 );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &R(HAUNTED!)&W" );
			obj->cost = obj->cost * 100;
		}
		if ( chance >= 99 )
		{
			send_to_char( "&cYou sense something SPECIAL about this creation...\n\r", ch );
			xTOGGLE_BIT( obj->extra_flags, get_oflag( "loyal" ) );
			CREATE(paf, AFFECT_DATA, 1);
			paf->type = -1;
			paf->duration = -1;
			paf->location = affectloc;
			paf->modifier = number_range( 200, 200 + ch->perm_int );
			xCLEAR_BITS(paf->bitvector);
			LINK(paf, obj->first_affect, obj->last_affect, next, prev);
			strcat( obj->short_descr, " &P(SHIMMERING!)&W" );
			obj->cost = obj->cost * 100;
		}
		sprintf( buf, "Training Gear" );
		obj->name = STRALLOC ( buf );
		done = TRUE;
	}
  }

  if ( !done )
  {
	send_to_char( "You can't make that from that.\n\r", ch );
	return;
  }
  obj->value[0] = obj->value[0] / 5;
  obj->value[1] = obj->value[1] / 5;
//  pager_printf( ch, "You use up all copies of %s in your inventory.\n\r", material->short_descr );
  send_to_char( "&CYou make a new item!\n\r", ch );
  obj_to_char( obj, ch );
  separate_obj( material );
  obj_from_char( material );
  extract_obj( material );
//  save_char_obj( ch );
  pager_printf( ch, "&WYou have created %s!\n\r", obj->short_descr );
  return;
}
*/

/*
  Adding wear flags

  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "" ) );
 */
/*
  Adding flags

  xTOGGLE_BIT(obj->extra_flags, get_oflag( "" ) );
 */
/*
  Addding Affects

  CREATE(paf, AFFECT_DATA, 1);
  paf->type = -1;
  paf->duration = -1;
  paf->location = APPLY_;
  paf->modifier = Amt;
  xCLEAR_BITS(paf->bitvector);
  LINK(paf, obj->first_affect, obj->last_affect, next, prev);

 */

void do_fish(CHAR_DATA *ch, char *argument)
{
	char arg[MAX_INPUT_LENGTH];
	int timer = number_range(50, 70);
	int size = number_range(310, 330);
	unsigned long long bonus;
	OBJ_DATA *bait = NULL;
	OBJ_DATA *fish = NULL;

	if (size > 305)
		size = number_range(310, 330);
	if (size > 310)
		size = number_range(310, 330);
	if (size > 315)
		size = number_range(310, 330);

	if ((who_fighting(ch)) != NULL)
	{
		send_to_char("In the middle of a fight?!  What the hell?!\n\r", ch);
		return;
	}

	if (ch->in_room->sector_type != SECT_WATER_SWIM)
	{
		send_to_char("&WLook for an area marked '&C~&W' on your map to fish.\n\r", ch);
		return;
	}

	if (!is_wearing(ch, 305))
	{
		send_to_char("You don't have a fishing rod.\n\r", ch);
		return;
	}

	switch (ch->substate)
	{
	default:
		for (bait = ch->first_carrying; bait; bait = bait->next_content)
		{
			if (bait->pIndexData->vnum == 45)
				break;
		}
		if (!bait /*|| strstr( bait->short_descr, "firefly" )|| strstr( "firefly", bait->name ) */ || bait->pIndexData->vnum != 45)
		{
			send_to_char("You have no worms to use as bait.\n\r", ch);
			return;
		}
		separate_obj(bait);
		obj_from_char(bait);
		extract_obj(bait);
		send_to_char("&YYou thread the worm onto the fishing hook.\n\r", ch);
		ch->skill_timer = timer;
		add_timer(ch, TIMER_DO_FUN, timer, do_fish, 1);
		send_to_char("&YNext you settle down and begin fishing, casting your line into the water.\n\r", ch);
		act(AT_MAGIC, "$n settles down and starts fishing, casting $s line into the water.", ch, NULL, NULL, TO_CANSEE);
		xSET_BIT(ch->affected_by, AFF_FISHING);
		ch->alloc_ptr = str_dup(arg);
		return;
	case 1:
		if (!ch->alloc_ptr)
		{
			send_to_char("&GYour fishing was interrupted!\n\r", ch);
			bug("do_fish: alloc_ptr NULL", 0);
			xREMOVE_BIT(ch->affected_by, AFF_FISHING);
			return;
		}
		strcpy(arg, ch->alloc_ptr);
		DISPOSE(ch->alloc_ptr);
		break;
	case SUB_TIMER_DO_ABORT:
		DISPOSE(ch->alloc_ptr);
		ch->substate = SUB_NONE;
		send_to_char("You stop fishing, your movements scaring away the fish.  You should stay perfectly still to fish in future.\n\r", ch);
		act(AT_GREEN, "$n's stops fishing.\n\r", ch, NULL, NULL, TO_CANSEE);
		xREMOVE_BIT(ch->affected_by, AFF_FISHING);
		ch->skill_timer = 0;
		return;
	}

	ch->substate = SUB_NONE;
	xREMOVE_BIT(ch->affected_by, AFF_FISHING);
	if (can_use_skill(ch, number_percent(), gsn_fish))
	{
		learn_from_success(ch, gsn_fish);
		fish = create_object(get_obj_index(size), 100);
		fish->timer = 40;
		bonus = (ch->basepl * (((size - 309) * 0.02) / 150));
		bonus /= 4;
		obj_to_char(fish, ch);
		ch_printf(ch, "You yank the line out of the water, and find %s on the end of your hook!\n\r", fish->short_descr);
		act(AT_MAGIC, "$n pulls a fish out of the water and looks pleased with $mself", ch, NULL, NULL, TO_CANSEE);
		ch_printf(ch, "You gain %s powerlevel from your successful fish.\n\r", format_pl(bonus));
		ch->basepl += bonus;
		update_current(ch, NULL);
		do_stand(ch, "");
	}
	else
	{
		learn_from_failure(ch, gsn_fish);
		fish = create_object(get_obj_index(304), 100);
		fish->timer = 20;
		obj_to_char(fish, ch);
		pager_printf(ch, "You yank the line out of the water, and find %s on the end of your hook!\n\r", fish->short_descr);
		act(AT_MAGIC, "$n pulls a boot out of the water, and looks somewhat irritated.", ch, NULL, NULL, TO_CANSEE);
		learn_from_failure(ch, gsn_fish);
	}
	return;
}

/* Learn for Bioandroids  -Zeno */
void do_learn(CHAR_DATA *ch, char *argument)
{
	int sn;
	char arg[MAX_INPUT_LENGTH];

	if (ch->race != 6)
	{
		send_to_char("Huh?\n\r", ch);
		return;
	}

	argument = one_argument(argument, arg);

	if (arg[0] == '\0')
	{
		send_to_char("Focus on learning what skill?\n\r", ch);
		return;
	}

	if (!str_cmp(arg, "none"))
	{
		send_to_char("You stop focusing on the skill.\n\r", ch);
		ch->pcdata->learnsn = 0;
		return;
	}

	sn = skill_lookup(arg);
	if (skill_table[sn] == NULL || skill_table[sn]->race_adept[ch->race] == 0)
	{
		send_to_char("No such skill exists.\n\r", ch);
		return;
	}

	ch_printf(ch, "You focus your thoughts on learning %s.\n\r", skill_table[sn]->name);
	ch->pcdata->learnsn = sn;

	return;
}

/* Refer other players  -Zeno */
void do_refer(CHAR_DATA *ch, char *argument)
{
	char arg[MAX_INPUT_LENGTH];
	CHAR_DATA *victim;
	char buf[MAX_STRING_LENGTH];

	one_argument(argument, arg);

	if (arg[0] == '\0')
	{
		send_to_char("Refer who as the person who bought you here?\n\r", ch);
		return;
	}
	if ((victim = get_char_world(ch, arg)) == NULL)
	{
		send_to_char("They aren't here.\n\r", ch);
		return;
	}
	if (IS_NPC(victim))
	{
		send_to_char("Not on NPC's.\n\r", ch);
		return;
	}
	if (!str_cmp(ch->desc->host, victim->desc->host))
	{
		send_to_char("Nice try, you multiplaying wanker.\n\r", ch);
		return;
	}
	if (xIS_SET(ch->act, PLR_REFERRED))
	{
		send_to_char("You have already referred someone.\n\r", ch);
		return;
	}
	if (ch->played / 3600 < 24)
	{
		send_to_char("After 24 hours of play time, maybe.\n\r", ch);
		return;
	}
	sprintf(buf, "%sspawn", victim->name);
	ch->pcdata->rank = str_dup(buf);
	ch_printf(ch, "You indicate %s as the person who brought you here.\n\r", victim->name);
	xSET_BIT(ch->act, PLR_REFERRED);
	ch->gold += 500;
	victim->basepl *= 1.005;
	update_current(victim, NULL);
	victim->gold += 10000;
	victim->refers++;
	adjust_hiscore("refers", victim, victim->refers);
	ch_printf(victim, "%s has indicated that you are the reason %s now plays.  Kudos.\n\r", ch->name, HESHE(ch->sex));
	return;
}

void do_energy(CHAR_DATA *ch, char *argument)
{
	int NewValue = 1;

	if (IS_NPC(ch))
	{
		send_to_char("Go away, pesky NPC...\n\r", ch);
		return;
	}
	if (ch->race == RACE_ANDROID)
	{
		send_to_char("Maybe if you reconfigured your BIOS...\n\r", ch);
		return;
	}
	if (!argument || argument == NULL || !is_number(argument))
	{
		ch_printf(ch, "Your attacks currently use %s%% energy.\n\r", format_pl(ch->energy));
		return;
	}

	if ((NewValue = atoi(argument)) < 50)
	{
		send_to_char("You must specify a value over 50.\n\r", ch);
		return;
	}

	if (IS_SET(ch->pcdata->flags, PCFLAG_MANIAC))
	{
		if (NewValue > (300))
		{
			send_to_char("Specified value exceeded maximum of three hundred.  Fixing...\n\r", ch);
			NewValue = (300);
		}
	}
	else
	{
		if (NewValue > (150))
		{
			send_to_char("Specified value exceeded maximum of one hundred and fifty.  Fixing...\n\r", ch);
			NewValue = (150);
		}
	}

	ch->energy = NewValue;
	ch_printf(ch, "Your attacks will now use %s%% energy\n\r", format_pl(NewValue));
}

void do_fix(CHAR_DATA *ch, char *argument)
{
	OBJ_DATA *obj;
	OBJ_DATA *tools;
	int int_bonus = number_range(0, 3);

	obj = get_obj_carry(ch, argument);

	if (ch->position != POS_STANDING)
	{
		ch_printf(ch, "You have to be standing to fix items.\n\r");
		return;
	}

	if (!str_cmp(argument, "all"))
	{
		ch_printf(ch, "Only a qualified store can repair all your items at once.\n\r");
		return;
	}

	if (!obj || obj == NULL || !obj->name || !obj->short_descr)
	{
		ch_printf(ch, "You can't find %s about you to repair it.\n\r", argument);
		return;
	}

	for (tools = ch->first_carrying; tools; tools = tools->next_content)
	{
		if (tools->item_type == ITEM_MATERIAL && !str_cmp(Materials[tools->value[1]], "tool") && tools->pIndexData->vnum != 4360)
			break;
	}

	if (!tools || tools->item_type != ITEM_MATERIAL || str_cmp(Materials[tools->value[1]], "tool") || tools->pIndexData->vnum == 4360)
	{
		send_to_char("You have no tools to repair your items with!\n\r", ch);
		return;
	}

	/*
   if ( !can_drop_obj( ch, obj ) )
   {
	ch_printf( ch, "You can't let go of %s long enough to repair it!\n\r", obj->short_descr );
	return;
   }
*/

	separate_obj(tools);
	tools->value[0]--;
	ch_printf(ch, "You use your %s, and ding it up some.\n\r", tools->short_descr);
	if (tools->value[0] < 1 || tools->value[0] > 100)
	{
		ch_printf(ch, "Your %s is exhausted, and disintegrates in your hands.\n\r", tools->short_descr);
		obj_from_char(tools);
		extract_obj(tools);
	}

	switch (obj->item_type)
	{
	default:
		ch_printf(ch, "You hammer at %s, but it just looks the same...\n\r", obj->short_descr);
		break;
	case ITEM_ARMOR:
		if (obj->value[0] == obj->value[1])
		{
			ch_printf(ch, "You hammer at %s, but it just looks the same...\n\r", obj->short_descr);
			return;
		}
		break;
	case ITEM_WEAPON:
		if (obj->value[0] == obj->pIndexData->value[0])
		{
			ch_printf(ch, "You hammer at %s, but it just looks the same...\n\r", obj->short_descr);
			return;
		}
		break;
	}

	if (!can_use_skill(ch, number_percent(), gsn_fix))
	{
		learn_from_failure(ch, gsn_fix);
		switch (obj->item_type)
		{
		default:
			ch_printf(ch, "You hammer at %s, but it just looks the same...\n\r", obj->short_descr);
			break;
		case ITEM_ARMOR:
			obj->value[0] = 1;
			ch_printf(ch, "You hammer at %s, and bend it right out of shape.  Whoops...\n\r", obj->short_descr);
			break;
		case ITEM_WEAPON:
			obj->value[0] = 1;
			ch_printf(ch, "You hammer at %s, and put a dirty great crack down the middle of it.  Whoops...\n\r", obj->short_descr);
			break;
		}
		return;
	}

	learn_from_success(ch, gsn_fix);
	switch (obj->item_type)
	{
	default:
		ch_printf(ch, "You hammer at %s, but it just looks the same...\n\r", obj->short_descr);
		break;
	case ITEM_ARMOR:
		ch_printf(ch, "You hammer at %s, and fix it a bit.\n\r", obj->short_descr);
		obj->value[0] += 1 + ((obj->value[1] / 100) * ch->pcdata->learned[gsn_fix]);
		if (obj->value[0] >= obj->value[1])
		{
			obj->value[0] = obj->value[1];
			ch_printf(ch, "Your %s is now fully repaired.\n\r", obj->short_descr);
		}
		oprog_repair_trigger(ch, obj);
		break;
	case ITEM_WEAPON:
		ch_printf(ch, "You hammer at %s, and fix it a bit.\n\r", obj->short_descr);
		obj->value[0] += 1 + ((obj->pIndexData->value[0] / 100) * ch->pcdata->learned[gsn_fix]);
		if (obj->value[0] >= obj->pIndexData->value[0] && int_bonus > 0)
		{
			obj->value[0] = obj->pIndexData->value[0];
			ch_printf(ch, "Your %s is now fully repaired.\n\r", obj->short_descr);
		}
		oprog_repair_trigger(ch, obj);
		break;
	}
}

/*
void do_forge( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *obj;
    char arg[MAX_STRING_LENGTH];

    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
	send_to_char( "Forge New Item\n\r", ch );
	send_to_char( "Name: \n\r", ch );
	send_to_char( "Type: \n\r", ch );
	send_to_char( "Description: \n\r", ch );
	send_to_char( "Type ? for help or 'done' to make the item.\n\r", ch );
	ch->desc->connected = CON_FORGE_NEW;
    }

    else if ( (obj = get_obj_carry( ch, arg )) != NULL && ( obj->item_type == ITEM_ARMOR || obj->item_type == ITEM_WEAPON ) )
    {
*/

void do_plgained(CHAR_DATA *ch, char *argument)
{
	if (!IS_NPC(ch))
	{
		pager_printf(ch, "&cGained today: &C%s", format_pl(ch->pcdata->pl_today));
		pager_printf(ch, "\n\r");
	}
}
