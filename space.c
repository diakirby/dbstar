/************************************************************************
 * This code is written for any Smaug based mud.  due to the isolated   *
 * nature of the way this code has been written, you will find it very  *
 * simple to install and, if necessary, remove.  All instructions are   *
 * included in the files space.doc                                      *
 ************************************************************************
 * Information:                                                         *
 * This is a fully contained space and ship module for a DBZ mud.  You  *
 * will notice that at present there is no supportfor space combat, but *
 * there are no plans to add this.  The nature of a DBZ mud means that  *
 * destruction of a ship simply results in the loss of money, and that's*
 * just plain annoying :\             - Yami,   Owner of DBStar         *
 ************************************************************************/

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"

void launch_ship( SHIP_DATA *onship );
void buses();
void space_messages( SHIP_DATA *ship );


/* local routines */

PLANET_DATA *get_planet( char *name )
{
PLANET_DATA *planet;
    
/*if ( name[0] == '\0' )
 return NULL;*/
    
for ( planet = first_planet; planet; planet = planet->next )
     {
     if ( !str_cmp( name, planet->name )
     || nifty_is_name( name, planet->name )
     || !str_prefix( name, planet->name )
     || nifty_is_name_prefix( name, planet->name ) )
          return planet;
     }

return NULL;
}

SHIP_DATA *get_ship( char *name )
{
SHIP_DATA *ship;
    
for ( ship = first_ship; ship; ship = ship->next )
     {
     if ( !str_cmp( name, ship->name )
     || nifty_is_name( name, ship->name )
     || !str_prefix( name, ship->name )
     || nifty_is_name_prefix( name, ship->name ) 
     || ship->name == name )
          return ship;
     }

return NULL;
}

void write_planet_list()
{
    PLANET_DATA *tplanet;
    FILE *fpout;
    char filename[256];

    sprintf( filename, "%s%s", PLANET_DIR, PLANET_LIST );
    fpout = Fopen( filename, "w" );
    if ( !fpout )
    {
	   bug( "ERROR: Planet.lst either not found or busted!  Cannot write!\n\r", 0 );
       return;
    }	  
    for ( tplanet = first_planet; tplanet; tplanet = tplanet->next )
	fprintf( fpout, "%s\n", tplanet->filename );
    fprintf( fpout, "$\n" );
    Fclose( fpout );
}

void save_planet( PLANET_DATA *planet )
{
    FILE *fp;
    char filename[256];
    char buf[MAX_STRING_LENGTH];

    if ( !planet )
    {
	bug( "SAVE_PLANET: No planet found!", 0 );
	return;
    }
        
    if ( !planet->filename || planet->filename[0] == '\0' )
    {
	sprintf( buf, "SAVE_PLANET: %s has no filename", planet->name );
	bug( buf, 0 );
	return;
    }
 
    sprintf( filename, "%s%s", PLANET_DIR, planet->filename );
    
    Fclose( fpReserve );
    if ( ( fp = Fopen( filename, "w" ) ) == NULL )
    {
    	bug( "save_planet: Fopen", 0 );
    	perror( filename );
    }
    else
    {
//        AREA_DATA *pArea;       
	fprintf( fp, "#PLANET\n" );
	fprintf( fp, "Filename     %s~\n",	planet->filename        );
 	fprintf( fp, "Name         %s~\n",	planet->name            );
	fprintf( fp, "Guardian     %s~\n",	planet->guardian        );
 	fprintf( fp, "Hero         %s~\n",	planet->hero            );
        fprintf( fp, "Area1         %s~\n",	planet->area1  );
        fprintf( fp, "Area2         %s~\n",	planet->area2  );
        fprintf( fp, "Area3         %s~\n",	planet->area3  );
        fprintf( fp, "Area4         %s~\n",	planet->area4  );
        fprintf( fp, "Area5         %s~\n",	planet->area5  );
        fprintf( fp, "Area6         %s~\n",	planet->area6  );
        fprintf( fp, "Area7         %s~\n",	planet->area7  );
        fprintf( fp, "Area8         %s~\n",	planet->area8  );
        fprintf( fp, "Area9         %s~\n",	planet->area9  );
        fprintf( fp, "Area10         %s~\n",	planet->area10  );
        fprintf( fp, "Area11         %s~\n",	planet->area11  );
        fprintf( fp, "Area12         %s~\n",	planet->area12  );
        fprintf( fp, "Area13         %s~\n",	planet->area13  );
        fprintf( fp, "Area14         %s~\n",	planet->area14  );
        fprintf( fp, "Area15         %s~\n",	planet->area15  );
 	fprintf( fp, "X            %d\n",	planet->x               );
	fprintf( fp, "Y            %d\n",	planet->y               );
	fprintf( fp, "Z            %d\n",	planet->z               );
	fprintf( fp, "Dock            %d\n",	planet->dock               );
	fprintf( fp, "Economy      %d\n",	planet->economy         );
	fprintf( fp, "Gravity      %d\n",	planet->gravity         );
	fprintf( fp, "End\n\n"						);
	fprintf( fp, "#END\n"						);
    }
    Fclose( fp );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}

#if defined(KEY)
#undef KEY
#endif

	#define KEY( literal, field, value )				\
				if ( !str_cmp( word, literal ) )	\
				{					                \
				    field  = value;			        \
				    fMatch = TRUE;			        \
				    break;				            \
				}

void fread_planet( PLANET_DATA *planet, FILE *fp )
{
    char buf[MAX_STRING_LENGTH];
    char *word;
    bool fMatch;

    for ( ; ; )
    {
	word   = feof( fp ) ? "End" : fread_word( fp );
	fMatch = FALSE;

	switch ( UPPER(word[0]) )
	{
	case '*':
	    fMatch = TRUE;
	    fread_to_eol( fp );
	    break;

	case 'A':
    KEY( "Area1",	        planet->area1,	fread_string( fp ) );
    KEY( "Area2",	        planet->area2,	fread_string( fp ) );
    KEY( "Area3",	        planet->area3,	fread_string( fp ) );
    KEY( "Area4",	        planet->area4,	fread_string( fp ) );
    KEY( "Area5",	        planet->area5,	fread_string( fp ) );
    KEY( "Area6",	        planet->area6,	fread_string( fp ) );
    KEY( "Area7",	        planet->area7,	fread_string( fp ) );
    KEY( "Area8",	        planet->area8,	fread_string( fp ) );
    KEY( "Area9",	        planet->area9,	fread_string( fp ) );
    KEY( "Area10",	        planet->area10,	fread_string( fp ) );
    KEY( "Area11",	        planet->area10,	fread_string( fp ) );
    KEY( "Area12",	        planet->area10,	fread_string( fp ) );
    KEY( "Area13",	        planet->area10,	fread_string( fp ) );
    KEY( "Area14",	        planet->area10,	fread_string( fp ) );
    KEY( "Area15",	        planet->area10,	fread_string( fp ) );
	
	    break;

	case 'D':
    KEY( "Dock",	        planet->dock,	fread_number( fp ) );
	break;

	case 'E':
    KEY( "Economy",	        planet->economy,	fread_number( fp ) );
	    if ( !str_cmp( word, "End" ) )
	    {
		if (!planet->name)
		  planet->name		= STRALLOC( "" );
		return;
	    }
	    break;

	case 'F':
	    KEY( "Filename",	planet->filename,	fread_string_nohash( fp ) );
	    break;
	
	case 'G':
	    KEY( "Guardian",	planet->guardian,	fread_string_nohash( fp ) );
	    KEY( "Gravity",	    planet->gravity,	fread_number( fp ) );
	    break;

	case 'H':
	    KEY( "Hero",	    planet->hero,		fread_string_nohash( fp ) );
	    break;
	
	case 'N':
	    KEY( "Name",	    planet->name,		fread_string( fp ) );
	    break;
	
	case 'X':
	    KEY( "X",	planet->x,		fread_number( fp ) );
	    break;

	case 'Y':
	    KEY( "Y",	planet->y,		fread_number( fp ) );
	    break;

	case 'Z':
	    KEY( "Z",	planet->z,		fread_number( fp ) );
	    break;
    
	}
	
	if ( !fMatch )
	{
	    sprintf( buf, "Fread_planet: no match: %s", word );
	    bug( buf, 0 );
	}
	
    }
}

/*
 *  Creates an empty instance of a planet and loads data into it
 */
bool load_planet_file( char *planetfile )
{
    char filename[256];
    PLANET_DATA *planet;
    FILE *fp;
    bool found;

    CREATE( planet, PLANET_DATA, 1 );
    
    planet->next_in_system = NULL;
    planet->last_in_system = NULL;
    planet->economy = 100;
    planet->guardian = NULL;
    planet->gravity = 10;
    planet->area1 = "";
    planet->area2 = "";
    planet->area3 = "";
    planet->area4 = "";
    planet->area5 = "";
    planet->area6 = "";
    planet->area7 = "";
    planet->area8 = "";
    planet->area9 = "";
    planet->area10 = "";
    planet->area11 = "";
    planet->area12 = "";
    planet->area13 = "";
    planet->area14 = "";
    planet->area15 = "";
    planet->x = 0;
    planet->y = 0;
    planet->z = 0;

    found = FALSE;
    sprintf( filename, "%s%s", PLANET_DIR, planetfile );

    if ( ( fp = Fopen( filename, "r" ) ) != NULL )
    {

	found = TRUE;
	for ( ; ; )
	{
	    char letter;
	    char *word;

	    letter = fread_letter( fp );
	    if ( letter == '*' )
	    {
		fread_to_eol( fp );
		continue;
	    }

	    if ( letter != '#' )
	    {
		bug( "Load_planet_file: # not found.", 0 );
		break;
	    }

	    word = fread_word( fp );
	    if ( !str_cmp( word, "PLANET"	) )
	    {
	    	fread_planet( planet, fp );
	    	break;
	    }
	    else
	    if ( !str_cmp( word, "END"	) )
	        break;
	    else
	    {
		char buf[MAX_STRING_LENGTH];

		sprintf( buf, "Load_planet_file: bad section: %s.", word );
		bug( buf, 0 );
		break;
	    }
	}
	Fclose( fp );
    }

    if ( !found )
      DISPOSE( planet );
    else
      LINK( planet, first_planet, last_planet, next, prev );

    return found;
}

/*
 * Loads all the planets in the game.
 */

void load_planets( )
{
    FILE *fpList;
    char *filename;
    char planetlist[256];
    char buf[MAX_STRING_LENGTH];
    
    first_planet	= NULL;
    last_planet	= NULL;

    log_string( "Loading planets..." );

    sprintf( planetlist, "%s%s", PLANET_DIR, PLANET_LIST );
    Fclose( fpReserve );
    if ( ( fpList = Fopen( planetlist, "r" ) ) == NULL )
    {
	perror( planetlist );
	exit( 1 );
    }

    for ( ; ; )
    {
	filename = feof( fpList ) ? "$" : fread_word( fpList );
	log_string( filename );
	if ( filename[0] == '$' )
	  break;

	if ( !load_planet_file( filename ) )
	{
	  sprintf( buf, "Cannot load planet file: %s", filename );
	  bug( buf, 0 );
	}
    }
    Fclose( fpList );
    log_string(" Done planets " );  
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}

void do_setplanet( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    PLANET_DATA *planet;
    AREA_DATA *area;

    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' )
    {
	send_to_char( "Usage: setplanet <planet> <field> [value]\n\r", ch );
	send_to_char( "\n\rField being one of:\n\r", ch );
	send_to_char( " name filename area1-15 guardian", ch );
	send_to_char( " economy gravity x y z dock\n\r", ch );
	return;
    }

    planet = get_planet( arg1 );
    if ( !planet )
    {
	send_to_char( "No such planet.\n\r", ch );
	return;
    }


    if ( !strcmp( arg2, "name" ) )
    {
	STRFREE( planet->name );
	planet->name = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	return;
    }

    if ( !strcmp( arg2, "guardian" ) )
    {
	STRFREE( planet->guardian );
	planet->guardian = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	return;
    }

    if ( !strcmp( arg2, "economy" ) )
    {
	planet->economy = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	return;
    }

    if ( !strcmp( arg2, "gravity" ) )
    {
	planet->gravity = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	return;
    }

    if ( !strcmp( arg2, "dock" ) )
    {
	planet->dock = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	return;
    }

    if ( !strcmp( arg2, "area1"))
	{
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area1 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
       	   save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
       	 return;
    }    
    if ( !strcmp( arg2, "area2"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area2 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area3"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area3 = area->name;
     	   area->planet = planet->name;
	   LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
       }

    if ( !strcmp( arg2, "area4"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area4 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area5"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area5 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area6"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area6 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
	}
    if ( !strcmp( arg2, "area7"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area7 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area8"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area8 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area9"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area9 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area10"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area10 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area11"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area11 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area12"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area12 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area13"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area13 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area14"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area14 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }
    if ( !strcmp( arg2, "area15"))
        {
        if (get_area(argument))
        {
           area = get_area( argument );
	   planet->area15 = area->name;
	   area->planet = planet->name;
           LINK( area , planet->first_area, planet->last_area, next_on_planet, prev_on_planet );
           send_to_char( "Done.\n\r", ch );
           save_planet( planet );
        }
        else
        {
           send_to_char( "Area not found\n\r", ch );
        }
         return;
    }


    if ( !strcmp( arg2, "x" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
    planet->x = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	write_planet_list( );
	return;

    }

    if ( !strcmp( arg2, "y" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
    planet->y = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	write_planet_list( );
	return;

    }

    if ( !strcmp( arg2, "z" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
    planet->z = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	write_planet_list( );
	return;

    }

    
    if ( !strcmp( arg2, "filename" ) )
    {
	DISPOSE( planet->filename );
	planet->filename = str_dup( argument );
	send_to_char( "Done.\n\r", ch );
	save_planet( planet );
	write_planet_list( );
	return;
    }

    do_setplanet( ch, "" );
    return;
}

void do_showplanet( CHAR_DATA *ch, char *argument )
{   
    PLANET_DATA *planet;
//    AREA_DATA *pArea;
//    char area[MAX_STRING_LENGTH];

    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    if ( argument[0] == '\0' )
    {
	send_to_char( "Usage: showplanet <planet>\n\r", ch );
	return;
    }

    planet = get_planet( argument );
    if ( !planet )
    {
	send_to_char( "No such planet.\n\r", ch );
	return;
    }

    pager_printf( ch, "&W%s\n\r", planet->name);
    if ( IS_IMMORTAL(ch) )
	pager_printf( ch, "&WFilename:      &G%s\n\r", planet->filename);
    pager_printf( ch, "%s | %s | %s | %s | %s\n\r %s | %s | %s | %s | %s\n\r  %s | %s | %s | %s | %s", 
planet->area1 == NULL ? "" : planet->area1, 
planet->area2 == NULL ? "" : planet->area2, 
planet->area3 == NULL ? "" : planet->area3, 
planet->area4 == NULL ? "" : planet->area4, 
planet->area5 == NULL ? "" : planet->area5, 
planet->area6 == NULL ? "" : planet->area6,
planet->area7 == NULL ? "" : planet->area7,
planet->area8 == NULL ? "" : planet->area8,
planet->area9 == NULL ? "" : planet->area9,
planet->area10 == NULL ? "" : planet->area10,
planet->area11 == NULL ? "" : planet->area11,
planet->area12 == NULL ? "" : planet->area12,
planet->area13 == NULL ? "" : planet->area13,
planet->area14 == NULL ? "" : planet->area14,
planet->area15 == NULL ? "" : planet->area15 );
    pager_printf( ch, "&WGuardian:      &G%s\n\r", planet->guardian );    
    pager_printf( ch, "&WGravity:       &G%d Gs\n\r", planet->gravity );
    pager_printf( ch, "&WEconomy:       &G%d\n\r", planet->economy );
    pager_printf( ch, "&WCo-Ordinates: (&G%d,%d,%d&W)\n\r", planet->x, planet->y, planet->z );
    return;
}

void do_makeplanet( CHAR_DATA *ch, char *argument )
{
    char filename[256];
    PLANET_DATA *planet;
    bool found;

    if ( !argument || argument[0] == '\0' )
    {
	send_to_char( "Usage: makeplanet <planet name>\n\r", ch );
	return;
    }

    found = FALSE;
    sprintf( filename, "%s%s", PLANET_DIR, strlower(argument) );
    CREATE( planet, PLANET_DATA, 1 );
    LINK( planet, first_planet, last_planet, next, prev );
    planet->name		= STRALLOC( argument );
    planet->next_in_system = NULL;
    planet->last_in_system = NULL;
    planet->economy = 0;
    planet->guardian = NULL;
    planet->gravity = 0;
    planet->hero = NULL;
    planet->x = 0;
    planet->y = 0;
    planet->z = 50;
}

char * get_quad( int x, int y, int z )
{
   if ( x == 0 || y == 0 )
	return ( "Galactic Leyline" );

 if ( x > 0 && y > 0 )
   return ( "North" );

 if ( x < 0 && y < 0 )
   return ( "South" );

 if ( x < 0 && y > 0  )
   return ( "East" );

 if ( x > 0 && y < 0  )
   return ( "West" );

 return ( "Otherworld" );
}
/*
void do_planets( CHAR_DATA *ch, char *argument )
{
PLANET_DATA *planet;

 for ( planet = first_planet ; planet ; planet = planet->next )
 {
 pager_printf( ch, "Name       Guardian   Hero       Location      Gravity   Qudrant" );
 pager_printf( ch, "%-10.10s %-10.10s %-10.10s (%-3.3d,%-3.3d,%-3.3d)    %-2.2d   %s\n\r",
               planet->name, planet->guardian, planet->hero,
               planet->x, planet->y, planet->z, planet->gravity,
               get_quad( planet->x, planet->y, planet->z ) );
 }
return;
}
*/
void do_planetlist ( CHAR_DATA *ch, char *argument )
{
  PLANET_DATA * planet;
  AREA_DATA   * pArea;
  CHAR_DATA   * c;
  int 		ctr = 0;
  for ( planet = first_planet ; planet ; planet = planet->next )
  {
	pager_printf( ch, "&G--- &W%-20.20s &G--- &W%-2dGs &G--- &C( &W%-4d&C,&W%-4d&C,&W%-4d &C) &G---\n\r",
			planet->name, planet->gravity, planet->x, planet->y, planet->z );
	for ( pArea = first_area; pArea; pArea = pArea->next )
        {
            if ( !str_cmp( "RoD", pArea->author ) || str_cmp( pArea->planet, planet->name ) )
                continue;

            if ( IS_SET(pArea->flags, AFLAG_LAWFUL) )
            {
                pager_printf( ch, "&c[%s%d&c] ", pArea->terror_level <= 1 ? "&G" : pArea->terror_level >= 4 ? "&R" : "&Y", pArea->terror_level );
            }
	    else
	    {
                pager_printf( ch, "    ", pArea->terror_level );
	    }

	    ctr = 0;
            for ( c = first_char; c; c = c->next )
            {
                if ( c->in_room->area == pArea )
                {
                    ctr++;
                }
            }
            pager_printf(ch, "&w%-30s &c[&W%3d&c]&w by %s&w\n\r", pArea->name, ctr, pArea->author );
        }
	pager_printf( ch, "\n\r&w" );
    }
}

void do_makeship( CHAR_DATA *ch, char *argument )
{
 char arg1[MAX_STRING_LENGTH];
// char arg2[MAX_STRING_LENGTH];
// int ctr = 0;
 SHIP_DATA *ship;
 char buf[MAX_STRING_LENGTH];

 if ( IS_NPC(ch) || !IS_IMMORTAL(ch) )
 {
    send_to_char( "Huh?\n\r", ch );
    return;
 }

 argument = one_argument ( argument, arg1 );

 if ( arg1[0] == '\0' )
    {
    send_to_char( "Makeship: Usage\n\r Makeship <name>\n\r", ch );
    return;
    }

    CREATE( ship, SHIP_DATA, 1 );
    LINK( ship, first_ship, last_ship, next, prev );
    sprintf(buf, "%s.ship", arg1 );
    ship->filename = STRALLOC( buf );
    ship->name = STRALLOC( arg1 );
    ship->owner = NULL;
    ship->atx = 0;
    ship->aty = 0;
    ship->atz = 50;
    ship->bridge = 1;
    ship->entrance = 1;
    ship->speed = 1;
    ship->sensor = 1;
    ship->max_fuel = 100;
    ship->fuel = 100;
    ship->state = SHIP_DOCKED;
    pager_printf( ch, "Ship %s created.  Null data assigned.\n\r", ship->name );
    write_ship_list();
    return;
}

void write_ship_list()
{
    SHIP_DATA *tship;
    FILE *fpout;
    char filename[256];

    sprintf( filename, "%s%s", SHIP_DIR, SHIP_LIST );
    fpout = Fopen( filename, "w" );
    if ( !fpout )
    {
	   bug( "ERROR: Ship.lst either not found or busted!  Cannot write!\n\r", 0 );
       return;
    }	  
    for ( tship = first_ship; tship; tship = tship->next )
	fprintf( fpout, "%s\n", tship->filename );
    fprintf( fpout, "$\n" );
    Fclose( fpout );
}

void save_ship( SHIP_DATA *ship )
{
    FILE *fp;
    char filename[256];
//    int ctr = 0;
    char buf[MAX_STRING_LENGTH];

    if ( !ship )
    {
	bug( "SAVE_SHIP: No ship found!", 0 );
	return;
    }
        
    if ( !ship->filename || ship->filename[0] == '\0' )
    {
	sprintf( buf, "SAVE_SHIP: %s has no filename", ship->name );
	bug( buf, 0 );
	return;
    }
 
    sprintf( filename, "%s%s", SHIP_DIR, ship->filename );
    
    Fclose( fpReserve );
    if ( ( fp = Fopen( filename, "w" ) ) == NULL )
    {
    	bug( "save_ship: Fopen", 0 );
    	perror( filename );
    }
    else
    {
	fprintf( fp, "#SHIP\n" );
	fprintf( fp, "Filename     %s~\n",	ship->filename    );
	fprintf( fp, "Name         %s~\n",	ship->name        );
	fprintf( fp, "Owner        %s~\n",	ship->owner       );
    fprintf( fp, "Cost         %d\n",   ship->cost        );
    fprintf( fp, "X            %d\n",   ship->atx         );
    fprintf( fp, "Y            %d\n",   ship->aty         );
    fprintf( fp, "Z            %d\n",   ship->atz         );
    fprintf( fp, "Wp1X            %d\n",   ship->wp1x         );
    fprintf( fp, "Wp1Y            %d\n",   ship->wp1y         );
    fprintf( fp, "Wp1Z            %d\n",   ship->wp1z         );
    fprintf( fp, "Wp2X            %d\n",   ship->wp2x         );
    fprintf( fp, "Wp2Y            %d\n",   ship->wp2y         );
    fprintf( fp, "Wp2Z            %d\n",   ship->wp2z         );
    fprintf( fp, "To_X         %d\n",   ship->tox         );
    fprintf( fp, "To_Y         %d\n",   ship->toy         );
    fprintf( fp, "To_Z         %d\n",   ship->toz         );
    fprintf( fp, "Speed        %d\n",   ship->speed       );
    fprintf( fp, "Sensor       %d\n",   ship->sensor      );
    fprintf( fp, "Fuel         %d\n",   ship->fuel        );
    fprintf( fp, "Maxfuel      %d\n",   ship->max_fuel     );
    fprintf( fp, "Bridge       %d\n",   ship->bridge      );
    fprintf( fp, "Entrance     %d\n",   ship->entrance    );
    fprintf( fp, "State        %d\n",   ship->state       );
    fprintf( fp, "Lock         %d\n",   ship->locked        );
    fprintf( fp, "End\n\n"	);
    fprintf( fp, "#END\n"	);
    }
    Fclose( fp );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}

#if defined(KEY)
#undef KEY
#endif

#define KEY( literal, field, value )				\
				if ( !str_cmp( word, literal ) )	\
				{					                \
				    field  = value;			        \
				    fMatch = TRUE;			        \
				    break;				            \
				}

SHIP_DATA *fread_ship( FILE *fp )
{
    char buf[MAX_STRING_LENGTH];
    char *word;
    bool fMatch;
    int ctr = -1;
    SHIP_DATA *ship;

    ctr++;
    CREATE( ship, SHIP_DATA, 1 );
    ship->cost        = 0;
    ship->atx         = 0;
    ship->aty         = 0;
    ship->atz         = 0;
    ship->tox         = 0;
    ship->toy         = 0;
    ship->toz         = 0;
    ship->speed       = 0;
    ship->sensor      = 0;
    ship->fuel        = 0;
    ship->max_fuel    = 0;
    ship->bridge      = 0;
    ship->entrance    = 0;
    ship->state       = SHIP_DOCKED;
    for ( ; ; )
    {
	word   = feof( fp ) ? "End" : fread_word( fp );
	fMatch = FALSE;

	switch ( UPPER(word[0]) )
	{
	case '*':
	    fMatch = TRUE;
	    fread_to_eol( fp );
	    break;

    case 'B':
   	    KEY( "Bridge",	    ship->bridge,	    fread_number( fp ) );

	case 'C':
	    KEY( "Cost",	    ship->cost, 	    fread_number( fp ) );
/*	    KEY( "Crew0",	    ship->crew[0],	    fread_string( fp ) );
	    KEY( "Crew1",	    ship->crew[1],	    fread_string( fp ) );
	    KEY( "Crew2",	    ship->crew[2],	    fread_string( fp ) );
	    KEY( "Crew3",	    ship->crew[3],	    fread_string( fp ) );
	    KEY( "Crew4",	    ship->crew[4],	    fread_string( fp ) );
	    KEY( "Crew5",	    ship->crew[5],	    fread_string( fp ) );
	    KEY( "Crew6",	    ship->crew[6],	    fread_string( fp ) );
	    KEY( "Crew7",	    ship->crew[7],	    fread_string( fp ) );
	    KEY( "Crew8",	    ship->crew[8],	    fread_string( fp ) );
	    KEY( "Crew9",	    ship->crew[9],	    fread_string( fp ) );*/
	    break;

    case 'E':
   	    KEY( "Entrance",	ship->entrance,	    fread_number( fp ) );
	    if ( !str_cmp( word, "End" ) )
	    {
		if (!ship->name)
		  ship->name		= STRALLOC( "" );
		return ship;
	    }
        break;

	case 'F':
	    KEY( "Filename",	ship->filename, 	fread_string_nohash( fp ) );
	    KEY( "Fuel",	    ship->fuel,      	fread_number( fp ) );
	    break;

	case 'L':
   	    KEY( "Lock", 	ship->locked, fread_number( fp ) );
	    break;

	case 'M':
   	    KEY( "Maxfuel", ship->max_fuel, fread_number( fp ) );
	    break;
	
	case 'N':
	    KEY( "Name",	    ship->name,		fread_string_nohash( fp ) );
	    break;

	case 'O':
	    KEY( "Owner",	    ship->owner,		fread_string_nohash( fp ) );
	    break;

  	case 'S':
        KEY( "Sensor",      ship->sensor,   fread_number( fp ) );
        KEY( "Speed",       ship->speed,    fread_number( fp ) );
        KEY( "State",       ship->state,    fread_number( fp ) );
  	case 'T':
	    KEY( "To_X",	    ship->tox,	    fread_number( fp ) );
	    KEY( "To_Y",	    ship->tox,	    fread_number( fp ) );
	    KEY( "To_Z",	    ship->tox,	    fread_number( fp ) );
	    break;

	case 'W':
	    KEY( "Wp1X",	ship->wp1x,		fread_number( fp ) );
	    KEY( "Wp1Y",	ship->wp1y,		fread_number( fp ) );
	    KEY( "Wp1Z",	ship->wp1z,		fread_number( fp ) );
	    KEY( "Wp2X",	ship->wp2x,		fread_number( fp ) );
	    KEY( "Wp2Y",	ship->wp2y,		fread_number( fp ) );
	    KEY( "Wp2Z",	ship->wp2z,		fread_number( fp ) );
	    break;

	case 'X':
	    KEY( "X",	ship->atx,		fread_number( fp ) );
	    break;

	case 'Y':
	    KEY( "Y",	ship->aty,		fread_number( fp ) );
	    break;

	case 'Z':
	    KEY( "Z",	ship->atz,		fread_number( fp ) );
	    break;
    
	}
	
	if ( !fMatch )
	{
	    sprintf( buf, "Fread_ship: no match: %s", word );
	    bug( buf, 0 );
	}
	
    }
    return ship;
}

/*
 *  Creates an empty instance of a ship and loads data into it
 */
bool load_ship_file( char *shipfile )
{
    char filename[256];
//    char *word;
    SHIP_DATA *ship = NULL;
    FILE *fp;
    bool found;

/*    CREATE( ship, SHIP_DATA, 1 );

    ship->cost        = 0;
    ship->atx           = 0;
    ship->aty           = 0;
    ship->atz           = 0;
    ship->tox        = 0;
    ship->toy        = 0;
    ship->toz        = 0;
    ship->speed       = 0;
    ship->sensor      = 0;
    ship->fuel        = 0;
    ship->max_fuel     = 0;
    ship->bridge      = 0;
    ship->entrance    = 0;
    ship->state       = SHIP_DOCKED; */

    found = FALSE;
    sprintf( filename, "%s%s", SHIP_DIR, shipfile );

    if ( ( fp = Fopen( filename, "r" ) ) != NULL )
    {

	found = TRUE;
	for ( ; ; )
	{
	    char letter;
	    char *word;

	    letter = fread_letter( fp );
	    if ( letter == '*' )
	    {
		fread_to_eol( fp );
		continue;
	    }

	    if ( letter != '#' )
	    {
		bug( "Load_ship_file: # not found.", 0 );
		break;
	    }

	    word = fread_word( fp );
	    if ( !str_cmp( word, "SHIP"	) )
	    {
	    	ship = fread_ship( fp );
	    	break;
	    }
	    else
	    if ( !str_cmp( word, "END"	) || !str_cmp( word, "#END" ))
	        break;
	    else
	    {
		char buf[MAX_STRING_LENGTH];

		sprintf( buf, "Load_ship_file: bad section: %s.", word );
		bug( buf, 0 );
		break;
	    }
	}
	Fclose( fp );
	}
    if ( !found && ship )
      DISPOSE( ship );
    else if ( ship )
      LINK( ship, first_ship, last_ship, next, prev );

    return found;
}

/*
 * Loads all the ships in the game.
 */

void load_ships( )
{
   FILE *fpList;
   char *filename;
   char list[256];
   char buf[MAX_STRING_LENGTH];

    sprintf( list, "%s%s", SHIP_DIR, SHIP_LIST );
    Fclose( fpReserve );

        if ( ( fpList = Fopen( list, "r" ) ) == NULL )
        {
                log_string ( "Cannot open ships.lst for reading. Loading aborted.");
                return;
    }

        for ( ; ; )
        {
                filename = feof( fpList ) ? "$" : fread_word( fpList );

                if ( filename[0] == '$' )
                        break;

                if ( !load_ship_file( filename ) )
                {
                        sprintf( buf, "Cannot load ship file: %s", filename );
                        bug( buf, 0 );
                }

    }

    Fclose( fpList );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;

}

/*void load_ships( )
{
    FILE *fpList;
    char *filename;
    char shiplist[256];
    char buf[MAX_STRING_LENGTH];
    
    first_ship	= NULL;
    last_ship	= NULL;

    log_string( "Loading ships..." );

    sprintf( shiplist, "%s%s", SHIP_DIR, SHIP_LIST );
 if ( fpReserve )
    Fclose( fpReserve );
    if ( ( fpList = Fopen( SHIP_LIST, "r" ) ) == NULL )
    {
	perror( shiplist );
	exit( 1 );
	return;
    }

    for ( ; ; )
    {
	filename = feof( fpList ) ? "$" : fread_word( fpList );
	log_string( filename );
	if ( filename[0] == '$' )
	  break;

	if ( !load_ship_file( filename ) )
	{
	  sprintf( buf, "Cannot load ship file: %s", filename );
	  bug( buf, 0 );
	}
    }
    Fclose( fpList );
    log_string(" Done loading ships" );
    fpReserve = Fopen( NULL_FILE, "r" );
    return;
}*/



void do_showship( CHAR_DATA *ch, char *argument )
{   
    SHIP_DATA *ship;
//    int ctr = 0;

    if ( IS_NPC( ch ) || !IS_IMMORTAL(ch) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    if ( argument[0] == '\0' )
    {
	send_to_char( "Usage: showship <ship>\n\r", ch );
	return;
    }

    ship = get_ship( argument );
    if ( !ship )
    {
	send_to_char( "No such ship.\n\r", ch );
	return;
    }

//    toupper( ship->name[0] );

    pager_printf( ch, "&W%s\n\r", ship->name);
if ( IS_IMMORTAL(ch) )
    pager_printf( ch, "&WFilename:      &G%s\n\r", ship->filename );
    pager_printf( ch, "&WOwner:         &G%s\n\r", ship->owner );
    pager_printf( ch, "&RData:\n\r" );
if ( IS_IMMORTAL(ch) )
    pager_printf( ch, "&WBridge Vnum: %d     Entrance Vnum:  %d\n\r", ship->bridge, ship->entrance );
    pager_printf( ch, "&WSpeed:       %d     Sensor Range %d\n\r", ship->speed, ship->sensor );
    pager_printf( ch, "&WFuel:        %d/%d\n\r", ship->fuel, ship->max_fuel );
    pager_printf( ch, "&WState:       %s\n\r", shipstate( ship->state ));
    pager_printf( ch, "&WCo-Ordinates: (&G%d,%d,%d&W)\n\r", ship->atx, ship->aty, ship->atz );
    pager_printf( ch, "&WDestination:  (&G%d,%d,%d&W)\n\r", ship->tox, ship->toy, ship->toz );
/*    pager_printf( ch, "&WWaypoints 1:  (&G%d,%d,%d&W)\n\r", ship->tox, ship->toy, 
ship->toz );
    pager_printf( ch, "&WDestination:  (&G%d,%d,%d&W)\n\r", ship->tox, ship->toy, ship->toz );
    pager_printf( ch, "&WDestination:  (&G%d,%d,%d&W)\n\r", ship->tox, ship->toy, ship->toz );
    pager_printf( ch, "&WDestination:  (&G%d,%d,%d&W)\n\r", ship->tox, ship->toy, ship->toz 
);*/
 return;
}


char * shipstate ( int state )
{
 switch ( state )
    {
    case SHIP_DOCKED:      return( "Docked" );
    case SHIP_LAUNCHING:   return( "Launching" );
    case SHIP_FLYING:      return( "Flying" );
    case SHIP_PRE_WARP:    return( "Pre Warp" );
    case SHIP_WARP:        return( "Warp" );
    case SHIP_POST_WARP:   return( "Post Warp" );
    case SHIP_LANDING:     return( "Landing" );
    case SHIP_DOCKING:     return( "Docking" );
    }
 return ("N/A");
}

void do_shipset( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    SHIP_DATA *ship;

    if ( IS_NPC( ch ) || !IS_IMMORTAL(ch) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' )
    {
	send_to_char( "Usage: shipset <ship> <field> [value]\n\r", ch );
	send_to_char( "\n\rField being one of:\n\r", ch );
        send_to_char( "filename name cost owner\n\r", ch );
        send_to_char( "x y z speed sensor fuel\n\r", ch );
        send_to_char( "bridge entrance \n\r(For Buses) wp1x wp2x wp1y wp2y wp1z wp2z\n\r", ch );
	return;
    }

    ship = get_ship( arg1 );
    if ( !ship )
    {
	send_to_char( "No such ship.\n\r", ch );
	return;
    }

    
    if ( !strcmp( arg2, "filename" ) )
    {
//	DISPOSE( ship->filename );
	ship->filename = str_dup( argument );
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	write_ship_list( );
	return;
    }

    if ( !strcmp( arg2, "name" ) )
    {
	STRFREE( ship->name );
	ship->name = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "owner" ) )
    {
	STRFREE( ship->owner );
	ship->owner = STRALLOC( argument );
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "cost" ) )
    {
    if ( atoi(argument) > 2000000000 )
       {
       send_to_char( "A little steep, don't you think?\n\r", ch );
       return;
       }
	ship->cost = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "wp1x" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp1x = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "wp2x" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp2x = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "wp1y" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp1y = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "wp2y" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp2y = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "wp2z" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp2z = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "wp1z" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->wp1z = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }


    if ( !strcmp( arg2, "x" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->atx = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "y" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->aty = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "z" ) )
    {
    if ( atoi(argument) > 1000 || atoi(argument) < -1000 )
       {
       send_to_char( "Out of Range\n\r", ch );
       return;
       }
	ship->atz = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "speed" ) )
    {
    if ( atoi(argument) > 50000000 || atoi(argument) < 1 )
       {
       send_to_char( "Range is from 50,000,000 to 1.\n\r", ch );
       return;
       }
	ship->speed = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "sensor" ) )
    {
    if ( atoi(argument) > 10 || atoi(argument) < 1 )
       {
       send_to_char( "Range is from 10 to 1.\n\r", ch );
       return;
       }
	ship->sensor = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "fuel" ) )
    {
    if ( atoi(argument) > 1000000 || atoi(argument) < 1 )
       {
       send_to_char( "Range is from 1,000,000 to 1.\n\r", ch );
       return;
       }
	ship->max_fuel = atoi(argument);
	ship->fuel = ship->max_fuel;
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "bridge" ) )
    {
    if ( atoi(argument) > 2000000000 || atoi(argument) < 1 )
       {
       send_to_char( "Range is from 2,000,000,000 to 1.\n\r", ch );
       return;
       }
	ship->bridge = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }
    if ( !strcmp( arg2, "lock" ) )
    {
	ship->locked = !ship->locked;
	if (ship->locked)
	send_to_char( "Locked\n\r", ch );
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

    if ( !strcmp( arg2, "entrance" ) )
    {
    if ( atoi(argument) > 2000000000 || atoi(argument) < 1 )
       {
       send_to_char( "Range is from 2,000,000,000 to 1.\n\r", ch );
       return;
       }
	ship->entrance = atoi(argument);
	send_to_char( "Done.\n\r", ch );
	save_ship( ship );
	return;
    }

 do_shipset( ch, "" );
 return;
}

void tell_bridge( SHIP_DATA *ship, char * argument )
{
 ROOM_INDEX_DATA *room;
 if ( !get_ship( ship->name ) )
	return;
 room = get_room_index(ship->bridge);
 if ( room )
	 echo_to_room( AT_GREEN, room, argument );
 return;
}


void do_board( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA *target;
 PLANET_DATA *planet;

 // default to own ship
 if ( IS_NPC(ch) )
    return;

    planet = get_planet( ch->in_room->area->planet );
/*
 if ( !argument || argument[0] == '\0' )
 {
    if ( !ch->pcdata->ship )
    {
     pager_printf(ch, "You have no ship.\n\r" );
     return;
    }

    if ( !planet || !IS_SET(ch->in_room->room_flags, ROOM_DOCK ) )
    {
     pager_printf(ch, "This isn't a spacedock!\n\r" );
     return;
    }
    
    if ( planet->x != ch->pcdata->ship->atx ||
         planet->y != ch->pcdata->ship->aty ||
         planet->z != ch->pcdata->ship->atz ||
         ch->pcdata->ship->state != SHIP_DOCKED )
    {
     pager_printf(ch, "Your ship isn't available at the moment.\n\r" );
     return;
    }

    if ( ch->pcdata->ship->locked == TRUE )
    {
     pager_printf( ch, "Unlock your ship first!\n\r" );
     return;
    }

    if ( ch->pcdata->ship->capital != TRUE )
       pager_printf( ch, "You pull down the steel ladder and climb on to %s.\n\r", ch->pcdata->ship->name );
    else
       pager_printf( ch, "You fly to docking bay one and enter %s.\n\r", ch->pcdata->ship->name );
    act( AT_YELLOW, "$n disappears into a ship.", ch, NULL, NULL, TO_CANSEE );
    char_from_room( ch );
    char_to_room( ch, get_room_index(ch->pcdata->ship->entrance) );
    do_look( ch, "auto" );
    return;
 }
 else
 {*/
  if ( !planet || !IS_SET(ch->in_room->room_flags, ROOM_DOCK ) )
  {
     send_to_char( "That ship isn't here.\n\r", ch );
     return;
  }

  if ( ( target = get_ship(argument) ) == NULL )
  {
     send_to_char( "That ship isn't here.\n\r", ch );
     return;
  }

  if ( planet->x != target->atx ||
       planet->y != target->aty ||
       planet->z != target->atz ||
       target->state != SHIP_DOCKED )
  {
     pager_printf(ch, "That ship isn't here.\n\r" );
     return;
  }

  if ( target->locked == TRUE && str_cmp( ch->name, target->owner) &&  str_cmp( "RENTAL", target->owner))
  {
     send_to_char( "What are you going to do, hijack it?\n\r", ch );
     return;
  }

  if ( target->capital != TRUE )
     pager_printf( ch, "You pull down the steel ladder and climb on to %s.\n\r", target->name );
  else
     pager_printf( ch, "You fly to docking bay one and enter %s.\n\r", target->name );
  act( AT_YELLOW, "$n disappears into a ship.", ch, NULL, NULL, TO_CANSEE );
  char_from_room( ch );
  char_to_room( ch, get_room_index(target->entrance) );
  do_look( ch, "auto" );
  return;
 }

PLANET_DATA *planet_lookup( int x, int y, int z )
{
PLANET_DATA *planet;    
   
for ( planet = first_planet; planet; planet = planet->next )
     {
     if ( planet->x == x && planet->y == y && planet->z == z )
          return planet;
     }

return NULL;
}

void do_leaveship( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA *ship, *onship = NULL;
 PLANET_DATA *planet;

 if ( !IS_SET( ch->in_room->room_flags, ROOM_ENTRANCE ) )
 {
    send_to_char( "You are not in a ship's entrance.\n\r", ch );
    return;
 }

 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->entrance == ch->in_room->vnum )
     {
         onship = ship;
         break;
     }
 }

 // Shouldn't happen but whatever

 if ( !onship || onship == NULL )
 {
    send_to_char( "You are not on a ship!\n\r", ch );
    return;
 }

 if ( onship->state != SHIP_DOCKED )
 {
    send_to_char( "And get sucked into space?\n\r", ch );
    return;
 }

 // Ok the ship should be docked, so this *should* be unecessary...

 if ( (planet = planet_lookup( ship->atx, ship->aty, ship->atz ) ) == NULL )
 {
    send_to_char( "Try stopping at a planet next time.\n\r", ch );
    return;
 }


 // All the checks are fine...

  if ( onship->capital != TRUE )
     pager_printf( ch, "You descend the steel ladder and climb off %s.\n\r", onship->name );
  else
     pager_printf( ch, "You fly out of docking bay one of %s.\n\r", onship->name );
  act( AT_YELLOW, "$n disappears from the ship.", ch, NULL, NULL, TO_CANSEE );
  char_from_room( ch );
  char_to_room( ch, get_room_index(planet->dock) );
  do_look( ch, "auto" );
  return;
}

/* Is it over yet...? */

void do_course( CHAR_DATA *ch, char *argument )
{
 int x,y,z;
 SHIP_DATA *ship = NULL, *onship = NULL/*, first_ship = NULL*/;
 char arg1[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];
 char arg3[MAX_INPUT_LENGTH];
 char buf[MAX_STRING_LENGTH];

 if ( !IS_SET(ch->in_room->room_flags, ROOM_BRIDGE ) )
 {
    send_to_char( "You need to be on a spaceship's bridge for that.\n\r", ch );
    return;
 }

 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->bridge == ch->in_room->vnum )
     {
         onship = ship;
         break;
     }
 }

 if ( !onship )
 {
    send_to_char( "You are not even on a ship!\n\r", ch );
    return;
 }

 if ( !str_cmp(onship->owner, ch->name ))
 {
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );
    if ( arg1==NULL || arg2==NULL || arg3==NULL || !is_number(arg1) || !is_number(arg2) || !is_number(arg3) )
    {
       send_to_char( "&PFormat: Course <x> <y> <z>   \n\r", ch );
       return;
    }
    x = atoi(arg1);
    y = atoi(arg2);
    z = atoi(arg3);
    if ( x > NQ_HI_X || x < SQ_LO_X || y > SQ_LO_Y || y < WQ_LO_X || z < LO_Z || z > HI_Z )
    {
       send_to_char( "Co-ordinates out of range.  Please try again.\n\r", ch );
    }
    if ( onship->atx == x && onship->aty == y && onship->atz == z )
    {
       send_to_char( "You are already there, genius.\n\r", ch );
       return;
    }
    onship->tox = x;
    onship->toy = y;
    onship->toz = z;
    sprintf( buf, "&PCourse set for %d %d %d - %s.\n\r", x, y, z, get_quad(x,y,z) );
    tell_bridge( onship, buf );
    return;
 }

 if ( !str_cmp( onship->owner, "RENTAL" ) )
 {
    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    argument = one_argument( argument, arg3 );
    if ( arg1==NULL || arg2==NULL || arg3==NULL || !is_number(arg1) || !is_number(arg2) || !is_number(arg3) )
    {
       send_to_char( "&PFormat: Course <x> <y> <z>   \n\r", ch );
       return;
    }
    x = atoi(arg1);
    y = atoi(arg2);
    z = atoi(arg3);

    if ( onship->atx == x && onship->aty == y && onship->atz == z )
    {
       send_to_char( "You are already there, genius.\n\r", ch );
       return;
    }
    if ( ch->gold < onship->cost / 1000 )
    {
       pager_printf( ch, "You need %d zeni to set a course on this ship.\n\r", onship->cost / 1000 );
       return;
    }
    onship->tox = x;
    onship->toy = y;
    onship->toz = z;
    pager_printf( ch, "You pay %d zeni to rent the ship.\n\r", onship->cost / 1000 );
    ch->gold -= ( onship->cost / 1000 );
    sprintf( buf, "&PCourse set for %d %d %d - %s Quadrant.\n\rLaunching.", x, y, z, get_quad(x,y,z) );
    tell_bridge( onship, buf );
    onship->state = SHIP_DOCKED;
    launch_ship( onship );
    return;
 }

 send_to_char( "This isn't your ship!\n\r", ch );
 return;
}

void do_launch ( CHAR_DATA *ch, char *argument )
{
// int x,y,z;
 SHIP_DATA *ship = NULL, *onship = NULL/*, *first_ship = NULL*/;
/* char arg1[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];
 char arg3[MAX_INPUT_LENGTH];*/
// char buf[MAX_STRING_LENGTH];


 if ( !IS_SET(ch->in_room->room_flags, ROOM_BRIDGE ) )
 {
    send_to_char( "You need to be on a spaceship's bridge for that.\n\r", ch );
    return;
 }

 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->bridge == ch->in_room->vnum )
     {
         onship = ship;
         break;
     }
     onship = NULL;
 }

 if ( !onship )
 {
    send_to_char( "You are not even on a ship!\n\r", ch );
    return;
 }

 if ( onship->state == SHIP_FLYING )
 {
    send_to_char( "Again?\n\r", ch );
    return;
 }

 if ( !str_cmp( onship->owner, ch->name ) )
 {

    if ( onship->atx == onship->tox && onship->aty == onship->toy && onship->atz == onship->toz )
    {
       send_to_char( "You are already at your destination.\n\r", ch );
       return;
    }
    if ( onship->fuel < 10 )
    {
       send_to_char( "Ship launching requires ten units of fuel.\n\r", ch );
       return;
    }
    if ( !str_cmp( onship->owner, "BUS" ) )
	return;

    launch_ship( onship );
    return;
 }

 send_to_char( "This isn't your ship!  To launch a rented ship, set a course.\n\r", ch );
 return;
}

void launch_ship( SHIP_DATA *onship )
{
 PLANET_DATA *planet;
 char buf[MAX_STRING_LENGTH];

 onship->state = SHIP_FLYING;
 tell_bridge( onship, "Ship Launched.\n\r" );

 planet = planet_lookup( onship->atx, onship->aty, onship->atz );
 if ( planet != NULL && planet && get_room_index(planet->dock) != NULL )
     echo_to_room( AT_PINK, get_room_index(planet->dock), "A ship takes off.\n\r" );
 else
 {
     sprintf( buf, "Ship launching in NULL room! Ship: %s", onship->name );
     log_string_plus( buf, LOG_NORMAL, 63 );
 }
 return;
}

void do_buyship( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA   * ship;
 PLANET_DATA   * planet;
 char          arg1[MAX_STRING_LENGTH];

 argument = one_argument( argument, arg1 );

 if ( arg1==NULL )
 {
    send_to_char ( "Buy what ship?  Shiplist to see the available ships.\n\r", ch );
    return;
 }
ship = get_ship(arg1);

 if ( !ship || ship == NULL )
 {
	send_to_char( "No such ship\n\r", ch );
	return;
 }

 if ( ship->owner == ch->name )
 {
    send_to_char( "That's already yours...\n\r", ch );
    return;
 }
 if ( !str_cmp( ship->owner, "RENTAL" ) )
 {
    send_to_char( "That ship is for rental only.\n\r", ch );
    return;
 }
 if ( ship->owner != NULL && str_cmp( ship->owner, "FORSALE" ) )
 {
    send_to_char( "That ship is not for sale.\n\r", ch );
    return;
 }
 if ( !str_cmp( ship->owner, "BUS" ) )
	return;

 if ( IS_NPC( ch ) )
 {
    send_to_char( "Mob owned ships?  Whatever next...\n\r", ch );
    return;
 }
 if ( ch->gold < ship->cost )
 {
    send_to_char( "You can't afford that ship yet.\n\r", ch );
    return;
 }
 if ( ship->clan && ch->pcdata->clan && ship->clan != ch->pcdata->clan )
 {
    send_to_char( "That ship was made for another clan.\n\r", ch );
    return;
 }
 ch->gold -= ship->cost;
 ship->owner = ch->name;
 pager_printf( ch, "You pull out your cellphone and make the call.\n\rYou now own the %s.\n\rThe ship appears before you and you get on it.\n\r", ship->name );
 act( AT_YELLOW, "$n disappears into a ship.", ch, NULL, NULL, TO_CANSEE );
 char_from_room( ch );
 char_to_room( ch, get_room_index(ship->bridge) );
 planet = get_planet(ch->in_room->area->name );
 if ( planet)
 {	
 ship->atx = planet->x;
 ship->aty = planet->y;
 ship->atz = planet->z;
 }
 ship->state = SHIP_DOCKED;
 ch->pcdata->ship = ship;
 do_save( ch, "" );
 save_ship( ship );
 return;
}

void do_shiplist( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA *ship/*, *first_ship = NULL*/;

 pager_printf( ch, "  NAME                     COST        CLAN  \n\r" );
 pager_printf( ch, "-----------------------------------------------\n\r" );
 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( (!ship->owner ||!str_cmp( ship->owner, "FORSALE" )) && ship->name && str_cmp(ship->name, "(null)") )
         pager_printf( ch, "| %-17s        %-12s     %s |\n\r", ship->name, format_pl( 
ship->cost ), ship->clan ? " X" : "  " );
 }
 pager_printf( ch, "-----------------------------------------------\n\r" );
 return;
}

/*void do_sellship( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA   * ship;

 if ( IS_NPC( ch ) )
 {
    send_to_char( "Mob owned ships?  Whatever next...\n\r", ch );
    return;
 }
 ship = ch->pcdata->ship;
 ch->gold += ship->cost * 0.8;
 ship->owner = STRALLOC( "FORSALE" );
 pager_printf( ch, "You pull out your cellphone and make the call.\n\rYou now no longer the %s, and go back to Earth.\n\r", ship->name );
 char_from_room( ch );
 char_to_room( ch, get_room_index(752) );
 ship->atx = 0;
 ship->aty = 0;
 ship->atz = 50;
 ship->state = SHIP_DOCKED;
 ch->pcdata->ship = NULL;
 do_save( ch, "" );
 save_ship( ship );
 return;
}
*/
void show_ships_to_char( CHAR_DATA *ch )
{
 SHIP_DATA *ship/*, *first_ship = NULL*/;
 PLANET_DATA *planet;

 if ( IS_NPC(ch) )
    return;

 if ( !IS_SET(ch->in_room->room_flags, ROOM_DOCK ) )
    return;

 planet = get_planet(ch->in_room->area->planet );
 if ( !planet)
{
	pager_printf( ch, "Bug in show_ships. Tell Yami." );
	return;
}
 pager_printf( ch, "\n\r&G  SHIPS: &r(Type 'Help Shuttles' for a full list)\n\r" );
 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->atx  == planet->x &&
          ship->aty  == planet->y &&
          ship->atz  == planet->z &&
          ship->state == SHIP_DOCKED )
          pager_printf( ch, "  &c- &P%s\n\r", ship->name );
 }
 return;
}

void moveships( int ctr )
{
 SHIP_DATA *ship;
 PLANET_DATA *planet;
 AREA_DATA *pArea;

 buses();

 // LAW
 for ( pArea = first_area; pArea; pArea = pArea->next )
 {
     if ( IS_SET(pArea->flags, AFLAG_LAWFUL) && pArea->crime_points > 0 )
     {
         if ( pArea->crime_timer > 0 )
	 {
	     pArea->crime_timer--;
	 }
         else
         {
             if ( pArea->crime_points > 0 )
             {
	         pArea->crime_points--;
             }
             if ( pArea->terror_level != correct_terror(pArea->crime_points) 
	       && pArea->crime_points > 1 && pArea->terror_level > 0 )
             {
                 char str[MAX_STRING_LENGTH];
                 pArea->terror_level = correct_terror(pArea->crime_points);
		 fold_area( pArea, pArea->filename, FALSE );
		 sprintf( str, "The terror level in %s has reduced to %s.  %s\n\r", pArea->name, terror_table[pArea->terror_level].name, terror_table[pArea->terror_level].description );
		 talk_info( AT_GREEN, str );
	         enforcement_wave( pArea );
             }
         }
     }
 }

 if ( ctr % 2 != 0 )
	return;

 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->state == SHIP_FLYING &&
        ( ship->atx != ship->tox || ship->aty != ship->toy || ship->atz != ship->toz )
       && ship->owner && str_cmp( ship->owner, "BUS" ) && str_cmp( ship->owner, "FORSALE" ) )
     {
          save_ship( ship );
	  if ( number_range( 1, 25 ) <= 1 )
		space_messages( ship );

          if ( ship->atx != ship->tox && ship->atx > ship->tox )
             ship->atx--;
          if ( ship->atx != ship->tox && ship->atx < ship->tox )
             ship->atx++;

          if ( ship->aty != ship->toy && ship->aty > ship->toy )
             ship->aty--;
          if ( ship->aty != ship->toy && ship->aty < ship->toy )
             ship->aty++;

          if ( ship->atz != ship->toz && ship->atz > ship->toz )
             ship->atz--;
          if ( ship->atz != ship->toz && ship->atz < ship->toz )
             ship->atz++;
          if ( ship->atx != ship->tox && ship->atx > ship->tox )
             ship->atx--;
          if ( ship->atx != ship->tox && ship->atx < ship->tox )
             ship->atx++;

          if ( ship->aty != ship->toy && ship->aty > ship->toy )
             ship->aty--;
          if ( ship->aty != ship->toy && ship->aty < ship->toy )
             ship->aty++;

          if ( ship->atz != ship->toz && ship->atz > ship->toz )
             ship->atz--;
          if ( ship->atz != ship->toz && ship->atz < ship->toz )
             ship->atz++;
          if ( ship->atx != ship->tox && ship->atx > ship->tox )
             ship->atx--;
          if ( ship->atx != ship->tox && ship->atx < ship->tox )
             ship->atx++;

          if ( ship->aty != ship->toy && ship->aty > ship->toy )
             ship->aty--;
          if ( ship->aty != ship->toy && ship->aty < ship->toy )
             ship->aty++;

          if ( ship->atz != ship->toz && ship->atz > ship->toz )
             ship->atz--;
          if ( ship->atz != ship->toz && ship->atz < ship->toz )
             ship->atz++;

          if ( ship->atx == ship->tox && ship->aty == ship->toy && ship->atz == ship->toz )
          {
             tell_bridge( ship, "Destination reached.  Have a nice trip.\n\r" );
             ship->state = SHIP_DOCKED;
	     planet = planet_lookup( ship->atx, ship->aty, ship->atz );
	    if ( planet )
             echo_to_room( AT_PINK, get_room_index(planet->dock), "A ship lands.\n\r" );
          }

     }
 }
 return;
}

void buses()
{
 SHIP_DATA *ship/*, first_ship*/;
 PLANET_DATA *planet;

 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->owner == NULL )
     {
	char errBuff[MAX_STRING_LENGTH];
	sprintf( errBuff, "Error with ship %s: Null owner string!", ship->name );
	log_string( errBuff );
     }
     if ( str_cmp( ship->owner, "BUS" ))
        continue;
     if ( ship->timer > 0 )
        ship->timer--;

     if ( ship->state == SHIP_DOCKED && ship->timer == 0 )
     {
        launch_ship( ship );
        continue;
     }
     if ( ship->state == SHIP_FLYING && ship->returning == FALSE )
     {
	  if ( number_range( 1, 25 ) <= 1 )
		space_messages( ship );
	  ship->tox = ship->wp1x;
	  ship->toy = ship->wp1y;
	  ship->toz = ship->wp1z;
          if ( ship->atx != ship->wp1x && ship->atx > ship->wp1x )
             ship->atx--;
          if ( ship->atx != ship->wp1x && ship->atx < ship->wp1x )
             ship->atx++;

          if ( ship->aty != ship->wp1y && ship->aty > ship->wp1y )
             ship->aty--;
          if ( ship->aty != ship->wp1y && ship->aty < ship->wp1y )
             ship->aty++;

          if ( ship->atz != ship->wp1z && ship->atz > ship->wp1z )
             ship->atz--;
          if ( ship->atz != ship->wp1z && ship->atz < ship->wp1z )
             ship->atz++;

          if ( ship->atx == ship->wp1x &&
               ship->aty == ship->wp1y &&
               ship->atz == ship->wp1z )
          {
               ship->timer = 100;
               tell_bridge( ship, "Destination reached.\n\r" );
	       ship->state = SHIP_DOCKED;
	       ship->returning = TRUE;
	       planet = planet_lookup( ship->atx, ship->aty, ship->atz );
	       if ( planet && planet->dock )
 		    echo_to_room( AT_PINK, get_room_index(planet->dock), "A ship lands.\n\r" );
          }
     }

     if ( ship->state == SHIP_FLYING && ship->returning == TRUE )
     {
	  if ( number_range( 1, 25 ) <= 1 )
		space_messages( ship );
	  ship->tox = ship->wp2x;
	  ship->toy = ship->wp2y;
	  ship->toz = ship->wp2z;

          if ( ship->atx != ship->wp2x && ship->atx > ship->wp2x )
             ship->atx--;
          if ( ship->atx != ship->wp2x && ship->atx < ship->wp2x )
             ship->atx++;

          if ( ship->aty != ship->wp2y && ship->aty > ship->wp2y )
             ship->aty--;
          if ( ship->aty != ship->wp2y && ship->aty < ship->wp2y )
             ship->aty++;

          if ( ship->atz != ship->wp2z && ship->atz > ship->wp2z )
             ship->atz--;
          if ( ship->atz != ship->wp2z && ship->atz < ship->wp2z )
             ship->atz++;

          if ( ship->atx == ship->wp2x &&
               ship->aty == ship->wp2y &&
               ship->atz == ship->wp2z )
          {
               ship->timer = 20;
               tell_bridge( ship, "Destination reached.\n\r" );
	       ship->returning = FALSE;
	       ship->state = SHIP_DOCKED;
	       planet = planet_lookup( ship->atx, ship->aty, ship->atz );
	       if ( planet && planet->dock )
 		    echo_to_room( AT_PINK, get_room_index(planet->dock), "A ship lands.\n\r" );
          }
     }
 }

 return;
}


void do_sellship( CHAR_DATA *ch, char *argument )
{
 SHIP_DATA   * ship;
 char          arg1[MAX_STRING_LENGTH];

 argument = one_argument( argument, arg1 );

 if ( arg1==NULL )
 {
    send_to_char ( "Sell what ship?\n\r", ch );
    return;
 }
 ship = get_ship(arg1);

 if ( !ship || ship == NULL )
 {
	send_to_char( "No such ship\n\r", ch );
	return;
 }

 if ( str_cmp( ship->owner, ch->name ))
 {
    send_to_char( "That's not already yours...\n\r", ch );
    return;
 }

 if ( IS_NPC( ch ) )
 {
    send_to_char( "Mob owned ships?  Whatever next...\n\r", ch );
    return;
 }
 ch->gold +=( 0.5 * ship->cost );
 ship->owner = STRALLOC( "FORSALE" );
 pager_printf( ch, "You pull out your cellphone and make the call.\n\rYou no longer own the %s.\n\rThe ship flies to the Intergalactic shipyard and you are teleported out.\n\r", ship->name );
 act( AT_YELLOW, "$n disappears from the ship.", ch, NULL, NULL, TO_CANSEE );
 char_from_room( ch );
 char_to_room( ch, get_room_index(753) );
 ship->atx = 0;
 ship->aty = 0;
 ship->atz = 50;
 ship->state = SHIP_DOCKED;
 ch->pcdata->ship = NULL;
 do_save( ch, "" );
 save_ship( ship );
 return;
}

void do_lockship ( CHAR_DATA *ch, char * argument )
{
 SHIP_DATA *ship;

 if ( IS_NPC(ch) )
	return;
 
 if ( !argument || argument[0] == '\0' )
 {
	pager_printf( ch, "Lock which ship?\n\r" );
	return;
 }

 ship = get_ship( argument );

 if ( !ship || str_cmp( ch->name, ship->owner ) )
 {
	pager_printf( ch, "Supply the ship's full name as an argument.  If it's not your ship, you can't lock it.\n\r" );
	return;
 }

 if ( ch->in_room != get_room_index( ship->entrance ) )
 {
	pager_printf( ch, "You need to be in the ship's entrance\n\r" );
	return;
 }

 ship->locked = !ship->locked;
 pager_printf( ch, "You toggle the lock on to your ship.\n\r" );
 if ( ship->locked )
	pager_printf( ch, "Now only you can get in.\n\r" );
 else
	pager_printf( ch, "Now anyone can get in.\n\r" );
 save_ship( ship );
 return;
}


void do_sensors( CHAR_DATA *ch, char * argument )
{
 char planetlist[4096] = " ";
 SHIP_DATA *ship;
 PLANET_DATA *planet;
 
 if ( !IS_SET( ch->in_room->room_flags, ROOM_BRIDGE ) )
 {
    send_to_char( "You are not on the bridge of a ship.\n\r", ch );
    return;
 }
 
 for ( ship = first_ship; ship; ship = ship->next )
 {
     if ( ship->bridge == ch->in_room->vnum )
        break;
 }
 
 if ( !ship )
 {
    send_to_char( "This ship is broken!\n\r", ch );
    return;
 }

 for ( planet = first_planet; planet; planet = planet->next )
 {
     if ( ( planet->x > ship->atx - 15 && planet->x < ship->atx + 15 )
       && ( planet->y > ship->aty - 15 && planet->y < ship->aty + 15 )
       && ( planet->z > ship->atz - 15 && planet->z < ship->atz + 15 ) )
       {
          strcat( planetlist, planet->name );
          strcat( planetlist, "\n\r&C|&W                   " );
       }
 }
                    
 pager_printf( ch, "&C|----------------------------------------------------------------\n\r" );
 pager_printf( ch, "&C|  &WCo-Ordinates &R(&W%4d&R,&W%4d&R,&W%4d&R)&C   |   &WHeading &R(&W%4d&R,&W%4d&R,&W%4d&R)&C\n\r", ship->atx, ship->aty, ship->atz, ship->tox, ship->toy, ship->toz );
 pager_printf( ch, "&C|----------------------------------------------------------------\n\r" );
 pager_printf( ch, "&C|  &WStatus&Y:&W      %s             &C|&W      &wQuadrant&Y:&W %-5s\n\r", ship->state == SHIP_FLYING ? "Flying" : "Docked", get_quad( ship->atx, ship->aty, ship->atz ) );
 pager_printf( ch, "&C|----------------------------------------------------------------\n\r" );
 pager_printf( ch, "&C|  &WPlanets Nearby&Y: &W%s\n\r", planetlist );
 pager_printf( ch, "&C|----------------------------------------------------------------\n\r" ); 
 return;
}

void space_messages( SHIP_DATA *ship )
{
  int selector;
  char buf[MAX_STRING_LENGTH];
  
  selector = number_range( 1, 3 );
  switch ( selector )
  {
       	case 1: 	sprintf( buf, "You hear some radio noise in another language.\n\r" );
	 	 	tell_bridge( ship, buf );				return;
      	case 2: 	sprintf( buf, "You see " );				break;
      	case 3: 	sprintf( buf, "You catch sight of " );			break;
  }
  selector = number_range( 1, 4 );
  switch ( selector )
  {
        case 1:         strcat( buf, "a small shooting star " );		break;
        case 2:         strcat( buf, "a little green man " );			break;
        case 3:         strcat( buf, "a huge meteor shower " );			break;
        case 4:         strcat( buf, "a minefield " );				break;
  }
  selector = number_range ( 1, 3 );
  switch ( selector )
  {
        case 1:         strcat( buf, "floating by the port side.\n\r" );	break;
        case 2:         strcat( buf, "drifting starboard.\n\r" );        	break;
        case 3:         strcat( buf, "flashing ahead of you.\n\r" );        	break;
  }
  tell_bridge( ship, buf );
}

void do_scanner( CHAR_DATA *ch, char * argument )
{
    PLANET_DATA *target;
    CHAR_DATA *c = NULL;

    if ( !IS_SET(ch->in_room->room_flags, ROOM_BRIDGE ) )
    {
       send_to_char( "You need to be on a spaceship's bridge for that.\n\r", ch );
       return;
    }

    if ( argument == NULL || argument[0] == '\0' )
    {
	ch_printf( ch, "         SCANNABLE TARGETS IN RANGE\n\r" );
	ch_printf( ch, "-----------------------------------------------\n\r" );
	for ( target = first_planet; target; target = target->next )
	{
	    if ( !target )
		return;
	    if ( !str_cmp( "The Afterlife", target->name ) )
		continue;
	    pager_printf( ch, "  - %s\n\r", target->name );
	}
	return;
    }
    target = get_planet( argument );
    if ( !target  || !strcmp( target->name, "The Afterlife" ))
    {
	ch_printf( ch, "Target not found.\n\r" );
	return;
    }
    ch_printf( ch, "   &GActive Powerlevels on &C%s:&G\n\r", target->name );
    for ( c = first_char; c; c = c->next )
    {
	if ( !str_cmp( c->in_room->area->planet, target->name ) && !IS_NPC(c) )
        {
	   ch_printf( ch, "%-20s  Powerlevel: %s\n", c->name, format_pl(c->currpl) );
	}
    }

}
