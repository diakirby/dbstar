/*#include <stdlib.h>
#include <limits.h>
#include <sys/cdefs.h>
#include <sys/time.h>
#include <math.h>
#include "mud.h"*/

/* START QUADRANT DATA */

#define LO_Z       0
#define HI_Z       100

#define NQ_LO_X    0
#define NQ_HI_X    100
#define NQ_LO_Y    0
#define NQ_HI_Y    100

#define SQ_LO_X   -100
#define SQ_HI_X    0
#define SQ_LO_Y    100
#define SQ_HI_Y    0

#define EQ_LO_X    0
#define EQ_HI_X    100
#define EQ_LO_Y   -100
#define EQ_HI_Y    0

#define WQ_LO_X   -100
#define WQ_HI_X    0
#define WQ_LO_Y    0
#define WQ_HI_Y    100

/* END QUADRANT DATA */
/* START MISC, DIRECTORY AND FUNCTION DATA */

#define MAX_PLANET      50
#define MAX_SHIP        250  /* increase with player count */
#define MAX_SHIP_ROOMS  20

#define SHIP_DIR        "../space"
#define SHIP_LIST       "ship.lst"
#define PLANET_DIR      "../space"
#define PLANET_LIST     "planet.lst"

#define SHIP_DOCKED     0
#define SHIP_LAUNCHING  1
#define SHIP_FLYING     2
#define SHIP_PRE_WARP   3
#define SHIP_WARP       4
#define SHIP_POST_WARP  5
#define SHIP_LANDING    6
#define SHIP_DOCKING    7

#define SH      SHIP_DATA
#define PL      PLANET_DATA

/*
 * Func Decs here
 */

#undef SH
#undef PL

/* END DIRECTORY AND FUNCTION DATA */

/* We need an independant timing schedule for space.  Adjust to taste. */
#define PULSE_SPACE               ( 7 * PULSE_PER_SECOND)


typedef struct        planet_data          PLANET_DATA;
struct planet_data
{
  PLANET_DATA      * prev;
  PLANET_DATA      * next;
  PLANET_DATA      * last_in_system;
  PLANET_DATA      * next_in_system;
  AREA_DATA        * first_area;
  AREA_DATA        * last_area;
  char             * filename;
  char             * name;
  AREA_DATA        * area;
  char             * guardian;
  char             * hero;
  int                x;
  int                y;
  int                z;
  int                economy;
  int                houses;           /* Max number of houses possible */
  int                gravity;          /* Earth = 10.  Sliding scale factor with */
};                                     /* Pl gain consequences.  And instakill :P */
PLANET_DATA *first_planet;
PLANET_DATA *last_planet;



typedef struct        ship_data            SHIP_DATA;
struct ship_data
{
  SHIP_DATA *prev;
  SHIP_DATA *next;
  char             * filename;
  char             * name;
  char             * owner;
  int                cost;
  int                atx;
  int                aty;
  int                atz;
  int                tox;
  int                toy;
  int                toz;
  int                speed;
  int                sensor;
  int                fuel;
  int                max_fuel;
  int                bridge;
  int                entrance;
  int                state;
  char *             crew[9];
};
SHIP_DATA *first_ship;
SHIP_DATA *last_ship;


/*DECLARE_DO_FUN( do_planets    );
DECLARE_DO_FUN( do_makeplanet );
DECLARE_DO_FUN( do_showplanet );
DECLARE_DO_FUN( do_setplanet  );*/


void	    fread_planet	    args( ( PLANET_DATA *planet, FILE *fp ) );
bool	    load_planet_file	args( ( char *planetfile ) );
void	    write_planet_list	args( ( void ) );
void        save_planet         args( ( PLANET_DATA *planet ) );
void        load_planets        args( ( void ) );
AREA_DATA * get_area            args( (char *argument) );
char      * get_quad            args( ( int x, int y, int z ) );
char      * shipstate           args( ( int state ) );
void        tell_bridge         args( ( SHIP_DATA *ship, char * argument ) );
void        tell_pilot          args( ( SHIP_DATA *ship, char * argument ) );
