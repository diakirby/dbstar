/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 *			     Player skills module			    *
 ****************************************************************************/

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"
#include <math.h>


extern  char *  const           sec_flags[];
extern	int			get_secflag( char *flag );

char * const spell_flag[] =
{ "water", "earth", "air", "astral", "area", "distant", "reverse",
"noself", "defendable", "accumulative", "recastable", "noscribe",
"nobrew", "group", "object", "character", "secretskill", "pksensitive",
"stoponfail", "nofight", "nodispel", "randomtarget", "summon", "wizspell", "r4",
"r5", "r6", "r7", "r8", "r9", "r10", "r11"
};

char * const spell_saves[] =
{ "none", "poison_death", "wands", "para_petri", "breath", "spell_staff" };

char * const spell_save_effect[] =
{ "none", "negate", "eightdam", "quarterdam", "halfdam", "3qtrdam",
"reflect", "absorb" };

char * const spell_damage[] =
{ "none", "fire", "cold", "electricity", "energy", "acid", "poison", "drain" };

char * const spell_action[] =
{ "none", "create", "destroy", "resist", "suscept", "divinate", "obscure",
"change" };

char * const spell_power[] =
{ "none", "minor", "greater", "major" };

char * const spell_class[] =
{ "none", "lunar", "solar", "travel", "summon", "life", "death", "illusion" };

char * const target_type[] =
{ "ignore", "offensive", "defensive", "self", "objinv" };


extern char * const StyleNames[];

/*char *  const   StyleNames       [] =
{
  "Fighting", "Offensive", "Defensive", "Evasive", "Tank", "Ki-Lord", "Down-Right Fierce",
  "MENSA", "Berserker", "Tempest", "Indestructible Aura", "Mastermind", "Meathead",
  "Circular Weaponry", "Ghandi", "ADHD", "Dead-Angle", "Narcophobic", "Meltdown",
  "Nukeproofing", "Tira"
};*/

void discrete_ssj( CHAR_DATA *ch );
void show_char_to_char( CHAR_DATA *list, CHAR_DATA *ch );
void show_list_to_char( OBJ_DATA *list, CHAR_DATA *ch, bool fShort, 
	bool fShowN );
int next_style( CHAR_DATA *ch );
int ris_save( CHAR_DATA *ch, int chance, int ris );
bool check_illegal_psteal( CHAR_DATA *ch, CHAR_DATA *victim );

/* from magic.c */
void failed_casting( struct skill_type *skill, CHAR_DATA *ch,
		     CHAR_DATA *victim, OBJ_DATA *obj );

void deduct_energy( CHAR_DATA *ch, int cost );

int next_style( CHAR_DATA *ch )
{
  switch ( ch->style )
  {
	case STYLE_FIGHTING: 
		return STYLE_KILORD;
	case STYLE_OFFENSIVE:
		return STYLE_DOWNRIGHTFIERCE;
	case STYLE_DEFENSIVE:
		if ( ch->styleunlocked[STYLE_KILORD] == 1 && ch->styleunlocked[STYLE_EVASIVE] == 0 )
		    return STYLE_EVASIVE;
		else
		    return STYLE_TANK;
	case STYLE_EVASIVE:
		if ( ch->styleunlocked[STYLE_MENSA] == 1 && ch->styleunlocked[STYLE_MASTERMIND] == 0 )
		    return STYLE_MASTERMIND;
		else
		    return STYLE_GHANDI;
	case STYLE_TANK:
		return STYLE_INDESTRUCTIBLEAURA;
	case STYLE_KILORD:
		if ( ch->styleunlocked[STYLE_EVASIVE] == 0 )
		    return STYLE_EVASIVE;
		else
		    return STYLE_MENSA;
	case STYLE_DOWNRIGHTFIERCE:
		return STYLE_BERSERKER;
	case STYLE_MENSA:
		if ( ch->styleunlocked[STYLE_EVASIVE] == 1 && ch->styleunlocked[STYLE_MASTERMIND] == 0 )
		    return STYLE_MASTERMIND;
	case STYLE_BERSERKER:
		return STYLE_MEATHEAD;
	case STYLE_TEMPEST:
		return STYLE_CIRCULARWEAPONRY;
	case STYLE_INDESTRUCTIBLEAURA:
		return STYLE_NUKEPROOFING;
	case STYLE_MASTERMIND:
		if ( ch->styleunlocked[STYLE_ADHD] == 1 && ch->styleunlocked[STYLE_TEMPEST] == 0 )
		    return STYLE_TEMPEST;
		else
	            return STYLE_NARCOPHOBIC;
	case STYLE_MEATHEAD:
		return STYLE_ADHD;
	case STYLE_CIRCULARWEAPONRY:
		return STYLE_TIRA;
	case STYLE_GHANDI:
		return STYLE_DEADANGLE;
	case STYLE_ADHD:
		if ( ch->styleunlocked[STYLE_MASTERMIND] == 1 && ch->styleunlocked[STYLE_TEMPEST] == 0 )
		    return STYLE_TEMPEST;
		else
		    return -1;
	case STYLE_DEADANGLE:
		return STYLE_MELTDOWN;
	case STYLE_NARCOPHOBIC:
		return -1;
	case STYLE_MELTDOWN:
		return -1;
	case STYLE_NUKEPROOFING:
		return -1;
	case STYLE_TIRA:
		return -1;
	default: 
		return -1;
  }
}

void deduct_energy( CHAR_DATA *ch, int cost )
{
 ch->mana -= cost/2;
 if ( ch->mana < 0 )
    ch->mana = 0;
 return;
}

void ApplyEffect( CHAR_DATA *ch );
/*
 * Dummy function
 */
void skill_notfound( CHAR_DATA *ch, char *argument )
{
    send_to_char( "Huh?\n\r", ch );
    return;
}


int get_ssave( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_saves) / sizeof(spell_saves[0]); x++ )
      if ( !str_cmp( name, spell_saves[x] ) )
        return x;
    return -1;
}

int get_starget( char *name )
{
    int x;

    for ( x = 0; x < sizeof(target_type) / sizeof(target_type[0]); x++ )
      if ( !str_cmp( name, target_type[x] ) )
        return x;
    return -1;
}

int get_sflag( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_flag) / sizeof(spell_flag[0]); x++ )
      if ( !str_cmp( name, spell_flag[x] ) )
        return x;
    return -1;
}

int get_sdamage( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_damage) / sizeof(spell_damage[0]); x++ )
      if ( !str_cmp( name, spell_damage[x] ) )
        return x;
    return -1;
}

int get_saction( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_action) / sizeof(spell_action[0]); x++ )
      if ( !str_cmp( name, spell_action[x] ) )
        return x;
    return -1;
}

int get_ssave_effect( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_save_effect) / sizeof(spell_save_effect[0]); x++ )
      if ( !str_cmp( name, spell_save_effect[x] ) )
        return x;
    return -1;
}

int get_spower( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_power) / sizeof(spell_power[0]); x++ )
      if ( !str_cmp( name, spell_power[x] ) )
        return x;
    return -1;
}

int get_sclass( char *name )
{
    int x;

    for ( x = 0; x < sizeof(spell_class) / sizeof(spell_class[0]); x++ )
      if ( !str_cmp( name, spell_class[x] ) )
        return x;
    return -1;
}

bool is_legal_kill(CHAR_DATA *ch, CHAR_DATA *vch)
{
  if ( IS_NPC(ch) || IS_NPC(vch) )
    return TRUE;
  if ( !IS_PKILL(ch) || !IS_PKILL(vch) )
    return FALSE;
  if ( ch->pcdata->clan && ch->pcdata->clan == vch->pcdata->clan )
    return FALSE;
  return TRUE;
}


extern char *target_name;	/* from magic.c */

/*
 * Perform a binary search on a section of the skill table
 * Each different section of the skill table is sorted alphabetically
 * Only match skills player knows				-Thoric
 */
bool check_skill( CHAR_DATA *ch, char *command, char *argument )
{
    CHAR_DATA *victim;
    int sn;
    int first = gsn_first_skill;
    int top   = gsn_first_weapon-1;
    int mana, blood;
    struct timeval time_used;


if ( IS_AFFECTED( ch, AFF_BERSERK ) )
{
   pager_printf( ch, "&RYou are currently out of control!  There is no stopping you in this state!\n\r" );
   return(FALSE);
}

ch->renzoku = FALSE;

    /* bsearch for the skill */
    for (;;)
    {
	sn = (first + top) >> 1;

	if ( LOWER(command[0]) == LOWER(skill_table[sn]->name[0])
	&&  !str_prefix(command, skill_table[sn]->name)
	&&  (skill_table[sn]->skill_fun || skill_table[sn]->spell_fun != spell_null)
        && ( can_use_skill(ch, 0, sn ) ) )
		break;
	if (first >= top)
	    return FALSE;
    	if (strcmp( command, skill_table[sn]->name) < 1)
	    top = sn - 1;
    	else
	    first = sn + 1;
    }

    ch->last_ability_time = time(NULL);

    if ( IS_AFFECTED( ch, AFF_CHARGING ) && sn != gsn_kicontrol && sn != gsn_charge )
    {
        pager_printf( ch, "&RYou stop charging.\n\r" );
        xREMOVE_BIT( ch->affected_by, AFF_CHARGING );
    }

    if ( IS_AFFECTED(ch, AFF_LSSJ) && ( sn == gsn_ssj || sn == gsn_ussj || sn == gsn_ssj2 || sn == gsn_ssj3 || sn == gsn_ssj4 ) )
    {
	send_to_char( "YOU ARE A GOD!  SUCH THINGS ARE BEYOND YOU!\n\r", ch );
	return TRUE;
    }
    if ( !check_pos( ch, skill_table[sn]->minimum_position ) )
	return TRUE;

    if ( IS_NPC(ch)
    &&  (IS_AFFECTED( ch, AFF_CHARM ) || IS_AFFECTED( ch, AFF_POSSESS )) )
    {
	send_to_char( "For some reason, you seem unable to perform that...\n\r", ch );
	act( AT_GREY,"$n wanders around aimlessly.", ch, NULL, NULL, TO_ROOM );
	return TRUE;
    }
 
    if ( ( victim = who_fighting(ch) ) != NULL  && IS_NPC( victim ) && xIS_SET( victim->act, ACT_SUPERGIRL ) )
	interpret( victim, skill_table[sn]->name );

    /* check if mana is required */
    if ( skill_table[sn]->min_mana )
    {
	mana = IS_NPC(ch) ? 0 : UMAX(skill_table[sn]->min_mana,
	   100 / ( 2 + ch->basepl - skill_table[sn]->race_level[ch->race] ) );
	blood = UMAX(1, (mana+4) / 8);     
	if ( !IS_NPC(ch) )
	{
		if ( ch->energy > 500 )
			mana = mana / 100 *(  ch->energy * 10); 
		else
			mana = mana / 100 *(  ch->energy ); 
	}
	if ( !IS_NPC(ch) && ch->mana < mana )
	{
	    send_to_char( "You don't have enough energy.\n\r", ch );
	    return TRUE;
	}
    }
    else
    {
	mana = 0;
	blood = 0;
    }
	if ( skill_table[sn]->race_adept[ch->race] < 10 && !IS_IMMORTAL(ch) && !IS_NPC(ch) )
	{
		send_to_char( "That's not meant for your kind.\n\r", ch );
		return( FALSE );
	}
    /* is the skill even set? */
	if ( ch->race != RACE_WIZARD && !IS_NPC(ch) && ch->pcdata->set[sn] == FALSE && skill_table[sn]->size > 0 && !IS_IMMORTAL(ch))
	{
		send_to_char( "You may want to set that skill first...\n\r", ch );
		return( FALSE);
	}
    /* Can they even use it?! */
	if ( !IS_NPC(ch) && ch->basepl < skill_table[sn]->race_level[ch->race] && !IS_IMMORTAL(ch))
	{
		send_to_char( "You need to be stronger for that one...\n\r", ch );
		return (FALSE);
	}
	if ( ch->position <= POS_SITTING )
	{
		send_to_char( "Get up first.\n\r", ch );
		return(FALSE);
	} 
    /*
     * Is this a real do-fun, or really a spell?
     */
    if ( !skill_table[sn]->skill_fun )
    {
	ch_ret retcode = rNONE;
	void *vo = NULL;
	CHAR_DATA *victim = NULL;
	OBJ_DATA *obj = NULL;

	target_name = "";

	switch ( skill_table[sn]->target )
	{
	default:
	    bug( "Check_skill: bad target for sn %d.", sn );
	    send_to_char( "Something went wrong...\n\r", ch );
	    return TRUE;

	case TAR_IGNORE:
	    vo = NULL;
	    if ( argument[0] == '\0' )
	    {
		if ( (victim=who_fighting(ch)) != NULL )
		    target_name = PERS( victim, ch );
	    }
	    else
		target_name = argument;
	    break;

	case TAR_CHAR_OFFENSIVE:
	    {
	      if ( argument[0] == '\0'
	      &&  (victim=who_fighting(ch)) == NULL )
	      {
		ch_printf( ch, "Confusion overcomes you as your '%s' has no target.\n\r", skill_table[sn]->name );
		return TRUE;
	      }
	      else
	      if ( argument[0] != '\0'
	      &&  (victim=get_char_room(ch, argument)) == NULL )
	      {
		send_to_char( "They aren't here.\n\r", ch );
		return TRUE;
	      }
	    }
	    if ( is_safe( ch, victim, TRUE ) )
		return TRUE;

	    if ( ch == victim && SPELL_FLAG(skill_table[sn], SF_NOSELF))
            {
                send_to_char( "You can't target yourself!\n\r", ch);
                return TRUE;
            }

	    if ( !IS_NPC(ch) )
	    {
		if ( !IS_NPC(victim) )
		{
		    /*  Sheesh! can't do anything
		    send_to_char( "You can't do that on a player.\n\r", ch );
		    return TRUE;
	            */	
/*
		    if ( xIS_SET(victim->act, PLR_PK))
*/
		    if ( get_timer( ch, TIMER_PKILLED ) > 0 )
		    {
			send_to_char( "You have been killed in the last 5 minutes.\n\r", ch);
			return TRUE;
		    }

		    if ( get_timer( victim, TIMER_PKILLED ) > 0 )
		    {
			send_to_char( "This player has been killed in the last 5 minutes.\n\r", ch );
			return TRUE;
		    }	

		    if ( victim != ch)
		        send_to_char( "You really shouldn't do this to another player...\n\r", ch );
		}

		if ( IS_AFFECTED(ch, AFF_CHARM) && ch->master == victim )
		{
		    send_to_char( "You can't do that on your own follower.\n\r", ch );
		    return TRUE;
		}
	    }

	    check_illegal_pk( ch, victim );
	    vo = (void *) victim;
	    break;

	case TAR_CHAR_DEFENSIVE:
	    {
	      if ( argument[0] != '\0'
	      &&  (victim=get_char_room(ch, argument)) == NULL )
	      {
		send_to_char( "They aren't here.\n\r", ch );
		return TRUE;
	      }
	      if ( !victim )
		victim = ch;
	    }

	    if ( ch == victim && SPELL_FLAG(skill_table[sn], SF_NOSELF))
            {
                send_to_char( "You can't target yourself!\n\r", ch);
                return TRUE;
            }

	    vo = (void *) victim;
	    break;

	case TAR_CHAR_SELF:
	    vo = (void *) ch;
	    break;

	case TAR_OBJ_INV:
	    {
	      if ( (obj=get_obj_carry(ch, argument)) == NULL )
	      {
		send_to_char( "You can't find that.\n\r", ch );
		return TRUE;
	      }
	    }
	    vo = (void *) obj;
	    break;
	}

	/* waitstate */
	WAIT_STATE( ch, skill_table[sn]->beats );
	/* check for failure */
	if ( (number_percent( ) + skill_table[sn]->difficulty * 5)
	   > (IS_NPC(ch) ? 75 : LEARNED(ch, sn)) )
	{
	    failed_casting( skill_table[sn], ch, vo, obj );
	    learn_from_failure( ch, sn );
/*	    if ( mana )
	    {
		ch->mana -= mana/2;
		if ( ch->mana < 0 )
			ch->mana = 0;
	    }*/
	    return TRUE;
	}
/*	if ( mana )
	{
	    ch->mana -= mana;
		if ( ch->mana < 0 )
			ch->mana = 0;
	}*/
	start_timer(&time_used);
	retcode = (*skill_table[sn]->spell_fun) ( sn, ch->level, ch, vo );
	end_timer(&time_used);
	update_userec(&time_used, &skill_table[sn]->userec);
	
	if ( retcode == rCHAR_DIED || retcode == rERROR )
	    return TRUE;

	if ( char_died(ch) )
	    return TRUE;

	if ( retcode == rSPELL_FAILED )
	{
	    learn_from_failure( ch, sn );
	    retcode = rNONE;
	}
	else
	    learn_from_success( ch, sn );

	if ( skill_table[sn]->target == TAR_CHAR_OFFENSIVE
	&&   victim != ch
	&&  !char_died(victim) )
	{
	    CHAR_DATA *vch;
	    CHAR_DATA *vch_next;

	    for ( vch = ch->in_room->first_person; vch; vch = vch_next )
	    {
		vch_next = vch->next_in_room;
		if ( victim == vch && !victim->fighting && victim->master != ch )
		{
		    retcode = multi_hit( victim, ch, TYPE_UNDEFINED );
		    break;
		}
	    }
	}
	return TRUE;
    }
/*
    if ( mana )
    {
	ch->mana -= mana;
                if ( ch->mana < 0 )
                        ch->mana = 0;

    }*/
    ch->prev_cmd = ch->last_cmd;    /* haus, for automapping */
    ch->last_cmd = skill_table[sn]->skill_fun;
    start_timer(&time_used);
    (*skill_table[sn]->skill_fun) ( ch, argument );
    end_timer(&time_used);
    update_userec(&time_used, &skill_table[sn]->userec);
    tail_chain( );
    return TRUE;
}

void do_skin( CHAR_DATA *ch, char *argument)
{
    OBJ_DATA *corpse;
//    OBJ_DATA *obj;
    OBJ_DATA *skin;
    bool found;
    char *name;
    char buf[MAX_STRING_LENGTH];
    found = FALSE;


    if ( argument[0] == '\0' )
    {
        send_to_char( "Whose corpse do you wish to skin?\n\r", ch );
        return;
    }
    if ( (corpse=get_obj_here(ch, argument)) == NULL )
    {
        send_to_char( "You cannot find that here.\n\r", ch );
        return;
    }

/*    if ( obj->value[3] != 1
    &&   obj->value[3] != 0 )*/
	if ( corpse->item_type != ITEM_CORPSE_PC &&  corpse->item_type != ITEM_CORPSE_NPC )
    {
        send_to_char( "There is nothing you can do with this corpse.\n\r", ch );
        return;
    }

    if ( get_obj_index( 75 ) == NULL )
    {
      bug( "Vnum 75 (OBJ_VNUM_SKIN) not found for do_skin!", 0);
      return;
    }

    name                = corpse->name + 7;

    if ( !str_cmp( ch->name, name ) )
    {
	send_to_char( "Skin your own corpse?!", ch );
	return;
    }

    separate_obj(corpse);

    if ( !can_use_skill( ch, number_percent(), gsn_skin ) || strstr( name, "headless" ) )
    {
	extract_obj( corpse );
	send_to_char( "You fumble with the corpse and spill it open messily... it's ruined.\n\r", ch );
	return;
    }

    skin                = create_object( get_obj_index(75), 0 );
    ch->pcdata->learned[gsn_skin] ++;
    if ( ch->pcdata->learned[gsn_skin] > 75 )
	ch->pcdata->learned[gsn_skin] --;
//    learn_from_success( ch, gsn_skin );

    sprintf( buf, "the skin of %s", name );
    STRFREE( skin->short_descr );
    skin->short_descr = STRALLOC( buf );

    sprintf( buf, "A pile of skin lies here... it looks a little like %s.", name );
    STRFREE( skin->description );
    skin->description = STRALLOC( buf );

    sprintf( buf, "skin %s", name );
    STRFREE( skin->name );
    skin->name = STRALLOC( buf );

    act( AT_BLOOD, "$n strips the skin from a corpse.", ch, NULL, NULL, TO_CANSEE);
    act( AT_BLOOD, "You strip the skin from the corpse.", ch, NULL, NULL, TO_CHAR);
    act( AT_MAGIC, "\nThe skinless corpse is dragged through the ground by a strange force...", ch, NULL, NULL, TO_CHAR);
    act( AT_MAGIC, "\nThe skinless corpse is dragged through the ground by a strange force...", ch, NULL, NULL,TO_CANSEE);
    extract_obj( corpse ); 
    obj_to_char( skin, ch );
    law_broken(ch, 3);
    return;
}


void do_behead( CHAR_DATA *ch, char *argument)
{
    OBJ_DATA *corpse;
    OBJ_DATA *head;
    CLAN_DATA *clan;
    bool found;
    char *name;
    char buf[MAX_STRING_LENGTH];
    found = FALSE;


    if ( argument[0] == '\0' )
    {
        send_to_char( "Whose corpse do you wish to behead?\n\r", ch );
        return;
    }
    if ( (corpse=get_obj_here(ch, argument)) == NULL )
    {
        send_to_char( "You cannot find that here.\n\r", ch );
        return;
    }

    if ( corpse->item_type != ITEM_CORPSE_PC )
    {
        send_to_char( "There is nothing you can do with this corpse.\n\r", ch );
        return;
    }

    if ( strstr( corpse->name, "headless" ) )
    {
        send_to_char( "The corpse is without a head already...\n\r", ch );
        return;
    }

    if ( get_obj_index( OBJ_VNUM_SEVERED_HEAD ) == NULL )
    {
      bug( "(OBJ_VNUM_SEVERED_HEAD) not found for do_behead!", 0);
      return;
    }

    name = corpse->name + 7;

    if ( !str_cmp( ch->name, name ) )
    {
	send_to_char( "Behead your own corpse?!", ch );
	return;
    }

    if ( ch->level < 50 )
    {
	bool found = FALSE;
        for ( clan = first_clan; clan; clan = clan->next )
	{
            if ( !str_cmp( clan->leader, name ) )
	    {
                found = TRUE;
		break;
	    }
            if ( !str_cmp( clan->number1, name ) )
	    {
                found = TRUE;
		break;
	    }
            if ( !str_cmp( clan->number2, name ) )
	    {
                found = TRUE;
		break;
	    }	
	}
	if ( !found )
	{
	    send_to_char( "But that's not the corpse of anyone remotely important...\n\r", ch );
	    return;
	}
    }


    sprintf( buf, "corpse %s headless", name );
    STRFREE(corpse->name);
    corpse->name = STRALLOC( buf );
    sprintf( buf, "the headless corpse of %s", name );
    STRFREE(corpse->short_descr);
    corpse->short_descr = STRALLOC( buf );
    sprintf( buf, "A headless corpse lies here... was it once %s?", name );
    STRFREE(corpse->description);
    corpse->description = STRALLOC( buf );

    head            = create_object( get_obj_index(OBJ_VNUM_SEVERED_HEAD), 0 );
    sprintf( buf, "the head of %s", name );
    STRFREE( head->short_descr );
    head->short_descr = STRALLOC( buf );
    sprintf( buf, "A head lies here, vaguely resembling %s.", name );
    STRFREE( head->description );
    head->description = STRALLOC( buf );
    sprintf( buf, "head %s", name );
    STRFREE( head->name );
    head->name = STRALLOC( buf );

    act( AT_BLOOD, "$n rips the head from a corpse with $s bare hands!", ch, NULL, NULL, TO_CANSEE);
    act( AT_BLOOD, "Showering blood everywhere, you tear the head from the corpse!", ch, NULL, NULL, TO_CHAR);

    // MUST FIX RESETTING BEHEAD BUGS: SHOULD MAKE A NEW CORPSE OBJECT (BEHEADED)

    law_broken(ch, 4);

    obj_to_char( head, ch );
    return;
}


/*{
    OBJ_DATA *korps;
    OBJ_DATA *corpse;
    OBJ_DATA *obj;
    OBJ_DATA *skin;
    bool found;
    char *name;
    char buf[MAX_STRING_LENGTH];
    found = FALSE;
 
    if ( !IS_PKILL(ch) && !IS_IMMORTAL(ch) )
    {
        send_to_char( "Leave the hideous defilings to the killers!\n", ch );
        return;
    }
    if ( argument[0] == '\0' )
    { 
        send_to_char( "Whose corpse do you wish to skin?\n\r", ch );
        return;
    }
    if ( (corpse=get_obj_here(ch, argument)) == NULL )
    {
	send_to_char( "You cannot find that here.\n\r", ch );
	return;
    }
    if ( (obj=get_eq_char(ch, WEAR_WIELD)) == NULL )
    {
        send_to_char( "You have no weapon with which to perform this deed.\n\r", ch );
        return;
    }
    if ( corpse->item_type != ITEM_CORPSE_PC )
    {
        send_to_char( "You can only skin the bodies of player characters.\n\r", ch);
        return;
    }
    if ( obj->value[3] != 1
    &&   obj->value[3] != 2
    &&   obj->value[3] != 3
    &&   obj->value[3] != 11 )
    {
        send_to_char( "There is nothing you can do with this corpse.\n\r", ch );
        return;
    }
    if ( get_obj_index( OBJ_VNUM_SKIN ) == NULL )
    {
      bug( "Vnum 23 (OBJ_VNUM_SKIN) not found for do_skin!", 0);
      return;
    }
    korps               = create_object( get_obj_index(OBJ_VNUM_CORPSE_PC), 0 );
    skin                = create_object( get_obj_index(OBJ_VNUM_SKIN), 0 );
    name                = IS_NPC(ch) ? korps->short_descr : corpse->short_descr;
    sprintf( buf, skin->short_descr, name );
    STRFREE( skin->short_descr );
    skin->short_descr = STRALLOC( buf );
    sprintf( buf, skin->description, name );
    STRFREE( skin->description );
    skin->description = STRALLOC( buf );
    act( AT_BLOOD, "$n strips the skin from $p.", ch, corpse, NULL, TO_ROOM);
    act( AT_BLOOD, "You strip the skin from $p.", ch, corpse, NULL, TO_CHAR);
  act( AT_MAGIC, "\nThe skinless corpse is dragged through the ground by a strange force...", ch, corpse, NULL, TO_CHAR);
    act( AT_MAGIC, "\nThe skinless corpse is dragged through the ground by a strange force...", ch, corpse, NULL, TO_ROOM);
    extract_obj( corpse ); 
    obj_to_char( skin, ch );
    return;
}*/

/*
 * Lookup a skills information
 * High god command
 */
void do_slookup( CHAR_DATA *ch, char *argument )
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    int sn;
    int iRace;
    SKILLTYPE *skill = NULL;

    one_argument( argument, arg );
    if ( arg[0] == '\0' )
    {
	send_to_char( "Slookup what?\n\r", ch );
	return;
    }

    if ( !str_cmp( arg, "all" ) )
    {
	for ( sn = 0; sn < top_sn && skill_table[sn] && skill_table[sn]->name; sn++ )
	    pager_printf( ch, "Sn: %4d Slot: %4d Skill/spell: '%-20s' Damtype: %s\n\r",
		sn, skill_table[sn]->slot, skill_table[sn]->name,
		spell_damage[SPELL_DAMAGE( skill_table[sn] )] );
    }
    else
    if ( !str_cmp( arg, "herbs" ) )
    {
	for ( sn = 0; sn < top_herb && herb_table[sn] && herb_table[sn]->name; sn++ )
	   pager_printf( ch, "%d) %s\n\r", sn, herb_table[sn]->name );
    }
    else
    {
	SMAUG_AFF *aff;
	int cnt = 0;

	if ( arg[0] == 'h' && is_number(arg+1) )
	{
	    sn = atoi(arg+1);
	    if ( !IS_VALID_HERB(sn) )
	    {
		send_to_char( "Invalid herb.\n\r", ch );
		return;
	    }
	    skill = herb_table[sn];
	}
	else
	if ( is_number(arg) )
	{
	    sn = atoi(arg);
	    if ( (skill=get_skilltype(sn)) == NULL )
	    {
		send_to_char( "Invalid sn.\n\r", ch );
		return;
	    }
	    sn %= 1000;
	}
	else
	if ( ( sn = skill_lookup( arg ) ) >= 0 )
	    skill = skill_table[sn];
	else
	if ( ( sn = herb_lookup( arg ) ) >= 0 )
	    skill = herb_table[sn];
	else
	{
	    send_to_char( "No such skill, spell, proficiency or tongue.\n\r", ch );
	    return;
	}
	if ( !skill )
	{
	    send_to_char( "Not created yet.\n\r", ch );
	    return;
	}

	ch_printf( ch, "Sn: %4d Slot: %4d %s: '%-20s'  Size %d\n\r",
	    sn, skill->slot, skill_tname[skill->type], skill->name, skill->size );
	if ( skill->info )
	    ch_printf( ch, "DamType: %s  ActType: %s   ClassType: %s   PowerType: %s\n\r",
		spell_damage[SPELL_DAMAGE(skill)],
		spell_action[SPELL_ACTION(skill)],
		spell_class[SPELL_CLASS(skill)],
		spell_power[SPELL_POWER(skill)] );
	if ( skill->flags )
	{
	    int x;

	    strcpy( buf, "Flags:" );
	    for ( x = 0; x < 32; x++ )
	      if ( SPELL_FLAG( skill, 1 << x ) )
	      {
		strcat( buf, " " );
		strcat( buf, spell_flag[x] );
	      }
	    strcat( buf, "\n\r" );
	    send_to_char( buf, ch );
	}
	ch_printf( ch, "Saves: %s  SaveEffect: %s\n\r",
	    spell_saves[(int) skill->saves],
	    spell_save_effect[SPELL_SAVE(skill)] );

	if ( skill->difficulty != '\0' )
	    ch_printf( ch, "Difficulty: %d\n\r", (int) skill->difficulty );

	ch_printf( ch, "Type: %s  Target: %s  Minpos: %d  Mana: %d  Beats: %d  Range: %d\n\r",
		skill_tname[skill->type],
		target_type[URANGE(TAR_IGNORE, skill->target, TAR_OBJ_INV)],
		skill->minimum_position,
		skill->min_mana, skill->beats, skill->range );
	ch_printf( ch, "Flags: %d  Guild: %d  Value: %d  Info: %d  Code: %s\n\r",
		skill->flags,
		skill->guild,
		skill->value,
		skill->info,
		skill->skill_fun ? skill_name(skill->skill_fun)
					   : spell_name(skill->spell_fun));
        ch_printf( ch, "Sectors Allowed: %s\n",
		skill->spell_sector?flag_string(skill->spell_sector,sec_flags):
			"All");
	ch_printf( ch, "Dammsg: %s\n\rWearoff: %s\n",
		skill->noun_damage,
		skill->msg_off ? skill->msg_off : "(none set)" );
	if ( skill->dice && skill->dice[0] != '\0' )
	    ch_printf( ch, "Dice: %s\n\r", skill->dice );
	if ( skill->teachers && skill->teachers[0] != '\0' )
	    ch_printf( ch, "Teachers: %s\n\r", skill->teachers );
	if ( skill->components && skill->components[0] != '\0' )
	    ch_printf( ch, "Components: %s\n\r", skill->components );
	if ( skill->participants )
	    ch_printf( ch, "Participants: %d\n\r", (int) skill->participants );
	if ( skill->userec.num_uses )
	    send_timer(&skill->userec, ch);
	for ( aff = skill->affects; aff; aff = aff->next )
	{
	    if ( aff == skill->affects )
	      send_to_char( "\n\r", ch );
	    sprintf( buf, "Affect %d", ++cnt );
	    if ( aff->location )
	    {
		strcat( buf, " modifies " );
		strcat( buf, a_types[aff->location % REVERSE_APPLY] );
		strcat( buf, " by '" );
		strcat( buf, aff->modifier );
		if ( aff->bitvector != -1 )
		  strcat( buf, "' and" );
		else
		  strcat( buf, "'" );
	    }
	    if ( aff->bitvector != -1 )
	    {
		strcat( buf, " applies " );
		strcat( buf, a_flags[aff->bitvector] );
	    }
	    if ( aff->duration[0] != '\0' && aff->duration[0] != '0' )
	    {
		strcat( buf, " for '" );
		strcat( buf, aff->duration );
		strcat( buf, "' rounds" );
	    }
	    if ( aff->location >= REVERSE_APPLY )
		strcat( buf, " (affects caster only)" );
	    strcat( buf, "\n\r" );
	    send_to_char( buf, ch );
	    
	    if ( !aff->next )
	      send_to_char( "\n\r", ch );
	}

	if ( skill->hit_char && skill->hit_char[0] != '\0' )
	    ch_printf( ch, "Hitchar   : %s\n\r", skill->hit_char );
	if ( skill->hit_vict && skill->hit_vict[0] != '\0' )
	    ch_printf( ch, "Hitvict   : %s\n\r", skill->hit_vict );
	if ( skill->hit_room && skill->hit_room[0] != '\0' )
	    ch_printf( ch, "Hitroom   : %s\n\r", skill->hit_room );
	if ( skill->hit_dest && skill->hit_dest[0] != '\0' )
	    ch_printf( ch, "Hitdest   : %s\n\r", skill->hit_dest );
	if ( skill->miss_char && skill->miss_char[0] != '\0' )
	    ch_printf( ch, "Misschar  : %s\n\r", skill->miss_char );
	if ( skill->miss_vict && skill->miss_vict[0] != '\0' )
	    ch_printf( ch, "Missvict  : %s\n\r", skill->miss_vict );
	if ( skill->miss_room && skill->miss_room[0] != '\0' )
	    ch_printf( ch, "Missroom  : %s\n\r", skill->miss_room );
	if ( skill->die_char && skill->die_char[0] != '\0' )
	    ch_printf( ch, "Diechar   : %s\n\r", skill->die_char );
	if ( skill->die_vict && skill->die_vict[0] != '\0' )
	    ch_printf( ch, "Dievict   : %s\n\r", skill->die_vict );
	if ( skill->die_room && skill->die_room[0] != '\0' )
	    ch_printf( ch, "Dieroom   : %s\n\r", skill->die_room );
	if ( skill->imm_char && skill->imm_char[0] != '\0' )
	    ch_printf( ch, "Immchar   : %s\n\r", skill->imm_char );
	if ( skill->imm_vict && skill->imm_vict[0] != '\0' )
	    ch_printf( ch, "Immvict   : %s\n\r", skill->imm_vict );
	if ( skill->imm_room && skill->imm_room[0] != '\0' )
	    ch_printf( ch, "Immroom   : %s\n\r", skill->imm_room );
	if ( skill->type != SKILL_HERB )
	{
/*            if(skill->type!=SKILL_RACIAL)
            {
               send_to_char( "--------------------------[CLASS USE]--------------------------\n\r",ch);
	       for ( iClass = 0; iClass < MAX_PC_CLASS; iClass++ )
	       {
		   strcpy( buf, class_table[iClass]->who_name );
		   sprintf(buf+3, ") lvl: %3d max: %2d%%",
				   skill->skill_level[iClass],
				   skill->skill_adept[iClass] );
		   if ( iClass % 3 == 2 )
			   strcat(buf, "\n\r" );
		   else
			   strcat(buf, "  " );
		   send_to_char( buf, ch );
	       }
            } else
            {
                 send_to_char( "\n\r--------------------------[RACE USE]--------------------------\n\r",ch);
	         for ( iRace = 0; iRace < MAX_PC_RACE; iRace++ )
	         {
		         sprintf(buf, "%8.8s) lvl: %3d max: %2d%%",
                            race_table[iRace]->race_name,
		            skill->race_level[iRace],
		            skill->race_adept[iRace] );
                         if( !strcmp(race_table[iRace]->race_name,"unused") )
                            sprintf(buf,"                           ");
		         if ( (iRace>0) && (iRace % 2 == 1 ))
		            strcat(buf, "\n\r" );
		         else
		            strcat(buf, "  " );
		     send_to_char( buf, ch );
	         }
	     }
*/
                 send_to_char( "\n\r-----------------------------[USE]----------------------------\n\r",ch);
	         for ( iRace = 0; iRace < MAX_PC_RACE; iRace++ )
	         {
		         sprintf(buf, "%8.8s) minpl: %llu max: %2d%%",
                            race_table[iRace]->race_name,
		            skill->race_level[iRace],
		            skill->race_adept[iRace] );
                         if( !strcmp(race_table[iRace]->race_name,"unused") )
                            sprintf(buf,"                           ");
		         if ( (iRace>0) && (iRace % 2 == 1 ))
		            strcat(buf, "\n\r" );
		         else
		            strcat(buf, "  " );
		     send_to_char( buf, ch );
	         }
	     }
         }
	 send_to_char( "\n\r", ch );
           
    return;
}


/*
 * Set a skill's attributes or what skills a player has.
 * High god command, with support for creating skills/spells/herbs/etc
 */
void do_sset( CHAR_DATA *ch, char *argument )
{
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int value;
//    unsigned long long value2;
    int sn,i;
    bool fAll;
    int x = 0;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );

    if ( arg1[0] == '\0' || arg2[0] == '\0' || argument[0] == '\0' )
    {
	send_to_char( "Syntax: sset <victim> <skill> <value>\n\r",	ch );
	send_to_char( "or:     sset <victim> all     <value>\n\r",	ch );
	if ( get_trust(ch) > LEVEL_SUB_IMPLEM )
	{
	  send_to_char( "or:     sset save skill table\n\r",		ch );
	  send_to_char( "or:     sset save herb table\n\r",		ch );
	  send_to_char( "or:     sset create skill 'new skill'\n\r",	ch );
	  send_to_char( "or:     sset create herb 'new herb'\n\r",	ch );
	  send_to_char( "or:     sset create ability 'new ability'\n\r",ch );
	}
	if ( get_trust(ch) > LEVEL_GREATER )
	{
	  send_to_char( "or:     sset <sn>     <field> <value>\n\r",	ch );
	  send_to_char( "\n\rField being one of:\n\r",			ch );
	  send_to_char( "  name code target minpos slot mana beats dammsg wearoff guild minpl\n\r", ch );
	  send_to_char( "  type damtype acttype classtype powertype seffect flag dice value difficulty\n\r", ch );
	  send_to_char( "  affect rmaffect hit miss die imm (char/vict/room)\n\r", ch );
	  send_to_char( "  components teachers racelevel adept\n\r",ch );
	  send_to_char( "  sector\n\r", ch );
	  send_to_char( "Affect having the fields: <location> <modfifier> [duration] [bitvector]\n\r", ch );
	  send_to_char( "(See AFFECTTYPES for location, and AFFECTED_BY for bitvector)\n\r", ch );
	}
	send_to_char( "Skill being any skill or spell.\n\r",		ch );
	return;
    }

    if ( get_trust(ch) > LEVEL_SUB_IMPLEM
    &&  !str_cmp( arg1, "save" )
    &&	!str_cmp( argument, "table" ) )
    {
	if ( !str_cmp( arg2, "skill" ) )
	{
	    send_to_char( "Saving skill table...\n\r", ch );
	    save_skill_table();
/*	    save_classes(); */
	    save_races();
      	    while( x <= 12 )
	    {	
		write_race_file(x);
		x++;
	    }
	    return;
	}
	if ( !str_cmp( arg2, "herb" ) )
	{
	    send_to_char( "Saving herb table...\n\r", ch );
	    save_herb_table();
	    return;
	}
    }
    if ( get_trust(ch) > LEVEL_SUB_IMPLEM
    &&  !str_cmp( arg1, "create" )
    && (!str_cmp( arg2, "skill" ) || !str_cmp( arg2, "herb" ) || !str_cmp( arg2, "ability" )) )
    {
	struct skill_type *skill;
	sh_int type = SKILL_UNKNOWN;

	if ( !str_cmp( arg2, "herb" ) )
	{
	    type = SKILL_HERB;
	    if ( top_herb >= MAX_HERB )
	    {
		ch_printf( ch, "The current top herb is %d, which is the maximum.  "
			   "To add more herbs,\n\rMAX_HERB will have to be "
			   "raised in mud.h, and the mud recompiled.\n\r",
			   top_herb );
		return;
	    }
	}
	else
	if ( top_sn >= MAX_SKILL )
	{
	    ch_printf( ch, "The current top sn is %d, which is the maximum.  "
			   "To add more skills,\n\rMAX_SKILL will have to be "
			   "raised in mud.h, and the mud recompiled.\n\r",
			   top_sn );
	    return;
	}
	CREATE( skill, struct skill_type, 1 );
	skill->slot = 0;
	if ( type == SKILL_HERB )
	{
	    int max, x;

	    herb_table[top_herb++] = skill;
	    for ( max = x = 0; x < top_herb-1; x++ )
		if ( herb_table[x] && herb_table[x]->slot > max )
		    max = herb_table[x]->slot;
	    skill->slot = max+1;
	}
	else
	    skill_table[top_sn++] = skill;
	skill->min_mana = 0;
	skill->name = str_dup( argument );
	skill->noun_damage = str_dup( "" );
	skill->msg_off = str_dup( "" );
	skill->spell_fun = spell_smaug;
	skill->size = 1;
	skill->form = FALSE;
	skill->defendable = TRUE;
	skill->type = type;
	skill->spell_sector = 0;
	skill->guild = -1;
        if (!str_cmp( arg2, "ability" ) )
          skill->type  = SKILL_RACIAL;

        for(i=0;i<MAX_PC_CLASS;i++)
        {
             skill->skill_level[i]= 0;
             skill->skill_adept[i]= 0;
        }
        for(i=0;i<MAX_PC_RACE;i++)
        {
             skill->race_level[i]= 0;
             skill->race_adept[i]= 0;
        }

	send_to_char( "Done.\n\r", ch );
	return;
    }

    if ( arg1[0] == 'h' )
	sn = atoi( arg1+1 );
    else
	sn = atoi( arg1 );
    if ( get_trust(ch) > LEVEL_GREATER
    && ((arg1[0] == 'h' && is_number(arg1+1) && (sn=atoi(arg1+1))>=0)
    ||  (is_number(arg1) && (sn=atoi(arg1)) >= 0)) )
    {
	struct skill_type *skill;

	if ( arg1[0] == 'h' )
	{
	    if ( sn >= top_herb )
	    {
		send_to_char( "Herb number out of range.\n\r", ch );
		return;
	    }
	    skill = herb_table[sn];
	}
	else
	{
	    if ( (skill=get_skilltype(sn)) == NULL )
	    {
		send_to_char( "Skill number out of range.\n\r", ch );
		return;
	    }
	    sn %= 1000;
	}

	if ( !str_cmp( arg2, "difficulty" ) )
	{
	    skill->difficulty = atoi( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "form" ) )
	{
	if ( atoi(argument) == 0 )
	    skill->form = FALSE;
	else
	    skill->form = TRUE;
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "defend" ) )
	{
	if ( atoi(argument) == 0 )
	    skill->defendable = FALSE;
	else
	    skill->defendable = TRUE;
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "size" ) )
	{
	    if ( atoi(argument ) > 5 || atoi(argument) < 0 )
		{
		send_to_char( "Range is 0 to 5\n\r", ch );
		return;
		}
	    skill->size = atoi( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "participants" ) )
	{
	    skill->participants = atoi( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "damtype" ) )
	{
	    int x = get_sdamage( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell damage type.\n\r", ch );
	    else
	    {
		SET_SDAM( skill, x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "acttype" ) )
	{
	    int x = get_saction( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell action type.\n\r", ch );
	    else
	    {
		SET_SACT( skill, x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "classtype" ) )
	{
	    int x = get_sclass( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell class type.\n\r", ch );
	    else
	    {
		SET_SCLA( skill, x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "powertype" ) )
	{
	    int x = get_spower( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell power type.\n\r", ch );
	    else
	    {
		SET_SPOW( skill, x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "seffect" ) )
	{
	    int x = get_ssave_effect( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell save effect type.\n\r", ch );
	    else
	    {
		SET_SSAV( skill, x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "flag" ) )
	{
	    int x = get_sflag( argument );

	    if ( x == -1 )
		send_to_char( "Not a spell flag.\n\r", ch );
	    else
	    {
		TOGGLE_BIT( skill->flags, 1 << x );
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "saves" ) )
	{
	    int x = get_ssave( argument );

	    if ( x == -1 )
		send_to_char( "Not a saving type.\n\r", ch );
	    else
	    {
		skill->saves = x;
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}

	if ( !str_cmp( arg2, "code" ) )
	{
	    SPELL_FUN *spellfun;
	    DO_FUN    *dofun;
		
	    if ( (spellfun=spell_function(argument)) != spell_notfound )
	    {
		skill->spell_fun = spellfun;
		skill->skill_fun = NULL;
	    }
	    else
	    if ( (dofun=skill_function(argument)) != skill_notfound )
	    {
		skill->skill_fun = dofun;
		skill->spell_fun = NULL;
	    }
	    else
	    {
		send_to_char( "Not a spell or skill.\n\r", ch );
		return;
	    }
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}

	if ( !str_cmp( arg2, "target" ) )
	{
	    int x = get_starget( argument );

	    if ( x == -1 )
		send_to_char( "Not a valid target type.\n\r", ch );
	    else
	    {
		skill->target = x;
		send_to_char( "Ok.\n\r", ch );
	    }
	    return;
	}
	if ( !str_cmp( arg2, "minpos" ) )
	{
	    skill->minimum_position = URANGE( POS_DEAD, atoi( argument ), POS_DRAG );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "minpl" ) )
	{
	    skill->min_level = atof( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "sector") )
	{
	    char tmp_arg[MAX_STRING_LENGTH];

	    while ( argument[0] != '\0' )
	    {
	       argument = one_argument( argument, tmp_arg );
	       value = get_secflag( tmp_arg );
	       if ( value < 0 || value > MAX_SECFLAG )
	          ch_printf( ch, "Unknown flag: %s\n\r", tmp_arg );
	       else
	          TOGGLE_BIT( skill->spell_sector, (1 << value) );
            }
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "slot" ) )
	{
	    skill->slot = URANGE( 0, atoi( argument ), 30000 );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "ki" ) )
	{
            int ki = atoi(argument)/20;
	    skill->min_mana = URANGE( 0, ki, 10000000 );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "beats" ) )
	{
	    skill->beats = URANGE( 0, atoi( argument ), 120 );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "range" ) )
	{
	    skill->range = URANGE( 0, atoi( argument ), 20 );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "guild" ) )
	{
	    skill->guild = atoi( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "value" ) )
	{
	    skill->value = atoi( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "type" ) )
	{
	    skill->type = get_skill( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "rmaffect" ) )
	{
	    SMAUG_AFF *aff = skill->affects;
	    SMAUG_AFF *aff_next;
	    int num = atoi( argument );
	    int cnt = 1;

	    if ( !aff )
	    {
		send_to_char( "This spell has no special affects to remove.\n\r", ch );
		return;
	    }
	    if ( num == 1 )
	    {
		skill->affects = aff->next;
		DISPOSE( aff->duration );
		DISPOSE( aff->modifier );
		DISPOSE( aff );
		send_to_char( "Removed.\n\r", ch );
		return;
	    }
	    for ( ; aff; aff = aff->next )
	    {
		if ( ++cnt == num && (aff_next=aff->next) != NULL )
		{
		    aff->next = aff_next->next;
		    DISPOSE( aff_next->duration );
		    DISPOSE( aff_next->modifier );
		    DISPOSE( aff_next );
		    send_to_char( "Removed.\n\r", ch );
		    return;
		}
	    }
	    send_to_char( "Not found.\n\r", ch );
	    return;
	}
	/*
	 * affect <location> <modifier> <duration> <bitvector>
	 */
	if ( !str_cmp( arg2, "affect" ) )
	{
	    char location[MAX_INPUT_LENGTH];
	    char modifier[MAX_INPUT_LENGTH];
	    char duration[MAX_INPUT_LENGTH];
/*	    char bitvector[MAX_INPUT_LENGTH];	*/
	    int loc, bit, tmpbit;
	    SMAUG_AFF *aff;

	    argument = one_argument( argument, location );
	    argument = one_argument( argument, modifier );
	    argument = one_argument( argument, duration );
	    
	    if ( location[0] == '!' )
		loc = get_atype( location+1 ) + REVERSE_APPLY;
	    else
		loc = get_atype( location );
	    if ( (loc % REVERSE_APPLY) < 0
	    ||   (loc % REVERSE_APPLY) >= MAX_APPLY_TYPE )
	    {
		send_to_char( "Unknown affect location.  See AFFECTTYPES.\n\r", ch );
		return;
	    }
	    bit = -1;
	    if ( argument[0] != '\0' )
	    {
		if ( (tmpbit=get_aflag(argument)) == -1 )
		  ch_printf( ch, "Unknown bitvector: %s.  See AFFECTED_BY\n\r", argument );
		else
		  bit = tmpbit;
	    }
	    CREATE( aff, SMAUG_AFF, 1 );
	    if ( !str_cmp( duration, "0" ) )
	      duration[0] = '\0';
	    if ( !str_cmp( modifier, "0" ) )
	      modifier[0] = '\0';
	    aff->duration = str_dup( duration );
	    aff->location = loc;
	    aff->modifier = str_dup( modifier );
	    aff->bitvector = bit;
	    aff->next = skill->affects;
	    skill->affects = aff;
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
/*	if ( !str_cmp( arg2, "level" ) )
	{
	    char arg3[MAX_INPUT_LENGTH];
	    int class;

	    argument = one_argument( argument, arg3 );
	    class = atoi( arg3 );
	    if ( class >= MAX_PC_CLASS || class < 0 )
		send_to_char( "Not a valid class.\n\r", ch );
	    else
		skill->skill_level[class] =
			URANGE(0, atoi(argument), MAX_LEVEL);
	    return;
        } */
	if ( !str_cmp( arg2, "racelevel" ) )
	{
	    char arg3[MAX_INPUT_LENGTH];
	    int race;

	    argument = one_argument( argument, arg3 );
	    race = atoll( arg3 );
	    if ( race >= MAX_PC_RACE || race < 0 )
		send_to_char( "Not a valid race.\n\r", ch );
	    else
		skill->race_level[race] = atof(argument);
	    return;
	}
/*	if ( !str_cmp( arg2, "adept" ) )
	{
	    char arg3[MAX_INPUT_LENGTH];
	    int class;

	    argument = one_argument( argument, arg3 );
	    class = atoi( arg3 );
	    if ( class >= MAX_PC_CLASS || class < 0 )
		send_to_char( "Not a valid class.\n\r", ch );
	    else
		skill->skill_adept[class] =
			URANGE(0, atoi(argument), 100);
	    return;
	} */
	if ( !str_cmp( arg2, "adept" ) )
	{
	    char arg3[MAX_INPUT_LENGTH];
	    int race;

	    argument = one_argument( argument, arg3 );
	    race = atoi( arg3 );
	    if ( race >= MAX_PC_RACE || race < 0 )
		send_to_char( "Not a valid race.\n\r", ch );
	    else
		skill->race_adept[race] = atoi(argument);
	    return;
	}


	if ( !str_cmp( arg2, "name" ) )
	{
	    DISPOSE(skill->name);
	    skill->name = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "dammsg" ) )
	{
	    DISPOSE(skill->noun_damage);
	    if ( !str_cmp( argument, "clear" ) )
//	      skill->noun_damage = str_dup( "" );
	      skill->noun_damage = str_dup( skill->name );
	    else
	      skill->noun_damage = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "wearoff" ) )
	{
	    DISPOSE(skill->msg_off);
	    if ( str_cmp( argument, "clear" ) )
	      skill->msg_off = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "hitchar" ) )
	{
	    if ( skill->hit_char )
	      DISPOSE(skill->hit_char);
	    if ( str_cmp( argument, "clear" ) )
	      skill->hit_char = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "hitvict" ) )
	{
	    if ( skill->hit_vict )
	      DISPOSE(skill->hit_vict);
	    if ( str_cmp( argument, "clear" ) )
	      skill->hit_vict = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "hitroom" ) )
	{
	    if ( skill->hit_room )
	      DISPOSE(skill->hit_room);
	    if ( str_cmp( argument, "clear" ) )
	      skill->hit_room = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "hitdest" ) )
	{
	    if ( skill->hit_dest )
	      DISPOSE(skill->hit_dest);
	    if ( str_cmp( argument, "clear" ) )
	      skill->hit_dest = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "misschar" ) )
	{
	    if ( skill->miss_char )
	      DISPOSE(skill->miss_char);
	    if ( str_cmp( argument, "clear" ) )
	      skill->miss_char = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "missvict" ) )
	{
	    if ( skill->miss_vict )
	      DISPOSE(skill->miss_vict);
	    if ( str_cmp( argument, "clear" ) )
	      skill->miss_vict = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "missroom" ) )
	{
	    if ( skill->miss_room )
	      DISPOSE(skill->miss_room);
	    if ( str_cmp( argument, "clear" ) )
	      skill->miss_room = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "diechar" ) )
	{
	    if ( skill->die_char )
	      DISPOSE(skill->die_char);
	    if ( str_cmp( argument, "clear" ) )
	      skill->die_char = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "dievict" ) )
	{
	    if ( skill->die_vict )
	      DISPOSE(skill->die_vict);
	    if ( str_cmp( argument, "clear" ) )
	      skill->die_vict = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "dieroom" ) )
	{
	    if ( skill->die_room )
	      DISPOSE(skill->die_room);
	    if ( str_cmp( argument, "clear" ) )
	      skill->die_room = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "immchar" ) )
	{
	    if ( skill->imm_char )
	      DISPOSE(skill->imm_char);
	    if ( str_cmp( argument, "clear" ) )
	      skill->imm_char = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "immvict" ) )
	{
	    if ( skill->imm_vict )
	      DISPOSE(skill->imm_vict);
	    if ( str_cmp( argument, "clear" ) )
	      skill->imm_vict = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "immroom" ) )
	{
	    if ( skill->imm_room )
	      DISPOSE(skill->imm_room);
	    if ( str_cmp( argument, "clear" ) )
	      skill->imm_room = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "dice" ) )
	{
	    if ( skill->dice )
	      DISPOSE(skill->dice);
	    if ( str_cmp( argument, "clear" ) )
	      skill->dice = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "components" ) )
	{
	    if ( skill->components )
	      DISPOSE(skill->components);
	    if ( str_cmp( argument, "clear" ) )
	      skill->components = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	if ( !str_cmp( arg2, "teachers" ) )
	{
	    if ( skill->teachers )
	      DISPOSE(skill->teachers);
	    if ( str_cmp( argument, "clear" ) )
	      skill->teachers = str_dup( argument );
	    send_to_char( "Ok.\n\r", ch );
	    return;
	}
	do_sset( ch, "" );
	return;
    }

    if ( ( victim = get_char_world( ch, arg1 ) ) == NULL )
    {
	if ( (sn = skill_lookup(arg1)) >= 0 )
	{
	    sprintf(arg1, "%d %s %s", sn, arg2, argument);
	    do_sset(ch, arg1);
	}
	else
	    send_to_char( "They aren't here.\n\r", ch );
	return;
    }

    if ( IS_NPC(victim) )
    {
	send_to_char( "Not on NPC's.\n\r", ch );
	return;
    }

    fAll = !str_cmp( arg2, "all" );
    sn   = 0;
    if ( !fAll && ( sn = skill_lookup( arg2 ) ) < 0 )
    {
	send_to_char( "No such skill or spell.\n\r", ch );
	return;
    }

    /*
     * Snarf the value.
     */
    if ( !is_number( argument ) )
    {
	send_to_char( "Value must be numeric.\n\r", ch );
	return;
    }

    value = atoi( argument );
    if ( value < 0 || value > 100 )
    {
	send_to_char( "Value range is 0 to 100.\n\r", ch );
	return;
    }

    if ( fAll )
    {
	for ( sn = 0; sn < top_sn; sn++ )
	{
            /* Fix by Narn to prevent ssetting skills the player shouldn't have. */ 
	    if ( skill_table[sn]->name 
            && ( victim->level >= skill_table[sn]->skill_level[victim->class] 
                      || value == 0 ) )
	    {
		if ( value == 100 && !IS_IMMORTAL( victim ) )
		  victim->pcdata->learned[sn] = GET_ADEPT( victim, sn );
		else
		  victim->pcdata->learned[sn] = value;
	    }
	}
    }
    else
	victim->pcdata->learned[sn] = value;

    return;
}


void learn_from_success( CHAR_DATA *ch, int sn )
{
    int adept;
    int mana = IS_NPC(ch) ? 0 : skill_table[sn]->min_mana;

    if ( IS_NPC(ch) || ch->pcdata->learned[sn] <= 0 || !skill_table[sn])
	return;

    if ( ch->race == RACE_WIZARD && number_range( 1, 20 ) == 20 )
    {
	if ( ch->basepl > skill_table[gsn_arcanetheft]->race_level[RACE_WIZARD] && ch->pcdata->learned[gsn_arcanetheft] < 10 )
	{
	    ch->pcdata->learned[gsn_arcanetheft] = 10;
	    send_to_char( "&RYou suddenly have a new idea for a spell... Arcane Thievery is a good name.\n\r", ch );
	}
    }

    if ( !IS_NPC(ch) && mana > 0)
    {
        if ( ch->energy > 500 )
            mana = mana / 100 * ( ch->energy * 10 );
        else
            mana = mana / 100 * ( ch->energy );
    }
    if ( mana )
    {
        ch->mana -= mana;
            if ( ch->mana < 0 )
                ch->mana = 0;
    }

if ( !IS_NPC(ch) && IS_SET(ch->pcdata->flags, PCFLAG_XBUSTER) )
{
 ch->plmod = 100;
 ch->multi_str -= 1000;
 ch->multi_int -= 1000;
 ch->multi_wis -= 1000;
 ch->multi_dex -= 1000;
 ch->multi_con -= 1000;
 ch->multi_cha -= 1000;
 ch->multi_lck -= 1000;
 REMOVE_BIT( ch->pcdata->flags, PCFLAG_XBUSTER );
 update_current( ch, NULL );
 send_to_char( "You regain control and flush out the X-Buster virus.\n\r", ch );
 act( AT_GREEN, "$n flinches and then looks like they have gotten some control back.", ch, NULL, NULL, TO_CANSEE );
}
    adept = GET_ADEPT(ch,sn);
    if ( ch->pcdata->learned[sn] < 
	skill_table[sn]->race_adept[ch->race] 
	&& number_range(1, 500 ) <
	get_curr_int(ch))
    {
	    ch->pcdata->learned[sn]++;
    }
    return;
}


void learn_from_failure( CHAR_DATA *ch, int sn )
{
    int adept/*, chance*/;

    if ( sn < 0 )
	return;

    if ( IS_NPC(ch) || ch->pcdata->learned[sn] <= 0 )
	return;
    if ( skill_table[sn]->race_adept[ch->race] <= ch->pcdata->learned[sn] )
	return;
    adept = GET_ADEPT(ch,sn);
    if ( ch->pcdata->learned[sn] < skill_table[sn]->race_adept[ch->race] && number_range(1, 500 ) < (get_curr_int(ch) / 1.5))
    {
	    ch->pcdata->learned[sn]++;
    }
    return;
}


void do_gouge( CHAR_DATA *ch, char *argument )
{
}

void do_detrap( CHAR_DATA *ch, char *argument )
{
}

    
void do_search( CHAR_DATA *ch, char *argument )
{
}


void do_steal( CHAR_DATA *ch, char *argument )
{
}


void do_backstab( CHAR_DATA *ch, char *argument )
{
}


void do_rescue( CHAR_DATA *ch, char *argument )
{
}

void do_kick( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
	send_to_char( "You can't concentrate enough for that.\n\r", ch );
	return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

    WAIT_STATE( ch, skill_table[gsn_kick]->beats );
	learn_from_failure( ch, gsn_kick );
	global_retcode = damage( ch, victim, 0, gsn_kick );
    return;
}

void do_punch( CHAR_DATA *ch, char *argument )
{
}


void do_bite( CHAR_DATA *ch, char *argument )
{
}


void do_claw( CHAR_DATA *ch, char *argument )
{
}

void do_sting( CHAR_DATA *ch, char *argument )
{
}


void do_tail( CHAR_DATA *ch, char *argument )
{
}


void do_bash( CHAR_DATA *ch, char *argument )
{
}


void do_stun( CHAR_DATA *ch, char *argument )
{
}

void do_bloodlet( CHAR_DATA *ch, char *argument )
{
}

void do_feed( CHAR_DATA *ch, char *argument )
{
}


/*
 * Disarm a creature.
 * Caller must check for successful attack.
 * Check for loyalty flag (weapon disarms to inventory) for pkillers -Blodkai
 */
void disarm( CHAR_DATA *ch, CHAR_DATA *victim )
{
}


void do_disarm( CHAR_DATA *ch, char *argument )
{
}

void do_trip ( CHAR_DATA *ch, char *argument )
{
}

/* Converted to function well as a skill for vampires -- Blodkai */
void do_mistwalk( CHAR_DATA *ch, char *argument )
{
}

void do_broach( CHAR_DATA *ch, char *argument )
{
}

void do_pick( CHAR_DATA *ch, char *argument )
{
}



void do_sneak( CHAR_DATA *ch, char *argument )
{
}



void do_hide( CHAR_DATA *ch, char *argument )
{
}

/*
 * Contributed by Alander.
 */
void do_visible( CHAR_DATA *ch, char *argument )
{
    if ( IS_AFFECTED( ch, AFF_HIDE ) || IS_AFFECTED(ch, AFF_INVISIBLE) || IS_AFFECTED(ch, AFF_SNEAK ) )
    {
	    xREMOVE_BIT  ( ch->affected_by, AFF_HIDE		);
	    xREMOVE_BIT  ( ch->affected_by, AFF_INVISIBLE	);
	    xREMOVE_BIT  ( ch->affected_by, AFF_SNEAK		);
	    xSET_BIT     ( ch->affected_by, AFF_VISIBLE		);
	    send_to_char( "You stop sneaking around, emerging from the shadows!\n\r", ch );
	    act( AT_ACTION, "$n steps forward from the shadows!", ch, NULL, NULL, TO_ROOM );	    
    }
    else
    {
	xREMOVE_BIT  ( ch->affected_by, AFF_VISIBLE		);
	de_equip_char( ch );
	re_equip_char( ch );
	if ( IS_AFFECTED( ch, AFF_HIDE ) || IS_AFFECTED( ch, AFF_SNEAK ) || IS_AFFECTED( ch, AFF_INVISIBLE ) )
	{
	    send_to_char( "You fade quickly into the shadows...\n\r", ch );
	    act( AT_ACTION, "$n fades quickly into the shadows...", ch, NULL, NULL, TO_ROOM );	    
	}
	else
	    send_to_char( "You have no stealth abilities to activate.\n\r", ch );
    }
    return;
}


void do_recall( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *location;
    CHAR_DATA *opponent;

    location = NULL;

    if ( !IS_NPC(ch) && ch->pcdata->clan )
      location = get_room_index( ch->pcdata->clan->recall );

    if ( !IS_NPC( ch ) && !location && ch->level >= 5
    &&   IS_SET( ch->pcdata->flags, PCFLAG_DEADLY ) )
       location = get_room_index( 3009 );

    /* 1998-01-02, h */
    if ( !location )
       location = get_room_index(race_table[ch->race]->race_recall);

    if ( !location )
       location = get_room_index( 2 ); 

    if ( !location )
    {
	send_to_char( "You are completely lost.\n\r", ch );
	return;
    }

    if ( ch->in_room == location )
	return;


    if ( ( opponent = who_fighting( ch ) ) != NULL )
    {
	int lose;

	if ( number_bits( 1 ) == 0 || ( !IS_NPC( opponent ) && number_bits( 3 ) > 1 ) )
	{
	    WAIT_STATE( ch, 4 );
	    lose = (exp_level(ch, ch->level+1) - exp_level(ch, ch->level)) * 0.1;
	    if ( ch->desc )
	      lose /= 2;
	    gain_exp( ch, 0 - lose );
	    ch_printf( ch, "You failed!  You lose %d exps.\n\r", lose );
	    return;
	}

	lose = (exp_level(ch, ch->level+1) - exp_level(ch, ch->level)) * 0.2;
	if ( ch->desc )
	  lose /= 2;
	gain_exp( ch, 0 - lose );
	ch_printf( ch, "You recall from combat!  You lose %d exps.\n\r", lose );
	stop_fighting( ch, TRUE );
    }

    act( AT_ACTION, "$n disappears in a swirl of smoke.", ch, NULL, NULL, TO_ROOM );
    char_from_room( ch );
    char_to_room( ch, location );
/*    if ( ch->mount )
    {
	char_from_room( ch->mount );
	char_to_room( ch->mount, location );
    }*/
    act( AT_ACTION, "$n appears in the room.", ch, NULL, NULL, TO_ROOM );
    do_look( ch, "auto" );

    return;
}


void do_aid( CHAR_DATA *ch, char *argument )
{
}


void do_mount( CHAR_DATA *ch, char *argument )
{
}


void do_dismount( CHAR_DATA *ch, char *argument )
{
}


/**************************************************************************/


/*
 * Check for parry.
 */
bool check_parry( CHAR_DATA *ch, CHAR_DATA *victim )
{
    return TRUE;
}



/*
 * Check for dodge.
 */
bool check_dodge( CHAR_DATA *ch, CHAR_DATA *victim )
{
    return TRUE;
}

bool check_tumble( CHAR_DATA *ch, CHAR_DATA *victim )
{
    return TRUE;
}

void do_poison_weapon( CHAR_DATA *ch, char *argument )
{
}

void do_scribe( CHAR_DATA *ch, char *argument )
{
}

void do_brew( CHAR_DATA *ch, char *argument )
{
}

bool check_grip( CHAR_DATA *ch, CHAR_DATA *victim )
{
return TRUE;
}
void do_circle( CHAR_DATA *ch, char *argument )
{
}

/* Berserk and HitAll. -- Altrag */
void do_berserk( CHAR_DATA *ch, char *argument )
{
}

/* External from fight.c */
ch_ret one_hit	args( ( CHAR_DATA *ch, CHAR_DATA *victim, int dt ) );
void do_hitall( CHAR_DATA *ch, char *argument )
{
}


bool check_illegal_psteal( CHAR_DATA *ch, CHAR_DATA *victim )
{
    if (!IS_NPC (victim) && !IS_NPC(ch))
    {
	if ( ( !IS_SET( victim->pcdata->flags, PCFLAG_DEADLY )
	|| ch->level - victim->level > 10
	|| !IS_SET( ch->pcdata->flags, PCFLAG_DEADLY ) )
	&& ( ch->in_room->vnum < 29 || ch->in_room->vnum > 43 )
	&& ch != victim )
	{
	    /* 
	    sprintf( log_buf, "%s illegally stealing from %s at %d",
		(IS_NPC(ch) ? ch->name_descr : ch->name),
		PERS( victim, ch ),
		victim->in_room->vnum );
	    log_string( log_buf );
	    to_channel( log_buf, CHANNEL_MONITOR, "Monitor", LEVEL_IMMORTAL );
	    */
	    return TRUE;
	}
    }
    return FALSE;
}

void do_scan( CHAR_DATA *ch, char *argument )
{
    ROOM_INDEX_DATA *curr_room;
    EXIT_DATA *pexit;
    EXIT_DATA *nexit;
    sh_int dir = -1;
    sh_int dist;
    sh_int max_dist = 8;
    char buf[MAX_STRING_LENGTH];
    sh_int bfound = 0;
    CHAR_DATA *victim = NULL;
    CHAR_DATA *this_char;
    CHAR_DATA *char_next;
    OBJ_DATA *obj_scouter=NULL;
    OBJ_DATA *obj;

    set_char_color( AT_ACTION, ch );

    /* testing for an equipped scouter --Gohan */
//    OBJ_DATA *obj_next;
    
  if ( ch->race != RACE_ANDROID )
  {
    for ( obj = ch->first_carrying; obj; obj = obj->next_content )
    {
        if( obj->wear_loc == WEAR_EYES && obj->item_type == ITEM_SCOUTER )
        {
            obj_scouter = obj;
            bfound = 1;
            break;
        }
    }

   
    if ( bfound  != 1 )
    {
        send_to_char( "You must equip a scouter to scan.\n\r", ch );
        return;
    }
  }

    if ( IS_AFFECTED( ch, AFF_BLIND ) )
    {
	send_to_char( "You are blind and can't see the viewscreen...\n\r", ch );
	return;
    }
 
    if ( argument[0] == '\0' )
    {
	send_to_char( "Scan who or what direction?\n\r", ch );
	return;
    }
    
    bfound = 0;
    if ( ( dir = get_door( argument ) ) == -1 )
    { /* not a door */
	/* get the char to be scanned */
       
        this_char = ch->in_room->first_person;
        while ( this_char )
        {
            char_next = this_char->next_in_room;
            if ( nifty_is_name_prefix( argument, this_char->name ) )
            {
                victim = this_char;
                bfound = 1;
                break;
            }
            this_char = char_next;
        }
        
        if ( !bfound )
        {
            send_to_char( "That person is not here.\n\r", ch );
            return;
        }

        if ( victim == ch )
        {
            send_to_char( "You cannot scan yourself.\n\r", ch );
            return;
        }

        act( AT_ACTION, "You scan $t.", ch, PERS( victim, ch ), NULL, TO_CHAR );
        act( AT_ACTION, "$n scans $t.", ch, PERS( victim, ch ), NULL, TO_ROOM );
	

	if ( ch->race == RACE_ANDROID )
	{
            if ( 4 * ch->currpl  >= victim->currpl )
            {
                sprintf( buf, "%s's powerlevel is %s.\n\r", PERS( victim, ch ), format_pl(victim->currpl) );
                send_to_char( buf, ch );
                return;
            }
	    else 
	    {
	        send_to_char( "You can't detect anything...\n\r", ch );
	        return;
 	    }
        }
        else if ( 10000 * obj_scouter->value[0] >= victim->currpl )
        {
            sprintf( buf, "%s's powerlevel is %s.\n\r", PERS( victim, ch ), format_pl(victim->currpl) );
            send_to_char( buf, ch );
            return;
        }
        else
        {
            act( AT_RED, "Your scouter explodes from $t's power!", ch, PERS( victim, ch ), NULL, TO_CHAR );
            act( AT_ACTION, "$n's scouter explodes from $t's power!", ch, PERS( victim, ch ), NULL, TO_ROOM );
            separate_obj( obj_scouter );
            extract_obj( obj_scouter );
            return;
        }

    }
    else
    {
        if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
        {
            act( AT_GREY, "You can't see $t.", ch, dir_name[dir], NULL, TO_CHAR );
            return;
        }

	if ( !obj_scouter && ch->race != RACE_ANDROID)
	{
		send_to_char( "You need a scouter first.\n\r", ch );
		return;
	}
        if ( obj_scouter )
        max_dist = obj_scouter->value[1];
	else
	max_dist = 2;
        curr_room = ch->in_room;

        act( AT_ACTION, "You scan $t.", ch, dir_name[dir], NULL, TO_CHAR );
        act( AT_ACTION, "$n scans $t.", ch, dir_name[dir], NULL, TO_ROOM );
        
        for ( dist = 1; dist <= max_dist; dist++ )
        {
            if ( ( nexit = get_exit( curr_room, dir ) ) == NULL )
            {
                act( AT_GREY, "You can't scan any farther $t.", ch, dir_name[dir], NULL, TO_CHAR );
                return;
            }
            set_char_color( AT_RMNAME, ch );
            send_to_char( nexit->to_room->name, ch );
            send_to_char( "\n\r", ch );
            this_char = nexit->to_room->first_person;
            set_char_color( AT_ACTION, ch );
            while( this_char )
            {
                char_next = this_char->next_in_room;
	if ( obj_scouter )
	{
                if( ch->basepl * obj_scouter->value[0] >= this_char->currpl )
                {
                     sprintf( buf, "You read a powerlevel of %s.\n\r", format_pl(this_char->currpl) );
                     send_to_char( buf, ch );
                }
                else
                {
                     send_to_char( "Your scouter explodes from a high powerlevel.\n\r", ch );
                     extract_obj( obj_scouter );
                     return;
                }
                this_char = char_next;
	}
	else
	{
                if( ch->basepl * 4 >= this_char->currpl )
                {
                     sprintf( buf, "You read a powerlevel of %s.\n\r", format_pl(this_char->currpl) );
                     send_to_char( buf, ch );
                }
                else
                {
	        send_to_char( "You can't detect anything...\n\r", ch );
	        return;
                }
                this_char = char_next;
	}
            }
            curr_room = nexit->to_room;
        }

        send_to_char( "Scan Complete\n\r", ch );

        //send_to_char( "Directional scan code incomplete --Gohan\n\r", ch );
        return;
    }

/*
    was_in_room = ch->in_room;
    act( AT_GREY, "Scanning $t...", ch, dir_name[dir], NULL, TO_CHAR );
    act( AT_GREY, "$n scans $t.", ch, dir_name[dir], NULL, TO_ROOM );

    if ( !can_use_skill(ch, number_percent(), gsn_scan ) )
    {
	act( AT_GREY, "You stop scanning $t as your vision blurs.", ch,
	    dir_name[dir], NULL, TO_CHAR );
	learn_from_failure( ch, gsn_scan );
	return;
    }

    if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
    {
	act( AT_GREY, "You can't see $t.", ch, dir_name[dir], NULL, TO_CHAR );
	return;
    }

    if ( ch->level < 50 ) --max_dist;
    if ( ch->level < 40 ) --max_dist;
    if ( ch->level < 30 ) --max_dist;

    for ( dist = 1; dist <= max_dist; )
    {
	if ( IS_SET(pexit->exit_info, EX_CLOSED) )
	{
	    if ( IS_SET(pexit->exit_info, EX_SECRET)
	    ||   IS_SET(pexit->exit_info, EX_DIG) )
		act( AT_GREY, "Your view $t is blocked by a wall.", ch,
		    dir_name[dir], NULL, TO_CHAR );
	    else
		act( AT_GREY, "Your view $t is blocked by a door.", ch, 
		    dir_name[dir], NULL, TO_CHAR );
	    break;
	}
	if ( room_is_private( pexit->to_room )
	&&   ch->level < LEVEL_GREATER )
	{
	    act( AT_GREY, "Your view $t is blocked by a private room.", ch, 
		dir_name[dir], NULL, TO_CHAR );
	    break;
	}
	char_from_room( ch );
	char_to_room( ch, pexit->to_room );    
	set_char_color( AT_RMNAME, ch );
	send_to_char( ch->in_room->name, ch );
	send_to_char( "\n\r", ch );
	show_list_to_char( ch->in_room->first_content, ch, FALSE, FALSE );
	show_char_to_char( ch->in_room->first_person, ch );

	switch( ch->in_room->sector_type )
	{
	    default: dist++; break;
	    case SECT_AIR:
		if ( number_percent() < 80 ) dist++; break;
	    case SECT_INSIDE:
	    case SECT_FIELD:
	    case SECT_UNDERGROUND:
		dist++; break;
	    case SECT_FOREST:
	    case SECT_CITY:
	    case SECT_DESERT:
	    case SECT_HILLS:
		dist += 2; break;
	    case SECT_WATER_SWIM:
	    case SECT_WATER_NOSWIM:
		dist += 3; break;
	    case SECT_MOUNTAIN:
	    case SECT_UNDERWATER:
	    case SECT_OCEANFLOOR:
		dist += 4; break;
	}

	if ( dist >= max_dist )
	{
	    act( AT_GREY, "Your vision blurs with distance and you see no "
		"farther $t.", ch, dir_name[dir], NULL, TO_CHAR );
	    break;
	}
	if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
	{
	    act( AT_GREY, "Your view $t is blocked by a wall.", ch, 
		dir_name[dir], NULL, TO_CHAR );
	    break;
	}
    }
  
    char_from_room( ch );
    char_to_room( ch, was_in_room );
    learn_from_success( ch, gsn_scan );
*/
    return;
}


/*
 * Basically the same guts as do_scan() from above (please keep them in
 * sync) used to find the victim we're firing at.	-Thoric
 */
CHAR_DATA *scan_for_victim( CHAR_DATA *ch, EXIT_DATA *pexit, char *name )
{
    CHAR_DATA *victim;
    ROOM_INDEX_DATA *was_in_room;
    sh_int dist, dir;
    sh_int max_dist = 8;

    if ( IS_AFFECTED(ch, AFF_BLIND) || !pexit )
	return NULL;
 
    was_in_room = ch->in_room;

    if ( ch->level < 50 ) --max_dist;
    if ( ch->level < 40 ) --max_dist;
    if ( ch->level < 30 ) --max_dist;

    for ( dist = 1; dist <= max_dist; )
    {
	if ( IS_SET(pexit->exit_info, EX_CLOSED) )
	    break;

	if ( room_is_private( pexit->to_room )
	&&   ch->level < LEVEL_GREATER )
	    break;

	char_from_room( ch );
	char_to_room( ch, pexit->to_room );    

	if ( (victim=get_char_room(ch, name)) != NULL )
	{
	    char_from_room(ch);
	    char_to_room(ch, was_in_room);
	    return victim;
	}

	switch( ch->in_room->sector_type )
	{
	    default: dist++; break;
	    case SECT_AIR:
		if ( number_percent() < 80 ) dist++; break;
	    case SECT_INSIDE:
	    case SECT_FIELD:
	    case SECT_UNDERGROUND:
		dist++; break;
	    case SECT_FOREST:
	    case SECT_CITY:
	    case SECT_DESERT:
	    case SECT_HILLS:
		dist += 2; break;
	    case SECT_WATER_SWIM:
	    case SECT_WATER_NOSWIM:
		dist += 3; break;
	    case SECT_MOUNTAIN:
	    case SECT_UNDERWATER:
	    case SECT_OCEANFLOOR:
		dist += 4; break;
	}

	if ( dist >= max_dist )
	    break;

	dir = pexit->vdir;
	if ( (pexit=get_exit(ch->in_room, dir)) == NULL )
	    break;
    }
  
    char_from_room(ch);
    char_to_room(ch, was_in_room);

    return NULL;
}

/*
 * Search inventory for an appropriate projectile to fire.
 * Also search open quivers.					-Thoric
 */
OBJ_DATA *find_projectile( CHAR_DATA *ch, int type )
{
    OBJ_DATA *obj, *obj2;

    for ( obj = ch->last_carrying; obj; obj = obj->prev_content )
    {
	if ( can_see_obj(ch, obj) )
	{
	    if ( obj->item_type == ITEM_QUIVER && !IS_SET(obj->value[1], CONT_CLOSED) )
	    {
		for ( obj2 = obj->last_content; obj2; obj2 = obj2->prev_content )
		{
		    if ( obj2->item_type == ITEM_PROJECTILE
		    &&   obj2->value[3] == type )
			return obj2;
		}
	    }
	    if ( obj->item_type == ITEM_PROJECTILE && obj->value[3] == type )
		return obj;
	}
    }

    return NULL;
}


ch_ret spell_attack( int, int, CHAR_DATA *, void * );

/*
 * Perform the actual attack on a victim			-Thoric
 */
ch_ret ranged_got_target( CHAR_DATA *ch, CHAR_DATA *victim, OBJ_DATA *weapon,
	OBJ_DATA *projectile, sh_int dist, sh_int dt, char *stxt, sh_int color )
{

    if ( IS_NPC(victim) && xIS_SET(victim->act, ACT_SENTINEL)
    &&   ch->in_room != victim->in_room )
    {
	/*
	 * letsee, if they're high enough.. attack back with fireballs
	 * long distance or maybe some minions... go herne! heh..
	 *
	 * For now, just always miss if not in same room  -Thoric
	 */

	if ( projectile )
	{
	    /* 50% chance of projectile getting lost */
	    if ( number_percent() < 50 )
		extract_obj(projectile);
	    else
	    {
	 	if ( projectile->in_obj )
		    obj_from_obj(projectile);
	 	if ( projectile->carried_by )
		    obj_from_char(projectile);
	 	obj_to_room(projectile, victim->in_room);
	    }
	}
	return damage( ch, victim, 0, dt );
    }

    if ( number_percent() > 50 )
    {
	if ( projectile )
	    global_retcode = projectile_hit(ch, victim, weapon, projectile, dist );
	else
	    global_retcode = spell_attack( dt, ch->level, ch, victim );
    }
    else
    {
	global_retcode = damage( ch, victim, 0, dt );

	if ( projectile )
	{
	    /* 50% chance of getting lost */
	    if ( number_percent() < 50 )
		extract_obj(projectile);
	    else
	    {
		if ( projectile->in_obj )
		    obj_from_obj(projectile);
		if ( projectile->carried_by )
		    obj_from_char(projectile);
		obj_to_room(projectile, victim->in_room);
	    }
	}
    }
    return global_retcode;
}

/*
 * Generic use ranged attack function			-Thoric & Tricops
 */
ch_ret ranged_attack( CHAR_DATA *ch, char *argument, OBJ_DATA *weapon,
		      OBJ_DATA *projectile, sh_int dt, sh_int range )
{
    CHAR_DATA *victim, *vch = NULL;
    EXIT_DATA *pexit;
    ROOM_INDEX_DATA *was_in_room;
    char arg[MAX_INPUT_LENGTH];
    char arg1[MAX_INPUT_LENGTH];
    char temp[MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    SKILLTYPE *skill = NULL;
    sh_int dir = -1, dist = 0, color = AT_GREY;
    char *dtxt = "somewhere";
    char *stxt = "burst of energy";
    int count;


    if ( argument && argument[0] != '\0' && argument[0] == '\''){
      one_argument( argument, temp );
      argument = temp;
    }

    argument = one_argument(argument, arg);
    argument = one_argument(argument, arg1);

    if ( arg[0] == '\0' )
    {
	send_to_char( "Where?  At who?\n\r", ch );
	return rNONE;
    }

    victim = NULL;

    /* get an exit or a victim */
    if ( (pexit = find_door(ch, arg, TRUE)) == NULL )
    {
	if ( (victim=get_char_room(ch, arg)) == NULL )
	{
	    send_to_char( "Aim in what direction?\n\r", ch );
	    return rNONE;
	}
	else
	{
	    if ( who_fighting(ch) == victim )
	    {
		send_to_char( "They are too close to release that type of attack!\n\r", ch );
		return rNONE;
	    }

	if ( xIS_SET(ch->act, PLR_KILLER) || xIS_SET(vch->act, PLR_KILLER) )
	{
	    send_to_char("Pkill like a real pkiller.\n\r", ch );
	    return rNONE;
	}
	    /* Taken out because of waitstate 
	    if ( !IS_NPC(ch) && !IS_NPC(victim) )
	    {
	    	send_to_char("Pkill like a real pkiller.\n\r", ch );
		return rNONE;
	    }
	    */
	}
    }
    else
	dir = pexit->vdir;

    /* check for ranged attacks from private rooms, etc */
    if ( !victim )
    {
	if ( IS_SET(ch->in_room->room_flags, ROOM_PRIVATE)
	||   IS_SET(ch->in_room->room_flags, ROOM_SOLITARY) )
	{
	    send_to_char( "You cannot perform a ranged attack from a private room.\n\r", ch );
	    return rNONE;
	}
	if ( ch->in_room->tunnel > 0 )
	{
	    count = 0;
	    for ( vch = ch->in_room->first_person; vch; vch = vch->next_in_room )
		++count;
	    if ( count >= ch->in_room->tunnel )
	    {
		send_to_char( "This room is too cramped to perform such an attack.\n\r", ch );
		return rNONE;
	    }
	}
    }

    if ( IS_VALID_SN(dt) )
	skill = skill_table[dt];

    if ( pexit && !pexit->to_room )
    {
	send_to_char( "Are you expecting to fire through a wall!?\n\r", ch );
	return rNONE;
    }

    /* Check for obstruction */
    if ( pexit && IS_SET(pexit->exit_info, EX_CLOSED) )
    {
	if ( IS_SET(pexit->exit_info, EX_SECRET)
	||   IS_SET(pexit->exit_info, EX_DIG) )
	    send_to_char( "Are you expecting to fire through a wall!?\n\r", ch );
	else
	    send_to_char( "Are you expecting to fire through a door!?\n\r", ch );
	return rNONE;
    }

    vch = NULL;
    if ( pexit && arg1[0] != '\0' )
    {
	if ( (vch=scan_for_victim(ch, pexit, arg1)) == NULL )
	{
	    send_to_char( "You cannot see your target.\n\r", ch );
	    return rNONE;
	}

	/* don't allow attacks on mobs stuck in another area?
	if ( IS_NPC(vch) && xIS_SET(vch->act, ACT_STAY_AREA)
	&&   ch->in_room->area != vch->in_room->area) )
	{
	}
	*/

	if ( xIS_SET(ch->act, PLR_KILLER) || xIS_SET(vch->act, PLR_KILLER) )
	{
	    send_to_char("Pkill like a real pkiller.\n\r", ch );
	    return rNONE;
	}


	/* can't properly target someone heavily in battle */
	if ( vch->num_fighting > max_fight(vch) )
	{
	    send_to_char( "There is too much activity there for you to get a clear shot.\n\r", ch );
	    return rNONE;
	}
    }
    if ( vch ) {
    if ( !IS_NPC( vch ) && !IS_NPC( ch ) &&
	 xIS_SET(ch->act, PLR_NICE ) )
    {
	send_to_char( "Your too nice to do that!\n\r", ch );
	return rNONE;
    }
    if ( vch && is_safe(ch, vch, TRUE) )
	    return rNONE;
    }
    was_in_room = ch->in_room;

    if ( projectile )
    {
	separate_obj(projectile);
	if ( pexit )
	{
	    if ( weapon )
	    {
		act( AT_GREY, "You fire $p $T.", ch, projectile, dir_name[dir], TO_CHAR );
		act( AT_GREY, "$n fires $p $T.", ch, projectile, dir_name[dir], TO_ROOM );
	    }
	    else
	    {
		act( AT_GREY, "You throw $p $T.", ch, projectile, dir_name[dir], TO_CHAR );
		act( AT_GREY, "$n throw $p $T.", ch, projectile, dir_name[dir], TO_ROOM );
	    }
	}
	else
	{
	    if ( weapon )
	    {
		act( AT_GREY, "You fire $p at $N.", ch, projectile, victim, TO_CHAR );
		act( AT_GREY, "$n fires $p at $N.", ch, projectile, victim, TO_NOTVICT );
		act( AT_GREY, "$n fires $p at you!", ch, projectile, victim, TO_VICT );
	    }
	    else
	    {
		act( AT_GREY, "You throw $p at $N.", ch, projectile, victim, TO_CHAR );
		act( AT_GREY, "$n throws $p at $N.", ch, projectile, victim, TO_NOTVICT );
		act( AT_GREY, "$n throws $p at you!", ch, projectile, victim, TO_VICT );
	    }
	}
    }
    else
    if ( skill )
    {
	if ( skill->noun_damage && skill->noun_damage[0] != '\0' )
	    stxt = skill->noun_damage;
	else
	    stxt = skill->name;
	/* a plain "spell" flying around seems boring */
	if ( !str_cmp(stxt, "spell") )
	    stxt = "magical burst of energy";
	if ( skill->type == SKILL_SPELL )
	{
	    color = AT_MAGIC;
	    if ( pexit )
	    {
		act( AT_MAGIC, "You release $t $T.", ch, aoran(stxt), dir_name[dir], TO_CHAR );
		act( AT_MAGIC, "$n releases $s $t $T.", ch, stxt, dir_name[dir], TO_ROOM );
	    }
	    else
	    {
		act( AT_MAGIC, "You release $t at $N.", ch, aoran(stxt), victim, TO_CHAR );
		act( AT_MAGIC, "$n releases $s $t at $N.", ch, stxt, victim, TO_NOTVICT );
		act( AT_MAGIC, "$n releases $s $t at you!", ch, stxt, victim, TO_VICT );
	    }
	}
    }
    else
    {
	bug( "Ranged_attack: no projectile, no skill dt %d", dt );
	return rNONE;
    }
  
    /* victim in same room */
    if ( victim )
    {
	return ranged_got_target( ch, victim, weapon, projectile,
		0, dt, stxt, color );
    }

    /* assign scanned victim */
    victim = vch;

    /* reverse direction text from move_char */
    dtxt = rev_exit(pexit->vdir);

    while ( dist <= range )
    {
	char_from_room(ch);
	char_to_room(ch, pexit->to_room);

	if ( IS_SET(pexit->exit_info, EX_CLOSED) )
	{
	    /* whadoyahknow, the door's closed */
	    if ( projectile )
		sprintf(buf,"You see your %s pierce a door in the distance to the %s.", 
		    myobj(projectile), dir_name[dir] );
	    else
		sprintf(buf, "You see your %s hit a door in the distance to the %s.",
		    stxt, dir_name[dir] );
	    act( color, buf, ch, NULL, NULL, TO_CHAR );
	    if ( projectile )
	    {
		sprintf(buf,"$p flies in from %s and implants itself solidly in the %sern door.",
		    dtxt, dir_name[dir] );
		act( color, buf, ch, projectile, NULL, TO_ROOM );
	    }
	    else
	    {
		sprintf(buf, "%s flies in from %s and implants itself solidly in the %sern door.",
		    aoran(stxt), dtxt, dir_name[dir] );
		buf[0] = UPPER(buf[0]);
		act( color, buf, ch, NULL, NULL, TO_ROOM );
	    }
	    break; 
	}


	/* no victim? pick a random one */
	if ( !victim )
	{
	    for ( vch = ch->in_room->first_person; vch; vch = vch->next_in_room )
	    {
		if ( ((IS_NPC(ch) && !IS_NPC(vch))
		||   (!IS_NPC(ch) &&  IS_NPC(vch)))
		&&    number_bits(1) == 0 )
		{
		    victim = vch;
		    break;
		}
	    }
	    if ( victim && is_safe(ch, victim, TRUE) )
	    {
	        char_from_room(ch);
	        char_to_room(ch, was_in_room);
		return rNONE;
	    }
	}

	/* In the same room as our victim? */
	if ( victim && ch->in_room == victim->in_room )
	{
	    if ( projectile )
		act( color, "$p flies in from $T.", ch, projectile, dtxt, TO_ROOM );
	    else
		act( color, "$t flies in from $T.", ch, aoran(stxt), dtxt, TO_ROOM );

	    /* get back before the action starts */
	    char_from_room(ch);
	    char_to_room(ch, was_in_room);

	    check_illegal_pk( ch, victim );
	    check_attacker( ch, victim );
	    return ranged_got_target( ch, victim, weapon, projectile,
					dist, dt, stxt, color );
 	}

	if ( dist == range ) 
	{
	    if ( projectile )
	    {
		act( color, "Your $t falls harmlessly to the ground to the $T.", 
		    ch, myobj(projectile), dir_name[dir], TO_CHAR );
		act( color, "$p flies in from $T and falls harmlessly to the ground here.",
		    ch, projectile, dtxt, TO_ROOM );
		if ( projectile->in_obj )
		    obj_from_obj(projectile);
		if ( projectile->carried_by )
		    obj_from_char(projectile);
		obj_to_room(projectile, ch->in_room);
	    }
	    else
	    {
		act( color, "Your $t fizzles out harmlessly to the $T.", ch, stxt, dir_name[dir], TO_CHAR );
		act( color, "$t flies in from $T and fizzles out harmlessly.",
		    ch, aoran(stxt), dtxt, TO_ROOM );
	    }
	    break;
	}

	if ( ( pexit = get_exit( ch->in_room, dir ) ) == NULL )
	{
	    if ( projectile )
	    {
		act( color, "Your $t hits a wall and bounces harmlessly to the ground to the $T.", 
		    ch, myobj(projectile), dir_name[dir], TO_CHAR );
		act( color, "$p strikes the $Tsern wall and falls harmlessly to the ground.",
		    ch, projectile, dir_name[dir], TO_ROOM );
		if ( projectile->in_obj )
		    obj_from_obj(projectile);
		if ( projectile->carried_by )
		    obj_from_char(projectile);
		obj_to_room(projectile, ch->in_room);
	    }
	    else
	    {
		act( color, "Your $t harmlessly hits a wall to the $T.", 
		    ch, stxt, dir_name[dir], TO_CHAR );
		act( color, "$t strikes the $Tsern wall and falls harmlessly to the ground.",
		    ch, aoran(stxt), dir_name[dir], TO_ROOM );
	    }
	    break;
	}
	if ( projectile )
	    act( color, "$p flies in from $T.", ch, projectile, dtxt, TO_ROOM );
	else
	    act( color, "$t flies in from $T.", ch, aoran(stxt), dtxt, TO_ROOM );
	dist++;
    }

    char_from_room( ch );
    char_to_room( ch, was_in_room );

    return rNONE;
}

/*
 * Fire <direction> <target>
 *
 * Fire a projectile from a missile weapon (bow, crossbow, etc)
 *
 * Design by Thoric, coding by Thoric and Tricops.
 *
 * Support code (see projectile_hit(), quiver support, other changes to
 * fight.c, etc by Thoric.
 */
void do_fire( CHAR_DATA *ch, char *argument )
{
    OBJ_DATA *arrow;
    OBJ_DATA *bow;
    sh_int max_dist;

    if ( argument[0] == '\0' || !str_cmp(argument, " ") )
    {
	send_to_char( "Fire where at who?\n\r", ch );
	return;
    }


    /*
     * Find the projectile weapon
     */
    if ( (bow=get_eq_char(ch, WEAR_MISSILE_WIELD)) != NULL )
	if ( !(bow->item_type == ITEM_MISSILE_WEAPON) )
	    bow = NULL;

    if ( !bow )
    {
	send_to_char( "You are not wielding a missile weapon.\n\r", ch );
	return;
    }

    /* modify maximum distance based on bow-type and ch's class/str/etc */
    max_dist = URANGE( 1, bow->value[4], 10 );

    if ( (arrow=find_projectile(ch, bow->value[3])) == NULL )
    {
	char *msg = "You have nothing to fire...\n\r";

	switch( bow->value[3] )
	{
	    case DAM_BOLT:	msg = "You have no bolts...\n\r";	break;
	    case DAM_ARROW:	msg = "You have no arrows...\n\r";	break;
	    case DAM_DART:	msg = "You have no darts...\n\r";	break;
	    case DAM_STONE:	msg = "You have no slingstones...\n\r";	break;
	    case DAM_PEA:	msg = "You have no peas...\n\r";	break;
	}
	send_to_char( msg, ch );
	return;
    }

    /* Add wait state to fire for pkill, etc... */
    WAIT_STATE( ch, 6 );

    /* handle the ranged attack */
    ranged_attack( ch, argument, bow, arrow, TYPE_HIT + arrow->value[3], max_dist );

    return;
}

/*
 * Attempt to fire at a victim.
 * Returns FALSE if no attempt was made
 */
bool mob_fire( CHAR_DATA *ch, char *name )
{
    OBJ_DATA *arrow;
    OBJ_DATA *bow;
    sh_int max_dist;


    if ( (bow=get_eq_char(ch, WEAR_MISSILE_WIELD)) != NULL )
	if ( !(bow->item_type == ITEM_MISSILE_WEAPON) )
	    bow = NULL;

    if ( !bow )
	return FALSE;

    /* modify maximum distance based on bow-type and ch's class/str/etc */
    max_dist = URANGE( 1, bow->value[4], 10 );

    if ( (arrow=find_projectile(ch, bow->value[3])) == NULL )
	return FALSE;

    ranged_attack( ch, name, bow, arrow, TYPE_HIT + arrow->value[3], max_dist );

    return TRUE;
}

/* -- working on -- 
 * Syntaxes: throw object  (assumed already fighting)
 *	     throw object direction target  (all needed args for distance 
 *	          throwing)
 *	     throw object  (assumed same room throw)

void do_throw( CHAR_DATA *ch, char *argument )
{
  ROOM_INDEX_DATA *was_in_room;
  CHAR_DATA *victim;
  OBJ_DATA *throw_obj;
  EXIT_DATA *pexit;
  sh_int dir;
  sh_int dist;
  sh_int max_dist = 3;
  char arg[MAX_INPUT_LENGTH];
  char arg1[MAX_INPUT_LENGTH];
  char arg2[MAX_INPUT_LENGTH];

  argument = one_argument( argument, arg );
  argument = one_argument( argument, arg1 );
  argument = one_argument( argument, arg2 );

  for ( throw_obj = ch->last_carrying; throw_obj;
	throw_obj = throw_obj=>prev_content )
  {
---    if ( can_see_obj( ch, throw_obj )
	&& ( throw_obj->wear_loc == WEAR_HELD || throw_obj->wear_loc == 
	WEAR_WIELDED || throw_obj->wear_loc == WEAR_DUAL_WIELDED )
	&& nifty_is_name( arg, throw_obj->name ) )
      break;
 ----
    if ( can_see_obj( ch, throw_obj ) && nifty_is_name( arg, throw_obj->name )
      break;
  }

  if ( !throw_obj )
  {
    send_to_char( "You aren't holding or wielding anything like that.\n\r", ch );
    return;
  }

----
  if ( ( throw_obj->item_type != ITEM_WEAPON)
  {
    send_to_char("You can only throw weapons.\n\r", ch );
    return;
  }
----

  if (get_obj_weight( throw_obj ) - ( 3 * (get_curr_str(ch) - 15) ) > 0)
  {
    send_to_char("That is too heavy for you to throw.\n\r", ch);
    if (!number_range(0,10))
      learn_from_failure( ch, gsn_throw );
    return;
  }

  if ( ch->fighting )
    victim = ch->fighting;
   else
    {
      if ( ( ( victim = get_char_room( ch, arg1 ) ) == NULL )
  	&& ( arg2[0] == '\0' ) )
      {
        act( AT_GREY, "Throw $t at whom?", ch, obj->short_descr, NULL,  
	  TO_CHAR );
        return;
      }
    }
}*/
  
void do_slice( CHAR_DATA *ch, char *argument )
{
}

/*------------------------------------------------------------ 
 *  Fighting Styles - haus
 *  Rewritten
 */
void do_style( CHAR_DATA *ch, char *argument )
{
  int ctr = 0;
  bool found = FALSE;

  if ( !argument || !str_cmp( argument, "" ) )
  {
      ch_printf( ch, "You are in the %s fighting style.\n\r", StyleNames[ch->style] );
      send_to_char( "Styles you know:\n\r &GFighting&r, &GOffensive&r, &GDefensive", ch );
      for ( ctr = STYLE_DEFENSIVE + 1; ctr <= STYLE_TIRA; ctr++ )
      {
	if ( ch->styleunlocked[ctr] == 1 )
	{
	    ch_printf( ch, "&r, &G%s", StyleNames[ctr] );
	}
      }
      if ( ch->stance_learn_timer == -1 )
      {
	ch_printf( ch, "\n\rYou need to select a new style before you can learn another one." );
	return;
      }
      ch_printf( ch, "\n\rThe next style you will learn at this rate is: " );
      if ( next_style(ch) == -1 || ch->styleunlocked[next_style(ch)] == 1 )
	ch_printf( ch, "Nothing!  Try another style.\n\r" );
      else
	ch_printf( ch, "%s, in %d combat rounds.\n\r", StyleNames[next_style(ch)], ch->stance_learn_timer );
      return;
  }

  if ( ch->style != STYLE_FIGHTING && !nifty_is_name( argument, StyleNames[STYLE_FIGHTING] )
	&& ch->style != STYLE_DEFENSIVE && !nifty_is_name( argument, StyleNames[STYLE_OFFENSIVE] )
	&& ch->style != STYLE_OFFENSIVE && !nifty_is_name( argument, StyleNames[STYLE_DEFENSIVE] ) )
  {
      send_to_char( "You must first change to Fighting, Offensive, or Defensive style.\n\r", ch );
      return;
  }

  for ( ctr = 0; ctr < STYLE_LAST; ctr++ )
  {
      if ( nifty_is_name( argument, StyleNames[ctr] ) )
      {
	  found = TRUE;
	  break;
      }
  }
  if ( !found || ch->styleunlocked[ctr] == FALSE )
  {
      send_to_char( "You do not know any styles of that name.\n\r", ch );
      return;
  }
  
  ch->skill_timer = 4;
  ch->fakelag = 4;
  ch->style = ctr;
  ch->stance_learn_timer = 500;
  ch_printf( ch, "You begin to change to the %s style.\n\r", StyleNames[ctr] );
  act( AT_YELLOW, "$n begins to change combat styles.\n\r", ch, NULL, NULL, TO_CANSEE );
  return;
}

/*  New check to see if you can use skills to support morphs --Shaddai */
bool can_use_skill( CHAR_DATA *ch, int percent, int gsn )
{
  
    if ( IS_NPC(ch) || IS_IMMORTAL(ch) )
	return( TRUE );

    if ( percent == 100 )
	return TRUE;

    if ( percent == 0 )
	return TRUE;

    if ( !IS_NPC(ch) && ch->pcdata->learned[gsn] == 0 )
	return FALSE;

    if ( ch->style == STYLE_MASTERMIND )
	percent -= 15;
    if ( ch->style == STYLE_MEATHEAD )
	percent += 15;
    if ( ch->style == STYLE_GHANDI )
	percent += 30;

    if ( ch->style == STYLE_CIRCULARWEAPONRY)
    {
	if ( gsn != gsn_concentration && gsn != gsn_criticalhit && gsn != gsn_epic_concentration && 
	     gsn != gsn_second_attack && gsn != gsn_third_attack && gsn != gsn_fourth_attack && 
	     gsn != gsn_fifth_attack )
	    pager_printf( ch, "You cannot use %s in this stance.\n\r", skill_table[gsn]->name );
	return ( FALSE );
    }

    if ( IS_AFFECTED(ch, AFF_CURSE) && skill_table[gsn]->size > 0 )
    {
	pager_printf( ch, "Damn that curse!\n\r" );
        return ( FALSE );
    }

    if ( IS_AFFECTED(ch, AFF_ARCANE_THEFT) && skill_table[gsn]->size == 1 )
    {
	pager_printf( ch, "As you try to remember your skill, an awful buzzing noise starts in your ears!\n\r" );
        return ( FALSE );
    }
   

    if ( !ch->pcdata->set[gsn] && skill_table[gsn]->size > 0 && ch->race != RACE_WIZARD )
    {
 	return FALSE;
    }
    if ( skill_table[gsn]->race_adept[ch->race] < 10 )
        return ( FALSE );

    if ( IS_AFFECTED(ch, AFF_ARCANE_THEFT) && skill_table[gsn]->size > 1 )
    {
	pager_printf( ch, "As you try to remember your skill, a crippling pain rushes through your brain!\n\r" );
        global_retcode = damage( who_fighting(ch), ch, (ch->hit/100)*(5*skill_table[gsn]->size), gsn_arcanetheft );
    }

    if ( percent < LEARNED(ch,gsn) )
	return( TRUE );
     return ( FALSE );
}

/*
 * Cook was coded by Blackmane and heavily modified by Shaddai
 *
 * Then redone from scratch by Yami.
 */
void do_cook( CHAR_DATA *ch, char *argument )
{
 char arg[MAX_STRING_LENGTH];
 int  timer = number_range( 15, 20 );
 int  GenericCookedFood = 340;
 int  BurnedFood = 341;
 int  CookedFish = 342;
 unsigned long long plbonus;
 OBJ_DATA *tocook = NULL;
 OBJ_INDEX_DATA *whatcook = NULL;
 OBJ_DATA *cooked = NULL;

if ( ( who_fighting( ch ) ) != NULL )
{
    send_to_char( "In the middle of a fight?!  What the hell?!\n\r", ch );
    return;
}

if ( ch->in_room->sector_type == SECT_UNDERWATER ||
     ch->in_room->sector_type == SECT_OUTERSPACE ||
     ch->in_room->sector_type == SECT_OCEANFLOOR )
{
    send_to_char( "&WYou cannot cook food here.\n\r", ch );
    return;
}

if ( !IS_AFFECTED(ch, AFF_COOKING ) )
{
if ( !argument || argument[0] == '\0' )
{
    send_to_char( "&WCook what?\n\r", ch );
    return;
}

tocook = get_obj_carry( ch, argument );

if ( !tocook )
{
    send_to_char( "You don't have that item to cook.\n\r", ch );
    return;
}

if ( tocook->item_type != ITEM_FOOD )
{
    send_to_char( "But that isn't food.\n\r", ch );
    return;
}

if ( tocook->pIndexData->vnum >= 340 && tocook->pIndexData->vnum <= 342 )
{
    send_to_char( "It's already been cooked...\n\r", ch );
    return;
}

ch->food = STRALLOC( tocook->name );
}
switch( ch->substate )
    {
        default:
               ch->skill_timer = timer;
               add_timer( ch, TIMER_DO_FUN, timer, do_cook, 1 );
               do_sit( ch, "" );
               send_to_char( "&RYou gather up some incendiary materials and set them on fire with your ki.\n\r"
                             "&RThreading your ingredients on a handy stick, you begin to cook them.\n\r", ch );
               act( AT_BLOOD,"$n lights a small fire, and begins to cook some food.", ch, NULL, NULL, TO_CANSEE );
               xSET_BIT( ch->affected_by, AFF_COOKING );
               ch->alloc_ptr = str_dup( arg );
               return;
        case 1:
               if ( !ch->alloc_ptr )
               {
                    send_to_char( "&GYour cooking was interrupted!\n\r", ch );
                    bug( "do_cook: alloc_ptr NULL", 0 );
                    return;
               }
               xREMOVE_BIT( ch->affected_by, AFF_COOKING );
               strcpy( arg, ch->alloc_ptr );
               DISPOSE( ch->alloc_ptr );
               break;
        case SUB_TIMER_DO_ABORT:
               DISPOSE( ch->alloc_ptr );
               ch->substate = SUB_NONE;
               send_to_char( "You stop cooking, and put out your fire.\n\r", ch );
               act( AT_GREEN, "$n stops cooking, and puts out $s fire.\n\r",  ch, NULL, NULL, TO_NOTVICT );
               xREMOVE_BIT( ch->affected_by, AFF_COOKING );
               ch->skill_timer = 0;
               return;
    }

    tocook = get_obj_carry( ch, ch->food );
    ch->substate = SUB_NONE;
    xREMOVE_BIT( ch->affected_by, AFF_COOKING );
    if ( can_use_skill(ch, number_percent(), gsn_cook ) && tocook->value[1] != 2 )
    {
          learn_from_success( ch, gsn_cook );
          if ( tocook->pIndexData->vnum >= 310 && tocook->pIndexData->vnum <= 330 )
             whatcook = get_obj_index( CookedFish );
	  else
             whatcook = get_obj_index( tocook->value[2] );
          if ( !whatcook || whatcook->item_type != ITEM_FOOD )
             whatcook = get_obj_index( GenericCookedFood );
          cooked = create_object( whatcook, 100 );
	  cooked->timer = 30;
          obj_to_char( cooked, ch );
	  if ( tocook->count <= 1 )
	  {
	    obj_from_char( tocook );
	    extract_obj( tocook );
	  }
	  else
	    tocook->count--;
          send_to_char( "You pull your item out of the fire, and see it is nicely cooked.\n\r", ch );
          act( AT_MAGIC, "$n pulls $s food out of the fire and looks pleased with $mself.", ch, NULL, NULL, TO_CANSEE );
	  plbonus = 5 * sqrt( sqrt( ch->basepl ) );
	  ch->basepl += plbonus;
	  update_current( ch, NULL );
	  ch_printf( ch, "You gain %s powerlevel.\n\r", format_pl( plbonus ) );
	  
    }
    else
    {
          learn_from_failure( ch, gsn_cook );
          whatcook = get_obj_index( BurnedFood );
          cooked = create_object( whatcook, 100 );
	  cooked->timer = 10;
          obj_to_char( cooked, ch );
	  if ( tocook->count <= 1 )
	  {
	    obj_from_char( tocook );
	    extract_obj( tocook );
	  }
	  else
	    tocook->count--;
          send_to_char( "The smell of smoke alerts you to your failed culinary attempts... better luck next time.\n\r", ch );
          act( AT_MAGIC, "$n completely burns $s food.", ch, NULL, NULL, TO_CANSEE );
          learn_from_failure( ch, gsn_cook );
    }
return;
}
/*
 * S T A R T   O F   S K I L L S   F O R   A L L   R A C E S
 */

 /*
  * Basic ki blast
  */

void do_kiblast( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 2;
        add_timer( ch, TIMER_DO_FUN, 2, do_kiblast, 1 );
        sprintf( buf, "&GYou take a small leap back and begin to form a %s &Gball of ki.\n\r", ch->energycolour );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s takes a small leap back from you and begins to form a %s &Gball of ki.\n\r", ch->name, ch->energycolour );
        pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n leaps away from $N and begins to charge a ki ball.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your ki blast was interupted!\n\r", ch );
          sprintf( buf, "&G%s's ki blast was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's ki blast was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your ki blast.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch-> sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch-> sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s ki blast!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_kiblast ) )
      {
   	  learn_from_success( ch, gsn_kiblast );
      sprintf( buf, "&GYour ki ball now complete, you fire it at %s &Gwith all your might!\n\r", PERS( victim, ch ) );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s screams and fires a small, %s &Gblast of ki at you!\n\r", ch->name, ch->energycolour );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "$n screams, and fires a small blast of ki at $N!", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 20, 40 )), gsn_kiblast );
      }
    else
      {
	  learn_from_failure( ch, gsn_kiblast );
      sprintf( buf, "&RAs soon as you gather some ki it just discharges from your hands.\n\r" );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&R%s doesn't form a stable ki blast, and so fails to attack you.\n\r", ch->name );
      pager_printf( victim, "%s", buf );
   	  act( AT_SKILL, "$n tries to fire a ki ball at $N, but fails.\n\r", ch, NULL, victim, TO_NOTVICT );
	  global_retcode = damage( ch, victim, 0, gsn_kiblast );
      }
return;
}

/*
 * Basic ki beam
 */

void do_kibeam( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 2;
        add_timer( ch, TIMER_DO_FUN, 2, do_kibeam, 1 );
        sprintf( buf, "&GYou look straight at %s and concentrate your energy.\n\r", PERS( victim, ch ) );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s looks right into your eyes, begins to glow %s &Gfaintly.\n\r", ch->name, ch->energycolour );
        pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n looks at $N, concentrating hard and glowing slightly.\n\r", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour ki beam was interupted!\n\r", ch );
          sprintf( buf, "&G%s's ki beam was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's ki beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your ki beam.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch-> sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch-> sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s ki beam!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_kibeam ) )
    {
   	  learn_from_success( ch, gsn_kibeam );
      sprintf( buf, "&GYou stick out your arm and fire a %s &Gbeam of ki at %s!\n\r", ch->energycolour, PERS( victim, ch ) );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s sticks his arm out and fires a %s &Gbeam of ki at you!\n\r", ch->name, ch->energycolour );
      pager_printf( victim, "%s", buf );
      act( AT_GREEN, "$n grunts and fires a thin beam of ki at $N.\n\r", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 25, 40 ) ), gsn_kibeam );
    }
    else
    {
      learn_from_failure( ch, gsn_kibeam );
      sprintf( buf, "&GYou cannot stay focused long enough to make your attack a reality.\n\r" );
      pager_printf( ch, "%s", buf );
      if ( ch-> sex == 0 )
         sprintf( buf, "&G%s suddenly has a vacant look on its face, as if it forgot how to attack.\n\r", ch->name );
      if ( ch-> sex == 1 )
         sprintf( buf, "&G%s suddenly has a vacant look on his face, as if he forgot how to attack.\n\r", ch->name );
      if ( ch-> sex == 2 )
         sprintf( buf, "&G%s suddenly has a vacant look on her face, as if she forgot how to attack.\n\r", ch->name );
      pager_printf( victim, "%s", buf );
      act( AT_GREEN, "$n tries to fire a ki beam at $N, but fails.\n\r", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, 0, gsn_kibeam );
    }
return;
}

/*
 * Combo Attack
 */

void do_combo( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 3;
        add_timer( ch, TIMER_DO_FUN, 3, do_combo, 1 );
        sprintf( buf, "&GYou close your fist tightly and try to control your anger and hatred." );
        pager_printf( ch, "%s", buf );
        if ( ch-> sex == 0 )
          sprintf( buf, "&G%s squeezes its hands tightly together, and begins to think of a quick combo.", ch->name );
        if ( ch-> sex == 1 )
          sprintf( buf, "&G%s squeezes his hands tightly together, and begins to think of a quick combo.", ch->name );
        if ( ch-> sex == 2 )
          sprintf( buf, "&G%s squeezes her hands tightly together, and begins to think of a quick combo.", ch->name );
        pager_printf( victim, "%s", buf );
        act( AT_SKILL, "$n looks at $N, concentrating hard.", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour combo was interupted!\n\r", ch );
          sprintf( buf, "&G%s's combo was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's combo was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your combo.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch-> sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch-> sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s combo attack.\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_combo ) )
    {
   	  learn_from_success( ch, gsn_combo );
      sprintf( buf, "&GYou pull your arm back and do a quick punch to the head and then a spin kick in %s's head.", PERS( victim, ch ) );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s punches you in the face and then spinkicks you in the side of the head!\n\r", ch->name );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "&G$n punches and kicks $N in the head.", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 30, 40 ) ), gsn_combo );
    }
    else
    {
      learn_from_failure( ch, gsn_combo );
      sprintf( buf, "&GSweat drips off your face as you try to focus your move, but you can not stay focused." );
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s tries to focus on %s move but forgets what he was doing.", ch->name, HISHER(ch->sex) );
      pager_printf( victim, "%s", buf );
      if ( ch-> sex == 0 )
         sprintf( buf, "&G%s tries to focus on his move but forgets what he was doing.", ch->name );
      if ( ch-> sex == 1 )
         sprintf( buf, "&G%s tries to focus on his move but forgets what he was doing.", ch->name );
      if ( ch-> sex == 2 )
         sprintf( buf, "&G%s tries to focus on his move but forgets what he was doing.", ch->name );
      send_to_char( buf, ch );
      act( AT_SKILL, "$n forgot what $e was going to do to $N.", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, 0, gsn_combo );
    }
return;
}

/*
 * S T A R T   O F   S A I Y A N   S K I L L S
 */

/*
 * Big Bang Attack
 */

void do_bigbang( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );
 argument = one_argument( argument, arg2 );

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

/* if ( !check_skill( ch, "big bang", NULL ) )
	return;*/

/* if ( !IS_AFFECTED( ch, AFF_SSJ    )
   && !IS_AFFECTED( ch, AFF_USSJ   )
   && !IS_AFFECTED( ch, AFF_SSJ2   )
   && !IS_AFFECTED( ch, AFF_SSJ3   )
   && !IS_AFFECTED( ch, AFF_SSJ4   )
   && !IS_AFFECTED( ch, AFF_MYSTIC ) )*/

	  if ( ch->plmod < 1200 )
	  {
	  send_to_char( "Your body is too weak to take such a powerful skill", ch );
	  return;
	  }

switch( ch->substate )
    {
	default:

        ch->skill_timer = 9;
        add_timer( ch, TIMER_DO_FUN, 9, do_bigbang, 1 );
        sprintf( buf, "&GYou hold your arm out and glare at %s, trying to look intimidating.\n\r", PERS( victim, ch ));
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&GAn orange and %s &Gball of ki forms at your fingers, and begins to grow.\n\r", ch->energycolour);
        pager_printf( ch, "%s", buf );
        if ( ch-> sex == 0 )
          sprintf( buf, "&G%s holds its arm out and glares at you, looking scary.", ch->name );
        if ( ch-> sex == 1 )
          sprintf( buf, "&G%s holds his arm out and glares at you, looking scary.", ch->name );
        if ( ch-> sex == 2 )
          sprintf( buf, "&G%s holds her arm out and glares at you, looking scary.", ch->name );
        pager_printf( victim, "%s", buf );
        sprintf( buf, "&GAn orange and %s &Gball of ki forms at %s's fingers, and begins to grow.\n\r", ch->energycolour, ch->name);
        pager_printf( victim, "%s", buf );
        act( AT_SKILL, "$n holds $s arm out and charges $s ki, glaring at $N.", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour big bang was interupted!\n\r", ch );
          sprintf( buf, "&G%s's big bang was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's big bang was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your big bang.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch-> sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch-> sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s big bang.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_bigbang ) )
    {
   	  learn_from_success( ch, gsn_bigbang );
      sprintf( buf, "&GYou scream '&RBig Bang Attack!&G' and fire your huge ball of ki at %s!!\n\r", PERS( victim, ch ));
      pager_printf( ch, "%s", buf );
      sprintf( buf, "&G%s screams '&RBig Bang Attack!&G' and fires the huge ball of ki at you!!\n\r", PERS( victim, ch ));
      pager_printf( victim, "%s", buf );
      act( AT_GREEN, "$n launches $s charged up Big Bang at $N!", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 1000, 1250 ) ), gsn_bigbang );
    }
    else
    {
      learn_from_failure( ch, gsn_bigbang );
      sprintf( buf, "&GYou try to remain calm, but your big bang is a big flop..." );
      pager_printf( ch, "%s", buf );
      if ( ch-> sex == 0 )
          sprintf( buf, "&G%s puts its arm out, but only a wimpy burst of ki appears.", ch->name );
      if ( ch-> sex == 1 )
          sprintf( buf, "&G%s puts his arm out, but only a wimpy burst of ki appears.", ch->name );
      if ( ch-> sex == 2 )
          sprintf( buf, "&G%s puts her arm out, but only a wimpy burst of ki appears.", ch->name );
      pager_printf( victim, "%s", buf );
      act( AT_SKILL, "$n tries to Big Bang $N, but fails miserably.", ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, 0, gsn_bigbang );
    }
return;
}

/*
 * Kamehameha Wave Attack
 */

void do_kamehameha( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 5;
        add_timer( ch, TIMER_DO_FUN, 5, do_kamehameha, 1 );
        sprintf( buf, "&GYou put your foot on %s's chest and push backwards, landing some distance away.", PERS( victim, ch ) );
	    pager_printf(ch, "%s\n\r", buf );
    	sprintf( buf, "&GPutting both hands together, you begin to charge your ki, forming a %s &Gball.", ch->energycolour );
    	pager_printf(ch, "%s\n\r", buf );
	    sprintf( buf, "&G%s puts a foot on your chest and pushes backwards, landing some distance away.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
        if ( ch->sex == 0 )
    	   sprintf( buf, "&GPutting both hands together, %s begins to charge its ki, forming a %s &Gball.", ch->name, ch->energycolour );
        if ( ch->sex == 1 )
 	       sprintf( buf, "&GPutting both hands together, %s begins to charge his ki, forming a %s &Gball.", ch->name, ch->energycolour );
        if ( ch->sex == 2 )
     	   sprintf( buf, "&GPutting both hands together, %s begins to charge her ki, forming a %s &Gball.", ch->name, ch->energycolour );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n holds $s arm out and charges $s ki, glaring at $N.", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour kamehameha was interupted!\n\r", ch );
          sprintf( buf, "&G%s's kamehameha was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's kamehameha was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your kamehameha.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s kamehameha.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_kamehameha ) )
    {
   	  learn_from_success( ch, gsn_kamehameha );
	  sprintf( buf, "&GFinally, you scream '&RKA-ME-HA-ME... HA!&G' and fire an orange and %s &Gki beam at %s's chest.", ch->energycolour, PERS( victim, ch ) );
	  pager_printf(ch, "%s\n\r", buf );     
	  sprintf( buf, "&GFinally, %s screams '&RKA-ME-HA-ME... HA!&G' and fires an orange and %s &Gki beam at you!", ch->name, ch->energycolour );
	  pager_printf(victim, "%s\n\r", buf );
	  act( AT_GREEN, "$n screams 'KAMEHAMEHA!' and fires a beam of ki at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, ( number_range( 200, 300 ) ), gsn_kamehameha );
	  if ( victim->hit <= 500 && !IS_NPC(ch) && ch->race == RACE_SAIYAN && ch->plmod >= 2800 && ch->pcdata->learned[gsn_bbk] < 10 )
	  {
		ch->pcdata->learned[gsn_bbk] = 10;
		send_to_char( "In your mind's eye you here the voices of your races heroes, urging you onwards.  To kill your enemy is not enough. No, you need to destroy them, to wipe their very essence from the universe. Power surges through your body and your mind blossoms with knowledge from the spectral voices' urgings. Next time they won't get off so easy, next time they'll learn to spend the last few seconds of their life in fear of your superior race, and your superior Big Bang Kamehameha!\n\r", ch );
	  }
    }
    else
    {
	 send_to_char( "Your kamehameha flies off the wrong direction...\n\r", ch);
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s launches a kamehameha wave at you, but it veers off to the right.", ch->name );
	 else
		 sprintf(buf, "%s launches a kamehameha wave at you, but it veers off to the left.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n screams 'KAMEHAMEHA!' and fires a beam of ki... in the wrong direction.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_kamehameha );
	 global_retcode = damage( ch, victim, 0, gsn_kamehameha );
    }
return;
}

/* Dont feel like writing up a full new skill for this 
   its photon shot for yakon and his mobs, mabye you give it 
    to androids, thats up to you all */

void do_photonshot( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 5;
        add_timer( ch, TIMER_DO_FUN, 5, do_photonshot, 1 );
        sprintf( buf, "&GYou raise your hand parallel to %s.", PERS( victim, ch ) );
	    pager_printf(ch, "%s\n\r", buf );
    	sprintf( buf, "&GYou begin to charge %s &Genergy through your arm.", ch->energycolour );
    	pager_printf(ch, "%s\n\r", buf );
	    sprintf( buf, "&G%s places their hand out in front of them aiming at you!", PERS(ch,victim) );
	    pager_printf(victim, "%s\n\r", buf );
        if ( ch->sex == 0 )
    	   sprintf( buf, "&G%s begins to charge %s &Genergy through their arm.",PERS(ch,victim), ch->energycolour );
        if ( ch->sex == 1 )
 	       sprintf( buf, "&G%s begins to charge a %s &Genergy through their arm.", PERS(ch,victim), ch->energycolour );
        if ( ch->sex == 2 )
     	   sprintf( buf, "&G%s begins to charge a %s &Genergy blast through their arm.",PERS(ch,victim), ch->energycolour );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n holds $s arm out and charges $s ki, glaring at $N.", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour Photon Shot was interupted!\n\r", ch );
          sprintf( buf, "&G%s's Photon Shot was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's Photon Shot was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your Photon Shot.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
        ch->skill_timer = 0;
    	act( AT_GREEN, "$n's cancels $s Photon Shot.\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_photonshot ) )
    {
   	  learn_from_success( ch, gsn_photonshot );
sprintf(buf, "&GYou release a beam of %s &Genergy at %s!", ch->energycolour, PERS( victim, ch ) );
	  pager_printf(ch, "%s\n\r", buf );     
sprintf(buf, "&G%s releases a stream of %s &Genergy at you",PERS(ch,victim), ch->energycolour );
	  pager_printf(victim, "%s\n\r", buf );
 act( AT_GREEN, "$n shoots a Photon Shot at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 125, 250 ) ), gsn_photonshot );
    }
    else
    {
	 send_to_char( "Your photon shot sputters out...\n\r", ch);
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s's photon shot sputters out.", ch->name );
	 else
		 sprintf(buf, "%s's photon shot misses you by a mile.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n fires a photon shot but it sputters out.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_photonshot );
	 global_retcode = damage( ch, victim, 0, gsn_photonshot );
    }
return;
}


/*
 * Final Flash Attack
 */

void do_finalflash( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );

 if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

            ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_finalflash, 1 );
	    sprintf( buf, "&GYou feel your anger rising, and suddenly all you see is %s.", PERS( victim, ch ) );
	    pager_printf(ch, "%s\n\r", buf );
	    sprintf( buf, "&GPutting your arms either side of you, you explode in %s &Genergy as you form two balls of ki.", ch->energycolour );
	    pager_printf(ch, "%s\n\r", buf );
	    sprintf( buf, "&G%s looks greatly enraged, and glares at you with renewed hatred and anger.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
	    sprintf( buf, "&GWith arms spread wide, %s begins to charge up, forming two balls of %s&G ki.", ch->name, ch->energycolour );
	    pager_printf(victim, "%s\n\r", buf );
	    act( AT_GREEN, "$n looks angrily at $N, and begins to charge $s ki...\n\r",  ch, NULL, victim, TO_NOTVICT );


        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour final flash was interupted!\n\r", ch );
          sprintf( buf, "&G%s's final flash was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's final flash was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your final flash.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s final flash.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_finalflash ) )
    {
   	 learn_from_success( ch, gsn_finalflash );
	 sprintf( buf, "&GYou bring your arms in front of you and, screaming '&RFINAL FLASH!&G', fire a %s&G beam of ki at %s.", ch->energycolour, PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 sprintf( buf, "&GFinally, %s screams '&RFINAL FLASH!&G' and fires a %s&G ki beam at you!", ch->name, ch->energycolour );
	 pager_printf(victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n screams 'FINAL FLASH!' and fires a beam of ki at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 400, 900 ) ), gsn_finalflash );
}
    else
    {
	 send_to_char( "You fire your final flash but miss completely...\n\r", ch);
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s launches a Final Flash at you, but it veers off to the right.", ch->name );
	 else
		 sprintf(buf, "%s launches a Final Flash at you, but it flies right over your head.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n screams 'FINAL FLASH!' and fires a beam of ki... in the wrong direction.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_finalflash );
	 global_retcode = damage( ch, victim, 0, gsn_finalflash );
    }
return;
}

/*
 * Chou Kamehameha Wave Attack
 */

void do_choukamehameha( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

 if ( ch->plmod < 2000 )
	{
	send_to_char( "Your body is too weak to take such a powerful skill", ch );
	return;
	}

switch( ch->substate )
    {
	default:

                ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_choukamehameha, 1 );
		sprintf( buf, "&GYou explode outwards in a larger aura of energy as your mind focuses completely." );
		pager_printf(ch, "%s\n\r", buf );
		sprintf( buf, "&GGathering all your energies, you draw back and form a large ball of %s&G ki in your hands.", ch->energycolour );
		pager_printf(ch, "%s\n\r", buf );
		sprintf( buf, "&G%s explodes outwards in a larger aura of energy, and looks you in the eyes.", ch->name );
		pager_printf(victim, "%s\n\r", buf );
		if ( ch->sex == 0 )
			sprintf( buf, "&GThe aura flares higher around %s, as it forms a ball of %s ki which begins to grow...", ch->name, ch->energycolour );
		if ( ch->sex == 1 )
			sprintf( buf, "&GThe aura flares higher around %s, as he forms a ball of %s ki which begins to grow...", ch->name, ch->energycolour );
		if ( ch->sex == 2 )
			sprintf( buf, "&GThe aura flares higher around %s, as she forms a ball of %s ki which begins to grow...", ch->name, ch->energycolour );
		pager_printf(victim, "%s\n\r", buf );
		act( AT_GREEN, "$n screams and $s aura explodes to about twice it's size!\n\r",  ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour chou kamehameha was interupted!\n\r", ch );
          sprintf( buf, "&G%s's chou kamehameha was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's chou kamehameha was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );


          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
         break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your chou kamehameha.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
          ch->skill_timer = 0;
           pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s chou kamehameha.\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_choukamehameha ) )
    {
   	 learn_from_success( ch, gsn_choukamehameha );
	 sprintf( buf, "&GFinally, %s screams '&RKA - ME - HA - ME - HA!&G' and launches a huge %s ki beam at you!", ch->name, ch->energycolour );
	 pager_printf(victim, "%s\n\r", buf );
	 sprintf( buf, "&GYou bring your arms in front of you and, screaming '&RKA - ME - HA - ME - HA!&G', launch a huge %s&G beam of ki at %s.", ch->energycolour, PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 act( AT_GREEN, "$n screams and fires a chou kamehameha wave at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, ( number_range( 1000, 2500 ) ), gsn_choukamehameha );
         if ( !IS_NPC(ch) && ch->race == RACE_HALFBREED && ch->plmod >= 3400 && victim->race == RACE_BIOANDROID &&ch->pcdata->learned[gsn_parent_child_kamehameha] < 10 )
         {
                ch->pcdata->learned[gsn_parent_child_kamehameha] = 10;
		pager_printf( ch, "&RThe abomination before you causes your blood to boil. As a child of two bloods, you know the pain of being two races, and this... thing embodies that suffering.  As the white hot hatered stabs through you, a strange moment of calm overtakes you. Your %s's voice whispers softly in your ear: &C'I'm always with you %s'.&R You feel as if %s is standing beside you,  and with %s there, nothing could stand before you!\n\r", ch->sex == 2 ? "mother" : "father", ch->sex == 2 ? "daughter" :"son", HESHE(ch->sex), HIMHER(ch->sex) );
         }

	}
    else
    {
	 sprintf(buf, "You lose concentration and miss %s with your Chou Kamehameha\n\r", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s launches a Chou Kamehameha at you, but you jump out of the way in time.", ch->name );
	 else
		 sprintf(buf, "%s launches a Chou Kamehameha at you, but it flies right over your shoulder.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n launches $s Chou Kamehameha at $N, who deftly jumps out of the way.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_choukamehameha );
	 global_retcode = damage( ch, victim, 0, gsn_choukamehameha );
    }
return;
}

/*
 * Big Bang flash Wave
 */

void do_bigbangflash( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg[MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );

 if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

 if (    !IS_AFFECTED( ch, AFF_SSJ2 )
      && !IS_AFFECTED( ch, AFF_SSJ3 )
      && !IS_AFFECTED( ch, AFF_SSJ4 )
      && !IS_AFFECTED( ch, AFF_MYSTIC ) )
	{
	send_to_char( "Your body is too weak to take such a powerful skill", ch );
	return;
	}

switch( ch->substate )
    {
	default:

        ch->skill_timer = 10;
        add_timer( ch, TIMER_DO_FUN, 10, do_bigbangflash, 1 );
	    sprintf( buf, "&GYour aura flares brightly as you form a large ball of %s &Gki \n\rand glare at %s. &GYou give in to rage, screaming '&RBIG BANG FLASH!&G'\n\ras you charge your ki wave", ch->energycolour, PERS( victim, ch ) );
	    pager_printf(ch, "%s\n\r", buf );
	    sprintf( buf, "&G%s's aura flares brightly as they form a ball of %s &Gki, glaring at you.", ch->name, ch->energycolour );
	    pager_printf(victim, "%s\n\r", buf );
		act( AT_GREEN, "$n's aura flares up as $e charges a big bang flash\n\r",  ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour big bang flash was interupted!\n\r", ch );
          sprintf( buf, "&G%s's big bang flash was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's big bang flash was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your big bang flash.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s big bang flash.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_bigbangflash ) )
    {
   	 learn_from_success( ch, gsn_bigbangflash );
	 sprintf( buf, "&G%s screams '&RBIG BANG FLASH&G' and launches a %s &Gki wave at you!", ch->name, ch->energycolour );
	 pager_printf(victim, "%s\n\r", buf );
	 sprintf( buf, "&GThe moment the ki wave hits it explodes with devastating force, filling the area with smoke" );
	 pager_printf(victim, "%s\n\r", buf );
	 sprintf( buf, "&GYou scream '&RBIG BANG FLASH&G' and launches a %s &Gki wave at %s.  The moment your attack connects it explodes with devastating force, filling the area with smoke.", ch->energycolour, PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 act( AT_GREEN, "$n fires $s Big Bang Flash at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
	 SET_BIT( ch->in_room->room_flags, ROOM_TRASHED );
     global_retcode = damage( ch, victim, ( number_range( 1000, 2000 ) ), gsn_bigbangflash);
	}
    else
    {
	 sprintf(buf, "You lose concentration and miss %s with your Big Bang flash\n\r", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s launches a Big Bang flash at you, but you jump out of the way in time.", ch->name );
	 else
		 sprintf(buf, "%s launches a Big Bang flash at you, but it flies right over you.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n launches a Big Bang Flash at $N, who deftly jumps out of the way.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_bigbangflash );
	 global_retcode = damage( ch, victim, 0, gsn_bigbangflash );
    }
return;
}




void do_finalshine( CHAR_DATA *ch, char *argument )
{
CHAR_DATA *victim;
char buf[MAX_STRING_LENGTH];
char arg[MAX_INPUT_LENGTH];
int vnum = 0, attempt, wherehit = 0;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

 argument = one_argument( argument, arg );

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }
 if ( ch->plmod < 2600 )
	{
	send_to_char( "Your body is too weak to take such a powerful skill.", ch );
	return;
	}

switch( ch->substate )
    {
	default:
    ch->skill_timer = 14;
    add_timer( ch, TIMER_DO_FUN, 14, do_finalshine, 1 );
    ch->target = victim;
 	sprintf( buf, "&GThe power of your ancestry flows through your veins as you begin to form a truly enormous ball of %s &Gki", ch->energycolour );
	pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&GYou realise your are unbeatable as you lift your right arm at %s, your energy blowing your hair back.", PERS( victim, ch ) );
	pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s draws back and begins to form two enormous balls of %s &Gand orange ki, staring intently.", ch->name, ch->energycolour );
	pager_printf(victim, "%s\n\r", buf );
	if ( ch->sex == 1 )
		sprintf( buf, "&GEvery hair on %s's head is blown straight back as he puts his hand in front of him.", ch->name );
	if ( ch->sex == 2 )
		sprintf( buf, "&GEvery hair on %s's head is blown straight back as she puts her hand in front of her.", ch->name );
	if ( ch->sex == 0 )
		sprintf( buf, "&GEvery hair on %s's head is blown straight back as it puts its hand in front of it.", ch->name );
	pager_printf(victim, "%s\n\r", buf );
	act( AT_GREEN, "$n begins to charge a Final Shine.\n\r",  ch, NULL, victim, TO_NOTVICT );


        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour final shine was interupted!\n\r", ch );
          sprintf( buf, "&G%s's final shine was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's final shine was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your final shine.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s final shine.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( ch->in_room != victim->in_room )
    {
      for ( attempt = 0; attempt < 8; attempt++ )
      {
        door = number_door( );
        if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
             continue;
        if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
        {
        pager_printf( victim, "&P%s's mouth beam flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
        pager_printf( ch, "Your beam flies %s!\n\r", dir_name[pexit->vdir] );
        vnum = victim->in_room->vnum;
        char_from_room( victim );
        char_to_room( victim, ch->in_room );
        moved = TRUE;
        }
}
}
    if ( can_use_skill(ch, number_percent(),gsn_finalshine ) )
    {
   	learn_from_success( ch, gsn_finalshine );
	sprintf( buf, "&GYou scream '&RFINAL SHINE!&G' and launch an enormous %s and orange ki beam at %s!", ch->energycolour, PERS( victim, ch ) );
	pager_printf(ch, "%s\n\r", buf );
	if ( wherehit == 1 )
		sprintf( buf, "&RThe ki beam rips through %s's chest, spraying blood everywhere.", PERS( victim, ch ) );
	if ( wherehit == 2 )
		sprintf( buf, "&RThe ki beam slams into %s's legs, damaging them forever.", PERS( victim, ch ) );
	if ( wherehit == 3 )
		sprintf( buf, "&RThe ki beam tears through %s's left arm, wounding it permenantly.", PERS( victim, ch ) );
 	if ( wherehit == 4 )
	 	sprintf( buf, "&RThe ki beam envelops %s's head, scarring and burning them forever.", PERS( victim, ch ) );
	 sprintf( buf, "&G%s screams '&RFINAL SHINE!&G' and launches an orange and %s &Gki beam at you!", ch->name, ch->energycolour);
	 pager_printf(victim, "%s\n\r", buf );

	 if ( wherehit == 1 )
		xSET_BIT( victim->affected_by, AFF_TRAUMA );
	 if ( wherehit == 2 )
		xSET_BIT( victim->affected_by, AFF_TIRED );
	 act( AT_GREEN, "$n fires $s Final Shine at $N, a truly enormous beam of ki flying from $s hand!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	 learn_from_success( ch, gsn_finalshine );

         global_retcode = damage( ch, victim, ( number_range( 5000, 8000 )), gsn_finalshine );
	}
    else
    {
	 sprintf(buf, "You lose concentration and miss %s with your Final Shine\n\r", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	 if ( number_range(1, 2) == 1 )
		 sprintf(buf, "%s launches a Final shine at you, but you jump out of the way in time.", ch->name );
	 else
		 sprintf(buf, "%s launches a Final shine at you, but it flies right over you.", ch->name );
	 pager_printf( victim, "%s\n\r", buf );
	 act( AT_GREEN, "$n launches a Final shine at $N, who deftly jumps out of the way.\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_finalshine );
	 global_retcode = damage( ch, victim, 0, gsn_finalshine );

    }
ch->target = NULL;
if ( moved == TRUE )
{
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ) );
     act( AT_GREEN, "$n's Final Shine flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
}
return;
}



/*
 * Warp Kamehameha Wave Attack
 */

void do_warpkamehameha( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_warpkamehameha, 1 );
	sprintf( buf, "&GYou take a small leap back from %s and begin charging a %s &Gball of ki.", PERS( victim, ch ), ch->energycolour );
	pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s takes a small leap back from you and begins to charge a %s &Gball of ki.", ch->name, 
ch->energycolour );
	pager_printf(victim, "%s\n\r", buf );
	act( AT_ACTION, "$n hits $N with a Warp Kamehameha!\n\r",  ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour warp kamehameha was interupted!\n\r", ch );
          sprintf( buf, "&G%s's warp kamehameha was interupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's warp kamehameha was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );

	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your warp kamehameha.\n\r", ch );
        if ( ch-> sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s warp kamehameha.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
        return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_warpkamehameha ) )
    {
   	 learn_from_success( ch, gsn_warpkamehameha );
	 sprintf( buf, "&G%s takes advantage of the situation and launches a %s &Gki blast at you!", PERS( victim, ch ), victim->energycolour );
	 pager_printf(ch, "%s\n\r", buf );
	 sprintf( buf, "&GMuttering '&Rka-me-ha-me...&G' you quickly instant transmission behind %s.", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );
	if ( victim->sex == 1 )
	 sprintf( buf, "&GYou scream '&RHA!&G' and fire a massive kamehameha wave at %s, heading right at him.", PERS( victim, ch ) );
	if ( victim->sex == 2 )
	 sprintf( buf, "&GYou scream '&RHA!&G' and fire a massive kamehameha wave at %s, heading right at her.", PERS( victim, ch ) );
	if ( victim->sex == 0 )
	 sprintf( buf, "&GYou scream '&RHA!&G' and fire a massive kamehameha wave at %s, heading right at it.", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );

	 sprintf( buf, "&GYou take advantage of the situation and fire off a ball of ki at %s!", ch->name );
	 pager_printf(victim, "%s\n\r", buf );
	 sprintf( buf, "&GSuddenly %s disappears from your view completely, until you hear '&RHA!&G'...", ch->name );
	 pager_printf(victim, "%s\n\r", buf );
	 sprintf( buf, "&G...and a huge %s wave of ki rips right through you!", ch->energycolour );
	 pager_printf(victim, "%s\n\r", buf );
	      global_retcode = damage( ch, victim, ( number_range( 250, 500 ) ), gsn_warpkamehameha );
	      global_retcode = damage( ch, victim, ( number_range( 175, 500 ) ), gsn_warpkamehameha );
	      global_retcode = damage( ch, victim, ( number_range( 100, 500 ) ), gsn_warpkamehameha );
	}
    else
    {
   	 learn_from_failure( ch, gsn_warpkamehameha );
	 send_to_char( "\n\r", ch );
	 sprintf( buf, "&G%s takes advantage of the situation and launches a %s &Gki blast at you!", PERS( victim, ch ), victim->energycolour );
	 pager_printf(ch, "%s\n\r", buf );
	 sprintf( buf, "&GYou try to instant transmission behind %s, but fail!  An energyball hits you in the chest!", PERS( victim, ch ) );
	 pager_printf(ch, "%s\n\r", buf );	 

	 sprintf( buf, "&GYou take advantage of the situation and fire off a %s ball of ki at %s!", victim->energycolour, ch->name );
	 pager_printf(victim, "%s\n\r", buf );
	if ( victim->sex == 0 )
	 sprintf( buf, "&G%s doesn't get out of the way in time, and you hit him in the chest!", PERS( victim, ch ) );
	if ( victim->sex == 1 )
	 sprintf( buf, "&G%s doesn't get out of the way in time, and you hit her in the chest!", PERS( victim, ch ) );
	if ( victim->sex == 2 )
	 sprintf( buf, "&G%s doesn't get out of the way in time, and you hit it in the chest!", PERS( victim, ch ) );
	 pager_printf(victim, "%s\n\r", buf );

	 act( AT_ACTION, "$n's Warp Kamehameha backfires totally!\n\r",  ch, NULL, victim, TO_NOTVICT );
	 learn_from_failure( ch, gsn_warpkamehameha );
 	 global_retcode = damage( victim, ch, number_range( 10, 60 ), gsn_kiblast );
	 learn_from_success( victim, gsn_kiblast );
    }
return;
}

/*
 * Genki Dama - Spirit bomb
 */

void do_spiritbomb( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

 if ( IS_AFFECTED( ch, AFF_SSJ )
      || IS_AFFECTED( ch, AFF_USSJ )
      || IS_AFFECTED( ch, AFF_SSJ2 )
      || IS_AFFECTED( ch, AFF_SSJ3 )
      || IS_AFFECTED( ch, AFF_SSJ4 )
      || IS_AFFECTED( ch, AFF_MYSTIC ) )
	{
	send_to_char( "Your transformation does not allow you to use such a technique", ch );
	return;
	}

    switch( ch->substate )
    {
	default:

         ch->skill_timer = 20;
       add_timer( ch, TIMER_DO_FUN, 20, do_spiritbomb, 1 );
	 sprintf( buf, "&GYou hurl both your hands in the air, and yell" );
	 pager_printf(ch, "%s\n\r", buf );
       sprintf( buf, "'I call upon the Suns, the Planet, living creatures, give me your energy!'" );
	 pager_printf(ch, "%s\n\r", buf );

	 sprintf( buf, "&G%s hurls both hands in the air, and yells out", ch->name );
	 pager_printf(victim, "%s\n\r", buf );
     sprintf( buf, "'I call upon the Suns, the Planet, living creatures, give me your energy!'" );
	 pager_printf(victim, "%s\n\r", buf );

	 act( AT_GREEN, "$n stops moving and puts $s hands in the air...\n\r",  ch, NULL, victim, TO_NOTVICT );

       ch->alloc_ptr = str_dup( arg );
	 return;

	 case 1:
	    if ( !ch->alloc_ptr )
	    {
		send_to_char( "Your spirit bomb was interupted and all of it's energy diffuses!\n\r", ch );
        sprintf( buf, "&G%s's spirit bomb was interrupted and the collected energy dissapates!", ch->name );
	 	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's spirit bomb was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
     	bug( "do_search: alloc_ptr NULL", 0 );
        ch->skill_timer = 0;
		return;
	    }
	    strcpy( arg, ch->alloc_ptr );

	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your spirit bomb...\n\r", ch );

          sprintf( buf, "&G%s's cancels the spirit bomb!", ch->name );
	    pager_printf(victim, "%s\n\r", buf );

    	    act( AT_GREEN, "$n's cancels $s spirit bomb!\n\r",  ch, NULL, victim, TO_NOTVICT );

	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_spiritbomb ) )
        {
		if ( ch->alignment > 0 )
		{
		  sprintf( buf, "&GYou harness the energy that they have lent you, and form it into a powerful ball of all that good represents and hurl it towards %s", PERS( victim, ch ) );
		  pager_printf(ch, "%s\n\r", buf );
	          sprintf( buf, "&G%s forms the energy into a powerful ball of all that good represents and hurls it towards you", ch->name );
		  pager_printf(victim, "%s\n\r", buf );
		  act( AT_GREEN, "$n fires a spirit bomb at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
	 	  learn_from_success( ch, gsn_spiritbomb );
	 	  global_retcode = damage( ch, victim, ( 0 - victim->alignment ) * 50, gsn_spiritbomb );
		}
		else
		{
		  sprintf( buf, "&GYou realise far too late that your heart isn't pure enough to take a spirit bomb, and it explodes in your face!" );
		  pager_printf(ch, "%s\n\r", buf );
	
		  if ( ch->sex == 0 )
		        sprintf( buf, "&G%s forms the energy into a powerful ball of all that good represents... and it blows up in its face!", ch->name );
		  if ( ch->sex == 1 )
		        sprintf( buf, "&G%s forms the energy into a powerful ball of all that good represents... and it blows up in his face!", ch->name );
		  if ( ch->sex == 2 )
		        sprintf( buf, "&G%s forms the energy into a powerful ball of all that good represents... and it blows up in her face!", ch->name );
		  pager_printf(victim, "%s\n\r", buf );
		  act( AT_GREEN, "$n's spirit bomb blows up in $s face!!\n\r",  ch, NULL, victim, TO_NOTVICT );
		  learn_from_failure( ch, gsn_spiritbomb );
	 	  global_retcode = damage( ch, ch, ( 0 - ch->alignment ) * 5, gsn_spiritbomb );
		}
	}
        else
	{
	   sprintf(buf, "You fail to collect any energy from the planets and cosmos.\n\r" );
	   pager_printf(ch, "%s\n\r", buf );
	   sprintf(buf, "%s fails to collect any energy from the planets and cosmos.\n\r", PERS(ch,victim) );
	   pager_printf(victim, "%s\n\r", buf );
	   act( AT_GREEN, "$n fails to collect any energy from the planets and cosmos.\n\r",  ch, NULL, victim, TO_NOTVICT  );
	   learn_from_failure( ch, gsn_spiritbomb );
 	}
return;
}


/*
 * S T A R T   O F   H U M A N   S K I L L S
 */

/*
 * Destructo Disk
 */

void do_destructodisk( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_destructodisk, 1 );
        sprintf( buf, "&GYou put one hand in the air, and begin to form a spinning disk of %s &Genergy.", ch->energycolour );
        pager_printf( ch, "%s\n\r", buf );
        sprintf( buf, "&G%s puts one hand in the air and begins to form a spinning disk of %s &Genergy.", ch->name, ch->energycolour );
        pager_printf( victim, "%s\n\r", buf );
        act( AT_GREEN, "$n puts one hand in the air and begins to form a spinning disk of energy.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your Destructo Disk was interupted!\n\r", ch );
          sprintf( buf, "&G%s's Destructo Disk was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's Destructo Disk was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your Destructo Disk.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s Destructo Disk!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_destructodisk ) )
    {
    	learn_from_success( ch, gsn_destructodisk );
        sprintf( buf, "&GYour destructo disk complete, you hurl it at %s, cutting into %s chest.", PERS( victim, ch ), victim->sex == 2 ? "her" : "his" );
        pager_printf( ch, "%s\n\r", buf );
        sprintf( buf, "&G%s throws %s destructo disk at you, and it cuts right into your chest!", PERS( ch, victim ), ch->sex == 2 ? "her" : "his" );
        pager_printf( victim, "%s\n\r", buf );
        act( AT_GREEN, "$n throws $s destructo disk at $N, hitting $M in the chest!\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, ( number_range( 333, 666 ) ), gsn_destructodisk );
    }
    else
    {
        learn_from_failure( ch, gsn_destructodisk );
        sprintf( buf, "&GYour destructo disk complete, you hurl it at %s, missing %s completely.", PERS(victim,ch), victim->sex == 2 ? "her" : "him" );
        pager_printf( ch, "%s\n\r", buf );
        sprintf( buf, "&G%s throws %s destructo disk at you, but it misses you completely!", PERS( ch, victim ), ch->sex == 2 ? "her" : "his" );
        pager_printf( victim, "%s\n\r", buf );
        act( AT_GREEN, "$n throws $s destructo disk at $N, but misses $M completely!\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_destructodisk );
    }
return;
}

/*
 * Tribeam
 */

void do_tribeam( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

    ch->skill_timer = 8;
    add_timer( ch, TIMER_DO_FUN, 8, do_tribeam, 1 );
	sprintf( buf, "&GYou form a triangle with your hands and line up %s in your sights.", PERS( victim, ch ) );
	pager_printf( ch, "%s\n\r", buf );
    act( AT_GREEN, "$n forms a triangle with $s hands and lines $N up in $s sights..\n\r", ch, NULL, victim, TO_VICT );
    ch->alloc_ptr = str_dup( arg );
    return;

	case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your tribeam was interupted!\n\r", ch );
          sprintf( buf, "&G%s's tribeam was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's tribeam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your tribeam.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s tribeam!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_tribeam ) )
    {
   	learn_from_success( ch, gsn_tribeam );
	sprintf( buf, "&GYou thrust your hands forward and yell '&YTRIBEAM - HA!&G', firing a massive %s &Gki wave at %s.", ch->energycolour, PERS( victim, ch ) );
	pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s thrust forwards, yelling '&YTRIBEAM - HA!&G' and firing a massive %s &Gki wave at you.", ch->name, ch->energycolour );
	pager_printf(victim, "%s\n\r", buf );
	act( AT_GREEN, "$n thrusts $s hands forward, yelling 'TRIBEAM - HA!&g', firing a massive ki wave at $N!..\n\r", ch, NULL, victim, TO_NOTVICT );
	ch->hit -= ( ch->max_hit / 4 );
	global_retcode = damage( ch, victim, ( number_range( 4000, 8000 ) ), gsn_tribeam );
    }
    else
    {
    learn_from_failure( ch, gsn_tribeam );
	sprintf( buf, "&GYou thrust your hands forward and yell '&YTRIBEAM - HA!&G', but nothing happens." );
	pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s thrust forwards, yelling '&YTRIBEAM - HA!&G', but nothing happens.", ch->name );
	pager_printf(victim, "%s\n\r", buf );
    act( AT_GREEN, "$n thrusts $s hands forward, yelling 'TRIBEAM - HA!&G', but nothing happens...\n\r", ch, NULL, victim, TO_NOTVICT );
    global_retcode = damage( ch, victim, 0, gsn_tribeam );
    }
return;
}


/*
 * S T A R T   O F   H A L F B R E E D   S K I L L S
 */

/*
 * Masenko Beam
 */

void do_masenko( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 3;
        add_timer( ch, TIMER_DO_FUN, 3, do_masenko, 1 );
        sprintf( buf, "&GYou close your fist tight and concentrate your energy.\n\r" );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s squeezes his hands tightly together, begins to glow %s&G.\n\r", ch->name, ch->energycolour );
        pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n looks at $N, concentrating hard.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your masenko was interupted!\n\r", ch );
          sprintf( buf, "&G%s's masenko was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's masenko was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your masenko.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s masenko!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_masenko ) )
    {
   	    learn_from_success( ch, gsn_masenko );
        sprintf( buf, "&GYou charge up a big ball of %s &Genergy. Holding your arms straight you yell '&RMASENKO!&G'.  You release your ki and throw it at %s!\n\r", ch->energycolour, PERS( victim, ch ) );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s yells '&RMASENKO BEAM!&G' and throws a %s &Gbeam of energy at you!\n\r", ch->name, ch->energycolour );
        pager_printf( victim, "%s", buf );
        act( AT_SKILL, "&G$n fires a masenko beam at $N, hitting $M hard in the chest!\n\r", ch, NULL, victim, TO_NOTVICT );
       global_retcode = damage( ch, victim, ( number_range( 75, 150 ) ), gsn_masenko );
    }
    else
    {
        learn_from_failure( ch, gsn_masenko );
        sprintf( buf, "&GYou start to gather your energy, but cannot stay focused.\n\r" );
        pager_printf( ch, "%s", buf );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s suddenly looks like it forgot what it was going to do.\n\r", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s suddenly looks like he forgot what he was going to do.\n\r", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s suddenly looks like she forgot what she was going to do.\n\r", ch->name );
        pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n tries to fire a masenko beam at $N, but fails.\n\r", ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_masenko );
    }
return;
}

/*
 * Icer and Bio Tailgrapple
 */

void do_tailgrapple( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 3;
        add_timer( ch, TIMER_DO_FUN, 3, do_tailgrapple, 1 );
        sprintf( buf, "&GYou draw your tail and point it over your shoulder.\n\r" );
        pager_printf( ch, "%s", buf );
        sprintf( buf, "&G%s's tail perks up and points towards you over %s shoulder.\n\r", ch->name, ch->sex == 2? "her" : "his" );
        pager_printf( victim, "%s", buf );
        act( AT_GREEN, "$n raises $s tail and points it at $N.\n\r", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
            return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your grapple was interupted!\n\r", ch );
          sprintf( buf, "&G%s's grapple was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's grapple was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;
         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your grapple.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s tail grapple!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_tailgrapple ) )
    {
            learn_from_success( ch, gsn_tailgrapple );
    sprintf( buf, "&GYou wrap your tail around %s, and proceed to beat the crap out of %s!!\n\r", PERS( victim, ch ), victim->sex == 0? "it" : victim->sex == 1 ? "him" : "her" );
    pager_printf( ch, "%s", buf );
    sprintf( buf, "&G%s wraps %s tail around you, and begins to beat the crap out of you!!\n\r", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her"  );
    pager_printf( victim, "%s", buf );
    act( AT_SKILL, "&G$n wraps $s tail around $N, and proceeds to beat the crap out of $M!!\n\r", ch, NULL, victim, TO_NOTVICT );
    global_retcode = damage( ch, victim, number_range( 300, 600), gsn_tailgrapple );
    }
    else
    {
    learn_from_failure( ch, gsn_tailgrapple );
    sprintf( buf, "&GYou try to wrap your tail around %s, but %s escapes.\n\r", PERS( victim, ch ), ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she" );
    pager_printf( ch, "%s", buf );
    sprintf( buf, "&G%s lunges at you, but you dodge out of the way.\n\r", ch->name  );
    pager_printf( victim, "%s", buf );
    act( AT_SKILL, "&G$n lunges at $N, but $E slips away.\n\r", ch, NULL, victim, TO_NOTVICT );
    global_retcode = damage( ch, victim, 0, gsn_tailgrapple );
    }
return;
}

/*
 * Death Ball - Icers
 */

void do_supernovaicer( CHAR_DATA *ch, char *argument )
{
  CHAR_DATA *victim;
  char arg[MAX_INPUT_LENGTH];
  if ( ( victim = who_fighting( ch ) ) == NULL )
  {
      send_to_char( "You aren't fighting anyone.\n\r", ch );
      return;
  }
  switch ( ch->substate )
  {
	default:
		if ( !IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
		{
		    send_to_char( "You must be in your form for this to work.\n\r", ch );
		    return;
		}
		ch->skill_timer = 14;
		add_timer( ch, TIMER_DO_FUN, 14, do_supernovaicer, 1 );
		pager_printf( ch, "&GYou raise a hand, pointing up towards the sky.  A smouldering ball of energy forms at the tip of your hand, growing and drawing in power from the destructive feelings in your heart and the surrounding world.  You laugh casually at %s: &C'This is it, my %s friend.  It's time for this battle to draw to a close, for you will almost certainly be struck down by this attack.  Prepare yourself... For Supernova!'&G  You try to stop yourself, but a smug grin spreads across your face nevertheless.\n\r", victim->name, race_table[victim->race] );
		act ( AT_GREEN, "$n extends one arm into the sky, and laughs to $mself.  As a smouldering ball of ki forms at $s fingertips, $e opens $s mouth and says &C'This is it, $N, my friend.  It's time for this battle to draw to a close, for you will almost certainly be struck down by this attack.  Prepare yourself... For Supernova!'", ch, NULL, victim, TO_CANSEE );
		ch->alloc_ptr = str_dup( arg );
		return;
        case 1:
	        if ( !ch->alloc_ptr )
        	{
 		    send_to_char( "Your Supernova was interupted!\n\r", ch );
	            act( AT_GREEN, "$n's Supernova was interupted!\n\r",  ch, NULL, victim, TO_CANSEE );
	            bug( "do_search: alloc_ptr NULL", 0 );
	            return;
          	}
                strcpy( arg, ch->alloc_ptr );
                DISPOSE( ch->alloc_ptr );
                break;

        case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
                send_to_char( "You stop your Supernova.\n\r", ch );
	        act( AT_GREEN, "$n's cancels $s Supernova!\n\r",  ch, NULL, victim, TO_CANSEE );
                return;
  }
  ch->substate = SUB_NONE;
  if ( can_use_skill( ch, number_percent(), gsn_supernovaicer ) )
  {
	learn_from_success( ch, gsn_supernovaicer );
	pager_printf( ch, "&GAn evil grin spreads across your face as you can contain yourself no longer.  Flinging your arm forwards, you drive your immense supernova down at your hapless foe, certain that it will crush %s puny body into oblivion.\n\r", HISHER( victim->sex ) );
	pager_printf( victim, "&GYou realise that %s's technique is finished moments before %s throws it down at you.  You attempt to brace yourself against it, but you have to wonder if it will be enough...\n\r", ch->name, HESHE( ch->sex ) );
	act( AT_GREEN, "$n launches $s Supernova, which you yourself only just avoid!", ch, NULL, victim, TO_NOTVICT );
	damage ( ch, victim, number_range( 2000, 6000 ), gsn_supernovaicer );
	xSET_BIT( victim->affected_by, AFF_TIRED );
	xSET_BIT( victim->affected_by, AFF_CHAOS );
	xSET_BIT( victim->affected_by, AFF_TRAUMA );
	if ( victim->hit > 1000 )
	{
	    send_to_char( "What?  No!  You put EVERYTHING into that attack, but it hardly left a scratch!  Maybe you should run?!\n\r", ch );
	    xSET_BIT( ch->affected_by, AFF_SCARED );
	}
  }
  else
  {
	learn_from_failure( ch, gsn_supernovaicer );
	pager_printf( ch, "Your Supernova dissipates into thin air.\n\r" );
	act ( AT_GREEN, "$n's Supernova dissipates into thin air.\n\r", ch, NULL, NULL, TO_CANSEE );
  }
  return;  
}

void do_deathball( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 int modder = number_range( 1, 200 );

    if ( ( victim = who_fighting( ch ) ) == NULL )  
    { 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
    }    

    switch( ch->substate )
    {
	default:
    ch->skill_timer = 10;
    add_timer( ch, TIMER_DO_FUN, 10, do_deathball, 1 );
    pager_printf( ch, "&GYou raise your hand, pointing towards space, and a small ball of %s&G energy forms at the end of your finger, and begins to grow larger and larger.\n\r", ch->energycolour );
    pager_printf( victim, "&G%s raises %s hand, pointing towards space, and a small ball of %s&G energy forms at the end of %s finger, and begins to grow larger and larger.", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her",  ch->energycolour, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her");
    act( AT_SKILL, "&G$n raises one hand, charging a deathball...\n\r", ch, NULL, victim, TO_NOTVICT );
    ch->alloc_ptr = str_dup( arg );
    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your Deathball was interupted!\n\r", ch );
          sprintf( buf, "&G%s's Deathball was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's Deathball was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your Deathball.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s Deathball!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

        if ( modder >= 190 )
	{
	    if ( ch->race == RACE_ICER && ch->plmod >= 2800 && !IS_NPC(ch) && ch->pcdata->learned[gsn_supernovaicer] < 10 )
	    {
		ch->pcdata->learned[gsn_supernovaicer] = 10;
		send_to_char( "Is... Is this possible?  A move stronger than even a twenty-times Deathball?  This... This is the skill Cooler perfected... But how has it come to you?  Is it in your genes?  Are you related to the mighty Cooler?  Or... Are all of your kind born of the same stuff, capable of the same things?  Surely not... You're unique!  You are almighty!  Immortal!  Invulnerable!  There can be NO OTHER like you!  YOU MUST WIPE THEM ALL OUT!  AND ALL OF THE FILTHY SAIYANS!  YOU MUST WIPE THEM OUT - WITH SUPERNOVA!\n\r", ch );
		damage ( ch, victim, number_range( 8000, 12000 ), gsn_supernovaicer );
		pager_printf( ch, "You laugh with insane glee as your Supernova technique hammers %s with every status effect known to man!\n\r", victim->name );
		pager_printf( victim, "%s starts to laugh insanely, and soon it dawns on you that you've suddenly been hit with every status effect known to man...\n\r", ch->name );
		xSET_BIT( victim->affected_by, AFF_CHAOS  );
		xSET_BIT( victim->affected_by, AFF_TIRED  );
		xSET_BIT( victim->affected_by, AFF_TRAUMA );
		if ( victim->hit > 0 )
		{
		    ch->skill_timer = 3;
		    send_to_char( "&RWhat?!  You gave it everything and they're STILL ALIVE!?  HOW CAN THIS BE!?\n\r", ch );
		}
		return;
	    }
       	    learn_from_success( ch, gsn_deathball );
 	    pager_printf( ch, "&GYou laugh maniacally as you point your finger at your opponent sending your massive ball of energy plummeting toward %s.\n\r", ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
 	    pager_printf( victim, "&G%s laughs mainiacally, and launches a massive energy ball at you!\n\r", ch->name );
            act( AT_GREEN, "$n's launches $s Deathball, pounding $N into the ground!\n\r",  ch, NULL, victim, TO_NOTVICT );
	    pager_printf( ch, "&RYou've done it!  A super deathball with twenty-times the power!\n\r" );
            global_retcode = damage( ch, victim, number_range( 20000, 80000 ), gsn_deathball );
	}
        else
	{
            learn_from_success( ch, gsn_deathball );
            pager_printf( ch, "&GYou laugh maniacally as you point your finger at your opponent sending your massive ball of energy plummeting toward %s.\n\r", ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
            pager_printf( victim, "&G%s laughs mainiacally, and launches a massive energy ball at you!\n\r", ch->name );
            act( AT_GREEN, "$n's launches $s Deathball, pounding $N into the ground!\n\r",  ch, NULL, victim, TO_NOTVICT );
            global_retcode = damage( ch, victim, number_range( 1000, 4000 ), gsn_deathball );
	}
/*    }
    else
    {
        learn_from_failure( ch, gsn_deathball );
        pager_printf( ch, "&GYour Death Ball goes screaming past your opponent as you miss %s.\n\r",  ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
        pager_printf( victim, "&G%s's deathball goes screaming past you as you just dodge out of the way!", ch->name );
     	act( AT_GREEN, "$n's launches $s Deathball, but narrowly misses $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_deathball );
    }*/
return;
}

/*
 * Hellzone Grenade - Namek, Takes argument
 */

void do_hellzone( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 int size;
 int time;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 char arg1[MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL || IS_NPC(ch) )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

 argument = one_argument( argument, arg1 );
 if ( !is_number( arg1 ) )
    {
    if ( ch->pcdata->attacksize < 3 || ch->pcdata->attacksize > 100 )
        {
        send_to_char( "You need to specify the size of your attack ( 3 - 100 )\n\r", ch );
        return;
        }
    }

size = atoi( arg1 );
if ( size )
	ch->pcdata->attacksize = size;
if ( ( size > 100 || size < 3 )) 
{
if (ch->pcdata->attacksize < 3 || ch->pcdata->attacksize > 100)
    {
	send_to_char( "You must pick between 3 and 100 energyballs for hellzone.\n\r", ch );
	return;
    }
else
	size = ch->pcdata->attacksize;
}

if ( ( ch->mana < size * 25 ) / 2 )
    {
    pager_printf( ch, "You need %s more energy for this skill\n\r", format_pl( size * 25 ) );
	return;
	}	

time = ( ( size / 5 ) + 5 );

deduct_energy( ch, size * 25 );
switch( ch->substate )
    {
	default:

        ch->skill_timer = time;
        add_timer( ch, TIMER_DO_FUN, time, do_hellzone, 1 );
        pager_printf( ch, "&GYou explode with a ferocious aura of %s&G energy, and begin to charge a single ball of ki in your hand.  When it is big enough, you fire it to near %s... and begin to do the same thing another %d times!\n\r", ch->energycolour, PERS( victim, ch ), size - 1 );
        pager_printf( victim, "&G%s explodes with a ferocious aura of %s&G energy, and begins to charge a single ball of ki.  When it is big enough, %s fires it to near you... and begins to do the same thing another %d times!", ch->name, ch->energycolour, ch->name, size - 1 );        ch->alloc_ptr = str_dup( arg );
        act( AT_GREEN, "$n's hellzone was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your hellzone was interupted, and all the energyballs vanish!\n\r", ch );
          pager_printf(victim, "&G%s's hellzone was interrupted, and all the energyballs vanish!\n\r", ch->name );
          act( AT_GREEN, "$n's hellzone was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your hellzone, destroying all the energyballs.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s hellzone!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_hellzone ) )
    {
   	 learn_from_success( ch, gsn_hellzone );
         pager_printf( ch, "&GWith %d %s&G energyballs growing around %s every second, you suddenly scream '&RHELLZONE!&G' and clench your fist hard!  Suddenly, %s finds %sself bombarded by energyballs!\n\r", size, ch->energycolour, PERS( victim, ch ), PERS( victim, ch ), victim->sex == 0? "its" : ch->sex == 1 ? "him" : "her" );
         pager_printf( victim, "&GWith %d %s&G energyballs growing around you every second, %s suddenly screams '&RHELLZONE!&G' and clenches %s fist hard!  Suddenly, you finds yourself bombarded by energyballs!\n\r", size, ch->energycolour, ch->name, victim->sex == 0? "its" : ch->sex == 1 ? "his" : "her"  );
         act( AT_GREEN, "$n screams 'HELLZONE!', clenching $s fist!  $N finds $mself bombarded by energyballs!\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, number_range( size * 100, size * 200 ), gsn_hellzone );
    }
    else
    {
         learn_from_failure( ch, gsn_hellzone );
         pager_printf( ch, "You can't maintain the concentration, and all your energy balls implode in little puffs of green smoke!" );
         pager_printf( ch, "%s can't stay concentrated, and all the energy balls implode in puffs of green smoke!", ch->name );
         act( AT_GREEN, "$n loses concentration, and all $s energy balls disappear in green puffs of smoke!", ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, 0, gsn_hellzone );
   }
return;
}

/*
 * Instant transmission
 */

void do_instanttransmission( CHAR_DATA *ch, char *argument )
{
EXIT_DATA *exit; 
bool missed = FALSE; 
sh_int counter = number_range(0,9); 
int newroom= 0;  
    char arg[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA *location, *in_room;
    CHAR_DATA *fch;
//    CHAR_DATA *fch_next;
//    AREA_DATA *pArea;
//    int vnum;

    one_argument( argument, arg );
    one_argument( argument, arg2 );
    if ( arg[0] == 't' && arg[1] == 'r' && arg[2] == 'a' && arg[3] == 'n' && arg[4] == 's' && arg[5] == 'm' )
    sprintf( arg, "%s", arg2);	
	
    if ( ch->position <= POS_SLEEPING )
    {
    send_to_char( "Try waking up first\n\r", ch );
    return;
    }

    if ( arg[0] == '\0' )
    {
        send_to_char( "Instant Transmission to who?\n\r", ch );
        return;
    }

   if ( is_number(arg) )
   {
	send_to_char( "That doesn't sound much like somebody's name...\n\r", ch );
        return;
   }

    if ( (get_timer(ch, TIMER_RECENTFIGHT) > 0 ||  get_timer(ch, TIMER_PKER) > 150 || ch->bursttimer > 0 )
    &&  !IS_IMMORTAL(ch) )
    {
        send_to_char( "&RYour adrenaline is pumping too hard to focus and use Instant Transmission!\n\r", ch );
        return;
    }


   if ( (fch=(get_char_world(ch, arg))) )
   {
      if ( !IS_NPC(fch) && get_trust(ch) < get_trust(fch)
      &&   IS_SET(fch->pcdata->flags, PCFLAG_DND)
      && IS_IMMORTAL(fch) )
      {
         pager_printf( ch,
         "Sorry. %s is an immortal and does not wish to be disturbed.\n\r", fch->name);
         return;
      }
   }

    if ( !fch )
    {
	send_to_char( "You can't feel their powerlevel...\n\r", ch );
	return;
    }

    if ( fch == ch || fch->in_room->vnum == ch->in_room->vnum )
	{
	send_to_char( "But you are already there!\n\r", ch );
	return;
	}

    if ( ch->mana < 5000 )
    {
	send_to_char( "You need 5000 energy.\n\r", ch );
	return;
    }

    if ( IS_SET( fch->in_room->room_flags, ROOM_NO_ASTRAL )
      || IS_SET( ch->in_room->room_flags, ROOM_NO_ASTRAL )||
	fch->race == RACE_ANDROID)
    {
	send_to_char( "You can't feel their powerlevel...\n\r", ch );
	return;
    }
	

    if ( ( location = find_location( ch, arg ) ) == NULL || ( !IS_NPC(fch) && number_range( 0, 100 ) > fch->plmod ))
    {
	send_to_char( "You can't feel their powerlevel...\n\r", ch );
	return;
    }

    if ( room_is_private( location ) )
    {
        send_to_char( "That room is too full right now.\n\r", ch );
        return;
    }

    in_room = ch->in_room;
    if ( who_fighting(ch) != NULL )
    {
	send_to_char( "A little cowardly, wouldn't you say?\n\r", ch );
        return;
    }

    if ( location->vnum != 101 && xIS_SET(ch->act,PLR_MISSIONING) /*&& !str_cmp(location->area->planet,ch->in_room->area->planet) */)
    {
	send_to_char( "That'd be a bit cheap, wouldn't you say?\n\r", ch );
        return;

    }
for ( exit = location->first_exit; exit; exit = exit->next ) 
{ 
    if ( exit->vdir == counter ) 
    { 
	if ( IS_SET ( exit->to_room->room_flags, ROOM_NO_ASTRAL ) )
	    break;
        missed = TRUE; 
        newroom = exit->to_room->vnum; 
    } 
}

    WAIT_STATE( ch, 12 );
    if ( !can_use_skill( ch, number_percent(), gsn_instanttransmission ))
    {
	learn_from_failure( ch, gsn_instanttransmission );
	send_to_char( "You can't feel their powerlevel...\n\r", ch );
	return;
    }
    else
    {
        if ( missed && get_room_index( newroom ) ) 
        { 
            // Any messages you want here 
            location = get_room_index( newroom ); 
        }
        ch->mana -= 5000;
	learn_from_success( ch, gsn_instanttransmission );
    	act( AT_GREEN, "$n disappears a flash of light!\n\r", ch, NULL, NULL, TO_ROOM );
    	send_to_char( "&RYou put two fingers to your head and disappear, reappearing in a new place!\n\r", ch );

    if ( !IS_SET(ch->in_room->area->flags, AFLAG_LAWFUL) && IS_SET(location->area->flags, AFLAG_LAWFUL) )
    {
        send_to_char( "&RYou are now entering an area enforced by the Galactic Peacekeeping Forces.  Observe the law or face the consequences.\n\r", ch );
        pager_printf( ch, "Current terror level is %s: %s\n\r", terror_table[location->area->terror_level].name, terror_table[location->area->terror_level].description );
    }

    if ( IS_SET(ch->in_room->area->flags, AFLAG_LAWFUL) && !IS_SET(location->area->flags, AFLAG_LAWFUL) )
    {
        send_to_char( "&RYou are now leaving the jurisdiction of the Galactic Peacekeeping Forces.\n\r", ch );
    }

    	char_from_room( ch );
    	char_to_room( ch, location );
    	act( AT_GREEN, "$n appears in a flash of light!\n\r", ch, NULL, NULL, TO_ROOM );
    	do_look( ch, "auto" );
	mprog_greet_trigger( ch );
     	return;
    }
}

bool is_wearing( CHAR_DATA *ch, int vnum )
{
 OBJ_DATA *obj;

 for (obj=ch->first_carrying; obj; obj=obj->next_content)
 {
	if ( ch==obj->carried_by &&   obj->wear_loc > -1 &&   obj->pIndexData->vnum == vnum )
        return TRUE;
 }
return FALSE;
}

/*
 * Hell's Flash - requires specific item to be worn - no GSN
 */

void do_hellsflash( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ch->race != RACE_ANDROID )
	{
	send_to_char( "Huh?", ch );
	return;
	}

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_hellsflash, 1 );
        pager_printf( ch, "&GYou break your hands off and hold them under your arms.  Slowly, you begin to chargy your energy systems.\n\r" );
        act( AT_GREEN, "$n breaks $s hands off and holds them under $s arms.  Slowly, $n begins to charge $s energy systems.\n\r", ch, NULL, NULL, TO_ROOM );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "&GYour Hell's Flash was interupted!\n\r", ch );
          sprintf( buf, "&G%s's Hell's Flash was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's Hell's Flash was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your Hell's Flash.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s Hell's Flash!\n\r",  ch, NULL, victim, TO_NOTVICT );
	    ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill( ch, number_percent(), gsn_hellsflash ))
    {
    learn_from_success( ch, gsn_hellsflash );
    pager_printf( ch, "&GYou scream &R'HELL'S FLASH!!'&G and fill the area with burning %s energy, roasting %s!\n\r", ch->energycolour, PERS( victim, ch ) );
    pager_printf( victim, "&G%s screams &R'HELL'S FLASH!!'&G and fills the area with burning %s energy, roasting you!\n\r", ch->name, ch->energycolour );
    act( AT_GREEN, "&G$n screams '&RHELL's FLASH!!&G and begins to fill the area with burning hot energy, roasting $N!!'.\n\r", ch, NULL, victim, TO_NOTVICT );
    global_retcode = damage( ch, victim, number_range( 1750, 2000 ), gsn_hellsflash );
    victim->burning = 10;
    }
    else
    {
    learn_from_failure( ch, gsn_hellsflash );
    pager_printf( ch, "&GYou scream '&RHELL'S FLASH!!&G'... but nothing happens!\n\r", ch->energycolour, PERS( victim, ch ) );
    act( AT_GREEN, "&G$n screams '&RHELL's FLASH!!&G'... but nothing happens!\n\r", ch, NULL, NULL, TO_ROOM );
    }
return;
}

/*
 * Drone attack
 */

void do_droneattack( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg[MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_droneattack, 1 );
        pager_printf( ch, "&GA thousand little hatches open up all over your body, and tiny %s &Gdrones fly out and surround %s in a %s cloud.", ch->energycolour, PERS( victim, ch ), ch->energycolour );
        pager_printf( victim, "&GA thousand little hatches open up all over %s's body, and tiny %s&G drones fly out and surround you in a cloud...", ch->name, ch->energycolour );
        act( AT_GREEN, "A thousand hatches open up all over $n, and tiny little drones fly out, surrounding $N in a big cloud...", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your drone attack was interupted!\n\r", ch );
          sprintf( buf, "&G%s's drone attack was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's drone attack was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your drone attack.\n\r", ch );
    	act( AT_GREEN, "$n's cancels $s drone attack!\n\r",  ch, NULL, NULL, TO_ROOM );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill( ch, number_percent(), gsn_droneattack ) )
    {
 	learn_from_success( ch, gsn_droneattack );
         pager_printf( ch, "&GYou close the small hatches on your body and scream '&RDRONE ATTACK!&G'  Your tiny drones all ram %s, exploding with ferocious force!", PERS( victim, ch ));
         pager_printf( victim, "&G%s closes up all the drone hatches and screams '&RDRONE ATTACK!&G'  You are rammed by thousands of tiny drones, each exploding with ferocious force!", ch->name );
         act( AT_GREEN, "$n closes up all the hatches on $s body and screams '&RDRONE ATTACK!&G'  $N is rammed by thousands of tiny drones, each exploding with immense power!", ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, ( number_range( 800, 1000 ) ), gsn_droneattack );
    }
    else
    {
 	learn_from_failure( ch, gsn_droneattack );
         pager_printf( ch, "&GYou close the small hatches on your body and scream '&RDRONE ATTACK!&G' - but your little drones fail to respond and fly off!!" );
         pager_printf( victim, "&G%s closes up all the drone hatches and screams '&RDRONE ATTACK!&G' - but the little drones fail to respond and just fly off!", ch->name );
         act( AT_GREEN, "$n closes up all the hatches on $s body and screams '&RDRONE ATTACK!&G' - but all the drones fail to respond and just fly off!",ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, 0, gsn_droneattack );

    }
return;
}

/*
 * Self Destruct... better give that a big timer!
 */

void do_selfdestruct( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

argument = one_argument( argument, arg );

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You seem to have lost your victim...\n\r", ch );
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_selfdestruct, 1 );
        pager_printf( ch, "&GYou grab hold of %s tight and small panel in your chest slides backwards to reveal a timer with the number '15' on it.  Slowly, it begins to count down...", PERS( victim, ch ) );
        pager_printf( victim, "&G%s grabs hold of you tightly and a small panel on %s chest opens up, revealing a timer with the number '15' on it.  Slowly, it begins to count down...", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
        act( AT_GREEN, "$n grabs hold of $N tightly and a small panel on $s chest slides backwrds to reveal a timer with the number '15' on it.  Slowly, it begins to count down...", ch, NULL, victim, TO_NOTVICT );
        victim->canflee = FALSE;
        ch->detonating = TRUE;
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "SYSTEM MALFUNCTION:  SELF DESTRUCT CANCELED!!\n\r", ch );
          sprintf( buf, "&G%s's attack was interrupted!", ch->name );
          victim->canflee = TRUE;
          ch->detonating = FALSE;
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's attack was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop self destructing, and let go of your victim.\n\r", ch );
    	act( AT_GREEN, "$n's stops self destructing, and lets go of $s victim!\n\r",  ch, NULL, NULL, TO_ROOM );
        victim->canflee = TRUE;
 	    return;
    }

    ch->substate = SUB_NONE;
    pager_printf( ch, "&RYou begin to glow bright %s&R and heat up ferociously.  In a few seconds, you detonate, annihilating all life around you!", ch->energycolour );
    act( AT_BLOOD + AT_BLINK, "$n heats up with immense power!  In a few seconds, $s detonates, annihilating all life immediately near $m!!", ch, NULL, victim, TO_NOTVICT );
    send_to_char( "\n\r&YEvery atom in your body begins to explode at the speed of light.\n\r  Soon, all you can see is bright white, with the words &G'MISSION COMPLETE'&Y fading fast from view...\n\r", ch );
    ch->detonating = FALSE;
    pager_printf( victim, "&G%s glows bright %s&G and begins to heat up ferociously.  In a few seconds, %s detonates, annihilating all li--", ch->name, ch->energycolour, ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she");
    sprintf( buf, "I may not survive, %s, but I'm gonna take you with me... Aha.. Ahaha... AHAHAHAHAHAH-", PERS( victim, ch ) );
    do_chat( ch, buf );
    learn_from_success( ch, gsn_selfdestruct );
    victim->canflee = TRUE;
    if ( can_use_skill( ch, number_percent(), gsn_selfdestruct ) )
    {
    raw_kill( ch, victim );
    raw_kill( ch, ch );
    stop_fighting( ch, TRUE );
    }
    else
    {
	sprintf( buf, "I may not survive, but I'm gonna take you w- wait, what the hell?  System malfunction?  Blue screen of death?  WHAT THE FUCK?!" );
        raw_kill( victim, ch );
	do_chat(ch, buf );
    }
 return;
}

/*
 * Rocket Punch
 */

void do_rocketpunch( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg[MAX_INPUT_LENGTH];
// char arg2[MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 5;
        add_timer( ch, TIMER_DO_FUN, 5, do_rocketpunch, 1 );
        pager_printf( ch, "&GYou leap back from %s, and begin to unscrew your right hand, pointing your arm at %s", PERS( victim, ch ), ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her"  );
        pager_printf( victim, "&G%s leaps away from you, and begins to unscrew %s right hand, pointing at you all the time.", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
        act( AT_GREEN, "$n leaps away from $N and begins to unscrew $s right hand, all the while pointing at $N", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your rocket punch was interupted!\n\r", ch );
          sprintf( buf, "&G%s's rocket punch was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's rocket punch was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your rocket punch.\n\r", ch );
    	act( AT_GREEN, "$n's cancels $s rocket punch!\n\r",  ch, NULL, NULL, TO_ROOM );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill( ch, number_percent(), gsn_rocketpunch ) )
    {
	 learn_from_success( ch, gsn_rocketpunch );
         pager_printf( ch, "&GFour rockets in your right wrist kick into action, and your hand flies out, pummeling %s before returning to you.\n\r", PERS( victim, ch ));
         pager_printf( victim, "&GFour rockets in %s's right wrist kick into action, and %s hand flies out, pummeling you before returning to its owner...\n\r", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
         act( AT_GREEN, "Four rockets in $n's right wrist kick into action, and $s hand flies out, pummeling $N before returning to $m.", ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, ( number_range( 200, 400 ) ), gsn_rocketpunch	);
    }
    else
    {
	 learn_from_failure( ch, gsn_rocketpunch );
         pager_printf( ch, "&GThe motors under your wrist fail to activate, so you screw your hand back on.\n\r" );
         act( AT_GREEN, "The motors in $n's wrist fail to activate, so $e just screws $s hand back on!",ch, NULL, victim, TO_ROOM );
         global_retcode = damage( ch, victim, 0, gsn_rocketpunch );
    }
return;
}

/*
*   Galick cannon
*/

void do_galickcannon( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

	ch->skill_timer = 5;
        add_timer( ch, TIMER_DO_FUN, 5, do_galickcannon, 1 );

	sprintf( buf, "&GYou jet away from %s to gain some distance between each other.", PERS( victim, ch ) );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&GYou put both hands together behind you, and begin to charge your ki, forming a %s &Gball.", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
            sprintf( buf, "&G%s flies backwards, landing some distance away.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s begins to charge its ki, forming a %s &Gball next to its head.", ch->name, ch->energycolour );
        if ( ch->sex == 1 )
               sprintf( buf, "&G%s begins to charge his ki, forming a %s &Gball next to his head.", ch->name, ch->energycolour );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s begins to charge her ki, forming a %s &Gball next to her head.", ch->name, ch->energycolour );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n holds $s arms out behind $m and charges $s ki, with an evil smile pointed at $N.", ch, NULL, victim, TO_NOTVICT );

ch->alloc_ptr = str_dup( arg );
return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your gallick cannon was interupted!\n\r", ch );
          sprintf( buf, "&G%s's gallick cannon was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's gallick cannon was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your gallick cannon.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s gallick cannon!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_galickcannon ) )
    {
        learn_from_success( ch, gsn_galickcannon );
          sprintf( buf, "&GFinally, you put your hand forward emit a %s &Gki beam at %s.", ch->energycolour, PERS( victim, ch ) );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s fires a %s &Gki beam at you aimed straight at your chest!", ch->name, ch->energycolour );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n screams and fires a gallick cannon at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
global_retcode = damage( ch, victim, ( number_range( 120, 230 ) ), gsn_galickcannon );
    }
	else
    {
        learn_from_failure( ch, gsn_galickcannon );
	send_to_char( "Your galick cannon flies off the wrong direction...\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s launches a galick cannon wave at you, but it veers off to the side.", ch->name );
         else
                 sprintf(buf, "%s launches a galick cannon wave at you, but it veers too high.", ch->name );
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n flings a stream of ki at $N... in the wrong direction.\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, 0, gsn_galickcannon );
    }
return;
}

void do_sbc( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

 switch( ch->substate )
    {
        default:

        ch->skill_timer =  9;
        add_timer( ch, TIMER_DO_FUN, 9, do_sbc, 1 );
	sprintf( buf, "&GYou back off from %s to creating a gap inbetween you and %s.", PERS( victim, ch ), victim->sex == 0? "it" : victim->sex == 1? "him" : "her" );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&GYou hold two fingers on your right hand up and %s &Gsparks start to leap from them.", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
        act( AT_GREEN, "$n holds $s two fingers out and charges $s ki, $s head bowed in concentration.", ch, NULL, NULL, TO_CANSEE );

	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your special beam cannon was interupted!\n\r", ch );
          sprintf( buf, "&G%s's special beam cannon was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's special beam cannon was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your special beam cannon.\n\r", ch );
	    if ( ch->sex == 0 )
               sprintf( buf, "&G%s's cancels its attack.", ch->name );
            if ( ch->sex == 1 )
               sprintf( buf, "&G%s's cancels his attack.", ch->name );
            if ( ch->sex == 2 )
               sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
            act( AT_GREEN, "$n stops perfoming $s special beam cannon!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_sbc ) )
    {
        learn_from_success( ch, gsn_sbc );
        ch_printf( ch, "&GFinally, you point your fingers at %s and launch a spiralling %s &Gki beam at %s.", PERS( victim, ch ), ch->energycolour, HIMHER(victim->sex) );
        sprintf( buf, "&G%s fires a %s &Gki spiralling beam at you screaming through the air as it does so!", ch->name, ch->energycolour );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n points $s crackling fingers at $N and a narrow spiral of ki leaps form $s fingers at $N!\n\r",  ch, NULL, victim, TO_NOTVICT ); 
	global_retcode = damage( ch, victim, number_range( 3250, 4750 ), gsn_sbc );
//	if ( !IS_NPC(victim) && global_retcode == rVICT_DIED )
	if ( !IS_NPC(victim) &&  who_fighting(victim) == NULL && !IS_SET(ch->in_room->room_flags, ROOM_DUEL))
	{
 	  switch ( number_range( 1,7 ) )
          {
	    case 1:	ch->perm_str += 15;	send_to_char( "Your strength has increased by 15.\n\r", ch ); 	break;
	    case 2:	ch->perm_dex += 15;     send_to_char( "Your dexterity has increased by 15.\n\r", ch );   break;
	    case 3:	ch->perm_int += 15;     send_to_char( "Your intelligence has increased by 15.\n\r", ch );   break;
	    case 4:	ch->perm_con += 15;     send_to_char( "Your constitution has increased by 15.\n\r", ch );   break;
	    case 5:	ch->perm_cha += 15;     send_to_char( "Your charisma has increased by 15.\n\r", ch );   break;
	    case 6:	ch->perm_lck += 15;     send_to_char( "Your luck has increased by 15.\n\r", ch );   break;
	    case 7:	ch->perm_wis += 15;     send_to_char( "Your wisdom has increased by 15.\n\r", ch );   break;

          }
        }
    }
    else
    {
        learn_from_failure( ch, gsn_sbc );
        send_to_char( "Your special beam cannon fires but fails to hit...\n\r", ch);
        if ( number_range(1, 2) == 1 )
                sprintf(buf, "%s launches a special beam cannon at you, but it veers off to the side.", ch->name );
        else
                sprintf(buf, "%s launches a special beam cannon at you, but it veers too high.", ch->name );
        pager_printf( ch, "%s\n\r", buf );
        act( AT_GREEN, "$n flings a spiralling column of ki at $N... but $E manages to evade it.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_sbc );
    }
return;
}


void do_dodonpa( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

 switch( ch->substate )
    {
        default:

        ch->skill_timer =  3;
        add_timer( ch, TIMER_DO_FUN, 3, do_dodonpa, 1 );
        sprintf( buf, "&GYou pause your attacks for a short while and you begin" );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&Gto gather your energy, focusing it into your hand." ); 
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s skims backwards, stopping a fair distance away.", ch->name );
          pager_printf(victim, "%s\n\r", buf );
        if ( ch->sex == 0 )
           sprintf( buf, "&GYou begin to feel %s moving its ki into its hand.", ch->name );
        if ( ch->sex == 1 )
      sprintf( buf, "&GYou begin to feel %s moving his ki into his hand.", ch->name );
        if ( ch->sex == 2 )
       sprintf( buf, "&GYou begin to feel %s moving her ki into her hand.", ch->name );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to channel $s ki into $s hand", ch, NULL, victim, TO_NOTVICT );

        ch->alloc_ptr = str_dup( arg );
        return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your dodonpa was interupted!\n\r", ch );
          sprintf( buf, "&G%s's dodonpa was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's dodonpa was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your dodonpa.\n\r", ch );
            if ( ch->sex == 0 )
               sprintf( buf, "&G%s's cancels its attack.", ch->name );
            if ( ch->sex == 1 )
               sprintf( buf, "&G%s's cancels his attack.", ch->name );
            if ( ch->sex == 2 )
               sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
            act( AT_GREEN, "$n stops perfoming $s dodonpa.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_dodonpa ) )
    {
        learn_from_success( ch, gsn_dodonpa );
        sprintf( buf, "&GYou raise your hand in a pistol shape and fire a sharp bolt of ki from your finger at %s", PERS( victim, ch ) );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s raises %s hand in a pistol position and fires a %s&G bolt at you.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her", ch->energycolour );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n points an outstreched finger at $N and fires a small but dense ball of energy at $M",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, ( number_range( 80, 180 ) ), gsn_dodonpa );
    }
    else
    {

        learn_from_failure( ch, gsn_dodonpa );
        send_to_char( "Your dodonpa is evaded.\n\r", ch);
        if ( number_range(1, 2) == 1 )
                sprintf(buf, "%s fires a dodonpa bolt at you, but you deflect it.", ch->name );
        else
                sprintf(buf, "%s fires a dodonpa bolt at you, but it wasn't aimed straight.", ch->name );
        pager_printf( victim, "%s\n\r", buf );
        act( AT_GREEN, "$n fires a bolt of ki at $N from his finger... but $N manages to evade it.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_dodonpa );
    }
return;
}

void do_burningattack( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }
 if ( ch->plmod < 2000 && !IS_NPC(ch))
        {
        send_to_char( "Your body is too weak to take such a powerful skill", ch );
        return;
        }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  10;
        add_timer( ch, TIMER_DO_FUN, 10, do_burningattack, 1 );
	sprintf( buf, "&GYou fly away from %s and your aura explodes around you in a", PERS( victim, ch ) );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&Gspectacular display of gold and %s &Gas you gather in energy from around you.", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s flies away from you and begins to power up", ch->name );
        pager_printf(victim, "%s\n\r", buf );
        if ( ch->sex == 0 )
            sprintf( buf, "&Gin a dazzling display of %s &Gand golden flames as it gathers in energy.", ch->energycolour );
        if ( ch->sex == 1 )
	    sprintf( buf, "&Gin a dazzling display of %s &Gand golden flames as he gathers in energy.", ch->energycolour );       
	if ( ch->sex == 2 )
	    sprintf( buf, "&Gin a dazzling display of %s &Gand golden flames as she gathers in energy.", ch->energycolour );        
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to charge $s ki energy up and explodes in an immense aura of color. ", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your burning attack was interupted!\n\r", ch );
          sprintf( buf, "&G%s's burning attack was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's burning attack was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your burning attack.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s burning attack!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_burningattack ) )
    {
          learn_from_success( ch, gsn_burningattack );
          sprintf( buf, "&Gyou hold your hands out in front of you and perform some high speed arm movements before placing both palms forward and emitting a huge single blast from them heading towards %s.", PERS( victim, ch ) );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s waves %s hands around in a spectacular fashion before holding %s hands forward and emitting one huge %s &Gblast out towards you!", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her", ch->sex == 0? "its" : ch->sex == 1? "his" : "her", ch->energycolour );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n waves $s hands around in an outrageous fashion and then emits a huge ki ball at $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(1750, 3000) ), gsn_burningattack );
    }
    else
    {
        learn_from_failure( ch, gsn_burningattack );
send_to_char( "Your burning attack flies off the wrong direction...\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s waves %s arms around and fires a burning attack at you, but it was badly aimed.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her" );
         else
                 sprintf(buf, "%s waves %s arms around and fires a burning attack at you, but it veers too high.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf(victim, "%s\n\r", buf );
         act( AT_GREEN, "$n waves $s arms around and fires an enormous bolt of ki at $N... in the wrong direction.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_burningattack);
         global_retcode = damage( ch, victim, 0, gsn_burningattack );
    }
return;
}

void do_fingerbeam ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  4;
        add_timer( ch, TIMER_DO_FUN, 4, do_fingerbeam, 1 );
	sprintf( buf, "&GYou fly towards %s getting in close for a good distance to fire from.", PERS( victim, ch ) );        
	pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s flies at you and begins to draw ki into %s hand", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" :"her"  );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to charge towards $N channeling $s ki into $s finger.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your finger beam was interupted!\n\r", ch );
          sprintf( buf, "&G%s's finger beam was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's finger beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your finger beam.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s finger beam!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_fingerbeam ) )
    {
          learn_from_success( ch, gsn_fingerbeam );
          sprintf( buf, "&GYou casually raise your finger and emit a thin %s &Gbeam at %s at close range.", ch->energycolour, PERS( victim, ch ));
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s casually raises %s finger and emits a thin %s &Gbeam at you at close range.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" :"her" ,ch->energycolour );
	  pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n casually raises $s finger and emits a thin beam to $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(150, 200)), gsn_fingerbeam );
    }
    else
    {
        learn_from_failure( ch, gsn_fingerbeam );
	send_to_char( "Your finger beam whizzes past your target.\n\r", ch);
        if ( number_range(1, 2) == 1 )
                sprintf(buf, "%s raises a finger and fires a thin beam at you but it streaks past your shoulder.", ch->name );
        else
                sprintf(buf, "%s fires a beam off %s finger but you deflect it with your arm.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
        pager_printf( ch, "%s\n\r", buf );
        act( AT_GREEN, "$n fires a thin bolt of ki at $N... in the wrong direction.\n\r",  ch, NULL, victim, TO_NOTVICT );
        learn_from_failure( ch, gsn_fingerbeam);
        global_retcode = damage( ch, victim, 0, gsn_fingerbeam );
    }
return;
}


void do_multifingerbeam ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

	if ( ( victim = who_fighting( ch ) ) == NULL )
        {
           send_to_char( "You aren't fighting anyone.\n\r", ch );
           return;
        }

	if ( ch->plmod < 2400 )
	{
	   send_to_char( "You need to be in a stronger form.\n\r", ch );
	}

switch( ch->substate )
    {
        default:
	ch->skill_timer = number_range( 7, 9 );
        add_timer( ch, TIMER_DO_FUN, ch->skill_timer, do_multifingerbeam, 1 );
	sprintf( buf, "&GYou fly towards %s getting in close for a good distance to fire from.", PERS( victim, ch ) );        
	pager_printf(ch, "%s\n\r", buf );
        sprintf( buf, "&G%s flies at you and begins to draw ki into %s hand", ch->name, HISHER(ch->sex) );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to charge towards $N channeling $s ki into $s fingers.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1: 
          if ( !ch->alloc_ptr )
          {
          send_to_char( "Your finger beams were interupted!\n\r", ch );
          sprintf( buf, "&G%s's finger beams were interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's finger beams were interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your multi finger beam.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s finger beam!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
    {
          learn_from_success( ch, gsn_multifingerbeam );
          sprintf( buf, "&GYou raise your fingers and start hammering at the air, pounding long, %s &Gbeams of ki at %s!", ch->energycolour, PERS( victim, ch ));
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s raises %s fingers and starts to bombard you with %s &Gfingerbeams!", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" :"her" ,ch->energycolour );
	  pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n casually raises $s fingers and hammers $N with beams of ki!",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(100, 350)), gsn_multifingerbeam );
       	  global_retcode = damage( ch, victim, (number_range(150, 400)), gsn_multifingerbeam );
          global_retcode = damage( ch, victim, (number_range(200, 450)), gsn_multifingerbeam );
          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
	  {
	          global_retcode = damage( ch, victim, (number_range(250, 500)), gsn_multifingerbeam );
		  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
	  }
          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
        	  global_retcode = damage( ch, victim, (number_range(300, 550)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }

          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
	          global_retcode = damage( ch, victim, (number_range(350, 600)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }

          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
        	  global_retcode = damage( ch, victim, (number_range(400, 650)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }

          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
	          global_retcode = damage( ch, victim, (number_range(450, 700)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }

          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
        	  global_retcode = damage( ch, victim, (number_range(500, 750)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }

          if ( can_use_skill(ch, number_percent(),gsn_multifingerbeam ) )
          {
	          global_retcode = damage( ch, victim, (number_range(400, 1000)), gsn_multifingerbeam );
                  ch->mana -= 5000; if ( ch->mana < 0 ) ch->mana = 0;
          }
    }
    else
    {
        learn_from_failure( ch, gsn_multifingerbeam );
        learn_from_failure( ch, gsn_multifingerbeam);
	send_to_char( "Your finger beams whiz past your target.\n\r", ch);
        sprintf(buf, "%s raises a finger and fires a few thin beams of ki at you, but they streak past your shoulder.", ch->name );
        pager_printf( victim, "%s\n\r", buf );
        act( AT_GREEN, "$n fires a number of thin bolts of ki at $N... but they all miss.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, 0, gsn_multifingerbeam );
    }
return;
}

void do_eyebeam ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  3;
        add_timer( ch, TIMER_DO_FUN, 3, do_eyebeam, 1 );
	sprintf( buf, "&GYou fly towards  %s and begin to channel energy into your head.", PERS( victim, ch ) );
            pager_printf(ch, "%s\n\r", buf );
        if ( ch->sex == 0 )
           	sprintf( buf, "&G%s begins to draw ki into its head.", ch->name );
        if ( ch->sex == 1 )
		sprintf( buf, "&G%s begins to draw ki into his head.", ch->name );
	if ( ch->sex == 2 )
		sprintf( buf, "&G%s begins to draw ki into her head.", ch->name );
        pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to charge towards $N putting $s ki into $s head. ", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your eye beam was interupted!\n\r", ch );
          sprintf( buf, "&G%s's eye beam was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's eye beam was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case 	SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your eye beam.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s eye beam!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_eyebeam ) )
    {
          learn_from_success( ch, gsn_eyebeam );
          sprintf( buf, "&GYour eyes begin to glow %s &Gand you emit a beam out of each eye towards %s.", ch->energycolour, PERS( victim, ch ));
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s opens %s eye wide and fires a beam out of one.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her" );
	  pager_printf (victim, "%s\n\r", buf );
          act( AT_GREEN, "$n stares at $N and fires a two beams of ki out of $s eyes at $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(30, 60)), gsn_eyebeam );
          global_retcode = damage( ch, victim, (number_range(30, 60)), gsn_eyebeam );
    }
    else
    {
         learn_from_failure( ch, gsn_eyebeam );
	 send_to_char( "Your eye beam wizzes past your target.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s opens %s eyes and fires a beam from each one, but you duck underneath them.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her" );
         else
                 sprintf(buf, "%s fires two beams of ki from %s eyes but you leap out of the way.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires two thin bolts of ki at $N from $s eyes but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_eyebeam);
         global_retcode = damage( ch, victim, 0, gsn_eyebeam );
    }
return;
}

void do_staticfeedback ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf [MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
        {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
        }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  4;
        add_timer( ch, TIMER_DO_FUN, 4, do_staticfeedback, 1 );

	sprintf(buf, "&GYou get in close to %s and await an for the opportunity to come.", victim->name );
            pager_printf(ch, "%s\n\r", buf );
	 sprintf( buf, "&%s draws in closer to you and looks more concentrated.", ch->name );
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n draws towards $N and become more focused. ", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your static feedback was interupted!\n\r", ch );
          sprintf( buf, "&G%s's static feedback was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's static feedback was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your static feedback.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s static feedback!\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;

if ( can_use_skill(ch, number_percent(),gsn_staticfeedback ) )
    {
          learn_from_success( ch, gsn_staticfeedback );
          sprintf( buf, "&G%s lands a puch on you and you seize the opportunity to send an immense electrical spark down %s arm.", victim->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her" );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s sends a bolt of electricity down your arm as you punch %s.", ch->name, ch->sex == 0? "it" : ch->sex == 1? "him" :"her" );
pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$N punches $n but $e instead fires a bolt of electricity down $S arm.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, (number_range(200, 250)), gsn_staticfeedback );
    }
    else
    {
        learn_from_failure( ch, gsn_staticfeedback );
send_to_char( "You miss-time your electrical discharge.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s attempts to shock you as you throw your punch but you pull away in time.", ch->name);
         else
                 sprintf(buf, "%s discharges electricity at you but you manage to knock %s away in time.", ch->name, ch->sex == 0? "it" : ch->sex == 1? "him" : "her");
         pager_printf( ch, "%s\n\r", buf );
         act( AT_GREEN, "$n attempts to electrocute $N but fails to cause any damage.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_staticfeedback);
         global_retcode = damage( ch, victim, 0, gsn_staticfeedback );
    }
return;
}





 /*
 * MODEL SKILL CODE
 *
 * note - DO_FUN_NAME = something like do_fingerbeam, not DO_fun_fingerbeam or crap like that.
void DO_FUN_NAME( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

	deduct_energy( ch, ENERGY_REQUIREMENT );
	switch( ch->substate )
    {
	default:

        add_timer( ch, TIMER_DO_FUN, SKILL_LENGTH, DO_FUN_NAME, 1 );
	ch->skill_timer = SKILL_LENGTH;
*
* Using sprintf, pager_printf and ACT, put the message people see when the skill
* starts HERE
*
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your SKILL_NAME was interupted!\n\r", ch );
          sprintf( buf, "&G%s's SKILL_NAME was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's SKILL_NAME was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your SKILL_NAME.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s SKILL_NAME!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),GSN_NAME ) )
    {
   	learn_from_success( ch, GSN_NAME );
*
* Using sprintf, pager_printf and ACT, put the message people see when the skill
* timer has ended and the skill is a success HERE
*
        global_retcode = damage( ch, victim, number_range( MIN_DAM, MAX_DAM(refer to wisdom ALWAYS)  ), GSN_NAME );
    }
    else
    {
        learn_from_failure( ch, GSN_NAME );
*
* Using sprintf, pager_printf and ACT, put the message people see when the skill
* timer has ended and the skill is a failure HERE
*        global_retcode = damage( ch, victim, 0, GSN_NAME );
    }
return;
}

*/

void do_senzu( CHAR_DATA *ch, char *argument )
{
 return;
}

void do_ghosts( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 int size;
 int time;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];
 char arg1[MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL || IS_NPC(ch) )  
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

 if ( ch->plmod < 2400 )
 {
      send_to_char( "Your body is too weak to do that!\n\r", ch );
      return;
 }

 argument = one_argument( argument, arg1 );
 if ( !is_number( arg1 ) )
    {
    if ( ch->pcdata->attacksize < 3 || ch->pcdata->attacksize > 20 )
        {
        send_to_char( "You need to specify the size of your attack ( 3 - 20 )\n\r", ch );
        return;
        }
    }

size = atoi( arg1 );
if ( size )
	ch->pcdata->attacksize = size;
if ( ( size > 20 || size < 3 ))
{
if (ch->pcdata->attacksize < 3 || ch->pcdata->attacksize > 20)
    {
	send_to_char( "You must pick between 3 and 20 ghosts.\n\r", ch );
	return;
    }
else
	size = ch->pcdata->attacksize;
}

if ( ( ch->mana < size * 2500 ) / 2 )
    {
    pager_printf( ch, "You need %s more energy for this skill\n\r", format_pl( size * 2500 ) );
	return;
	}	

time = URANGE( 9, ( size * 0.75), 24 );

switch( ch->substate )
    {
	default:

        ch->skill_timer = time;
        add_timer( ch, TIMER_DO_FUN, time, do_ghosts, 1 );
        pager_printf( ch, "&GYou bulk up your chest, moving all your ki into your stomach, as you prepare your attack!  Soon, you begin to spit out some ghosts...");
        pager_printf( victim, "&G%s begins to move all %s ki to %s chest, bulking up quite a lot!  Soon, %s releases a number of ghosts!",ch->name, ch->sex == 2 ? "her" : "his", ch->sex == 2 ? "her" : "his", ch->sex == 2 ? "she" : "he" );
        ch->alloc_ptr = str_dup( arg );
        act( AT_GREEN, "$n bulks up and starts spitting out white ghosts!",  ch, NULL, victim, TO_NOTVICT );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	  {
          send_to_char( "Your attack was interupted, and all the ghosts vanish!\n\r", ch );
          pager_printf(victim, "&G%s's attack was interrupted, and all the ghosts vanish!\n\r", ch->name );
          act( AT_GREEN, "$n's attack was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your attack.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
	    pager_printf(victim, "%s\n\r", buf );
    	act( AT_GREEN, "$n's cancels $s attack!\n\r",  ch, NULL, victim, TO_NOTVICT );
 	    return;
    }

    ch->substate = SUB_NONE;
    deduct_energy( ch, size * 1250 );

    if ( can_use_skill(ch, number_percent(),gsn_ghosts ) )
    {
  	learn_from_success( ch, gsn_ghosts );
    pager_printf( ch, "&GYou finish releasing your white ghosts, and glare at %s.  Suddenly, you scream '&RSUPER GHOST KAMIKAZE ATTACK!&G', and they charge at %s, all exploding violently...\n\r", PERS( victim, ch ),PERS( victim, ch ) );
    pager_printf( victim, "&G%s finishes releasing all the white ghosts, and glares at you.  Suddenly, %s screams '&RSUPER GHOST KAMIKAZE ATTACK!&G', and they all charge at you, exploding violently!\n\r",PERS( ch, victim ), HESHE(ch->sex) );
    act( AT_GREEN, "$n screams 'SUPER GHOST KAMIKAZE ATTACK!' and launches a load of white ghosts at $N!",  ch, NULL, victim, TO_NOTVICT );
    global_retcode = damage( ch, victim, number_range( size * 100, size * 200 ), gsn_ghosts );
    }
    else
    {
   learn_from_failure( ch, gsn_ghosts );
   pager_printf( ch, "You can't maintain the concentration, and all your ghosts implode in little puffs of green smoke!\n\r" );
   pager_printf( victim, "%s can't stay concentrated, and all the ghosts implode in puffs of green smoke!\n\r", ch->name );
   act( AT_GREEN, "$n loses concentration, and all $s ghosts disappear in green puffs of smoke!", ch, NULL, victim, TO_NOTVICT );
   global_retcode = damage( ch, victim, 0, gsn_ghosts );
   }
return;
}

void do_mightinessbomber ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  8;
        add_timer( ch, TIMER_DO_FUN, 8, do_mightinessbomber, 1 );
	ch->target = victim;

	sprintf( buf, "&GYou puff out steam from your pores and begin to charge up." );
        pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s oozes steam and you can feel %s energy rising.", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her"  );
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n oozes steam from every pore and begins to charge up. ", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your mightiness bomber was interupted!\n\r", ch );
          sprintf( buf, "&G%s's mightiness bomber was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's mightiness bomber was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;


            send_to_char( "You stop your mightiness bomber.\n\r", ch );
        act( AT_GREEN, "$n's cancels $s mightiness bomber!\n\r",  ch, NULL, NULL, TO_CANSEE );
            return;
    }

    ch->substate = SUB_NONE;

if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's mightiness bomber flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your mightiness bomber flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

ch->target = NULL;
 if ( can_use_skill(ch, number_percent(),gsn_mightinessbomber) ) 
    {
          learn_from_success( ch, gsn_mightinessbomber);
          sprintf( buf, "&GYou extend both of your arms out infront of you and fire an immense ball of %s ki at %s.", ch->energycolour, victim->name );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s extends %s arms towards you and fires a %s spherical ball of ki at you.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her", ch->energycolour );
  	  pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n extends $s arms and emits a spherical ball of ki at $N.",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(1500, 2500) ), gsn_mightinessbomber );
    }
    else
    {
        learn_from_failure( ch, gsn_mightinessbomber );
	send_to_char( "You miss-time your mightiness bomber and it flies off elsewhere.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s fires a huge ball of ki at you but you manage to evade it.", ch->name);
         else
                 sprintf(buf, "%s fires %s ball of ki at you but misses entirely.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires a great ball of ki at $N but misses.",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_mightinessbomber);
         global_retcode = damage( ch, victim, 0, gsn_mightinessbomber );
    }
if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ));
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
return;
}

/*
void do_cloak( CHAR_DATA *ch, char *argument )
{
	int chance;

	if ( IS_NPC(ch) )
	return;

	if ( (IS_SET(ch->pcdata->flags, PCFLAG_NOTITLE))
	{
	send_to_char("You try but you are not able too.\n\r", ch);
	return;
	}

	if ( argument[0] == '\0' )
	{
	send_to_char("Syntax: Cloak (cloak, or clear)\n\r", ch);
	return;
	}

	if (!str_cmp(argument, "clear") )
	{
	STRFREE(ch->pcdata->cloak);
	ch->pcdata->cloak = STRALLOC( "" );
	send_to_char("Cloak has been removed.\n\r", ch);
	return;
	}

	chance = (int) (ch->pcdata->learned[gsn_cloak]);

	if ( number_percent( ) > chance )
	{
	send_to_char("You try to cloak yourself but fail.\n\r", ch);
	return;
	}

	if(strlen(argument) > 40)
	argument[40] = '\0';

	learn_from_success(ch, gsn_cloak);

	smash_tilde( argument );

	STRFREE( ch->pcdata->cloak );
	ch->pcdata->cloak = STRALLOC( argument );
	send_to_char("You wave your hands magically consealing your identity.\n\r", ch);
	}

*/

void do_spiritsurge ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  4;
        add_timer( ch, TIMER_DO_FUN, 4, do_spiritsurge, 1 );
	ch->target = victim;
	sprintf( buf, "&Gyou hold your arms together and begin to form a %s &Gball of spirit energy.", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s puts %s arms together and begins to form a small %s &Gball.", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour  );
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to form a small ball of spiritual energy. ", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

         case 1:
            if ( !ch->alloc_ptr )
          {
          send_to_char( "Your spirit surge was interupted!\n\r", ch );
          sprintf( buf, "&G%s's spirit surge was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's spirit surge was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
          }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's spirit surge flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your spirit surge flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

            send_to_char( "You stop your spirit surge.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s spirit surge.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's spirit surge flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your spirit surge flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

ch->target = NULL;
if ( can_use_skill(ch, number_percent(),gsn_spiritsurge) )
    {
          learn_from_success( ch, gsn_spiritsurge);
          sprintf( buf, "&GYou hold your ball of energy out and long %s strings of %s &Genergy snake their way towards %s.", ch->energycolour, ch->alignment >= 0? "holy" : "evil", victim->name );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&G%s extends %s sphere of %s energy at you and long strings of %s &Genergy snake their way towards you.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her", ch->alignment >= 0? "holy" : "evil", ch->energycolour);
	pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n extens $s ball of ki and long tendrils of energy snake their way towards $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(50, 300) ), gsn_spiritsurge );
    }
    else
    {
        learn_from_failure( ch, gsn_spiritsurge);
send_to_char( "You fail to control your spirit surge the energy flies off elsewhere.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "%s fires a set of long ki tendrils at you but you weave between them.", ch->name);
         else
                 sprintf(buf, "%s fires %s ball of ki at you but misses entirely.", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires a great ball of ki at $N but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_spiritsurge);
         global_retcode = damage( ch, victim, 0, gsn_spiritsurge);
    }
if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ) );
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }

return;
}



void do_finishbuster ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  8;
        add_timer( ch, TIMER_DO_FUN, 8, do_finishbuster, 1 );
	ch->target = victim;
	sprintf( buf, "&GYou hold your arms behind your head and start to form a sphere of %s ki.", ch->energycolour );
        pager_printf(ch, "%s\n\r", buf );
	sprintf( buf, "&G%s puts %s arms behind %s head and begins to form a %s &Gball.", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her",  ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour );
	pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n begins to form a large ball of energy behind $s head.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your finish buster was interupted!\n\r", ch );
          sprintf( buf, "&G%s's finish buster was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's finish buster was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
        DISPOSE( ch->alloc_ptr );
        ch->substate = SUB_NONE;
	if ( ch->in_room != victim->in_room )
	{
	    for ( attempt = 0; attempt < 8; attempt++ )
	    {
		door = number_door( );
		if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's finish buster flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your spirit finish buster %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

            send_to_char( "You stop your finish buster.\n\r", ch );
        if ( ch->sex == 0 )
           sprintf( buf, "&G%s's cancels its attack.", ch->name );
        if ( ch->sex == 1 )
           sprintf( buf, "&G%s's cancels his attack.", ch->name );
        if ( ch->sex == 2 )
           sprintf( buf, "&G%s's cancels her attack.", ch->name );
            pager_printf(victim, "%s\n\r", buf );
        act( AT_GREEN, "$n's cancels $s finish buster.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    ch->substate = SUB_NONE;
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's finish buster flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
      pager_printf( ch, "Your finish buster flies %s!\n\r", dir_name[pexit->vdir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
}}}
ch->target = NULL;
if ( can_use_skill(ch, number_percent(),gsn_finishbuster) )
    {
          learn_from_success( ch, gsn_finishbuster);
          sprintf( buf, "&R'Finish Buster!' &GYou swing your arms over your head and throw the ball of ki towards %s.",  victim->name );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&R'Finish Buster!' &G%s throws a sphere of %s energy at you!.", ch->name, ch->energycolour);
	  pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n throws $s ball of ki towards $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
        global_retcode = damage( ch, victim, (number_range(200, 500)), gsn_finishbuster );
    }
    else
    {
        learn_from_failure( ch, gsn_finishbuster);
send_to_char( "&R'Finish Buster!' &GYou fail to control your attack and it flies off elsewhere.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "&R'Finish Buster!'&G %s Throws a huge ball of ki at you, but misses", ch->name);
         else
                 sprintf(buf, "&R'Finish Buster!'&G %s fires %s ki at you, but you manage to evade it", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires a great ball of ki at $N but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_finishbuster);
         global_retcode = damage( ch, victim, 0, gsn_finishbuster);
    }
if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum ));
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }
return;
}

void do_ddd ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH], arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
     }

    switch( ch->substate )
    {
        default:
                ch->skill_timer = 13;
                add_timer( ch, TIMER_DO_FUN, 13, do_ddd, 1 );
                pager_printf( ch, "&GYou raise both arms and begin to form two giant %s&W disks of energy.", ch->energycolour );
                act( AT_GREEN, "$n brings both of $s arms up and begins to form two giant disks of energy.", ch, NULL, victim, TO_NOTVICT );
                ch->alloc_ptr = str_dup( arg );
                return;
         case 1:
                if ( !ch->alloc_ptr )
                {
                     send_to_char( "Your double destructo disk was interupted!\n\r", ch );
                     act( AT_GREEN, "$n's double destructo disk was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
                     bug( "do_search: alloc_ptr NULL", 0 );
                     return;
                }
                strcpy( arg, ch->alloc_ptr );
                DISPOSE( ch->alloc_ptr );
                break;

         case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
                send_to_char( "You stop your double destructo disk.\n\r", ch );
                act( AT_GREEN, "$n's cancels $s double destructo disk.\n\r",  ch, NULL, victim, TO_NOTVICT );
                return;
    }

    ch->substate = SUB_NONE;
         if ( can_use_skill(ch, number_percent(),gsn_ddd) )
         {
            learn_from_success( ch, gsn_ddd);
            sprintf( buf, "&RYou throw both your arms forward and launch your double destructo disk at %s!",  victim->name );
            pager_printf(ch, "%s\n\r", buf );
            sprintf( buf, "&R%s swings both arms forward and launches a pair of destructo disks at you!", ch->name );
            pager_printf(victim, "%s\n\r", buf );
            act( AT_GREEN, "$n throws a pair of destructo disks at $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
            global_retcode = damage( ch, victim, (number_range(800, 1250)), gsn_ddd );
            global_retcode = damage( ch, victim, (number_range(500, 1000)), gsn_ddd );
         }
         else
         {
            learn_from_failure( ch, gsn_ddd);
            send_to_char( "&R'Double Destructo Disk!' &GYou fail to control your attack and it flies off elsewhere.\n\r", ch);
            sprintf(buf, "&R'Double Destructo Disk!'&G %s fires %s ki at you, but you manage to evade it", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
            pager_printf( victim, "%s\n\r", buf );
            act( AT_GREEN, "$n fires a pair of ki disks at $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
            learn_from_failure( ch, gsn_ddd);
            global_retcode = damage( ch, victim, 0, gsn_ddd);
         }
    return;
}

void do_splitform( CHAR_DATA *ch, char * argument )
{
  int times = 0;

  if ( !IS_AFFECTED( ch, AFF_SPLITFORM ) && !IS_AFFECTED( ch, AFF_MULTIFORM ) && !IS_AFFECTED( ch, AFF_TRIFORM ) )
  {
    if ( !argument || !is_number( argument ) )
    {
          pager_printf( ch, "You need to specify between 2 and 4 splits to make.\n\r" );
          return;
    }
    times = atoi ( argument );
    if ( times > 4 || times < 2 )
    {
          pager_printf( ch, "Syntax: Splitform <2, 3 or 4>\n\r" );
          return;
    }
    if ( !can_use_skill ( ch, number_percent(), gsn_splitform ) )
    {
       learn_from_failure( ch, gsn_splitform );
       pager_printf( ch, "You try and separate into %d, but fail.\n\r", times );
       act( AT_GREEN, "$n tries and separates, but fails.\n\r", ch, NULL, NULL, TO_CANSEE );
       return;
    }
    if ( times == 2 )
    {
        xSET_BIT( ch->affected_by, AFF_SPLITFORM );
        act( AT_DANGER, "$n splits in two!", ch, NULL, NULL, TO_CANSEE );
    }
    if ( times == 3 )
    {
        xSET_BIT( ch->affected_by, AFF_TRIFORM );
        act( AT_DANGER, "$n splits in three!", ch, NULL, NULL, TO_CANSEE );        
    }
    if ( times == 4 )
    {
        xSET_BIT( ch->affected_by, AFF_MULTIFORM );
        act( AT_DANGER, "$n splits in four!", ch, NULL, NULL, TO_CANSEE );        
    }    
    pager_printf( ch, "You bring your hands to either side of your head and split in %d!\n\r", times );
    return;
  }
  pager_printf( ch, "You pull yourself back together and regain your lost stats." );
  act( AT_GREEN, "$n and $s splitforms draw back into one.  The freakshow has ended.", ch, NULL, NULL, TO_CANSEE );
  xREMOVE_BIT( ch->affected_by, AFF_SPLITFORM );
  xREMOVE_BIT( ch->affected_by, AFF_TRIFORM );    
  xREMOVE_BIT( ch->affected_by, AFF_MULTIFORM );
}


void do_timefreeze ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
     }

    switch( ch->substate )
    {
        default:
                ch->skill_timer = 7;
                add_timer( ch, TIMER_DO_FUN, 7, do_timefreeze, 1 );
		victim->skill_timer = 5;
		WAIT_STATE( victim, 5 * PULSE_PER_SECOND );
                pager_printf( ch, "&GYou breathe in, and time around you slows to an incredibly slow pace.  You then begin to form three small energy balls in each hand.\n\r" );
                act( AT_GREEN, "$n breathes in, and seems to be moving /VERY/ quickly...", ch, NULL, NULL, TO_CANSEE );
                ch->alloc_ptr = str_dup( arg );
                return;
         case 1:
                if ( !ch->alloc_ptr )
                {
                     send_to_char( "Your Time Freeze was interupted!\n\r", ch );
                     act( AT_GREEN, "$n's Time Freeze was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
		     victim->skill_timer = 5;
		     WAIT_STATE( victim, -5 * PULSE_PER_SECOND );
                     bug( "do_search: alloc_ptr NULL", 0 );
                     return;
                }
                strcpy( arg, ch->alloc_ptr );
                DISPOSE( ch->alloc_ptr );
                break;

         case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
                send_to_char( "You stop your Time Freeze.\n\r", ch );
		victim->skill_timer = 0;
		victim->wait = 0;
                act( AT_GREEN, "$n's cancels $s Time Freeze.\n\r",  ch, NULL, victim, TO_NOTVICT );
                return;
    }

    ch->substate = SUB_NONE;
         if ( can_use_skill(ch, number_percent(),gsn_timefreeze) )
         {
            learn_from_success( ch, gsn_timefreeze);
            pager_printf(ch, "&GYou throw your energy balls at %s with all your strength, and breathe out just in time for %s to see them explode in %s face!\n\r", PERS( victim, ch ), victim->sex == 2 ? "her" : "him", victim->sex == 2 ? "her" : "his" );
            pager_printf(victim, "Suddenly time catches up with you, and a bunch of energy balls explode in your face!\n\r" );
            act( AT_GREEN, "A barrage of energy balls thrown by $n explode in $N's face.\n\r",  ch, NULL, victim, TO_NOTVICT );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
            global_retcode = damage( ch, victim, (number_range(50, 300)), gsn_timefreeze );
         if ( !IS_NPC(ch) && ch->race == RACE_MUTANT && IS_NPC(victim) && victim->pIndexData->vnum == 5802 && ch->pcdata->learned[gsn_stasis] < 10 )
         {
                ch->pcdata->learned[gsn_stasis] = 10;
                pager_printf( ch, "&R...Guldo was an idiot!  Why didn't he think of doing this?\n\r" );
                pager_printf( ch, "&RYou have learned 'Stasis'!\n\r" );
         }

         }
         else
         {
            learn_from_failure( ch, gsn_timefreeze);
            pager_printf( ch, "&GYou breathe out too soon, and your energy balls fizzle out as %s resumes normal movement.\n\r", PERS(victim,ch));
            pager_printf( victim, "&GSuddenly time catches up with you, and %s is left standing there like a moron.\n\r", PERS(ch, victim) );
            act( AT_GREEN, "$n breathes out, and is suddenly left open to attack from $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
            learn_from_failure( ch, gsn_timefreeze);
            global_retcode = damage( ch, victim, 0, gsn_timefreeze);
         }
    return;
}

/* Sort of a meditate skill  -Zeno */
void do_charge( CHAR_DATA *ch, char *argument )
{
/*
  int minrange;
  int maxrange;
  int restoreamt;
  char buf[MAX_STRING_LENGTH];
*/

    if ( IS_AFFECTED( ch, AFF_CHARGING ) )
    {
	xREMOVE_BIT( ch->affected_by, AFF_CHARGING );
	send_to_char( "You stop charging.\n\r", ch );
	return;
    }
 if ( IS_AFFECTED( ch, AFF_INVULNERABILITYSHIELD ) )
 {	
	ch_printf( ch, "You cannot charge wihile invulnerable\n\r");
	return;
 }
    if ( IS_AFFECTED( ch, AFF_TIRED ) )
    {
	send_to_char( "You cannot charge whilst tired.\n\r", ch );
	return;
    }
    if (  IS_SET( ch->in_room->room_flags, ROOM_HOSPITAL ) )
    {
	send_to_char( "This skill is for use 'in the field', as it were.\n\r", ch );
	return;
    }
    if ( ch->mana == ch->max_mana )
    {
      send_to_char( "You are already at max energy.\n\r", ch );
      return;
    }


    if (!can_use_skill( ch, number_percent(), gsn_charge ) )
    {
	send_to_char( "You fail to charge your energy.\n\r", ch );
	learn_from_failure( ch, gsn_charge );
	return;
    }

    xSET_BIT( ch->affected_by, AFF_CHARGING );
    send_to_char( "You explode in a shower of ki and start charging your energy!\n\r", ch );
    act( AT_BLUE, "&B$n's aura &Bflares around $m, as $e charges up power.", ch, NULL, NULL, TO_NOTVICT );
    learn_from_success( ch, gsn_charge );
    return;

/*    minrange = (ch->max_mana * 0.025 );
    maxrange = (ch->max_mana * 0.075 );

    restoreamt = number_range( minrange, maxrange );

    if ( restoreamt < 1 )
	restoreamt = 1;

    sprintf( buf, "&BYour %s &Baura flares up as you charge up your power.\n\r", ch->energycolour );
    pager_printf( ch, "%s", buf );
   
    act( AT_BLUE, "&B$n's aura &Bflares around $m, as $e charges up power.", ch, NULL, NULL, TO_NOTVICT );
    learn_from_success( ch, gsn_charge );
    WAIT_STATE( ch, skill_table[gsn_charge]->beats );

    ch->mana += restoreamt;
    if ( ch->mana > ch->max_mana )
	ch->mana = ch->max_mana;*/
}


void do_vengeancebuster ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char buf[MAX_STRING_LENGTH];
 char arg [MAX_INPUT_LENGTH];

int vnum = 0, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

switch( ch->substate )
    {
        default:

	ch->skill_timer =  4;
        add_timer( ch, TIMER_DO_FUN, 4, do_vengeancebuster, 1 );
	ch->target = victim;
	sprintf( buf, "&GYou hold your arms behind your head and start to form a sphere of %s ki.\n\r", ch->energycolour );
	send_to_char( buf, ch );
	sprintf( buf, "&G%s puts %s arms behind %s head and begins to form a %s &Gball.\n\r", ch->name, ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her",  ch->sex == 0? "its" : ch->sex == 1 ? "his" : "her", ch->energycolour );
	send_to_char( buf, victim );
        act( AT_GREEN, "$n begins to form a large ball of energy behind $s head.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;
        case 1:
            if ( !ch->alloc_ptr )
            { 
                  bug( "do_vengeancebuster: alloc_ptr NULL", 0 );
                  return;
            }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;
        case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
	    if ( ch->in_room != victim->in_room )
   	    {
		for ( attempt = 0; attempt < 8; attempt++ )
		{
		   door = number_door( );
		   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
		      continue;
		   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
		   {
		      pager_printf( victim, "&P%s's vengeance buster flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
		      pager_printf( ch, "Your vengeance buster %s!\n\r", dir_name[pexit->vdir] );
		      vnum = victim->in_room->vnum;
		      char_from_room( victim );
		      char_to_room( victim, ch->in_room );
		      moved = TRUE;
		   }
		}
	    }

            send_to_char( "You stop your vengeance buster.\n\r", ch );
            act( AT_GREEN, "$n's cancels $s vengeance buster.\n\r",  ch, NULL, NULL, TO_CANSEE );
            return;
    }
    ch->substate = SUB_NONE;
    if ( ch->in_room != victim->in_room )
    {
        for ( attempt = 0; attempt < 8; attempt++ )
	{
	   door = number_door( );
	   if ( ( pexit = get_exit( ch->in_room, door ) ) == NULL || !pexit->to_room )
	      continue;
	   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
	   {
	      pager_printf( victim, "&P%s's vengeance buster flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->vdir] );
	      pager_printf( ch, "Your vengeance buster flies %s!\n\r", dir_name[pexit->vdir] );
	      vnum = victim->in_room->vnum;
	      char_from_room( victim );
	      char_to_room( victim, ch->in_room );
	      moved = TRUE;
	   }
	}
    }
    ch->target = NULL;
    if ( can_use_skill(ch, number_percent(),gsn_vengeancebuster) )
    {
          learn_from_success( ch, gsn_vengeancebuster);
          sprintf( buf, "&R'Vengeance Buster!' &GYou swing your arms over your head and throw the ball of ki towards %s.",  victim->name );
          pager_printf(ch, "%s\n\r", buf );
          sprintf( buf, "&R'Vengeance Buster!' &G%s throws a sphere of %s energy at you!", ch->name, ch->energycolour);
	  pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n throws $s ball of ki towards $N.\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, (number_range(400, 600)), gsn_vengeancebuster);
    }
    else
    {
        learn_from_failure( ch, gsn_vengeancebuster);
	send_to_char( "&R'Vengeance Buster!' &GYou fail to control your attack and it flies off elsewhere.\n\r", ch);
         if ( number_range(1, 2) == 1 )
                 sprintf(buf, "&R'Vengeance Buster!'&G %s Throws a huge ball of ki at you, but misses", ch->name);
         else
                 sprintf(buf, "&R'Vengeance Buster!'&G %s fires %s ki at you, but you manage to evade it", ch->name, ch->sex == 0? "its" : ch->sex == 1? "his" : "her");
         pager_printf( victim, "%s\n\r", buf );
         act( AT_GREEN, "$n fires a great ball of ki at $N but misses.\n\r",  ch, NULL, victim, TO_NOTVICT );
         learn_from_failure( ch, gsn_vengeancebuster);
         global_retcode = damage( ch, victim, 0, gsn_vengeancebuster);
    }
    if ( moved == TRUE )
    {
	 char_from_room( victim );
	 char_to_room( victim, get_room_index( vnum ));
	 act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
    }
return;
}

void ApplyEffect( CHAR_DATA *ch )
{
    float nrg = ch->energy;
    if ( IS_SET( ch->pcdata->flags, PCFLAG_MANIAC ) )
	nrg /= 2;
    if ( number_range( 101, 150 ) <= nrg )
    {
	if ( nrg >= 150 + number_range(-10,10)  )
	{
	    send_to_char( "Whoops.\n\r&RYou explode in a shower of guts and gibs.\n\r", ch );
	    raw_kill( ch, ch );
	}
	else if ( nrg > 130 + number_range(-5,5) && !IS_AFFECTED( ch, AFF_CHAOS ) )
	{
	    send_to_char( "ARGH!  THE MENTAL STRAIN IS TOO MUCH!  YOU'VE GONE NUTS!!\n\r", ch );
	    xSET_BIT( ch->affected_by, AFF_CHAOS );
	}
	else if ( ch->race != RACE_ANDROID && nrg > 115 + number_range(-2,2) && !IS_AFFECTED( ch, AFF_TIRED ) && ch->style != STYLE_NARCOPHOBIC )
	{
	    send_to_char( "Damn... Fighting like this sure is tiring...\n\r", ch );
	    xSET_BIT( ch->affected_by, AFF_TIRED );
	}
    }
    return;
}


void do_bbk( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );
 argument = one_argument( argument, arg2 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
     send_to_char( "You aren't fighting anyone.\n\r", ch );
     return;
 }

 if ( ch->plmod < 2800 && !IS_NPC(ch) )
 {
     send_to_char( "You need to be in your final form for Big Bang Kamehameha to work.", ch );
     return;
 }

 switch( ch->substate )
 {
      default:
		ch->skill_timer = 12;
		add_timer( ch, TIMER_DO_FUN, 12, do_bbk, 1 );
        	pager_printf( ch, "&GYou thrust both hands in front of you, hovering upwards slightly, and begin to form a whirling sphere of %s &Genergy\n\r", ch->energycolour );
	        pager_printf( victim, "&G%s grins wickedly, hovering upwards. %s thrusts %s hands forward, forming a massive %s &Gsphere while looking you dead in the eyes.\n\r", ch->name, HESHE(ch->sex), HISHER(ch->sex), ch->energycolour );
		act( AT_SKILL, "$n grins wickedly and stares at $N, $s feet leaving the ground as $e begins to form a massive sphere of ki before $mself.", ch, NULL, victim, TO_NOTVICT );
	        ch->alloc_ptr = str_dup( arg );
		return;
		
      case 1:
      		if ( !ch->alloc_ptr )
      		{
      		    send_to_char( "&GYour Big Bang Kamehameha was interupted!\n\r", ch );
          	    act( AT_GREEN, "$n's Big Bang Kamehameha was interrupted!\n\r",  ch, NULL, NULL, TO_CANSEE );
          	    bug( "do_search: alloc_ptr NULL", 0 );
          	    return;
          	}
          	strcpy( arg, ch->alloc_ptr );
          	DISPOSE( ch->alloc_ptr );
          	break;
          	
      case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
	        send_to_char( "You cancel your Big Bang Kamehameha", ch );
	        act( AT_GREEN, "$n's cancels $s Big Bang Kamehameha.\n\r",  ch, NULL, NULL, TO_CANSEE );
	        ch->skill_timer = 0;
		return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill( ch, number_percent(), gsn_bbk ) )
    {
	learn_from_success( ch, gsn_bbk );
	pager_printf( ch, "&GYou thrust your hands forward, causing the massive sphere to form and ooze lazily forward, sending a massive river of %s &Gki at %s.  Screaming 'BIG BANG KAMEHAMEHA!!', you watch %s vanish in the %s&G light!\n\r", ch->energycolour, victim->name, HIMHER(victim->sex), ch->energycolour  );
	pager_printf( victim, "&G%s pushes the sphere forward, washing you in a massive stream of %s&G.  The only thing you can hear above the rush of energy is 'BIG BANG KAMEHAMEHA!!'", ch->name, ch->energycolour );
	act( AT_GREEN, "$n unleashes $s ultimate attack, washing $N away in a blast of bright light!", ch, NULL, victim, TO_NOTVICT );
	global_retcode = damage( ch, victim, ( number_range( 500, 2500 ) ), gsn_bbk );
	global_retcode = damage( ch, victim, ( number_range( 500, 2500 ) ), gsn_bbk );
	global_retcode = damage( ch, victim, ( number_range( 500, 2500 ) ), gsn_bbk );
	global_retcode = damage( ch, victim, ( number_range( 500, 2500 ) ), gsn_bbk );
	victim->skill_timer = 8;
	WAIT_STATE( victim, 8 );
	if ( victim->hit > 1000 )
	{
	    send_to_char( "But... You put everything into that move, and it didn't work?  Time to run!\n\r", ch );
	    xSET_BIT( ch->affected_by, AFF_SCARED );
	}
    }
    else
    {
	learn_from_failure( ch, gsn_bbk );
	pager_printf( ch, "You thrust your hands outwards...and the sphere evaporates into a shower of confettii. You'll -never- live this down if someone finds out.." );
	pager_printf( victim, "%s thrusts %s hands out at you, %s arrogant grin evaporating as the ki turns into a colourful cloud of confettii. To think you were almost scared...", ch->name, HISHER(ch->sex), HISHER(ch->sex) );
	act( AT_SKILL, "$n thrusts $s hands towards $N, screaming 'BIG BA....', $s cocksure voice dying away when $e realizes $e just shot streamers and confettii at $N. What an ass.", ch, NULL, victim, TO_NOTVICT );
    }
return;
}


void do_parent_child_kamehameha( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );
 argument = one_argument( argument, arg2 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
     send_to_char( "You aren't fighting anyone.\n\r", ch );
     return;
 }

 if ( ch->plmod < 3500)
 {
     send_to_char( "You need to be in mystic form for this to work.", ch );
     return;
 }

 switch( ch->substate )
 {
      default:
		ch->skill_timer = 15;
		add_timer( ch, TIMER_DO_FUN, 15, do_parent_child_kamehameha, 1 );
        	pager_printf( ch, "&GYou feel the presence of %s%s, your %s, behind you, guiding your attack somehow.\n\r", ch->name, ch->sex == 2 ? "chi" : "oku", ch->sex == 2 ? "mother" : "father" );
	        pager_printf( victim, "&G%s takes a step backwards, cupping %s hands, a strange calm look over coming %s.\n\r", ch->name, HISHER(ch->sex), HIMHER(ch->sex) );
		act( AT_SKILL, "&G$n looks like $e's not all that worried about $N as $e take the Kamehameha stance.", ch, NULL, victim, TO_NOTVICT );
	        ch->alloc_ptr = str_dup( arg );
		return;
		
      case 1:
      		if ( !ch->alloc_ptr )
      		{
      		    send_to_char( "&GYour Parent-Child Kamehameha was interupted!\n\r", ch );
          	    act( AT_GREEN, "$n's Parent-Child Kamehameha was interrupted!\n\r",  ch, NULL, NULL, TO_CANSEE );
          	    bug( "do_search: alloc_ptr NULL", 0 );
          	    return;
          	}
          	strcpy( arg, ch->alloc_ptr );
          	DISPOSE( ch->alloc_ptr );
          	break;
          	
      case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
	        send_to_char( "You cancel your Parent-Child Kamehameha", ch );
	        act( AT_GREEN, "$n's cancels $s Parent-Child Kamehameha.\n\r",  ch, NULL, NULL, TO_CANSEE );
	        ch->skill_timer = 0;
		return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill( ch, number_percent(), gsn_parent_child_kamehameha ) )
    {
	learn_from_success( ch, gsn_parent_child_kamehameha );
	pager_printf( ch, "&G%s%s's spirit appears beside you, and as parent and child you unleash a double kamehameha attack, sending %s awash in a wave of %s &Gki!\n\r", ch->name, ch->sex == 2 ? "chi" : "oku", victim->name, ch->energycolour );
	pager_printf( victim, "&G%s unleashes %s kamehameha attack, which you quickly brace for. &C'This is too easy'&G, you think to yourself, before you see an older %s that looks alot like %s appear beside them, the two of them blasting you off to %s &Gcolored oblivion as one!\n\r", ch->name, HISHER(ch->sex), ch->sex == 2 ? "woman" : "man", HIMHER(ch->sex), ch->energycolour );
	act( AT_GREEN, "Someone that looks like an older version of $n appears behind $m, the two unleasing a ludicrously huge kamehameha attack!", ch, NULL, victim, TO_NOTVICT );
	global_retcode = damage( ch, victim, ( number_range( 7000, 14000 ) ), gsn_parent_child_kamehameha );
	ch->hit = URANGE( 1, ch->hit + 12000, ch->max_hit );

    }
    else
    {
	learn_from_failure( ch, gsn_parent_child_kamehameha );
	pager_printf( ch, "&GYou feel a presence guiding you:  Your eyes dart to to the side to see... Green. Cell happily smashes your jaw with a lightning fast jab before vanishing.  How pathetic... &R[8000]\n\r" );
	ch->hit -= 8000;
	pager_printf( victim, "&G%s doesn't notice as the meancing form of Perfect Cell appears behind %s. The look of helplessness on %s face as he drives his fist into %s jaw will be a fond memory.\n\r", ch->name, HIMHER(ch->sex), HISHER(ch->sex), HISHER(ch->sex) );
	act( AT_SKILL, "$n doesn't seem to notice that Cell just instant transmissioned behind $m, but they do manage to catch a glimpse before he sends $m to $s knees in pain, vanishing again.", ch, NULL, victim, TO_NOTVICT );
    }
return;
}



void do_obliterator_cannon( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];
 ROOM_INDEX_DATA *pRoomIndex = NULL;
 int ctr;

 argument = one_argument( argument, arg );
 argument = one_argument( argument, arg2 );

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
     send_to_char( "You aren't fighting anyone.\n\r", ch );
     return;
 }

 if ( ch->plmod < 2900)
 {
     send_to_char( "You need to be in your strongest form first.", ch );
     return;
 }

 switch( ch->substate )
 {
      default:
		ch->skill_timer = 10;
		add_timer( ch, TIMER_DO_FUN, 10, do_obliterator_cannon, 1 );
        	pager_printf( ch, "&GYou float up into the air on the spot, curling up into a ball as you channel your innate magical abilities...\n\r" );
	        pager_printf( victim, "&G%s floats backwards and up into the air, curling up into a ball as a faint aura glows around %s.\n\r", ch->name, HIMHER(ch->sex) );
		act( AT_SKILL, "&G$n floats up slowly into the air, curling into a ball as a faint aura starts to glow around $m.", ch, NULL, victim, TO_NOTVICT );
	        ch->alloc_ptr = str_dup( arg );
		return;
		
      case 1:
      		if ( !ch->alloc_ptr )
      		{
      		    send_to_char( "&GYour obliterator cannon was interupted!\n\r", ch );
          	    act( AT_GREEN, "$n's obliterator cannon was interrupted!\n\r",  ch, NULL, NULL, TO_CANSEE );
          	    bug( "do_search: alloc_ptr NULL", 0 );
          	    return;
          	}
          	strcpy( arg, ch->alloc_ptr );
          	DISPOSE( ch->alloc_ptr );
          	break;
          	
      case SUB_TIMER_DO_ABORT:
                DISPOSE( ch->alloc_ptr );
                ch->substate = SUB_NONE;
	        send_to_char( "You cancel your obliterator cannon.\n\r", ch );
	        act( AT_GREEN, "$n's cancels $s obliterator cannon.",  ch, NULL, NULL, TO_CANSEE );
	        ch->skill_timer = 0;
		return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill( ch, number_percent(), gsn_obliterator_cannon ) )
    {
	learn_from_success( ch, gsn_obliterator_cannon );
	pager_printf( ch, "&PYour eyes snap open suddenly, now completely purple in colour, as you quickly snap out your body to full height before thrusting all limbs forward, firing a huge %s &Pbolt of magic at %s!\n\r", ch->energycolour, PERS( victim, ch ));
	pager_printf( victim, "&P%s's eyes snap open suddenly, now completely purple in colour, as %s quickly snaps out %s body before thrusting all limbs forward, firing a huge %s &Pbolt of magic at you!\n\r", PERS(ch,victim), HESHE(ch->sex), HISHER(ch->sex), ch->energycolour );
	act( AT_GREEN, "$n quickly snaps out his entire body before firing a huge blast of magic at $N!", ch, NULL, victim, TO_NOTVICT );
	switch ( number_range( 0, 7 ) )
	{
	    case 0:
		victim->mana *= 0.5;
		ch_printf( ch, "You hammer %s's energy levels!\n\r", PERS(victim,ch) );
		ch_printf( victim, "Your energy levels take a huge beating!\n\r" );
		break;
	    case 1:
		interpret( victim, "stagger self" );
		ch_printf( ch, "You manage to make %s stagger back in shock!\n\r", PERS(victim,ch) );
		break;
	    case 2:
		if ( victim->skill_timer < 4 ) victim->skill_timer = 4;
		WAIT_STATE( victim, 16 );
		ch_printf( ch, "%s doesn't even notice being hit at first - you've created a time vortex around %s!\n\r", PERS(victim,ch), HIMHER(victim->sex) );
		ch_printf( victim, "You're suddenly unable to react to anything around you...\n\r" );
		break;
	    case 3:
		for ( ctr = 0; ctr < 5000; ctr++ )
		{
		    pRoomIndex = get_room_index( number_range( 0, 50000 ) );
		    if ( pRoomIndex )
		       if ( !IS_SET(pRoomIndex->room_flags, ROOM_PRIVATE)
		       &&   !IS_SET(pRoomIndex->room_flags, ROOM_SOLITARY)
		       &&   !IS_SET(pRoomIndex->room_flags, ROOM_NO_ASTRAL)
		       &&   !IS_SET(pRoomIndex->room_flags, ROOM_PROTOTYPE) )
		           break; 	
		}
		if ( pRoomIndex )
		{
		    char_from_room(victim);
		    char_to_room( victim, pRoomIndex );
		    ch_printf( ch, "%s disappears in a puff of smoke!\n\r", PERS(victim,ch) );
		    ch_printf( victim, "What the... Where the hell are you?\n\r" );
		    break;
		}
	    case 4:
		xSET_BIT(victim->affected_by, AFF_TIRED );
		ch_printf( ch, "%s looks exhausted suddenly!\n\r", PERS(victim,ch) );
		ch_printf( victim, "Fighting sure is tiring business...  Is it nap time yet?\n\r" );
		break;
	    case 5:
	    case 6:
	    case 7:
		global_retcode = damage( ch, victim, ( number_range( 4000, 8000 ) ), gsn_obliterator_cannon );
		break;
	}

    }
    else
    {
	learn_from_failure( ch, gsn_obliterator_cannon );
	if ( !IS_AFFECTED(ch, AFF_TIRED ) )
	    xSET_BIT( ch->affected_by, AFF_TIRED );
	else
	    ch->mana *= 0.33;
	pager_printf( ch, "&GYou attempt to vent the magical energy built up within you, but it backfires - badly - and hammers your remaining energy levels!\n\r" );
	act( AT_SKILL, "$n spams in sudden agony as magical sparks explode from $s body!", ch, NULL, NULL, TO_CANSEE );
    }
return;
}


void  do_stungun_headbutt( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA * victim;
    char 	arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )  
    { 
        send_to_char( "You aren't fighting anyone.\n\r", ch );	
        return;    
    }    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_stungun_headbutt, 1 );
        pager_printf(ch, "&GYou hop a few metres backwards, dropping into a low mantis-stance...\n\r" );
        pager_printf(victim, "&G%s hops back a few metres, and drops into a low mantis-stance...\n\r", PERS( ch, victim ) );
        act( AT_GREEN, "$n hops back a few metres and drops into a low mantis-stance.", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	 if ( !ch->alloc_ptr )
     	 {
             send_to_char( "&GYour stungun headbutt was interupted!\n\r", ch );
             pager_printf(victim, "%s's stungun headbutt was interrupted!\n\r", PERS(ch,victim) );
             act( AT_GREEN, "$n's attack was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
             bug( "do_search: alloc_ptr NULL", 0 );
             return;
         }
	 strcpy( arg, ch->alloc_ptr );
	 DISPOSE( ch->alloc_ptr );
	 break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your stungun headbutt.\n\r", ch );
	    pager_printf(victim, "%s stops %s stungun headbutt.\n\r", PERS(ch,victim), HISHER(ch->sex) );
 	    act( AT_GREEN, "$n's cancels $s stungun headbutt.",  ch, NULL, victim, TO_NOTVICT );
            ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(), gsn_stungun_headbutt ) )
    {
   	  learn_from_success( ch, gsn_stungun_headbutt );
	  pager_printf(ch, "You leap forward and grab %s, slamming your head into %s face!\n\r", PERS(victim,ch), HISHER(victim->sex));     
	  pager_printf(victim, "%s leaps forward and grabs you, before slamming %s head right into your face!  OW!\n\r", PERS(ch,victim), HISHER(ch->sex) );
	  act( AT_GREEN, "$n leaps forward and grabs $s opponent, before slamming his head into $N's face!",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, number_range( 5, 20 ), gsn_stungun_headbutt );
          if ( can_use_skill( ch, number_percent(), gsn_stungun_headbutt) )
	  {
	      interpret(victim, "stagger" );
	      WAIT_STATE(victim, 2 * PULSE_PER_SECOND );
	      victim->skill_timer = 2;
	      victim->fakelag = 2;
	  }
    }
    else
    {
	  pager_printf(ch, "You leap forward and try to grab %s, but you miss %s!\n\r", PERS(victim,ch), HIMHER(victim->sex));     
	  pager_printf(victim, "%s leaps forward and tries to grab you - but you deftly dodge %s clumsy lunge.\n\r", PERS(ch,victim), HISHER(ch->sex) );
	  act( AT_GREEN, "$n leaps forward and tries to grab $n, but misses!",  ch, NULL, victim, TO_NOTVICT );
 	  learn_from_failure( ch, gsn_stungun_headbutt );
	  global_retcode = damage( ch, victim, 0, gsn_stungun_headbutt );
    }
return;
}


void  do_suckerpunch( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA * victim;
    char 	arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )  
    { 
        send_to_char( "You aren't fighting anyone.\n\r", ch );	
        return;    
    }    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 5;
        add_timer( ch, TIMER_DO_FUN, 5, do_suckerpunch, 1 );
        pager_printf(ch, "&GYou jerk back your arm and ready a mean uppercut...\n\r" );
        pager_printf(victim, "&G%s jerks back %s arm and readies an uppercut...\n\r", PERS( ch, victim ), HISHER(ch->sex) );
        act( AT_GREEN, "$n readies an uppercut...", ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

	 case 1:
	 if ( !ch->alloc_ptr )
     	 {
             send_to_char( "&GYour suckerpunch was interupted!\n\r", ch );
             pager_printf(victim, "%s's attack was interrupted!\n\r", PERS(ch,victim) );
             act( AT_GREEN, "$n's attack was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
             bug( "do_search: alloc_ptr NULL", 0 );
             return;
         }
	 strcpy( arg, ch->alloc_ptr );
	 DISPOSE( ch->alloc_ptr );
	 break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop your suckerpunch.\n\r", ch );
	    pager_printf(victim, "%s stops charging %s attack.\n\r", PERS(ch,victim), HISHER(ch->sex) );
 	    act( AT_GREEN, "$n's cancels $s suckerpunch.",  ch, NULL, victim, TO_NOTVICT );
            ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(), gsn_suckerpunch ) )
    {
   	  learn_from_success( ch, gsn_suckerpunch );
	  pager_printf(ch, "You leap forward and deliver a mean punch to %s's stomach!\n\r", PERS(victim,ch) );     
	  pager_printf(victim, "%s leaps forward and punches you hard in the gut!  OW!\n\r", PERS(ch,victim) );
	  act( AT_GREEN, "$n leaps forward and sucker-punches $N!",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, number_range( 20, 50 ), gsn_suckerpunch );
	  interpret(victim, "stagger" );
	  if ( number_range(1, 20) == 10 )
	  {
   	      WAIT_STATE(victim, 1 * PULSE_PER_SECOND );
	      victim->skill_timer = 1;
	      victim->fakelag = 1;
	      pager_printf(ch, "Awesome!  %s starts vomiting all over the place from your well-placed hit!\n\r", PERS(victim,ch) );
	      pager_printf(victim, "Aw shit... You start spewing disgusting chunks of vomit all over the place... Clearly you shouldn't have had that third breakfast today.\n\r" );
   	      act( AT_ORANGE, "Take cover!  $N has started throwing up all over the fucking place!",  ch, NULL, victim, TO_NOTVICT );
	 }
    }	
    else
    {
        pager_printf(ch, "You leap forward and try to punch %s, but you miss because you're an enormous retard...\n\r", PERS(victim,ch) );
        pager_printf(victim, "%s leaps forward and tries to punch you - but you deftly dodge %s clumsy lunge.\n\r", PERS(ch,victim), HISHER(ch->sex) );
        act( AT_GREEN, "$n leaps forward and tries to sucker-punch $n, but misses!",  ch, NULL, victim, TO_NOTVICT );
        learn_from_failure( ch, gsn_suckerpunch );
        global_retcode = damage( ch, victim, 0, gsn_suckerpunch );
    }
return;
}


void do_stasis ( CHAR_DATA *ch, char *argument )
{
  CHAR_DATA *victim;
  char buf[MAX_STRING_LENGTH];
  char arg [MAX_INPUT_LENGTH];

  if ( ( victim = who_fighting( ch ) ) != NULL )
  {
      send_to_char( "You are fighting someone.\n\r", ch );
      return;
  }

  switch( ch->substate )
  {
        default:

	ch->skill_timer =  20;
        add_timer( ch, TIMER_DO_FUN, 20, do_stasis, 1 );
	sprintf( buf, "&GYou hold your breath and enter a trance." );
        pager_printf(ch, "%s\n\r", buf );
        act( AT_GREEN, "$n holds $s breath and enters a trance.", ch, NULL, victim, TO_NOTVICT );
	ch->alloc_ptr = str_dup( arg );
	return;

        case 1:
        if ( !ch->alloc_ptr )
        {
          send_to_char( "Your trance was interupted!\n\r", ch );
          sprintf( buf, "&G%s's trance was interrupted!", ch->name );
          pager_printf(victim, "%s\n\r", buf );
          act( AT_GREEN, "$n's trance was interupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
          bug( "do_search: alloc_ptr NULL", 0 );
          return;
        }
        strcpy( arg, ch->alloc_ptr );
        DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop your trance.\n\r", ch );
            act( AT_GREEN, "$n visibly relaxes.\n\r",  ch, NULL, victim, TO_NOTVICT );
            return;
    }

    learn_from_success( ch, gsn_stasis );
    ch->substate = SUB_NONE;
    xSET_BIT(ch->affected_by, AFF_STASIS);
    pager_printf(ch, "You manage to distill the essence of your time freeze move, ready to unleash your next attack even faster!\n\r", buf );
    act( AT_GREEN, "$n visibly relaxes.	\n\r",  ch, NULL, victim, TO_NOTVICT );
    return;
}


void do_rapidcancel(CHAR_DATA *ch, char *argument)
{
  CHAR_DATA *vict = who_fighting( ch );
  if ( vict == NULL )
  {
      pager_printf( ch, "There is no need to rapid cancel anything right now.\n\r");
      return;
  }

  if ( ch->skill_timer == 0 )
  {
     pager_printf( ch, "You are not performing a skill to cancel.\n\r");
     return;
  }  
  
  if ( ch->skill_timer < 2 || vict->skill_timer <= 1 )
  {
    pager_printf( ch, "You are too close to completion to cancel now...\n\r");
    return;
  }


  // If your skill will finish first or they activated first, we fail and take a penalty
  if ( ch->skill_timer < vict->skill_timer || vict->last_ability_time < ch->last_ability_time )
  {
      int dam = get_curr_wis(vict) + get_curr_int(vict);
      interpret(ch, "whimper");
      pager_printf( ch, "You screwed up!  Your technique would have finished first...\n\rYou are stunned!\n\rYou take %d damage!", dam);
      pager_printf( vict, "%s has screwed up a rapid cancel!  %s discharges %s energy too quickly, and is stunned!", HESHE(ch->sex), HISHER(ch->sex));
      act( AT_GREY,"$n wanders around aimlessly.", ch, NULL, NULL, TO_ROOM );
      damage(ch, ch, dam, TYPE_UNDEFINED);
      WAIT_STATE(ch, vict->skill_timer * PULSE_PER_SECOND );
      ch->skill_timer =  vict->skill_timer;
      ch->fakelag =  vict->skill_timer;
  }
  else
  {
      int stagger = vict->skill_timer;
      int damage = get_curr_wis(ch) + get_curr_int(ch);

      interpret(vict, "stagger");
      pager_printf( ch, "Success!  You rapidly cancel your technique and use the excess energy to stun %s!\n\rYou gain %d energy!", PERS(vict,ch), damage );
      pager_printf( vict, "%s rapid cancels!  %s discharges %s energy and quickly stuns you!", HESHE(ch->sex), HISHER(ch->sex));
      act( AT_GREY,"$n wanders around aimlessly.", vict, NULL, NULL, TO_ROOM );
      ch->mana += damage;
      WAIT_STATE(ch, stagger * PULSE_PER_SECOND );
      ch->skill_timer = stagger;
      ch->fakelag = stagger;
  }
}
