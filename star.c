/************************************************
 *        Logo my ass-cii :P - Yami	    	*
 *	DBStar Module - For DBStar only	      	*
 ************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mud.h"
#include <math.h>

bool is_holding( CHAR_DATA *ch, int vnum );
void rprog_dig_trigger( CHAR_DATA *ch, ROOM_INDEX_DATA *room );
double log10( double x );
extern OBJ_INDEX_DATA *obj_index_hash[MAX_KEY_HASH];
double sqrt( double x );
void pl_update( CHAR_DATA *ch, char *argument );
void do_gstat( CHAR_DATA *ch, char *argument );
void scatter_balls();
bool can_defend( int dt );
char * get_limb( int number );
void turbotrain( CHAR_DATA *ch, char * argument );
void kiabsorb( CHAR_DATA *victim, CHAR_DATA *ch , int dam, int dt );
extern CHAR_DATA * infected;
void kaioken_burst( CHAR_DATA *ch, char *arg );

/* kicolor_types --Gohan */
char * const 	kicolor_types	[]	=
{
"^x&zblack",    "^x&Rred",  "^x&gdark-green",   "^x&Obrown",   "^x&bdark-blue",   "^x&ppurple",
"^x&ccyan",     "^x&wgrey",      "^x&zdark-grey",    "^x&rdark-red",     "^x&Ggreen",       "^x&Yyellow", 
"^x&Bblue",     "^x&Ppink",      "^x&Clight-blue",   "^x&Wwhite",   "^x&Ygold",        "^x&revil",
"^x&Ccrystal",  "^x&Oearthy",    "^x&bmidnight",     "^b&xpitch-black^x"
};


void do_gstat( CHAR_DATA *ch, char *argument )
{
 char arg[MAX_INPUT_LENGTH];

 if ( IS_NPC(ch) )
    return;
 argument = one_argument (argument, arg);

 if ( !str_cmp( ch->pcdata->gstat, "None" ) )
 {
     switch( arg[0] )
     {
         case 's': ch->pcdata->gstat = STRALLOC("Strength");      break;
         case 'd': ch->pcdata->gstat = STRALLOC("Dexterity") ;    break;
         case 'c': 
		if(arg[1] == 'o')
		    ch->pcdata->gstat = STRALLOC("Constitution") ;
		if(arg[1] == 'h')
		    ch->pcdata->gstat = STRALLOC("Charisma");
		break;
         case 'i': ch->pcdata->gstat = STRALLOC("Intelligence") ;  break;
         case 'w': ch->pcdata->gstat = STRALLOC("Wisdom") ;        break;
         case 'p': ch->pcdata->gstat = STRALLOC("Powerlevel") ;    break;
	 case 'n': ch->pcdata->gstat = STRALLOC("None")	;	   break;
         default:  ch->pcdata->gstat = STRALLOC("Powerlevel") ;   ch->statup=0; break;
     }
 }
 else
 {
      switch( arg[0] )
     {
         case 's': ch->pcdata->gstat = STRALLOC("Strength");     ch->statup=0; break;
         case 'd': ch->pcdata->gstat = STRALLOC("Dexterity") ;   ch->statup=0; break;
         case 'c':
                if(arg[1] == 'o')
                    ch->pcdata->gstat = STRALLOC("Constitution") ;
                if(arg[1] == 'h')
                    ch->pcdata->gstat = STRALLOC("Charisma");
		ch->statup=0;
                break;
         case 'i': ch->pcdata->gstat = STRALLOC("Intelligence") ; ch->statup=0; break;
         case 'w': ch->pcdata->gstat = STRALLOC("Wisdom") ;       ch->statup=0; break;
         case 'p': ch->pcdata->gstat = STRALLOC("Powerlevel") ;   ch->statup=0; break;
         case 'n': ch->pcdata->gstat = STRALLOC("None") ;         break;
         default:  ch->pcdata->gstat = STRALLOC("Powerlevel") ;   ch->statup=0; break;
     }
 }

pager_printf( ch, "%s set as gravity training stat.\n\r", ch->pcdata->gstat );
return;
}

/*
 *
 * Super Saiyan One
 *
 */

void do_ssj( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];
 int msgno = number_range(1, 3);

if ( IS_AFFECTED(ch, AFF_MAJIN ) )
{
	send_to_char( "You are blinded by your own strength!\n\r", ch );
	return;
}

if ( can_use_skill( ch, number_percent(), gsn_ssj ) )
{
learn_from_success( ch, gsn_ssj );

if ( IS_AFFECTED( ch, AFF_USSJ ) || IS_AFFECTED( ch, AFF_SSJ2) 
  || IS_AFFECTED( ch, AFF_SSJ3 ) || IS_AFFECTED( ch, AFF_SSJ4 )|| IS_AFFECTED( ch, AFF_SSJ5 ) )
	{
	ch->multi_str = 50;
	ch->multi_int = 25;
	ch->multi_wis = 25;
	ch->multi_dex = 25;
	ch->multi_con = 25;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 1200;
	xREMOVE_BIT( ch->affected_by, AFF_USSJ ); xREMOVE_BIT( ch->affected_by, AFF_SSJ2 );
	xREMOVE_BIT( ch->affected_by, AFF_SSJ3 ); xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
        xREMOVE_BIT( ch->affected_by, AFF_SSJ5 );
	xSET_BIT( ch->affected_by, AFF_SSJ );
	send_to_char("Your hair and eyes flash for a moment, then your aura dies down slightly as you power down to SSJ 1.\n\r", ch );
    act( AT_YELLOW, "$n flashes for a moment with a radiant golden energy as he powers down to SSJ 1.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_SSJ ) )
	{
	ch->multi_str = 50;
	ch->multi_int = 25;
	ch->multi_wis = 25;
	ch->multi_dex = 25;
	ch->multi_con = 25;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 1200;
	if ( ch->alignment > 0 )
		sprintf(buf, "&YYour hair begins to rise and flare up with your aura.  Soon you flash with a bright energy as your hair and aura explode in a brilliant, fiery colour and turn&O g&Yo&Ol&Yd&O!&Y" );
	else
		sprintf(buf, "&YYour hair begins to rise and flare up with your aura.  Soon you flash for a moment with a bright energy as your hair and aura explode in a brilliant, fiery colour, and turn &Rd&ra&Rr&rk&R r&re&Rd&Y!");
	pager_printf( ch, "%s\n\r", buf );
	sprintf(buf, "  &YYour eyes glaze over and turn blank, your pupils being replaced with an angry &Gg&gr&Ge&ge&Gn&Y.");
	pager_printf( ch, "%s\n\r", buf );
	sprintf(buf, "  &YYou let out one final scream that cracks the ground underneath your feet as you finally acheive Super Saiyan!");
	pager_printf( ch, "%s\n\r", buf );
	xSET_BIT( ch->affected_by, AFF_SSJ );
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
    act( AT_YELLOW, "&Y$n is overwhelmed with rage, and lets out a huge scream as $s saiyan blood begins to boil.  Slowly, $s hair begins to raise and $s eyes fade away, pupil-less, white voids, but they are soon replaced with raging green orbs as sweat begins to pour from $m, $s muscles bulking up as $e bursts into a brilliant&O g&Yo&Ol&Yd&Oe&Yn aura as $e powers up to Super Saiyan!", ch, NULL, NULL, TO_ROOM );
	}
else if ( IS_AFFECTED( ch, AFF_SSJ ) )
	{
	sprintf(buf, "&GYou take a deep breath and power down, causing your hair to fall and your aura to revert to %s &Gagain.", ch->energycolour);
	pager_printf( ch, "%s\n\r", buf );
        act( AT_YELLOW, "&Y$n flashes a brilliant gold and releases a burst of ki as $e powers back down.", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 0;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_SSJ );
	}
}
else
{
	learn_from_failure( ch, gsn_ssj );
    if ( msgno == 1 )
       {
	if ( ch->alignment > 0 ){
       send_to_char("You scream and flash gold, but nothing more happens...\n\r", ch );
       act( AT_YELLOW, "$n screams and flashes gold, but nothing else happens...", ch, NULL, NULL, TO_ROOM );}
	else{
       send_to_char("You scream and flash grey, but nothing more happens...\n\r", ch );
       act( AT_GREY, "$n screams and flashes grey, but nothing else happens...", ch, NULL, NULL, TO_ROOM );}
       }
    if ( msgno == 2 )
       {
       send_to_char("You nearly have a heart attack trying to go Super Saiyan\n\r!", ch );
       act( AT_YELLOW, "$n screams and tenses up, trying to go Super Saiyan.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 3 )
       {
       send_to_char("You collapse to your knees with exhaustion from trying to go SSJ!\n\r!", ch );
       act( AT_YELLOW, "$n collapses to $s knees, trying to go Super Saiyan.", ch, NULL, NULL, TO_ROOM );
       }
}
pl_update( ch, NULL );
return;
}

/*
 *
 * Ultimate Super Saiyan
 *
 */

void do_ussj( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];
	char thing[MAX_STRING_LENGTH];
 int msgno = number_range(1, 3);

if ( IS_AFFECTED(ch, AFF_MAJIN ) )
{
	send_to_char( "You are blinded by your own strength!\n\r", ch );
	return;
}
if ( can_use_skill( ch, number_percent(), gsn_ussj ) )
{
learn_from_success( ch, gsn_ussj );
if ( IS_AFFECTED( ch, AFF_SSJ2) || IS_AFFECTED( ch, AFF_SSJ3 ) 
|| IS_AFFECTED( ch, AFF_SSJ4)  || IS_AFFECTED( ch, AFF_SSJ5) )
	{
	sprintf( thing, "&CS&Wu&Cp&We&Cr &W%s", ch->name );
	ch->multi_str = 150;
	ch->multi_int = 50;
	ch->multi_wis = 50;
	ch->multi_dex = -100;
	ch->multi_con = 100;
	ch->multi_cha = 0;
	ch->multi_lck = 50;
	ch->plmod     = 1600;
	xREMOVE_BIT( ch->affected_by, AFF_SSJ2 );
	xREMOVE_BIT( ch->affected_by, AFF_SSJ3 );
	xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
        xREMOVE_BIT( ch->affected_by, AFF_SSJ5 );
	xSET_BIT( ch->affected_by, AFF_USSJ );
	send_to_char("&YYour electrical aura flickers and fades away as you power down to USSJ.\n\r", ch );
        act( AT_YELLOW, "&Y$n crackles with electricity and dies down as he reverts from SSJ2.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_USSJ ) )
	{
    if ( IS_AFFECTED( ch, AFF_SSJ ) )
        {
       	ch->multi_str = 150;
    	ch->multi_int = 50;
    	ch->multi_wis = 50;
    	ch->multi_dex = -100;
    	ch->multi_con = 100;
    	ch->multi_cha = 0;
    	ch->multi_lck = 50;
    	ch->plmod     = 1600;
	xREMOVE_BIT( ch->affected_by, AFF_SSJ );
	xSET_BIT( ch->affected_by, AFF_USSJ );
    	sprintf(buf, "&YYour muscles begin to bulk up and expand as your aura flares up brighter, cracking the ground around you, dirt swirling upwards, until soon you are almost double your original muscle mass and achieve USSJ!  " );
    	pager_printf( ch, "%s\n\r", buf );
    	sprintf(buf, "&YYou flex your powerful new muscles and throw a few punches - you're slower, but more powerful than ever!\n\r");
    	pager_printf( ch, "%s\n\r", buf );
        act( AT_BLOOD, "&Y$n throws $s head back, as the sheer force and power of $s aura causes the very heavens themselves to rip apart as bolts of lightning flash, and $e begins to bulk up, growing in ferocity and sheer battle-lust, before letting out a soul-shattering roar, as $e achieves USSJ!", ch, NULL, NULL, TO_ROOM );
        }
    else
        send_to_char("Only a Super Saiyan can become an Ultimate Super Saiyan.", ch );
    }
else if ( IS_AFFECTED( ch, AFF_USSJ ) )
		do_ssj( ch, "" );
}
else
{
	learn_from_failure( ch, gsn_ussj );
    if ( msgno == 1 )
       {
       send_to_char("You scream and your muscles bulk up, but quickly return to normal.\n\r", ch );
       act( AT_YELLOW, "$n's muscles bulk up, but return to normal quickly.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 2 )
       {
       send_to_char("You nearly have a heart attack trying to become an Ultimate Super Saiyan!\n\r", ch );
       act( AT_YELLOW, "$n screams and tenses up, trying to become an Ultimate Super Saiyan.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 3 )
       {
       send_to_char("You collapse to your knees with exhaustion from trying to go USSJ!!\n\r", ch );
       act( AT_YELLOW, "$n collapses to $s knees, trying to become an Ultimate Super Saiyan.", ch, NULL, NULL, TO_ROOM );
       }
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Super Saiyan Two
 *
 */

void do_ssj2( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];
 int msgno = number_range(1, 3);
if ( IS_AFFECTED(ch, AFF_MAJIN ) )
{
	send_to_char( "You are blinded by your own strength!\n\r", ch );
	return;
}

if ( can_use_skill( ch, number_percent(), gsn_ssj2 ) )
{
    learn_from_success( ch, gsn_ssj2 );
    if ( IS_AFFECTED( ch, AFF_SSJ3 ) || IS_AFFECTED( ch, AFF_SSJ4 ) || IS_AFFECTED( ch, AFF_SSJ5 ) )
	{
	    ch->multi_str = 100;
	    ch->multi_int = 100;
	    ch->multi_wis = 100;
	    ch->multi_dex = 100;
	    ch->multi_con = 100; 
	    ch->multi_cha = 0;
	    ch->multi_lck = 100;
	    ch->plmod     = 2000;
	    xREMOVE_BIT( ch->affected_by, AFF_SSJ3 );
	    xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
        xREMOVE_BIT( ch->affected_by, AFF_SSJ5 );
	    xSET_BIT( ch->affected_by, AFF_SSJ2 );
	    send_to_char("Your hair begins to retract to its original length and your eyebrows pop back into place as you powerdown to SSJ2!\n\r", ch );
        act( AT_BLOOD, "$n's hair retracts back to its original length and $s eyebrows pop back into place as $e powers down to SSJ2.", ch, NULL, NULL, TO_ROOM );
	}
    else if ( !IS_AFFECTED( ch, AFF_SSJ2 ) )
	{
        if ( IS_AFFECTED( ch, AFF_SSJ ) || IS_AFFECTED( ch, AFF_USSJ ) )
        {
		    ch->multi_str = 100;
		    ch->multi_int = 100;
		    ch->multi_wis = 100;
		    ch->multi_dex = 100;
		    ch->multi_con = 100;
		    ch->multi_cha = 0;
		    ch->multi_lck = 100;
            ch->plmod     = 2000;
		    xREMOVE_BIT( ch->affected_by, AFF_SSJ );
		    xREMOVE_BIT( ch->affected_by, AFF_USSJ );
		    xSET_BIT( ch->affected_by, AFF_SSJ2 );
            sprintf(buf, "&YYour bright aura flares up around your body, brighter than ever as your hair begins to perk up, electricity starts to arc about your body and you feel yourself near finality...");
            send_to_char( buf, ch );
            sprintf(buf, "&Y  With a blood curdling scream, your hair flares up again and arcs back into a point, and the electricity around you becomes more frequent and threateningly powerful!!\n\r");
            send_to_char( buf, ch );
            act( AT_YELLOW, "&Y$n's aura flares up more and electricity starts arcing around $m.  Slowly $e begins walking towards you with a hand outstretched as electricity jumps from $s hand, leaping out at you.  Before $e can manage to reach you $e falls to the ground and grabs $s head, muscles bulking slightly as the veins in $s head throb.  $e lets out another final scream and rises to $s feet, looking much more focused and powerful!", ch, NULL, NULL, TO_ROOM );
        }
        else
            send_to_char("Only a Super Saiyan or Ultimate Super Saiyan can become a Super Saiyan 2.", ch );
    }
    else if ( IS_AFFECTED( ch, AFF_SSJ2 ) )
	{
		do_ssj(ch,"");	
	}
}
else
{
	learn_from_failure( ch, gsn_ssj2 );
    if ( msgno == 1 )
       {
       send_to_char("A single spark of electricity arcs around you, but disappears.\n\r", ch );
       act( AT_LBLUE, "A spark of electricty arcs around $n.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 2 )
       {
       send_to_char("Your muscles bulk up a little as you try to go SSJ2, but you fail.\n\r", ch );
       act( AT_YELLOW, "$n screams and tenses up, trying to become a Super Saiyan 2.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 3 )
       {
       send_to_char("You collapse to your knees with exhaustion from trying to go SSJ2!!\n\r", ch );
       act( AT_YELLOW, "$n collapses to $s knees, trying to become a Super Saiyan Two.", ch, NULL, NULL, TO_ROOM );
       }
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Super Saiyan Three
 *
 */

void do_ssj3( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];
 int msgno = number_range(1, 3);

if ( IS_AFFECTED(ch, AFF_MAJIN ) )
{
	send_to_char( "You are blinded by your own strength!\n\r", ch );
	return;
}
if ( can_use_skill( ch, number_percent(), gsn_ssj3 ) )
{
learn_from_success( ch, gsn_ssj3 );
if ( IS_AFFECTED( ch, AFF_SSJ4 ) || IS_AFFECTED( ch, AFF_SSJ5 ))
		{
	ch->multi_str = 175;
	ch->multi_int = 150;
	ch->multi_wis = 100;
	ch->multi_dex = 175;
	ch->multi_con = 150;
	ch->multi_cha = 50;
	ch->multi_lck = 0;
	ch->plmod     = 2500;
	xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
        xREMOVE_BIT( ch->affected_by, AFF_SSJ5 );
	xSET_BIT( ch->affected_by, AFF_SSJ3 );
	send_to_char("You powerdown to SSJ3\n\r", ch );
    act( AT_YELLOW, "$n powers down to SSJ3.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_SSJ3 ) )
	{
    if ( IS_AFFECTED( ch, AFF_SSJ2 ) )
        {
        ch->multi_str = 175;
        ch->multi_int = 150;
        ch->multi_wis = 100;
        ch->multi_dex = 175;
        ch->multi_con = 150;
        ch->multi_cha = 50;
        ch->multi_lck = 0;
        ch->plmod     = 2500;
	    xREMOVE_BIT( ch->affected_by, AFF_SSJ2 );
	    xSET_BIT( ch->affected_by, AFF_SSJ3 );
       	sprintf(buf, "&YYour golden aura flares brighter and brighter as your eyebrows disappear, the saiyan rage within you stirring, pushing deeper than you ever have before for power as your blood begins to boil!");
        send_to_char( buf, ch );
       	sprintf(buf, "&Y  With a roar to end all creation, you shatter the ground underneath you with the sheer power of your aura and your hair begins crawls down your back!!!");
        send_to_char( buf, ch );
        act( AT_YELLOW, "&Y$n seems to have reached the limits of $s power. But then destiny decides to prove $m wrong. From within, at levels of $s being $e never even knew -EXISTED-, comes a new wellspring of power.  A strength that few reach and fewer still know what to do with. The heavens themselves express their joy at this new plateau, this new challenge to overcome. Lights dance, cloud become true shapes and figures, with all the proper colors. The oceans rise and produce tidal waves in anticipation, the mountains tremble, and then the hair on $s head begins to grow to incredible proportions, $s eyebrows vanish.... with a blinding flash of light the change is complete! Like the cry of the beast that screamed at the heart of the world, this scream is enough to make galaxies shudder in fear. The world now has a Super Saiyan Three to nurture as its own!", ch, NULL, NULL, TO_ROOM );
        }
    else
        send_to_char("Only a Super Saiyan 2 can become a Super Saiyan 3.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_SSJ3 ) )
	{
		do_ssj2(ch,"");
	}
}
else
{
	learn_from_failure( ch, gsn_ssj3 );
    if ( msgno == 1 )
       {
       send_to_char("Your aura flares higher and your hair extends, but quickly returns to normal.\n\r", ch );
       act( AT_LBLUE, "$n's energy flares up, but returns to normal quickly.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 2 )
       {
       send_to_char("You almost kill yourself trying to go SSJ3.\n\r", ch );
       act( AT_YELLOW, "$n screams and tenses up, trying to become a Super Saiyan 3.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 3 )
       {
       send_to_char("You collapse to your knees with exhaustion from trying to go SSJ3!!\n\r", ch );
       act( AT_YELLOW, "$n collapses to $s knees, trying to become a Super Saiyan Three.", ch, NULL, NULL, TO_ROOM );
       }
}
pl_update( ch, NULL );
return;
}

/*
 *
 * Super Saiyan Four
 *
 */

void do_ssj4( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];
 int msgno = number_range(1, 3);
 int  level = 0;
 
if ( !IS_NPC(ch) && !ch->pcdata->monkey )
{
	send_to_char("You must be confusing your belt for a tail...\n\r", ch );
	return;
}

if ( ch->alignment < 333 )
{
	send_to_char( "Only those with a pure heart can take Super Saiyan any further.\n\r", ch );
	return;
}

if ( can_use_skill( ch, number_percent(), gsn_ssj4 ) )
{
   learn_from_success( ch, gsn_ssj4 );
   if ( !IS_AFFECTED( ch, AFF_SSJ4 ) )
   {
     if ( IS_AFFECTED( ch, AFF_SSJ3 ) )
     {
	level = ch->perm_str/100;;
	ch->multi_str = 700;
        ch->multi_int = 250 + ( 10 * level );
        ch->multi_wis = 250 + ( 10 * level );
        ch->multi_dex = 250 + ( 10 * level );
        ch->multi_con = 250 + ( 10 * level );
        ch->multi_cha = 250 + ( 10 * level );
        ch->multi_lck = 250 + ( 10 * level );
	    ch->plmod = 4000;
	    xREMOVE_BIT( ch->affected_by, AFF_SSJ3 );
	    xSET_BIT( ch->affected_by, AFF_SSJ4 );
    	sprintf(buf, "&rYou look down at your hands and ball them into fists, realising this simple power that you now have is no longer enough to satisfy you, your brilliant golden aura flickers for a moment and turns to a dark blood red, your body is covered with %s &rfur and your hair shortens and returns to normal color, letting out a scream that shatters the mountain tops and blows the ground underneath your feet away as you realise your dream: The ultimate warrior...", ch->energycolour);
    	pager_printf( ch, "%s\n\r", buf );
	act( AT_BLOOD, "&zWith a slight smirk, $n stands still. $e closes $s eyes slowly, and begins to power up. $e pushes out a tremendous &Yy&Oe&Yl&Ol&Yo&Ow&z and &Yo&Or&Ya&On&Yg&Oe&z aura, while a blindingly bright light pushes itself outwards, eminating from the centre of $s body. $s hair begins to change subtly, getting slightly longer and adopting the shape and colour of $s original hair. As the light dims, and your vision returns, you see &Rred&z fur quickly creeping upon $s body, engulfing $s back, tail and arms first, before creeping to $s chest. $s muscle mass increases almost three-fold, and as $s aura returns to its normal state, $n stands tall and mighty, with $s tail proudly protuding.", ch, NULL, NULL, TO_ROOM );
     }
     else
        send_to_char("Only a Super Saiyan 3 can become a Super Saiyan 4.", ch );
   }
   else if ( IS_AFFECTED( ch, AFF_SSJ4 ) )
		do_ssj3(ch,"");
}
else
{
	learn_from_failure( ch, gsn_ssj4 );
    if ( msgno == 1 )
       {
       send_to_char("&RFur starts to grow on your body, but soon disappears.\n\r", ch );
       act( AT_LBLUE, "Fur grows around $n, but soon disappears.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 2 )
       {
       send_to_char("You almost kill yourself trying to go SSJ4.\n\r", ch );
       act( AT_YELLOW, "$n screams and tenses up, trying to become a Super Saiyan 4.", ch, NULL, NULL, TO_ROOM );
       }
    if ( msgno == 3 )
       {
       send_to_char("You collapse to your knees with exhaustion from trying to go SSJ4!!\n\r", ch );
       act( AT_YELLOW, "$n collapses to $s knees, trying to become a Super Saiyan Four.", ch, NULL, NULL, TO_ROOM );
       }
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Semi Perfect
 *
 */

void do_semi_perfect( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];

if ( ch->pkills + ch->pcdata->mkills < 500 )
	{
	send_to_char("You should kill some more before you go for this form.\n\r", ch );
    return;
	}
if ( can_use_skill( ch, number_percent(), gsn_semi ) )
{
learn_from_success( ch, gsn_semi );
if ( IS_AFFECTED( ch, AFF_PERFECT ) || IS_AFFECTED( ch, AFF_FINAL_PERFECT ) )
	{
	ch->multi_str = 25;
	ch->multi_int = 25;
	ch->multi_wis = 0;
	ch->multi_dex = 25;
	ch->multi_con = 25;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 1400;
	xREMOVE_BIT( ch->affected_by, AFF_PERFECT );
	xREMOVE_BIT( ch->affected_by, AFF_FINAL_PERFECT );
	xSET_BIT( ch->affected_by, AFF_SEMI_PERFECT );
	send_to_char("You begin to feel inferior to your previous form. Your features become more distorted as you begin to powerdown to semi perfect\n\r", ch );
    act( AT_YELLOW, "$n's features begin to grow more and more distorted as they power down to semi perfect.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_SEMI_PERFECT ) )
	{
	ch->multi_str = 25;
	ch->multi_int = 25;
	ch->multi_wis = 0;
	ch->multi_dex = 25;
	ch->multi_con = 25;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 1400;
	xSET_BIT( ch->affected_by, AFF_SEMI_PERFECT );
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
	sprintf(buf, "&gYour body begins to glow %s &gand change quickly as your scales smoothen slightly and your facial features rearrange themselves to something slightly more humane and your voice deepens. Then, using the soul energy you've collected from all of those you have killed, you explode with an arua of immense power, creating a shockwave of air all around you.", ch->energycolour);
    send_to_char( buf, ch );
    act( AT_YELLOW, "$n changes visibly as $s scales smoothen slightly and $s facial features form into something more normal, and $s voice deepens a great deal. Then $e throws some punches and kicks in the air, and exclaims 'Nice!'", ch, NULL, NULL, TO_ROOM );
	}
else if ( IS_AFFECTED( ch, AFF_SEMI_PERFECT ) )
	{
	sprintf(buf, "&GYou begin to grow more and more distorted as you revert back to the hatchling stage.");
	pager_printf( ch, "%s\n\r", buf );
    act( AT_GREEN, "$n begins to grow more and more distorted as they revert to the hatchling stage.", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 0;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_SEMI_PERFECT );
	}
}
else
{
	learn_from_failure( ch, gsn_semi );
    send_to_char("&gPerfection seems to be just a few steps away...", ch );
    act( AT_GREEN, "$n looks annoyed as $e fails to go Semi perfect.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Perfect
 *
 */

void do_perfect( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

if ( ch->pkills + ch->pcdata->mkills < 2000 )
	{
	send_to_char("You should kill some more before you go for this form.\n\r", ch );
	return;
	}
if ( can_use_skill( ch, number_percent(), gsn_perfect ) )
{
learn_from_success( ch, gsn_perfect );
if ( IS_AFFECTED( ch, AFF_FINAL_PERFECT ) )
	{
	ch->multi_str = 100;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 100;
	ch->multi_con = 50;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 2000;
	xREMOVE_BIT( ch->affected_by, AFF_FINAL_PERFECT );
	xSET_BIT( ch->affected_by, AFF_PERFECT );
	send_to_char("You powerdown to perfect.\n\r", ch );
    act( AT_GREEN, "$n powers down to perfect.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_PERFECT ) )
	{
    if ( IS_AFFECTED( ch, AFF_SEMI_PERFECT ) )
        {
   	    ch->multi_str = 100;
    	ch->multi_int = 0;
	    ch->multi_wis = 0;
	    ch->multi_dex = 100;
	    ch->multi_con = 50;
     	ch->multi_cha = 0;
     	ch->multi_lck = 0;
     	ch->plmod     = 2000;
	xREMOVE_BIT( ch->affected_by, AFF_SEMI_PERFECT );
	xSET_BIT( ch->affected_by, AFF_PERFECT );
     	sprintf(buf, "&gGlowing with the massing numbers of those who have become part of you, you scream a sound which echoes all around. You blow away your rough outer layers, replacing them with leaner faster and alltogether better musculaire. Your tail, now less needed than before, rectracts up behind your beetle-like wings. You take a moment to contemplate your existance, offering only a slight smirk to the damage you have caused transforming");
        send_to_char( buf, ch );
        act( AT_GREEN, "$n's tail retracts as $e grows taller, $s facial features become much more defined and consistent with each other and $s scales smoothen out, reaching perfection.", ch, NULL, NULL, TO_ROOM );
        }
    else
        send_to_char("You need to be Semi Perfect to be Perfect.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_PERFECT ) )
	{
	sprintf(buf, "&GYou lose your aura of souls and begin to feel slightly more self conscious of yourself as you powerdown to semi perfect.");
	pager_printf( ch, "%s\n\r", buf );
    act( AT_GREEN, "$n powers down to Semi Perfect, and looks slightly uglier.", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 25;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 25;
	ch->multi_con = 25;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 1400;
	xREMOVE_BIT( ch->affected_by, AFF_PERFECT );
	xSET_BIT( ch->affected_by, AFF_SEMI_PERFECT );
	}
}
else
{
	learn_from_failure( ch, gsn_perfect );
    send_to_char("&gPerfection seems to be just a few steps away...\n\r", ch );
    act( AT_GREEN, "$n looks annoyed as $e fails to go perfect.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Final Perfect
 *
 */

void do_final_perfect( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

if ( ch->pkills + ch->pcdata->mkills < 5000 )
	{
	send_to_char("You should kill some more before you go for this form.\n\r", ch );
    return;
	}
if ( can_use_skill( ch, number_percent(), gsn_finalperfect ) )
{
learn_from_success( ch, gsn_finalperfect );
if ( !IS_AFFECTED( ch, AFF_FINAL_PERFECT ) )
	{
    if ( IS_AFFECTED( ch, AFF_PERFECT ) )
        {
        ch->multi_str = 300;
        ch->multi_int = 150;
        ch->multi_wis = 150;
        ch->multi_dex = 300;
       	ch->multi_con = 300;
       	ch->multi_cha = 150;
       	ch->multi_lck = 150;
       	ch->plmod     = 2600;
	    xREMOVE_BIT( ch->affected_by, AFF_PERFECT );
	    xSET_BIT( ch->affected_by, AFF_FINAL_PERFECT );
       	sprintf(buf, "&gYour body jerks stiffly with a great surge of power and you arch backwards with a cry of elation your aura ferrocously explodes around you. You rise into the air as the ground collapses from under your feet, and all your senses peak in an instant as one final insurgence of energy erupts from you, crying with all those you have absorbed. You feel you have the power of a God.\n\r");
        send_to_char( buf, ch );
        act( AT_GREEN, "$n jerks up as a surge of power runs through $m.", ch, NULL, NULL, TO_ROOM );
        }
    else
        send_to_char("You need to be perfect to reach Final Perfect.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_FINAL_PERFECT ) )
	{
	sprintf(buf, "&GYou powerdown to perfect and feel a little less impressed with yourself as the demi-god feeling slowly fades away.");
	pager_printf( ch, "%s\n\r", buf );
    act( AT_GREEN, "$n powers down to perfect and their aura doesn't seem quite so ferrocious now.", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 100;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 100;
	ch->multi_con = 50;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 2000;
	xREMOVE_BIT( ch->affected_by, AFF_FINAL_PERFECT );
	xSET_BIT( ch->affected_by, AFF_PERFECT );
    }
}
else
{
	learn_from_failure( ch, gsn_finalperfect );
    send_to_char("&gPerfection seems to be just a few steps away...\n\r", ch );
    act( AT_GREEN, "$n looks annoyed as $e fails to go Final Perfect.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Hyper
 *
 */

void do_hyper( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

if ( IS_AFFECTED( ch, AFF_NPOTWK ) )
{
	send_to_char( "Wolf warrior AND Hyper?  Fat chance.\n\r", ch );
	return;
}
if ( ch->perm_con < 100 )
	{
	send_to_char("Hyper needs a lot of stamina to pull off.\n\r", ch );
    return;
	}
if ( can_use_skill( ch, number_percent(), gsn_hyper ) )
{
    learn_from_success( ch, gsn_hyper );
    if ( ch->plmod > 2400 )
    {
	ch->multi_str = 400;
	ch->multi_dex = 150;
	ch->multi_con = 400;
	ch->multi_int = 150;
	ch->multi_wis = 400;
	ch->multi_cha = 150;
	ch->multi_lck = 400;
	ch->plmod     = 2400;
	send_to_char("You feel slightly less strained and your muscles slightly decrease in mass as you powerdown to hyper.\n\r", ch );
        act( AT_LBLUE, "$n looks slightly more relaxed as $s muscle mass decreases when $e power down.", ch, NULL, NULL, TO_ROOM );
    } 
    else if ( !IS_AFFECTED( ch, AFF_HYPER ) )
    {
	ch->multi_str = 400;
	ch->multi_dex = 150;
	ch->multi_con = 400;
	ch->multi_int = 150;
	ch->multi_wis = 400;
	ch->multi_cha = 150;
	ch->multi_lck = 400;
	ch->plmod     = 2400;
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
        xSET_BIT( ch->affected_by, AFF_HYPER );
 	sprintf(buf, "&rYou begin to heave heavily as you gasp for air, holding your chest tight, you begin to reach up and claw at your head as the rage inside you begins to build, finally you fall to your knees and smash your fists into the ground, making a huge crater as your %s &renergy flares up and your heartbeat echoes around the area...", ch->energycolour);
        pager_printf( ch, "%s\n\r", buf );
        act( AT_LBLUE, "$n begins to heave heavily and gasp for air, holding $s chest tight, $e begins to reach up and claw at $s head as the rage inside $m begins to build.  Finally $e falls to $s knees and smashes $s fists into the ground, making a huge crater as &renergy flares up and $s heartbeat echoes around the area!", ch, NULL, NULL, TO_ROOM );
    }
    else if ( IS_AFFECTED( ch, AFF_HYPER ) )
    {
    	sprintf(buf, "&GYou feel slightly more relaxed and your muscles decrease in mass as powerdown to your base.");
        pager_printf( ch, "%s\n\r", buf );
        ch->multi_str = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_lck = 0;
	ch->multi_cha = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_HYPER );
        act( AT_LBLUE, "$n looks slightly more relaxed as $s muscle mass decreases when $e powers down.", ch, NULL, NULL, TO_ROOM );
    }
}
else
{
	learn_from_failure( ch, gsn_hyper );
    send_to_char("&zYou ALMOST reach hyper...\n\r", ch );
    act( AT_GREEN, "$n looks annoyed as $e fails to reach hyper.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Super Android
 *
 */

/*
void do_super_android( CHAR_DATA *ch, char *argument  )
return;
{
 char buf[MAX_STRING_LENGTH];

if ( ch->perm_int < 250 && ch->basepl < 5000000000 )
	{
	send_to_char("You'd have to be thinking VERY clearly to make such a system modification.\n\r", ch );
    return;
	}
if ( can_use_skill( ch, number_percent(), gsn_superandroid ) )
{
learn_from_success( ch, gsn_superandroid );
if ( ch->plmod > 5000 )
	{
	ch->multi_str = 200;
	ch->multi_wis = 0;
	ch->multi_dex = 100;
	ch->multi_con = 200;
	ch->multi_int = 100;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 5000;
	send_to_char("You powerdown to Super.", ch );
    act( AT_MAGIC, "$n powers down to super.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_SUPER_ANDROID ) )
	{
	ch->multi_str = 200;
	ch->multi_wis = 0;
	ch->multi_dex = 100;
	ch->multi_con = 200;
	ch->multi_int = 100;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod     = 5000;
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
	xSET_BIT( ch->affected_by, AFF_SUPER_ANDROID );
	sprintf(buf, "&rYou are surrounded by %s &rsparks, and your hair grows long and straight...", ch->energycolour);
	pager_printf( ch, "%s\n\r", buf );
	sprintf(buf, "&rYour mission is now clear in your mind... Kill!!");
	pager_printf( ch, "%s\n\r", buf );
        act( AT_MAGIC, "$n's hair goes long and straight as $e finally understands $s mission...", ch, NULL, NULL, TO_ROOM );
	}
else if ( IS_AFFECTED( ch, AFF_SUPER_ANDROID ) )
	{
	sprintf(buf, "&GYou powerdown to your base.");
	pager_printf( ch, "%s\n\r", buf );
	ch->multi_str = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_int = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_SUPER_ANDROID );
    act( AT_MAGIC, "$n powers down.", ch, NULL, NULL, TO_ROOM );
	}
 }
else
{
	learn_from_failure( ch, gsn_superandroid );
    sprintf(buf, "&rSomething goes wrong with your super( %s ) function!", ch->name);
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks a bit strange...", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}
*/

/*
 *
 * Namekian Growth
 *
 */

void do_grow( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];
char arg1[MAX_INPUT_LENGTH];
int mod, argnum;


argument = one_argument(argument, arg1);
argnum = atoi(arg1);

if ( argnum < 2 || argnum > 10)
{	
	send_to_char("Grow range is from 2 to 10.\n\r", ch );
	return;
}	

if ( can_use_skill( ch, number_percent(), gsn_grow ) )
{
learn_from_success( ch, gsn_grow );
mod = argnum;
if ( IS_AFFECTED( ch, AFF_GROW ) && ch->plmod > ( mod * 100 ) )
{
	ch->multi_str = mod * 2;
	ch->multi_wis = mod * 2;
	ch->multi_dex = mod * 2;
	ch->multi_con = mod * 2;
	ch->multi_int = mod * 2;
	ch->multi_cha = mod * 2;
	ch->multi_lck = mod * 2;
	ch->plmod     = mod * 75;
	send_to_char("Your muscles bulk up as you grow to an enourmous size.", ch );
        act( AT_MAGIC, "$n grows.", ch, NULL, NULL, TO_ROOM );
}
else if ( ch->plmod < mod * 100 )
{
	ch->multi_str = mod * 5;
	ch->multi_wis = mod * 10;
	ch->multi_dex = mod * 5;
	ch->multi_con = mod * 6;
	ch->multi_int = mod * 10;
	ch->multi_cha = mod * 2;
	ch->multi_lck = mod;
	ch->plmod     = mod * 100;
	if( !IS_AFFECTED( ch, AFF_GROW ) )
		xSET_BIT( ch->affected_by, AFF_GROW );
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
	sprintf(buf, "&rYou grow much larger in a bid to increase your power.");
	pager_printf( ch, "%s\n\r", buf );
    act( AT_MAGIC, "$n grows in size immensely.", ch, NULL, NULL, TO_ROOM );
}
else if ( IS_AFFECTED( ch, AFF_GROW ) && ch->plmod == mod * 100 )
{
	sprintf(buf, "&GYou powerdown to your base and return to normal size.");
	pager_printf( ch, "%s\n\r", buf );
	ch->multi_str = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_int = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_GROW );
 	act( AT_MAGIC, "$n powers down amd shrinks to their regular size.", ch, NULL, NULL, TO_ROOM );
}
}
else
{
	learn_from_failure( ch, gsn_grow );
    sprintf(buf, "&rYou try to grow in size, but fail to change\n\r");
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks pensive.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}


/*
 *
 * Super Namek
 *
 */

void do_super_namek( CHAR_DATA *ch, char *argument )
{
char arg1[MAX_INPUT_LENGTH];
char buf[MAX_STRING_LENGTH];
int mod, argnum;


argument = one_argument( argument, arg1);
argnum = atoi(arg1);

if ( argnum < 18 || argnum > 28)
{	
	send_to_char("Range is from 18 to 28.\n\r", ch );
	return;
}	

if ( can_use_skill( ch, number_percent(), gsn_supernamek ) )
{
learn_from_success( ch, gsn_supernamek );

mod = argnum;
if ( get_curr_wis( ch ) / 5 < mod  ) 
{
	send_to_char("You need 5 times the level of Super you are going for in wisdom.\n\r", ch );
	return;
}
if ( IS_AFFECTED( ch, AFF_SUPER_NAMEK ) && ch->plmod > ( mod * 150 ) )
{
	ch->multi_str = mod * 12;
	ch->multi_wis = mod * 14;
	ch->multi_dex = mod * 12;
	ch->multi_con = mod * 22;
	ch->multi_int = mod * 17;
	ch->multi_cha = mod * 7;
	ch->multi_lck = mod * 4;
	ch->plmod     = mod * 150;
	send_to_char("Your energy flares down as you powerdown.", ch );
        act( AT_MAGIC, "$n's energy flares downwards a little.", ch, NULL, NULL, TO_ROOM );
}
else if ( ch->plmod < mod * 150 )
{
	ch->multi_str = mod * 12;
	ch->multi_wis = mod * 14;
	ch->multi_dex = mod * 12;
	ch->multi_con = mod * 22;
	ch->multi_int = mod * 17;
	ch->multi_cha = mod * 7;
	ch->multi_lck = mod * 4;
	ch->plmod     = mod * 150;
	if( !IS_AFFECTED( ch, AFF_SUPER_NAMEK ) )
		xSET_BIT( ch->affected_by, AFF_SUPER_NAMEK );
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
        xREMOVE_BIT( ch->affected_by, AFF_GROW );
	sprintf(buf, "&rYour %s &renergy flares up, and you feel much more vibrant and wise.", ch->energycolour);
	pager_printf( ch, "%s\n\r", buf );
	sprintf(buf, "&rSuddenly knowledge of your ancestors fills your mind.");
	pager_printf( ch, "%s\n\r", buf );
        act( AT_MAGIC, "$n calls upon ancient Namekian skills and explodes in a bright flash as $e turns into a Super Namek.  Now $s eyes radiate power and knowledge.", ch, NULL, NULL, TO_ROOM );
}
else if ( IS_AFFECTED( ch, AFF_SUPER_NAMEK ) && ch->plmod == mod * 150 )
{
	sprintf(buf, "&GYou powerdown to your base.");
	pager_printf( ch, "%s\n\r", buf );
	ch->multi_str = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
	ch->multi_int = 0;
	ch->multi_cha = 0;
	ch->multi_lck = 0;
	ch->plmod = 100;
	xREMOVE_BIT( ch->affected_by, AFF_SUPER_NAMEK );
 	act( AT_MAGIC, "$n powers down.", ch, NULL, NULL, TO_ROOM );
}
}
else
{
	learn_from_failure( ch, gsn_supernamek );
    sprintf(buf, "&rYou forgot some of the wisdom of the Namekian Elders...");
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks pensive.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}

/*
 *
 * Second Form
 *
 */

void do_form_two( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];

if ( can_use_skill( ch, number_percent(), gsn_formtwo ) )
{
learn_from_success( ch, gsn_formtwo );
if ( IS_AFFECTED( ch, AFF_THIRD_FORM ) || IS_AFFECTED( ch, AFF_FOURTH_FORM )
	|| IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
	{
	ch->multi_str = UMIN ( 250, ch->perm_str * 2 );
    	ch->multi_int = UMIN ( 250, ch->perm_int * 2 );
	ch->multi_wis = UMIN ( 250, ch->perm_wis * 2 );
	ch->multi_dex = UMIN ( 250, ch->perm_dex * 2 );
	ch->multi_con = UMIN ( 250, ch->perm_con * 2 );
    	ch->multi_cha = UMIN ( 250, ch->perm_cha * 2 );
    	ch->multi_lck = UMIN ( 250, ch->perm_lck * 2 );
	ch->plmod     = 1200;
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
	xREMOVE_BIT( ch->affected_by, AFF_THIRD_FORM );
	xREMOVE_BIT( ch->affected_by, AFF_FOURTH_FORM );
	xREMOVE_BIT( ch->affected_by, AFF_FIFTH_FORM );
	xSET_BIT( ch->affected_by, AFF_SECOND_FORM );
	send_to_char("You grow less streamlined as you powerdown to Form Two.\n\r", ch );
        act( AT_YELLOW, "$n looks less steamlined as they power down to Form Two.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_SECOND_FORM ) )
    {
        ch->multi_str = UMIN ( 250, ch->perm_str * 2 );
        ch->multi_int = UMIN ( 250, ch->perm_int * 2 );
        ch->multi_wis = UMIN ( 250, ch->perm_wis * 2 );
        ch->multi_dex = UMIN ( 250, ch->perm_dex * 2 );
        ch->multi_con = UMIN ( 250, ch->perm_con * 2 );
        ch->multi_cha = UMIN ( 250, ch->perm_cha * 2 );
        ch->multi_lck = UMIN ( 250, ch->perm_lck * 2 );
        ch->plmod     = 1200;
	xREMOVE_BIT( ch->affected_by, AFF_FIRST_FORM );
	xSET_BIT( ch->affected_by, AFF_SECOND_FORM );
        sprintf(buf, "&pPurple and white flames of your aura explode outwards as your energy within surges towards each extremity, causing it to bulge outwards asymetrically in a groutesque display of growth, soon your whole body has enlarged and evened out again, your horns rearing high with razor like edges, your tail lengthened and ready to use as a weapon.");
        pager_printf( ch, "%s\n\r", buf );
        act( AT_PINK, "Dark purple flames spring into existence around $n as $e collapses to $s knees and begins to scream with pain. Large chunks of the planet are hurled into the air as their powerlevel skyrockets. $n's body slowly begins to grow more and more distorted, the horns on $s head grow much longer, and turn so that they point towards the sky. You cant help but gape as $e nearly doubles in height, and $s muscle mass increases dramaticly. Finally, with a sinister chuckle, $n slowly stands, with a large icey purple aura surrounding $s massive body, dark red bolts of lightning flicker through the aura at odd intervals as the Icer turns to smile at you, twitching $s long muscular tail.", ch, NULL, NULL, TO_ROOM );
    }
else if ( IS_AFFECTED( ch, AFF_SECOND_FORM ) )
	{
	sprintf(buf, "&GYou powerdown.");
	pager_printf( ch, "%s\n\r", buf );
        act( AT_YELLOW, "$n powers down and reverts to $s original size.\n\r", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 0;
    	ch->multi_int = 0;
	ch->multi_wis = 0;
	ch->multi_dex = 0;
	ch->multi_con = 0;
    	ch->multi_cha = 0;
    	ch->multi_lck = 0;
	ch->plmod     = 100;
	xREMOVE_BIT( ch->affected_by, AFF_SECOND_FORM );
 	}
}
else
{
	learn_from_failure( ch, gsn_formtwo );
    sprintf(buf, "&rYou fail to go to Second Form." );
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks angry.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Third Form
 *
 */

void do_form_three( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];

if ( can_use_skill( ch, number_percent(), gsn_formthree ) )
{
learn_from_success( ch, gsn_formthree );
if ( IS_AFFECTED( ch, AFF_FOURTH_FORM ) || IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
	{
        ch->multi_str = UMIN ( 500, ch->perm_str * 3 );
        ch->multi_int = UMIN ( 500, ch->perm_int * 3 );
        ch->multi_wis = UMIN ( 500, ch->perm_wis * 3 );
        ch->multi_dex = UMIN ( 500, ch->perm_dex * 3 );
        ch->multi_con = UMIN ( 500, ch->perm_con * 3 );
        ch->multi_cha = UMIN ( 500, ch->perm_cha * 3 );
        ch->multi_lck = UMIN ( 500, ch->perm_lck * 3 );
	ch->plmod     = 2000;
	xREMOVE_BIT( ch->affected_by, AFF_FOURTH_FORM );
	xREMOVE_BIT( ch->affected_by, AFF_FIFTH_FORM );
	xSET_BIT( ch->affected_by, AFF_THIRD_FORM );
	send_to_char("You grow in size and regain your spikes as you powerdown to Form Three.\n\r", ch );
        act( AT_YELLOW, "$n grows in size and regains their spikes as ther power down to Form Three.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_THIRD_FORM ) )
	 {
     if ( IS_AFFECTED( ch, AFF_SECOND_FORM ) )
     	 {
        ch->multi_str = UMIN ( 500, ch->perm_str * 3 );
        ch->multi_int = UMIN ( 500, ch->perm_int * 3 );
        ch->multi_wis = UMIN ( 500, ch->perm_wis * 3 );
        ch->multi_dex = UMIN ( 500, ch->perm_dex * 3 );
        ch->multi_con = UMIN ( 500, ch->perm_con * 3 );
        ch->multi_cha = UMIN ( 500, ch->perm_cha * 3 );
        ch->multi_lck = UMIN ( 500, ch->perm_lck * 3 );
         ch->plmod     = 2000;
	xREMOVE_BIT( ch->affected_by, AFF_SECOND_FORM );
	xSET_BIT( ch->affected_by, AFF_THIRD_FORM );
 	
         pager_printf( ch, "&PThe purple and white flames around you grow brighter and ground begins to crumble. The back of your head starts to grow out violently. Your bodily form becomes exaggerated around your arms and legs and you lean forwards under the force of your new shape. A sinister laughter escapes you as you complete your transformation.\n\r", ch->energycolour );
         act( AT_YELLOW, "$n completely changes $s body stature as $e transforms into $s third form", ch, NULL, NULL, TO_ROOM );
         }
     else
         send_to_char("You need to be second form before you can be third form.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_THIRD_FORM ) )
	{
		do_form_two( ch, "" );
	}
}
else
{
	learn_from_failure( ch, gsn_formthree );
    sprintf(buf, "&rYou fail to go to Third Form." );
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks angry.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}


/*
 *
 * Fourth Form
 *
 */

void do_form_four( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];
if ( can_use_skill( ch, number_percent(), gsn_formfour ) )
{
learn_from_success( ch, gsn_formfour );
if ( IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
	{
        ch->multi_str = UMIN ( 750, ch->perm_str * 4 );
        ch->multi_int = UMIN ( 750, ch->perm_int * 4 );
        ch->multi_wis = UMIN ( 750, ch->perm_wis * 4 );
        ch->multi_dex = UMIN ( 750, ch->perm_dex * 4 );
        ch->multi_con = UMIN ( 750, ch->perm_con * 4 );
        ch->multi_cha = UMIN ( 750, ch->perm_cha * 4 );
        ch->multi_lck = UMIN ( 750, ch->perm_lck * 4 );
	ch->plmod     = 2800;
	xREMOVE_BIT( ch->affected_by, AFF_FIFTH_FORM );
	xSET_BIT( ch->affected_by, AFF_FOURTH_FORM );
	send_to_char("You powerdown to Form Four.\n\r", ch );
        act( AT_YELLOW, "$n powers down to Form Four.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_FOURTH_FORM ) )
	{
    if ( IS_AFFECTED( ch, AFF_THIRD_FORM ) )
	   {
        ch->multi_str = UMIN ( 750, ch->perm_str * 4 );
        ch->multi_int = UMIN ( 750, ch->perm_int * 4 );
        ch->multi_wis = UMIN ( 750, ch->perm_wis * 4 );
        ch->multi_dex = UMIN ( 750, ch->perm_dex * 4 );
        ch->multi_con = UMIN ( 750, ch->perm_con * 4 );
        ch->multi_cha = UMIN ( 750, ch->perm_cha * 4 );
        ch->multi_lck = UMIN ( 750, ch->perm_lck * 4 );
	   ch->plmod     = 2800;
	xREMOVE_BIT( ch->affected_by, AFF_THIRD_FORM );
	xSET_BIT( ch->affected_by, AFF_FOURTH_FORM );

	   pager_printf( ch, "&PYour body begins to shine as your exterior begins cracking. Screaming in pain as %s &Pflames flare up around you, emerging from the many cracks in your body. A hand reaches out from your body, shattering it completely. What remains is a smaller white version of yourself complete with perfectly toned physique and an ego to match.\n\r ", ch->energycolour );
       act( AT_YELLOW, "$n powers up as $s bright ki breaches through $s skin blowing it away leaving behind a smooth and sleek form.", ch, NULL, NULL, TO_ROOM );
	   }
    else
        send_to_char("You need to be third form before you an be fourth form.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_FOURTH_FORM ) )
	{
	do_form_three( ch, "" ); return;
	}
}
else
{
	learn_from_failure( ch, gsn_formfour );
    sprintf(buf, "&rYou fail to go to Fourth Form.");
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks angry.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

/*
 *
 * Fifth Form
 *
 */


void do_form_five( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];
if ( can_use_skill( ch, number_percent(), gsn_formfive ) )
{
learn_from_success( ch, gsn_formfive );
if ( ch->plmod > 3200 )
	{
        ch->multi_str = UMIN ( 1000, ch->perm_str * 5 );
        ch->multi_int = UMIN ( 1000, ch->perm_int * 5 );
        ch->multi_wis = UMIN ( 1000, ch->perm_wis * 5 );
        ch->multi_dex = UMIN ( 1000, ch->perm_dex * 5 );
        ch->multi_con = UMIN ( 1000, ch->perm_con * 5 );
        ch->multi_cha = UMIN ( 1000, ch->perm_cha * 5 );
        ch->multi_lck = UMIN ( 1000, ch->perm_lck * 5 );
	ch->plmod     = 3200;
	send_to_char("You powerdown to Form Five.\n\r", ch );
        act( AT_YELLOW, "$n powers down to Form Five.", ch, NULL, NULL, TO_ROOM );
	}
else if ( !IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
	{
    if ( IS_AFFECTED( ch, AFF_FOURTH_FORM ) )
	   {
        ch->multi_str = UMIN ( 1000, ch->perm_str * 5 );
        ch->multi_int = UMIN ( 1000, ch->perm_int * 5 );
        ch->multi_wis = UMIN ( 1000, ch->perm_wis * 5 );
        ch->multi_dex = UMIN ( 1000, ch->perm_dex * 5 );
        ch->multi_con = UMIN ( 1000, ch->perm_con * 5 );
        ch->multi_cha = UMIN ( 1000, ch->perm_cha * 5 );
        ch->multi_lck = UMIN ( 1000, ch->perm_lck * 5 );
	ch->plmod     = 3200;
	xREMOVE_BIT( ch->affected_by, AFF_FOURTH_FORM );
	xSET_BIT( ch->affected_by, AFF_FIFTH_FORM );
	  
	   pager_printf( ch, "&PYour aura explodes around you. Purple and white flames swirl around your body as you push your forth form to its limit. You become lost in the though of the power you are gaining as your muscles grow outwards and razor sharp spikes emerge from your body, each one a leathal weapon in its own right. You stare down at you hand and marvel at the sheer volume of energy you have.\n\r", ch->energycolour );
       act( AT_YELLOW, "$n grows in height and horns grow out of $s arms and legs and you can sense $s power rise significantly", ch, NULL, NULL, TO_ROOM );
	   }
    else
        send_to_char("You need to be fourth form before you can be fifth form.\n\r", ch );
    }
else if ( IS_AFFECTED( ch, AFF_FIFTH_FORM ) )
	{
	sprintf(buf, "&GYou powerdown to Form Four.");
	pager_printf( ch, "%s\n\r", buf );
        act( AT_YELLOW, "$n powers down to Form Four.", ch, NULL, NULL, TO_ROOM );
	ch->multi_str = 150;
    	ch->multi_int = -40;
	ch->multi_wis = -25;
	ch->multi_dex = 100;
	ch->multi_con = 50;
    	ch->multi_cha = 0;
    	ch->multi_lck = 0;
	ch->plmod     = 2800;
	xREMOVE_BIT( ch->affected_by, AFF_FIFTH_FORM );
	xSET_BIT( ch->affected_by, AFF_FOURTH_FORM );
	}
}
else
{
	learn_from_failure( ch, gsn_formfive );
    sprintf(buf, "&rYou fail to go to Fifth Form.");
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n looks angry.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );  
return;
}

void do_ascension( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

if ( can_use_skill( ch, number_percent(), gsn_ascension ) )
{
learn_from_success( ch, gsn_ascension );
if ( ch->plmod > 1000 )
        {
        ch->multi_str = 75;
        ch->multi_dex = 100;
        ch->multi_con = 25;
        ch->multi_int = 125;
        ch->multi_wis = 100;
        ch->multi_cha = 40;
        ch->multi_lck = 30;
        ch->plmod     = 1000;
xSET_BIT( ch->affected_by, AFF_ASCENSION );
xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM);
xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS);
xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS);
xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS);
xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS);
        send_to_char("your wings are retracted into your back as you powerdown to your ascended form.\n\r", ch );
    act( AT_LBLUE, "$n's wings are retrated into their back as they powers down.", ch, NULL, NULL, TO_ROOM );
        }
else if ( !IS_AFFECTED( ch, AFF_ASCENSION ) )
        {
        ch->multi_str = 75;
        ch->multi_dex = 100;
        ch->multi_con = 25;
        ch->multi_int = 125;
        ch->multi_wis = 100;
        ch->multi_cha = 40;
        ch->multi_lck = 30;
        ch->plmod     = 1000;

xSET_BIT( ch->affected_by, AFF_ASCENSION );
        sprintf(buf, "&CYour %s &Cenergy flares up and the knowledge and power of you predecessors fills your mind.", ch->energycolour);
        pager_printf( ch, "%s\n\r", buf );
        sprintf(buf, "&CYour body flows with the raw power of the universe as you reach a new level level of");
        pager_printf( ch, "%s\n\r", buf );
        sprintf(buf, "&Cenlightenment and understanding.");
        pager_printf( ch, "%s\n\r", buf );
    act( AT_LBLUE, "$n's aura flares up as $s eyes show a sudden show of understanding and wisdom.", ch, NULL, NULL, TO_ROOM );
        }
else if ( IS_AFFECTED( ch, AFF_ASCENSION ) )
        {
        sprintf(buf, "&GYou powerdown to your base.");
        pager_printf( ch, "%s\n\r", buf );
        ch->multi_str = 0;
        ch->multi_dex = 0;
	ch->multi_con = 0;
        ch->multi_int = 0;
        ch->multi_wis = 0;
        ch->multi_lck = 0;
        ch->multi_cha = 0;
        ch->plmod = 100;
        xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
    act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
    }
}
else
{
        learn_from_failure( ch, gsn_ascension );
    send_to_char("&GYou stress as you cannot remember the knowledge of the gods.\n\r", ch );
    act( AT_GREEN, "$n looks annoyed as $e fails to ascend.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}

void do_wings( CHAR_DATA *ch, char *argument  )
{
char buf[MAX_STRING_LENGTH];


// first stage

if ( ch->alignment > 800 )
{
	if ( ch->plmod > 2120 )
	{
	        ch->multi_str = 10;
	        ch->multi_dex = 75;
	        ch->multi_con = 55;
	        ch->multi_int = 95;
	        ch->multi_wis = 50;
	        ch->multi_cha = 60;
	        ch->multi_lck = 40;
	        ch->plmod     = 2120;
		xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM );
	        send_to_char("You powerdown to your angelic form.\n\r", ch );
		act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_ASCENSION ) )
	{
	        ch->multi_str = 30;
	        ch->multi_dex = 75;
	        ch->multi_con = 55;
	        ch->multi_int = 75;
	        ch->multi_wis = 50;
	        ch->multi_cha = 60;
	        ch->multi_lck = 40;
	        ch->plmod     = 2120;
		xSET_BIT(ch->affected_by, AFF_4GOODWINGS);
		xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
		pager_printf( ch, "&WRealising the extent of the power that has been gained through your life of purity and selflessness you lean over forward and concentrate your energies around you back, causing four great white wings to emerge from your back to facilitate the power required to rid this Universe of its evils.\n\r"  );
		act( AT_LBLUE, "$n bends over forward and a luminous set of four wings sprout from out of $s back.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_4GOODWINGS) || IS_AFFECTED( ch, AFF_2GOODWINGS) || IS_AFFECTED( ch, AFF_2BADWINGS) || IS_AFFECTED( ch, AFF_4BADWINGS))
        {
	        sprintf(buf, "&GYour wings retreat back into your body and your power down to your ascended form.");
        	pager_printf( ch, "%s\n\r", buf );
        	ch->multi_str = 75;
	        ch->multi_dex = 100;
		ch->multi_con = 25;
	        ch->multi_int = 125;
        	ch->multi_wis = 100;
	        ch->multi_lck = 40;
        	ch->multi_cha = 30;
	        ch->plmod = 1000;
	        xSET_BIT( ch->affected_by, AFF_ASCENSION );
		xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS );
		act( AT_LBLUE, "$n pulls $s wings back in powering down back to $s ascended form.", ch, NULL, NULL, TO_ROOM );
	} 
	else
	{
	pager_printf( ch, "You need to be in your ascended form to perform this transformation.\n\r" );
	}
}

// second stage 

if( ch->alignment > 0 && ch->alignment < 801 )
{
	if ( ch->plmod > 2120 )
        {
        	ch->multi_str = 10;
	        ch->multi_dex = 75;
	        ch->multi_con = 35;
	        ch->multi_int = 65;
	        ch->multi_wis = 40;
	        ch->multi_cha = 50;
	        ch->multi_lck = 30;
	        ch->plmod     = 2020;
		xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM );
	        send_to_char("You powerdown to your angelic form.\n\r", ch );
		act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_ASCENSION ) )
        {
	        ch->multi_str = 30;
	        ch->multi_dex = 75;
	        ch->multi_con = 55;
        	ch->multi_int = 75;
	        ch->multi_wis = 50;
        	ch->multi_cha = 60;
	        ch->multi_lck = 40;
        	ch->plmod     = 2120;
		xSET_BIT(ch->affected_by, AFF_2GOODWINGS);
		xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
		pager_printf( ch, "&CShowing your worthiness you stretch your arms out wide and two feathery wings emerge from you back filling you with much additional power and glowing brightly as they do so.\n\r"  );
		act( AT_LBLUE, "$n bends over forward and a luminous set of two wings sprout from out of $s back.", ch, NULL, NULL, TO_ROOM );
        }
else if ( IS_AFFECTED( ch, AFF_4GOODWINGS) || IS_AFFECTED( ch, AFF_2GOODWINGS) || IS_AFFECTED( ch, AFF_2BADWINGS) || IS_AFFECTED( ch, AFF_4BADWINGS))
        {
	        sprintf(buf, "&GYour wings retreat back into your body and your power down to your ascended form.");
        	pager_printf( ch, "%s\n\r", buf );
        	ch->multi_str = 75;
	        ch->multi_dex = 100;
		ch->multi_con = 25;
	        ch->multi_int = 125;
        	ch->multi_wis = 100;
		ch->multi_lck = 40;
	        ch->multi_cha = 30;
        	ch->plmod = 1000;
	        xSET_BIT( ch->affected_by, AFF_ASCENSION );
		xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS );
		act( AT_LBLUE, "$n pulls $s wings back in powering down back to $s ascended form.", ch, NULL, NULL, TO_ROOM );
	}
        else
        {
        pager_printf( ch, "You need to be in your ascended form to perform this transformation.\n\r" );
        }

}

// third stage 

if ( ch->alignment > -800  &&  ch->alignment < 1 )
{
	learn_from_success( ch, gsn_wings );
	if ( ch->plmod > 2120 )
        {
        	ch->multi_str = 30;
	        ch->multi_dex = 65;
        	ch->multi_con = 55;
		ch->multi_int = 45;
	        ch->multi_wis = 50;
        	ch->multi_cha = 70;
	        ch->multi_lck = 20;
        	ch->plmod     = 2020;
		xREMOVE_BIT(ch->affected_by, AFF_SERAPHIM );
        	send_to_char("You powerdown to your angelic form.\n\r", ch );
		act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_ASCENSION ) )
        {
	        ch->multi_str = 30;
	        ch->multi_dex = 75;
	        ch->multi_con = 55;
	        ch->multi_int = 75;
	        ch->multi_wis = 50;
	        ch->multi_cha = 60;
	        ch->multi_lck = 40;
	        ch->plmod     = 2120;
		xSET_BIT(ch->affected_by, AFF_2BADWINGS);
		xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
		pager_printf( ch, "&YRealising that your life is too tarnished to rely on the powers of peace and hope you turn to the dark energies. You are surrounded by darkness as you feel your body morph and reform. When the darkness subsides you are left with two demonic wings and a taste for blood.\n\r"  );
		act( AT_LBLUE, "$n is shouded by a black fog, when it clears you see that $e has got two demonic wings.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_4GOODWINGS) || IS_AFFECTED( ch, AFF_2GOODWINGS) || IS_AFFECTED( ch, AFF_2BADWINGS) || IS_AFFECTED( ch, AFF_4BADWINGS))
        {
	        sprintf(buf, "&GYour wings retreat back into your body and your power down to your ascended form.");
        	pager_printf( ch, "%s\n\r", buf );
        	ch->multi_str = 75;
	        ch->multi_dex = 100;
		ch->multi_con = 25;
	        ch->multi_int = 125;
        	ch->multi_wis = 100;
	        ch->multi_lck = 40;
	        ch->multi_cha = 30;
        	ch->plmod = 1000;
	        xSET_BIT( ch->affected_by, AFF_ASCENSION );
		xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS );
		act( AT_LBLUE, "$n pulls $s wings back in powering down back to $s ascended form.", ch, NULL, NULL, TO_ROOM );
	}
        else
        {
        pager_printf( ch, "You need to be in your ascended form to perform this transformation.\n\r" );
        }

}
// fouth stage 

if ( ch->alignment < -801 )
{
	if ( ch->plmod > 2120 )
        {
        	ch->multi_str = 50;
	        ch->multi_dex = 65;
        	ch->multi_con = 75;
	        ch->multi_int = 55;
	        ch->multi_wis = 45;
	        ch->multi_cha = 60;
        	ch->multi_lck = 40;
        	ch->plmod     = 2120;
		xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM );
	        send_to_char("You powerdown to your angelic form.\n\r", ch );
		act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_ASCENSION ) )
        {
        	ch->multi_str = 30;
	        ch->multi_dex = 75;
        	ch->multi_con = 55;
	        ch->multi_int = 75;
	        ch->multi_wis = 50;
        	ch->multi_cha = 60;
        	ch->multi_lck = 40;
	        ch->plmod     = 2120;
		xSET_BIT(ch->affected_by, AFF_4BADWINGS);
		xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
		pager_printf( ch, "&RFeeling your presence as one with darkness itself you allow the dark energies to consume you and mould you to their preference. Four huge fleshy, bat-like wings emerge from your back as the desire to kill overpowers you.\n\r"  );
		act( AT_LBLUE, "$n bends over forward and a luminous set of four wings sprout from out of $s back.", ch, NULL, NULL, TO_ROOM );
        }
	else if ( IS_AFFECTED( ch, AFF_4GOODWINGS) || IS_AFFECTED( ch, AFF_2GOODWINGS) || IS_AFFECTED( ch, AFF_2BADWINGS) || IS_AFFECTED( ch, AFF_4BADWINGS))
        {
	        sprintf(buf, "&GYour wings retreat back into your body and your power down to your ascended form.");
        	pager_printf( ch, "%s\n\r", buf );
        	ch->multi_str = 75;
	        ch->multi_dex = 100;
		ch->multi_con = 25;
	        ch->multi_int = 125;
        	ch->multi_wis = 100;
	        ch->multi_lck = 40;
        	ch->multi_cha = 30;
	        ch->plmod = 1000;
        	xSET_BIT( ch->affected_by, AFF_ASCENSION );
		xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS );
		xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS );
		act( AT_LBLUE, "$n pulls $s wings back in powering down back to $s ascended form.", ch, NULL, NULL, TO_ROOM );
	}
        else
        {
        pager_printf( ch, "You need to be in your ascended form to perform this transformation.\n\r" );
        }

}

pl_update( ch, NULL );
return;
}



void do_seraphim( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];


if ( can_use_skill( ch, number_percent(), gsn_seraphim ) )
{
learn_from_success( ch, gsn_seraphim );
if ( ch->plmod > 3000 )
        {
        ch->multi_str = 135;
        ch->multi_dex = 250;
        ch->multi_con = 75;
        ch->multi_int = 175;
        ch->multi_wis = 150;
        ch->multi_cha = 80;
        ch->multi_lck = 60;
        ch->plmod     = 3000;
xSET_BIT(ch->affected_by, AFF_SERAPHIM);
        send_to_char("You powerdown to serpahim.\n\r", ch );
    act( AT_LBLUE, "$n powers down.", ch, NULL, NULL, TO_ROOM );
        }
else if ( !IS_AFFECTED( ch, AFF_SERAPHIM ) )
        {
if (IS_AFFECTED (ch, AFF_ASCENSION) || IS_AFFECTED( ch, AFF_4GOODWINGS) || IS_AFFECTED( ch, AFF_2GOODWINGS) || IS_AFFECTED( ch, AFF_2BADWINGS) || IS_AFFECTED(ch, AFF_4BADWINGS ))
{
ch->multi_str = 135;
        ch->multi_dex = 250;
        ch->multi_con = 75;
        ch->multi_int = 175;
        ch->multi_wis = 150;
        ch->multi_cha = 80;
        ch->multi_lck = 60;
        ch->plmod     = 3000;
xSET_BIT(ch->affected_by, AFF_SERAPHIM);
xREMOVE_BIT( ch->affected_by, AFF_4GOODWINGS);
xREMOVE_BIT( ch->affected_by, AFF_2GOODWINGS);
xREMOVE_BIT( ch->affected_by, AFF_2BADWINGS);
xREMOVE_BIT( ch->affected_by, AFF_4BADWINGS);
xREMOVE_BIT( ch->affected_by, AFF_ASCENSION );
        sprintf(buf, "&YYou suddenly feel that your physical body is of no use to you anymore");
        pager_printf( ch, "%s\n\r", buf );
sprintf(buf, "&Yand so you call upon the powers of the universe and construct a body of");
        pager_printf( ch, "%s\n\r", buf );
sprintf(buf, "&Ypure energy, resembling you previous form somewhat. You leap inside this");
        pager_printf( ch, "%s\n\r", buf );
sprintf(buf, "&Ynew form and experience the beauty of being unrestricted by you previous body.");
        pager_printf( ch, "%s\n\r", buf );
    act( AT_LBLUE, "$n constructs a body of pure energy and transfers $s consciousness into it.", ch, NULL, NULL, TO_ROOM );
        }
else 
{
send_to_char( "&GYou need to be in your ascended or angelic state to transform.", ch );
return;
}}

else if ( IS_AFFECTED( ch, AFF_SERAPHIM ) )

        {
        sprintf(buf, "&GYou summon your previous body and enter back into it powering down to your ascended form.");
        pager_printf( ch, "%s\n\r", buf );
        ch->multi_str = 75;
        ch->multi_dex = 100;
ch->multi_con = 25;
        ch->multi_int = 125;
        ch->multi_wis = 100;
        ch->multi_lck = 40;
        ch->multi_cha = 30;
        ch->plmod = 1000;
        xSET_BIT( ch->affected_by, AFF_ASCENSION );
xREMOVE_BIT( ch->affected_by, AFF_SERAPHIM );
    act( AT_LBLUE, "$n summons $s former body and reverts into it powering down back to $s ascended form.", ch, NULL, NULL, TO_ROOM );
    }
}
else if ( !IS_AFFECTED(ch, AFF_ASCENSION) )
send_to_char( "&GYou must be in your ascended state to perform this.",ch );
{
        learn_from_failure( ch, gsn_seraphim );
    send_to_char("", ch );
    act( AT_GREEN, "", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}





/*
 *
 * Mutie
 *
 *

void do_mutie( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];
int random = number_range(2, 24);

if ( can_use_skill( ch, number_percent(), gsn_mutie ) )
{
	learn_from_success( ch, gsn_mutie );
	ch->multi_str = random * 2;
	ch->multi_int = random * 2;
	ch->multi_wis = random * 3;
	ch->multi_dex = random * 3;
	ch->multi_con = random * 4;
	ch->multi_cha = random * 4;
	ch->multi_lck = random * 5;
	ch->plmod     = 100 * random;
	send_to_char("You power into another level of Mutie.\n\r", ch );
	if( !IS_AFFECTED( ch, AFF_MUTIE ) )
		xSET_BIT( ch->affected_by, AFF_MUTIE );
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
       sprintf(buf, "%s ki flies from you as you grow some new limbs and your DNA morphs.\n\r", ch->energycolour );
	act( AT_YELLOW, "Wierd limbs protrude from $e as he utilises the bizarre Mutie skill.", ch, NULL, NULL, TO_ROOM );
}
else
{
    learn_from_failure( ch, gsn_mutie );
	if( IS_AFFECTED( ch, AFF_MUTIE ) )
		xREMOVE_BIT( ch->affected_by, AFF_MUTIE );
    sprintf(buf, "&rMutie fails on you.  Miserably. And with some comical results." );
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n turns into a bat, then a bear, then a frog, then back to normal!", ch, NULL, NULL, TO_ROOM );
}
update_current(ch, NULL);
return;
}
*/
void pl_update( CHAR_DATA *ch, char *argument )
{
 update_current( ch, NULL );
 return;
/*
 if ( ch->plmod < 1 || ch->plmod > 10000 )
      {
      ch->currpl = ch->basepl;
      }
 else
      {
      ch->currpl = ( ch->basepl / 100 ) * ch->plmod;
      if ( ch->currpl < ch->basepl && ch->plmod >= 100 )
           ch->currpl = ch->basepl;
      }
 return;*/
}

void do_gamble ( CHAR_DATA *ch, char *argument )
{
 int chance;

 if ( ch->gold > 0 )
	{
	send_to_char("You toss a zeni into the air and catch it...\n\r", ch );
	chance = number_range ( 1, 10 );
	switch ( chance )
        {
        	case 1:
        	case 2:
        	case 3:
        	case 4: send_to_char("It comes down heads!  You are showered with Zeni!\n\r", ch );
              		ch->gold += get_curr_lck( ch );
	       	break;
	        case 5:
        	case 6:
        	case 7:
        	case 8:
        	case 9: send_to_char("It comes down tails!  Vegeta steals a pile of your zeni!\n\r", ch );
            		ch->gold -= 2 * ( get_curr_lck( ch ) );
           	break;
           	case 10: send_to_char("The coin lands on it's side and you lose a load of zeni! What are the odds?!\n\r", ch );
                     ch->gold = ch->gold / 2;
            break;
        }
 	act( AT_MAGIC, "$n tosses a coin...", ch, NULL, NULL, TO_ROOM );
    }
    else
        send_to_char("You need money to actually gamble it...\n\r", ch );
 if ( ch->gold < 0 )
	 ch->gold = 0;
 return;
}

/* send_scoutermap - displays the exits in a room, if the player is wearing a scouter --Gohan */
void send_scoutermap ( CHAR_DATA *ch )
{
	EXIT_DATA *pexit;
	sh_int dir[9];
	char buf[MAX_STRING_LENGTH];
	sh_int bfound=0;
	OBJ_DATA *obj_next;
	OBJ_DATA *obj_scouter;
	OBJ_DATA *obj;

	//init
	dir[0]=dir[1]=dir[2]=dir[3]=dir[4]=dir[5]=dir[6]=dir[7]=dir[8]=dir[9]=0;

	if( !ch->desc )
		return;
if ( ch->race != RACE_ANDROID )
{
	obj = ch->first_carrying;
	while( obj )
	{
		obj_next = obj->next_content;
		
		if( obj->item_type == ITEM_SCOUTER && obj->wear_loc == WEAR_EYES )
		{
			obj_scouter = obj;
			bfound = 1;
			break;
		}

		obj = obj_next;
	}

	if( !bfound )
		return;
}
	for( pexit = ch->in_room->first_exit; pexit; pexit = pexit->next )
	{
		if( pexit->to_room && pexit->vdir <= 9
		&& !IS_SET( pexit->exit_info, EX_WINDOW )
		&& !IS_SET( pexit->exit_info, EX_HIDDEN ) )
		{
			if( IS_SET( pexit->exit_info, EX_CLOSED ) || IS_SET( pexit->exit_info, EX_LOCKED ) )
			{
				dir[pexit->vdir] = 2;
			}
			else
			{
				dir[pexit->vdir] = 1;
			}
		}
	}
//	strcpy( buf, "" );
	sprintf( buf, "%s     %s     %s\n\r", ( dir[7] > 0 ? ( dir[7] == 1 ? "&D&WNW&G" : "&R# &G" ) : "&G- " ),
					      ( dir[0] > 0 ? ( dir[0] == 1 ? "&WN&G" : "&R#&G" ) : "&G|" ),
					      ( dir[6] > 0 ? ( dir[6] == 1 ? "&WNE&G" : "&R #&G" ) : "&G -" ) );
	send_to_char_color( buf, ch );
	sprintf( buf, "%s-%s--(+)--%s-%s\n\r",  ( dir[3] > 0 ? ( dir[3] == 1 ? "&WW &G" : "&R# &G" ) : "&G- " ),
						( dir[4] > 0 ? ( dir[4] == 1 ? "&Wu&G" : "&R#&G" ) : "&G-" ),
						( dir[5] > 0 ? ( dir[5] == 1 ? "&Wd&G" : "&R#&G" ) : "&G-" ),
						( dir[1] > 0 ? ( dir[1] == 1 ? "&W E&G" : "&R #&G" ) : "&G -" ) );
	send_to_char_color( buf, ch );
	sprintf( buf, "%s     %s     %s\n\r",	( dir[9] > 0 ? ( dir[9] == 1 ? "&WSW&G" : "&R# &G" ) : "&G- " ),
						( dir[2] > 0 ? ( dir[2] == 1 ? "&WS&G" : "&R#&G" ) : "&G|" ),
						( dir[8] > 0 ? ( dir[8] == 1 ? "&WSE&G" : "&R #&G" ) : "&G -" ) );
	send_to_char_color( buf, ch );

	return;
}


void do_aura( CHAR_DATA *ch, char *argument )
{
	char buf[MAX_STRING_LENGTH];
	int count;

	if( IS_NPC( ch ) )
		return;

	//argument = one_argument( argument, arg1 );
	
//	send_to_char( argument, ch );
	if( argument[0] == '\0' )
	{
		// display kicolor and available kicolors
		sprintf( buf, "&cYour current aura color is %s.\n\r\n\r", ch->energycolour );
		send_to_char_color( buf, ch );
		sprintf( buf, "&cAvailable colors:\n\r%s\t\t%s\n\r", kicolor_types[0], kicolor_types[1] );
		send_to_char_color( buf, ch );
		sprintf( buf, "%s\t%s\n\r%s\t%s\n\r", kicolor_types[2], kicolor_types[3],
							  kicolor_types[4], kicolor_types[5] );
		send_to_char_color( buf, ch );
		sprintf( buf, "%s\t\t%s\n\r%s\t%s\n\r", kicolor_types[6], kicolor_types[7],
							  kicolor_types[8], kicolor_types[9] );
		send_to_char_color( buf, ch );
		sprintf( buf, "%s\t\t%s\n\r%s\t\t%s\n\r", kicolor_types[10], kicolor_types[11],
							  kicolor_types[12], kicolor_types[13] );
		send_to_char_color( buf, ch );
		sprintf( buf, "%s\t%s\n\r", kicolor_types[14], kicolor_types[15] );
		send_to_char_color( buf, ch );
	}
	else
	{
		// set kicolor
		for( count = 0; count <=15; count++ )
		{
			if( !str_cmp( kicolor_types[count]+2, argument ) )
			{
				STRFREE( ch->energycolour );
				ch->energycolour = STRALLOC( kicolor_types[count] );
				send_to_char( "Done.\n\r", ch );
				return;
			}
		}
		send_to_char( "That is not a color.\n\r", ch );
	}

	return;
}


/* added because for some reason there is not a fly skill... --Gohan */

void do_fly( CHAR_DATA *ch, char *argument )
{

	if( IS_AFFECTED( ch, AFF_FLYING ) )
	{
		act( AT_GREEN, "You fall back to the ground.", ch, NULL, NULL, TO_CHAR );
		act( AT_GREEN, "$n falls back to the ground.", ch, NULL, NULL, TO_ROOM );
		xREMOVE_BIT( ch->affected_by, AFF_FLYING );
		return;
	}
	
	if( ch->mana < 100 )
	{
		act( AT_DGREY, "You need more energy to fly!", ch, NULL, NULL, TO_CHAR );
		return;
	}

	ch->mana -= 100;
                if ( ch->mana < 0 )
                        ch->mana = 0;


	if( can_use_skill( ch, number_percent(), gsn_fly ) )
	{
		learn_from_success( ch, gsn_fly );
		act( AT_GREEN, "You rise up into the currents of the air...", ch, NULL, NULL, TO_CHAR );
		act( AT_GREEN, "$n rises up into the currents of the air...", ch, NULL, NULL, TO_ROOM );
		xSET_BIT( ch->affected_by, AFF_FLYING );
	}
	else
	{
		learn_from_failure( ch, gsn_fly );
		act( AT_GREEN, "You attempt to fly, but fall back to the ground.", ch, NULL, NULL, TO_CHAR );
		act( AT_GREEN, "$n attempts to fly, but falls back to the ground.", ch, NULL, NULL, TO_ROOM );
	}
	
	return;
}


/* added to use for skill busy triggers 
 * added by Gohan, should be expanded by skill-creators */
/* skill creators, to add a skill here:
 * 1) assign a gsn for your skill
 * 2) make the function for your skill
 * 3) put the SET_BUSY code in your function : SET_BUSY( ch, timer, gsn )
 * 4) add cases below : case gsn_yourskill:
 * 5) remember to put break; at the end of your case
 * 6) I think that's it...
 */
void busy_trigger( CHAR_DATA *ch, bool update )
{
	
	if( !ch || ch == NULL )
	{
		bug( "busy_trigger: NULL ch" );
	}
    if( !update )
    { // busy info removed after this!
	switch( ch->busygsn )
	{
	default:
		break;
	}
    }
    else
    { // busy info not removed after this
	switch( ch->busygsn )
	{
	default:
		break;
	}
    }
	
	return;
}

void do_bounty( CHAR_DATA *ch, char *argument )
{
    char arg1 [MAX_INPUT_LENGTH];
    char arg2 [MAX_INPUT_LENGTH];
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *victim;

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
	
    
	if ( arg1[0] == '\0' || arg2[0] == '\0' )
        {
	     send_to_char( "Place a bounty on who's head?\n\rSyntax: Bounty <victim> <amount>\n\r", ch );
             return;
        }
	
        if ( ( victim = get_char_world( ch, arg1 ) ) == NULL)
	{
  	   send_to_char( "They are currently not logged in!", ch );
	   return;
        }
  
       if (IS_NPC(victim))
       {
 	send_to_char( "You cannot put a bounty on NPCs!", ch );
 	return;
       }

       if (ch->pcdata->bountytimer > 0)
       {
 	send_to_char( "It is too soon since you last put a bounty on someone's head.\n\r", ch );
 	return;
       }

	if ( ch->basepl >= victim->basepl * 5 || ch->in_room->area == victim->in_room->area)
        {
	send_to_char( "Why pay someone when you can do it yourself so easily?\n\r", ch );
	return;
        }

	if ( is_number( arg2 ) )
        {
	int amount;
	amount   = atoi(arg2);
        if (ch->gold < amount * 2)
        {
		send_to_char( "You don't have that much zeni!", ch );
		return;
        }

       if ( victim->pcdata->bounty >= 1000000 || amount <= 0)
       {
 	send_to_char( "They are worth too much already.\n\r", ch );
 	return;
       }

	SET_BIT( ch->pcdata->flags, PCFLAG_DEADLY );
	ch->gold -= amount * 2;
	victim->pcdata->bounty +=amount;
	sprintf( buf, "&RYou are charged a 100%% tax on the bounty, making it cost %i zeni.\n\r", (amount * 2) );
	pager_printf(ch, "%s",buf );
	sprintf( buf, "&RYou have placed a %d zeni bounty on %s.\n\r%s now has a bounty of %d.\n\r",amount,PERS( victim, ch ),PERS( victim, ch ),victim->pcdata->bounty );
	pager_printf(ch, "%s",buf );
	if ( victim->level < 50 )
             adjust_hiscore( "bounty", victim, victim->pcdata->bounty );
	sprintf( buf, "&Y&W[&RBOUNTY&W]&C%s &Rputs a bounty of &Y%d &RZeni on &C%s&R!", ch->name, amount, PERS( victim, ch ) );
	echo_to_all( AT_BLINK, buf, ECHOTAR_ALL );
	sprintf( buf, "\n\r&W%s is now worth %d Zeni!",PERS( victim, ch ), victim->pcdata->bounty );
	echo_to_all( AT_IMMORT, buf, ECHOTAR_ALL );
	WAIT_STATE( ch, 5 * PULSE_PER_SECOND );
	return;	
 }
}

// By Vegeta, 24/2/03

void do_bountylist ( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *vch;
 CHAR_DATA *vch_next;

 if ( IS_NPC(ch) )
	return;
 
 pager_printf( ch, "&Y---------------&RBOUNTIES&Y---------------\n\r" );
 for ( vch = first_char; vch; vch = vch_next )
 {
	vch_next = vch->next;
	if ( !IS_NPC(vch) && vch->pcdata->bounty > 0 && !IS_IMMORTAL(vch))
		pager_printf( ch, "&Y&W%-20s %8d zeni\n\r", vch->name, vch->pcdata->bounty );
 }
return;
}

void do_eye( CHAR_DATA *ch, char *argument )
{
 char arg1[MAX_INPUT_LENGTH];

 if ( IS_NPC(ch) )
	return;

 if ( ch->pcdata->eyedone != 0 )
	{
	send_to_char("You can't do that!\n\r", ch );
	return;
	}

 argument = one_argument( argument, arg1 );

 if ( arg1[0] == '\0' || arg1==NULL )
	{
	send_to_char("Options:\n\r", ch );
	send_to_char("Blue  Cyan  Red  Yellow  Green  Black  Grey\n\r", ch );
	return;
	}
 
 if ( arg1[0] == 'R' || arg1[0] == 'r' )
	{
	send_to_char( "Red set as eye colour\n\r", ch );
	ch->pcdata->eye = STRALLOC ("red" );
	ch->pcdata->eyedone = 1;
	return;
	}

 if ( arg1[0] == 'C' || arg1[0] == 'c' )
	{
	send_to_char( "Light blue set as eye colour\n\r", ch );
	ch->pcdata->eye = STRALLOC ("light blue");
	ch->pcdata->eyedone = 1;
	return;
	}

 if ( arg1[0] == 'Y' || arg1[0] == 'y' )
	{
	send_to_char( "Yellow set as eye colour\n\r", ch );
	ch->pcdata->eye = STRALLOC ("yellow");
	ch->pcdata->eyedone = 1;
	return;
	}

 if ( arg1[0] == 'G' || arg1[0] == 'g' )
	{
	if ( arg1 [3] == 'e' )
		{
		send_to_char( "Green set as eye colour\n\r", ch );
		ch->pcdata->eye = STRALLOC ("green");
		ch->pcdata->eyedone = 1;
		}
	else
		{
		send_to_char( "Grey set as eye colour\n\r", ch );
		ch->pcdata->eye = STRALLOC ("grey");
		ch->pcdata->eyedone = 1;
		}
	return;
	}

 if ( arg1[0] == 'B' || arg1[0] == 'b' )
	{
	if ( arg1 [2] == 'u' )
		{
		send_to_char( "Blue set as eye colour\n\r", ch );
		ch->pcdata->eye = STRALLOC ( "blue" );
		ch->pcdata->eyedone = 1;
		}
	else
		{
		send_to_char( "Black set as eye colour\n\r", ch );
		ch->pcdata->eye = STRALLOC ("black");
		ch->pcdata->eyedone = 1;
		}
	return;
	}

return;
}

void do_hair( CHAR_DATA *ch, char *argument )
{
 char arg1[MAX_INPUT_LENGTH];

 if ( IS_NPC(ch) )
	return;


 argument = one_argument( argument, arg1 );

 if ( arg1[0] == '\0' || arg1==NULL )
 {
	send_to_char("Options:\n\r", ch );
	send_to_char("Grey\t\t Blue\t\t Blonde\t\t Black\t\t Brown\t\t Dark Brown\n\r", ch );
	send_to_char("Lilac\t\t Platinum\t Pink\t\t Red\t\t Ginger\t\t Green\n\r", ch );
	return;
 }

 if ( !str_cmp( arg1, "Blue" ) )
 {
      send_to_char( "Blue set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC ("blue");
 }
 if ( !str_cmp( arg1, "Blonde" ) )
 {
      send_to_char( "Blonde set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("blonde");
 }
 if ( !str_cmp( arg1, "Black" ) )
 {
      send_to_char( "Black set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("black");
 }
 if ( !str_cmp( arg1, "Brown" ) )
 {
      send_to_char( "Brown set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("brown");
 }
 if ( !str_cmp( arg1, "Dark" ) )
 {
      send_to_char( "Dark Brown set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("dark brown");
 }
 if ( !str_cmp( arg1, "Platinum" ) )
 {
      send_to_char( "Platinum set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("platinum");
 }
 if ( !str_cmp( arg1, "Pink" ) )
 {
      send_to_char( "Pink set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("pink");
 }
 if ( !str_cmp( arg1, "Red" ) )
 {
      send_to_char( "Red set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("red");
 }
 if ( !str_cmp( arg1, "Ginger" ) )
 {
      send_to_char( "Ginger set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("ginger");
 }
 if ( !str_cmp( arg1, "Green" ) )
 {
      send_to_char( "Green set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("green");
 }
 if ( !str_cmp( arg1, "Grey" ) )
 {
      send_to_char( "Grey set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("grey");
 }
 if ( !str_cmp( arg1, "Lilac" ) )
 {
      send_to_char( "Lilac set as hair colour\n\r", ch );
      ch->pcdata->hairdone = 1;
      ch->pcdata->hair = STRALLOC("lilac");
 }

return;
}

void do_height ( CHAR_DATA *ch, char *argument )
{
 int number;
 char arg[MAX_INPUT_LENGTH];
 
 if ( IS_NPC(ch) )
	return;

 argument = one_argument( argument, arg );
 number = atoi(arg);

 if ( number < 36 && ch->pcdata->age > 6 && ch->race != 10 )
	{
	send_to_char("You can't be that short!\n\r", ch );
	return;
	}

 if ( number > 120  )
	{
	send_to_char("A little tall, don't you think?\n\r", ch );
	return;
	}
	
 ch->height = number;
 pager_printf( ch, "Height set at %i.\n\r", ch->height );
 return;
}


void do_age ( CHAR_DATA *ch, char *argument )
{
 int number;
 char arg[MAX_INPUT_LENGTH];
 
 if ( IS_NPC(ch) )
	return;

 argument = one_argument( argument, arg );
 number = atoi(arg);

 if ( number < 4 )
	{
	send_to_char("A little young to be so strong!\n\r", ch );
	return;
	}

 if ( number > 1000  )
	{
	send_to_char("Most people can't live that long...\n\r", ch );
	return;
	}
	
 ch->pcdata->age = number;
 pager_printf( ch, "Age set at %i.\n\r", ch->pcdata->age );
 return;
}


void do_weight ( CHAR_DATA *ch, char *argument )
{
 int number;
 char arg[MAX_INPUT_LENGTH];
 
 if ( IS_NPC(ch) )
	return;

 argument = one_argument( argument, arg );
 number = atoi(arg);

 if ( number < 30 )
	{
	send_to_char("What are you made of, paper?\n\r", ch );
	return;
	}

 if ( number > 1000  )
	{
	send_to_char("So that'd be an infinity squared sized waist, huh?\n\r", ch );
	return;
	}
	
 ch->weight = number;
 pager_printf( ch, "Weight set at %i.\n\r", ch->weight );
 return;
}

void do_suppress( CHAR_DATA *ch, char *argument )
{
 int percent;
 if (( ch->race == RACE_WIZARD  || ch->race == RACE_DEMON ) )
 {
        send_to_char( "Huh?", ch );
        return;
 }

 if ( ch->race == RACE_DEMON )
 {
	send_to_char( "The only thing demons do with their pl is increase it!\n\r", ch );
	return;
 }

 if ( !argument || argument[0] == '\0')
 {
	send_to_char( "You need to specify a suppress percent...\n\r", ch );
	return;
 }
 if ( !str_cmp(ch->in_room->area->name, "Hell" ) || ch->in_room->vnum == 6 )
 {
	send_to_char( "Don't think you can get out of here THAT easily!\n\r", ch );
	return;
 }
 percent = atoi(argument);
 if ( percent >= 100 )
 {
	send_to_char( "It's called powering up, you fool.\n\r", ch );
	return;
 }
 if ( percent < 1 )
 {
	send_to_char( "I think not.\n\r", ch );
	return; 
 }
 do_powerdown( ch, "" );
 ch->plmod = percent;
 update_current( ch, NULL );
 send_to_char( "You concentrate and lower your powerlevel.\n\r", ch );
 act( AT_LBLUE, "$n takes a deep breath and you feel $s powerlevel drop.\n\r", ch, NULL, NULL, TO_CANSEE );
 return;
}


int kidefend( CHAR_DATA *ch, CHAR_DATA *victim, int dam, int dt )
{
 int percent = number_percent();

 if ( victim->style == STYLE_DEADANGLE )
   percent -= 20;

if ( IS_AFFECTED( ch, AFF_CHARGING ) )
{
  return dam;
}


 if ( !can_defend( dt ) || IS_NPC(victim) || ch->race == RACE_WIZARD || dt == gsn_combo || dt == gsn_tailattack || dt == gsn_shockwavepunch || dt == gsn_criticalhit )
       return dam;

if ( ( IS_NPC(victim) && number_range( 1, 10 ) <= 3 ) || ( !IS_NPC(victim) && victim->pcdata->set[gsn_kishield] == TRUE && ch->mana >= 1000 ) )
{
  ch->mana -= 1000;
  if (ch->mana < 0 )
        ch->mana = 0;

  if ( can_use_skill( victim, percent, gsn_kishield ) )
  {
      learn_from_success( victim, gsn_kishield );
      pager_printf( victim, "&PYou explode in a furious aura of %s ki and wait for %s's attack to come closer.  It reaches you, and detonates in your huge aura! &B[&G%s&B]\n\r", victim->energycolour, ch->name, "75% absorbed" );
      pager_printf( ch, "&P%s explodes in a furious aura of %s ki as your attack homes in... when it arrives, %s aura absorbs some of it! &B[&G%s&B]\n\r", PERS( victim, ch ), victim->energycolour, victim->sex == 0? "its" : victim->sex == 1 ? "his" : "her", "75% absorbed" );
      act( AT_MAGIC, "$N's aura absorbs some of $n's attack!\n\r", ch, NULL, victim, TO_NOTVICT );
      dam = dam * 0.25;
      return dam;
  }
  else
  {
	  learn_from_failure( victim, gsn_kishield );
  	  pager_printf( victim, "&pYou explode in a furious aura of %s ki and wait for %s's attack to come closer.  It reaches you, but goes right through your aura, which only adds to it's power! [10%% added]\n\r", victim->energycolour, ch->name, skill_table[dt]->name );
	  pager_printf( ch, "&p%s explodes in a furious aura of %s ki and waits for your attack to come close.  It reaches %s, but just carries on through %s aura, which actually adds to it's power! [10%% added]\n\r", PERS( victim, ch ), victim->energycolour, ch->sex== 0? "it" : victim->sex == 1 ? "him" : "her", victim->sex == 0? "its" : victim->sex == 1 ? "his" : "her" );
      act( AT_MAGIC, "$N explodes in a ferocious aura of ki, but fails to break the flow of power coming from $n's attack!\n\r", ch, NULL, victim, TO_NOTVICT );
      dam += ( dam / 10 );
     }
    }
 if ( ( IS_NPC(victim) && ( number_range( 1, 10 ) <= 2 || victim->pIndexData->vnum == 11201 || victim->pIndexData->vnum == 11202 ))||(!IS_NPC(victim) && victim->pcdata->set[gsn_kireturn] == TRUE && ch->mana > 500 ))
    {
     ch->mana -= 500;
                if ( ch->mana < 0 )
                        ch->mana = 0;

     if ( can_use_skill( victim,percent, gsn_kireturn ) && dam < victim->hit )
     {
	    learn_from_success( victim, gsn_kireturn );	
        pager_printf( victim, "&PGetting desperate, you fire off a large energyball at %s's attack, hoping to blast it back at %s! You succeed as it arcs up and down onto %s! [77.5%% attack power lost]\n\r", ch->name, ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her", ch->sex == 0? "it" : ch->sex == 1 ? "him" : "her" );
        pager_printf( ch, "&P%s suddenly launches a huge energyball at your attack, sending it right back! [77.5%% attack power lost]\n\r", PERS( victim, ch ) );
        act( AT_MAGIC, "$N launches an energyball at $n's attack, and sends it back at $m!\n\r", ch, NULL, victim, TO_NOTVICT );
        damage( victim, ch, URANGE( 0, dam * 0.225, victim->hit - 1), dt );
	   dam = 0;	
        return dam;
     }
     else
     {
	    learn_from_failure( victim, gsn_kireturn );	
            pager_printf( victim, "&pYou launch a large energy ball at %s's attack, but it just gets absorbed into it!! &B[&G%s&B]\n\r", ch->name, "17.5% added" );
            pager_printf( ch, "&p%s launches an energy ball at your attack, but it just increases it's power even more! &B[&G%s&B]\n\r", PERS( victim, ch ), skill_table[dt]->name, ch->sex == 0? "it" : ch->sex == 1 ? "he" : "she", "17.5% power added" );
            act( AT_MAGIC, "$N launches an energyball at $n's attack, but just increases its power!\n\r", ch, NULL, victim, TO_NOTVICT );
            dam *= 1.175;
     }
    }
 if (( IS_NPC(victim) && number_range( 1, 10 ) <= 2) || (!IS_NPC(victim) && victim->pcdata->set[gsn_kideflect] == TRUE && ch->mana > 250  ))
    {
     ch->mana -= 250;
                if ( ch->mana < 0 )
                        ch->mana = 0;

     if ( can_use_skill( victim, percent, gsn_kideflect ) )
     {
	    learn_from_success( victim, gsn_kideflect );	
            pager_printf( victim, "&PYou move to a better posture and wait as %s's attack homes in on you... Finally it arrives and you sweep it away with one arm! [Attack Power Negated]\n\r", ch->name );
            pager_printf( ch, "&P%s moves to a better posture and waits as your attack homes in... Finally it arrives, and %s sweeps it away with one arm! [Attack Power Negated]\n\r", PERS( victim, ch ), victim->sex == 0? "it" : victim->sex == 1 ? "he" : "she" );
            act( AT_MAGIC, "$N sweeps away $n's attack with one arm!\n\r", ch, NULL, victim, TO_NOTVICT );
            dam = 0;
            return dam;
     }
     else
     {
	    learn_from_failure( victim, gsn_kideflect );	
            pager_printf( victim, "&pYou move to a better posture and wait as %s's attack homes in on you... Finally it arrives and you try sweep it away... but fail!\n\r", ch->name );
            pager_printf( ch, "&p%s moves to a better posture and waits as your attack homes in... Finally it arrives, but %s fails to redirect it!\n\r", PERS( victim, ch ), victim->sex == 0? "it" : victim->sex == 1 ? "he" : "she" );
            act( AT_MAGIC, "$N sweeps at $n's attack with one arm, but fails to deflect it!\n\r", ch, NULL, victim, TO_NOTVICT );     
    }
   }
 if (( IS_NPC(victim) && number_range( 1, 10 ) <= 2) || (!IS_NPC(victim) && victim->pcdata->set[gsn_kibrace] == TRUE && ch->mana > 100 ))
 {
     ch->mana -= 100;
                if ( ch->mana < 0 )
                        ch->mana = 0;

     if ( can_use_skill( victim, percent, gsn_kibrace ) )
     {
	    learn_from_success( victim, gsn_kibrace );	
            pager_printf( victim, "&PYou curse under your breath and fold your arms in front of your face, and dig in to brace against the attack! &B[&G%s&B]\n\r", "33% absorbed");
            pager_printf( ch, "&P%s swears quietly, and folds %s arms in front of %s face to try brace for the attack! &B[&G%s&B]\n\r", PERS( victim, ch ), victim->sex == 0? "its" : victim->sex == 1 ? "his" : "her", victim->sex == 0? "its" : victim->sex == 1 ? "his" : "her", "33% absorbed" );
            act( AT_MAGIC, "$N braces against $n's attack, reducing it's damage!\n\r", ch, NULL, victim, TO_NOTVICT );
            dam *= 0.66;
	    return dam;
     }
     else
     {
	    learn_from_failure( victim, gsn_kibrace );	
            pager_printf( victim, "&pYou quickly try to bring up your arms up against the attack, but you merely get caught off balance when it hits!\n\r" );
            pager_printf( ch, "&p%s quickly tries to bring %s arms up against the attack, but merely gets caught off balance when it hits!!\n\r", PERS( victim, ch ), victim->sex == 0? "its" : victim->sex == 1 ? "his" : "her" );
            act( AT_MAGIC, "$N braces against $n's attack, but only gets caught off guard when it does hit $M!!\n\r", ch, NULL, victim, TO_NOTVICT );
     }
 }

 if( victim->hit - dam > 5000 && !IS_NPC(victim) 
  && can_use_skill( victim, percent, gsn_regeneration ) && number_range( 1, 5 ) == 3
  && victim->pcdata->set[gsn_regeneration])
 {
    int heal = number_range( victim->pcdata->learned[gsn_regeneration] * 4, victim->pcdata->learned[gsn_regeneration] * 20 );
    learn_from_success( victim, gsn_regeneration );
    pager_printf( ch, "&GAlthough your ki attack really wounds %s, it just regrows the damaged limbs.\n\r", victim->name );
    pager_printf( victim, "&GYou regrow the limbs damaged by %s's attack. [HP restored]\n\r", ch->name );
    victim->hit += heal;
 } 
return dam;
}

bool can_defend( int dt )
{

 if ( skill_table[dt]->defendable == FALSE )
    return FALSE;
 if ( !skill_table[dt]->name )
    return FALSE;
 if ( dt < 0 || dt > 500 )
    return FALSE;
 return TRUE;
}

void do_kaioken( CHAR_DATA *ch, char *argument )
{
char buf[MAX_STRING_LENGTH];
char arg1[MAX_INPUT_LENGTH];
int mod, argnum;

argument = one_argument(argument, arg1);
    if ( IS_AFFECTED(ch, AFF_LSSJ) )
    {
        send_to_char( "YOU ARE A GOD!  SUCH THINGS ARE BEYOND YOU!\n\r", ch );
        return;
    }

if ( !str_cmp( arg1, "burst" ) )
{
    argument = one_argument( argument, arg1 );
    kaioken_burst( ch, arg1 );
    return;
}

argnum = atoi(arg1);

if ( ch->plmod > 1000 )
{
	send_to_char( "What a step down...\n\r", ch );
	return;
}

if ( argnum < 2 || argnum > 10)
{
        send_to_char("Kaioken range is from 2 to 10.\n\r", ch );
        return;
}

if ( IS_AFFECTED( ch, AFF_KAIOKEN ) && ch->plmod == argnum * 100 )
{
        sprintf(buf, "&GYou powerdown to your base and your mystic flames subside.");
        pager_printf( ch, "%s\n\r", buf );
        ch->multi_str = 0;
        ch->multi_wis = 0;
        ch->multi_dex = 0;
        ch->multi_con = 0;
        ch->multi_int = 0;
        ch->multi_cha = 0;
        ch->multi_lck = 0;
        ch->plmod = 100;
        xREMOVE_BIT( ch->affected_by, AFF_KAIOKEN );
        act( AT_MAGIC, "$n powers down.", ch, NULL, NULL, TO_ROOM );
	return;
}

if ( can_use_skill( ch, number_percent(), gsn_kaioken ) )
{
learn_from_success( ch, gsn_kaioken );
mod = argnum;
if ( ch->plmod > ( mod * 100 ) )
{
      	do_powerdown(ch, "" );
        ch->multi_str = mod * 3;
        ch->multi_wis = mod * 3;
        ch->multi_dex = mod * 3;
        ch->multi_con = mod * 3;
        ch->multi_int = mod * 2;
        ch->plmod     = mod * 100;
        pager_printf(ch, "&RYou scream 'KAIOKEN TIMES %i!!' and glow red, as your muscles expand and the ground trembles!\n\r", mod );
        act( AT_BLOOD, "$n screams 'KAIOKEN ATTACK!!'and bursts into a mystical firey red aura.\n\r", ch, NULL, NULL, TO_ROOM );
        xSET_BIT( ch->affected_by, AFF_KAIOKEN );
}
else if ( ch->plmod < mod * 100 )
{
        ch->multi_str = mod * 3;
        ch->multi_wis = mod * 3;
        ch->multi_dex = mod * 3;
        ch->multi_con = mod * 3;
        ch->multi_int = mod * 2;
        ch->plmod     = mod * 100;
        pager_printf(ch, "&RYou scream 'KAIOKEN TIMES %i!!' and glow red, as your muscles expand and the ground trembles!\n\r", mod );
        act( AT_BLOOD, "$n screams 'KAIOKEN ATTACK!!'and bursts into a mystical firey red aura.\n\r", ch, NULL, NULL, TO_ROOM );
        xSET_BIT( ch->affected_by, AFF_KAIOKEN );
}
}
else
{
        learn_from_failure( ch, gsn_kaioken );
    sprintf(buf, "&RYou try to use kaioken, but fail!\n\r");
    pager_printf( ch, "%s\n\r", buf);
    act( AT_GREEN, "$n fails to go Kaioken.", ch, NULL, NULL, TO_ROOM );
}
pl_update( ch, NULL );
return;
}

/*void destroy_limb( CHAR_DATA *ch, CHAR_DATA *victim, int dam )
{
 int limb = 0;

 if ( dam <= 15 || IS_NPC(victim) )
	return;

 for( ; ; )
 {
	limb = number_range(LIMB_TENTACLE, LIMB_TAIL );
	if ( ch->race != RA_ICER && ch->race != RA_BIOANDROID && limb == LIMB_TAIL )
		continue;
	if ( ch->race != RA_DEMON && limb == LIMB_TAIL )
		continue;
	break;
 }

 if ( ch->limb[limb] == TRUE )
 {
  	switch( limb )
 	{
 		case LIMB_TENTACLE:
			pager_printf( ch, "You grab ahold of %s's tentacle and rip it right off!!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s grabs ahold of your tentacle and rips it right off!!\n\r", ch->name );
			victim->limb_wis += 50;
			victim->limb_int += 50;
			victim->hit -= 25;
			victim->limb[LIMB_TENTACLE] = FALSE;
			break;
 		case LIMB_HEAD:
			pager_printf( ch, "You hit %s in the head and hear a horrible crunch!!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s hits you in the head and suddenly you feel very dizzy!\n\r", ch->name );
			victim->limb_wis += 50;
			victim->limb_int += 50;
			victim->hit -= 25;
			victim->limb[LIMB_HEAD] = FALSE;
			break;
 		case LIMB_LARM:
			pager_printf( ch, "You hit %s in the left arm and hear a horrible snapping sound!!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s hits you in the left arm, breaking it clean in two!\n\r", ch->name );
			victim->limb_str += 25;
			victim->hit -= 25;
			victim->limb[LIMB_LARM] = FALSE;
			break;
 		case LIMB_RARM:
			pager_printf( ch, "You hit %s in the right arm and hear a horrible snapping sound!!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s hits you in the right arm, breaking it clean in two!\n\r", ch->name );
			victim->limb_str += 25;
			victim->hit -= 25;
			victim->limb[LIMB_RARM] = FALSE;
			break;
 		case LIMB_BODY:
			pager_printf( ch, "You hit %s in the body and break several ribs!!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s hits you in the chest, breaking several limbs!\n\r", ch->name );
			victim->limb_con += 50;
			victim->hit -= 25;
			victim->limb[LIMB_BODY] = FALSE;
			break;
 		case LIMB_LEGS:
			pager_printf( ch, "You hit %s in the leg and hear a disgusting crack!\n\r", PERS( victim, ch ));
 			pager_printf( victim, "%s hits you in the leg, and you hear a horrible crack!\n\r", ch->name );
			victim->limb_dex += 50;
			victim->limb[LIMB_LEGS] = FALSE;
			break;
 		case LIMB_TAIL:
			pager_printf( ch, "You grab %s's tail and rip it right off!\n\r", PERS( victim, ch ) );
 			pager_printf( victim, "%s rips your tail right off!\n\r", ch->name 
);
			victim->limb_dex += 50;
			victim->hit -= 25;
			victim->limb[LIMB_TAIL] = FALSE;
			break;

 	}
 }
return;
}

void do_treatment( CHAR_DATA *ch, char *argument )
{
 int number = 0;

 if ( !argument  || !is_number(argument) )
 {
	pager_printf(ch, "&CT&cr&Ce&ca&Ct&cm&Ce&cn&Ct&c:\n\r" );
	number = LIMB_TENTACLE;
	if ( ch->race != RA_DEMON )
		number++;
	for ( ; ; )
	{
		pager_printf( ch, "%20.20s     %10s  %i\n\r", get_limb(number), ch->limb[number] == TRUE ? "&GNormal&C" : "&RBroken&C", number);
		number++;
		if ( ch->race != RA_ICER && ch->race != RA_BIOANDROID && number > LIMB_LEGS )
			break;
		if ( number <= LIMB_TAIL )
			continue;
		else
			break;	
	}
	pager_printf( ch, "Use TREATMENT <LIMB NUMBER> to get treated!\n\r" );
	return;
 }

 if ( !IS_SET(ch->in_room->room_flags, ROOM_HOSPITAL ) )
 {
	pager_printf( ch, "Go to a hospital first!\n\r" );
	return;
 }

 number = atoi(argument);

 if ( number < 1 || number > LIMB_TAIL )
 {
	pager_printf(ch, "No such limb.\n\r" );
	return;
 }

 if ( ch->limb[number] == TRUE 
 || ( number == LIMB_TAIL && ch->race != RA_ICER && ch->race != RA_BIOANDROID )  
 || ( number == LIMB_TENTACLE && ch->race != RA_DEMON ) )
 {
	pager_printf( ch, "You have no injury there.\n\r" );
	return;
 }

 ch->limb[number] = TRUE;
 switch(number)
 {
	case LIMB_TAIL: ch->limb_dex -= 50; break;
	case LIMB_LEGS: ch->limb_dex -= 50; break;
	case LIMB_BODY: ch->limb_con -= 50; break;
	case LIMB_LARM: ch->limb_str -= 25; break;
	case LIMB_RARM: ch->limb_str -= 25; break;
	case LIMB_HEAD: ch->limb_int -= 50; ch->limb_wis -= 50; break;
	case LIMB_TENTACLE: ch->limb_int -= 50; ch->limb_wis -= 50; break;
 }
 pager_printf( ch, "%s healed.\n\r", get_limb(number) );
 return;
}

char * get_limb( int number )
{
 switch( number )
 {
 case LIMB_TAIL: return "TAIL";
 case LIMB_LEGS: return "LEGS";
 case LIMB_BODY: return "BODY";
 case LIMB_LARM: return "L. ARM";
 case LIMB_RARM: return "R. ARM";
 case LIMB_HEAD: return "HEAD";
 case LIMB_TENTACLE: return "TENTACLE";
 }
 return ( "NONE" );
}*/

void do_pk( CHAR_DATA *ch, char * argument )
{
 char arg[MAX_INPUT_LENGTH];

 argument = one_argument( argument, arg );

 if ( IS_NPC(ch) || IS_SET(ch->pcdata->flags, PCFLAG_MANIAC) )
 {
    send_to_char( "Huh?\n\r", ch );
    return;
 }
 if ( AreaAttack )
 {
    send_to_char( "An area attack is occuring.  Wait for it to end.\n\r", ch );
    return;
 }
 if ( who_fighting( ch ) != NULL )
 {
    send_to_char( "IN A FIGHT?!\n\r", ch );
    return;
 }
 if ( get_timer(ch, TIMER_PKER) > 0 )
 {
    send_to_char( "You PKed too recently.\n\r", ch );
    return;
 }

 if ( arg[0] == '\0' || arg == NULL )
 {
    if ( ch->pktimer2 > 0 )
    pager_printf( ch, "You have %d minutes and %d seconds on your pktimer.\n\r", ch->pktimer2 / 60, ch->pktimer2 % 60 );
    return;
 }

 if ( arg[0] != 'o' && arg[0] != 'O' )
 {
    send_to_char( "Syntax: PK <On/Off>\n\r", ch );
    return;
 }

 if ( arg[1] == 'n' || arg[1] == 'N' )
 {
    if ( xIS_SET( ch->act, PLR_KILLER ) )
    {
       send_to_char( "You are already set as NPK.\n\r", ch );
       return;
    }
    if ( ch->pktimer2 > 0 )
    {
       pager_printf( ch, "You must wait another %d minutes before you can turn on your NPK flag.\n\r", ch->pktimer / 60 );
       return;
    }
    pager_printf( ch, "&WYou are now safe.\n\r" );
    xSET_BIT( ch->act, PLR_KILLER );
    return;
 }

 if ( ( arg[1] == 'f' || arg[1] == 'F' ) && ( arg[2] == 'f' || arg[2] == 'F' ) )
 {
    if ( !xIS_SET( ch->act, PLR_KILLER ) )
    {
       send_to_char( "You are not set as NPK anyway.\n\r", ch );
       return;
    }
    pager_printf( ch, "&YYou are no longer set as NPK.\n\r", ch->pktimer );
    xREMOVE_BIT( ch->act, PLR_KILLER );
    return;
 }
do_pk( ch, "z" );
return;
}

void do_evildude( CHAR_DATA  *ch, char *argument )
{
CHAR_DATA *vch;
CHAR_DATA *target = NULL;
unsigned long long pl = 0;

if ( !IS_NPC(ch) )
	return;

for ( vch = first_char; vch; vch = vch->next )
{
	if ( vch->basepl > pl )
	{
		target = vch;
		pl = vch->basepl;
	}
}

if ( pl > pow( 10, 9 ) * 5 )
{
	do_mpgoto( ch, target->name );
	do_murder( ch, target->name );
}
return;
}


/*
 *
 * Meta
 *
 */

void do_metafighter( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

 if ( ch->alignment > -300 )
 {
    pager_printf( ch, "You're not evil enough.\n\r" );
    return;
 }

 if ( ch->plmod == 3300  )
 {
    ch->multi_str =  0;
    ch->multi_dex =  0;
    ch->multi_con =  0;
    ch->multi_int =  0;
    ch->multi_wis =  0;
    ch->multi_cha =  0;
    ch->multi_lck =  0;
    ch->plmod     =  0;
    pager_printf( ch, "You power out of meta, and spikes retract into your body.\n\r" );
    act( AT_PINK, "$n drops out of meta level.", ch, NULL, NULL, TO_CANSEE );
 }
 else
 {
 if ( ch->plmod > 100 )
 {
    send_to_char( "This transformation can only be achieved when you are completely powered down.\n\r", ch );
    return;
 }
    ch->multi_str =  1200;
    ch->multi_dex =  1200;
    ch->multi_con =  500;
    ch->multi_int =  400;
    ch->multi_wis =  400;
    ch->multi_cha =  750;
    ch->multi_lck =  500;
    ch->plmod     =  3200;
    sprintf(buf, "&pYou feel a chill all the way to your bones as you begin to snarl, doubling inside, your mane of spikes transforming from sleek to terrible; boney spike covered plates slide from beneath your flesh and your face becomes covered in a protective organic mask, leaving only two cold, red eyes to glare at the world in utter hate!" );
    pager_printf( ch, "%s\n\r", buf );
    act( AT_PINK, "&P$n doubles in size, $s flesh bursting as thick boke spikes extend from $s joints, $s face melding together to form a mask.", ch, NULL, NULL, TO_CANSEE );
 }

pl_update( ch, NULL );
return;
}

void do_unlockpotential( CHAR_DATA *ch, char *argument  )
{
 char buf[MAX_STRING_LENGTH];

 if ( ch->plmod > 100 )
 {
    send_to_char( "This transformation can only be achieved when you are completely powered down.\n\r", ch );
    return;
 }

 if ( ch->plmod == 1250 )
 {
    ch->multi_str =  0;
    ch->multi_dex =  0;
    ch->multi_con =  0;
    ch->multi_int =  0;
    ch->multi_wis =  0;
    ch->multi_cha =  0;
    ch->multi_lck =  0;
    ch->plmod     =  0;
    pager_printf( ch, "You drop down and suppress your potential.\n\r" );
    act( AT_WHITE, "$n looks a little more scared.", ch, NULL, NULL, TO_CANSEE );
 }
 else
 {
    ch->multi_str =  200;
    ch->multi_dex =  150;
    ch->multi_con =  200;
    ch->multi_int =  0;
    ch->multi_wis =  0;
    ch->multi_cha =  0;
    ch->multi_lck =  100;
    ch->plmod     =  1250;
    sprintf(buf, "&WYou begin to flare up in a brilliant aura of all the tremendous potential energy inside you.  Your muscles bulk up and expand slightly when your aura explodes outwards and strips the ground away from under your feet!" );
    pager_printf( ch, "%s\n\r", buf );
    act( AT_WHITE, "$n begins to howl and become slightly more focused as the potential energy inside $m is unlocked.  Now $e looks like $e means buisness!", ch, NULL, NULL, TO_CANSEE );
 }

pl_update( ch, NULL );
return;
}


void do_ssj5( CHAR_DATA *ch, char *argument  )
{

if ( !IS_NPC(ch) && !ch->pcdata->monkey )
{
        send_to_char("You must be confusing your belt for a tail...\n\r", ch );
        return;
}

 if ( !IS_AFFECTED( ch, AFF_SSJ4 ) )
 {
    send_to_char( "This transformation can only be achieved when you are SSJ4.\n\r", ch );
    return;
 }

 if ( IS_AFFECTED( ch, AFF_SSJ5 ))
 {
	do_ssj4(ch, "" );
 }
 else
 {
    ch->multi_str =  1000;
    ch->multi_dex =  1000;
    ch->multi_con =  1000;
    ch->multi_int =  1000;
    ch->multi_wis =  1000;
    ch->multi_cha =  1000;
    ch->multi_lck =  1000;
    ch->plmod     =  6000;
    pager_printf( ch, "&WYou stare at your hands, realising that even Super Saiyan 4 is not enough for you. Bringing up your latent powers, you prepare to unleash unimaginable forces. With your energy building, you begin to scream, your yellow aura blasting outwards. In a great blast your aura turns silver, and your hair begins to re-grow. With an almighty crack of thunder your hair turns silver, and your muscles ripple." );
    pager_printf( ch, "&W With a final roar that shatters the very fabric of the universe itself, your power reaches its absolute maximum, and you dig down, ready for anything the universe can throw at you...\n\r" );
    xSET_BIT( ch->affected_by, AFF_SSJ5 );
    xREMOVE_BIT( ch->affected_by, AFF_SSJ4 );
    act( AT_WHITE, "$n turns into a super saiyan FIVE! Time to run your ass off!", ch, NULL, NULL, TO_CANSEE );
 }

pl_update( ch, NULL );
return;
}

void do_xbuster( CHAR_DATA *ch, char * argument )
{
 int mod   = ( ( get_curr_int(ch) / 16 ) * 100 );
 int vnum  =     1;                                 /* Vnum of the XBuster chip */
 
 if ( IS_NPC(ch) || IS_SET(ch->pcdata->flags, PCFLAG_XBUSTER) || ( ch->race != RACE_ANDROID && !IS_IMMORTAL(ch) ) || !is_wearing( ch, vnum ))
 {
      send_to_char( "Huh?", ch);
      return;
 }
 
 ch->plmod = mod;
 ch->multi_str += 1000;
 ch->multi_int += 1000;
 ch->multi_wis += 1000;
 ch->multi_dex += 1000;
 ch->multi_con += 1000;
 ch->multi_cha += 1000;
 ch->multi_lck += 1000;
 SET_BIT( ch->pcdata->flags, PCFLAG_XBUSTER );
 update_current( ch, NULL );
 send_to_char( "&GY&Yo&Gu &Yf&Ge&Ye&Gl &Yy&Go&Yu&Gr &Yp&Gr&Yo&Gg&Yr&Ga&Ym&Gm&Yi&Gn&Yg &Gb&Ye&Gg&Yi&Gn &Yt&Go &Yc&Go&Yr&Gr&Yu&Gp&Yt &Ga&Ys &Gt&Yh&Ge &RX-BUSTER &Yv&Gi&Yr&Gu&Ys &Gt&Ya&Gk&Ye&Gs &Yo&Gv&Ye&Gr &Yy&Go&Yu&G.&Y.&G.", ch );
 act( AT_GREEN, "$n suddenly has a crazed look in $s eye, not long after installing some small chip...", ch, NULL, NULL, TO_CANSEE );
}


/* The 'Train' command.  Allows outdoor training without gtrain or duel facilities */

#define TRAIN_JUMP         0
#define TRAIN_HIJUMP       1
#define TRAIN_LEAP         2
#define TRAIN_FLY          3
#define TRAIN_KICK         4
#define TRAIN_FASTKICK     5
#define TRAIN_SPINKICK     6
#define TRAIN_HEAVYKICK    7
#define TRAIN_PUNCH        8
#define TRAIN_FASTPUNCH    9
#define TRAIN_HEAVYPUNCH  10
#define TRAIN_POSE        11
#define TRAIN_METEOR	  12
#define TRAIN_SPLIT	  13
#define TRAIN_TRI		  14
#define TRAIN_MULTI	  15
#define TRAIN_KIBLAST	  16
#define TRAIN_KIBEAM	  17
#define TRAIN_TRANSFORMATION  18
#define TRAIN_MAX		19

void do_train ( CHAR_DATA *ch, char * argument )
{
 char arg[7][MAX_INPUT_LENGTH];
 char cmd[MAX_INPUT_LENGTH];
 double gain = 0;
 int score = 0, ctr;
 int commands[TRAIN_MAX];
 int bonus = 0;
 PLANET_DATA *planet = get_planet( ch->in_room->area->planet );
 int ctr2 =0;

if ( ch->in_room->vnum == 3198 )
{
	turbotrain(ch, argument);
	return;
}

for ( ; ; )
{
commands[ctr2] = 0;
ctr2++;
if ( ctr2 == TRAIN_MAX )
	break;
}

 if ( !planet )
 {
	pager_printf( ch, "Planet not found!!!! Tell Yami where you saw this message.\n\r" );
	return;
 }

 ctr = 0;
 for ( ; ; )
 {
     argument = one_argument( argument, arg[ctr] );
     ctr++;
     if ( ctr > 7 )
        break;
 }
 
 if ( arg[0] == '\0' )
 {
    pager_printf( ch, "&pYou must first input your training commands.\n\rSyntax: Train <command1> <command2> ... <command8>\n\rEg: Train Jump Kick Punch.\n\r" );
    return;
 }

 if ( !IS_NPC( ch ) && ch->rps < 5 )
 {
    pager_printf( ch, "You need 5 roleplay points to use this ability.\n\r" );
    return;
 }
 
 ctr = -1;
 for ( ; ; )
 {
    ctr++;
    sprintf( cmd, arg[ctr] );
 
    if ( cmd==NULL || cmd[0] == '\0' || ctr > 7 )
       break;
       
    gain += bonus;
    if ( !str_cmp( cmd, "Jump" ) )
    {
       commands[TRAIN_JUMP]++;
       score++;
       gain += 2 * planet->gravity;
       act ( AT_ACTION, "$n jumps as high into the air as $e can muster.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou jump as high into the air as you can muster.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Hijump" ) )
    {
       commands[TRAIN_HIJUMP]++;
       score++;
       gain += 2.75 * planet->gravity;
       act ( AT_ACTION, "$n jumps high into the air, releasing some ki to go that bit further.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou jump high into the air, releasing ki to maximise your jump.\n\r" );
       continue;
    }    
    if ( !str_cmp( cmd, "Leap" ) )
    {
       commands[TRAIN_LEAP]++;
       score += 2;
       gain += 2.5 * planet->gravity;
       act ( AT_ACTION, "$n jumps high into the air, bringing $s arms back gracefully.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou leap into the air, swinging back your arms in a graceful arc.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Fly" ) )
    {
       if ( commands[TRAIN_FLY] > 0 )
       {
          pager_printf( ch, "&RConfused by trying to fly whilst flying already, you lose concentration and end your session.\n\r" );
          score -= 6;
          break;
       }
       commands[TRAIN_FLY]++;
       bonus += 2;
       score += 3;
       gain += 4 * planet->gravity;
       act ( AT_ACTION, "$n flies up to continue the rest of $s training session in the air.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou fly up, keen to continue this training session in the air.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Kick" ) )
    {
       commands[TRAIN_KICK]++;
       score += 1;
       gain += planet->gravity;
       act ( AT_ACTION, "$n kicks up into the air with great power.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou kick out hard at an imaginary Saibaman, hitting him in the imaginary chest.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Fastkick" ) )
    {
       commands[TRAIN_FASTKICK]++;
       score += 2;
       gain += 2 * planet->gravity;
       act ( AT_ACTION, "$n's leg flicks up and returns to it's original position.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou flick out your leg and pull it back as fast as you can.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Spinkick" ) )
    {
       commands[TRAIN_SPINKICK]++;
       score += 3;
       gain += 3 * planet->gravity;
       act ( AT_ACTION, "$n kicks whilst spinning round, looking like some kind of tornado.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou twist your torso right whilst kicking, pulling off a 360 degree spin kick.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Heavykick" ) )
    {
       commands[TRAIN_HEAVYKICK]++;
       score += 4;
       gain += 5 * planet->gravity;
       act ( AT_ACTION, "$n throws all his energy into a huge kick.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou throw all of your energy into an enormous kick.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Punch" ) )
    {
       commands[TRAIN_PUNCH]++;
       score += 1;
       gain += 1.6 * planet->gravity;
       act ( AT_ACTION, "$n thrusts $s fist out and punches the air.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou thrust your fist out and punch the air.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Fastpunch" ) )
    {
       commands[TRAIN_FASTPUNCH]++;
       score += 2;
       gain += 2.35 * planet->gravity;
       act ( AT_ACTION, "$n's punches the air as quickly as $e can.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou punch the air as fast as you can.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Heavypunch" ) )
    {
       commands[TRAIN_HEAVYPUNCH]++;
       score += 3;
       gain += 3.2 * planet->gravity;
       act ( AT_ACTION, "$n glows a little as $e punches the air as hard as $e can.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou punch the air with all your might.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Pose" ) )
    {
       commands[TRAIN_POSE]++;
	 if ( ctr == 7 )
	 {
		gain *= 10;
		score += 15;
	 }
	bonus += 15;
       score += 5;
       gain += 1 * planet->gravity;
       act ( AT_ACTION, "$n strikes a fetching pose and flashes you a smile!", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou strike a fetching pose and flash everyone a smile.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Meteor" ) )
    {
       commands[TRAIN_METEOR]++;
       score += 8 / commands[TRAIN_METEOR];
       gain += ( 15 * planet->gravity ) / commands[TRAIN_METEOR];
       act ( AT_ACTION, "$n glows brightly with $s energy and arcs up and over himself.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou glow bright %s&R and arc up and over yourself in a 360 degree spin.\n\r", ch->energycolour );
       continue;
    }
    if ( !str_cmp( cmd, "Splitform" ) )
    {
       commands[TRAIN_SPLIT]++;
       score += 3;
       gain += 2 * planet->gravity;
       act ( AT_ACTION, "$n brings both hands to $s forehead and splits in two!", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou put both hands on your forehead and split in two.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Triform" ) )
    {
       commands[TRAIN_TRI]++;
       score += 6;
       gain += 4 * planet->gravity;
       act ( AT_ACTION, "$n brings both hands to $s forehead and splits in three!", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou put both hands on your forehead and split in three.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Multiform" ) )
    {
       commands[TRAIN_MULTI]++;
       score += 10;
       gain += 8 * planet->gravity;
       act ( AT_ACTION, "$n brings both hands to $s forehead and splits in four!", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou put both hands on your forehead and split in four.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Kiblast" ) )
    {
       commands[TRAIN_KIBLAST]++;
       score += 2;
       gain += 2.5 * planet->gravity;
       act ( AT_ACTION, "$n fires a kiblast into the air and tries to avoid hitting $mself with it.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou fire a kiblast into the air and try to avoid hitting it.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Kibeam" ) )
    {
       commands[TRAIN_KIBEAM]++;
       score += 3;
       gain += 4 * planet->gravity;
       act ( AT_ACTION, "$n fires a kibeam into the air and tries to guide it in a spiral.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou fire a kibeam into the air and try to guide it in a spiral.\n\r" );
       continue;
    }
    if ( !str_cmp( cmd, "Transformation" ) )
    {
       if ( commands[TRAIN_TRANSFORMATION] > 0 )
       {
          pager_printf( ch, "&RYou push your form as far as it can go!\n\r" );
          bonus += 5;
          break;
       }
       commands[TRAIN_TRANSFORMATION]++;
	 bonus += 5;
       act ( AT_ACTION, "$n powers up to the highest level of $s transformation.", ch, NULL, NULL, TO_CANSEE );
       pager_printf( ch, "&RYou power up as high as you can and try and hold your power for this training session.\n\r" );
       continue;
    }
    pager_printf( ch, "You call that a training command?!\n\r" );
    break;
 }
if ( gain > 0 )
	ch->rps -= 5;
gain *= ( sqrt( sqrt ( ch->currpl ) ) + score );
act ( AT_ACTION, "$n finishes $s training session, and looks fairly pleased with $mself.", 
ch, NULL, NULL, TO_CANSEE );
pager_printf( ch, "&YYou end your training session.\n\r&YYou accumilate &R%s&Y powerlevel and score &B%d&Y points, with a bonus of &R%d&Y.\n\r", format_pl( gain ), score , bonus );
ch->basepl += gain;
 if ( !IS_NPC(ch ) )
     ch->pcdata->pl_today += gain;
update_current( ch, NULL );
return;
}

void turbotrain( CHAR_DATA *ch, char * argument )
{
 int sn = 0;

 if ( IS_NPC(ch) )
	return;

 for ( sn = 0; sn < top_sn; sn++ )
 {
	if ( !str_cmp( argument, skill_table[sn]->name ) )
		break;
 }

 if ( sn >= top_sn - 1 )
 {
	pager_printf( ch, "A voice booms out: 'Hey kid! Try a skill that actually EXISTS!'\n\r" );
	return;
 }

 if ( ch->pcdata->learned[sn] < 10 )
 {
	pager_printf( ch, "A voice booms out: 'Hey kid! I ain't some n00b trainer! Try a skill you know about!'\n\r" );
	return;
 }

 if ( ch->gold < 250000 )
 {
	pager_printf( ch, "A voice booms out: 'Hey kid! Come back when you got some cash!'\n\r" );
	return;
 }

 if ( ch->pcdata->learned[sn] >= skill_table[sn]->race_level[ch->race] )
 {
	pager_printf( ch, "A voice booms out: 'Hey kid! You know how to do that almost flawlessly already!'\n\r" );
	return;
 }

 ch->gold -= 250000;
 ch->pcdata->learned[sn]+= 2;
 pager_printf( ch, "A bright flash of light blinds you for a second as you are bombarded with radiation!'\n\r" );
 return;
}

void do_sense( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 
 if ( IS_NPC( ch ) || !argument || !str_cmp( argument, "Help" ) )
 {
    pager_printf( ch, "&YSyntax: Sense <Target Name>\n\r" );
    return;
 }
 
 victim = get_char_world( ch, argument );
 if ( !victim || victim->race == RACE_ANDROID )
 {
    pager_printf( ch, "&WYou can't sense them anywhere.\n\r" );
    return;    
 }
 if ( can_use_skill( ch, number_percent(), gsn_sense ) )
 {
    learn_from_success( ch, gsn_sense );
    if ( ch->pcdata->learned[gsn_sense] >= 50 )
    {
       pager_printf( ch, "&WYou sense a powerlevel of about %s.\n\r", IS_NPC(ch) ? format_pl( victim->pIndexData->basepl ) : format_pl( ( victim->currpl ) ) );
       act( AT_WHITE, "$n closes $s eyes briefly.", ch, NULL, NULL, TO_CANSEE );
       return;
    }
    else
    {
        if ( victim->in_room->vnum != ch->in_room->vnum )
        {
             pager_printf( ch, "They are not close enough for you to sense.\n\r" );
             return;
        }
       pager_printf( ch, "&WYou sense a powerlevel of about %s.\n\r", format_pl( ( victim->currpl / 1000 ) * 1000 ) );
       act( AT_WHITE, "$n closes $s eyes briefly.", ch, NULL, NULL, TO_CANSEE );
       return;
    }
 }
 else
 {
   learn_from_failure( ch, gsn_sense );
   pager_printf( ch, "&WYou cannot sense them anywhere.\n\r" );
   return;
 }
}

void do_menu ( CHAR_DATA *ch, char *argument )
{
 if ( ch->race != RACE_DEMON )
 {
    pager_printf( ch, "Huh?\n\r" );
    return;
 }
 
 pager_printf( ch, "&c-=-=-=-=-&PYou have absorbed so far&c-=-=-=-=-=-=\n\r" );
 pager_printf( ch, "&c|%-3d&C Mystics              %-3d&Y Super Saiyans &c|\n\r", ch->absorbs[0], ch->absorbs[1] );
 pager_printf( ch, "&c|%-3d&z Fused Beings         %-3d&G Super Nameks  &c|\n\r", ch->absorbs[2], ch->absorbs[3] );
 pager_printf( ch, "&c|%-3d&p Evil Demons          %-3d&P Good Demons   &c|\n\r", ch->absorbs[4], ch->absorbs[5] );
 pager_printf( ch, "&c|%-3d&W Androids             %-3d&B Others        &c|\n\r", ch->absorbs[7], ch->absorbs[6] ); 
 pager_printf( ch, "&c-=-=-=-=-=-=-=-=-=-=&P**&c=-=-=-=-=-=-=-=-=-=-=-=\n\r" ); 

}


/*
 *  Fused Supers
 */

void do_super( CHAR_DATA *ch, char *argument  )
{

 if ( can_use_skill( ch, number_percent(), gsn_super ) )
 {
       	ch->multi_str = 500;
    	ch->multi_int = 500;
    	ch->multi_wis = 500;
    	ch->multi_dex = 500;
    	ch->multi_con = 500;
    	ch->multi_cha = 500;
    	ch->multi_lck = 500;
    	ch->plmod     = 2700;
	xSET_BIT( ch->affected_by, AFF_SUPER );
	pager_printf( ch, "&YEnergy begins to build up inside you, and you feel the power of not only yourself, but also your fusion partner pushing out from inside you... All of a sudden, you are engulfed in an enormous, sparkly yelow aura, and your hair turns a shocking golden blonde colour!\n\r" );
        act( AT_YELLOW, "$n looks a little ill, then explodes in an enormous sparkly golden aura as $s hair goes golden blonde.", ch, NULL, NULL, TO_ROOM );
 }
 else
 {
    learn_from_failure( ch, gsn_super );
    send_to_char("You scream and your muscles bulk up, but quickly return to normal.\n\r", ch );
    act( AT_YELLOW, "$n's muscles bulk up, but return to normal quickly.", ch, NULL, NULL, TO_ROOM );
 }
 pl_update( ch, NULL );  
 return;
}

void do_kili( CHAR_DATA *ch, char * argument )
{
 if ( IS_NPC( ch ) ) return;
 pager_printf( ch, "&CKili data:\n\r" );
 pager_printf( ch, "&c-------------------\n\r" );
 pager_printf( ch, "&CTotal Kili&Y:   &c    %s\n\r", num_punct( KILI( ch ) ) );
 pager_printf( ch, "&CRoleplay Kili&Y:&c    %s\n\r", num_punct( ch->rps ) );
 pager_printf( ch, "&CMission Kili&Y: &c    %s\n\r", num_punct( ch->pcdata->missionkili ) );
 pager_printf( ch, "&CPeace Kili&Y:   &c    %s\n\r", num_punct( ch->pcdata->peacekili ) );
 pager_printf( ch, "&CHonour Kili&Y:  &c    %s\n\r", num_punct( ch->honourkili ) );
 pager_printf( ch, "&CLoyalty Kili&Y: &c    %s\n\r", num_punct( ch->pcdata->loyalty ) );
}

void do_dig( CHAR_DATA *ch, char * argument )
{
   OBJ_DATA *worm;
   if ( is_wearing( ch, 69 ) || IS_NPC(ch) )
   {
	   send_to_char( "You dig, looking for treasure.\n\r", ch );
	   rprog_dig_trigger( ch, ch->in_room );
	   act( AT_GREY, "$n digs for treasure.", ch, NULL, NULL, TO_CANSEE );
	   if ( IS_NPC(ch) )
		do_drop(ch, "All" );
	   if ( number_range( 1, 20 ) == 10 )
	   {
		worm = create_object( get_obj_index(45), 100 );
		ch_printf( ch, "You unearth a worm.  It is lying on the ground now.\n\r" );
		obj_to_room( worm, ch->in_room );
	   }
   }
   else
   {
	   send_to_char( "You don't have a shovel equipped, genius...\n\r",ch );
	   return;
   }
  
}

void do_sensepl( CHAR_DATA *ch, char *argument )
{
  unsigned long long Pl;    // Assigned as the pl to sense for.
  CHAR_DATA *mob;           // Not mobs, but candidates.

  if ( !argument || argument[0] == '\n' || argument == NULL )  // Default to their current powerlevel.
  {
	Pl = ch->currpl;
    }
  else
  {
	Pl = atof( argument );
  }
  pager_printf( ch, "Searching for entities around %s...\n\r", format_pl( Pl ) );
  if ( can_use_skill( ch, number_percent(), gsn_sensepl ) )
  {
	  for ( mob = first_char; mob; mob = mob->next )
	  {
		if ( !mob )
			break;
		if ( ( mob->currpl < Pl * 0.8 || mob->currpl > Pl * 1.2 ) || mob->race == RACE_ANDROID || xIS_SET(mob->act, ACT_PACIFIST))
			continue;
		pager_printf( ch, "%s%-35s - &C%sPL\n\r", mob->in_room->area->planet==ch->in_room->area->planet ? "&G" : "&R", PERS( mob, ch ), format_pl(mob->currpl ) );
		learn_from_failure( ch, gsn_sensepl );   // Deliberate failure not success, Yami
	  }
  	  pager_printf( ch, "&WCharacters in &Ggreen &Ware on this planet.  Characters in &Rred&W are not.\n\r" );
  }
  else
  {
	pager_printf( ch, "You fail to sense any powerlevels.\n\r" );
	learn_from_failure( ch, gsn_sensepl );
	return;
  }
}


void do_virus ( CHAR_DATA *ch, char * argument )
{
     char buf[MAX_STRING_LENGTH];
     CHAR_DATA *target;

     if ( ( !IS_IMMORTAL( ch ) && infected != ch ) || IS_NPC( ch ) )
     {
          send_to_char( "You don't have the virus.\n\r", ch );
          if ( infected )
             pager_printf( ch, "%s does.\n\r", infected->name );
          return;
     }

     if ( IS_IMMORTAL( ch ) )
     {
          CHAR_DATA *target;
          if ( infected != NULL )
          {
             pager_printf( ch, "%s is already infected.\n\r", infected->name );
             return;
          }
          for ( target = first_char; target; target = target->next )
          {
              if ( !str_cmp( target->name, argument ) )
                 break;
          }
          if ( !target || str_cmp( target->name, argument ) || IS_IMMORTAL( target ) || IS_NPC( target ))
          {
             send_to_char( "They are not playing.\n\r", ch );
             return;
          }
          send_to_char( "Virus mode started!", ch );
          infected = target;
          sprintf( buf, "%s contracts the Viral Heart Disease!  Use 'VIRUS <target>' to pass it on!", target->name );
          talk_info( AT_PINK, buf );
          send_to_char( "You have the Viral Heart Disease!\n\r", infected );
          return;
     }

     target = get_char_room( ch, argument );
     if ( !target || str_cmp( target->name, argument ) || IS_IMMORTAL( target ) || IS_NPC( target ))
     {
          send_to_char( "They are not here.\n\r", ch );
          return;
     }
     infected = target;
     send_to_char( "You have the Viral Heart Disease!\n\r", infected );
     sprintf( buf, "Damn! Now I have the disease!" );
     do_chat ( infected, buf );
     return;
}


void do_reincarnate( CHAR_DATA *ch, char *argument )
{
 char arg1[MAX_INPUT_LENGTH];
 char arg2[MAX_INPUT_LENGTH];
 char arg3[MAX_INPUT_LENGTH];
 char arg4[MAX_INPUT_LENGTH];
 int  race;
 bool picked = FALSE;

 if ( IS_NPC(ch) || IS_IMMORTAL(ch) )
 {
    send_to_char( "What do you want that for?\n\r", ch );
    return;
 }

 argument = one_argument( argument, arg1 );
 argument = one_argument( argument, arg2 );
 argument = one_argument( argument, arg3 );
 argument = one_argument( argument, arg4 );

 if ( arg1==NULL || arg2==NULL || arg3==NULL || arg4==NULL )
 {
    send_to_char( "Syntax: Reincarnate <Your name> <New Race> <New Race Again> Yes\n\r", ch );
    return;
 }

 if ( str_cmp( ch->name, arg1 ) )
 {
    send_to_char( "Syntax: Reincarnate <Your name> <New Race> <New Race Again> Yes\n\r", ch );
    return;
 }

 if ( !str_cmp( arg4, "Yes" ) )
 {
    send_to_char( "Syntax: Reincarnate <Your name> <New Race> <New Race Again> Yes\n\r", ch );
    return;
 }

 if ( !str_cmp( arg2, "Saiyan" ) && !str_cmp( arg3, "Saiyan" ) )
 {
      race = RACE_SAIYAN;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Human" ) && !str_cmp( arg3, "Human" ) )
 {
      race = RACE_HUMAN;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Halfbreed" ) && !str_cmp( arg3, "Halfbreed" ) 
)
 {
      race = RACE_HALFBREED;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Tuffle" ) && !str_cmp( arg3, "Tuffle" ) )
 {
      race = RACE_TUFFLE;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Icer" ) && !str_cmp( arg3, "Icer" ) )
 {
      race = RACE_ICER;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Android" ) && !str_cmp( arg3, "Android" ) )
 {
      race = RACE_ANDROID;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Bioandroid" ) && !str_cmp( arg3, "Bioandroid" ) )
 {
      race = RACE_BIOANDROID;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Namek" ) && !str_cmp( arg3, "Namek" ) )
 {
      race = RACE_NAMEK;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Mutant" ) && !str_cmp( arg3, "Mutant" ) )
 {
      race = RACE_MUTANT;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Kaio" ) && !str_cmp( arg3, "Kaio" ) )
 {
      race = RACE_KAI;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Wizard" ) && !str_cmp( arg3, "Wizard" ) )
 {
      race = RACE_WIZARD;
      picked = TRUE;
 }
 else if ( !str_cmp( arg2, "Demon" ) && !str_cmp( arg3, "Demon" ) )
 {
      race = RACE_DEMON;
      picked = TRUE;
 }
 else
 {
      send_to_char( "Pick a valid race.\n\r", ch );
      return;
 }

 if ( race == ch->race )
 {
      send_to_char( "That is already your race.\n\r", ch );
      return;
 }

 send_to_char( "You slit your throat in a mighty ritual, only to come back as...\n\r", ch );
 pager_printf( ch, "a %s!\n\r", arg2 );
 ch->race = race;
 do_remove( ch, "All" );
 update_aris( ch );
 ch->plmod = 100;
 ch->basepl *= 0.8;
 ch->currpl = ch->basepl;
 ch->perm_str *= 0.8;
 ch->perm_int *= 0.8;
 ch->perm_wis *= 0.8;
 ch->perm_dex *= 0.8;
 ch->perm_con *= 0.8;
 ch->perm_cha *= 0.8;
 ch->perm_lck *= 0.8;
 ch->gold *= 0.8;
 if ( ch->basepl < 500 )
 {
      ch->basepl = 500;
      update_current( ch, NULL );
 }
 save_char_obj( ch );
 return;
}


bool is_holding( CHAR_DATA *ch, int vnum )
{
 OBJ_DATA *obj;
 
 for ( obj = ch->first_carrying; obj; obj = obj->next_content )
 {
        if ( obj->pIndexData->vnum == vnum )
		return TRUE;
 }
 return FALSE;
}


void do_wish( CHAR_DATA *ch, char *argument )
{
 char buf[MAX_STRING_LENGTH];
 char buf2[MAX_STRING_LENGTH];
 OBJ_DATA *obj;
 int  cnt = 0;

 if ( !argument || !str_cmp( argument, "list" ) || IS_NPC(ch) ) 
 {
	do_help( ch, "wishlist" );
	return;
 }

 if ( !is_holding( ch, 3000 ) || !is_holding( ch, 3001 ) || !is_holding( ch, 3002 ) ||
      !is_holding( ch, 3003 ) || !is_holding( ch, 3004 ) || !is_holding( ch, 3005 ) || !is_holding( ch, 3006 ) )
 {
	send_to_char( "You need all seven dragonballs to do that.\n\r", ch );
	return;
 }

 sprintf( buf, "%s wishes for ", ch->name );

 switch ( atoi( argument ) )
 {
	default:
		send_to_char( "Select a wish from 1 to 9.\n\r", ch );
		return;
	case 1:
		ch->basepl = ch->basepl * 1.1;
		ch->currpl = ( ch->basepl / 100 ) * ch->plmod;
		strcat( buf, "Soul Boosting!" );
		break;
	case 2:
		if ( ch->alignment >= 0 )
		    ch->alignment = 1000;
		else
		    ch->alignment = -1000;
		strcat( buf, "a Legendary Reputation!" );
		break;
	case 3:
		ch->perm_str += 300;
		ch->perm_int += 300;
		ch->perm_wis += 300;
		ch->perm_dex += 300;
		ch->perm_con += 300;
		ch->perm_cha += 300;
		ch->perm_lck += 300;
		strcat( buf, "Bonuses to the Physical Form!" );
		break;
	case 4:
	        sprintf( buf2, "%s reincarnate", ch->pcdata->bestowments );
	        DISPOSE( ch->pcdata->bestowments );
	        ch->pcdata->bestowments = str_dup( buf2 );
		strcat( buf, "The Ability to Return from the Dead!" );
		break;
	case 5:
		ch->perm_cha += 600;
		strcat( buf, "A Magnetic Personality!" );
		break;
	case 6:
		ch->perm_dex += 600;
		strcat( buf, "Faster Reflexes than Gilford!" );
		break;
	case 7:
		ch->perm_lck += 600;
		strcat( buf, "More Luck Than Hercule!" );
		break;
	case 8:
		ch->pcdata->missionkili = 500;
                strcat( buf, "To Get Out Of This Damn Recession!" );
                break;
	case 9:
		ch->gold += 10000000;
                strcat( buf, "Hercule's Millions!" );
                break;
 }
 talk_info( AT_PINK, buf );

 while ( cnt++ <= 7 )
 {
	sprintf( buf, "%dstar", cnt );
	obj = get_obj_carry( ch, buf );
	if( obj )
	{
		separate_obj( obj );
		obj_from_char( obj );
		extract_obj( obj );
	}
 }

}


void do_treatment( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 victim = get_char_room( ch, argument );

 if ( IS_NPC( ch ) || !argument )
 {
     send_to_char( "Please specify your target\n\r", ch );
     return;
 }
 if ( who_fighting(ch) != NULL || IS_SET(ch->in_room->room_flags, ROOM_GTRAIN ))
 {
     send_to_char( "And you plan to be able to stay still and concentrate for that long do you?\n\r",ch);
     return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ) )
 {
     send_to_char( "Go to the hospital instead of healing in the duelling grounds.\n\r", ch );
     return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_HOSPITAL ) )
 {
     send_to_char( "You're in a hospital already.  Be patient.\n\r", ch );
     return;
 }
 if ( !victim )
 {
     send_to_char( "That person is not here.\n\r",ch);
     return;
 }
 if ( IS_AFFECTED(victim, AFF_LSSJ ) )
 {
     send_to_char( "You're stupid, not suicidal.\n\r", ch );
     return;
 }
 if ( who_fighting(victim) != NULL || IS_NPC(victim))
 {
     send_to_char( "They're a little preoccupied at the moment.\n\r",ch);
     return;
 }

 if ( victim->hit == (victim->max_hit) )
 {
     if (victim == ch)
     {
	send_to_char( "You are not in need of medical attention.\n\r",ch);
	return;
     }
     else
     {
	send_to_char( "They are not in need of medical attention.\n\r",ch);
	return;
     }
 }


 if ( can_use_skill( ch, number_percent(), gsn_treatment ) )
 {
    learn_from_success( ch, gsn_treatment );
    if (victim == ch)
    {
        pager_printf( ch,"&CYou place a hand on your wounds and begin to use your energies to close them up.\n\r" );
        victim->hit += (get_curr_int(ch)/2);
	if (victim->hit > (victim->max_hit))
		victim->hit = (victim->max_hit);
	act( AT_WHITE, "$n places $s hand on $s wounds and begins to heal them.", ch, NULL, NULL, TO_CANSEE );
        return;
    }
    else
    {

	pager_printf( ch,"&CYou place a hand on %s's wounds and begin to use your energies to close them up.\n\r", PERS(victim,ch) );
	pager_printf( victim,"&C%s places a hand on your wounds and begins to use %s energies to close them up.\n\r", ch->name, ch->sex == 2? "her" : "his" );
	victim->hit += (get_curr_int(ch)/2);
        if (victim->hit > ( victim->max_hit))
		victim->hit = (victim->max_hit);
        act( AT_WHITE, "$n places $s hand on $N's wounds and begins to heal them.", ch, NULL, victim, TO_CANSEE );        
	return;
    }
 }
 else
 {
	learn_from_failure( ch, gsn_treatment );
	if (ch == victim)
	{
		send_to_char( "You desperately try to summon the energy needed to heal yourself, but you fail.\n\r",ch);
		return;
	}
	else
	{
		pager_printf(ch, "You desperately try to summon the energy needed to heal %s, but you fail.\n\r", PERS(victim,ch));
		pager_printf(victim, "%s desperately tries to summon the energy needed to heal you, but fails.\n\r", PERS(ch,victim));
		return;
	}
 }
}

void do_imptreatment( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 victim = get_char_room( ch, argument );

 if ( IS_NPC( ch ) )
 {
     send_to_char( "Please specify your target\n\r", ch );
     return;
 }
 if ( !argument )
 	victim = ch;
 if ( who_fighting(ch) != NULL || IS_SET(ch->in_room->room_flags, ROOM_GTRAIN ))
 {
     send_to_char( "And you plan to be able to stay still and concentrate for that long do you?\n\r",ch);
     return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ) )
 {
     send_to_char( "Go to the hospital instead of healing in the duelling grounds.\n\r", ch );
     return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_HOSPITAL ) )
 {
     send_to_char( "You're in a hospital already.  Be patient.\n\r", ch );
     return;
 }
 if ( ch->mana < ch->max_mana / 3 )
 {
     send_to_char( "You need a third of your full energy to do that.\n\r", ch );
     return;
 }
 if ( !victim )
 {
     send_to_char( "That person is not here.\n\r",ch);
     return;
 }
 if ( IS_AFFECTED(victim, AFF_LSSJ ) )
 {
     send_to_char( "You're stupid, not suicidal.\n\r", ch );
     return;
 }
 if ( who_fighting(victim) != NULL || IS_NPC(victim))
 {
     send_to_char( "They're a little preoccupied at the moment.\n\r",ch);
     return;
 }

 if ( victim->hit == (victim->max_hit) )
 {
     if (victim == ch)
     {
	send_to_char( "You are not in need of medical attention.\n\r",ch);
	return;
     }
     else
     {
	send_to_char( "They are not in need of medical attention.\n\r",ch);
	return;
     }
 }


 if ( can_use_skill( ch, number_percent(), gsn_imptreatment ) )
 {
    learn_from_success( ch, gsn_imptreatment );
    if (victim == ch)
    {
        pager_printf( ch,"&RYou place your hands on your chest and exhale deeply as a %s&R glow appears around your wounds.  You gasp in pain as you feel them heal up.  You sigh heavily as you relax again.\n\r", ch->energycolour );
        victim->hit += 100*(get_curr_int(ch)/2);
	if (victim->hit > (victim->max_hit))
		victim->hit = (victim->max_hit);
	ch->mana -= ch->max_mana / 3;
	act( AT_WHITE, "$n places $s hand on $s chest and casts a healing incantation.", ch, NULL, NULL, TO_CANSEE );
        return;
    }
    else
    {

	pager_printf( ch,"&CYou place both hands on %s's wounds and begin to use your energies to close them up.\n\r", PERS(victim,ch) );
	pager_printf( victim,"&C%s places both hands on your wounds and begins to use %s powerful mystic energies to close them up.\n\r", ch->name, ch->sex == 2? "her" : "his" );
	victim->hit += 25*(get_curr_int(ch)/2);
        if (victim->hit > ( victim->max_hit))
		victim->hit = (victim->max_hit);
        act( AT_WHITE, "$n places $s hand on $N's wounds and begins to heal them.", ch, NULL, victim, TO_CANSEE );        
	return;
    }
 }
 else
 {
	learn_from_failure( ch, gsn_imptreatment );
	if (ch == victim)
	{
		send_to_char( "You desperately try to summon the energy needed to heal yourself, but you fail.\n\r",ch);
		return;
	}
	else
	{
		pager_printf(ch, "You desperately try to summon the energy needed to heal %s, but you fail.\n\r", PERS(victim,ch));
		pager_printf(victim, "%s desperately tries to summon the energy needed to heal you, but fails.\n\r", PERS(ch,victim));
		return;
	}
 }
}

void do_mystic( CHAR_DATA *ch, char *argument  )
{
    char buf[MAX_STRING_LENGTH];

    if ( ch->alignment < 800 )
    {
        pager_printf( ch, "You're not good enough.\n\r" );
        return;
    }
    if ( IS_AFFECTED( ch, AFF_KAIOKEN ) )
    {
	pager_printf( ch, "You drop out of Kaioken.\n\r" );
	do_powerdown( ch, "" );
	return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_mystic ) )
    {
	learn_from_success( ch, gsn_mystic );
	if ( ch->plmod > 3500 )
	{
	    WAIT_STATE( ch, 15 );
	    ch->multi_str = 1500;
	    ch->multi_dex = 1500;
	    ch->multi_con = 1500;
	    ch->multi_int = 1500;
	    ch->multi_wis = 1500;
	    ch->multi_cha = 0;
	    ch->multi_lck = 0;
	    ch->plmod     = 4100;
            sprintf(buf, "&WA single thought of true belief of your powers enters you mind suddenly it happens");
            pager_printf( ch, "%s\n\r", buf );
 	    sprintf(buf, "&Wraging %s &Wflames errupt from you as your clothes billow upwards and your power", ch->energycolour ); 
	    pager_printf( ch, "%s\n\r", buf );
	    sprintf(buf, "&Wincreases by a huge amount and your senses sharpen as you grasp your true potential.");
  	    pager_printf( ch, "%s\n\r", buf );
	    act( AT_LBLUE, "&C$n clenches $s fists, $s aura swirling briefly, causing a small cloud of dust to be kicked up.  $s aura quickly returns to normal, as if nothing happened. By the look in $n's eyes, $e is ready to face anything in the universe twice.", ch, NULL, NULL, TO_ROOM );
	}
        else if ( !IS_AFFECTED( ch, AFF_MYSTIC ) )
	{
	    ch->multi_str = 1500;
	    ch->multi_dex = 1500;
	    ch->multi_con = 1500;
	    ch->multi_int = 1500;
	    ch->multi_wis = 1500;
	    ch->multi_cha = 0;
 	    ch->multi_lck = 0;
	    ch->plmod     = 4100;
	    xSET_BIT( ch->affected_by, AFF_MYSTIC );
	    sprintf(buf, "&CA single thought of true belief of your powers enters you mind suddenly it happens raging %s flames&C errupt from you as your clothes billow upwards and your power increases by a huge amount and your senses sharpen as you grasp your true potential", ch->energycolour);
	    pager_printf( ch, "%s\n\r", buf );
            act( AT_LBLUE, "&C$n clenches $m fists, their aura swirling aura swirling briefly, causing a small cloud of dust to briefly be kicked up. Their aura quickly returns to normal, as if nothing happened. By the look in $n eyes $m ready to face anything in the universe twice.", ch, NULL, NULL, TO_ROOM );
	}
	else if ( IS_AFFECTED( ch, AFF_MYSTIC ) )
	{
	    sprintf(buf, "&GMystic doesn't even seem necessary any more...");
	    pager_printf( ch, "%s\n\r", buf );
	    ch->multi_str = 0;
	    ch->multi_dex = 0;
	    ch->multi_con = 0;
	    ch->multi_int = 0;
	    ch->multi_wis = 0;
	    ch->multi_lck = 0;
	    ch->multi_cha = 0;
	    ch->plmod = 100;
	    xREMOVE_BIT( ch->affected_by, AFF_MYSTIC );
 	    act( AT_YELLOW, "$n powers down.", ch, NULL, NULL, TO_ROOM );
	}
    }
    else
    {
	learn_from_failure( ch, gsn_mystic );
        send_to_char("&gYou fail to unleash your hidden power... How worthless you are.", ch );
	act( AT_GREEN, "$n tries to unleash $s full power.", ch, NULL, NULL, TO_ROOM );
    }
    pl_update( ch, NULL );
    return;
}

void kaioken_burst( CHAR_DATA *ch, char *arg )
{
  if ( ch->race != RACE_SAIYAN && ch->race != RACE_HALFBREED )
  {
	send_to_char( "What the hell is Kaioken Burst?\n\r", ch );
	return;
  }
  if ( ch->basepl < 500000000 )
  {
	send_to_char( "You need 500,000,000 powerlevel to use Kaioken Burst.\n\r", ch );
	return;
  }
  if ( ch->bursttimer > 0 )
  {
	send_to_char( "Your last Kaioken Burst hasn't worn off yet.\n\r", ch );
	return;
  }
  ch->bursttimer = 10;
  ch->plmod += 2000;
  send_to_char("&RTorrents of red energy literally spew from your body, as you scream 'SUPER KAIOKEN ATTACK!'  Your muscles bulk up immensely...\n\r", ch );
  act( AT_GREEN, "$n yells 'Super Kaioken Attack!'.", ch, NULL, NULL, TO_ROOM );
  update_current( ch, NULL );
}

void  do_stonespit( CHAR_DATA *ch, char *argument )
{
}



void do_lssj( CHAR_DATA *ch, char *argument )
{
  if ( !IS_NPC(ch) && !ch->pcdata->monkey )
  {
        send_to_char( "You must be confusing your belt for a tail...\n\r", ch );
        return;
  }
  if ( IS_AFFECTED(ch, AFF_LSSJ ) )
  {
	send_to_char( "YOU ARE A GOD ALREADY!\n\r", ch );
	return;
  }
  if ( ch->alignment > -333 )
  {
	send_to_char( "ALL the Legendary Super Saiyans were evil.\n\r", ch );
	return;
  }

	learn_from_success( ch, gsn_lssj );
        ch->multi_str = 8888 - ( ch->perm_str + ch->mod_str );
        ch->multi_int = 8888 - ( ch->perm_int + ch->mod_int );
        ch->multi_wis = 8888 - ( ch->perm_wis + ch->mod_wis );
        ch->multi_dex = 8888 - ( ch->perm_dex + ch->mod_dex );
        ch->multi_con = 8888 - ( ch->perm_con + ch->mod_con );
        ch->multi_cha = 8888 - ( ch->perm_cha + ch->mod_cha );
        ch->multi_lck = 8888 - ( ch->perm_lck + ch->mod_lck );
        xREMOVE_BIT	( ch->affected_by, AFF_SSJ 	    );
        xREMOVE_BIT	( ch->affected_by, AFF_USSJ 	    );
        xREMOVE_BIT	( ch->affected_by, AFF_SSJ2 	    );
        xREMOVE_BIT	( ch->affected_by, AFF_SSJ3 	    );
        xREMOVE_BIT	( ch->affected_by, AFF_KAIOKEN	    );
        xSET_BIT	( ch->affected_by, AFF_LSSJ 	    );
	pager_printf( ch, "Your muscles begin to tense, compacting in on themselves and increasing in size,"
			  " then compacting again and going through the same process a number of times. "
			  " As your muscle density increaes in this way, you feel incredibile stress upon"
			  " your weak, pitiful skelleton as at near-crumbles at the increased mass you continue"
			  " to build your powerlevel.  With a tremendous crash, you explode in a terrifying"
			  " sparkly-gold aura, your eyes filled with hate, and your mind focused only on"
			  " destruction.\n\r" );
        act( AT_BLOOD, "&r$n explodes in an aura of rage, $s muscles getting bulkier and bulkier as time passes.  Eventually $e looks like $e can take no more of this muscle-building, and explodes in a ferocious, sparkly gold aura, and screams with hatred and rage...", ch, NULL, NULL, TO_CANSEE );
  ch->plmod = 2800;
  update_current( ch, NULL );
  hp_update(ch );
  ch->hit 	= ch->max_hit;
  ch->mana 	= ch->max_mana;
  return;  
}

void do_quit( CHAR_DATA *ch, char *argument )
{
        if ( !ch || IS_NPC(ch) )
            return;

        if ( IS_AFFECTED(ch, AFF_CHAOS ) || (infected && !str_cmp( infected->name, ch->name ) ) )
        {
            send_to_char( "Quit?!  But everything's so swirly, and pretty and... MUST KILL!  DESTROY!!\n\r", ch );
            return;
        }

        char_quit( ch, TRUE, argument );
}


void do_scry( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int cost;
    ROOM_INDEX_DATA *original;

    set_char_color( AT_IMMORT, ch );
    one_argument( argument, arg );

    if ( arg[0] == '\0' )
    {
        send_to_char( "Scry upon whom?\n\r", ch );
        return;
    }

    if ( who_fighting( ch ) != NULL )
    {
         send_to_char( "You are busy fighting...\n\r", ch );
         return;
    }


    if ( ( victim = get_char_world( ch, arg ) ) == NULL )
    {
        send_to_char( "They aren't here.\n\r", ch );
        return;
    }


    cost = get_curr_wis(victim) * 4;

    if ( victim->mana < cost || victim->level > 50 )
    {
	send_to_char( "You cannot afford the cost of this spell.\n\r", ch );
	return;
    }

    if ( !can_use_skill( ch, number_percent(), gsn_scry ) )
    {
	send_to_char( "You fail to remember the invocation.\n\r", ch );
	act( AT_SKILL, "$n looks momentarily confused.", ch, NULL, NULL, TO_NOTVICT );
	return;
    }

    victim->mana -= cost;


    send_to_char( "You chant a brief invocation, then the mysteries of your target's locations become clear to you...\n\r", ch );
    act( AT_SKILL, "$n mutters an invocation before momentarily slipping into a trance.", ch, NULL, NULL, TO_NOTVICT );
    if ( !IS_NPC(victim) && victim->position > POS_SLEEPING)
        send_to_char( "You feel an itching sensation behind your eyeballs, as if they were being shared...\n\r", victim );

    if ( victim->position <= POS_SLEEPING )
    {
	ch_printf( ch, "You see nothing!  Is %s blind or something?!\n\r", HESHE(victim->sex) );
    }
    else
    {
        original = ch->in_room;
        char_from_room( ch );
        char_to_room( ch, victim->in_room );
        do_look( ch, " " );
        char_from_room( ch );
        char_to_room( ch, original );
    }
    learn_from_success(ch, gsn_scry );
    return;
}


void do_soulsearch( CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int ctr = 0;

    if ( who_fighting(ch) )
    {
	send_to_char( "You are busy.\n\r", ch );
	return;
    }

    if ( ch->mana < 7500 )
    {
	send_to_char( "You lack the energy.\n\r", ch );
	return;
    }

    act( AT_SKILL, "$n closes $s eyes, as if searching for something in the recesses of $s mind.", ch, NULL, NULL, TO_NOTVICT );

    for ( victim = first_char; victim; victim = victim->next )
    {
	if ( !IS_NPC(victim) && victim != ch && victim->level < 50 )
        {
            if ( can_use_skill( ch, number_percent(), gsn_soulsearch ) ) 
            {
		ctr++;
	        ch_printf( ch, "&Y%-10s &z| &r%s&z/&R%s\n\r", PERS(victim,ch), format_pl( victim->basepl), format_pl(victim->currpl));
		learn_from_success( ch, gsn_soulsearch );
            }
	}
    }
    if ( ctr == 0 )
    {
	send_to_char( "You fail to find any souls...\n\r", ch );
	return; 
    }
}


void do_sync( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 victim = get_char_room( ch, argument );

 if ( IS_NPC( ch ) || !argument || ch == victim )
 {
     send_to_char( "Please specify your player-character target.\n\r", ch );
     return;
 }

 if ( who_fighting(ch) != NULL || IS_SET(ch->in_room->room_flags, ROOM_GTRAIN ))
 {
     send_to_char( "And you plan to be able to stay still and concentrate for that long do you?\n\r",ch);
     return;
 }

 if ( !victim )
 {
     send_to_char( "That person is not here.\n\r",ch);
     return;
 }

 if ( victim->race == RACE_ANDROID )
 {
     pager_printf( ch, "But %s is a big stupid robot!\n\r", PERS(victim,ch) );
     return;
 }

 if ( victim->fused )
 {
     send_to_char( "Odd: You seem to be detecting two powerlevels...\n\r", ch );
     return;
 }

 if ( who_fighting(victim) != NULL || IS_NPC(victim))
 {
     send_to_char( "They're a little preoccupied at the moment.\n\r",ch);
     return;
 }

 if ( ch->basepl <= victim->basepl / 10 )
 {
     send_to_char( "They're way stronger than you - even with the power of synchronisation you couldn't get that much stronger so quickly.\n\r",ch);
     return;
 }

 if ( victim->position > POS_SITTING )
 {
     pager_printf( ch, "You need to get them to sit, sleep or rest first.\n\r" );
     return;
 }

 if ( IS_AFFECTED(ch, AFF_SYNCED ))
 {
     pager_printf( ch, "Once is enough!\n\r" );
     return;
 }

 if ( IS_AFFECTED(victim, AFF_SYNCED ))
 {
     pager_printf( ch, "They're synchronised with someone else.\n\r" );
     return;
 }

 if ( ( ch->alignment >= 333 && victim->alignment < 333 ) || ( ch->alignment <= -333 && victim->alignment > -333 ) )
 {
     send_to_char( "Your alignments conflict too strongly.  Only neutral Namekians can synchronise with anybody...\n\r", ch );
     return;
 }

 learn_from_success( ch, gsn_sync );
 pager_printf( ch, "&CKneeling down, you grasp %s's shoulder and begin to synchronise %s power with yours.  Smoke starts to envelop you both, and when it clears you realise you have synchronised with %s's energy!\n\r", PERS(victim,ch), HISHER(victim->sex), PERS(victim,ch) );
 if ( victim->position > POS_SLEEPING )
     pager_printf( victim, "&C%s kneels down beside you and grasps your shoulder, begining to synchronise %s power with yours... You feel nauseous...\n\r", PERS(ch,victim), HISHER(ch->sex) );
 act( AT_WHITE, "$n places $s hand on $N's shoulder and starts to synchronise with $M!  Light-blue smoke begins to fill the room...", ch, NULL, victim, TO_NOTVICT );
 ch->multi_str += UMIN( 1500, victim->perm_str / 2 );
 ch->multi_dex += UMIN( 1500, victim->perm_dex / 2 );
 ch->multi_con += UMIN( 1500, victim->perm_con / 2 );
 ch->multi_int += UMIN( 1500, victim->perm_int / 2 );
 ch->multi_wis += UMIN( 1500, victim->perm_wis / 2 );
 ch->multi_cha += UMIN( 1500, victim->perm_cha / 2 );
 ch->multi_lck += UMIN( 1500, victim->perm_lck / 2 );
 ch->plmod +=   UMIN( URANGE(100,victim->plmod/2,victim->plmod), 1500);
 xSET_BIT(ch->affected_by, AFF_SYNCED );
 pager_printf( ch, "&YStrength up &R%d&Y!\n\rDexterity up &R%d&Y!\n\rConstitution up &R%d&Y!\n\rIntelligence up &R%d&Y!\n\rWisdom up &R%d&Y!\n\rCharisma up &R%d&Y!\n\rPowerlevel modifier up &R%d&Yx!\n\r", 
		UMIN( 1500, victim->perm_str/2 ), UMIN( 1500, victim->perm_dex/2 ), UMIN( 1500, victim->perm_con/2 ), UMIN( 1500, victim->perm_int/2 ), 
		UMIN( 1500, victim->perm_wis/2 ), UMIN( 1500, victim->perm_cha/2 ), UMIN( 15, URANGE(100,victim->plmod/2,victim->plmod)/100 ) );
 return;
}

void do_consecrate( CHAR_DATA *ch, char *argument )
{
  OBJ_DATA *obj;
  AFFECT_DATA *paf;
  char arg1[MAX_INPUT_LENGTH];
  char arg2[MAX_INPUT_LENGTH];
  int cost;

  if ( IS_NPC(ch) || !ch->pcdata->deity )
  {
      pager_printf( ch, "Not so fast, you dirty, godless heathen...\n\r" );
      return;
  }

  if ( !argument || argument == NULL )
  {
     pager_printf( ch, "&WFormat: &CConsecrate 'itemname' check\n\r&C        Consecrete 'itemname' confirm\n\r" );
     return;
  }

  argument = one_argument( argument, arg1 );
  argument = one_argument( argument, arg2 );

  if ( arg1 == NULL )
  {
     pager_printf( ch, "&WFormat: &CConsecrate 'itemname' check\n\r&C        Consecrate 'itemname' confirm\n\r" );
     return;
  }

  obj = get_obj_carry( ch, arg1 );
  if ( !obj )
  {
        pager_printf( ch, "You must specify an object you are holding.\n\r" );
        pager_printf( ch, "&WFormat: &CConsecrate 'itemname' check\n\r&C        Consecrate 'itemname' confirm\n\r" );
        return;
  }
  if ( obj->item_type != ITEM_ARMOR || IS_OBJ_STAT(obj, ITEM_LOYAL) )
  {
	pager_printf( ch, "You can only request the gods consecrate armour that is not already loyal.\n\r" );
        return;
  }

  cost = obj->value[0] / 10;

  for ( paf = obj->pIndexData->first_affect; paf; paf = paf->next )
  {
      if ( ( paf->location == APPLY_STR || paf->location == APPLY_DEX ||
	 paf->location == APPLY_INT || paf->location == APPLY_WIS ||
	 paf->location == APPLY_CON || paf->location == APPLY_CHA ||
	 paf->location == APPLY_LCK ) && paf->modifier > 0 )
          cost += paf->modifier;
  }
  for ( paf = obj->first_affect; paf; paf = paf->next )
  {
      if ( ( paf->location == APPLY_STR || paf->location == APPLY_DEX ||
	 paf->location == APPLY_INT || paf->location == APPLY_WIS ||
	 paf->location == APPLY_CON || paf->location == APPLY_CHA ||
	 paf->location == APPLY_LCK ) && paf->modifier > 0 )
          cost += paf->modifier;
  }
  
  if ( !str_cmp( arg2, "check" ) )
  {
      pager_printf( ch, "The cost to consecrate this item would be %s loyality kili.\n\r", format_pl( cost ) );
      return;
  }
  else if ( !str_cmp( arg2, "confirm" ) )
  {
     if ( ch->pcdata->loyalty < cost )
     {
	  pager_printf( ch, "You do not have enough loyality kili.\n\r" );
	  return;
     }
     ch->pcdata->loyalty -= cost;
     xSET_BIT( obj->extra_flags, ITEM_LOYAL );
     pager_printf( ch, "Your deity, %s, reaches down and touches your %s, making it loyal forever.\n\r", ch->pcdata->deity->name, obj->short_descr );
  }
  else
  {
     pager_printf( ch, "&WFormat: &CConsecrate 'itemname' check\n\r&C       Consecrate 'itemname' confirm\n\r" );
  }
}



void do_battery( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 victim = get_char_room( ch, argument );

 if ( IS_NPC( ch ) || !argument )
 {
     send_to_char( "Please specify your target\n\r", ch );
     return;
 }
 if ( who_fighting(ch) != NULL || IS_SET(ch->in_room->room_flags, ROOM_GTRAIN ))
 {
     send_to_char( "And you plan to be able to stay still and concentrate for that long do you?\n\r",ch);
     return;
 }
/*
 if ( IS_SET(ch->in_room->room_flags, ROOM_DUEL ) )
 {
     send_to_char( "Go to the hospital instead of healing in the duelling grounds.\n\r", ch );
     return;
 }
 if ( IS_SET(ch->in_room->room_flags, ROOM_HOSPITAL ) )
 {
     send_to_char( "You're in a hospital already.  Be patient.\n\r", ch );
     return;
 }
*/
 if ( !victim )
 {
     send_to_char( "That person is not here.\n\r",ch);
     return;
 }
 if ( IS_AFFECTED(victim, AFF_LSSJ ) )
 {
     send_to_char( "You're stupid, not suicidal.\n\r", ch );
     return;
 }
 if ( who_fighting(victim) != NULL || IS_NPC(victim))
 {
     send_to_char( "They're a little preoccupied at the moment.\n\r",ch);
     return;
 }

 if ( victim->mana == (victim->max_mana) || victim->race == RACE_ANDROID)
 {
     if (victim == ch)
     {
	send_to_char( "You are not in need of further energy.\n\r",ch);
	return;
     }
     else
     {
	send_to_char( "They are not in need of further energy.\n\r",ch);
	return;
     }
 }


 if ( can_use_skill( ch, number_percent(), gsn_battery ) )
 {
    learn_from_success( ch, gsn_battery );
    {

	pager_printf( ch,"&CYou place a hand on %s and begin to restore energy to %s.\n\r", PERS(victim,ch), HIMHER(victim->sex) );
	pager_printf( victim,"&C%s places a hand on your wounds and begins to restore your energy.\n\r", ch->name );
	ch->hit -= (get_curr_int(ch));
	victim->mana += (get_curr_int(ch));
        if (victim->mana > ( victim->max_hit))
		victim->mana = (victim->max_mana);
        act( AT_WHITE, "$n places $s hand on $N's shoulder.", ch, NULL, victim, TO_CANSEE );        
	return;
    }
 }
 else
 {
	learn_from_failure( ch, gsn_battery );
	{
		pager_printf(ch, "&G>>BUFFER_OVERFLOW: TRANSFER ERROR...\n\r", PERS(victim,ch));
		return;
	}
 }
}

