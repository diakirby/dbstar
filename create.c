/*
 *
 *  Code to allow item creation and enchantment
 *
 */

#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"

#define BASE_WEAPON 67
#define BASE_ARMOUR 68

char *  const   Levels    [] =
{
  "Flimsy", "Braced", "Reinforced", "Strong", "Dire", "Grim", "Gold", "Rare", "Sparkling"
};

char *  const   Blades    [] =
{
  "Knife", "Dagger", "Short-sword", "Tanto", "Long-sword", "Katana", "Claymore", "Flamberge", "Fellblade", "Crystaline Sword"
};

char *  const   Axes      [] =
{
  "Hatchet", "Hand axe", "Fire axe", "Great-axe", "Chaos-axe"
};

char *  const   Hammers   [] =
{
  "Hammer", "Mallet", "Mace", "Bone Breaker", "Righteous Maul"
};

char *  const   Armour    [] =
{
  "Pair of Gloves", "Pair of Boots", "Pair of Pants", "Helmet", "Cape", "Set of Body-armour"
};

void do_create ( CHAR_DATA *ch, char *argument )
{
  int ctr = 0, wood = 0, metal = 0, leather = 0, plastic = 0, cloth = 0, glass = 0, ceramic = 0, lvl = 0;
  OBJ_DATA * obj;
  OBJ_DATA * obj2;
  char arg[MAX_INPUT_LENGTH];
  char buf[MAX_STRING_LENGTH];
  lvl = number_range( 0, 2 );
  if ( lvl == 2 )
      lvl = number_range( 2, 4 );
  if ( lvl == 4 )
      lvl = number_range( 4, 7 );
  if ( lvl == 7 )
      lvl = number_range( 7, 8 );

  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4361 )
	wood = obj->count;
  }
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4362 )
	metal = obj->count;
  }
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4363 )
	leather = obj->count;
  }  
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4364 )
	plastic = obj->count;
  }  
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4365 )
	cloth = obj->count;
  }  
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4366 )
	glass = obj->count;
  }  
  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4367 )
	ceramic = obj->count;
  }  

   if ( ch->position <= POS_SLEEPING )
   {
        ch_printf( ch, "You make some stuff of dreams from your sleep-borne attempts to make stuff.\n\r" );
        return;
   }
   if ( ch->position == POS_FIGHTING )
   {
        send_to_char( "Maybe later?\n\r", ch );
        return;
   }

  if ( !argument || !str_cmp( argument, "" ) )
  {
      send_to_char( "&WCrafting options:\n\r&CBlades:\t\t", ch );
      for ( ctr = 0; ctr <= 8; ctr++ )
      {
	  if ( wood >= 2 + ( ctr * 2 ) && metal >= 2 + ( ctr * 3 ) && leather >= 2 + ( ctr * 1 ) )
	      ch_printf( ch, "&G%s", Blades[ctr] );
	  else
	      ch_printf( ch, "&R%s", Blades[ctr] );
	  if ( ctr < 8 )
	      send_to_char( ", ", ch );
      }
      send_to_char( "&w\n\r&CAxes:\t\t", ch );
      for ( ctr = 0; ctr <= 4; ctr++ )
      {
	  if ( wood >= 2 + ( ctr * 2 ) && metal >= 2 + ( ctr * 3 ) && leather >= 2 + ( ctr * 1 ) )
	      ch_printf( ch, "&G%s", Axes[ctr] );
	  else
	      ch_printf( ch, "&R%s", Axes[ctr] );
	  if ( ctr < 4 )
	      send_to_char( ", ", ch );
      }
      send_to_char( "\n\r&CHammers:\t", ch );
      for ( ctr = 0; ctr <= 4; ctr++ )
      {
	  if ( wood >= 2 + ( ctr * 2 ) && metal >= 2 + ( ctr * 3 ) && leather >= 2 + ( ctr * 1 ) )
	      ch_printf( ch, "&G%s", Hammers[ctr] );
	  else
	      ch_printf( ch, "&R%s", Hammers[ctr] );
	  if ( ctr < 4 )
	      send_to_char( ", ", ch );
      }
      ch_printf( ch, "\n\r&w&WYou have &Y%d&W wood, &Y%d&W metal, and &Y%d&W leather.\n\r", wood, metal, leather );
      send_to_char( "\n\r&CArmour:\t\t", ch );
      for ( ctr = 0; ctr <= 5; ctr++ )
      {
	  if ( plastic >= ( ctr + 1 ) && glass >= ( ctr + 1 ) * 2 && cloth >= ( ctr + 1 ) * 3 && ceramic >= ( ctr + 1 ) * 3 )
	      ch_printf( ch, "&G%s", Armour[ctr] );
	  else
	      ch_printf( ch, "&R%s", Armour[ctr] );
	  if ( ctr < 5 )
	      send_to_char( ", ", ch );
      }
      ch_printf( ch, "\n\r&w&WYou have &Y%d&W plastic, &Y%d&W cloth, &Y%d&W glass, and &Y%d&W ceramic.\n\r", plastic, cloth, glass, ceramic );
      send_to_char( "&PSyntax: Craft <Item Type> <Item Name>\n\rWhere <Item Type> is one of: Blade, Axe, Hammer, Armour\n\r", ch );
      return;
  }
  argument = one_argument( argument, arg );
  if ( !str_cmp( arg, "Blade" ) )
  {
      for ( ctr = 0; ctr <= 8; ctr++ )
      {
	if ( !str_cmp( argument, Blades[ctr] ) )
	{
	    if ( wood >= 1 + ( ctr * 2 ) && metal >= 1 + ( ctr * 3 ) && leather >= 1 + ( ctr * 1 ) )
	    {
		obj = create_object( get_obj_index( 67 ), ch->basepl / 10 );
		sprintf( buf, "a %s %s", Levels[lvl], Blades[ctr] );
		obj->name = STRALLOC ( buf );
		obj->short_descr = STRALLOC ( buf );
		obj->value[3] = 1;
		if ( ctr > 2 )
		    obj->value[3] = 3;
		obj->value[1] = 2 + lvl;
		obj->value[2] = obj->value[1] * ( 2 + ctr );
		obj->value[1] *= ch->pcdata->learned[gsn_create] + 30;
		obj->value[2] *= ch->pcdata->learned[gsn_create] + 30;
		obj->value[1] /= 100;
		obj->value[2] /= 100;
		obj->value[0] = ch->pcdata->learned[gsn_create] * 7;
		obj->weight   = 1 + ( ch->perm_str / 100 );

	        for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
	        {
		    if ( obj2->pIndexData->vnum == 4361 )
		    {
	                split_obj( obj2, ( 1 + ( ctr * 2 ) ) );
	                extract_obj( obj2 );
			break;
		    }
		}

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4362 )
                    {
                        split_obj( obj2, ( 1 + ( ctr * 3 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4363 )
                    {
		         split_obj( obj2, ( 1 + ( ctr ) ) );
                         extract_obj( obj2 );
                         break;
                    }
                }
		ch_printf( ch, "You create %s.\n\r", obj->short_descr );
		learn_from_success( ch, gsn_create );
		obj_to_char( obj, ch );
		return;
	    }
	    else
	    {
		ch_printf( ch, "You need %d wood, %d metal and %d leather to make that.\n\r", (1+(ctr*2)), (1+(ctr*3)), (1+ctr) );
		return;
	    }
	}
      }
      send_to_char( "Item type not known.\n\r", ch );
      return;
  }

  if ( !str_cmp( arg, "Axe" ) )
  {
      for ( ctr = 0; ctr <= 4; ctr++ )
      {
        if ( !str_cmp( argument, Axes[ctr] ) )
        {
            if ( wood >= 1 + ( ctr * 2 ) && metal >= 1 + ( ctr * 3 ) && leather >= 1 + ( ctr * 1 ) )
            {
                obj = create_object( get_obj_index( 67 ), ch->basepl / 10 );
                sprintf( buf, "a %s %s", Levels[lvl], Axes[ctr] );
                obj->name = STRALLOC ( buf );
                obj->short_descr = STRALLOC ( buf );
                obj->value[3] = 1;
                if ( ctr > 2 )
                    obj->value[3] = 3;
                obj->value[1] = 3 + lvl;
                obj->value[2] = obj->value[1] * ( 1 + ( 0.8 * ctr ) );
                obj->value[1] *= ch->pcdata->learned[gsn_create] + 30;
                obj->value[2] *= ch->pcdata->learned[gsn_create] + 30;
                obj->value[1] /= 100;
                obj->value[2] /= 100;
                obj->value[0] = ch->pcdata->learned[gsn_create] * 7;
                obj->weight   = 1 + ( ch->perm_str / 100 );

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4361 )
                    {
                        split_obj( obj2, ( 1 + ( ctr * 2 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4362 )
                    {
                        split_obj( obj2, ( 1 + ( ctr * 3 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4363 )
                    {
                         split_obj( obj2, ( 1 + ( ctr ) ) );
                         extract_obj( obj2 );
                         break;
                    }
                }
                ch_printf( ch, "You create %s.\n\r", obj->short_descr );
                learn_from_success( ch, gsn_create );
                obj_to_char( obj, ch );
                return;
            }
            else
            {
                ch_printf( ch, "You need %d wood, %d metal and %d leather to make that.\n\r", (1+(ctr*2)), (1+(ctr*3)), (1+ctr) );
                return;
            }
        }
      }
      send_to_char( "Item type not known.\n\r", ch );
      return;
  }

  if ( !str_cmp( arg, "Hammer" ) )
  {
      for ( ctr = 0; ctr <= 4; ctr++ )
      {
        if ( !str_cmp( argument, Hammers[ctr] ) )
        {
            if ( wood >= 1 + ( ctr * 2 ) && metal >= 1 + ( ctr * 3 ) && leather >= 1 + ( ctr * 1 ) )
            {
                obj = create_object( get_obj_index( 67 ), ch->basepl / 10 );
                sprintf( buf, "a %s %s", Levels[lvl], Hammers[ctr] );
                obj->name = STRALLOC ( buf );
                obj->short_descr = STRALLOC ( buf );
                obj->value[3] = 8;
                if ( ctr > 2 )
                    obj->value[3] = 3;
                obj->value[1] = 1 + lvl;
                obj->value[2] = obj->value[1] * ( 2 + ( 1.4 * ctr ) );
                obj->value[1] *= ch->pcdata->learned[gsn_create] + 40;
                obj->value[2] *= ch->pcdata->learned[gsn_create] + 40;
                obj->value[1] /= 100;
                obj->value[2] /= 100;
                obj->value[0] = ch->pcdata->learned[gsn_create] * 7;
                obj->weight   = 1 + ( ch->perm_str / 100 );

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4361 )
                    {
                        split_obj( obj2, ( 1 + ( ctr * 2 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4362 )
                    {
                        split_obj( obj2, ( 1 + ( ctr * 3 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4363 )
                    {
                         split_obj( obj2, ( 1 + ( ctr ) ) );
                         extract_obj( obj2 );
                         break;
                    }
                }
                ch_printf( ch, "You create %s.\n\r", obj->short_descr );
                learn_from_success( ch, gsn_create );
                obj_to_char( obj, ch );
                return;
            }
            else
            {
                ch_printf( ch, "You need %d wood, %d metal and %d leather to make that.\n\r", (1+(ctr*2)), (1+(ctr*3)), (1+ctr) );
                return;
            }
        }
      }      
      send_to_char( "Item type not known.\n\r", ch );
      return;
  }

  if ( !str_cmp( arg, "Armour" ) || !str_cmp( arg, "Armor" ) )
  {
      for ( ctr = 0; ctr <= 5; ctr++ )
      {
        if ( !str_cmp( argument, Armour[ctr] ) )
        {
            if ( plastic >= ( ctr + 1 ) && glass >= ( ctr + 1 ) * 2 && cloth >= ( ctr + 1 ) * 3 && ceramic >= ( ctr + 1 ) * 3 )
            {
                obj = create_object( get_obj_index( 68 ), ch->basepl / 10 );
                sprintf( buf, "a %s %s", Levels[lvl], Armour[ctr] );
                obj->name = STRALLOC ( buf );
                obj->short_descr = STRALLOC ( buf );
                obj->value[0] = 200 * ( lvl + ctr );
                obj->value[1] = obj->value[0];
                obj->value[0] *= ch->pcdata->learned[gsn_create] + 30;
                obj->value[1] *= ch->pcdata->learned[gsn_create] + 30;
                obj->value[0] /= 100;
                obj->value[1] /= 100;
                obj->weight   = 1 + ( ch->perm_str / 200 );
		if ( ctr == 0 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "hands" ) );
		if ( ctr == 1 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "feet" ) );
		if ( ctr == 2 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "legs" ) );
		if ( ctr == 3 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "head" ) );
		if ( ctr == 4 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "about" ) );
		if ( ctr == 5 )
		  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "body" ) );
                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4364 )
                    {
                        split_obj( obj2, ( ( ctr + 1 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4365 )
                    {
                        split_obj( obj2, ( 3 * ( ctr + 1 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4366 )
                    {
                         split_obj( obj2, ( 2 * ( 1 + ctr ) ) );
                         extract_obj( obj2 );
                         break;
                    }
                }

                for ( obj2 = ch->first_carrying; obj2; obj2 = obj2->next_content )
                {
                    if ( obj2->pIndexData->vnum == 4367 )
                    {
                        split_obj( obj2, ( 3 * ( ctr + 1 ) ) );
                        extract_obj( obj2 );
                        break;
                    }
                }

                ch_printf( ch, "You create %s.\n\r", obj->short_descr );
                learn_from_success( ch, gsn_create );
                obj_to_char( obj, ch );
                return;
            }
            else
            {
                ch_printf( ch, "You need %d plastic, %d glass, %d ceramic, and %d cloth to make that.\n\r", ( ctr + 1 ), ( ctr + 1 ) * 2, ( ctr + 1 ) * 3, ( ctr + 1 ) * 3 );
                return;
            }
        }
      }
      send_to_char( "Item type not known.\n\r", ch );
      return;
  }
  send_to_char( "Craft what, exactly?\n\r", ch );
}

void do_salvage( CHAR_DATA *ch, char *argument )
{
   OBJ_DATA *obj;
   OBJ_DATA *tools;
   OBJ_DATA *salvaged = NULL;
   int cnt = 0, i = 0, j = 0;
   

   obj = get_obj_carry( ch, argument );

   if ( ch->position <= POS_SLEEPING )
   {
        ch_printf( ch, "You recover some stuff of dreams from your sleep-borne attempts to salvage stuff.\n\r" );
        return;
   }
   if ( ch->position == POS_FIGHTING )
   {
   	send_to_char( "Maybe later?\n\r", ch );
   	return;
   }

   if ( !str_cmp( argument, "all" ) )
   {
        ch_printf( ch, "I think not.\n\r" );
        return;
   }

   if ( !obj || obj == NULL || !obj->name || !obj->short_descr )
   {
        ch_printf( ch, "You can't find %s about you to salvage it.\n\r", argument );
        return;
   }

   for ( tools = ch->first_carrying; tools ; tools = tools->next_content )
   {
        if ( tools->pIndexData->vnum == 4360 )
            break;
   }

   if ( !tools || tools->pIndexData->vnum != 4360 )
   {
        send_to_char( "You have no salvage kit!\n\r", ch );
        return;
   }
   
   switch ( number_range( 1, 10 ) )
   {
   	default:
   	    j = 1;
   	    break;
   	case 1:
   	case 2:
   	case 3:
   	case 4:
   	case 5:
   	case 6:
   	    j = 1;
   	    break;
   	case 7:
   	case 8:
   	case 9:
   	    j = 2;
   	    break;
   	case 10:
   	    j = 3;
   	    break;
    }

   separate_obj( tools );
   separate_obj( obj );
   obj_from_char( obj );
   extract_obj( obj );
   tools->value[0]--;
   ch_printf( ch, "You use your %s, and scuff it up some.\n\r", tools->short_descr );
   if ( tools->value[0] < 1 || tools->value[0] > 100 )
   {
        ch_printf( ch, "Your %s is exhausted, and disintegrates in your hands.\n\r", tools->short_descr );
        obj_from_char( tools );
        extract_obj( tools );
   }

   if ( !can_use_skill( ch, number_percent(), gsn_salvage ) )
   {
   	send_to_char( "Your salvage attempt failed.\n\r", ch );
   	learn_from_failure( ch, gsn_salvage );
   	return;
   	
   }
   learn_from_success( ch, gsn_fix );
   if ( number_range( 1, 10 ) == 10 )
   {
        ch_printf( ch, "You artfully deconstruct %s, but gain nothing special.\n\r", obj->short_descr );
        return;
   }
   switch ( obj->item_type )
   {
        default:
            ch_printf( ch, "You artfully deconstruct %s, but gain nothing special.\n\r", obj->short_descr );
            break;
        case ITEM_ARMOR:
	    i = number_range( 4361, 4363 );
	    while ( cnt < j )
	    {
		cnt++;
		salvaged = NULL;
		salvaged = create_object( get_obj_index( i ), 500 );
	    	obj_to_char( salvaged, ch );
            }
            ch_printf( ch, "You successfully salvage %s (%d) from %s.\n\r", salvaged->short_descr, j, obj->short_descr );
	    return;
        break;
        case ITEM_WEAPON:
	    i = number_range( 4364, 4367 );
	    while ( cnt < j )
	    {
		cnt++;
		salvaged = NULL;
		salvaged = create_object( get_obj_index( i ), 500 );
	    	obj_to_char( salvaged, ch );
            }
            ch_printf( ch, "You successfully salvage %s (%d) from %s.\n\r", salvaged->short_descr, j, obj->short_descr );
	    return;
        break;
        case ITEM_TREASURE:
	    i = number_range( 4361, 4369 );
	    if ( i > 4367 )
		i = number_range( 4361, 4369 );
	    salvaged = NULL;
	    salvaged = create_object( get_obj_index( i ), 500 );
	    obj_to_char( salvaged, ch );
            ch_printf( ch, "You successfully salvage %s (1) from %s.\n\r", salvaged->short_descr, obj->short_descr );
	    return;
        break;
   }  
}

void do_enchant( CHAR_DATA *ch, char *argument )
{
  OBJ_DATA * obj;
  int dust = 0, ecto = 0, obsidian = 0, essence = 0, heart = 0, ember = 0;

  for ( obj = ch->first_carrying; obj; obj = obj->next_content )
  {
    if ( obj->pIndexData->vnum == 4369 )
        dust = obj->count;
    if ( obj->pIndexData->vnum == 4370 )
        ecto = obj->count;
    if ( obj->pIndexData->vnum == 4371 )
        obsidian = obj->count;
    if ( obj->pIndexData->vnum == 4372 )
        essence = obj->count;
    if ( obj->pIndexData->vnum == 4373 )
        heart = obj->count;
    if ( obj->pIndexData->vnum == 4374 )
        ember = obj->count;
  }


   if ( ch->position <= POS_SLEEPING )
   {
        ch_printf( ch, "You make some stuff of dreams from your sleep-borne attempts to make stuff.\n\r" );
        return;
   }
   if ( ch->position == POS_FIGHTING )
   {
        send_to_char( "Maybe later?\n\r", ch );
        return;
   }

  if ( !argument || !str_cmp( argument, "" ) )
  {
      ch_printf( ch, "&w&WYou have &Y%d&W magic dust, &Y%d&W ectoplasm, &Y%d&W obsidian shards, &Y%d&W dark hearts, and &Y%d&W glowing embers.\n\r", dust,ecto,obsidian,essence,heart,ember );
  }
  
}

/*
  Adding wear flags

  TOGGLE_BIT( obj->wear_flags, 1 << get_wflag( "" ) );
 */
/*
  Adding flags

  xTOGGLE_BIT(obj->extra_flags, get_oflag( "" ) );
 */
/*
  Addding Affects

  CREATE(paf, AFFECT_DATA, 1);
  paf->type = -1;
  paf->duration = -1;
  paf->location = APPLY_;
  paf->modifier = Amt;
  xCLEAR_BITS(paf->bitvector);
  LINK(paf, obj->first_affect, obj->last_affect, next, prev);

 */

