#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include "mud.h"

#define Tornado     1
#define Lightning   2
#define Hurricane   3
#define Hail        4
#define Meteor      5
#define IonStorm    6
#define AcidRain    7
#define Earthquake  8
#define Snowstorm   9
#define Radiation  10

int   Disaster;
int   WeatherTimer;
bool  WeatherOn;


void RealWeather ()
{
 int   Chooser;
 char *Message = NULL;
 
 if ( number_range( 1, 50 ) == 1 )  // Something will actually happen.
 {
      Chooser = number_range( 1, 10 );
      switch ( Chooser )
      {
          case Tornado:
               if ( number_range( 1, 2 ) == 1 )
                  sprintf( Message, "Tornado Warning: Tornadoes will be striking Earth randomly soon." );
               else
               {   
                  if ( number_range( 1, 2 ) == 1 )
                     sprintf( Message, "Tornado Warning: Residents of Earth told to be wary of tornadoes for a while." );
                  else
                     sprintf( Message, "Tornado Warning: Better get to those shelters, Earthlings." );
               }
          Disaster = Tornado;
          WeatherTimer = 600;
	  WeatherOn = TRUE;
          break;
      }
 }
 if ( WeatherOn == TRUE )
	 talk_info( AT_PINK, Message );
}


void ActOnDisaster ()
{
 CHAR_DATA *ch;
 ROOM_INDEX_DATA *room;

 if ( number_range( 1, 10 ) <= 6 )
    return;

   switch ( Disaster )
   {
       case Tornado:
            for ( ch = first_char ; ch ; ch = ch->next )
            {
                if ( IS_NPC(ch) || IS_SET(ch->in_room->room_flags, ROOM_INDOORS ) || str_cmp( ch->in_room->area->planet, "Earth" ) )
                   continue;
            }
            act( AT_PINK, "A gust picks up $n and carries $m off!", ch, NULL, NULL, TO_CANSEE );
            char_from_room( ch );
            room = get_room_index( number_range( 20000, 20199 ) );     /* Can't be bothered to loop it now */
            if ( !room )
                 room = get_room_index( number_range( 20000, 20199 ) );               
            if ( !room )
                 room = get_room_index( number_range( 20000, 20199 ) );                
            char_to_room( ch, room );
            act( AT_PINK, "$n falls out of the sky and slams into the ground!!", ch, NULL, NULL, TO_CANSEE );
            ch->hit -= number_range( 1, 50 );
   }
}





