Traveling ki guide

1) Add these to the initial declarations

int vnum, attempt;
sh_int door;
EXIT_DATA *pexit;
bool moved = FALSE;

and change the "You aren't fighting anyone" to this
if ( ( victim = who_fighting( ch ) ) == NULL )
     {
         victim = ch->target;
         if ( ch->target == NULL )
         {
            send_to_char( "You aren't fighting anyone.\n\r", ch );
            return;
         }
     }

2) where you set the ch->skill_timer, add:
     ch->target = victim;

3) under ch->substate = SUB_NONE; add:

/* Move victim to ch and back after damage is done if... */
if ( ch->in_room != victim->in_room )
{
for ( attempt = 0; attempt < 8; attempt++ )
{
   door = number_door( );
   if ( ( pexit = get_exit( was_in, door ) ) == NULL || !pexit->to_room )
      continue;
   if ( ch->target->in_room == get_room_index( pexit->to_room->vnum ) )
   {
      pager_printf( victim, "&P%s's xxxxxxx flies in from the %s!!\n\r", PERS( ch, victim ), dir_name[pexit->dir] );
      pager_printf( ch, "Your xxxxxxx flies %s!\n\r", dir_name[pexit->dir] );
      vnum = victim->in_room->vnum;
      char_from_room( victim );
      char_to_room( victim, ch->in_room );
      moved = TRUE;
   }
}
}

Above the: if ( can_use_skill(ch, number_percent(), xxxxxxxxx ) ), add:
ch->target = NULL;

4) And before the final return, add:
 if ( moved == TRUE )
 {
     char_from_room( victim );
     char_to_room( victim, get_room_index( vnum );
     act( AT_GREEN, "$n's attack flies in at $N!",  ch, NULL, victim, TO_NOTVICT );
 }

