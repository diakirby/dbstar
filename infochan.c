/*
 *  Simple Info channel
 *  Author: Rantic (supfly@geocities.com)
 *  of FrozenMUD (empire.digiunix.net 4000)
 *
 *  Permission to use and distribute this code is granted provided
 *  this header is retained and unaltered, and the distribution
 *  package contains all the original files unmodified.
 *  If you modify this code and use/distribute modified versions
 *  you must give credit to the original author(s).
 */
#include <stdio.h>
#include "mud.h"

void talk_info ( sh_int AT_COLOR, char *argument )
{
	DESCRIPTOR_DATA *d;
	char buf[MAX_STRING_LENGTH];
	CHAR_DATA *original;
	int position;
		
	sprintf (buf, "&z[&W&YS&z&OT&Y&rA&W&RR&z]&Y&W %s", argument );
	
	for ( d = first_descriptor; d; d = d->next )
	{
		original = d->original ? d->original : d->character;
		if (( d->connected == CON_PLAYING ) && !IS_SET( original->deaf, CHANNEL_INFO )
		&& !IS_SET( original->in_room->room_flags, ROOM_SILENCE ) && !NOT_AUTHED( original ) )
		{
			position = original->position;
			original->position = POS_STANDING;
			act( AT_COLOR, buf, original, NULL, NULL, TO_CHAR );
			original->position = position;
		}
	}
}

void log_info( CHAR_DATA *ch, bool login, char *argument )
{
    DESCRIPTOR_DATA *d;
    char buf[MAX_STRING_LENGTH];
    CHAR_DATA *original;
    int position;

 if ( IS_IMMORTAL(ch) )
	return;

    if( login == TRUE )
	sprintf (buf, "&R&z[&W&YS&z&OT&Y&rA&Y&RR&z]&Y&W %s has joined the game!", ch->name );
    else
    {
	if ( !argument || argument == NULL || !str_cmp( argument, "" ) )
		sprintf (buf, "&R&z[&W&YS&z&OT&Y&rA&Y&RR&z]&Y&W %s has left the game", ch->name );
	else
		sprintf ( buf, "&R&z[&W&YS&z&OT&Y&rA&Y&RR&z]&Y&W %s quit - '%s'", ch->name, argument );
    }

    for ( d = first_descriptor; d; d = d->next )
    {
	original = d->original ? d->original : d->character;
	if (( d->connected == CON_PLAYING ) && !IS_SET( original->deaf, CHANNEL_INFO )
	    && !IS_SET( original->in_room->room_flags, ROOM_SILENCE )
	    && !NOT_AUTHED( original )
	    && can_see( d->character, ch ) )
	{
	    position = original->position;
	    original->position = POS_STANDING;
	    act( AT_BLUE, buf, original, NULL, NULL, TO_CHAR );
	    original->position = position;
	}
    }
}

