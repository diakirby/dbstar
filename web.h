/*	
 *	Web.h was made because some webmasters don't know the first thing
 *	about code.  So I saw it as a chance to dip my quill into a little
 *	more coding.
 *	
 *	What this is for is use with 'show_web_file' in websvr.c, it will
 *	do about the exact same as 'show_file' but for the WWW.  What this
 *	will do is show HTML files from a directory rather from code.
 *	
 *	The file types go like this:
 *
 *	*.html   These are files that are just one solid page with no code
 *	before or after it.  Such as an index page or error page.
 *
 *	*h.html  This will be a header (Telnet Interface Header), that will
 *	start out a HTML file with tags or what not, before the code.
 *	Placing graphics for headers is not uncommon and leave a nice touch.
 *	
 *	*f.html  This will be a footer (Telnet Interface Footer), it will clean
 *	up the mess made by the code, and finish the HTML, providing links
 *	and so on and so forth.  This is your interface, be creative :)
 *
 *	This should make those rudy poo webmasters happy, somewhat :)
 *	Just explain that they will have to account for the middle of the html
 *	to be code generated and to format accordingly before and after.
 *
 *	-- Christopher Aaron Haslage (Yakkov) 9/9/99 (No Help)
 */	

#define PUBLIC_WEB		"../web/public_html/"
#define STAFF_WEB		"../web/staff_html/"

#define PUB_INDEX		PUBLIC_WEB "index.html"
#define PUB_ERROR		PUBLIC_WEB "error.html"
#define PUB_ABOUT		PUBLIC_WEB "about.html"
#define PUB_WIZLIST_H		PUBLIC_WEB "wizlisth.html"
#define PUB_WIZLIST_F		PUBLIC_WEB "wizlistf.html"
#define PUB_WHOLIST_H		PUBLIC_WEB "wholisth.html"
#define PUB_WHOLIST_F		PUBLIC_WEB "wholistf.html"

#define STA_ERROR		STAFF_WEB "error.html"
