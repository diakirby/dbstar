CC      = gcc
#PROF    = -p
NOCRYPT =

#Please read the file READNOW before you start this MUD or edit this file!
#By default, GAME port is 4000, can change it by editing the "startup" file!

#Uncomment the two lines below if compiling on a Solaris box
#SOLARIS_FLAG = -Dsun -DSYSV
#SOLARIS_LINK = -lnsl -lsocket

#Uncomment the line below if you are getting a line like:
#interp.c:757: warning: int format, time_t arg (arg 7)
TIME = -DTIMEFORMAT

#Uncomment the line below if you are getting implicit decleration of re_exec
REG = -DREGEX

#Uncomment the line below if you are getting undefined re_exec errors
#NEED_REG = -lgnuregex

#Uncomment the line below if you are getting undefined crypt errors
NEED_CRYPT = -lm -lcrypt

#DBUGFLG = -DREQUESTS

#Intermud-3 - Comment out to disable I3 support in your code
#I3 = 1

#IMC2 - Comment out to disable IMC2 support
#IMC = 1

#Uncomment the line below if you want a performance increase though beware
#your core files may not be as much of a benefit if you do.
#OPT_FLAG = -finline-functions -funroll-loops -fdefer-pop -fstrength-reduce

C_FLAGS = $(OPT_FLAG) -O -g3 -Wall $(PROF) $(NOCRYPT) $(DBUGFLG) $(SOLARIS_FLAG) $(TIME) $(REG)
L_FLAGS = $(OPT_FLAG) $(PROF) $(SOLARIS_LINK) $(NEED_CRYPT)

#By default, WEB port is 4005, can change it on the MUD using the "cset" command!
#Uncomment the next three lines below if you want to use the Telnet Interface
#USE_WEBSVR     = -DUSE_WEBSVR

#WEBSVR_OFILES = websvr.o base64.o

#WEBSVR_CFILES = websvr.c base64.c

C_FILES = act_comm.c act_info.c act_move.c act_obj.c act_wiz.c arena.c  ban.c board.c boards.c \
          build.c clans.c colorize.c comm.c comments.c const.c create.c db.c deity.c \
          fight.c finger.c grub.c handler.c hashstr.c hiscores.c house.c ibuild.c ident.c immscore.c \
          imm_host.c infochan.c interp.c lockers.c magic.c makeobjs.c mapout.c marry.c misc.c mission.c mpxset.c \
          mud_comm.c mud_prog.c poll.c planes.c player.c polymorph.c requests.c pfiles.c \
          reset.c save.c services.c shops.c skills.c space.c special.c stat_obj.c \
          star.c starmap.c tables.c track.c tuffle.c update.c wizards.c \
          $(WEBSVR_CFILES) #games.c

ifdef IMC
   C_FILES := imc.c $(C_FILES)
   O_FILES := imc.o $(O_FILES)
   C_FLAGS := $(C_FLAGS) -DIMC -DIMCSMAUG
endif
 
ifdef I3
   C_FILES := i3.c $(C_FILES)
   O_FILES := i3.o $(O_FILES)
   C_FLAGS := $(C_FLAGS) -DI3 -DI3SMAUG
endif
 
O_FILES := $(patsubst %.c,o/%.o,$(C_FILES))
 
H_FILES = $(wildcard *.h)
 
all:
	make -s star
 
star: $(O_FILES)
	rm -f star
	$(CC) $(L_FLAGS) -o star $(O_FILES)
	echo "Done compiling DBStar...";
 
# Check in all files
checkin:
	ci -l $(C_FILES) $(H_FILES) Makefile
	rm -f *~
 
# Make a unified diff of all files.
diff:
	rcsdiff -u $(C_FILES) $(H_FILES) Makefile > ../diff.new
	gzip -9 ../diff.new
 
o/%.o: %.c
	echo "DBStar now compiling... $@";
	$(CC) -c $(C_FLAGS) $< -o $@
 
clean:
	rm -f o/*.o star *~
