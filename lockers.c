#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "mud.h"

#define LOCKER_DIR      "../lockers/"

void fwrite_locker( CHAR_DATA *ch, OBJ_DATA *locker )
{
    /* Variables */
    FILE *fp = NULL;
    char strsave[MAX_INPUT_LENGTH];

    if( !locker )
    {
	bug( "Fwrite_locker: NULL object.", 0 );
	bug( ch->name, 0 );
        return;
    }

    sprintf( strsave, "%s%s", LOCKER_DIR, capitalize( ch->name ) );
    
    if ( ( fp = fopen( strsave, "w" ) ) != NULL )
    {
        fwrite_obj( ch, locker, fp, 0, OS_LOCKER );
        fprintf( fp, "#END \n\r" ); 
        fclose( fp );
    }
    return;
}

void do_locker( CHAR_DATA *ch, char *argument )
{ 
    /* Variables */   
    FILE *fp = NULL;
    char strsave[MAX_INPUT_LENGTH];
    char buf [MAX_INPUT_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *locker;
          
    if ( IS_NPC(ch) )
        return;
        
    argument = one_argument(argument, arg);
    if ( arg[0] == '\0' )
    {
        send_to_char( "Syntax: locker <open | close>\r\n", ch );
    }
    
    if( !str_cmp( arg, "open" ) )
    {
        if (ch->in_room->vnum != ROOM_LOCKER)
        {
            send_to_char( "Does this smell like a locker room to you?\r\n", ch );
            return;
        } 

	locker = get_obj_list( ch, ch->name, ch->in_room->first_content );
                                                
        if (locker)
        {
            send_to_char("Your locker is already open.\n\r",ch);
            return;
        }
           
        sprintf( strsave, "%s%s", LOCKER_DIR, capitalize( ch->name ) );
    
        if ( ( fp = fopen( strsave, "r" ) ) != NULL )
        {
          for ( ; ; )
          {
            char letter;
            char *word;
            
            letter = fread_letter( fp );
            if ( letter == '#')
            {
              word = fread_word( fp );
  
              if (!strcmp(word,"END" ))
                   break;
                             
              if (!strcmp(word,"OBJECT"))
              {
                fread_obj( ch, fp, OS_LOCKER );
              }
            }
          }    
          fclose( fp );
        }
        else
        {                                                
          if (ch->gold < 10000)
          {
            send_to_char("You do not have enough zeni to create a locker.\n\r",ch);
            return;
          }
          
          ch->gold -= 10000;
          locker = create_object(get_obj_index(OBJ_VNUM_LOCKER),0);
          //sprintf(buf, locker->name, ch->name);
	  sprintf( buf, "%s locker", ch->name );
          STRFREE( locker->name);
          locker->name = STRALLOC(buf);
          sprintf(buf, "%s's locker", ch->name);
          //sprintf(buf, locker->short_descr, ch->name);
          STRFREE( locker->short_descr);
          locker->short_descr = STRALLOC(buf);
	  sprintf( buf, "%s's locker is here.", ch->name );
          //sprintf(buf, locker->description, ch->name);
          STRFREE( locker->description);
          locker->description = STRALLOC(buf);
          obj_to_room( locker, ch->in_room );
        }
        act(AT_TELL, "$n opens $s locker.",ch, NULL, NULL, TO_ROOM);
 	act(AT_TELL, "You open your locker.",ch, NULL, NULL, TO_CHAR);
	return;
     }
    else if( !str_cmp( arg, "close" ) ) 
    {
        if(ch->in_room->vnum != ROOM_LOCKER )
        {
            send_to_char( "You are not currently in the locker room.\r\n", ch );
            return;
        }

	locker = get_obj_list( ch, ch->name, ch->in_room->first_content );

	if (!locker)
	{
	  send_to_char("Your locker is not open.\n\r",ch);
	  return;
	}
	else
	{
	  act(AT_TELL, "$n closes $s locker.",ch, NULL, NULL, TO_ROOM);
	  act(AT_TELL, "You close your locker.", ch, NULL, NULL, TO_CHAR);
          fwrite_locker( ch, locker );
          extract_obj(locker);
        }
	return;
     }    
    send_to_char( "Syntax: Locker <Close|Open>\n\rWhere 'Open' invokes your locker and 'Close' saves it.\n\r", ch );
    return;    
}

