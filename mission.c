#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mud.h"

bool     unequal_rank(CHAR_DATA *ch, CHAR_DATA *victim);
ASSIGNMENT_DATA *generate_assignment(CHAR_DATA *ch);

struct reward_type
{
	char *name;
	char *keyword;
	int cost;
	bool object;
	unsigned long long value;
	void *      where;
};

/* Descriptions of mission items go here:
Format is: "short" "keywords","Long description" */

const struct mission_desc_type mission_desc[] =
{
{
"the Sceptre of Courage",
"mission sceptre",
"The Sceptre of Courage is lying here, waiting to be returned to its owner."
},

{
"the Crown of Wisdom",
"mission crown",
"The Crown of Wisdom is lieing here, waiting to be returned to its owner."
},

{
"the Gauntlets of Strength",
"mission gauntlet",
"The Gauntlets of Strength are lying here, waiting to be returned to its owner."
},

{
"the Holy Grail",
"mission holy grail",
"The Holy Grail is lying here, waiting to be returned to its owner."
},

{
"the Cataclysm Gem",
"mission cataclysm gem",
"The Cataclysm gem is lying here, waiting to be returned to its owner."
},

{NULL, NULL, NULL}
};

/* Local functions */
bool generate_mission	args((CHAR_DATA *ch, CHAR_DATA *missionman));
void mission_update	args((void));
bool mission_level_diff   args((int clevel, int mlevel));

/* The main mission function */
void do_mission(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *missionman;
	OBJ_DATA *obj = NULL, *obj_next;
	OBJ_INDEX_DATA *missioninfoobj;
	MOB_INDEX_DATA *missioninfo;
	MOB_INDEX_DATA *missionwanker;
	ROOM_INDEX_DATA *missionroom;
	char buf[MAX_STRING_LENGTH];
	char arg1[MAX_INPUT_LENGTH];
	char arg2[MAX_INPUT_LENGTH];
	int i;

	/* Add your rewards here.  Works as follows:
	"Obj name shown when Mission list is typed", "keywords for buying",
	"Amount of mission points",  Does it load an object?,  IF it loads an
	object, then the vnum, otherwise a value to set in the next thing,  This
	is last field is a part of the char_data to be modified */

	const struct reward_type reward_table[] =
	{

	  { "A cookie",                 "cookie",                5,  TRUE,      90,                  0 },
	  { "A steel overdrive",        "steel overdrive",      15,  TRUE,    3101,                  0 },
	  { "An owner plushie",         "Owner plushie",        20,  TRUE,      80,                  0 },
	  { "A mega tonic",             "mega tonic",           25,  TRUE,    2026,                  0 },
	  { "A magic Ring",             "magic ring",           30,  TRUE,      64,                  0 },
	  { "50,000 Zeni",              "50000",                40,  FALSE,  50000,                  &ch->gold },
	  { "3 Pkills",                 "3 Pkills",             50,  FALSE,      3,                  &ch->pkills },
	  { "20 Strength",              "strength",             75,  FALSE,     15,                  &ch->perm_str },
	  { "20 Wisdom",                "wisdom",               85,  FALSE,     15,                  &ch->perm_wis },
	  { "20 Luck",                  "luck",                 95,  FALSE,     15,                  &ch->perm_lck },
	  { "A senzu bean",             "senzu bean",          100,  TRUE,      56,                  0 },
	  { "Orphaned Tuffle eggs",     "tuffle eggs",         100,  FALSE,      2,                  &ch->eggslaid },
	  { "A mystical hat",           "mystical hat",        125,  TRUE,    1328,                  0 },
	  { "Reputation Up 150",        "reputation up",       150,  FALSE,    100,                  &ch->alignment },
	  { "Reputation Down 150",      "down",                150,  FALSE,   -100,                  &ch->alignment },
	  { "A power necklace",         "power necklace",      175,  TRUE,    3118,                  0 },
	  { "A spacesuit",              "spacesuit",           200,  TRUE,    3850,                  0 },
	  { "A power charm",            "power charm",         200,  TRUE,    3119,                  0 },
	  { "50 Charisma",              "charisma",            225,  FALSE,     50,                  &ch->perm_cha },
	  { "50 Intelligence",          "intelligence",        250,  FALSE,     50,                  &ch->perm_int },
	  { "50 Constitution",          "constitution",        275,  FALSE,     50,                  &ch->perm_con },
	  { "75 Dexterity",		        "dexterity",	       300,  FALSE,     75,                  &ch->perm_dex },
	  { "750,000 Zeni",             "750000",              350,  FALSE,  750000,                 &ch->gold },
	  { "A big wad of cash",	    "wad cash", 	       400,  FALSE,   ch->gold * 0.3,        &ch->gold },
	  { "A sainted reputation",     "sainted",             450,  FALSE,    1000,                 &ch->alignment },
	  { "A demonic reputation",     "demonic",             450,  FALSE,   -1000,                 &ch->alignment },
	  { "A hellfighter chip",   	"Hellfighter Chip",    500,  TRUE,    1393,                  0 },
	  { "A cosmic item",            "Cosmic Item",         500,  TRUE,      81,                  0 },
	  { "Unstoppable force",        "unstoppable force",   750,  FALSE,    200,                  &ch->perm_str },
	  { "Windborne speed",          "windborne speed",     750,  FALSE,    200,                  &ch->perm_dex },
	  { "Genius intellect",         "genius intellect",    750,  FALSE,    200,                  &ch->perm_int },
	  { "Sagely insight",           "sagely insight",      750,  FALSE,    200,                  &ch->perm_wis },
	  { "Heroic survivability",     "heroic survivability",750,  FALSE,    200,                  &ch->perm_con },
	  { "Animal magnetism",         "animal magnetism",    750,  FALSE,    200,                  &ch->perm_cha },
	  { "Insight of gamblers",      "insight gamblers",    750,  FALSE,    200,                  &ch->perm_lck },
	  { "A clanstone",		"clanstone",	      1000,  TRUE,	23,		     0 },
	  { NULL, NULL, 0, FALSE, 0, 0  } /* Never remove this!!! */
	};


	argument = one_argument(argument, arg1);
	argument = one_argument(argument, arg2);

	if (IS_NPC(ch))
	{
		send_to_char("NPC's can't mission.\n\r", ch); return;
	}

	if (arg1[0] == '\0')
	{
		send_to_char("Mission commands: Time, Request, Complete, Quit, List, and Buy.\n\r", ch);
		send_to_char("For more information, type 'Help Mission'.\n\r", ch);
		return;
	}

	if (!strcmp(arg1, "info"))
	{
		if (xIS_SET(ch->act, PLR_MISSIONING))
		{
			if (ch->pcdata->missionmob == -1 || ch->pcdata->missionzeni == -1 || ch->pcdata->missionroom == -1 || ch->pcdata->missionobj == -1)
			{
				missionwanker = get_mob_index(ch->pcdata->missiongiver);
				sprintf(buf, "Your mission is ALMOST complete!\n\rGet back to %s before your time runs out!\n\r", (missionwanker->short_descr ? missionwanker->short_descr : " "));
				send_to_char(buf, ch);
				return;
			}
			if (ch->pcdata->missionroom > 0)
			{
				if ((missionroom = get_room_index(ch->pcdata->missionroom)) == NULL)
				{
					sprintf(buf, "You are on a mission to check out %s!\n\r", missionroom->name);
					send_to_char(buf, ch);
					return;
				}
				else
				{
					send_to_char("You aren't currently on a mission.\n\r", ch);
					return;
				}
			}
			if (ch->pcdata->missionzeni > 0)
			{
				sprintf(buf, "You are on a mission to collect %s zeni!\n\r", num_punct(ch->pcdata->missionzeni));
				send_to_char(buf, ch);
				sprintf(buf, "You have %s so far!\n\r", num_punct(ch->pcdata->missionzenicollected));
				send_to_char(buf, ch);
				return;
			}

			if (ch->pcdata->missionobj > 0)
			{
				missioninfoobj = get_obj_index(ch->pcdata->missionobj);
				if (missioninfoobj != NULL)
				{
					sprintf(buf, "You are on a mission to recover the fabled %s!\n\r", missioninfoobj->name);
					send_to_char(buf, ch);
				}
				else
				{
					send_to_char("You aren't currently on a mission.\n\r", ch);
				}
				return;
			}
			if (ch->pcdata->missionmob > 0)
			{
				missioninfo = get_mob_index(ch->pcdata->missionmob);
				if (missioninfo != NULL)
				{
					sprintf(buf, "You are on a mission to slay the dreaded %s!\n\r", missioninfo->short_descr);
					send_to_char(buf, ch);
				}
				else
				{
					send_to_char("You aren't currently on a mission.\n\r", ch);
				}
				return;
			}
		}
		else
			send_to_char("You aren't currently on a mission.\n\r", ch);
		return;
	}
	if (!strcmp(arg1, "time"))
	{
		if (!xIS_SET(ch->act, PLR_MISSIONING))
		{
			send_to_char("You aren't currently on a mission.\n\r", ch);
			if (ch->pcdata->nextmission > 1)
			{
				sprintf(buf, "There are %d minutes remaining until you can "
					"go on another mission.\n\r", ch->pcdata->nextmission);
				send_to_char(buf, ch);
			}
			else if (ch->pcdata->nextmission == 1)
			{
				sprintf(buf, "There is less than a minute remaining until you can go on another mission.\n\r");
				send_to_char(buf, ch);
			}
		}
		else if (ch->pcdata->countdown > 0)
		{
			sprintf(buf, "Time left for current mission: %d\n\r", ch->pcdata->countdown);
			send_to_char(buf, ch);
		}
		return;
	}

	for (missionman = ch->in_room->first_person; missionman != NULL; missionman = missionman->next_in_room)
	{
		if (!IS_NPC(missionman)) continue;
		if (xIS_SET(missionman->act, ACT_AUTONOMOUS))
			break;
	}

	if (missionman == NULL || missionman->spec_fun != spec_lookup("spec_mission_master"))
	{
		send_to_char("You can't do that here.\n\r", ch);
		return;
	}

	if (who_fighting(missionman) != NULL)
	{
		send_to_char("Wait until the fighting stops.\n\r", ch);
		return;
	}

	ch->pcdata->missiongiver = missionman->pIndexData->vnum;

	if (!strcmp(arg1, "list"))
	{
		act(AT_GREY, "$n asks $N for a list of mission items.", ch, NULL, missionman, TO_ROOM);
		act(AT_GREY, "You ask $N for a list of mission items.", ch, NULL, missionman, TO_CHAR);
		send_to_char("Current mission Items available for Purchase:\n\r", ch);
		if (reward_table[0].name == NULL)
			send_to_char("  Nothing.\n\r", ch);
		else {
			send_to_char("  &w[&WCost&w]     [&BName&w]\n\r", ch);
			for (i = 0;reward_table[i].name != NULL;i++)
			{
				sprintf(buf, "   &W%-4d&w       &b%s&w\n\r"
					, reward_table[i].cost, reward_table[i].name);
				send_to_char(buf, ch);
			}
			send_to_char("\n\rTo buy an item, type 'Mission buy <item>'.\n\r", ch);
			return;
		}
	}
	else if (!strcmp(arg1, "buy"))
	{
		bool found = FALSE;
		if (arg2[0] == '\0')
		{
			send_to_char("To buy an item, type 'Mission buy <item>'.\n\r", ch);
			return;
		}
		/* Use keywords rather than the name listed in Mission list */
		/* Do this to avoid problems with something like 'Mission buy the' */
		/* And people getting things they don't want... */
		for (i = 0;reward_table[i].name != NULL;i++)
			if (is_name(arg2, reward_table[i].keyword))
			{
				found = TRUE;
				if (ch->pcdata->missionkili >= reward_table[i].cost)
				{
					OBJ_INDEX_DATA *bleh;
					if (reward_table[i].object)
					{
						bleh = get_obj_index(reward_table[i].value);
						obj = create_object(bleh, 0);
						obj->level = obj->pIndexData->level;
						if (ch->basepl < obj->level)
						{
							send_to_char("You cannot use the item you're trying to buy.\n\r", ch);
							return;
						}
						ch->pcdata->missionkili -= reward_table[i].cost;
					}
					else
					{
						ch->pcdata->missionkili -= reward_table[i].cost;
						sprintf(buf, "In exchange for %d Mission Kili, %s gives you %s.\n\r",
							reward_table[i].cost, missionman->short_descr, reward_table[i].name);
						send_to_char(buf, ch);
						*(int *)reward_table[i].where += reward_table[i].value;
						if (ch->alignment > 1000)	ch->alignment = 1000;
						if (ch->alignment < -1000)	ch->alignment = -1000;
					}
					break;
				}
				else
				{
					sprintf(buf, "Sorry, %s, but you don't have enough Mission Kili for that.", ch->name);
					do_say(missionman, buf);
					return;
				}
			}
		if (!found)
		{
			sprintf(buf, "I don't have that item, %s.", ch->name);
			do_say(missionman, buf);
		}
		if (obj != NULL)
		{
			sprintf(buf, "In exchange for %d Mission Kili, %s gives you %s.\n\r",
				reward_table[i].cost, missionman->short_descr, obj->short_descr);
			send_to_char(buf, ch);
			obj_to_char(obj, ch);
		}
		return;
	}
	else if (!strcmp(arg1, "request"))
	{
		act(AT_GREY, "$n asks $N for a mission.", ch, NULL, missionman, TO_ROOM);
		act(AT_GREY, "You ask $N for a mission.", ch, NULL, missionman, TO_CHAR);
		if (xIS_SET(ch->act, PLR_MISSIONING))
		{
			sprintf(buf, "But you're already on a mission!");
			do_say(missionman, buf);
			return;
		}
		if (ch->pcdata->nextmission > 0)
		{
			sprintf(buf, "You're very brave, %s, but let someone else have a chance.", ch->name);
			do_say(missionman, buf);
			sprintf(buf, "Come back later.");
			do_say(missionman, buf);
			return;
		}

		sprintf(buf, "Thank you, brave %s!", ch->name);
		if (!xIS_SET(ch->act, PLR_MISSIONING))
			do_say(missionman, buf);
		ch->pcdata->missionmob = 0;
		ch->pcdata->missionobj = 0;
		ch->pcdata->missionroom = 0;
		ch->pcdata->missionzeni = 0;
		if (!generate_mission(ch, missionman))
			return;

		if (ch->pcdata->missionmob > 0 || ch->pcdata->missionobj > 0 || ch->pcdata->missionzeni > 0 || ch->pcdata->missionroom > 0)
		{
			ch->pcdata->countdown = number_range(12, 22);
			if (IS_SET(ch->pcdata->flags, PCFLAG_MANIAC))
				ch->pcdata->countdown -= 7;
			xSET_BIT(ch->act, PLR_MISSIONING);
			sprintf(buf, "You have %d minutes to complete this mission.", ch->pcdata->countdown);
			do_say(missionman, buf);
			sprintf(buf, "May Yami be with you!  Or... whoever your less important deity is.");
			do_say(missionman, buf);
		}
		return;
	}

	else if (!strcmp(arg1, "complete"))
	{
		act(AT_GREY, "$n informs $N $e has completed $s mission.", ch, NULL, missionman, TO_ROOM);
		act(AT_GREY, "You inform $N you have completed $s mission.", ch, NULL, missionman, TO_CHAR);
		if (ch->pcdata->missiongiver != missionman->pIndexData->vnum)
		{
			sprintf(buf, "I never sent you on a mission! Perhaps you're thinking of someone else.");
			do_say(missionman, buf);
			return;
		}

		if (xIS_SET(ch->act, PLR_MISSIONING))
		{
			bool obj_found = FALSE;
			if (ch->pcdata->missionobj > 0 && ch->pcdata->countdown > 0)
			{
				for (obj = ch->first_carrying; obj != NULL; obj = obj_next)
				{
					obj_next = obj->next_content;

					if (obj != NULL && obj->pIndexData->vnum == ch->pcdata->missionobj)
					{
						obj_found = TRUE;
						break;
					}
				}
			}

			if (ch->pcdata->missionzeni == -1 && ch->gold < ch->pcdata->missionzenicollected)
			{
				sprintf(buf, "But you don't have the zeni I requested!");
				do_say(missionman, buf);
				return;
			}

			if ((ch->pcdata->missionmob == -1 || (ch->pcdata->missionobj && obj_found) || ch->pcdata->missionroom == -1 || ch->pcdata->missionzeni == -1)
				&& ch->pcdata->countdown > 0)
			{
				int reward, pointreward;
				reward = number_range(2500, 10000);
				pointreward = number_range(2, 6);
				sprintf(buf, "Congratulations on completing your mission!");
				do_say(missionman, buf);
				sprintf(buf, "As a reward, I am giving you %d mission kili, and %d zeni.", pointreward, reward);
				do_say(missionman, buf);
				if (can_gain(ch))
				{
					ch_printf(ch, "You gain %s pl from your successful mission.\n\r", format_pl(sqrt(ch->basepl) / 2));
					ch->basepl += sqrt(ch->basepl) / 2;
				}
				act(AT_YELLOW, "A golden aura flares up around $n as $e gains some power and standing.", ch, NULL, NULL, TO_CANSEE);
				sprintf(buf, "Thank you, %s!  You'll be a famous hero in time.", ch->name);
				do_say(missionman, buf);
				law_broken(ch, 6);
				xREMOVE_BIT(ch->act, PLR_MISSIONING);
				if (ch->pcdata->missionzenicollected > 0)
					ch->gold -= ch->pcdata->missionzenicollected;
				ch->pcdata->missiongiver = 0;
				ch->pcdata->countdown = 0;
				ch->pcdata->missionmob = 0;
				ch->pcdata->missionobj = 0;
				ch->pcdata->missionroom = 0;
				ch->pcdata->missionzeni = 0;
				ch->pcdata->missionzenicollected = 0;
				ch->pcdata->nextmission = 5;
				ch->gold += reward;
				ch->pcdata->missionkili += pointreward;
				ch->pcdata->totalmissionkili += pointreward;
				if (obj_found)
				{
					obj_from_char(obj);
					obj_to_room(obj, get_room_index(3));
				}
				return;
			}
			else if ((ch->pcdata->missionmob > 0 || ch->pcdata->missionobj > 0 || ch->pcdata->missionroom > 0 || ch->pcdata->missionzeni > 0)
				&& ch->pcdata->countdown > 0)
			{
				sprintf(buf, "You haven't completed the mission yet, but there is still time!");
				do_say(missionman, buf);
				return;
			}
		}
		if (ch->pcdata->nextmission > 0)
			sprintf(buf, "But you didn't complete your mission in time!");
		else
			sprintf(buf, "You have to request a mission first, %s.", ch->name);
		do_say(missionman, buf);
		return;
	}

	else if (!strcmp(arg1, "quit"))
	{
		act(AT_GREY, "$n informs $N $e wishes to quit $s mission.", ch, NULL, missionman, TO_ROOM);
		act(AT_GREY, "You inform $N you wish to quit $s mission.", ch, NULL, missionman, TO_CHAR);
		if (ch->pcdata->missiongiver != missionman->pIndexData->vnum)
		{
			sprintf(buf, "I never sent you on a mission! Perhaps you're thinking of someone else.");
			do_say(missionman, buf);
			return;
		}

		if (xIS_SET(ch->act, PLR_MISSIONING))
		{
			xREMOVE_BIT(ch->act, PLR_MISSIONING);
			ch->pcdata->missiongiver = 0;
			ch->pcdata->countdown = 0;
			ch->pcdata->missionmob = 0;
			ch->pcdata->missionzeni = 0;
			ch->pcdata->missionroom = 0;
			ch->pcdata->missionobj = 0;
			if (!IS_IMMORTAL(ch))
				ch->pcdata->nextmission = 15;
			sprintf(buf, "Your mission is over, but for your cowardly behavior, you may not mission again for 15 minutes.");
			do_say(missionman, buf);
			return;
		}
		else
		{
			send_to_char("You aren't on a mission!", ch);
			return;
		}
	}


	send_to_char("Mission commands: Time, Request, Complete, Quit, List, and Buy.\n\r", ch);
	send_to_char("For more information, type 'Help Mission'.\n\r", ch);
	return;
}

bool generate_mission(CHAR_DATA *ch, CHAR_DATA *missionman)
{
	CHAR_DATA *victim, *mob;
	MOB_INDEX_DATA *vsearch;
	ROOM_INDEX_DATA *room;
	OBJ_DATA *missionitem;
	char buf[MAX_STRING_LENGTH];
	long mcounter;
	int mob_vnum;

	for (mcounter = 0; mcounter < 99999; mcounter++)
	{
		mob_vnum = number_range(100, 32600);

		if ((vsearch = get_mob_index(mob_vnum)) != NULL)
		{
			mob = get_char_world(ch, vsearch->player_name);
			if (mob && IS_NPC(mob)
				&& mission_level_diff(ch->level, mob->level) == TRUE
				&& vsearch->pShop == NULL
				&& vsearch->rShop == NULL
				&& !xIS_SET(mob->act, ACT_PACIFIST)
				&& !xIS_SET(mob->act, ACT_PRACTICE)
				&& !xIS_SET(mob->act, ACT_PET)
				&& !xIS_SET(mob->affected_by, AFF_CHARM)
				&& !xIS_SET(mob->affected_by, AFF_INVISIBLE)
				&& !xIS_SET(mob->act, ACT_IMMORTAL)
				&& !xIS_SET(mob->act, ACT_PROTOTYPE)
				&& mob->basepl < ch->basepl * 50
				&& number_percent() < 40
				&& !str_cmp(mob->in_room->area->planet, ch->in_room->area->planet)) break;
			else vsearch = NULL;
		}
	}

	if (vsearch == NULL || (victim = get_char_world(ch, vsearch->player_name)) == NULL)
	{
		sprintf(buf, "I'm sorry, but I don't have any missions for you at this time.");
		do_say(missionman, buf);
		sprintf(buf, "Try again later.");
		do_say(missionman, buf);
		return FALSE;
	}

	if ((room = find_location(ch, victim->name)) == NULL)
	{
		sprintf(buf, "I'm sorry, but I don't have any missions for you at this time.");
		do_say(missionman, buf);
		sprintf(buf, "Try again later.");
		do_say(missionman, buf);
		return FALSE;
	}

	/*  40% chance it will send the player on a 'recover item' mission. */
	// 25% item
	// 35% Kill Mob
	// 25% go to room
	// 15% collect Zeni
	switch (number_range(1, 20))
	{
	case 1: case 2: case 3: case 4: case 5:
	{
		int numobjs = 0;
		int descnum = 0;
		for (numobjs = 0;mission_desc[numobjs].name_descr != NULL;numobjs++)
			;
		numobjs--;
		descnum = number_range(0, numobjs);
		missionitem = create_object(get_obj_index(97), ch->basepl);
		if (descnum > -1)
		{
			if (missionitem->short_descr)
				STRFREE(missionitem->short_descr);
			if (missionitem->description)
				STRFREE(missionitem->description);
			if (missionitem->name)
				STRFREE(missionitem->name);

			missionitem->name = STRALLOC(mission_desc[descnum].name_descr);
			missionitem->description = STRALLOC(mission_desc[descnum].long_descr);
			missionitem->short_descr = STRALLOC(mission_desc[descnum].short_descr);
			xSET_BIT(missionitem->extra_flags, ITEM_MISSION);
		}
		obj_to_room(missionitem, room);
		ch->pcdata->missionobj = missionitem->pIndexData->vnum;
		sprintf(buf, "Vile pilferers have stolen %s from the my treasury!", missionitem->short_descr);
		do_say(missionman, buf);
		do_say(missionman, "My computer has pinpointed its location.");
		sprintf(buf, "Look in the general area of %s for %s!", room->area->name, room->name);
		do_say(missionman, buf);
		return TRUE;
		break;
	}
	case 6: case 7: case 8: case 9: case 10: case 11: case 12:
	{
		switch (number_range(0, 1))
		{
		case 0:
			sprintf(buf, "An enemy of mine, %s, is making vile threats against me.", victim->short_descr);
			do_say(missionman, buf);
			sprintf(buf, "This threat must be eliminated!");
			do_say(missionman, buf);
			break;
		case 1:
			sprintf(buf, "The most heinous criminal, %s, has escaped from the dead zone!", victim->short_descr);
			do_say(missionman, buf);
			sprintf(buf, "Since the escape, %s has murdered %d civillians!", victim->short_descr, number_range(2, 20));
			do_say(missionman, buf);
			do_say(missionman, "The penalty for this crime is death, and you are to deliver the sentence!");
			break;
		}
		if (room->name != NULL)
		{
			sprintf(buf, "Seek %s out somewhere in the vicinity of %s!", victim->short_descr, room->name);
			do_say(missionman, buf);
			sprintf(buf, "That location is in the general area of %s.", room->area->name);
			do_say(missionman, buf);
		}
		ch->pcdata->missionmob = victim->pIndexData->vnum;
		return TRUE;
		break;
	}
	case 13: case 14: case 15:
	{
		for (mcounter = 0; mcounter < 99999; mcounter++)
		{
			room = get_room_index(number_range(100, 11000));
			if (!room || room == NULL || str_cmp(ch->in_room->area->planet, room->area->planet) || !str_cmp(room->area->name, "New South City")
				|| !str_cmp(room->name, "Floating in a void") || room->first_exit == NULL)
				continue;
			else
				break;
		}
		sprintf(buf, "I need someone to do some recon work in %s.", room->area->name);
		do_say(missionman, buf);
		sprintf(buf, "Go to %s and take a look around, then return to me.", room->name);
		do_say(missionman, buf);
		ch->pcdata->missionroom = room->vnum;
		return TRUE;
	}
	case 16: case 17: case 18: case 19: case 20:
	{
		ch->pcdata->missionzeni = number_range(500, 5000);
		sprintf(buf, "My organisation is a little strapped for cash at present...");
		do_say(missionman, buf);
		sprintf(buf, "I realise it's not entirely ethical, but I need you to...");
		do_say(missionman, buf);
		sprintf(buf, "Loot a total of %d zeni and return it to me.", ch->pcdata->missionzeni);
		do_say(missionman, buf);
		sprintf(buf, "Only money taken from dead citizens will do!  We must teach the ignorant masses a lesson!");
		do_say(missionman, buf);
		ch->pcdata->missionzenicollected = 0;
		return TRUE;
	}

	}
	return TRUE;
}

bool mission_level_diff(int clevel, int mlevel)
{
	return TRUE;

}

/* Called from update_handler() by pulse_area */

void mission_update(void)
{
	DESCRIPTOR_DATA *d;
	CHAR_DATA *ch;
	char buf[MAX_STRING_LENGTH];

	for (d = first_descriptor; d != NULL; d = d->next)
	{
		if (d->character != NULL && d->connected == CON_PLAYING)
		{
			ch = d->character;
			if (IS_NPC(ch))
				continue;
			if (ch->pcdata->nextmission > 0)
			{
				ch->pcdata->nextmission--;
				if (ch->pcdata->nextmission == 0)
				{
					send_to_char("You may now mission again.\n\r", ch);
					return;
				}
			}
			else if (xIS_SET(ch->act, PLR_MISSIONING))
			{
				if (--ch->pcdata->countdown <= 0)
				{
					ch->pcdata->nextmission = 10;
					sprintf(buf, "You have run out of time for your mission!\n\rYou may mission again in %d minutes.\n\r", ch->pcdata->nextmission);
					send_to_char(buf, ch);
					xREMOVE_BIT(ch->act, PLR_MISSIONING);
					ch->pcdata->missiongiver = 0;
					ch->pcdata->countdown = 0;
					ch->pcdata->missionmob = 0;
					ch->pcdata->missionobj = 0;
				}
				if (ch->pcdata->countdown > 0 && ch->pcdata->countdown < 6)
				{
					send_to_char("Better hurry, you're almost out of time for your mission!\n\r", ch);
					return;
				}
			}
		}
	}
	return;
}


void do_crusade(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *missionman, *target = NULL, *victim;
	OBJ_INDEX_DATA 	*pObjIndex;
	OBJ_DATA 		*obj;
	char buf[MAX_STRING_LENGTH];

	for (missionman = ch->in_room->first_person; missionman != NULL; missionman = missionman->next_in_room)
	{
		if (!IS_NPC(missionman))
			continue;
		if (xIS_SET(missionman->act, ACT_SCHOLAR))
			break;
	}

	if (ch->alignment < 333 || IS_NPC(ch) || ch->basepl < 1000000)
	{
		send_to_char("Crusades are for GOOD aligned fighters only.\n\r", ch);
		return;
	}

	if (!str_cmp(argument, "check"))
	{
		if (!ch->pcdata->crusade || !str_cmp(ch->pcdata->crusade, "(null)"))
		{
			send_to_char("You are not on a crusade.\n\r", ch);
			pager_printf(ch, "Your last crusade was on month %d of this year.\n\r", ch->pcdata->crusade_day);
			return;
		}
		if (!str_cmp(ch->pcdata->crusade, "complete"))
		{
			send_to_char("You have completed your crusade.  Return to the Supreme Kai for rewarding.\n\r", ch);
			return;
		}
		pager_printf(ch, "You are on a crusade against %s.\n\r", ch->pcdata->crusade);
		return;
	}

	if (missionman == NULL || !xIS_SET(missionman->act, ACT_SCHOLAR))
	{
		send_to_char("You can't do that here - Find the Supreme Kai.\n\r", ch);
		return;
	}
	if (!str_cmp(argument, "request"))
	{
		if (ch->pcdata->crusade_day == time_info.month  && ch->pcdata->crusade_day != 0)
		{
			do_say(missionman, "Sorry, but you've already had a crusade this month.");
			return;
		}

		if (ch->pcdata->crusade && str_cmp(ch->pcdata->crusade, "(null)"))
		{
			do_say(missionman, "Finish the last crusade I asked you about first.");
			return;
		}

		for (victim = first_char; victim; victim = victim->next)
		{
			if (!IS_NPC(victim)
				&& (!unequal_rank(ch, victim)
					|| IS_SET(victim->pcdata->flags, PCFLAG_MANIAC)
					|| IS_SET(victim->pcdata->flags, PCFLAG_MANIAC))
				&& victim->alignment < -333
				&& victim->basepl > 1000000
				&& !IS_IMMORTAL(victim))
				if (target == NULL || number_range(1, 3) == 2)
					target = victim;
		}

		if (!target)
		{
			do_say(missionman, "There are no unchecked evil powers you can deal with right now.");
			return;
		}

		sprintf(buf, "Ah!  I need your help, %s.  Curently, %s is on a mad rampage, and must be stopped!  I want YOU to perform this task.  Good luck, my friend.", ch->name, target->name);
		do_say(missionman, buf);
		ch->pcdata->crusade = STRALLOC(target->name);
		ch->pcdata->crusade_day = time_info.month;
		return;
	}
	if (!str_cmp(argument, "cancel"))
	{
		do_say(missionman, "Very well.");
		ch->pcdata->crusade = STRALLOC("(null)");
		return;
	}
	if (!str_cmp(argument, "complete"))
	{
		if (str_cmp(ch->pcdata->crusade, "complete"))
		{
			do_say(missionman, "You may want to actually finish your crusade before telling me you have.");
			return;
		}
		ch->pcdata->crusade = STRALLOC("(null)");
		switch (number_range(1, 20))
		{
		case 1:
		case 2:
		case 3:
		case 4:
			do_say(missionman, "I bestow on you this Halo of the Greater Gods!");
			pObjIndex = get_obj_index(5855);
			obj = create_object(pObjIndex, pObjIndex->level);
			obj = obj_to_char(obj, ch);
			break;
		case 5:
		case 6:
			do_say(missionman, "I bestow on you this pair of Angel Wings!");
			pObjIndex = get_obj_index(5857);
			obj = create_object(pObjIndex, pObjIndex->level);
			obj = obj_to_char(obj, ch);
			break;
		case 7:
			do_say(missionman, "I bestow on you this pair of Castrated Potarra Earrings!");
			pObjIndex = get_obj_index(5856);
			obj = create_object(pObjIndex, pObjIndex->level);
			obj = obj_to_char(obj, ch);
			break;
		case 8:
		case 9:
		case 10:
			do_say(missionman, "I bestow on you some of this money stuff you people seem to like!");
			ch->gold += 10000;
			break;
		case 11:
		case 12:
			do_say(missionman, "I bestow on you some more of this money stuff you people seem to like!");
			ch->gold += 30000;
			break;
		case 13:
			do_say(missionman, "I bestow on you a load of this money stuff you people seem to like!");
			ch->gold += 10000;
			break;
		case 14:
			do_say(missionman, "I'm feeling unoriginal - Have 10 strength!");
			ch->perm_str += 10;
			break;
		case 15:
			do_say(missionman, "I'm feeling unoriginal - Have 10 intelligence!");
			ch->perm_int += 10;
			break;
		case 16:
			do_say(missionman, "I'm feeling unoriginal - Have 10 wisdom!");
			ch->perm_wis += 10;
			break;
		case 17:
			do_say(missionman, "I'm feeling unoriginal - Have 10 dexterity!");
			ch->perm_dex += 10;
			break;
		case 18:
			do_say(missionman, "I'm feeling unoriginal - Have 10 constitution!");
			ch->perm_con += 10;
			break;
		case 19:
			do_say(missionman, "I'm feeling unoriginal - Have 10 charisma!");
			ch->perm_cha += 10;
			break;
		case 20:
			do_say(missionman, "I'm feeling unoriginal - Have 10 luck!");
			ch->perm_lck += 10;
			break;
		}
		return;
	}
	send_to_char("Syntax: Crusade request|complete|check|cancel\n\r", ch);
	return;
}


#define MAINFRAME 4407

void do_assignment(CHAR_DATA *ch, char *argument)
{
	OBJ_DATA *mainframe;
	OBJ_DATA *obj = NULL;
	char buf[MAX_STRING_LENGTH];
	char arg1[MAX_INPUT_LENGTH];
	char arg2[MAX_INPUT_LENGTH];
	int i;

	/* Add your rewards here.  Works as follows:
	"Obj name shown when Mission list is typed", "keywords for buying",
	"Amount of mission points",  Does it load an object?,  IF it loads an
	object, then the vnum, otherwise a value to set in the next thing,  This
	is last field is a part of the char_data to be modified */

	const struct reward_type reward_table[] =
	{

	  { "1:  A cookie",                 "1",              100,  TRUE,      90,                  0 },
	  { "2:  50 strength",              "2",            300,  FALSE,     50,                  &ch->perm_str },
	  { "3:  50 dexterity",             "3",           300,  FALSE,     50,                  &ch->perm_dex },
	  { "4:  50 constitution",          "4",        300,  FALSE,     50,                  &ch->perm_con },
	  { "5:  50 intelligence",          "5",        300,  FALSE,     50,                  &ch->perm_int },
	  { "6:  50 wisdom",                "6",              300,  FALSE,     50,                  &ch->perm_wis },
	  { "7:  50 charisma",              "7",            300,  FALSE,     50,                  &ch->perm_cha },
	  { "8:  50 luck",                  "8",                300,  FALSE,     50,                  &ch->perm_lck },
	  { "9:  A senzu bean",             "9",               500,  TRUE,      56,                  0 },
	  { "10: A big wad of cash",	    "10", 	       800,  FALSE,   ch->gold * 0.3,        &ch->gold },
	  { "11: A sainted reputation",     "11",            1000,  FALSE,    1000,                 &ch->alignment },
	  { "12: A demonic reputation",     "12",            1000,  FALSE,   -1000,                 &ch->alignment },
	  { "13: A hellfighter chip",       "13",   1500,  TRUE,    1393,                  0 },
	  { "14: 5 percent more strength",  "14",           4000,  FALSE,     ch->perm_str * 0.05,                  &ch->perm_str },
	  { "15: 5 percent more dexterity", "15",          4000,  FALSE,     ch->perm_dex * 0.05,                  &ch->perm_dex },
	  { "16: 5 percent more constitution", "16",       4000,  FALSE,     ch->perm_con * 0.05,                  &ch->perm_con },
	  { "17: 5 percent more intelligence", "17", 	      4000,  FALSE,     ch->perm_int * 0.05,                  &ch->perm_int },
	  { "18: 5 percent more wisdom",       "18",             4000,  FALSE,     ch->perm_wis * 0.05,                  &ch->perm_wis },
	  { "19: 5 percent more charisma",     "19",           4000,  FALSE,     ch->perm_cha * 0.05,                  &ch->perm_cha },
	  { "20: A clanstone",		"20",	      5000,  TRUE,	23,		     0 },

	  { NULL, NULL, 0, FALSE, 0, 0  } /* Never remove this!!! */
	};


	argument = one_argument(argument, arg1);
	argument = one_argument(argument, arg2);

	if (IS_NPC(ch))
	{
		send_to_char("NPCs can't get GPF assignments.\n\r", ch); return;
	}

	if (ch->pcdata->legality < CITIZENS_SCREAM_THRESHOLD)
	{
		send_to_char("The GPF does not employ those with outstanding criminal records.\n\r", ch); return;
	}

	if (arg1[0] == '\0')
	{
		send_to_char("Assignment commands: Info, Request, Complete, List, and Buy.\n\r", ch);
		send_to_char("For more information, type 'Help Mission'.\n\r", ch);
		return;
	}

	if (!strcmp(arg1, "info"))
	{
		ASSIGNMENT_DATA *assignment = ch->pcdata->assignment;
		if (assignment != NULL)
		{
			switch (assignment->type)
			{
			case ASSIGNMENT_PEACEKEEPING:
				pager_printf(ch, "You are currently on peacekeeping duty.\n\r");
				pager_printf(ch, "GPF central command has ordered you bring %d criminals to justice.\n\r", assignment->param1);
				pager_printf(ch, "You have caught %d criminals, and must deal with %d more to complete this assignment.\n\r", assignment->num_done, assignment->param1 - assignment->num_done);
				break;
			case ASSIGNMENT_PROPAGANDA:
				pager_printf(ch, "You are currently on propaganda dissemination duty.\n\r");
				pager_printf(ch, "GPF central command has ordered you hand out %d leaflets.\n\r", assignment->param1);
				pager_printf(ch, "You have handed out %d leaflets, and must hand out %d more to complete this assignment.\n\r", assignment->num_done, assignment->param1 - assignment->num_done);
				break;
			case ASSIGNMENT_CLEANUP:
				pager_printf(ch, "You are currently on cleanup duty.\n\r");
				pager_printf(ch, "GPF central command has ordered you clean up %d pieces of litter!\n\r", assignment->param1);
				pager_printf(ch, "You have cleaned up %d streets, but %d more require your attention.\n\r", assignment->num_done, assignment->param1 - assignment->num_done);
				break;
			case ASSIGNMENT_PATROL:
				pager_printf(ch, "You are currently on patrol duty.\n\r");
				pager_printf(ch, "GPF central command has ordered you patrol %d waypoints.\n\r", assignment->param1);
				pager_printf(ch, "You have checked %d already, but you need to check %d more to complete your assignment.\n\r", assignment->num_done, assignment->param1 - assignment->num_done);
				break;
			}
		}
		else
			send_to_char("You currently have no GPF assignment.\n\r", ch);
		return;
	}

	for (mainframe = ch->in_room->first_content; mainframe != NULL; mainframe = mainframe->next_content)
	{
		if (mainframe->pIndexData->vnum == MAINFRAME)
			break;
	}

	if (mainframe == NULL || mainframe->pIndexData->vnum != MAINFRAME)
	{
		send_to_char("You need the GPF mainframe to do that.\n\r", ch);
		return;
	}

	if (who_fighting(ch) != NULL)
	{
		send_to_char("Wait until the fighting stops.\n\r", ch);
		return;
	}

	if (!strcmp(arg1, "list"))
	{
		act(AT_GREY, "$n brings up a list of mission items on the mainframe computer.", ch, NULL, NULL, TO_ROOM);
		act(AT_GREY, "You type in the command to view a list of mission items available with your current peace kili the mainframe.", ch, NULL, NULL, TO_CHAR);
		send_to_char("Current assignment rewards available for you to purchase:\n\r", ch);
		if (reward_table[0].name == NULL)
			send_to_char("  Nothing.  Complete assignments to earn more peace kili to get more items.\n\r", ch);
		else
		{
			send_to_char("  &w[&WCost&w]     [&BName&w]\n\r", ch);
			for (i = 0;reward_table[i].name != NULL;i++)
			{
				sprintf(buf, "   &W%-4d&w       &B%s&w\n\r", reward_table[i].cost, reward_table[i].name);
				send_to_char(buf, ch);
			}
			send_to_char("\n\rTo buy an item, type 'Assignment buy <item number>'.\n\r", ch);
			return;
		}
	}
	else if (!strcmp(arg1, "buy"))
	{
		//	bool found=FALSE;
		int num = 0;
		int cnt = 0;

		if (arg2[0] == '\0')
		{
			send_to_char("To buy an item, type 'Assignment buy <item number>'.\n\r", ch);
			return;
		}

		num = atoi(arg2);

		if (num <= 0 || num >= 21)
		{
			send_to_char("To buy an item, type 'Assignment buy <item number>'.\n\r", ch);
			return;
		}

		cnt = num - 1;

		if (ch->pcdata->peacekili >= reward_table[cnt].cost)
		{
			OBJ_INDEX_DATA *bleh;
			if (reward_table[cnt].object)
			{
				bleh = get_obj_index(reward_table[cnt].value);
				obj = create_object(bleh, 0);
				obj->level = obj->pIndexData->level;
				if (ch->basepl < obj->level)
				{
					send_to_char("You cannot use the item you're trying to buy.\n\r", ch);
					return;
				}
				ch->pcdata->peacekili -= reward_table[cnt].cost;
				sprintf(buf, "In exchange for %d peace kili, the mainframe dispenses %s, which you take.\n\r",
					reward_table[cnt].cost, obj->short_descr);
				send_to_char(buf, ch);
				obj_to_char(obj, ch);
				return;
			}
			else
			{
				ch->pcdata->peacekili -= reward_table[cnt].cost;
				sprintf(buf, "In exchange for %d peace kili, the mainframe gives you %s.\n\r", reward_table[cnt].cost, reward_table[cnt].name);
				send_to_char(buf, ch);
				*(int *)reward_table[cnt].where += reward_table[cnt].value;
				if (ch->alignment > 1000)	ch->alignment = 1000;
				if (ch->alignment < -1000)	ch->alignment = -1000;
				return;
			}
		}
		else
		{
			pager_printf(ch, "You need %d more peace kili to buy '%s'\n\r", reward_table[cnt].cost - ch->pcdata->peacekili, reward_table[cnt].name);
			return;
		}
		return;
	}
	else if (!strcmp(arg1, "request"))
	{
		ASSIGNMENT_DATA *assig;
		act(AT_GREY, "$n types in an assignment request to the mainframe.", ch, NULL, NULL, TO_ROOM);
		act(AT_GREY, "You request an assignment from the mainframe.", ch, NULL, NULL, TO_CHAR);

		if (ch->pcdata->assignment)
		{
			ch_printf(ch, "But you've already been given an assignment!");
			return;
		}

		assig = generate_assignment(ch);
		ch->pcdata->assignment = assig;
		do_save(ch, "auto");
		return;
	}

	else if (!strcmp(arg1, "complete"))
	{
		act(AT_GREY, "$n enters the completion code for $s assignment into the mainframe.", ch, NULL, NULL, TO_ROOM);
		act(AT_GREY, "You enter the completion code for the assignment into the mainframe.", ch, NULL, NULL, TO_CHAR);

		if (!ch->pcdata->assignment)
		{
			send_to_char("Hey, you gave the computer a blue screen of death!  Did you even HAVE an assignment...?\n\r", ch);
			return;
		}

		if (ch->pcdata->assignment->num_done >= ch->pcdata->assignment->param1)
		{
			int reward, pointreward;
			reward = number_range(10000, 60000);
			pointreward = number_range(25, 50);
			pager_printf(ch, "'Congratulations on completing your assignment!', the mainframe displays.  'As a reward, GPF central command is giving you %d bonus peace kili, and %s zeni.", pointreward, format_pl(reward));
			if (can_gain(ch))
			{
				ch_printf(ch, "You gain %s pl from your successful assignment.\n\r", format_pl(sqrt(ch->basepl) * 6));
				ch->basepl += sqrt(ch->basepl) * 6;
			}
			act(AT_YELLOW, "A golden aura flares up around $n as $e gains some power and standing.", ch, NULL, NULL, TO_CANSEE);
			pager_printf(ch, "You also gain %d legality points.\n\r", 100);
			ch->pcdata->legality += 100;
			if (ch->pcdata->legality > 1000)
				ch->pcdata->legality = 1000;
			ch->pcdata->assignment = NULL;
			ch->gold += reward;
			ch->pcdata->peacekili += pointreward;
			ch->pcdata->peacekilitotal += pointreward;
		}
		else
		{
			pager_printf(ch, "You haven't completed the assignment yet.");
			return;
		}
		return;
	}

	send_to_char("Assignment commands: Info, Request, Complete, List, and Buy.\n\r", ch);
	send_to_char("For more information, type 'Help Mission'.\n\r", ch);
	return;
}


ASSIGNMENT_DATA *generate_assignment(CHAR_DATA *ch)
{
	ASSIGNMENT_DATA *assignment;
	CREATE(assignment, ASSIGNMENT_DATA, 1);

	switch (number_range(0, 3))
	{
	case ASSIGNMENT_PEACEKEEPING:
		/*		pager_printf( ch, "The computer tells you that you've been chosen for a peacekeeping assignment.\n\r" );
				pager_printf( ch, "You are required to kill 500 criminals from lawful zones around the galaxy.\n\r" );
				pager_printf( ch, "You can tell who is a criminal by examining them closely.\n\r" );
						assignment->param1  = 500;
						assignment->type    = ASSIGNMENT_PEACEKEEPING;
						assignment->num_done = 0;
				break;*/
	case ASSIGNMENT_PROPAGANDA:
		pager_printf(ch, "The computer tells you that you've been chosen for a propaganda dissemination assignment.\n\r");
		pager_printf(ch, "You are required to hand out 1000 leaflets to civilians in lawful zones around the galaxy.\n\r");
		pager_printf(ch, "You can find boxes of leaflets in the docking bay storage room.\n\r");
		assignment->param1 = 1000;
		assignment->type = ASSIGNMENT_PROPAGANDA;
		assignment->num_done = 0;
		break;
	case ASSIGNMENT_PATROL:
		/*		pager_printf( ch, "The computer tells you that you've been chosen for patrol duty.\n\r" );
				pager_printf( ch, "You are required to check 200 checkpoints around the galaxy.\n\r" );
				pager_printf( ch, "You can work out which areas are checkpoints by checking your map for a red !.\n\r" );
						assignment->param1  = 200;
						assignment->type    = ASSIGNMENT_PATROL;
						assignment->num_done = 0;
				break;*/
	case ASSIGNMENT_CLEANUP:
		pager_printf(ch, "The computer tells you that you've been chosen for a litter cleanup assignment.\n\r");
		pager_printf(ch, "You are required to collect 250 pieces of litter in lawful zones around the galaxy.\n\r");
		pager_printf(ch, "Be aware that dropping litter reduces the number you've picked up so far...\n\r");
		assignment->param1 = 250;
		assignment->type = ASSIGNMENT_CLEANUP;
		assignment->num_done = 0;
		break;
	}
	return assignment;
}

