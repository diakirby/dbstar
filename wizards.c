#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"

void sum_skullzombie(  CHAR_DATA *ch  );
void sum_metalwarrior( CHAR_DATA *ch  );
void sum_dragonslayer( CHAR_DATA *ch  );
void sum_soulreaper( CHAR_DATA *ch );
void sum_vampirelord( CHAR_DATA *ch );
void sum_vampirelich( CHAR_DATA *ch );
void sum_hercule( CHAR_DATA *ch );
void sum_blackguard( CHAR_DATA *ch );
void sum_helpoemer( CHAR_DATA *ch );
void sum_aedric( CHAR_DATA *ch );

void wiz_minorheal( CHAR_DATA *ch );
void wiz_greaterheal( CHAR_DATA *ch );
void wiz_mattersphere( CHAR_DATA *ch );
void wiz_manatap( CHAR_DATA *ch );
void wiz_bloodsuck( CHAR_DATA *ch );


void remove_summons(   CHAR_DATA *ch  );

void do_transform( CHAR_DATA *ch, char * argument )
{
 char arg[MAX_STRING_LENGTH];
 int  sn;

 if ( ch->race != RACE_WIZARD && !IS_IMMORTAL(ch) )
 {
      send_to_char( "Huh?\n\r", ch );
      return;
 }

 if ( IS_AFFECTED(ch, AFF_CHARGING) )
 {
     xREMOVE_BIT( ch->affected_by, AFF_CHARGING );
     send_to_char( "You stop charging.\n\r", ch );
 }

 sprintf( arg, argument );
 if ( arg==NULL || ( arg[0] == 'l' && arg[1] == 'i' ) )
 {
       pager_printf( ch, "&RTransformation list\n\r&W--------------------------------&C\n\r" );
       for ( sn = 0; sn < top_sn; sn++ )
       {
            if ( SPELL_FLAG( skill_table[sn], SF_TRANSFORM ) )
                 pager_printf( ch, "&w&W%-20s   &C%3d&w/&c%3d%%\n\r", skill_table[sn]->name, ch->pcdata->learned[sn], skill_table[sn]->race_adept[RACE_WIZARD] );
       }
       return;
 }

 if ( !str_cmp( arg, "Skull Zombie" ) )
    sum_skullzombie( ch );
 else if ( !str_cmp( arg, "Metal Warrior" ) )
    sum_metalwarrior( ch );
 else if ( !str_cmp( arg, "Dragon Slayer" ) )
    sum_dragonslayer( ch );
 else if ( !str_cmp( arg, "Soul Reaper" ) )
    sum_soulreaper( ch );
 else if ( !str_cmp( arg, "Vampire Lord" ) )
    sum_vampirelord( ch );
 else if ( !str_cmp( arg, "Vampire Lich" ) )
    sum_vampirelich( ch );
 else if ( !str_cmp( arg, "Hercule" ) )
    sum_hercule( ch );    
 else if ( !str_cmp( arg, "Blackguard" ) )
    sum_blackguard( ch );    
 else if ( !str_cmp( arg, "Hellish" ) )
    sum_helpoemer( ch );
 else if ( !str_cmp( arg, "Aedric" ) )
    sum_aedric( ch );            
 else if ( !str_cmp( arg, "Dismiss" ) )
 {
	remove_summons( ch );
	pager_printf( ch, "You dismiss your current transformation.\n\r" );
	return;
 }
 else
    pager_printf( ch, "Transform into what?\n\r", ch );

 update_current(ch, NULL);
 return;
}

void sum_skullzombie( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_skullzombie]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_skullzombie]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_skullzombie ) )
    {
        learn_from_success( ch, gsn_skullzombie );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  Suddenly, a darkness descends around you, clouding your vision momentarily, as you feel a sinister force enter your body.  Feeling somewhat violated, you wave the mist away and realise you've turned into a half-zombie, half-skeleton!\n\r" );
        act( AT_BLUE, "$n says some magic words, and surrounds $mself by mist as $e turns into one of the legions of the undead!\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     = 250;
        ch->multi_str = 100;
        ch->multi_con = 50;
        xSET_BIT( ch->affected_by, AFF_SKULLZOMBIE );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_skullzombie );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_blackguard( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_blackguard]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_blackguard]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_blackguard ) )
    {
        learn_from_success( ch, gsn_blackguard );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  A hellish picture invades your mind... A dark knight, battling throughout all creation... Suddenly, you realise... YOU ARE THAT KNIGHT!!\n\r" );
        act( AT_BLUE, "$n says some magic words, and surrounds $mself by mist as $e turns into a foul black knight with no morals.\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     = 2800;
        ch->multi_str = 1000;
        ch->multi_con = 500;
        ch->multi_dex = 500;        
        xSET_BIT( ch->affected_by, AFF_BLACKGUARD );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_blackguard );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_helpoemer( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_helpoemer]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_helpoemer]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_helpoemer ) )
    {
        learn_from_success( ch, gsn_helpoemer );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  A hellish vision of an undead poet fills your mind, as his work is celebrated by evil and filth throughout all ages... Suddenly, you realise... You are the hellish poet himself!!\n\r" );
        act( AT_BLUE, "$n says some magic words, and surrounds $mself by mist as $e becomes more lyrical, in a rather hellish way...\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     = 2500;
        ch->multi_int = 1000;
        ch->multi_wis = 1000;
        ch->multi_lck = 1000;        
        xSET_BIT( ch->affected_by, AFF_HELPOEMER );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_helpoemer );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_aedric( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_aedric]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_aedric]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_aedric ) )
    {
        learn_from_success( ch, gsn_aedric );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  Immediately a rune opens up on the ground, a strange pentagram, and begins to glow with a hellish light.  A rift is created between this world and the next, momentarily linking your world with that of the next.  Up from the portal comes an enormous mechanical beast, with blades for arms and a cannon for a head, with a convenient wizard-sized seat in the centre...\n\r" );
        act( AT_BLUE, "$n says some magic words, and transforms into a hellish mechanical device.\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     = 3600;
        ch->multi_str = 1000;
        ch->multi_con = 200;
        ch->multi_dex = 200;        
        ch->multi_lck = 200;     
        ch->armourboost = 10000;   
        xSET_BIT( ch->affected_by, AFF_AEDRIC );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_aedric );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_metalwarrior( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_metalwarrior]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_metalwarrior]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_metalwarrior ) )
    {
        learn_from_success( ch, gsn_metalwarrior );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  Metal armour begins to grow around your body, and you double in height as blades grow out of your forearms!\n\r" );
        act( AT_BLUE, "$n says some magic words, and armour begins to grow out all over $m!\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     = 500;
        ch->multi_str = 150;
        ch->multi_con = 100;
        ch->multi_wis =  50;
        ch->multi_cha = -50;
        xSET_BIT( ch->affected_by, AFF_METALWARRIOR );
        ch->armourboost = 2500;
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_metalwarrior );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_dragonslayer( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_dragonslayer]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_dragonslayer]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_dragonslayer ) )
    {
        learn_from_success( ch, gsn_dragonslayer );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your arms in the air.  Light armour grows around your body as you grow taller, and your right arm turns into a huge broadsword!!\n\r" );
        act( AT_BLUE, "$n says some magic words, and as light armour begins to grow all over $m, $n's right arm turns into a huge broadsword and $e grows taller!\n\r", ch, NULL, NULL, TO_CANSEE );
        xSET_BIT( ch->affected_by, AFF_DRAGONSLAYER );
        ch->plmod     = 1000;
        ch->multi_str = 300;
        ch->multi_con = 150;
        ch->multi_wis = 100;
        ch->multi_dex = 75;
        ch->armourboost = 1250;
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_dragonslayer );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}


void sum_vampirelord( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_vampirelord]->race_level[ch->race] < 100 || ch->basepl < 
skill_table[gsn_vampirelord]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_vampirelord ) )
    {
        learn_from_success( ch, gsn_vampirelord );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your arms in the air.\n\r&GA hellish green glow surrounds you as your skin turns deathly white and your hair goes light green.  You suddently lust for blood...\n\r" );
        act( AT_BLUE, "$n says some magic words, and $s skin turns pale white as $s hair turns light green!\n\r", ch, NULL, NULL, TO_CANSEE );
        xSET_BIT( ch->affected_by, AFF_VAMPIRE );
        ch->plmod     = 2200;
        ch->multi_str = 200;
        ch->multi_con = 200;
        ch->multi_dex = 300;
        ch->armourboost = 500;
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_vampirelord );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_vampirelich( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_vampirelich]->race_level[ch->race] < 100 || ch->basepl < 
skill_table[gsn_vampirelich]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_vampirelich ) )
    {
        learn_from_success( ch, gsn_vampirelich );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your arms in the air.\n\r&GA hellish green glow surrounds you as your skin turns deathly white and your hair goes light green.  You suddently lust for blood...\n\r" );
        act( AT_BLUE, "$n says some magic words, and $s skin turns pale white as $s hair turns light green!\n\r", ch, NULL, NULL, TO_CANSEE );
        xSET_BIT( ch->affected_by, AFF_VAMPIRE );
        ch->plmod     = 2000;
        ch->multi_con = 100;
        ch->multi_wis = 500;
        ch->multi_int = 500;
        ch->armourboost = 500;
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_vampirelord );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}

void sum_soulreaper( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_soulreaper]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_soulreaper]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_soulreaper ) )
    {
        learn_from_success( ch, gsn_soulreaper );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  The air turns cold as a gaint scythe made of dark matter forms in your hands and dark armour and a black cloak grow around you!\n\r" );
        act( AT_BLUE, "$n says some magic words, turning the air cold as a huge scythe appears and armour grows around $s body!\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     =    1200;
        ch->multi_str =     300;
        ch->multi_con =    -200;
        ch->multi_wis =     300;
        ch->armourboost =  1000;
        xSET_BIT( ch->affected_by, AFF_SOULREAPER );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_soulreaper );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}



void remove_summons( CHAR_DATA * ch )
{
  ch->plmod     = 100;
  ch->currpl    = ch->basepl;
  ch->multi_str = 0;
  ch->multi_dex = 0;
  ch->multi_con = 0;
  ch->multi_wis = 0;
  ch->multi_int = 0;
  ch->multi_cha = 0;
  ch->multi_lck = 0;
  ch->armourboost = 0;
  xREMOVE_BIT( ch->affected_by, AFF_SKULLZOMBIE  );
  xREMOVE_BIT( ch->affected_by, AFF_METALWARRIOR );
  xREMOVE_BIT( ch->affected_by, AFF_DRAGONSLAYER );
  xREMOVE_BIT( ch->affected_by, AFF_SOULREAPER );
  xREMOVE_BIT( ch->affected_by, AFF_VAMPIRE );
  xREMOVE_BIT( ch->affected_by, AFF_HERCULE );
  xREMOVE_BIT( ch->affected_by, AFF_HELPOEMER );
  xREMOVE_BIT( ch->affected_by, AFF_AEDRIC );
  xREMOVE_BIT( ch->affected_by, AFF_BLACKGUARD );      
  return;
}

void do_cast( CHAR_DATA *ch, char * argument )
{
 char arg[MAX_STRING_LENGTH];
 int  sn;

 if ( ch->race != RACE_WIZARD && !IS_IMMORTAL(ch) )
 {
      send_to_char( "Huh?\n\r", ch );
      return;
 }

 if ( IS_AFFECTED(ch, AFF_CHARGING) )
 {
     xREMOVE_BIT( ch->affected_by, AFF_CHARGING );
     send_to_char( "You stop charging.\n\r", ch );
 }
 argument = one_argument( argument, arg );

 if ( !str_cmp( arg, "list" ) || arg==NULL )
 {
       pager_printf( ch, "&RSpell book&W\n\r-------------------------------&C\n\r" );
       for ( sn = 0; sn < top_sn; sn++ )
       {
            if ( SPELL_FLAG( skill_table[sn], SF_WIZSPELL ) )
	    {
		if ( SPELL_FLAG( skill_table[sn], SF_SECRETSKILL ) && ch->pcdata->learned[sn] < 10 )
                 pager_printf( ch, "&w&W????????????????????   &C%3d&w/&c%3d%%\n\r", ch->pcdata->learned[sn], skill_table[sn]->race_adept[RACE_WIZARD] );
		else
                 pager_printf( ch, "&w&W%-20s   &C%3d&w/&c%3d%%\n\r", skill_table[sn]->name, ch->pcdata->learned[sn], skill_table[sn]->race_adept[RACE_WIZARD] );
 	    }
       }
       return;
 }

 if ( !str_cmp( arg, "Minor Heal" ) )
      wiz_minorheal( ch );
 else if ( !str_cmp( arg, "Greater Heal" ) )
      wiz_greaterheal( ch );
 else if ( !str_cmp( arg, "Matter Sphere" ) )
      wiz_mattersphere( ch );
 else if ( !str_cmp( arg, "Rock Bomb" ) )
      do_rockbomb( ch, "" );
 else if ( !str_cmp( arg, "Mana Tap" ) )
      wiz_manatap( ch);
 else if ( !str_cmp( arg, "Blood Suck" ) )
      wiz_bloodsuck( ch );
 else if ( !str_cmp( arg, "Ghoul Touch" ) )
      do_ghoultouch( ch, "" );
 else if ( !str_cmp( arg, "Mini Missile" ) )
      do_minimissile( ch, "" );
 else if ( !str_cmp( arg, "Blast" ) )
      do_blast( ch, "");
 else if ( !str_cmp( arg, "Arcanium" ) )
      do_arcanium( ch, "" );
 else if ( !str_cmp( arg, "Shabang" ) )
      do_shabang( ch, "" );
 else if ( !str_cmp( arg, "Mecha Cannon" ) )
      do_mechacannon( ch, "" );
 else if ( !str_cmp( arg, "Tetra Nova" ) )
      do_tetranova( ch, "" );
 else if ( !str_cmp( arg, "Legend of Gilgamesh" ) )
      do_gilgamesh( ch, "" );      
 else if ( !str_cmp( arg, "Arcane Thievery" ) )
      do_arcanetheft( ch, "" );      
 else if ( !str_cmp( arg, "Scry" ) )
      do_scry( ch, argument );
 else if ( !str_cmp( arg, "Soulsearch" ) )
      do_soulsearch( ch, "" );
 else
 {
      send_to_char( "Unknown spell.  Syntax: Cast '<Spell Name>'\n\rEg: Cast 'Ghoul Touch'\n\r", ch );
      return;
 }
 return;
}

void do_arcanetheft( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 {
         send_to_char( "You aren't fighting anyone.\n\r", ch );
         return;
 }

 if ( IS_NPC(victim) )
 {
	send_to_char( "This is really a Player versus Player skill.\n\r", ch );
	return;
 }

switch( ch->substate )
    {
        default:

        ch->skill_timer = 12;
        add_timer( ch, TIMER_DO_FUN, 12, do_arcanetheft, 1 );
        pager_printf(ch, "&cTorrents of Purple-Green energy explode from your opponent as you begin to sap all their fighting spirit away from them.\n\r" );
        act( AT_BLUE, "A shower of energy starts to leak from you as $n begins $s spell...\n\r",  ch, NULL, victim, TO_VICT );
        act( AT_BLUE, "A shower of energy starts to leak from $N as $n begins $s spell...\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->alloc_ptr = str_dup( arg );
        return;

         case 1:
            if ( !ch->alloc_ptr )
	    {
		send_to_char( "&cYour spell was interupted!\n\r", ch );
		act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
		bug( "do_search: alloc_ptr NULL", 0 );
		return;
            }
            strcpy( arg, ch->alloc_ptr );
            DISPOSE( ch->alloc_ptr );
            break;

         case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop reciting your spell.\n\r", ch );
	    act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
	    ch->skill_timer = 0;
            return;
    }

    ch->substate = SUB_NONE;
    if ( can_use_skill( ch, number_percent(), gsn_arcanetheft ) )
    {
	ch_printf( ch, "A number of runes explode from %s and shatter in midair, each one representing one of %s battle skills which you have rendered useless!\n\r", victim->name, HISHER( victim->sex ) );
	ch_printf( victim, "A number of glowing symbols float out of your body and explode in mid air... Strange, you didn't seem to take any damage...\n\r" );
	interpret( victim, "blink" );
	act( AT_YELLOW, "You smirk to yourself as $n renders all of $N's skills useless with $s spell.\n\r", ch, NULL, victim, TO_NOTVICT );
	xSET_BIT( victim->affected_by, AFF_ARCANE_THEFT );
	learn_from_success( ch, gsn_arcanetheft );
    }
    else
    {
	ch_printf( ch, "Nothing happens as you complete your spell.  Hmm... More study is required.\n\r" );
	ch_printf( victim, "You can't help but smile as %s's spell fizzles in %s hands.\n\r", ch->name, HISHER( ch->sex ) );
	act ( AT_YELLOW, "$n's spell does nothing.  What a disappointment.\n\r", ch, NULL, victim, TO_NOTVICT );
	learn_from_failure( ch, gsn_arcanetheft );
    }

}

void do_gilgamesh( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 char buf [MAX_STRING_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 { 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
 }    
 if ( ch->plmod < 2500 )
 {
    pager_printf( ch, "You need to be stronger to maintain that much concentration\n\r" );
    return;
 }

switch( ch->substate )
    {
	default:

        ch->skill_timer = 8;
        add_timer( ch, TIMER_DO_FUN, 8, do_gilgamesh, 1 );
	    pager_printf(ch, "&cExploding in a shower of mana, you begin a ritual to invoke the mighty immortal Gilgamesh, legendary warrior...\n\r" );
	    act( AT_BLUE, "$n waves $s hands in front of $m as $e begins some kind of ritual...\n\r",  ch, NULL, victim, TO_CANSEE );
	    ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&cYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop reciting your spell.\n\r", ch );
    	act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana > 25000 
      && can_use_skill(ch, number_percent(), gsn_gilgamesh )
      && get_curr_wis( ch ) * number_range( 2, 6 ) > ( get_curr_lck( victim ) + get_curr_dex( victim ) ) * number_range( 1, 4 ) )
    {
   	  learn_from_success( ch, gsn_gilgamesh );
          pager_printf( ch, "&cYou scream out the magic words '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c'\n\r&RFar in the distance, a deep bell sounds as midnight magically comes over the area.  From behind a tree showered in lotus blossom steps a man in a large red cloak, covering his mouth and lower face.  He approaches %s, and gives a short, hollow laugh, before drawing a mighty two-handed sword and slicing %s clean in two!\n\r", PERS( victim, ch ), HIMHER( victim->sex ) );
          pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'\n\r&RFar in the distance, a deep bell sounds as midnight magically comes over the area.  From behind a tree showered in lotus blossom steps a man in a large red cloak, covering his mouth and lower face.  He approaches you, and gives a short, hollow laugh, before drawing a mighty two-handed sword and slicing you clean in two!\n\r&WHOLY HELL THAT HURT!\n\r", ch->name );
          act( AT_GREEN, "$n screams 'PAPARAPA!!'\n\r&RFar in the distance, a deep bell sounds as midnight magically comes over the area.  From behind a tree showered in lotus blossom steps a man in a large red cloak, covering his mouth and lower face.  He approaches $N, and gives a short, hollow laugh, before drawing a mighty two-handed sword and slicing $M clean in two!",  ch, NULL, victim, TO_NOTVICT );   
          ch->basepl +=  ( victim->hit * 5 );
	  if ( !IS_NPC(ch ))
	      ch->pcdata->pl_today += victim->hit *5;
          ch->mana -= 25000;
          raw_kill( ch, victim );
          sprintf( buf, "&WIt looks like %s has managed to contract Gilgamesh to do %s bidding...", ch->name, HISHER( ch->sex ) );
          talk_info( AT_PINK, buf );
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'  Nothing happens...", ch, NULL, NULL, TO_CANSEE );
      if ( ch->mana < 25000 )
            send_to_char ( "Need 25,000 mana...\n\r", ch );
	  learn_from_failure( ch, gsn_gilgamesh );
    }
return;
}


void  do_wrath( CHAR_DATA *ch, char *argument )
{
}

void  do_blast( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	   send_to_char( "You aren't fighting anyone.\n\r", ch );
	   return;
	}    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 4;
        add_timer( ch, TIMER_DO_FUN, 4, do_blast, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for 'blast'.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&GYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
         DISPOSE( ch->alloc_ptr );
         ch->substate = SUB_NONE;
         send_to_char( "You stop reciting your spell.\n\r", ch );
       	 act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
         ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana >= 10000 && can_use_skill(ch, number_percent(),gsn_blast ) )
    {
   	 learn_from_success( ch, gsn_blast);
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'  %s begins to look a little ill, and then suddenly explodes with internal energy!\n\r", victim->name );
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'  At first it seems nothing has happened... but suddenly you feel a central core of energy inside you growing so fast you cannot keep control of it!  It explodes from every pore!  PAINFUL!\n\r", ch->name );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' and $N explodes in an aura of magical energy!!\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, URANGE(0, ( number_range( 250, 400 ) * ( get_curr_wis(ch) / 120 )), 4000 ), gsn_blast );
     if ( number_range( 1, 100 ) == 1 )
     {
        send_to_char( "Yes!  Success!  An instant kill!\n\r", ch );
        send_to_char( "Oh shit... The energy is a little more than you can handle...\n\r", victim );
	    act( AT_BLOOD, "$N explodes in a shower of meaty gibs.  Mmm, tasty.\n\r",  ch, NULL, victim, TO_NOTVICT );
        raw_kill( ch, victim );
     }
     ch->mana -= 10000;
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	  learn_from_failure( ch, gsn_blast);
	  if ( ch->mana < 10000 )
	     send_to_char ( "Need 10,000 mana\n\r", ch );
	  global_retcode = damage( ch, victim, 0, gsn_blast );
    }
return;
}
void  do_arcanium( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	   send_to_char( "You aren't fighting anyone.\n\r", ch );
	   return;
	}    

    switch( ch->substate )
    {
	default:
            ch->skill_timer = 10;
            add_timer( ch, TIMER_DO_FUN, 10, do_arcanium, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for 'arcanium'.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );
            ch->alloc_ptr = str_dup( arg );
	 return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	    {
                send_to_char( "&GYour spell was interupted!\n\r", ch );
                act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
                bug( "do_search: alloc_ptr NULL", 0 );
                return;
            }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	 break;

	 case SUB_TIMER_DO_ABORT:
            DISPOSE( ch->alloc_ptr );
            ch->substate = SUB_NONE;
            send_to_char( "You stop reciting your spell.\n\r", ch );
            act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
            ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;
    if ( ch->mana >= 10000 && can_use_skill(ch, number_percent(),gsn_arcanium ) )
    {
   	 learn_from_success( ch, gsn_arcanium );
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'  You invoke a small disc of energy and hurl it at %s.  It explodes on contact!\n\r", victim->name );
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and hurls a disc of %s &Genergy at you - it explodes on contact!\n\r", ch->name, ch->energycolour );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' and hurls an explosive disc of energy at $N!!\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, URANGE(0,( number_range( 1000, 2000 ) * ( get_curr_wis(ch) / 500 ) ),8000), gsn_arcanium );
         ch->mana -= 10000;
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
      learn_from_failure( ch, gsn_arcanium );
      if ( ch->mana < 10000 )
          send_to_char ( "You need 10,000 mana.\n\r", ch );
      global_retcode = damage( ch, victim, 0, gsn_arcanium );
    }
}
void  do_shabang( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	   send_to_char( "You aren't fighting anyone.\n\r", ch );
	   return;
	}    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 10;
        add_timer( ch, TIMER_DO_FUN, 10, do_shabang, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for 'shabang'.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&GYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
         DISPOSE( ch->alloc_ptr );
         ch->substate = SUB_NONE;
         send_to_char( "You stop reciting your spell.\n\r", ch );
       	 act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
         ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana >= 5000 && can_use_skill(ch, number_percent(),gsn_shabang ) )
    {
   	 learn_from_success( ch, gsn_shabang );
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, &w&W'&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' You point one hand at %s and say &R'Bow to your master!'&G  You feel %s energy disappearing, and eagerly await him dropping out of %s current form!\n\r", victim->name, HISHER(victim->sex), HIMHER(victim->sex));
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and tells you to bow to your master!  Suddenly, you feel all your energy draining from your body...\n\r", ch->name, ch->energycolour );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' orders $N to bow to $S master!\n\r",  ch, NULL, victim, TO_NOTVICT );
         victim->mana = number_range( 1000, 5000 );
         global_retcode = damage( ch, victim, ( number_range( 10, 20 ) ), gsn_shabang );
         ch->mana -= 5000;    
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	  learn_from_failure( ch, gsn_shabang );
	  if ( ch->mana < 5000 )
	     send_to_char ( "Need 5,000 mana\n\r", ch );	  
	  global_retcode = damage( ch, victim, 0, gsn_shabang );
    }
}
void  do_mechacannon( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 int cost;

    if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	   send_to_char( "You aren't fighting anyone.\n\r", ch );
	   return;
	}    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_mechacannon, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for 'mecha cannon'.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&GYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
         DISPOSE( ch->alloc_ptr );
         ch->substate = SUB_NONE;
         send_to_char( "You stop reciting your spell.\n\r", ch );
       	 act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
         ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(),gsn_mechacannon ) )
    {
   	 learn_from_success( ch, gsn_mechacannon );
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'  You invoke a magical hand cannon, point it at %s, and fire a chunk of your mana at %s!\n\r", victim->name, HIMHER(victim->sex));
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and fires a magical hand cannon at you! Although it hurts like hell, you feel the magical energy draining significantly from the freaky little wizard...\n\r", ch->name );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' orders $N to bow to $S master!\n\r",  ch, NULL, victim, TO_NOTVICT );
	 cost = number_range( ch->mana / 100, ch->mana );
	 ch->mana -= cost;
         global_retcode = damage( ch, victim, cost, gsn_mechacannon );
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	  learn_from_failure( ch, gsn_mechacannon );
	  global_retcode = damage( ch, victim, 0, gsn_mechacannon );
    }
}
void  do_tetranova( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];
 int cost;

    if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	   send_to_char( "You aren't fighting anyone.\n\r", ch );
	   return;
	}    

    switch( ch->substate )
    {
	default:

        ch->skill_timer = 15;
        add_timer( ch, TIMER_DO_FUN, 15, do_tetranova, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for 'Tetra Nova'.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );
        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&GYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
         DISPOSE( ch->alloc_ptr );
         ch->substate = SUB_NONE;
         send_to_char( "You stop reciting your spell.\n\r", ch );
       	 act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
         ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana >= 15000 && can_use_skill(ch, number_percent(),gsn_tetranova ) )
    {
   	 learn_from_success( ch, gsn_tetranova );
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G'  Next you fire off four blasts of ki at %s, one for each element!\n\r", victim->name, HIMHER(victim->sex));
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and fires off four blasts of elemental ki at you!\n\r", ch->name );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' orders $N to bow to $S master!\n\r",  ch, NULL, victim, TO_NOTVICT );
     cost = URANGE(1,get_curr_wis(ch) / 900,5);
     cost *= number_range( 500, 1000 );
     send_to_char( "&CHere goes water!\n\r", ch );
     global_retcode = damage( ch, victim, cost, gsn_tetranova );
     send_to_char( "&RFlame on!\n\r", ch );
     cost *= 1.25;
     global_retcode = damage( ch, victim, cost, gsn_tetranova );
     send_to_char( "&OEat mud and die!\n\r", ch );
     cost *= 1.25;
     global_retcode = damage( ch, victim, cost, gsn_tetranova );
     send_to_char( "&WFeel the wind!\n\r", ch );
     cost *= 1.5;
     global_retcode = damage( ch, victim, cost, gsn_tetranova );
     if ( number_range( 1, 100 ) <= 5 )
     {
        send_to_char( "&zMay as well throw in death... DIE!\n\r", ch );
        raw_kill( ch, victim );
     }
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	  if ( ch->mana < 15000 )
	     send_to_char ( "Need 15,000 mana\n\r", ch );	        
	  learn_from_failure( ch, gsn_tetranova );
	  global_retcode = damage( ch, victim, 0, gsn_tetranova );
    }
}

void do_cregen( CHAR_DATA *ch, char * argument )
{
 if ( who_fighting( ch ) == NULL )
 {
      send_to_char( "You're not fighting anyone\n\r", ch );
      return;
 }

 if ( ch->hit == ch->max_hit )
 {
      send_to_char( "You are not injured.\n\r", ch );
      return;
 }

 if ( !IS_NPC(ch) && LEARNED(ch, gsn_cregen) < ( ( ch->hit / ch->max_hit ) * 100 ) )
 {
      send_to_char( "Your circuit regenerators fail to activate right.", ch );
      return;
 }

 WAIT_STATE( ch, 2 * PULSE_PER_SECOND );
 if ( can_use_skill( ch, number_percent(), gsn_cregen ) )
 {
    learn_from_success( ch, gsn_cregen );
    pager_printf( ch, "&GA light %s &Gglow surrounds you as you replace some damaged circuits.\n\r", ch->energycolour );
    act( AT_GREEN, "$n glows faintly as $e heals a couple of lower functions.\n\r", ch, NULL, NULL, TO_CANSEE );
    ch->hit += ( ch->max_hit / 100 );
    if ( ch->hit > ch->max_hit )
         ch->hit = ch->max_hit;
 }
 else
 {
    learn_from_failure( ch, gsn_cregen );
    send_to_char( "Your circuit regenerators fail to activate right.", ch );
 }

 return;
}


void wiz_minorheal( CHAR_DATA *ch )
{
 if ( who_fighting( ch ) != NULL || IS_NPC( ch ) )
 {
      send_to_char( "In the middle of a fight?\n\r", ch );
      return;
 }

 if ( ch->hit == ch->max_hit )
 {
      send_to_char( "You are not injured.\n\r", ch );
      return;
 }
 if ( ch->mana < 1000 )
 {
      send_to_char ( "Need more mana...\n\r", ch );
      return;
 }

 WAIT_STATE( ch, 2 * PULSE_PER_SECOND );
 if ( can_use_skill( ch, number_percent(), gsn_minorheal ) )
 {
    learn_from_success( ch, gsn_minorheal );
    pager_printf( ch, "&GA light %s &Gglow surrounds you as you heal a few wounds.\n\r", ch->energycolour );
    act( AT_GREEN, "$n glows faintly as $e heals a couple of wounds.\n\r", ch, NULL, NULL, TO_CANSEE );
    ch->mana -= 1000;
    ch->hit += ( ( ( ch->max_hit / 5 ) / 100 ) * ch->pcdata->learned[gsn_minorheal] );
    if ( ch->hit > ch->max_hit )
         ch->hit = ch->max_hit;
 }
 else
 {
    learn_from_failure( ch, gsn_minorheal );
    pager_printf( ch, "&GYou try to heal yourself, but forget a section of the incantation.\n\r" );
    act( AT_GREEN, "$n looks vaguely frustrated as $e tries to recall a spell.\n\r", ch, NULL, NULL, TO_CANSEE );
 }
 return;
}


void wiz_mattersphere( CHAR_DATA *ch )
{
 if ( who_fighting( ch ) != NULL || IS_NPC( ch ) )
 {
      send_to_char( "In the middle of a fight?\n\r", ch );
      return;
 }

 if ( ch->armourboost >= 1000 )
 {
      send_to_char( "You already have a mattersphere up.\n\r", ch );
      return;
 }

 WAIT_STATE( ch, 3 * PULSE_PER_SECOND );
 if ( can_use_skill( ch, number_percent(), gsn_mattersphere ) )
 {
    learn_from_success( ch, gsn_mattersphere );
    pager_printf( ch, "&GYou throw your arms up in the air and a shield of %s &Genergy forms around you.\n\r", ch->energycolour );
    act( AT_GREEN, "$n throws $s arms up in the air, and a shield of energy forms around $m.\n\r", ch, NULL, NULL, TO_CANSEE );
    ch->armourboost = 100 * ch->pcdata->learned[gsn_mattersphere];
 }
 else
 {
    learn_from_failure( ch, gsn_mattersphere );
    pager_printf( ch, "&GYou try to conjour a shield up around you, but nothing happens.\n\r" );
    act( AT_GREEN, "$n looks vaguely frustrated as $e tries to recall a spell.\n\r", ch, NULL, NULL, TO_CANSEE );
 }
 return;
}

void do_rockbomb( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

        ch->skill_timer = 6;
        add_timer( ch, TIMER_DO_FUN, 6, do_rockbomb, 1 );
	    pager_printf(ch, "&GYou invoke your \"Big Book O' Spells\" and begin reciting the incantation for rockbomb.\n\r" );
	    act( AT_GREEN, "$n invokes $s spell book and begins to look up a spell...\n\r",  ch, NULL, victim, TO_CANSEE );


        ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&GYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
         DISPOSE( ch->alloc_ptr );
         ch->substate = SUB_NONE;
         send_to_char( "You stop reciting your spell.\n\r", ch );
       	 act( AT_GREEN, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
         ch->skill_timer = 0;
	 return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana >= 250 && can_use_skill(ch, number_percent(),gsn_rockbomb ) )
    {
   	 learn_from_success( ch, gsn_rockbomb );
   	 ch->mana -= 250;
	 pager_printf( ch, "&GYou scream the finishing clause of the spell, '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G', and an almighty collection of magic rocks fall on %s's head!\n\r", victim->name );
	 pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and a huge collection of glowing %s &Grocks fall on your head!\n\r", ch->name, ch->energycolour );
	 act( AT_GREEN, "$n screams 'PAPARAPA!!' and invokes a ton of magic rocks which fall on $n's head!\n\r",  ch, NULL, victim, TO_NOTVICT );
         global_retcode = damage( ch, victim, URANGE(0,( number_range( 350, 500 ) * ( get_curr_wis(ch) / 100 ) ),3000), gsn_rockbomb );
    }
    else
    {
      pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
      act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
  	  if ( ch->mana < 250 )
	     send_to_char ( "Need 250 mana\n\r", ch );	  
	  learn_from_failure( ch, gsn_rockbomb );
	  global_retcode = damage( ch, victim, 0, gsn_rockbomb );
    }
return;
}

void do_ghoultouch( CHAR_DATA *ch, char *argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    

switch( ch->substate )
    {
	default:

            ch->skill_timer = 4;
        add_timer( ch, TIMER_DO_FUN, 4, do_ghoultouch, 1 );
	    pager_printf(ch, "&cYou wave your hands in front of you and begin to change an incantation as you open a rift between this world and the shadow realm!\n\r" );
	    act( AT_BLUE, "$n waves $s hands in front of $m as $e begins to chant about some kind of portal...\n\r",  ch, NULL, victim, TO_CANSEE );
            ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&cYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop reciting your spell.\n\r", ch );
    	act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( ch->mana >= 7500 && can_use_skill(ch, number_percent(), gsn_ghoultouch ) )
    {
   	  learn_from_success( ch, gsn_ghoultouch );
      pager_printf( ch, "&cYou scream out the magic words '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c' and a large hand comes out of the portal you made, draining some life from %s!\n\r", PERS( victim, ch ) );
      pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and a skelletal hand extends from nowhere and drains some life out of you!\n\r", ch->name );
      act( AT_GREEN, "$n screams 'PAPARAPA!!' and a skelletal hand appears, draining some life from $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, URANGE(0,( number_range( 100, 150 ) * ( get_curr_wis(ch) / 150 ) ),2000), gsn_ghoultouch );
      ch->hit += ( number_range( 100, 150 ) * get_curr_wis(ch) / 150 );
      ch->mana -= 7500;
    }
    else
    {
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	learn_from_failure( ch, gsn_ghoultouch );
	  if ( ch->mana < 7500 )
	     send_to_char ( "Need 7,500 mana\n\r", ch );	  	
	global_retcode = damage( ch, victim, 0, gsn_ghoultouch );
    }
return;
}

void do_minimissile( CHAR_DATA *ch, char * argument )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

    if ( ( victim = who_fighting( ch ) ) == NULL )
    { 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
    }    

    switch( ch->substate )
    {
        default:
          ch->skill_timer = 3;
          add_timer( ch, TIMER_DO_FUN, 3, do_minimissile, 1 );
	  send_to_char ( "You start to recite a spell.\n\r", ch );
	  send_to_char ( "Your opponent starts counting backwards from 3...\n\r", victim );
	  ch->alloc_ptr = str_dup( arg );
	return;


        case 1:
          if ( !ch->alloc_ptr )
          {
             send_to_char( "&cYour spell was interupted!\n\r", ch );
             act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
             bug( "do_search: alloc_ptr NULL", 0 );
             return;
          }
          strcpy( arg, ch->alloc_ptr );
          DISPOSE( ch->alloc_ptr );
        break;

        case SUB_TIMER_DO_ABORT:
          DISPOSE( ch->alloc_ptr );
          ch->substate = SUB_NONE;
          send_to_char( "You stop reciting your spell.\n\r", ch );
          act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
          ch->skill_timer = 0;
        return;
    }
    ch->substate = SUB_NONE;
    if ( can_use_skill(ch, number_percent(), gsn_minimissile ) )
    {
   	  learn_from_success( ch, gsn_minimissile );
	  pager_printf( ch, "&cYou form a very small ball of %s &Gmana and fire it at %s!\n\r", victim->name );
	  pager_printf( victim, "&c%s forms a very tiny ball of mana and fires it at you!\n\r", ch->name, ch->energycolour );
	  act( AT_GREEN, "$n screams 'PAPARAPA!!' and spams a few magic balls at $n!\n\r",  ch, NULL, victim, TO_NOTVICT );
          global_retcode = damage( ch, victim, number_range( 40, 60 ), gsn_minimissile );
    }
    else
    {
          pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
          act( AT_BLUE, "$n screams 'Parapa The Rappa!', trying to form an energyball.\n\r", ch, NULL, NULL, TO_CANSEE );
	  learn_from_failure( ch, gsn_minimissile );
	  global_retcode = damage( ch, victim, 0, gsn_minimissile );
    }
return;
}


void do_break( CHAR_DATA *ch, char * argument )
{
}
void do_bloodsuck(  CHAR_DATA *ch, char *argument )
{
wiz_bloodsuck( ch );
return;
}
void wiz_bloodsuck( CHAR_DATA *ch )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    
 if ( ch->plmod != 2000 && ch->plmod != 2200 )
 {
    pager_printf( ch, "Ewwww!  What do you want blood for?!\n\r" );
    return;
 }

switch( ch->substate )
    {
	default:

            ch->skill_timer = 7;
        add_timer( ch, TIMER_DO_FUN, 7, do_bloodsuck, 1 );
	    pager_printf(ch, "&cYou wave your hands in front of you and begin to change an incantation as you open a rift between this world and the shadow realm!\n\r" );
	    act( AT_BLUE, "$n waves $s hands in front of $m as $e begins to chant about some kind of portal...\n\r",  ch, NULL, victim, TO_CANSEE );
            ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&cYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop reciting your spell.\n\r", ch );
    	act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;
    if ( ch->mana >= 5000 && can_use_skill(ch, number_percent(), gsn_bloodsuck ) )
    {
   	  learn_from_success( ch, gsn_bloodsuck );
      pager_printf( ch, "&cYou scream out the magic words '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c' and a face appears, looming above your opponent...  The jaws open, and take a bite out of %s!  You feel greatly strengthened...\n\r", PERS( victim, ch ) );
      pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and a gaint face appears from nowhere, biting you and draining some of your blood!\n\r", ch->name );
      act( AT_GREEN, "$n screams 'PAPARAPA!!' and a vampiric face appears, draining some life from $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( 400, 600 ) * ( get_curr_wis(ch) / 200 ) ), gsn_bloodsuck );
      ch->hit += ( number_range( 100, 150 ) * get_curr_wis(ch) / 150 );
      ch->mana -= 5000;
    }
    else
    {
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	  if ( ch->mana < 5000 )
	     send_to_char ( "Need 5,000 mana\n\r", ch );	          
	learn_from_failure( ch, gsn_bloodsuck );
	global_retcode = damage( ch, victim, 0, gsn_bloodsuck );
    }
return;
}



void do_manatap( CHAR_DATA *ch, char *argument )
{
wiz_manatap( ch );
}

void wiz_manatap( CHAR_DATA *ch )
{
 CHAR_DATA *victim;
 char arg [MAX_INPUT_LENGTH];

 if ( ( victim = who_fighting( ch ) ) == NULL )
 	{ 
	 send_to_char( "You aren't fighting anyone.\n\r", ch );	
	 return;    
	}    
 if ( ch->plmod != 2000 && ch->plmod != 2200 )
 {
    pager_printf( ch, "Such evil deeds are for the living dead - not you!!\n\r" );
    return;
 }

switch( ch->substate )
    {
	default:

            ch->skill_timer = 20;
        add_timer( ch, TIMER_DO_FUN, 20, do_manatap, 1 );
	    pager_printf(ch, "&cExploding in a shower of mana, you begin a ritual to turn your opponent's own strength into attack power...\n\r" );
	    act( AT_BLUE, "$n waves $s hands in front of $m as $e begins some kind of ritual...\n\r",  ch, NULL, victim, TO_CANSEE );
            ch->alloc_ptr = str_dup( arg );
	    return;

	 case 1:
	    if ( !ch->alloc_ptr )
     	{
           send_to_char( "&cYour spell was interupted!\n\r", ch );
           act( AT_GREEN, "$n's spell was interrupted!\n\r",  ch, NULL, victim, TO_NOTVICT );
           bug( "do_search: alloc_ptr NULL", 0 );
           return;
        }
	    strcpy( arg, ch->alloc_ptr );
	    DISPOSE( ch->alloc_ptr );
	    break;

	 case SUB_TIMER_DO_ABORT:
	    DISPOSE( ch->alloc_ptr );
	    ch->substate = SUB_NONE;
	    send_to_char( "You stop reciting your spell.\n\r", ch );
    	act( AT_BLUE, "$n's stops reciting $s spell.\n\r",  ch, NULL, victim, TO_NOTVICT );
        ch->skill_timer = 0;
 	    return;
    }

    ch->substate = SUB_NONE;

    if ( can_use_skill(ch, number_percent(), gsn_manatap ) )
    {
   	  learn_from_success( ch, gsn_manatap );
      pager_printf( ch, "&cYou scream out the magic words '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c' and a vile chain of energy springs from the ground and twists around %s, burning them with their own energy!\n\r", PERS( victim, ch ) );
      pager_printf( victim, "&G%s screams '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&G' and a powerful chain of energy springs from the ground and twists around you, burning you with your own energy!\n\r", ch->name );
      act( AT_GREEN, "$n screams 'PAPARAPA!!' and a chain of pure energy appears, wrapping itself around $N!\n\r",  ch, NULL, victim, TO_NOTVICT );
      global_retcode = damage( ch, victim, ( number_range( victim->mana / 100, victim->mana / 10 ) ), gsn_manatap );
      ch->mana += ( number_range( 100, 150 ) * get_curr_wis(ch) / 150 );
    }
    else
    {
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
	learn_from_failure( ch, gsn_manatap );
	global_retcode = damage( ch, victim, 0, gsn_manatap );
    }
return;
}



void sum_hercule( CHAR_DATA *ch )
{
    if ( ( skill_table[gsn_hercule]->race_level[ch->race] < 100 || ch->basepl < skill_table[gsn_hercule]->race_level[ch->race] ) && !IS_IMMORTAL(ch) )
    {
         send_to_char( "Not yet.\n\r", ch );
         return;
    }

    if ( can_use_skill( ch, number_percent(), gsn_hercule ) )
    {
        learn_from_success( ch, gsn_hercule );
        remove_summons( ch );
        pager_printf( ch, "&cYou scream '&zP&Ra&zp&Ra&zr&Ra&zp&Ra&z!&c', and wave your hands in the air.  A large afro springs up into the air on your head, and you hold your right hand out, two fingers in the peace sign.  'I am the Greatest!'\n\r" );
        act( AT_BLUE, "$n says some magic words, and a large afro springs up into the air on $s head, and $e holds $s right hand out, two fingers in the peace sign.  'I am the Greatest!'\n\r", ch, NULL, NULL, TO_CANSEE );
        ch->plmod     =    1000;
        ch->multi_lck =    1000;
        ch->multi_cha =    1000;
        xSET_BIT( ch->affected_by, AFF_HERCULE );
        return;
    }
    else
    {
        learn_from_failure( ch, gsn_hercule );
        pager_printf( ch, "&cYou scream 'Parapa The Rappa!', almost sure that you've done something wrong...\n\r" );
        act( AT_BLUE, "$n screams 'Parapa The Rappa!'\n\r", ch, NULL, NULL, TO_CANSEE );
        return;
    }
}


void wiz_greaterheal( CHAR_DATA *ch )
{
 if ( who_fighting( ch ) != NULL || IS_NPC( ch ) )
 {
      send_to_char( "In the middle of a fight?\n\r", ch );
      return;
 }

 if ( ch->hit == ch->max_hit )
 {
      send_to_char( "You are not injured.\n\r", ch );
      return;
 }

 WAIT_STATE( ch, 4 * PULSE_PER_SECOND );
 if ( can_use_skill( ch, number_percent(), gsn_greaterheal ) )
 {
    learn_from_success( ch, gsn_greaterheal );
    pager_printf( ch, "&GA bright and heavy %s &Gglow surrounds you as you heal a few wounds.\n\r", ch->energycolour );
    act( AT_GREEN, "$n glows vibrantly as $e heals a number of wounds.\n\r", ch, NULL, NULL, TO_CANSEE );
    ch->hit += ( ( ( ch->max_hit / 2 ) / 100 ) * ch->pcdata->learned[gsn_greaterheal] );
    if ( ch->hit > ch->max_hit )
         ch->hit = ch->max_hit;
 }
 else
 {
    learn_from_failure( ch, gsn_greaterheal );
    pager_printf( ch, "&GYou try to heal yourself, but forget a section of the incantation.\n\r" );
    act( AT_GREEN, "$n looks vaguely frustrated as $e tries to recall a spell.\n\r", ch, NULL, NULL, TO_CANSEE );
 }
 return;
}


void do_spellbook( CHAR_DATA *ch, char *argument )
{
  if ( ch->race != RACE_WIZARD )
  {
      send_to_char( "Huh?\n\r", ch );
      return;
  }
  do_transform( ch, "list" );
  send_to_char( "\n\r", ch );
  do_cast( ch, "list" );
}
