/****************************************************************************
 * [S]imulated [M]edieval [A]dventure multi[U]ser [G]ame      |   \\._.//   *
 * -----------------------------------------------------------|   (0...0)   *
 * SMAUG 1.4 (C) 1994, 1995, 1996, 1998  by Derek Snider      |    ).:.(    *
 * -----------------------------------------------------------|    {o o}    *
 * SMAUG code team: Thoric, Altrag, Blodkai, Narn, Haus,      |   / ' ' \   *
 * Scryn, Rennard, Swordbearer, Gorog, Grishnakh, Nivek,      |~'~.VxvxV.~'~*
 * Tricops and Fireblade                                      |             *
 * ------------------------------------------------------------------------ *
 * Merc 2.1 Diku Mud improvments copyright (C) 1992, 1993 by Michael        *
 * Chastain, Michael Quan, and Mitchell Tse.                                *
 * Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,          *
 * Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.     *
 * ------------------------------------------------------------------------ *
 *                     Housing Module Header File                           *
 ****************************************************************************
 * Author : Yami                                                            *
 * E-Mail : diabound@gmail.com                                              *
 ****************************************************************************/
 
#define HOUSE_AREA	       "Houses"
#define HOUSE_PRICE		1000000
#define APARTMENT_PRICE		 500000
#define HOUSE_UPGRADE_COST	 750000
#define ROOM_COST		 200000
#define ROOM_ADD_HEAL_COST	 100000
#define ROOM_ADD_DUEL_COST	2000000
#define ROOM_ADD_GTRAIN_COST   10000000
#define HOUSE_DIR          "../houses/"
#define HOUSE_LIST          "house.lst"


#define HOUSE_MAX_SIZE		      5 // Doesn't include base room.
#define HOUSE_NONE		      0
#define HOUSE_APARTMENT		      1
#define HOUSE_FULL		      2
#define HOUSE_CLANHALL		      3

#define RES_ZONE_EARTH		   3910 // vnum for Earth's residential zone
#define RES_ZONE_NAMEK		   8289 // vnum for Namek's residential zone
#define RES_ZONE_ICER		   5219 // vnum for Icer's residential zone
#define RES_ZONE_VEGETA		  11020 // vnum for Vegeta's residential zone
#define RES_ZONE_BRUMA		  27167 // vnum for Vegeta's residential zone

void save_house_by_vnum(int vnum);
void do_house( CHAR_DATA *ch, char *argument );
